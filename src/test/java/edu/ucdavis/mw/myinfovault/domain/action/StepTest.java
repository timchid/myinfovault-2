/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: StepTest.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * JUnit tests for {@link Step}.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.4.1
 */
public class StepTest
{
    /*
     * Assertion message labels.
     */
    private static final String ABOVESCALE = "Above scale";
    private static final String NOTAPPLICABLE = "Not applicable";
    private static final String ONEHALF = "One-half";
    private static final String ONE = "One";
    private static final String THREEHALVES = "Three-halves";
    private static final String TEN = "Ten";

    /**
     * Acceptable arguments to {@link Step#Step(String)} keyed to assertion message labels.
     */
    private static final Map<String, List<String>> arguments =
        ImmutableMap.<String, List<String>>builder()
                    .put(ABOVESCALE, ImmutableList.of("-1", "A", "AS", "A/S", " -1.00  "))
                    .put(NOTAPPLICABLE, ImmutableList.of("0", "N", "NA", "N/A", "  00.000  "))
                    .put(ONEHALF, ImmutableList.of(".5", "00.5", ".500", "  .5", ".5  "))
                    .put(ONE, ImmutableList.of("1", "001", "1.00", "  1", "1  "))
                    .put(THREEHALVES, ImmutableList.of("1.5", "001.5", "1.500", "  1.5", "1.5  "))
                    .put(TEN, ImmutableList.of("10", "0010", "10.00", "  10", "10  "))
                    .build();

    /**
     * Expected decimal values from {@link Step#getValue()} keyed to assertion message labels.
     */
    private static final Map<String, BigDecimal> values =
        ImmutableMap.<String, BigDecimal>builder()
                    .put(ABOVESCALE, BigDecimal.ONE.negate())
                    .put(NOTAPPLICABLE, BigDecimal.ZERO)
                    .put(ONEHALF, new BigDecimal(".5"))
                    .put(ONE, BigDecimal.ONE)
                    .put(THREEHALVES, new BigDecimal("1.5"))
                    .put(TEN, BigDecimal.ONE.movePointRight(1))
                    .build();

    /**
     * Expected representations from {@link Step#toString()} keyed to assertion message labels.
     */
    private static final Map<String, String> representations =
        ImmutableMap.<String, String>builder()
                    .put(ABOVESCALE, "AS")
                    .put(NOTAPPLICABLE, "NA")
                    .put(ONEHALF, "0.5")
                    .put(ONE, "1")
                    .put(THREEHALVES, "1.5")
                    .put(TEN, "10")
                    .build();

    /**
     * Test unacceptable argument for {@link edu.ucdavis.mw.myinfovault.domain.action.Step#Step(String)}
     */
    @Test(expected = NumberFormatException.class)
    public void testUnacceptableEntryOne()
    {
        @SuppressWarnings("unused")
        Step step = new Step("asdf");
    }

    /**
     * Test unacceptable argument for {@link edu.ucdavis.mw.myinfovault.domain.action.Step#Step(String)}
     */
    @Test(expected = NumberFormatException.class)
    public void testUnacceptableEntryTwo()
    {
        @SuppressWarnings("unused")
        Step step = new Step("1.2.3");
    }

    /**
     * Test for {@link Step#Step(String)}, {@link Step#getValue()}, and {@link Step#toString()}.
     */
    @Test
    public void testStep()
    {
        /*
         * Loop over the keys for the arguments, values and
         * representations maps (keys for each are the same).
         */
        for (String label : arguments.keySet())
        {
            // expected decimal value for this test category
            BigDecimal value = values.get(label);

            // expected string representation for this test category
            String representation = representations.get(label);

            // loop thru acceptable arguments for this label
            for (String argument : arguments.get(label))
            {
                try
                {
                    // test constructor with argument
                    Step step = new Step(argument);

                    // test decimal value for constructed
                    assertEquals(label, value, step.getValue());

                    // test string representation for constructed
                    assertEquals(label, representation, step.toString());
                }
                catch (NumberFormatException e)
                {
                    fail("Bad argument: '" + argument + "'");
                }
            }
        }
    }
}
