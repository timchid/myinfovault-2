/**
 * 
 */
package edu.ucdavis.mw.myinfovault.domain.biosketch;

import junit.framework.TestCase;

/**
 * @author Stephen Paulsen
 */
public class RulesetTest extends TestCase
{
    Ruleset r;
    /* (non-Javadoc)
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        r = new Ruleset(0, 0);
        r.setOperator(Ruleset.Operator.valueOf("AND"));

        Criterion c;

        c = new Criterion(0);
        c.setField("A").setOperator(Criterion.RelOp.NE).setValue("a");
        r.addCriterion(c);

        c = new Criterion(0);
        c.setField("B").setOperator(Criterion.RelOp.GE).setValue("b");
        r.addCriterion(c);
    }

    /**
     * Test method for {@link edu.ucdavis.myinfovault.biosketch.Ruleset#setOperator(edu.ucdavis.myinfovault.biosketch.Ruleset.Operator)}.
     */
    public final void testSetOperator()
    {
        r.setOperator(Ruleset.Operator.AND);
        assertEquals("(A != 'a') AND (B >= 'b')", r.toString());

        r.setOperator(Ruleset.Operator.OR);
        assertEquals("(A != 'a') OR (B >= 'b')", r.toString());
    }

    /**
     * Test method for {@link edu.ucdavis.myinfovault.biosketch.Ruleset#toString()}.
     */
    public final void testToString()
    {
        assertEquals("(A != 'a') AND (B >= 'b')", r.toString());
    }

}
