/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HtmlUtilTest.java
 */


package edu.ucdavis.mw.myinfovault.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * JUnit tests for the HtmlUtil class utilities.
 * @author Stephen Paulsen
 */
public class HtmlUtilTest
{

    @Before
    public void setUp() //throws Exception
    {
        // Looks like this will need to set up Log4j in some way.
        // Strangely, the tests were working before, without doing that, and now they're not.

        // Ok, it seems to depend on the built/clean state of the eclipse project,
        // not necessarily tied to the maven build.
        // Doing --
        //    menus > Project > Clean...
        // followed by
        //    menus > Run > Run as... > JUnit Test
        // works fine.
    }


    /**
     * Fix tag case (from 'TABLE' to 'table'), eliminate spaces within the tag, remove newlines, add missing close tags.
     */
    @Test
    public final void testPurifyHTML1()
    {
        final String input  = "<TABLE    style=\"WIDTH: 510px; TABLE-LAYOUT1: fixed\"\nborder=1 width=510><tr><td>™¿©ÄÑìΔΓΒõ";
        final String expect = "<table style=\"WIDTH: 510px; TABLE-LAYOUT1: fixed\" border=\"1\" width=\"510\"><tr><td>™¿©ÄÑìΔΓΒõ</td></tr></table>";
        assertEquals(expect, HtmlUtil.purifyHTML(input));
    }


    /**
     * remove newlines ... preserve multiple consecutive spaces in the content
     */
    @Test
    public final void testPurifyHTML2()
    {
        final String input  = "<span>Line One</span><p>This has U<sup>2</sup> superscript \nin it.</p><p>Line Three ™¿©Ä  ÑìΔ<br /></p>";
        final String expect = "<span>Line One</span><p>This has U<sup>2</sup> superscript in it.</p><p>Line Three ™¿©Ä  ÑìΔ<br /></p>";
        assertEquals(expect, HtmlUtil.purifyHTML(input));
    }

    /**
     *
     */
    @Test
    public final void testPurifyHTML4()
    {
        final String input  = "<p>X   <sup> 2   </sup> - y     <sup>2     </sup> = (X-Y) < (X+Y)</p>";
        // Note the extra but escaped </p> near the end of the expected output.  This is not good, but this is what JTidy does.
        final String expect = "<p>X   <sup> 2   </sup> - y     <sup>2     </sup> = (X-Y) &lt; (X+Y)&lt;&lt;/p&gt;</p>";
        assertEquals(expect, HtmlUtil.purifyHTML(input));
    }

    /**
     * Remove newlines, eliminate spaces within the tag, replaces <br> with <br />
     */
    @Test
    public final void testPurifyHTML5()
    {
        final String input  = "<p  class=\"foo    bar\">Line1</p>\n<p><br></p>\n<p>Line3</p>\n<p><br /></p>\n<p><br></p>\n<p>Line6</p>";
        final String expect = "<p class=\"foo bar\">Line1</p><p><br /></p><p>Line3</p><p><br /></p><p><br /></p><p>Line6</p>";
        assertEquals(expect, HtmlUtil.purifyHTML(input));
    }



    /**
     * Fix improperly nested tags, preserve spaces in the text.
     */
    @Test
    public final void testPurifyHTML8()
    {
        final String input  = "<div>Text <span>span  text</div> post-text</span>";
        final String expect = "<div>Text <span>span  text</span></div> post-text";
        assertEquals(expect, HtmlUtil.purifyHTML(input));
    }

    /**
     *  Input: [<p>x <sup> 2 </sup></p>]
     * Output: [[<p>, <sup>, </sup>, </p>]]
     */
    @Test
    public final void testGetHtmlTags1()
    {
        final String input = "<p>x <sup> 2 </sup></p>";
        //final String[] expectA = { "<p>", "<sup>", "</sup>", "</p>" };
        final List<String> expect = Arrays.asList(new String[] { "<p>", "<sup>", "</sup>", "</p>" });
        assertEquals(expect, HtmlUtil.getHtmlTags(input));
    }


    /**
     * Preserve attributes in the tag.
     *  Input: [<p class="foo">x <sup> 2 </sup></p>]
     * Output: [[<p class="foo">, <sup>, </sup>, </p>]]
     */
    @Test
    public final void testGetHtmlTags2()
    {
        final String input = "<p class=\"foo\">x <sup> 2 </sup></p>";
        final List<String> expect = Arrays.asList(new String[] { "<p class=\"foo\">", "<sup>", "</sup>", "</p>" });
        assertEquals(expect, HtmlUtil.getHtmlTags(input));
    }
}
