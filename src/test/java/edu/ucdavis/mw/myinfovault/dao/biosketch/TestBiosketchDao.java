package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;

/**
 * @author dreddy
 * @since MIV 2.1
 */
@Ignore
public class TestBiosketchDao extends TestCase
{

    private BiosketchDao biosketchDao;

    @Override
    protected void setUp() throws Exception
    {
        ClassPathXmlApplicationContext util = new ClassPathXmlApplicationContext(
                new String[] { "/edu/ucdavis/mw/myinfovault/dao/biosketch/test-myinfovault-data.xml" });

        biosketchDao = (BiosketchDao) util.getBean("biosketchDao");
    }

    public void testFindBiosketches()
    {
        try
        {
            List<Biosketch> biosketchList = biosketchDao.findBiosketches(18807, 3);

            for (int i = 0; i < biosketchList.size(); i++)
            {
                Biosketch biosketch = biosketchList.get(i);
                System.out.println("BIOSKETCH - ID is " + biosketch.getId());
                System.out.println("BIOSKETCH - USERID is " + biosketch.getUserID());
                System.out.println("BIOSKETCH - NAME is " + biosketch.getName());
                System.out.println("BIOSKETCH - TITLE is " + biosketch.getTitle());
                System.out.println("BIOSKETCH - COLLECTIONID is " + biosketch.getBiosketchType());
                System.out.println("BIOSKETCH - STYLEID is " + biosketch.getStyleID());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testFindBiosketch()
    {
        try
        {
            Biosketch biosketch = biosketchDao.findBiosketch(3);
            System.out.println("BIOSKETCH - ID is " + biosketch.getId());
            System.out.println("BIOSKETCH - USERID is" + biosketch.getUserID());
            System.out.println("BIOSKETCH - NAME is " + biosketch.getName());
            System.out.println("BIOSKETCH - TITLE is " + biosketch.getTitle());
            System.out.println("BIOSKETCH - COLLECTIONID is " + biosketch.getBiosketchType());
            System.out.println("BIOSKETCH - STYLEID is " + biosketch.getStyleID());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testFindBiosketchAttributes()
    {
        try
        {
            //List<BiosketchAttributes> attributesList = biosketchDao.findBiosketchAttributesByBiosketchID(3);
            List<BiosketchAttributes> attributesList = biosketchDao.findBiosketchAttributes(3);

            for (int i = 0; i < attributesList.size(); i++)
            {
                BiosketchAttributes attributes = attributesList.get(i);
                System.out.println("BIOSKETCHATTRIBUTES - ID is " + attributes.getId());
                System.out.println("BIOSKETCHATTRIBUTES - BiosketchID is " + attributes.getBiosketchID());
                System.out.println("BIOSKETCHATTRIBUTES - name is " + attributes.getName());
                System.out.println("BIOSKETCHATTRIBUTES - value is " + attributes.getValue());
                System.out.println("BIOSKETCHATTRIBUTES - sequence is " + attributes.getSequence());
                System.out.println("BIOSKETCHATTRIBUTES - display is " + attributes.isDisplay());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testInsertBiosketch()
    {
        try
        {
            Biosketch biosketch = new Biosketch(18807, 3);
            biosketch.setName("Deepika");
            biosketch.setTitle("Deepika");
            biosketch.setStyleID(3);

            int biosketchID = biosketchDao.insertBiosketch(biosketch);
            System.out.println("auto generated biosketchid is " + biosketchID);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testUpdateBiosketch()
    {
        try
        {
            Biosketch biosketch = biosketchDao.findBiosketch(105);
            biosketchDao.updateBiosketch(biosketch);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testDeleteBiosketch()
    {
        try
        {
            biosketchDao.deleteBiosketch(103);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testInsertBiosketchAttributes()
    {
        try
        {
            BiosketchAttributes attributes = new BiosketchAttributes(0);
            attributes.setBiosketchID(101);
            attributes.setName("era name");
            attributes.setValue("deepika");
            attributes.setSequence(20);
            attributes.setDisplay(true);

            biosketchDao.insertBiosketchAttributes(attributes, 18807);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testUpdateBiosketchAttributes()
    {
        try
        {
            List<BiosketchAttributes> attributesList = biosketchDao.findBiosketchAttributes(3);

            for (int i = 0; i < attributesList.size(); i++)
            {
                BiosketchAttributes attributes = attributesList.get(i);
                biosketchDao.updateBiosketchAttributes(attributes, 18807);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testDeleteBiosketchAttributes()
    {
        try
        {
            biosketchDao.deleteBiosketchAttributes(100);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }
}
