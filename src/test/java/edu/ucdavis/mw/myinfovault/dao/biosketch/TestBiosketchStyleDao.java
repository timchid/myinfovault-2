package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle;
import edu.ucdavis.myinfovault.designer.Alignment;

/**
 * @author dreddy
 * @since MIV 2.1
 */
@Ignore
public class TestBiosketchStyleDao extends TestCase
{
    private BiosketchStyleDao biosketchStyleDao;

    @Override
    protected void setUp() throws Exception
    {
        ClassPathXmlApplicationContext util = new ClassPathXmlApplicationContext(
                new String[] { "/edu/ucdavis/mw/myinfovault/dao/biosketch/test-myinfovault-data.xml" });

        biosketchStyleDao = (BiosketchStyleDao) util.getBean("biosketchStyleDao");
    }

    public void testFindDefaultBiosketchStyle()
    {
        try
        {
            BiosketchStyle biosketchStyle = biosketchStyleDao.findDefaultBiosketchStyle(3);

            if (biosketchStyle != null)
            {
                System.out.println("BIOSKETCHSTYLE - ID is " + biosketchStyle.getId());
                System.out.println("BIOSKETCHSTYLE - USERID is " + biosketchStyle.getUserID());
                System.out.println("BIOSKETCHSTYLE - STYLENAME is " + biosketchStyle.getStyleName());
                System.out.println("BIOSKETCHSTYLE - PAGEWIDTH is " + biosketchStyle.getPageWidth());
                System.out.println("BIOSKETCHSTYLE - PAGEHEIGHT is " + biosketchStyle.getPageHeight());
            }
            else
            {
                System.out.println("BiosketchStyle IS NULL");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();

        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testFindBiosketchStyleById()
    {
        try
        {
            BiosketchStyle biosketchStyle = biosketchStyleDao.findBiosketchStyleById(100);

            if (biosketchStyle != null)
            {
                System.out.println("BIOSKETCHSTYLE - ID is " + biosketchStyle.getId());
                System.out.println("BIOSKETCHSTYLE - USERID is " + biosketchStyle.getUserID());
                System.out.println("BIOSKETCHSTYLE - STYLENAME is " + biosketchStyle.getStyleName());
                System.out.println("BIOSKETCHSTYLE - PAGEWIDTH is " + biosketchStyle.getPageWidth());
                System.out.println("BIOSKETCHSTYLE - PAGEHEIGHT is " + biosketchStyle.getPageHeight());
            }
            else
            {
                System.out.println("IT IS NULL");
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();

        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testInsertBiosketchStyle()
    {
        try
        {
            BiosketchStyle biosketchStyle = new BiosketchStyle(0, 18807);
            biosketchStyle.setStyleName("Deepika Style");
            biosketchStyle.setBodyFontID(1);
            biosketchStyle.setBodyFormatting(true);
            biosketchStyle.setBodyIndent(1);
            biosketchStyle.setBodySize(1);
            biosketchStyle.setFooterOn(true);
            biosketchStyle.setFooterPageNumbers(true);
            biosketchStyle.setFooterRules(true);
            biosketchStyle.setHeaderAlign(Alignment.LEFT);
            biosketchStyle.setHeaderFontID(1);
            biosketchStyle.setHeaderIndent(1);
            biosketchStyle.setHeaderSize(1);
            biosketchStyle.setMarginBottom(1);
            biosketchStyle.setMarginLeft(1);
            biosketchStyle.setMarginRight(1);
            biosketchStyle.setMarginTop(1);
            biosketchStyle.setPageHeight(1);
            biosketchStyle.setPageWidth(1);
            biosketchStyle.setTitleAlign(Alignment.LEFT);
            biosketchStyle.setTitleEditable(true);
            biosketchStyle.setTitleFontID(1);
            biosketchStyle.setTitleRules(true);
            biosketchStyle.setTitleSize(1);

            int styleid = biosketchStyleDao.insertBiosketchStyle(biosketchStyle);

            System.out.println("biosketchstyle id returned is " + styleid);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testUpdateBiosketchStyle()
    {
        try
        {
            BiosketchStyle biosketchStyle = biosketchStyleDao.findDefaultBiosketchStyle(100);
            biosketchStyle.setStyleName("Deepika Style edited");

            biosketchStyleDao.updateBiosketchStyle(biosketchStyle);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testDeleteBiosketchStyle()
    {
        try
        {
            biosketchStyleDao.deleteBiosketchStyle(102);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }
}
