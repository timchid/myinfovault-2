/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LdapPersonDataSourceTest.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.Collection;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.ucdavis.mw.myinfovault.common.test.BaseMivJunit;
import edu.ucdavis.mw.myinfovault.service.person.PersonDataSource.PersonDTO;

/**
 * Test case for {@link LdapPersonDataSource}.
 *
 * @author Craig Gilmore
 * @since MIV ?
 */

public class LdapPersonDataSourceTest extends BaseMivJunit
{
    private static final String PRINCIPAL_NAME = "fzmccart";
    private static final String EMAIL = "bllerol@ucdavis.edu";
    private static final String PERSON_ID = "00003845";

    @BeforeClass
    public static void setUp()
    {
        BaseMivJunit.setUp();
    }

    @Autowired
    private LdapPersonDataSource ldapPersonDataSource;

    @Test
    public void testGetPersonFromId()
    {
        PersonDTO person = ldapPersonDataSource.getPersonFromId(PERSON_ID);

        Assert.assertNotNull("Person DTO", person);
    }

    @Test
    public void testGetPersonFromPrincipal()
    {
        PersonDTO person = ldapPersonDataSource.getPersonFromPrincipal(PRINCIPAL_NAME);

        Assert.assertNotNull("Person DTO", person);
    }

    @Test
    public void testGetPersonFromEmail()
    {
        Collection<PersonDTO> people = ldapPersonDataSource.getPersonFromEmail(EMAIL);

        Assert.assertTrue("More than one listing for " + EMAIL, people.size() > 1);
    }

    @Test
    public void testIsActive()
    {
        boolean active = ldapPersonDataSource.isActive(PERSON_ID);

        Assert.assertTrue(PERSON_ID + " is active", active);
    }

}