package edu.ucdavis.mw.myinfovault.service.biosketch;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset.Operator;
import edu.ucdavis.myinfovault.designer.Alignment;

/**
 * @author dreddy
 *
 */
@Ignore
public class TestBiosketchService extends TestCase
{

    private IBiosketchService biosketchService;

    @Override
    protected void setUp() throws Exception
    {
        ClassPathXmlApplicationContext util = new ClassPathXmlApplicationContext(
                new String[] { "/edu/ucdavis/mw/myinfovault/service/biosketch/test-myinfovault-service.xml" });
        biosketchService = (BiosketchService) util.getBean("biosketchService");
    }

    public void testGetBiosketchList()
    {
        try
        {
            List<Biosketch> biosketchList = biosketchService.getBiosketchList(18807, 3);
            System.out.println("size of biosketchList is " + biosketchList.size());
            for (int i = 0; i < biosketchList.size(); i++)
            {
                Biosketch biosketch = biosketchList.get(i);
                System.out.println("Biosketch -> ID " + biosketch.getId());
                System.out.println("Biosketch -> UserId " + biosketch.getUserID());
                System.out.println("Biosketch -> Name " + biosketch.getName());
                System.out.println("Biosketch -> Title " + biosketch.getTitle());
                System.out.println("Biosketch -> BiosketchType " + biosketch.getBiosketchType());
                System.out.println("Biosketch -> StyleID " + biosketch.getStyleID());
            }
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testGetBiosketch()
    {
        try
        {
            Biosketch biosketch = biosketchService.getBiosketch(151);
            System.out.println("Biosketch -> ID " + biosketch.getId());
            System.out.println("Biosketch -> UserId " + biosketch.getUserID());
            System.out.println("Biosketch -> Name " + biosketch.getName());
            System.out.println("Biosketch -> Title " + biosketch.getTitle());
            System.out.println("Biosketch -> BiosketchType " + biosketch.getBiosketchType());
            System.out.println("Biosketch -> StyleID " + biosketch.getStyleID());

            BiosketchStyle biosketchStyle = biosketch.getBiosketchStyle();
            System.out.println("BiosketchStyle -> ID is " + biosketchStyle.getId());
            System.out.println("BiosketchStyle -> UserID is " + biosketchStyle.getUserID());
            System.out.println("BiosketchStyle -> StyleName is " + biosketchStyle.getStyleName());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getBodyFontID());
            System.out.println("BiosketchStyle -> " + biosketchStyle.isBodyFormatting());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getBodyIndent());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getBodySize());
            System.out.println("BiosketchStyle -> " + biosketchStyle.isFooterOn());
            System.out.println("BiosketchStyle -> " + biosketchStyle.isFooterPageNumbers());
            System.out.println("BiosketchStyle -> " + biosketchStyle.isFooterRules());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getHeaderAlign());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getHeaderFontID());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getHeaderIndent());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getHeaderSize());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getMarginBottom());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getMarginLeft());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getMarginRight());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getMarginTop());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getPageHeight());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getPageWidth());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getTitleAlign());
            System.out.println("BiosketchStyle -> " + biosketchStyle.isTitleEditable());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getTitleFontID());
            System.out.println("BiosketchStyle -> " + biosketchStyle.isTitleRules());
            System.out.println("BiosketchStyle -> " + biosketchStyle.getTitleSize());

            Iterable<Ruleset> rulesetList = biosketch.getRuleset();
            if (rulesetList != null)
            {
                for (Ruleset ruleset : rulesetList)
                {
                    System.out.println("Ruleset - ID  " + ruleset.getId());
                    System.out.println("Criteria " + ruleset.toString());
                }
            }

            Iterable<BiosketchSection> biosketchSectionsList = biosketch.getBiosketchSections();
            for (BiosketchSection biosketchSection : biosketchSectionsList)
            {
                System.out.println("BiosketchSection - ID  " + biosketchSection.getId());
                System.out.println("BiosketchSection - BiosketchID  " + biosketchSection.getBiosketchID());
                System.out.println("BiosketchSection - SectionID  " + biosketchSection.getSectionID());
                System.out.println("BiosketchSection - Availability " + biosketchSection.getAvailability());
                System.out.println("BiosketchSection - Display " + biosketchSection.isDisplay());
            }

            Iterable<BiosketchAttributes> attributeList = biosketch.getBiosketchAttributes();
            if (attributeList != null)
            {
                for (BiosketchAttributes attributes : attributeList)
                {
                    System.out.println("BIOSKETCHATTRIBUTES - ID is " + attributes.getId());
                    System.out.println("BIOSKETCHATTRIBUTES - BiosketchID is " + attributes.getBiosketchID());
                    System.out.println("BIOSKETCHATTRIBUTES - name is " + attributes.getName());
                    System.out.println("BIOSKETCHATTRIBUTES - value is " + attributes.getValue());
                    System.out.println("BIOSKETCHATTRIBUTES - sequence is " + attributes.getSequence());
                    System.out.println("BIOSKETCHATTRIBUTES - display is " + attributes.isDisplay());
                }
            }
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    /* public void testLoadBiosketchDesignData()
     {
     try
     {
     Biosketch biosketch = biosketchService.loadBiosketchDesignData(1);
     if (biosketch != null)
     {
     Iterable<BiosketchExclude> it = biosketch.getBiosketchData().getBiosketchExcludeList();
     Iterator it1 = it.iterator();
     while (it1.hasNext())
     {
     BiosketchExclude biosketchExclude = (BiosketchExclude) it1.next();
     System.out.println("BiosketchExclude - ID  " + biosketchExclude.getId());
     System.out.println("BiosketchExclude - USERID  " + biosketchExclude.getUserID());
     System.out.println("BiosketchExclude - BIOSKETCHID  " + biosketchExclude.getBiosketchID());
     System.out.println("BiosketchExclude - RECORDTYPE  " + biosketchExclude.getRecType());
     System.out.println("BiosketchExclude - RECORDID  " + biosketchExclude.getRecordID());
     }

     }
     }
     catch (Throwable t)
     {
     t.printStackTrace();
     fail(t.getMessage());
     }
     }  */

    public void testSaveBiosketch()
    {
        try
        {
            Biosketch biosketch = new Biosketch(18807, 3);
            biosketch.setName("Shashi");
            biosketch.setTitle("Shashi's biosketch");

            BiosketchStyle biosketchStyle = new BiosketchStyle(0, 18807);
            biosketchStyle.setStyleName("Shashis Style");
            biosketchStyle.setBodyFontID(1);
            biosketchStyle.setBodyFormatting(true);
            biosketchStyle.setBodyIndent(1);
            biosketchStyle.setBodySize(1);
            biosketchStyle.setFooterOn(true);
            biosketchStyle.setFooterPageNumbers(true);
            biosketchStyle.setFooterRules(true);
            biosketchStyle.setHeaderAlign(Alignment.LEFT);
            biosketchStyle.setHeaderFontID(1);
            biosketchStyle.setHeaderIndent(1);
            biosketchStyle.setHeaderSize(1);
            biosketchStyle.setMarginBottom(1);
            biosketchStyle.setMarginLeft(1);
            biosketchStyle.setMarginRight(1);
            biosketchStyle.setMarginTop(1);
            biosketchStyle.setPageHeight(1);
            biosketchStyle.setPageWidth(1);
            biosketchStyle.setTitleAlign(Alignment.LEFT);
            biosketchStyle.setTitleEditable(true);
            biosketchStyle.setTitleFontID(1);
            biosketchStyle.setTitleRules(true);
            biosketchStyle.setTitleSize(1);
            // setting the biosketchstyle on biosketch
            biosketch.setBiosketchStyle(biosketchStyle);

            List<Ruleset> rulesetList = new ArrayList<Ruleset>();
            for (int i = 0; i < 2; i++)
            {
                Ruleset ruleset = new Ruleset(0, 18807);
                ruleset.setRecType("Shashi rec type");
                ruleset.setOperator(Operator.valueOf("AND"));

                Criterion criteria = new Criterion(0);
                criteria.setField("this is field");
                criteria.setOperator(Criterion.RelOp.LT);
                criteria.setValue("this is my value");

                ruleset.addCriterion(criteria);
                rulesetList.add(ruleset);
            }
            // setting the list of ruleset on biosketch
            biosketch.setRuleset(rulesetList);

            // saving the biosketch
            biosketchService.saveBiosketch(biosketch);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testUpdateBiosketch()
    {
        try
        {
            Biosketch biosketch = biosketchService.getBiosketch(148);
            biosketchService.saveBiosketch(biosketch);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testDeleteBiosketch()
    {
        try
        {
            Biosketch biosketch = biosketchService.getBiosketch(151);
            biosketchService.deleteBiosketch(biosketch);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testDupliacteBiosketch()
    {
        try
        {
            biosketchService.duplicateBiosketch(152);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }
}