'''
This script retrieves individual scripts defined in the grinder.properties files and
executes each in sequence. Test numbers for each of the individual scripts are assigned
dynamically.

Created on June 2010
@author: rhendric
'''

# Scripts are defined in Python modules (x.py, y.py)
# specified in grinder.properties:
#
#   script1=x.py
#   script2=y.py
#   .
#   .
#   .
#   scriptn=n.py

import string
import mivCommon
from net.grinder.script.Grinder import grinder

from java.util import TreeMap

# TreeMap is the simplest way to sort a Java map.
scripts = TreeMap(grinder.getProperties().getPropertySubset("script"))
mivCommon.setControlVariables()
mivCommon.setServerUrl()

# Dictionary of script names and test numbers
scriptNames = {}
# Ensure each script module is initialized in the process thread.

for key in scripts.keySet():
    scriptNames[scripts.get(key)] = key
    exec("import %s" % scripts.get(key))

def createTestRunner(module):
    x = ''
    exec("x = %s.TestRunner()" % module)
    return x

class TestRunner:
    
    # Create the TestRunner instances for each script
    def __init__(self):
        self.testRunners = []
        for script in scripts.values():
            self.testRunners.append(createTestRunner(script))

    # Run each script in sequence.
    def __call__(self):
        # initialize
        self.nextTestNumber = 0
        self.testIncrement = 100
        [self.username, self.fullname, self.principalId, self.password, ] = mivCommon.getRandomLoginUserAndPassword().split(':')

        # Loop through each defined script testRunner
        for testRunner in self.testRunners:
            # Set the test number and increment for this test in the testRunner
            mivCommon.setTestIncrement(testRunner,self.testIncrement)
            mivCommon.setNextTestNumber(testRunner,self.nextTestNumber)
            mivCommon.setUserNameAndPassword(testRunner, self.username, self.fullname, self.principalId, self.password )
            print "****** %s script (thread %d run %d) ******" % (testRunner.getScriptName(), grinder.getThreadNumber(),grinder.getRunNumber())

            testRunner()
            # get the next test number from the previous script
            self.nextTestNumber = mivCommon.getNextTestNumber(testRunner)
            
            # End the test if the login fails
            if testRunner.getScriptName() == 'mivLogin' and not mivCommon.getTestSuccess(testRunner):
                print testRunner.getScriptName()+' Login Failed...Ending Test %d' % grinder.getRunNumber()
                break
