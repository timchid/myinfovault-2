'''
This script exercises the MIV Manage User functionality.
Users are searched randomly by Name, LastName, Department, or School.
A user is selected from the resulting list of users and edited by changing roles.
Roles are only changed for users with DEPT_STAFF and SCHOOL_STAFF roles.
The roles are simply toggled between those two roles. This forces document
reresolution of Kuali documents. 

Note that role updates may be throttled to only allow a specified number of 
role changes to be made during a specific time interval. To configure a maximum
number of role updates per time interval, set the controlMinRoleChangeInterval 
value in the grinder.properties file.   


Created on June 2010
@author: rhendric
'''

import mivCommon
from net.grinder.script import Test
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl, HTTPRequest
from HTTPClient import NVPair
connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

from java.util.regex import Matcher
from java.util.regex import Pattern
import time


# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started.

connectionDefaults.setUseCookies(1)
connectionDefaults.useContentEncoding = 1
connectionDefaults.setFollowRedirects(0)

# defaults
nextTestNumber = 0
testIncrement = 100
testSuccess = 1

scriptName = 'manageUser'    
serverURL = mivCommon.getServerUrl()

connectionDefaults.defaultHeaders = \
  ( NVPair('Accept-Language', 'en-us,en;q=0.5'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.7'),
    NVPair('Accept-Encoding', 'gzip,deflate'),
    NVPair('User-Agent', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.2) Gecko/2008092418 CentOS/3.0.2-3.el5.centos Firefox/3.0.2'),
    NVPair('Cache-Control', 'no-cache'), )


headers0= \
  ( NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Referer', 'https://'+mivCommon.getServerUrl()+'/miv/MIVMain'), )

url0 = 'https://'+mivCommon.getServerUrl()+':443'

class TestRunner:
  """A TestRunner instance is created for each worker thread."""

  def getScriptName(self):
      return scriptName
  
  # A method for each recorded page.
  def manageUser_page(self):
    
    result = self.request101.GET('/miv/ManageUsers?_flowId=manageUsers-flow')

    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
        mivCommon.setTestSuccess(self,0)
    
    # Expecting 302 'Moved Temporarily'
    if result.getStatusCode() == 302:
       self.token__flowExecutionKey = \
       httpUtilities.valueFromLocationURI('_flowExecutionKey') 
       redirectURL = mivCommon.getRedirectURL(result)
       mivCommon.logMessage("Redirecting to: %s" % redirectURL)
       result1 = self.request102.GET(redirectURL)
       if result1.getStatusCode() >= 400 and result1.getStatusCode() <= 599:
           grinder.getStatistics().getForLastTest().setSuccess(0)

    mivCommon.writeToFile(self.fileHandle,"\nManageUser_page\n"+result1.toString()+result1.getText())

  def manageUserSelectOption_page(self):
    
    # Expecting 302 'Moved Temporarily'
    result = self.request103.GET('/miv/ManageUsers' +
      '?_flowId=manageUsers-flow' +
      '&_flowExecutionKey=' +
      self.token__flowExecutionKey +
      '&_eventId=edit')
    self.token__flowExecutionKey = \
      httpUtilities.valueFromLocationURI('_flowExecutionKey')
      
    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
        mivCommon.setTestSuccess(self,0)

    if result.getStatusCode() == 302:
       self.token__flowExecutionKey = \
         httpUtilities.valueFromLocationURI('_flowExecutionKey') 
       redirectURL = mivCommon.getRedirectURL(result)
       mivCommon.logMessage("Redirecting to: %s" % redirectURL)
       
       result1 = self.request104.GET(redirectURL)
       if result1.getStatusCode() >= 400 and result1.getStatusCode() <= 599:
           grinder.getStatistics().getForLastTest().setSuccess(0)
       
       # set the departments/schools allowed to select from for search
       mivCommon.setSearchCriteria(self, result1.getText())  

    mivCommon.writeToFile(self.fileHandle,"\nManageUserSelectOption_page\n"+result1.toString()+result1.getText())

  def manageUserSearch_page(self):
        
      logMessage = "ManageUser Searching by "+self.searchBy+": "+self.searchValue
      mivCommon.logAndPrint(logMessage)
    
      # Expecting 302 'Moved Temporarily'
      request = '/miv/ManageUsers' + \
        '?inputName=' + \
        self.token_inputName + \
        '&_flowExecutionKey=' + \
        self.token__flowExecutionKey + \
        '&lname=' + \
        self.token_lname + \
        '&'+self.token_eventId+'=Search+Now!' + \
        '&department=' + \
        self.token_department
              
      if self.token_school != '':
        request = request+'&school=' + self.token_school
      
      result = self.request105.GET(request)
      
      self.token__flowExecutionKey = \
        httpUtilities.valueFromLocationURI('_flowExecutionKey') # '_cFFF44136-6
  
      if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0)

      if result.getStatusCode() == 302:
         self.token__flowExecutionKey = \
           httpUtilities.valueFromLocationURI('_flowExecutionKey')
         redirectURL = mivCommon.getRedirectURL(result)
         result1 = self.request106.GET(redirectURL)
         mivCommon.writeToFile(self.fileHandle,"\nManageUserSearchResults_page\n"+result1.toString()+result1.getText())

      return result1

  def editUserDetails_page(self):
    if len(self.userSelected) > 0:

       pattern = Pattern.compile(">.*?</a>")
       result = pattern.matcher(self.userSelected)
       self.userName = ''
       if result.find():
           self.userName = result.group().lstrip(">").rstrip("</a>")
        
       mivCommon.parseTokens(self, self.userSelected)    
   
       logMessage = 'Selected User '+self.userName
       mivCommon.logAndPrint(logMessage)
       
       result = self.request107.GET('/miv/ManageUsers' +
         '?_flowExecutionKey=' +
         self.token__flowExecutionKey +
         '&_eventId=' +
         self.token__eventId +
         '&id=' +
         self.token_id)
       self.token__flowExecutionKey = \
          httpUtilities.valueFromLocationURI('_flowExecutionKey')
          
       if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0)

       if result.getStatusCode() == 302:
         redirectURL = mivCommon.getRedirectURL(result)
         result1 = self.request108.GET(redirectURL)
         mivCommon.writeToFile(self.fileHandle,"\nEditUserDetails_page\n"+result1.toString()+result1.getText())

         # parse current values from the result
         self.parseUserOptionValues(result1.getText())
         if self.roleOption == '' or self.emailOption == '' or self.departmentOptionArray.count==0:
            logMessage = "Unable to edit user details for "+self.userName+" (Dossier in process)"
            mivCommon.logAndPrint(logMessage)
         else:
            logMessage = "Current Role: "+self.roleOption
            mivCommon.logAndPrint(logMessage)
            logMessage = "Current Email Address: "+self.emailAddress+" (option "+self.emailOption+")"
            mivCommon.logAndPrint(logMessage)
            logMessage = "Current Departments: "+self.departmentOptionArray[0]+", "+self.departmentOptionArray[1]+", "+self.departmentOptionArray[2]+", "+self.departmentOptionArray[3]+", "+self.departmentOptionArray[4]
            mivCommon.logAndPrint(logMessage)
            
            # We are only going to update roles for DEPT_STAFF and SCHOOL_STAFF
            if self.roleOption == "CANDIDATE" \
                or self.roleOption == "VICE_PROVOST_STAFF" \
                or self.roleOption == "DEPT_ASSISTANT" \
                or self.roleOption == "SYS_ADMIN":
                logMessage = "Role update skipped for "+self.roleOption+" role."
            else:
                if self.roleOption == "SCHOOL_STAFF":
                    self.roleOption = "DEPT_STAFF"
                else:
                    self.roleOption = "SCHOOL_STAFF"
                
                if mivCommon.validateRoleChangeLimit():
                    self.saveUserDetails_page()    
                    logMessage = "User Role updated to "+self.roleOption+"."
           
            mivCommon.logAndPrint(logMessage)
         
       return result

  def saveUserDetails_page(self):
    # Expecting 302 'Moved Temporarily'
    result = self.request109.POST('/miv/ManageUsers' +
      '?_flowExecutionKey=' +
      self.token__flowExecutionKey,
      ( NVPair('mivRole', self.roleOption),
        NVPair('emailAddress', self.emailOption),
        NVPair('departments[0]', self.departmentOptionArray[0]),
        NVPair('departments[1]', self.departmentOptionArray[1]),
        NVPair('departments[2]', self.departmentOptionArray[2]),
        NVPair('departments[3]', self.departmentOptionArray[3]),
        NVPair('departments[4]', self.departmentOptionArray[4]),
        NVPair('_eventId_save', 'Save'),
        NVPair('id', ''), ),
      ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ))
    self.token__flowExecutionKey = \
      httpUtilities.valueFromLocationURI('_flowExecutionKey')

    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
        mivCommon.setTestSuccess(self,0)

    if result.getStatusCode() == 302:
        redirectURL = mivCommon.getRedirectURL(result)
        result1 = self.request110.GET(redirectURL)
        mivCommon.writeToFile(self.fileHandle,"\nSaveUserDetails_page\n"+result1.toString()+result1.getText())

    return result

  def parseUserOptionValues(self, text):
     # get the current role.    
     pattern = Pattern.compile("<select id=\"mivRole\".*?>.*?</select>")
     text = ' '.join(text.split())    # remove all white space
     roleOptions = pattern.matcher(text)
     self.roleOption = ''
     self.emailOption = ''
     self.emailAddress = ''
     self.departmentOptionArray = ['','','','','']
     if roleOptions.find():
         pattern = Pattern.compile("<option selected=.*?value=\"(.*?)\">.*?</option>")
         option = pattern.matcher(roleOptions.group())
         if option.find():
            self.roleOption = option.group(1)

     # get the email address selected.    
     pattern = Pattern.compile("<input type=\"radio\" id=\"emailAddress\" name=\"emailAddress\" value=\"(.*?)\" checked=\"checked\">.*?\"emailAddress\">(.*?)</label>")
     emailOptions = pattern.matcher(text)
     if emailOptions.find():
         self.emailOption = emailOptions.group(1)
         self.emailAddress = emailOptions.group(2)
            
     # get current department options
     for i in range(1,6):
         pattern = Pattern.compile("<select id=\"department"+str(i)+"\".*?>.*?</select>")
         departmentOptions = pattern.matcher(text)
         if departmentOptions.find():
             pattern = Pattern.compile("<option value=\"([0-9]+:[0-9]+)\" selected=\"selected\">.*?</option>")
             option = pattern.matcher(departmentOptions.group())
             if option.find():
                self.departmentOptionArray[i-1] = option.group(1)
                    
            
  
  def __call__(self):
    """This method is called for every run performed by the worker thread."""

    # open a test file
    self.fileHandle = mivCommon.openTestFile(self.getScriptName())

    # defaults
    if self.nextTestNumber is None:
        self.nextTestNumber = nextTestNumber
    
    if self.testIncrement is None:
        self.testIncrement = testIncrement

    self. testSuccess = 1
    
    # If the response back from the server is not one that we except,
    # we want to mark the test as unsuccessful and not include the statistics
    # in the test times. To do this, the delayReports variable can be set to 1.
    # Doing so will delay the reporting back of the statistics until after
    # the test has completed and we have had chance to check its operation.
    # The default is to report back when the test returns control back to
    # the script, i.e. immediately after a test has executed.
    grinder.getStatistics().setDelayReports(1)
 
# Create an HTTPRequest for each request, then replace the
    # reference to the HTTPRequest with an instrumented version.
    # You can access the unadorned instance using request101.__target__.
    self.nextTestNumber = self.nextTestNumber+self.testIncrement   
    self.request101 = HTTPRequest(url=url0, headers=headers0)
    self.request101 = Test(self.nextTestNumber+1, 'GET ManageUser').wrap(self.request101)
    self.request102 = HTTPRequest(url=url0, headers=headers0)
    self.request102 = Test(self.nextTestNumber+2, 'ManageUser').wrap(self.request102)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'manageUser_page'), 'manageUser_page')

    self.nextTestNumber += self.testIncrement
    self.request103 = HTTPRequest(url=url0, headers=headers0)
    self.request103 = Test(self.nextTestNumber+1, 'GET MananageUserSelectOption').wrap(self.request103)
    self.request104 = HTTPRequest(url=url0, headers=headers0)
    self.request104 = Test(self.nextTestNumber+2, 'ManageUserSelectOption').wrap(self.request104)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'manageUserSelectOption_page'), 'manageUserSelectOption_page')

    self.nextTestNumber += self.testIncrement
    self.request105 = HTTPRequest(url=url0, headers=headers0)
    self.request105 = Test(self.nextTestNumber+1, 'GET MananageUserSearch').wrap(self.request105)
    self.request106 = HTTPRequest(url=url0, headers=headers0)
    self.request106 = Test(self.nextTestNumber+2, 'ManageUserSearch').wrap(self.request106)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'manageUserSearch_page'), 'manageUserSearch_page')


    self.nextTestNumber += self.testIncrement
    self.request107 = HTTPRequest(url=url0, headers=headers0)
    self.request107 = Test(self.nextTestNumber+1, 'GET EditUserDetails').wrap(self.request107)
    self.request108 = HTTPRequest(url=url0, headers=headers0)
    self.request108 = Test(self.nextTestNumber+2, 'EditUserDetails').wrap(self.request108)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'editUserDetails_page'), 'editUserDetails_page')


    self.nextTestNumber += self.testIncrement
    self.request109 = HTTPRequest(url=url0, headers=headers0)
    self.request109 = Test(self.nextTestNumber+1, 'POST SaveUserDetails').wrap(self.request109)
    self.request110 = HTTPRequest(url=url0, headers=headers0)
    self.request110 = Test(self.nextTestNumber+2, 'SaveUserDetails').wrap(self.request110)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'saveUserDetails_page'), 'saveUserDetails_page')

    mivCommon.sleep()  
    self.manageUser_page()

    mivCommon.sleep()  
    self.manageUserSelectOption_page() 
    result = self.manageUserSearch_page() 

    searchResult = mivCommon.getSearchResult(self,result.getText())
    mivCommon.logAndPrint(searchResult)

    # If a User is selected, Edit the details
    if len(self.userSelected) > 0:
       self.editUserDetails_page()    

    scriptResult = "Success"
    if not mivCommon.getTestSuccess(self) :
       scriptResult = "Failed"
    mivCommon.printMessage(self.getScriptName()+' Script Complete: '+ scriptResult)

    mivCommon.closeTestFile(self.fileHandle)    

def instrumentMethod(test, method_name, c=TestRunner):
  """Instrument a method with the given Test."""
  unadorned = getattr(c, method_name)
  import new
  method = new.instancemethod(test.wrap(unadorned), None, c)
  setattr(c, method_name, method)

