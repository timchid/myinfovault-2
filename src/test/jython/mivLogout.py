'''
This script logs out the currently logged in MIV User.

Created on June 2010
@author: rhendric
'''
import mivCommon
from net.grinder.script import Test
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl, HTTPRequest
from HTTPClient import NVPair

connectionDefaults = HTTPPluginControl.getConnectionDefaults()

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started.

connectionDefaults.setUseCookies(1)
connectionDefaults.useContentEncoding = 1
connectionDefaults.setFollowRedirects(0)

httpUtilities = HTTPPluginControl.getHTTPUtilities()

from java.util.regex import Matcher
from java.util.regex import Pattern

serverURL = mivCommon.getServerUrl()

# defaults
nextTestNumber = 0
testIncrement = 100
testSuccess = 1

scriptName = 'mivLogout'


connectionDefaults.defaultHeaders = \
  ( NVPair('Accept-Language', 'en-us,en;q=0.5'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.7'),
    NVPair('Accept-Encoding', 'gzip,deflate'),
    NVPair('User-Agent', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.2) Gecko/2008092418 CentOS/3.0.2-3.el5.centos Firefox/3.0.2'), )

headers0= \
  ( NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Referer', 'http://'+serverURL+'/'), )

headers1= \
  ( NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Referer', 'https://cas-test.ucdavis.edu/impersonate/login?service=https%3a%2f%2f'+serverURL+'%2fmiv%2fMIVLogin'), )


url0 = 'https://'+serverURL+':443'

# Create an HTTPRequest for each request, then replace the
# reference to the HTTPRequest with an instrumented version.
# You can access the unadorned instance using request101.__target__.

class TestRunner:
  """A TestRunner instance is created for each worker thread."""

  def getScriptName(self):
      return scriptName

  def MivLogout(self):
    """GET logout (request 101)."""
    
    # Expecting 302 'Moved Temporarily'
    result = self.request101.GET('/miv/logout')
    
    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
       mivCommon.setTestSuccess(self,0)
       
    mivCommon.writeToFile(self.fileHandle,"\nMIVLogout\n"+result.toString()+result.getText())
    
    if mivCommon.getTestSuccess(self):
       message = "Logged out %s (%s / %s)" % (self.fullname, self.username, self.principalId)    
       self.token_jsessionid = mivCommon.getSessionId()
       mivCommon.logMessage("JSESSIONID: %s" % self.token_jsessionid)
    else:    
       message = "Log out failed for %s (%s / %s)" % (self.fullname, self.username, self.principalId)    

    mivCommon.logAndPrint(message)

    return result

  def MivLogout_page(self):
    """GET logout.html (request 201)."""
    result = self.request201.GET('/logout.html')

    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
       mivCommon.setTestSuccess(self,0);

    mivCommon.writeToFile(self.fileHandle,"\nMIVLogout_page\n"+result.toString()+result.getText())
    return result

  def __call__(self):
    """This method is called for every run performed by the worker thread."""
    
    # open a test file
    self.fileHandle = mivCommon.openTestFile(self.getScriptName())

    # defaults
    if self.nextTestNumber is None:
        self.setNextTestNumber(nextTestNumber)
    
    if self.testIncrement is None:
        self.setNextTestNumber(testIncrement)
    self. testSuccess = 1
    
    # If the response back from the server is not one that we except,
    # we want to mark the test as unsuccessful and not include the statistics
    # in the test times. To do this, the delayReports variable can be set to 1.
    # Doing so will delay the reporting back of the statistics until after
    # the test has completed and we have had chance to check its operation.
    # The default is to report back when the test returns control back to
    # the script, i.e. immediately after a test has executed.
    grinder.getStatistics().setDelayReports(1)
    
    # Create an HTTPRequest for each request, then replace the
    # reference to the HTTPRequest with an instrumented version.
    # You can access the unadorned instance using request101.__target__.
    self.nextTestNumber = self.nextTestNumber+self.testIncrement   
    self.request101 = HTTPRequest(url=url0, headers=headers0)
    self.request101 = Test(self.nextTestNumber+1, 'logout').wrap(self.request101)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'MivLogout'), 'MivLogout')

    self.nextTestNumber += self.testIncrement
    self.request201 = HTTPRequest(url=url0, headers=headers1)
    self.request201 = Test(self.nextTestNumber+1, 'logout.html').wrap(self.request201)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'MivLogout_page'), 'MivLogout_page')

    mivCommon.sleep()
    self.MivLogout()      # GET MIVLogout (request 101)

    mivCommon.sleep()
    self.MivLogout_page()      # GET MIVLogout page (request 201)
    
    scriptResult = "Success"
    if not  mivCommon.getTestSuccess(self):
       scriptResult = "Failed"
    mivCommon.printMessage(self.getScriptName()+' Script Complete: '+ scriptResult)

    mivCommon.closeTestFile(self.fileHandle) 

def instrumentMethod(test, method_name, c=TestRunner):
  """Instrument a method with the given Test."""
  unadorned = getattr(c, method_name)
  import new
  method = new.instancemethod(test.wrap(unadorned), None, c)
  setattr(c, method_name, method)

