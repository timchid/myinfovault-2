
import edu.ucdavis.mw.myinfovault.web.MIVServlet;


public class WorkflowTest extends MIVServlet
{
    private static final long serialVersionUID = 20090320171910L;
//    private static Logger log = Logger.getLogger("MIV");
//
//	enum Action {
//		INITIATE ("initiate"),
//		APPROVE ("approve"),
//                CANCEL ("cancel"),
//		ROUTE ("route"),
//		RETURN ("return"),
//		FIND ("find"),
//		FINDACTIONLISTBYNAME ("findactionlistbyname"),
//		FINDALLSCOPEDBYNAME ("findallscopedbyname"),
//		FINDBYSCHOOLDEPTNAME ("findbyschooldeptname"),
//		FINDSINGLEBYNAME ("findsinglebyname"),
//		FINDBYLOCATION ("findbylocation"),
//		ROUTETOLOCATION ("routetolocation"),
//                VALIDATEUSERROLES ("validateuserroles"),
//                SYNCHUSERROLES ("synchronizeuserroles"),
//                UPDATEUSERACCOUNT ("updateuseraccount");
//
//	    public final String description;
//
//	    Action(String description)
//	    {
//		 this.description = description;
//	    }
//	}
//
//	private static final IdentityService identityService = KIMServiceLocator.getIdentityService();
//
//
//    @Override
//    public void init(ServletConfig config)
//        throws ServletException
//    {
//        super.init(config);
//    }
//
//    @Override
//    public void doGet(HttpServletRequest request, HttpServletResponse response)
//    {
//        ServletOutputStream out = null;
//        try
//        {
//            out = response.getOutputStream();
//            response.setContentType("text/html");
//
//            prologue(out);
//
//            out.println("<h2>Workflow Test</h2>");
//
//            out.println("<h3>Validate User Roles</h3>");
//            out.println("<div id='queryform12'>");
//            out.println(" <form name='validateroles' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name: </label>");
//            out.println("  <input type='text' name='validateUserRoleName' id='validateUserRoleName'><br>");
//            out.println("  <input type='submit' name='validate' value='validate'>");
//            out.println(" </form>\r\n</div><!--queryform12-->");
//
//            out.println("<h3>Synchronize User Roles</h3>");
//            out.println("<div id='queryform13'>");
//            out.println(" <form name='synchronizeroles' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name: </label>");
//            out.println("  <input type='text' name='synchronizeUserRoleName' id='synchronizeUserRoleName'><br>");
//            out.println("  <input type='submit' name='synchronize' value='synchronize roles'>");
//            out.println(" </form>\r\n</div><!--queryform13-->");
//
//            out.println("<h3>Synchronize Dossier Locations with Workflow</h3>");
//            out.println("<div id='queryform0'>");
//            out.println(" <form name='synchronize' method='GET' action='WorkflowTest'>");
//            out.println("  <input type='submit' name='synchronize' value='synchronize'>");
//            out.println(" </form>\r\n</div><!--queryform0-->");
//
//            out.println("<h3>Initiate a Dossier</h3>");
//            out.println("<div id='queryform1'>");
//            out.println(" <form name='initiate' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name of owner: </label>");
//            out.println("  <input type='text' name='ownerName' id='ownerName'><br>");
//            out.println("  <label for='packetId'>Dossier Description: </label>");
//            out.println("  <input type='text' name='description' id='description' MAXLENGTH=50><br>");
//            out.println("  <label for='annotation'>Annotation: </label>");
//            out.println("  <input type='text' name='annotation' id='annotation' MAXLENGTH=20 ><br>");
//            out.println("  <label for='actionType'>Action Type: </label></br>");
//            out.println("  <input type='radio' name='actionType' value='Merit' checked>Merit");
//            out.println("  <input type='radio' name='actionType' value='Promotion'>Promotion<br>");
//            out.println("  <label for='delegationAuthority'>Delegation Authority: </label></br>");
//            out.println("  <input type='radio' name='delegationAuthority' value='Redelegated' checked>Relegated");
//            out.println("  <input type='radio' name='delegationAuthority' value='Redelegated_Federation' checked>Relegated Federation");
//            out.println("  <input type='radio' name='delegationAuthority' value='Non_Redelegated' checked>Non-Relegated");
//            out.println("  <input type='submit' value='initiate'>");
//            out.println(" </form>\r\n</div><!--queryform1-->");
//
//            out.println("<h3>Approve a Dossier</h3>");
//            out.println("<div id='queryform2'>");
//            out.println(" <form name='approve' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name of approver: </label>");
//            out.println("  <input type='text' name='approverName' id='approverName'><br>");
//            out.println("  <label for='approve'>Dossier ID: </label>");
//            out.println("  <input type='text' name='approveDossierId' id='approveDossierId'><br>");
//            out.println("  <label for='annotation'>Annotation: </label>");
//            out.println("  <input type='text' name='annotation' id='annotation' MAXLENGTH=20 ><br>");
//            out.println("  <input type='submit' value='approve'>");
//            out.println(" </form>\r\n</div><!--queryform2-->");
//
//            out.println("<h3>Route a Dossier</h3>");
//            out.println("<div id='queryform3'>");
//            out.println(" <form name='route' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name of router: </label>");
//            out.println("  <input type='text' name='routeName' id='routeName'><br>");
//            out.println("  <label for='route'>Dossier ID: </label>");
//            out.println("  <input type='text' name='routeDossierId' id='routeDossierId'><br>");
//            out.println("  <label for='annotation'>Annotation: </label>");
//            out.println("  <input type='text' name='annotation' id='annotation' MAXLENGTH=20 ><br>");
//            out.println("  <input type='submit' value='route'>");
//            out.println(" </form>\r\n</div><!--queryform3-->");
//
//            out.println("<h3>Find a Dossier</h3>");
//            out.println("<div id='queryform4'>");
//            out.println(" <form name='find' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='find'>Dossier ID: </label>");
//            out.println("  <input type='text' name='findDossierId' id='findDossierId'><br>");
//            out.println("  <input type='submit' value='find'>");
//            out.println(" </form>\r\n</div><!--queryform4-->");
//
//            out.println("<h3>Find Action List Dossiers By principal Id</h3>");
//            out.println("<div id='queryform5'>");
//            out.println(" <form name='find' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name : </label>");
//            out.println("  <input type='text' name='findActionListByName' id='findActionListByName'><br>");
//            out.println("  <input type='submit' value='find'>");
//            out.println(" </form>\r\n</div><!--queryform5-->");
//
//            out.println("<h3>Find a Dossier By principal Id</h3>");
//            out.println("<div id='queryform6'>");
//            out.println(" <form name='find' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name : </label>");
//            out.println("  <input type='text' name='findSingleByName' id='findSingleByName'><br>");
//            out.println("  <input type='submit' value='find'>");
//            out.println(" </form>\r\n</div><!--queryform6-->");
//
//            out.println("<h3>Return dossier to previous node</h3>");
//            out.println("<div id='queryform7'>");
//            out.println(" <form name='find' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Returning user login name: </label>");
//            out.println("  <input type='text' name='returnName' id='returnName'><br>");
//            out.println("  <label for='route'>Dossier ID: </label>");
//            out.println("  <input type='text' name='returnDossierId' id='returnDossierId'><br>");
//            out.println("  <label for='annotation'>Annotation: </label>");
//            out.println("  <input type='text' name='annotation' id='annotation' MAXLENGTH=20 ><br>");
//            out.println("  <input type='submit' value='return'>");
//            out.println(" </form>\r\n</div><!--queryform7-->");
//
//            out.println("<h3>Find dossier by Workflow Location</h3>");
//            out.println("<div id='queryform8'>");
//            out.println(" <form name='find' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='location'>Workflow Location: </label>");
//            out.println("  <select name='findLocation'>");
//            for (DossierLocation location : DossierLocation.values())
//            {
//                out.println("  <option value='"+location+"'>"+location+"</option>");
//            }
//            out.println("  </select><br>");
//            out.println("  <label for='loginName'>Login name : </label>");
//            out.println("  <input type='text' name='findByLocationName' id='findByLocationName'><br>");
//            out.println("  <input type='submit' value='find'>");
//            out.println(" </form>\r\n</div><!--queryform8-->");
//
//            out.println("<h3>Find dossier by School and Department</h3>");
//            out.println("<div id='queryform9'>");
//            out.println(" <form name='find' method='GET' action='WorkflowTest'>");
//
//            //    	    	Map<String, Map<String, String>> schoolMap =  MIVConfig.getConfig().getMap("schools");
//            //
//            //            	out.println("  <label for='school'>School: </label>");
//            //            	out.println("  <select name='school'>");
//            //            	for (String school : schoolMap.keySet())
//            //    	    	{
//            //                	out.println("  <option value='"+school+"'>"+schoolMap.get(school).get("description"));
//            //    	    	}
//            //            	out.println("  </select><br>");
//            Map<String, Map<String, String>> deptMap =  MIVConfig.getConfig().getMap("departments");
//
//            out.println("  <label for='department'>School - Department: </label>");
//            out.println("  <select name='department'>");
//            out.println("  <option value='ALL'>ALL</option>");
//            for (String dept : deptMap.keySet())
//            {
//                out.println("  <option value='"+dept+"'>"+deptMap.get(dept).get("school")+" - "+deptMap.get(dept).get("description"));
//            }
//            out.println("  </select><br>");
//            out.println("  <label for='location'>Workflow Location: </label>");
//            out.println("  <select name='findLocation'>");
//            out.println("  <option value='ALL'>ALL</option>");
//            for (DossierLocation location : DossierLocation.values())
//            {
//                out.println("  <option value='"+location+"'>"+location+"</option>");
//            }
//            out.println("  </select><br>");
//            out.println("  <label for='loginName'>Login name : </label>");
//            out.println("  <input type='text' name='findBySchoolDeptName' id='findBySchoolDeptName'><br>");
//            out.println("  <input type='submit' value='find'>");
//            out.println(" </form>\r\n</div><!--queryform9-->");
//
//            out.println("<h3>Find Dossiers scoped by principal Id</h3>");
//            out.println("<div id='queryform10'>");
//            out.println(" <form name='find' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name : </label>");
//            out.println("  <input type='text' name='findAllScopedByName' id='findAllScopedByName'><br>");
//            out.println("  <input type='submit' value='find'>");
//            out.println(" </form>\r\n</div><!--queryform10-->");
//
//
//            out.println("<h3>Cancel a Dossier</h3>");
//            out.println("<div id='queryform11'>");
//            out.println(" <form name='cancel' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name of cancelling user: </label>");
//            out.println("  <input type='text' name='cancellerName' id='cancellerName'><br>");
//            out.println("  <label for='approve'>Dossier ID: </label>");
//            out.println("  <input type='text' name='cancelDossierId' id='cancelDossierId'><br>");
//            out.println("  <label for='annotation'>Annotation: </label>");
//            out.println("  <input type='text' name='annotation' id='annotation' MAXLENGTH=20 ><br>");
//            out.println("  <input type='submit' value='cancel'>");
//            out.println(" </form>\r\n</div><!--queryform11-->");
//
//
//            out.println("<h3>Route Dossier to specific location</h3>");
//            out.println("<div id='queryform12'>");
//            out.println(" <form name='route' method='GET' action='WorkflowTest'>");
//            out.println("  <label for='loginName'>Login name of router: </label>");
//            out.println("  <input type='text' name='routeName' id='routeName'><br>");
//            out.println("  <label for='route'>Dossier ID: </label>");
//            out.println("  <input type='text' name='routeDossierId' id='routeDossierId'><br>");
//            out.println("  <label for='annotation'>Annotation: </label>");
//            out.println("  <input type='text' name='annotation' id='annotation' MAXLENGTH=20 ><br>");
//            out.println("  <label for='location'>Workflow Location: </label>");
//            out.println("  <select name='routeLocation'>");
//            for (DossierLocation location : DossierLocation.values())
//            {
//                out.println("  <option value='"+location+"'>"+location+"</option>");
//            }
//            out.println("  </select><br>");
//            out.println("  <input type='submit' value='route'>");
//            out.println(" </form>\r\n</div><!--queryform11-->");
//
//
//            out.println("<h3>Update User Accounts</h3>");
//            out.println("<div id='queryform14'>");
//            out.println(" <form name='updateuser' method='GET' action='WorkflowTest'>");
//            out.println("  <input type='submit' name='updateUserAccount' value='update user accounts'>");
//            out.println(" </form>\r\n</div><!--queryform14-->");
//
//            String ownerName = request.getParameter("ownerName");
//            String description = request.getParameter("description");
//            String approverName = request.getParameter("approverName");
//            String cancellerName = request.getParameter("cancellerName");
//            String approveDossierId = request.getParameter("approveDossierId");
//            String cancelDossierId = request.getParameter("cancelDossierId");
//            String routeName = request.getParameter("routeName");
//            String returnName = request.getParameter("returnName");
//            String routeDossierId = request.getParameter("routeDossierId");
//            String returnDossierId = request.getParameter("returnDossierId");
//            String annotation = request.getParameter("annotation");
//            String findDossierId = request.getParameter("findDossierId");
//            String findActionListByName = request.getParameter("findActionListByName");
//            String findSingleByName = request.getParameter("findSingleByName");
//            String findByLocationName = request.getParameter("findByLocationName");
//            String findAllScopedByName = request.getParameter("findAllScopedByName");
//            String findBySchoolDeptName = request.getParameter("findBySchoolDeptName");
//            String synchronize = request.getParameter("synchronize");
//            String validateUserRoleName = request.getParameter("validateUserRoleName");
//            String synchronizeUserRoleName = request.getParameter("synchronizeUserRoleName");
//            String updateUserAccount = request.getParameter("updateUserAccount");
//
//            int schoolId = 0;
//            int departmentId = 0;
//
//            if (request.getParameter("department") != null && !request.getParameter("department").equalsIgnoreCase("All"))
//            {
//                String [] dept = request.getParameter("department").split(":");
//                schoolId = Integer.parseInt(dept[0]);
//                departmentId = Integer.parseInt(dept[1]);
//            }
//
//            DossierLocation findByLocation = null;
//            if (request.getParameter("findLocation") != null)
//            {
//                if (!request.getParameter("findLocation").equalsIgnoreCase("All"))
//                {
//                    findByLocation = DossierLocation.valueOf(request.getParameter("findLocation"));
//                }
//            }
//
//            DossierLocation routeLocation = null;
//            if (request.getParameter("routeLocation") != null)
//            {
//                routeLocation = DossierLocation.valueOf(request.getParameter("routeLocation"));
//            }
//
//            if (ownerName != null && ownerName.length() > 0)
//            {
//
//                DossierActionType actionType = DossierActionType.MERIT;
//                if (request.getParameter("actionType") != null)
//                {
//                    actionType = DossierActionType.valueOf(request.getParameter("actionType").toUpperCase());
//                }
//
//                DossierDelegationAuthority delegationAuthority = DossierDelegationAuthority.NON_REDELEGATED;
//                if (request.getParameter("delegationAuthority") != null)
//                {
//                    delegationAuthority = DossierDelegationAuthority.valueOf(request.getParameter("delegationAuthority").toUpperCase());
//                }
//
//                if (request.getParameter("redelegated") != null)
//                {
//                    delegationAuthority = request.getParameter("redelegated").equalsIgnoreCase("on") ? DossierDelegationAuthority.REDELEGATED : DossierDelegationAuthority.NON_REDELEGATED;
//                }
//
//                displayResults(null, Action.INITIATE, ownerName, description, delegationAuthority, actionType, annotation,  null, out);
//            }
//            else if (approveDossierId != null && approveDossierId.length() > 0)
//            {
//                displayResults(approveDossierId, Action.APPROVE, approverName, null, null, null, annotation,null, out);
//            }
//            else if (cancelDossierId != null && cancelDossierId.length() > 0)
//            {
//                displayResults(cancelDossierId, Action.CANCEL, cancellerName, null, null, null, annotation,null, out);
//            }
//            else if (routeDossierId != null && routeDossierId.length() > 0)
//            {
//                if (routeLocation != null)
//                {
//                    displayResults(routeDossierId, Action.ROUTETOLOCATION, routeName, null, null, null, annotation, routeLocation, out);
//                }
//                else
//                {
//                    displayResults(routeDossierId, Action.ROUTE, routeName, null, null, null, annotation, null, out);
//                }
//            }
//            else if (returnDossierId != null && returnDossierId.length() > 0)
//            {
//                displayResults(returnDossierId, Action.RETURN, returnName, null, null, null, annotation,  null, out);
//            }
//            else if (findDossierId != null && findDossierId.length() > 0)
//            {
//                displayResults(findDossierId, Action.FIND, mivSession.get().getLoggedInPerson().getPrincipalName(),
//                               null,null, null, null, null, out);
//            }
//            else if (findActionListByName != null && findActionListByName.length() > 0)
//            {
//                displayListResults(findActionListByName, null, Action.FINDACTIONLISTBYNAME, out);
//            }
//            else if (findSingleByName != null && findSingleByName.length() > 0)
//            {
//                displayListResults(findSingleByName, null, Action.FINDSINGLEBYNAME, out);
//            }
//            else if (findByLocationName != null)
//            {
//                displayListResults(findByLocationName, findByLocation, Action.FINDBYLOCATION, out);
//            }
//            else if (findAllScopedByName != null)
//            {
//                displayListResults(findAllScopedByName, null, Action.FINDALLSCOPEDBYNAME, out);
//            }
//            else if (findBySchoolDeptName != null)
//            {
//                displayListResults(findBySchoolDeptName, schoolId, departmentId, findByLocation, out);
//            }
//            else if (validateUserRoleName != null)
//            {
//                validateUserRoles(validateUserRoleName, out);
//            }
//            else if (synchronizeUserRoleName != null)
//            {
//                synchronizeUserRoles(synchronizeUserRoleName, out);
//            }
//            else if (synchronize != null)
//            {
//                DossierService dossierService = MivServiceLocator.getDossierService();
//                out.println(" <p>Synchronizing dossier locations.</p>");
//                long start = Calendar.getInstance().getTimeInMillis();
//                try
//                {
//                    dossierService.synchronizeDossierLocations();
//                }
//                catch (WorkflowException e)
//                {
//                    log.warning("WorkflowTest: got a WorkflowException synchronizeDossierLocations: "+e.getLocalizedMessage());
//                }
//                long stop = Calendar.getInstance().getTimeInMillis();
//                out.println(" <p>Completed synchronizing dossier locations in "+(stop - start)+"ms </p>");
//            }
//            else if (updateUserAccount != null)
//            {
//                updateUserAccount(out);
//            }
//
//            epilogue(out);
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    void updateUserAccount(ServletOutputStream out)
//    {
//        UserService userService = MivServiceLocator.getUserService();
//        DossierService dossierService = MivServiceLocator.getDossierService();
//        try
//        {
//            List<MivPerson> userAccountList = userService.findUsers(new AttributeSet());
//            log.info("====== BEGIN: Update UserAccount Records =======");
//            out.println(userAccountList.size() + " users retrieved from the UserAccount table...");
//            for (MivPerson mivPerson : userAccountList)
//            {
//                log.info("@@@@ "+mivPerson);
//                out.println("<p>Processing " + mivPerson + "</p>");
//                if (StringUtils.isBlank((mivPerson.getEntityId())))
//                {
//                    log.info("** Skipped - No entityId present **");
//                    out.println("** Skipped - No entityId present **");
//                    continue;
//                }
//                // If there is not principalId or if there is a principal Id and
//                // it is
//                // not numeric, update
//                if ((!StringUtils.isBlank(mivPerson.getPrincipalId()) && StringUtils.isAlpha(mivPerson.getPrincipalId().replaceAll("\\.", "")))
//                        || StringUtils.isBlank(mivPerson.getPrincipalId()))
//                {
//                    userService.savePerson(mivPerson);
//                    log.info(" Account update complete");
//                }
//                if (!StringUtils.isBlank(mivPerson.getPrincipalId()) && !StringUtils.isAlpha(mivPerson.getPrincipalId().replaceAll("\\.", "")))
//                {
//                    // Update the initiatior principalid for any dossiers which
//                    // may exist for this user
//                    try
//                    {
//                        List<Dossier> dossiers = dossierService.getAllDossiersByUserId(mivPerson.getUserId());
//                        log.info(" Dossiers found = "+dossiers.size());
//                        for (Dossier dossier : dossiers)
//                        {
//                            // Only update if the initiatorprincipalId does not match
//                            if (!dossier.getInitiatorPrincipalId().equals(mivPerson.getPrincipalId()))
//                            {
//                                dossier.setInitiatorPrincipalId(mivPerson.getPrincipalId());
//                                dossierService.saveDossier(dossier);
//                                log.info(" Dossier "+dossier.getDossierId()+" update complete");
//                                out.println("Dossier=" + dossier.getDossierId() + " updated.");
//                            }
//                        }
//                    }
//                    catch (WorkflowException wfe)
//                    {
//                        log.info("Attempt to update dossier(s) failed: "+wfe.getLocalizedMessage());
//                        out.println("<strong>Attempt to update dossiers failed - Exception: " + wfe.getMessage() + "</strong>");
//                    }
//                    catch (Exception e)
//                    {
//                        log.info("Attempt to update dossier(s) failed: "+e.getLocalizedMessage());
//                        out.println("<strong>Attempt to update dossiers failed - Exception: " + e.getMessage() + "</strong>");
//                    }
//                }
//            }
//        }
//        catch (IOException ioe)
//        {
//            ioe.printStackTrace();
//        }
//        log.info("====== END: Update UserAccount Records =======");
//    }
//
//    void displayListResults(String principalName, int school, int department, DossierLocation location, ServletOutputStream out)
//    {
//        DossierDocumentTest dossierDocumentTest = new DossierDocumentTest();
//
//        UserService userService = MivServiceLocator.getUserService();
//        List<Dossier> dossiers = new ArrayList<Dossier>();
//
//        MivPerson mivPerson = userService.getPersonByPrincipalName(principalName);
//
//        try
//        {
//            try
//            {
//                dossiers = dossierDocumentTest.findDossiersBySchoolDepartmentAndLocation(mivPerson.getPrincipalId(), school, department, location);
//            }
//            catch (WorkflowException wfe)
//            {
//                out.println(" <p><strong>Attempt to find dossiers failed - Exception: " + wfe.getMessage() + "</strong></p>");
//                wfe.printStackTrace();
//            }
//            for (Dossier dossier : dossiers)
//            {
//                try
//                {
//                    displayDocumentDetail(dossier, out);
//                }
//                catch (WorkflowException e)
//                {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//        }
//        catch (IOException ioe)
//        {
//            ioe.printStackTrace();
//        }
//    }
//
//    void displayListResults(String principalName, Object location, Action action, ServletOutputStream out)
//    {
//        DossierDocumentTest dossierDocumentTest = new DossierDocumentTest();
//
//        DossierService dossierService = MivServiceLocator.getDossierService();
//        UserService userService = MivServiceLocator.getUserService();
//        MivPerson mivPerson = userService.getPersonByPrincipalName(principalName);
//        String principalId = mivPerson.getPrincipalId();
//
//        try
//        {
//            try
//            {
//                List<Dossier> dossiers = new ArrayList<Dossier>();
//                List<Long> dossierIds = new ArrayList<Long>();
//                switch (action)
//                {
//                    case FINDACTIONLISTBYNAME:
//                        dossierIds = dossierDocumentTest.findActionListDossiersByPrincipalId(principalId);
//                        for (Long dossierId : dossierIds)
//                        {
//                            try
//                            {
//                                dossiers.add(dossierService.getDossier(dossierId));
//                            }
//                            catch (WorkflowException wfe)
//                            {
//                                System.out.println("Skipped dossier ID " + dossierId + ": " + wfe.getLocalizedMessage());
//                            }
//                        }
//                        break;
//                    case FINDSINGLEBYNAME:
//                        dossiers = dossierDocumentTest.getDossierByPrincipalId(principalId);
//                        break;
//                    case FINDBYLOCATION:
//                        dossiers = dossierDocumentTest.getDossiersByLocation(principalId, (DossierLocation) location);
//                        break;
//                    case FINDALLSCOPEDBYNAME:
//                        dossiers = dossierDocumentTest.getDossiersScopedByName(principalName);
//                        break;
//                    default:
//                        return;
//                }
//                for (Dossier dossier : dossiers)
//                {
//                    try
//                    {
//                        displayDocumentDetail(dossier, out);
//                    }
//                    catch (IOException e)
//                    {
//                        e.printStackTrace();
//                    }
//                }
//            }
//            catch (WorkflowException wfe)
//            {
//                out.println(" <p><strong>Attempt to find dossiers failed - Exception: " + wfe.getMessage() + "</strong></p>");
//                wfe.printStackTrace();
//            }
//            catch (IllegalStateException ise)
//            {
//                out.println(" <p><strong>Attempt to find dossier(s) for " + principalName + " failed - Exception: " + ise.getMessage()
//                        + "</strong></p>");
//                ise.printStackTrace();
//            }
//        }
//        catch (IOException ioe)
//        {
//            ioe.printStackTrace();
//        }
//    }
//
//    void displayResults(String dossierIdStr, Action action, String principalName, String description,
//            DossierDelegationAuthority delegationAuthority, DossierActionType actionType, String annotation, DossierLocation location,
//            ServletOutputStream out)
//    {
//        DossierDocumentTest dossierDocumentTest = new DossierDocumentTest();
//        Dossier dossier = null;
//        Long dossierId = 0L;
//
//        // The principalName is required for everything except the FIND action
//        String principalId = "";
//        if (principalName != null)
//        {
//            UserService userService = MivServiceLocator.getUserService();
//            MivPerson mivPerson = userService.getPersonByPrincipalName(principalName);
//            principalId = mivPerson.getPrincipalId();
//        }
//
//        try
//        {
//            try
//            {
//                if (dossierIdStr != null)
//                {
//                    dossierId = Long.valueOf(dossierIdStr);
//                }
//                switch (action)
//                {
//                    case INITIATE:
//                        dossier = dossierDocumentTest.initiateDossier(principalId, description, delegationAuthority, actionType,
//                                annotation);
//                        break;
//                    case APPROVE:
//                        dossier = dossierDocumentTest.approveDossier(principalId, dossierId, annotation);
//                        break;
//                    case CANCEL:
//                        dossier = dossierDocumentTest.cancelDossier(principalId, dossierId, annotation);
//                        break;
//                    case ROUTE:
//                        dossier = dossierDocumentTest.routeDossier(principalId, dossierId, annotation);
//                        break;
//                    case ROUTETOLOCATION:
//                        dossier = dossierDocumentTest.routeDossierToLocation(principalId, dossierId, location, annotation);
//                        break;
//                    case RETURN:
//                        dossier = dossierDocumentTest.returnDossierToPreviousNode(principalId, dossierId, annotation);
//                        break;
//                    case FIND:
//                        dossier = dossierDocumentTest.getDossier(dossierId);
//                        break;
//                    default:
//                        return;
//                }
//
//                if ((dossier == null))
//                {
//                    out.println(" <p><strong>Attempt to " + action.description + " dossier failed, " + dossierId
//                            + " does not exist!</strong></p>");
//                }
//                else
//                {
//                    displayDocumentDetail(dossier, out);
//                }
//            }
//            catch (WorkflowException wfe)
//            {
//                out.println(" <p><strong>Attempt to " + action.description + " dossier " + dossierId + " failed - Exception: "
//                        + wfe.getMessage() + "</strong></p>");
//                wfe.printStackTrace();
//            }
//            catch (IllegalStateException ise)
//            {
//                out.println(" <p><strong>Attempt to " + action.description + " dossier " + dossierId + " failed - Exception: "
//                        + ise.getMessage() + "</strong></p>");
//                ise.printStackTrace();
//            }
//            catch (NumberFormatException nfe)
//            {
//                out.println(" <p><strong>Attempt to " + action.description + " dossier " + dossierId
//                        + " failed - Dossier Id must be numeric.</strong></p>");
//                nfe.printStackTrace();
//            }
//            catch (Exception e)
//            {
//                out.println(" <p><strong>Attempt to " + action.description + " dossier " + dossierId + " failed - Exception: "
//                        + e.getMessage() + "</strong></p>");
//                e.printStackTrace();
//            }
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    void displayDocumentDetail(Dossier dossierDocument, ServletOutputStream out) throws WorkflowException, IOException
//    {
//		out.println("<h2>Document Result</h2>");
//    	out.println("<div id=\"results\">");
//    	out.println(" <p>Dossier "+dossierDocument.getDossierId()+"</p>");
//    	WorkflowInfo workflowInfo = new WorkflowInfo();
//    	out.println(" <p>Route Status: "+workflowInfo.getDocumentDetail(dossierDocument.getDossierId()).getDocRouteStatus()+"</p>");
//    	out.println(" <p>DocumentContent: "+workflowInfo.getDocumentContent(dossierDocument.getDossierId()).getApplicationContent()+"</p>");
//        out.println("<p>Node Names:</p>");
//	    for (String node : workflowInfo.getCurrentNodeNames(dossierDocument.getDossierId()))
//	    {
//	    	out.println("\n&nbsp;"+node);
//	    }
//    	out.println("<p>Action Requests:</p>");
//	    if (workflowInfo.getActionRequests(dossierDocument.getDossierId()) != null)
//	    {
//	    	for (ActionRequestDTO actionRequest : workflowInfo.getActionRequests(dossierDocument.getDossierId()))
//	    	{
//	    		out.println("<p>");
//		    	out.println("&nbsp;&nbsp;Action Requested: "+actionRequest.getActionRequested());
//		    	out.println("&nbsp;&nbsp;Pending: "+actionRequest.isPending());
//		    	out.println("&nbsp;&nbsp;Node Name: "+actionRequest.getNodeName());
//		    	out.println("&nbsp;&nbsp;Principal Id: "+actionRequest.getPrincipalId());
//	//	    	out.println("&nbsp;&nbsp;Principal Id: "+actionRequest.getPrincipalId()+ "("+identityService.getPrincipal(actionRequest.getPrincipalId()).getPrincipalName()+")");
//		    	out.println("&nbsp;&nbsp;Role Name: "+actionRequest.getRoleName());
//	        	out.println("/<p>");
//	    	}
//	    }
//
//    	out.println("<p>Actions Taken:</p>");
//	    if (workflowInfo.getActionsTaken(dossierDocument.getDossierId()) != null)
//	    {
//	    	for (ActionTakenDTO actionTaken : workflowInfo.getActionsTaken(dossierDocument.getDossierId()))
//	    	{
//	    		out.println("<p>");
//		    	out.println("&nbsp;&nbsp;Action: "+actionTaken.getActionTaken());
//		    	out.println("&nbsp;&nbsp;Taken By: "+actionTaken.getPrincipalId()+" ("+identityService.getPrincipal(actionTaken.getPrincipalId()).getPrincipalName()+")");
//		    	out.println("&nbsp;&nbsp;Annotation: "+actionTaken.getAnnotation());
//		    	out.println("&nbsp;&nbsp;Action Date: "+new Date(actionTaken.getActionDate().getTimeInMillis()).toString());
//		    	out.println("</p>");
//
//	    	}
//	    }
//	    out.println("<p>Actions Allowed:</p>");
//
//	    if (workflowInfo.getDocumentDetail(dossierDocument.getDossierId()).getValidActions() != null)
//	    {
//		    for (String action : workflowInfo.getDocumentDetail(dossierDocument.getDossierId()).getValidActions().getValidActionCodesAllowed())
//		    {
//		    	out.println("\n&nbsp;"+action);
//		    }
//	    }
//		out.println("</div><!--results-->");
//    }
//
//    void prologue(ServletOutputStream out)
//    {
//        try {
//            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
//            out.println("<html>\r\n<head>");
//            out.println(" <title>Workflow Test</title>");
//            out.println(" <link rel='stylesheet' type='text/css' href='../myinfovault.css'>");
//            out.println(" <link rel='shortcut icon' href='../images/favicon.ico' type='image/x-icon'>");
//            out.println(" <style>");
//            out.println("  div.resultTable {");
//            out.println("    margin-bottom: 1.5em;");
//            out.println("  }");
//            out.println(" </style>");
//            out.println("</head>\r\n<body>");
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    void epilogue(ServletOutputStream out)
//    {
//        try {
//            out.println("</body>\r\n</html>");
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    int beginTable(ServletOutputStream out, String... columns)
//        throws IOException
//    {
//        int colcount = 0;
//        out.println("\n<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\">");
//        out.print("  <thead>\n    <tr>");
//        for (String s : columns)
//        {
//            out.print("<th>" + s + "</th>");
//            colcount++;
//        }
//        out.println("</tr>\n  </thead>\n  <tbody>");
//        return colcount;
//    }
//
//    void finishTable(ServletOutputStream out) throws IOException
//    {
//        out.println("  </tbody>\n</table>\n");
//    }
//
//    void validateUserRoles(String principalName, ServletOutputStream out)
//    {
//        MivPersonDaoImpl mivPersonDao = (MivPersonDaoImpl) MivServiceLocator.getBean("personDao");
//        UserService userService = MivServiceLocator.getUserService();
//        MivPerson mivPerson = userService.getPersonByPrincipalName(principalName);
//        Map<KimUpdateAction, Map<String, AttributeSet>> validationResultsMap =
//            new HashMap<KimUpdateAction, Map<String, AttributeSet>>();
//
//        if (mivPerson != null)
//        {
//            try
//            {
//                MivPersonImpl p = (MivPersonImpl) mivPerson;
//                out.println("<h3>Validate Roles Result For " + p.getDisplayName() + " (Status: Active="+p.isActive()+").</h3>");
//                validationResultsMap = p.validateRoles();
//                if (validationResultsMap.isEmpty())
//                {
//                    out.println("<p>** Roles are currenly IN SYNC between MIV and KIM. **</p>");
//                }
//                else
//                {
//                    out.println("<p>*** Roles are OUT OF SYNC between MIV and KIM. ***</p>");
//
//                    for (KimUpdateAction action : validationResultsMap.keySet())
//                    {
//                        Map<String, AttributeSet> kimAttributeMap = validationResultsMap.get(action);
//                        for (String schoolDepartmentRoleKey : kimAttributeMap.keySet())
//                        {
//                            MivRole role = null;
//                            // Get the role from the key
//                            String [] schoolDepartmentRole = schoolDepartmentRoleKey.split(":");
//                            if (schoolDepartmentRole.length == 3)
//                            {
//                                role = MivRole.valueOf(schoolDepartmentRole[2]);
//                            }
//                            else
//                            {
//                                role = MivRole.valueOf(schoolDepartmentRole[1]);
//                            }
//
//                            // Only DEPT_STAFF and SCHOOL_STAFF present in KIM
//                            switch (role)
//                            {
//                                case DEPT_STAFF:
//                                case SCHOOL_STAFF:
//                                    // Get KIM qualifiers
//                                    AttributeSet attributeSet = kimAttributeMap.get(schoolDepartmentRoleKey);
//                                    StringBuffer qualifiers = new StringBuffer();
//                                        qualifiers.append("(");
//                                        for (String attribute : attributeSet.keySet())
//                                        {
//                                            qualifiers.append(attribute).append("=").append(attributeSet.get(attribute)).append(", ");
//                                        }
//                                        if (qualifiers.length() > 0)
//                                        {
//                                            qualifiers.delete(qualifiers.length() - 2, qualifiers.length()).append(")");
//                                        }
//                                    out.println("The " + role.name() + " role for " + mivPerson.getDisplayName() + " is "+(action == KimUpdateAction.ADD ? "not":"") +" present in KIM "
//                                                + (qualifiers.length() > 0 ? "with qualifiers " + qualifiers : "") + ", but is "+(action == KimUpdateAction.ADD ? "":"not")+" present in MIV<br>");
//                            }
//                        }
//                    }
//                }
//
//                // Print the validation summary
//                for (String messageLine : mivPersonDao.getValidationSummary(mivPerson)) {
//                    out.println(messageLine+"<br>");
//
//                }
//
//            }
//            catch (IOException ioe)
//            {
//                ioe.printStackTrace();
//            }
//        }
//    }
//
//    void synchronizeUserRoles(String principalName, ServletOutputStream out)
//    {
//        UserService userService = MivServiceLocator.getUserService();
//        MivPerson mivPerson = userService.getPersonByPrincipalName(principalName);
//        Map<KimUpdateAction, Map<String, AttributeSet>> validationResultsMap =
//            new HashMap<KimUpdateAction, Map<String, AttributeSet>>();
//        try
//        {
//            if (mivPerson != null)
//            {
//                out.println("<h3>Synchronize Roles For " + mivPerson.getDisplayName() + ".</h3>");
//                MivPersonImpl p = (MivPersonImpl) mivPerson;
//                validationResultsMap = p.validateRoles();
//                if (!validationResultsMap.isEmpty())
//                {
//                   if (userService.synchronizeRoles(mivPerson))
//                   {
//                       out.println("<p>** Synchronization Successful **<p>");
//                   }
//                   else
//                   {
//                       out.println("<p>** Synchronization Failed **<p>");
//                   }
//                }
//                else
//                {
//                    out.println("<p>** No Synchronization Required **<p>");
//                }
//                // Validate after sync
//                validateUserRoles(principalName, out);
//            }
//            else
//            {
//                out.println("<h3>Unable To Synchronize Roles - Invalid principalName " + principalName + ".</h3>");
//            }
//        }
//        catch (IOException ioe)
//        {
//            ioe.printStackTrace();
//        }
//    }
}
