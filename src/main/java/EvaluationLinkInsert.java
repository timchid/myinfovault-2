/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EvaluationLinkInsert.java
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import edu.ucdavis.myinfovault.MIVConfig;


/**
 * Load the VetMed teaching evaluation data after the manual steps have been performed.
 * Loads the records from the eval_links table into the CourseEvaluationSummary table.
 *
 * @author Lawrence Fyfe
 * @since MIV 1.x
 */
public class EvaluationLinkInsert extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    DataSource source = null;
    Connection connection = null;


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            ServletOutputStream out = response.getOutputStream();
            response.setContentType("text/html");

            out.println("<p>This version supports Quarters and Semesters</p>");

            out.println("<form action='EvaluationLinkInsert' method='post'>");
            out.println("<table cellpadding='5'>");

            out.println("<tr>");
            out.println("<td>Evaluation Insert Query</td>");
            out.println("</tr>");

            out.println("<tr>");
            out.println("<td><input type='submit' value='Execute Query'/></td>");
            out.println("</tr>");

            out.println("</table>");
            out.println("</form>");

            //MIVPage.displayFooter(out);

            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    {
        final String evalQuery =
            "select UserId, " +
            "term_code, " +
            "course_name, " +
            "course_title, " +
            "response, " +
            "instructor_score, " +
            "eval_links.url, " +
            "course_score " +
            "from eval_links left join UserAccount on mothra_id = PersonUUID " +
            "where UserID IS NOT NULL";

        String evaInsert = "insert into CourseEvaluationSummary(ID,UserID,TypeID,Year,Course,Description,ResponseTotal,InstructorScore,CourseScore,Link,Sequence,Display) values \n";
        String userID = "";
        String termCode = "";
        String year = "";
        String term = "";
        String sequence = "";
        int count = 0;
        String separator = "";
        int insertCount = 0;
        String typeId = "2";
        Statement evalStatement = null;
        ResultSet evalSet = null;

        try
        {
            this.source = MIVConfig.getConfig().getDataSource();
            this.connection = this.source.getConnection();

            evalStatement = connection.createStatement();
            evalSet = evalStatement.executeQuery(evalQuery);

            ServletOutputStream out = response.getOutputStream();
            response.setContentType("text/html");
            String insertedUsersList = "";
            out.println("<div style=\"border-width: 1px 3px 3px 1px; border-style: solid; padding: 1em; box-shadow: 5px 3px 15px rgb(119, 119, 119); width: 50em; font-family: Helvetica,Arial,sans-serif;\">");
            out.println("<h1 style=\"color: rgb(0, 38, 102); background-color: transparent; padding: 0px 10px 10px 0px; margin: 0px; font-size: 1.3em;\">Execution Result</h1>");
            while (evalSet.next())
            {
                userID = evalSet.getString(1);

                if(insertedUsersList.length()==0)
                    insertedUsersList = userID;
                else
                    insertedUsersList += "\n"+userID;

                termCode = evalSet.getString(2);

                year = termCode.substring(0, 4);
                term = termCode.substring(4, 6);

                sequence = "1";
                int seq = this.getNextSequence("CourseEvaluationSummary", Integer.parseInt(userID));
                if (seq > 0)
                {
                    sequence = Integer.toString(seq);
                }

                if (count > 0)
                {
                    separator = ",\n";
                }

                evaInsert = evaInsert + separator + "(0,'" +
                userID +
                "',"+typeId+",'" +
                replaceTerm(term) + " " + year +
                "','" +
                evalSet.getString("course_name") +
                "','" +
                evalSet.getString("course_title") +
                "','" +
                evalSet.getString("response") +
                "','" +
                evalSet.getString("instructor_score") +
                "','" +
                evalSet.getString("course_score") +
                "','" +
                evalSet.getString("url") +
                "',"+sequence+",1)";

                count++;
            }
            evaInsert = evaInsert + ";\n";
            //MIVPage.displayHeader(request, out);

            // insert records if there record count greater than zero
            if(count > 0)
            {
                Statement evaStatement = connection.createStatement();
                insertCount = evaStatement.executeUpdate(evaInsert);

                out.println("List of valid users:");out.println("<br>");
                out.println("<textarea wrap=\"soft\" cols=\"85\" rows=\"10\" name=\"validusers\" id=\"validusers\">"+insertedUsersList+"</textarea>");
                out.println("<br>");
                out.println("<span style=\"color:green\">"+Integer.toString(insertCount) + " record"+(insertCount>1?"s":"")+" inserted.</span>");
                out.println("<hr/>");
                out.println("Insert Script:");out.println("<br>");
                out.println("<textarea wrap=\"soft\" cols=\"85\" rows=\"10\" name=\"scripts\" id=\"scripts\">"+evaInsert+"</textarea>");
                out.println("<hr/>");
            }

            out.println("Invalid records:");out.println("<br>");
            out.println("<textarea wrap=\"soft\" cols=\"85\" rows=\"10\" name=\"invalidrecords\" id=\"invalidrecords\">");

            final String inValidUsersQuery = "select e.mothra_id,e.mail_id,e.employee_id,e.first_name,e.last_name,e.term_code,e.crn,e.course_name,e.course_title,e.response,e.instructor_score,e.course_score,e.url, "
                    +" if(ua.Active IS NULL, 'Not a User', if(ua.Active=1,'Active User','Deactivate User'))AS `Status`"
                    +" from eval_links e left join UserAccount ua on e.mothra_id = ua.PersonUUID"
                    +" where UserID IS NULL"
                    +" order by `Status`";
            evalStatement = connection.createStatement();
            evalSet = evalStatement.executeQuery(inValidUsersQuery);

            int index=0;
            while (evalSet.next())
            {
                if(index==0)
                {
                    out.println("mothra_id,mail_id,employee_id,first_name,last_name,term_code,crn,course_name,course_title,response,instructor_score,course_score,url");
                }
                out.println(evalSet.getString("mothra_id")+","+evalSet.getString("mail_id")+","+evalSet.getString("employee_id")+","
                                +evalSet.getString("first_name")+","+evalSet.getString("last_name")+","+evalSet.getString("term_code")+","
                                +evalSet.getString("crn")+","+evalSet.getString("course_name")+","+evalSet.getString("course_title")+","
                                +evalSet.getString("response")+","+evalSet.getString("instructor_score")+","+evalSet.getString("course_score")+","
                                +evalSet.getString("url"));
                index++;
            }
            out.println("</textarea>");
            out.println("<br>");
            out.println("<span style=\"color:red\">"+Integer.toString(index) + " invalid record"+(index>1?"s":"")+" found.</span>");

            //out.println(Integer.toString(insertCount) + " records inserted.<br><br>");
            //out.println(evaInsert);

            out.println("</div>");

            //MIVPage.displayFooter(out);

            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                evalSet.close();
                evalStatement.close();
                this.connection.commit();
                this.connection.close();
            }
            catch(SQLException sqle)
            {
                // ignore
            }
        }
    }


    private String replaceTerm(String oldTerm)
    {
        String newTerm = "";

        if (oldTerm.equals("01"))
        {
            newTerm = "Winter Quarter";
        }
        else if (oldTerm.equals("02"))
        {
            newTerm = "Spring Semester";
        }
        else if (oldTerm.equals("03"))
        {
            newTerm = "Spring Quarter";
        }
        else if (oldTerm.equals("04"))
        {
            newTerm = "Summer Semester";
        }
        else if (oldTerm.equals("05"))
        {
            newTerm = "Summer Session I";
        }
        else if (oldTerm.equals("07"))
        {
            newTerm = "Summer Session II";
        }
        else if (oldTerm.equals("08"))
        {
            newTerm = "Summer Quarter";
        }
        else if (oldTerm.equals("09"))
        {
            newTerm = "Fall Semester";
        }
        else if (oldTerm.equals("10"))
        {
            newTerm = "Fall Quarter";
        }
        else
        {
            newTerm = "Unknown";
        }

        return newTerm;
    }


    /**
     * Find the next Sequence number to use in a table.
     * If the table doesn't exist or there is no Sequence number column a value
     * of -1 is returned; the call shouldn't try to insert a sequence number.
     * @param table Name of the table
     * @return An available sequence number, or -1 to indicate nothing appropriate.
     */
    private int getNextSequence(String table, int userID)
    {
        int seq = -1;

        String query = "SELECT MAX(Sequence) FROM "+table+" WHERE UserID=?";

        PreparedStatement statement = null;
        ResultSet results = null;

        try
        {
            statement = this.connection.prepareStatement(query);
            statement.setInt(1, userID);
            results = statement.executeQuery();
            if (results.next())
            {
                seq = results.getInt(1);
                seq += 10;
            }
        }
        catch (SQLException e)
        {
            //table doesn't exist or there is no Sequence number column a value of -1 is returned
            seq = -1;
            // Ignore the exception, allow the default (10) to be returned
            //e.printStackTrace();
        }
        finally
        {
            if (results != null)    try { results.close();   results = null;   } catch (Exception e) {}
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
        }
        return seq;
    }
}
