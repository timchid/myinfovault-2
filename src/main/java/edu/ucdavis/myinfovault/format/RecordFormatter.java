/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecordFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**<p>
 * Format a record given a mapping of fields to data in a Map.
 * The single method, {@link #format(Map)}, performs any field value
 * transformations that the RecordFormatter implementor advertises.
 * An implementor may add to or modify the contents of the provided Map,
 * or may create and return a new Map with the modifications. An implementor
 * of RecordFormatter should document what exactly is returned, along
 * with what formatting is provided.</p>
 * <p>It is recommended, when the original map is being returned, that
 * the formatted (modified) values are added to the map with a new key
 * rather than replacing the original data under the same key. Further,
 * the new key should bear some relation to the original key (or keys)
 * and the implementing class should clearly document the additional
 * key(s).</p>
 * <p>The interface method {@link #getAddedFields()} returns the (possibly
 * empty) list of new fields added to the map.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public interface RecordFormatter
{
    // Define constants here for the preview formatting
    // Constants are defined in alphabetical order. If you want add one in the order
    public static final String CLOSE_PARENTHESIS = ")";
    public static final String COLON = ":";
    public static final String COMMA = ",";
    public static final String DASH = "-";
    public static final String DOUBLE_SPACE = "  ";
    public static final String NOTHING = "";
    public static final String OPEN_PARENTHESIS = "(";
    public static final String PERIOD = ".";
    public static final String SINGLE_SPACE = " ";
    public static final String LINE_BREAK = "<br>";
    // Combined Constants
    public static final String COMMA_SPACE = COMMA+SINGLE_SPACE;

    /**
     * Apply the formatting that an implementor provides on the given field map.
     * @param m map of field names to data
     * @return The modified original map, <em>or</em> a new map.
     */
    public Map<String,String> format(Map<String,String> m);

    /**<p>
     * Add a sub-formatter to this (higher level) formatter. When the containing
     * formatters {@link #format(Map) format()} method is called it should call
     * any additional formatters added to it by this method. The order that
     * sub-formatters are called is not guaranteed and is up to the implementation &mdash;
     * if order is important the caller should create additional top-level formatters
     * and apply them in the necessary order. It is allowable for implementations to
     * throw away added sub-formatters and/or never apply them, though doing so will
     * likely confuse callers using your class.</p>
     * <p>Implementors should return <code><em>this</em></code> from the <code>add()</code>
     * method to allow for chaining of additions in the form:</p><pre>
     *   topFormatter.add(sub1).add(sub2).add(sub3);
     * </pre>
     * @param formatter An instance of a RecordFormatter to add as a sub-formatter
     * @return this
     */
    public RecordFormatter add(RecordFormatter formatter);

    /**
     * Get the names of pseudo-fields added by a formatter.
     * @return An {@link java.lang.Iterable Iterable} list of field names added by the formatter.
     */
    public Iterable<String> getAddedFields();


    /**
     * Specify the Record Type for each formatter.
     * @param rectype type of record this formatter is used for.
     */
    public void setRecType(String rectype);


    /**
     * Add a pseudo-field or derived field as a required field.
     * This allows formatters that add extra information, translated fields,
     * or derived fields to treat them like they are required.
     * See the WorksFormatter for an example.
     * @param name Name of the (pseudo) field to make required
     */
    public void addRequiredField(String name);

}
