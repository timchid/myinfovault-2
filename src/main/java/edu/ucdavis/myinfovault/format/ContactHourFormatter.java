package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class ContactHourFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder resequencePreview = new StringBuilder();

        //removed fixFloat process here because .00 should be displayed.  miv-2240
        String discussion = m.get("discussion");
        String lab = m.get("lab");
        String lecture = m.get("lecture");
        String clinic = m.get("clinic");

        boolean emptyBeforeDiscussion = isEmptyOrBlank(lecture);
        boolean emptyBeforeLab = isEmptyOrBlank(lecture) && isEmptyOrBlank(discussion);
        boolean emptyBeforeClinic = isEmptyOrBlank(m.get(lecture)) && isEmptyOrBlank(discussion) && isEmptyOrBlank(lab);

        preview.append( insertValue("term", m.get("term"), "<span class=\"prefix\">", "</span>"));
        resequencePreview.append( insertValue("lecture", lecture, " Lecture=", ""));


        String prefix = (emptyBeforeDiscussion ? "" : COMMA) + " Discussion=";
        resequencePreview.append(insertValue("discussion", discussion, prefix, ""));

        prefix = (emptyBeforeLab ? "" : COMMA) + " Lab=";
        resequencePreview.append(insertValue("lab", lab, prefix, ""));

        prefix = (emptyBeforeClinic ? "" : COMMA) + " Clinic=";
        resequencePreview.append(insertValue("clinic", clinic, prefix, ""));

 /*       if(emptyBeforeDiscussion)
        {
           resequencePreview.append(insertValue("discussion", discussion, " Discussion=", ""));
        }
        else
        {
            resequencePreview.append(insertValue("discussion", discussion, ", Discussion=", ""));
        }

        if(emptyBeforeLab)
        {
            resequencePreview.append( insertValue("lab", lab, " Lab=", ""));
        }
        else
        {
            resequencePreview.append( insertValue("lab", lab, ", Lab=", ""));
        }

        if(emptyBeforeClinic)
        {
            resequencePreview.append( insertValue("clinic", clinic, " Clinic=", ""));
        }
        else
        {
            resequencePreview.append( insertValue("clinic", clinic, ", Clinic=", ""));
        }
*/


        String sequencePreview = resequencePreview.toString().replaceAll(COMMA+SINGLE_SPACE+COMMA, COMMA+SINGLE_SPACE);
        m.put("sequencepreview", sequencePreview);

        prefix = sequencePreview.length()>0 ? COMMA_SPACE : "";
        preview.append(prefix).append(sequencePreview);

/*        m.put("sequencepreview", resequencePreview.toString().replaceAll(COMMA+SINGLE_SPACE+COMMA, COMMA+SINGLE_SPACE));

        if(resequencePreview.toString().length()>0)
        {
            preview.append(", ").append(resequencePreview).toString().replaceAll(COMMA+SINGLE_SPACE+COMMA, COMMA+SINGLE_SPACE);
        }
        else
        {
            preview.append(resequencePreview).toString().replaceAll(COMMA+SINGLE_SPACE+COMMA, COMMA+SINGLE_SPACE);
        }
*/
        m.put("preview", preview.toString());
        return m;
    }

/*
    private String fixFloat(String in)   //miv-2240 this routine is nolonger used
    {
        String pattern = "\\.0+\\Z";
        if (in == null || in.length() == 0) return in;
        String out = in.replaceAll(pattern, "");
        return out;
    }
*/
}
