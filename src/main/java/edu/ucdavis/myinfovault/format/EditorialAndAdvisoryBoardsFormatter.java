package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class EditorialAndAdvisoryBoardsFormatter extends PreviewBuilder
{
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        m.put("remark", insertValue("remark", m.get("remark"),COMMA_SPACE, ""));

        m.put("year", insertValue("years", m.get("years")));

        // All the fields used in a preview are required - shout out if any are missing
        preview
                .append( insertValue("description", m.get("description"), SINGLE_SPACE, ""))
                ;

        return preview.toString();
    }
}
