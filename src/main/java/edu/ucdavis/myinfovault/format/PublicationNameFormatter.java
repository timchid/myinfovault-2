package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.logging.Logger;

/**
 * Marks the publication name ("journal" field) with a span tag.
 * @author Stephen Paulsen
 */
public class PublicationNameFormatter extends FormatAdapter implements RecordFormatter
{
    private static final String FIELDKEY = "journal";
    private static Logger log = Logger.getLogger("MIV");

    @Override
    public Map<String,String> format(Map<String,String> m)
    {
        log.entering(this.getClass().getSimpleName(), "format");

        String value, newval="";
        value = m.get(FIELDKEY);

        if (!isEmpty(value))
        {
            newval = "<span class=\""+FIELDKEY+"\">" + value + "</span>";
            m.put(FIELDKEY, newval);
        }

        log.exiting(this.getClass().getSimpleName(), "format");
        return m;
    }
}
