package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.logging.Logger;

/**
 * Format the URL of a record for the publication citation previews.
 * This formatter modifies the value of the existing <em>URL</em> field.
 *
 * @author Brij Garg
 * @since MIV 2.0
 */
public class URLFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private static final String FIELDKEY = "link";

    private PreviewType previewType;

    protected static final String PUBLICATIONLINKLABEL = "View this Article";
    protected static final String EVALUATIONLINKLABEL = "View this On-Line Evaluation";
    protected static final String PATENTLINKLABEL = "View Patent Information";
    protected static final String WORKSLINKLABEL = "View Work Information";
    protected static final String EVENTSLINKLABEL = "View Event Information";
    protected static final String REVIEWSLINKLABEL = "View Review Information";

    enum PreviewType {
        EVALUATION,
        PUBLICATION,
        PATENT,
        WORKS,
        PUBLICATIONEVENTS,
        EVENTS,
        REVIEWS
    }

    public URLFormatter(PreviewType style)
    {
        this.previewType = style;
    }

    @Override
    public RecordFormatter add(RecordFormatter formatter)
    {
        return this;
    }

    @Override
    public Map<String,String> format(Map<String,String> m)
    {
        log.entering(this.getClass().getSimpleName(), "format");
        this.applySubformats(m);
        StringBuilder formattedURL = new StringBuilder();

        String link = m.get(FIELDKEY);
        if (!isEmpty(link))
        {
            if (!link.startsWith("http://") && !link.startsWith("https://"))
            {
                link = "http://"+link;
            }

            // Generate a hyperlink for the URL entered, to show in the preview

            formattedURL.append("<a href=\"").append(link).append("\" title=\"");
            switch (previewType)
            {
                case EVALUATION:
                    formattedURL.append(EVALUATIONLINKLABEL).append("\">");
                    formattedURL.append(EVALUATIONLINKLABEL);
                    break;
                case PUBLICATION:
                    formattedURL.append(PUBLICATIONLINKLABEL).append("\">");
                    formattedURL.append(PUBLICATIONLINKLABEL);
                    break;
                case PATENT:
                    formattedURL.append(PATENTLINKLABEL).append("\">");
                    formattedURL.append(PATENTLINKLABEL);
                    break;
                case WORKS:
                    formattedURL.append(WORKSLINKLABEL).append("\">");
                    formattedURL.append(WORKSLINKLABEL);
                    break;
                case PUBLICATIONEVENTS:
                    formattedURL.append(PUBLICATIONLINKLABEL).append("\">");
                    formattedURL.append(PUBLICATIONLINKLABEL);
                    break;
                case EVENTS:
                    formattedURL.append(EVENTSLINKLABEL).append("\">");
                    formattedURL.append(EVENTSLINKLABEL);
                    break;
                case REVIEWS:
                    formattedURL.append(REVIEWSLINKLABEL).append("\">");
                    formattedURL.append(REVIEWSLINKLABEL);
                    break;
                default:
                    // do nothing if it's not one of these types (but that's impossible)
                    break;
            }
//            if (EVALUATION.equals(previewType))
//            {
//                formattedURL.append(EVALUATIONLINKLABEL).append("\">");
//                formattedURL.append(EVALUATIONLINKLABEL);
//            }
//            else if (PUBLICATION.equals(previewType))
//            {
//                formattedURL.append(PUBLICATIONLINKLABEL).append("\">");
//                formattedURL.append(PUBLICATIONLINKLABEL);
//            }

            formattedURL.append("</a>");

            log.finest(" url: orig=[" + m.get(FIELDKEY) + "] new=[" + formattedURL + "]");

            m.put(FIELDKEY, formattedURL.toString());
        }

        log.exiting(this.getClass().getName(), "format");
        return m;
    }
}
