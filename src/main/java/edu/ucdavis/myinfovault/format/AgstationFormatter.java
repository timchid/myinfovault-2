/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AgstationFormatter.java
 */


package edu.ucdavis.myinfovault.format;

import java.io.File;
import java.util.Map;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class AgstationFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder(100);

        String uploadName = "PDF Document";
        String readOnly;
        String s;

        // Transfer the Agriculture Experiment Station "dataspan" field
        // into a "year" pseudo-field for consistency w/other rec types.
        if (m.containsKey("reporttitle"))
        {
            s = m.get("datespan");
            m.put("year", insertValue("datespan", m.get("datespan")));
            preview
            //  .append( isEmpty(s=m.get("datespan")) ? "" : s+", " ) // leave the datespan out of the preview.
                .append( insertValue("reporttitle", m.get("reporttitle")))
                .append( insertValue("reportnumber", m.get("reportnumber"), ", Number: ", ""))
                .append( insertValue("investigatorname", m.get("investigatorname"), ", Investigators: ", ""))
                ;

            m.put("preview", preview.toString());
            m.put("sequencepreview", preview.toString());
        }
        else
        {
            readOnly = "true";
            if ( ! isEmpty(s=m.get("uploadname")))
            {
                uploadName = s;
            }

            if ( ! isEmpty(s=m.get("uploadfile")))
            {
                // Build a URL from the file name
                int userId = Integer.parseInt(m.get("userid"));
                // Full upload file path
                File uploadFile = new File(MIVConfig.getConfig().getDossierPdfLocation(userId),s);
                // Convert the file path to a URL
                String uploadFileUrl  = new PathConstructor().getUrlFromPath(uploadFile.getAbsoluteFile());
                String link = "<a href=\"" +  uploadFileUrl + "\" title=\""+uploadName+"\">" + uploadName + "</a>";
                preview .append( isEmpty(s=m.get("uploadfile")) ? "" : link );
            }
            else
            {
                s = "";
            }

            m.put("readonly", readOnly);
            m.put("preview", preview.toString());
            m.put("sequencepreview", preview.toString());
        }

        return m;
    }
}
