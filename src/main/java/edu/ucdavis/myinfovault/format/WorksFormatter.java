/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: WorksFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class WorksFormatter extends PreviewBuilder implements RecordFormatter
{
    /**
     * A formatter for works.
     * @param names An Iterable list of names to highlight in the title. An empty list is allowed, <code>null</code> is not.
     */
    public WorksFormatter(Iterable<String> names)
    {
        this.add( new TitleFormatter(PERIOD, PUB_GROUP_JOURNALS) )
            .add( new AuthorFormatter(PERIOD, "creators") )
            .add( new URLFormatter(URLFormatter.PreviewType.WORKS).add(new QuoteCleaner("link")))
            .add( new SpanClassFormatter("worktypedescription", "typefield") )
            ;
        //this.addRequiredField("worktypeid"); // Don't have to do this now that we got the 'fieldKey' right in "buildPreview" below.
    }

    public WorksFormatter()
    {
        this(null);
    }

    @Override
    public String buildPreview(Map<String, String> m)
    {

        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder subpreview = new StringBuilder();

        preview.append(insertValue("year", m.get("workyear"), "<div class=\"year\">", "</div>", true));

        subpreview.append(insertValue("worktypeid", m.get("worktypedescription"), "", COLON+SINGLE_SPACE))
            .append(insertValue("creators", m.get("creators"), "", SINGLE_SPACE))
            .append(insertValue("title", m.get("title"), "", SINGLE_SPACE))
            .append(insertValue("description", m.get("description"), "", SINGLE_SPACE));

        preview.append("<div class=\"subrecordentry\">")
            .append(removeTrailingComma(subpreview.toString()))
            .append("</div>")
            /*.append("<div class=\"creative-activities-links\">")*/;

        return preview.toString();
    }

    @Override
    public java.util.Map<String,String> format(java.util.Map<String,String> m)
    {
        String preview = buildPreview(m);

        m.put("preview", preview);
        m.put("sequencepreview", preview);
        return m;
    }
}
