package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * Removes HTML/XML style tags, enclosed in <code>&lt;&hellip;&gt;</code>, from a field or fields.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class TagCleaner extends FormatAdapter implements RecordFormatter
{
    protected String[] fieldList = { "" };

    public TagCleaner()
    {
        //DO NOTHING
    }

    public TagCleaner(String... fields)
    {
        this.fieldList = fields;
    }

    public void setFields(String... fields)
    {
        this.fieldList = fields;
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        for (String field : fieldList)
        {
            String s = m.get(field);
            //System.out.println("Looking at field '"+field+"' with value ["+s+"]");
            if (s != null)
            {
                s = s.replaceAll("<[^>]+>", "");
                m.put(field, s);
            }
        }

        return m;
    }
}
