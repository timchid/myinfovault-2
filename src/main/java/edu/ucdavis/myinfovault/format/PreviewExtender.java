package edu.ucdavis.myinfovault.format;

import java.util.Map;
/**
 * Provision to apply more formatting to the preview for the fields specified.
 * @author stun
 */
public class PreviewExtender extends FormatAdapter implements RecordFormatter
{
    private String[] fields;
    public PreviewExtender(String... fields)
    {
        this.fields = fields;
    }
    public Map<String, String> format(Map<String, String> m)
    {        
        //this.applySubformats(m);
        StringBuilder preview = new StringBuilder();
        preview.append(m.get("preview"));
        for(String field: fields)
        {
            preview.append( isEmpty(m.get(field)) ? "" : m.get(field) );            
        }
        m.put("preview", preview.toString());
        return m;
    }
}
