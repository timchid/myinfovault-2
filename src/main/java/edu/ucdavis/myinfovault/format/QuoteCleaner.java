package edu.ucdavis.myinfovault.format;

import java.util.Arrays;
import java.util.Map;

/**
 * Translate plain double-quote marks to HTML quote entities (&amp;quot;)
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class QuoteCleaner extends FormatAdapter implements RecordFormatter
{
    private String[] fieldList = null;


    /**
     * Create a QuoteCleaner that will clean up all fields.
     */
    public QuoteCleaner()
    {
        // allow this.fieldList to remain null
    }

    /**
     * Create a QuoteCleaner that will clean up the specified fields.
     * @param fields One or more strings specifying the field names to clean.
     */
    public QuoteCleaner(String... fields)
    {
        this.fieldList = fields;
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        Iterable<String> keys;
        if (fieldList != null) {
            keys = Arrays.asList(fieldList);
        }
        else {
            keys = m.keySet();
        }

        for (String field : keys)
        {
            String s = m.get(field);

            if (s != null)
            {
                s = s.replaceAll("\"", "&quot;");
                m.put(field, s);
            }
        }

        return m;
    }


    /**
     * This method cleans the quotes in the passed string and returns the formatted string.
     *
     * @param str String that needs to be formatted.
     * @return str - String with double-quote characters replaced.
     */
    public String format(String str)
    {
        return str == null ? str : str.replaceAll("\"", "&quot;");
    }
}
