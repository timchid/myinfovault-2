package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.Set;

public class HTMLCleaner extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        System.out.println("HTMLCleaner input map is (("+m+"))");
        Set<String> keys = m.keySet();
        System.out.println("HTMLCleaner keys are (("+keys+"))");
        for (String k : keys)
        {
            String value = m.get(k);
            if (value != null)
            {
                // FIXME: This is definitely too simplistic, but works as a proof-of-concept.
                //   The ampersand replacement catches numeric entity references,
                //   which we want to preserve, and we need skip replacement of
                //   less-than so we keep "</{0,1}su[pb]>"
                //   This also needs to be done *BEFORE* applying <span class="author">
                //   and other tags, so this will have to run prior to the existing
                //   "standard" formatters in the FormatManager.
                value = value.replaceAll("&", "&amp;");
                value = value.replaceAll("<", "&lt;");
                m.put(k, value);
            }
        }
        return m;
    }
}
