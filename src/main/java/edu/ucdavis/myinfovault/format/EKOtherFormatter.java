package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class EKOtherFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder resequencePreview = new StringBuilder();

        preview .append( insertValue("datespan", m.get("datespan"), "<span class=\"prefix\">", "</span>"));
        resequencePreview .append( insertValue("comment", m.get("comment"), SINGLE_SPACE, ""));

        if (!this.hasPunctuation(resequencePreview.toString())) {
            resequencePreview.append(PERIOD);
        }

        m.put("sequencepreview", resequencePreview.toString());
        m.put("preview", preview.append(COMMA_SPACE).append(resequencePreview).toString());

        return m;
    }
}
