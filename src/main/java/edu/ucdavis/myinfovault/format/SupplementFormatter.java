package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class SupplementFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

        preview // Only one of description or monthname should appear because
                // one or the other should always be blank, so just pretend
                // we're writing them both. If both ever show up we might need
                // some logic to choose either/or.
                .append( insertValue("description", m.get("description"), "", COMMA_SPACE))
                .append( insertValue("monthname", m.get("monthname"), "", COMMA_SPACE))
                .append( insertValue("teachingtype", m.get("teachingtype")))
                .append( insertValue("title", m.get("title"), COLON+SINGLE_SPACE, ""))
                .append( insertValue("locationdescription", m.get("locationdescription"), COMMA_SPACE, ""))
                .append( insertValue("hours", m.get("hours"), COMMA_SPACE, " hours."))
                ;
        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());

        return m;
    }
}
