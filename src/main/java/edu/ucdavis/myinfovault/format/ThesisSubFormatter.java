package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.logging.Logger;

/**
 * Format the Committee, Degree Type and Degree for the Thesis Committees.
 *
 * @author Brij Garg
 * @since MIV 2.0
 */
public class ThesisSubFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private static final String COMMITTEEMEMBER = "committeemember";
    private static final String COMMITTEECHAIR = "committeechair";
    private static final String COCHAIR = "cochair";
    private static final String DOCTORALDEGREE = "doctoraldegree";
    private static final String DOCTORATEDEGREE = "doctoratedegree";
    private static final String MASTERSDEGREE = "mastersdegree";
    private static final String DEGREEAWARDED = "degreeawarded";
    private static final String DEGREEPROGRESS = "degreeprogress";
    public static final String COMMITTEEPOSITION = "committeeposition";
    public static final String DEGREETYPEID = "degreetypeid";
    public static final String DEGREESTATUSID = "degreestatusid";

    public RecordFormatter add(RecordFormatter formatter)
    {
        return this;
    }

    @Override
    public Map<String,String> format(Map<String,String> m)
    {
        log.entering(this.getClass().getSimpleName(), "format");

        String commiteepos = "";
        if ("1".equals(m.get(COMMITTEECHAIR)))
        {
            commiteepos = "Chair";
        }
        else if ("1".equals(m.get(COMMITTEEMEMBER)))
        {
            commiteepos = "Member";
        }
        else if ("1".equals(m.get(COCHAIR)))
        {
            commiteepos = "Co-Chair";
        }
        
        m.put(COMMITTEEPOSITION, commiteepos);

        String degreeid = "";
        
        if ("1".equals(m.get(DOCTORALDEGREE)))
        {
            degreeid = "Ph.D.";
        }
        else if ("1".equals(m.get(MASTERSDEGREE)))
        {
            degreeid = "Masters";
        }
        else if ("1".equals(m.get(DOCTORATEDEGREE)))
        {
            degreeid = "Doctorate";
        }
        m.put(DEGREETYPEID, degreeid);

        String degawarded = "";
        if ("1".equals(m.get(DEGREEPROGRESS)))
        {
            degawarded = "In Progress";
        }
        if ("1".equals(m.get(DEGREEAWARDED)))
        {
            degawarded = "Awarded";
        }
        else if ("1".equals(m.get(DEGREEPROGRESS)))
        {
            degawarded = "In Progress";
        }
        m.put(DEGREESTATUSID, degawarded);

        log.exiting(this.getClass().getName(), "format");
        return m;
    }
}
