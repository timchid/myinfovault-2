package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class AdministrativeActivitiesFormatter extends PreviewBuilder
{
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder yearfield = new StringBuilder();
        String startYear,endYear;

        // All the fields used in a preview are required - shout out if any are missing

        /*if ( ! isEmpty(startYear=m.get("startyear")) )
        {
            yearfield.append(startYear);
        }
        else
        {
            missing(" START YEAR MISSING ");
        }
        if ( ! isEmpty(endYear=m.get("endyear")) )
        {
            yearfield .append("-");
            yearfield.append(endYear);
        }*/

        startYear = m.get("startyear");
        endYear = m.get("endyear");
        yearfield.append(insertValue("startyear", startYear));
        yearfield.append(DASH);
        yearfield.append(insertValue("endyear", endYear));
        m.put("year", yearfield.toString());

        preview .append( insertValue("title", m.get("title"), SINGLE_SPACE, ""))
                .append( insertValue("percenteffort", m.get("percenteffort"), COMMA_SPACE, "% effort"))
                ;

        return preview.toString();
    }
}
