/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AnnotationFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;
import edu.ucdavis.myinfovault.MIVConfig;


/**
 * Creates a pseudo-field named "annotationText" in a provided record map.
 * @author Venkat Vegesna
 */
public class AnnotationFormatter extends FormatAdapter
{
    private static final Collection<String> addedFields = Arrays.asList(new String[] {"annotationText"});

    private final String recType;
    private final String section;
    private final Long packetId;
    private final String annotationType;

    private static final Map<String, Map<String, String>>sectionidByNameMap =
        MIVConfig.getConfig().getMap("sectionidbyname");

    Map<String, String> sectionMap;
    Map<String, Annotation> annotationRecordMap = null;

    public AnnotationFormatter(String recType, String section, Long packetId, String annotationType, Map<String, Annotation> annotationRecordMap )
    {
        this.recType = recType;
        this.section = section;
        this.packetId = packetId;
        this.annotationType = annotationType;
        this.annotationRecordMap = annotationRecordMap;
        // Get the sectionMap for this record type
        this.sectionMap =  sectionidByNameMap.get(section);
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);
        String recordID = m.get("id");
        String annotationText = "";

        if (!isEmpty(recordID))
        {
            int recID = new Integer(recordID).intValue();

            // Find annotations associated with this record
            if (annotationRecordMap != null && !annotationRecordMap.isEmpty())
            {
                for (String key : annotationRecordMap.keySet())
                {
                    // Section id is null/0 for older publication and presentation records so of the record id matches flag it
                    final String sectionBaseTable = annotationRecordMap.get(key).getSectionBaseTable();
                    String annotation = annotationRecordMap.get(key).getNotation();
                    if (annotationRecordMap.get(key).getRecordId() == Integer.parseInt(recordID) &&
                            sectionBaseTable == null)
                    {
                        // Check master ([M]) or packet specific ([P]) annotation
                        annotationText = (annotationRecordMap.get(key).getPacketId() != 0 ? "[P]" : "") +
                                         (annotation != null && (annotation.contains("+") || annotation.contains("@")) ? "[M]" : "");
                        break;
                    }
                    // If the section id is not null, then we must match the section and record
                    else if (sectionBaseTable != null &&
                            annotationRecordMap.get(key).getRecordId() == Integer.parseInt(recordID) &&
                            sectionBaseTable.equals(sectionMap.get("sectionbasetable")))
                    {
                        // As so as we find one, get out
                        annotationText = (annotationRecordMap.get(key).getPacketId() != 0 ? "[P]" : "") +
                                         (annotation != null && (annotation.contains("+") || annotation.contains("@")) ? "[M]" : "");
                        break;
                    }
                }
            }

            String link = m.get("link");
            StringBuilder annotationLink = new StringBuilder();
            if (link != null
                    && (link.indexOf(URLFormatter.PUBLICATIONLINKLABEL) > 0 ||
                            link.indexOf(URLFormatter.PATENTLINKLABEL) > 0 ||
                            link.indexOf(URLFormatter.WORKSLINKLABEL) > 0 ||
                            link.indexOf(URLFormatter.EVENTSLINKLABEL) > 0 ||
                            link.indexOf(URLFormatter.REVIEWSLINKLABEL) > 0)
                )
            {
                annotationLink.append("|&nbsp;");
            }
            else
            {
                if ( !(new ArrayList<String>(Arrays.asList("works", "events", "reviews", "publicationevents"))).contains(this.recType) )
                {
                    annotationLink.append("<br>");
                }
            }

            annotationLink.append("<a href=\"/miv/Annotations?")
                          .append("annotationtype=").append(annotationType)
                          .append("&amp;type=").append(recType)
                          .append("&amp;section=").append(section)
                          .append("&amp;recid=").append(recID)
                          .append("&amp;packetid=").append(packetId).append('"');

            annotationLink.append(" title=\"Add or Edit Annotations for this entry\">Edit ")
                          .append(packetId.equals("0") ? "Master" : "Packet ")
                          .append(" Annotation</a>");

            m.put("annotationLink", annotationLink.toString());
        }

        m.put("annotationText", annotationText);
        return m;
    }

    @Override
    public Iterable<String> getAddedFields()
    {
        return addedFields;
    }
}
