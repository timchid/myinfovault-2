package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class StrongTagRenderer extends SimpleTagRenderer
{
    
    public StrongTagRenderer(Node n)
    {
        super(n);
    }

}
