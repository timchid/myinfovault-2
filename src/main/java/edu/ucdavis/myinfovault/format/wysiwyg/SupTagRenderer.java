package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class SupTagRenderer extends SimpleTagRenderer
{
    public SupTagRenderer(Node n)
    {
        super(n);
    }
}
