package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.TagAttributeRenderer;

public class PTagRenderer extends TagAttributeRenderer
{
    public PTagRenderer(Node n)
    {
        super(n, "align","style");
    }
}
