package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class UTagRenderer extends SimpleTagRenderer
{
    public UTagRenderer(Node n)
    {
        super(n);
    }
}
