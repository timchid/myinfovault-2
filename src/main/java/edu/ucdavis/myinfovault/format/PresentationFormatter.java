/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PresentationFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class PresentationFormatter extends FormatAdapter implements RecordFormatter
{
    private static final Calendar calendar = Calendar.getInstance();
    private static final DateFormat df = new SimpleDateFormat("MMMM");

    public PresentationFormatter()
    {
        this.add( new TitleFormatter(COMMA, PUB_GROUP_PRESENTATIONS) );
    }


    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

        String month = m.get("monthname");
        String monthID = m.get("monthid");

        if (month == null)
        {
            String monthname = "";
            if (monthID != null)
            {
                int monthnumber = Integer.parseInt(monthID);
                if (monthnumber > 0)
                {
                    calendar.set(Calendar.MONTH, monthnumber-1);
                    monthname = df.format(calendar.getTime());
                }
            }

            m.put("monthname", monthname);
        }

        if ("none".equalsIgnoreCase(month) || "".equals(month) || month == null)
        {
            m.put("monthname", "");
            m.put("day", "");
        }
        else if ("".equals(m.get("day")) || m.get("day") == null)
        {
            m.put("monthname", m.get("monthname")+", ");
        }

        m.put("year", insertValue("year", m.get("year")));

        preview // Only one of description or monthname should appear because
                // one or the other should always be blank, so just pretend
                // we're writing them both. If both ever show up we might need
                // some logic to choose either/or.
                //.append( insertValue("description", m.get("description"), "", " - "))
                .append( insertValue("monthname", m.get("monthname")))
                .append( insertValue("day", m.get("day"), " ", ", "))
                .append( insertValue("title", m.get("title")))
                .append( insertValue("locationdescription", m.get("locationdescription"), SINGLE_SPACE, ""))
                //.append( insertValue("hours", m.get("hours"), COMMA_SPACE, " hours."))
                ;

        String previewString = removeTrailingComma(preview.toString());

        m.put("preview", previewString);
        m.put("sequencepreview", previewString);
        return m;
    }
}
