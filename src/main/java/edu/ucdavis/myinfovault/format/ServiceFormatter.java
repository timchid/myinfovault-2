package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class ServiceFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

        preview .append( insertValue("role", m.get("role"), "", COMMA_SPACE))
                .append( insertValue("description", m.get("description")))
                ;
        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());

        return m;
    }
}
