/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PatternFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;

/**
 * Applies highlighting patterns to a record, replacing the original field contents.
 * @author Stephen Paulsen
 */
public class PatternFormatter extends FormatAdapter
{

    private List<HighlightPattern> patterns = new LinkedList<HighlightPattern>();

    public PatternFormatter(DocumentIdentifier docIdentifier, HighlightPattern... patterns)
    {
        for (HighlightPattern p : patterns)
        {
            if (p.getDocumentType() != null && p.getDocumentType() == docIdentifier)
            {
                this.patterns.add(p);
            }
        }
    }


    public PatternFormatter(DocumentIdentifier docIdentifier, Iterable<HighlightPattern> patterns)
    {
        for (HighlightPattern p : patterns)
        {
            if (p.getDocumentType() != null && p.getDocumentType() == docIdentifier)
            {
                this.patterns.add(p);
            }
        }
    }

    @Override
    public Map<String, String> format(final Map<String, String> m)
    {
        final List<String> formatFields = MIVConfig.getConfig().getFormatFields();
        for (String key : m.keySet())
        {
            if (!formatFields.contains(key)) continue;

            for (HighlightPattern p : patterns)
            {
                if (key.equalsIgnoreCase(p.getFieldName()))
                {
                    String val = m.get(key);
                    if (!isEmpty(val))
                    {
                        m.put(key, p.tag(val));
                    }
                }
            }
        }
        return m;
    }
}
