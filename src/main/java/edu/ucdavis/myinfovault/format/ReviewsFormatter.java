/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReviewsFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * A formatter for Reviews.
 * TODO: As mentioned in https://jira.ucdavis.edu/jira/browse/MIV-4132
 *       this class could use some work to reduce code duplication in
 *       the "else" clauses, and take advantage of the automatic flagging
 *       of required fields that is done by "insertValue()" when the
 *       record-type configuration properties are set up correctly.
 *       This also hard-codes "venue" as a required field; if that is
 *       changed, then changing mivconfig.properties will require this
 *       code to also be changed. That shouldn't happen.
 */
public class ReviewsFormatter extends PreviewBuilder implements RecordFormatter
{
    private static final int PRINT_REVIEW = 1;
    private static final int MEDIA_REVIEW = 2;

    /**
     * Create a formatter
     * @param names An Iterable list of names to highlight in the title. An empty list is allowed, <code>null</code> is not.
     */
    public ReviewsFormatter(Iterable<String> names)
    {
        this.add( new TitleFormatter(PERIOD, "") )
            .add( new AuthorFormatter(PERIOD, "reviewer") )
            .add( new PublicationNameFormatter() )
            .add( new VolumeIssueFormatter() )
            .add( new CityProvinceCountryFormatter() )
            .add( new URLFormatter(URLFormatter.PreviewType.REVIEWS).add(new QuoteCleaner("link")))
            ;
    }

    public ReviewsFormatter()
    {
        this(null);
    }

    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder subpreview = new StringBuilder();

        // Check the category of the review and display the preview accordingly.
        int category = 0;
        String categoryId = m.get("categoryid");

        if (hasText(categoryId)) // Will categoryId EVER be missing??  It's an "int not null" in the database.
        {
            try {
                category = Integer.parseInt(categoryId);
            }
            catch (NumberFormatException e) {
                // ignore it, allow category to remain == 0 (This can't really happen, it's not user entered data)
            }
        }

        if (category == PRINT_REVIEW)
        {
            // THIS is parially re-worked to be a bit more like the PeriodicalFormatter.
            preview
                .append(insertValue("date", m.get("printreviewyear"), "<div class=\"year\">", "</div>", true));

            subpreview.append(insertValue("reviewer", m.get("reviewer"), SINGLE_SPACE, ""))
                .append(insertValue("title", m.get("title"), SINGLE_SPACE, ""))
                .append(insertValue("description", m.get("description"), SINGLE_SPACE, PERIOD))
                .append(insertValue("publication", m.get("publication"), SINGLE_SPACE, COMMA))
                .append(insertValue("volume", m.get("VolumeIssue"), SINGLE_SPACE, ""));

            if (hasText(m.get("pages")))
            {
                if (hasText(m.get("VolumeIssue")))
                {
                    subpreview.append(insertValue("pages", m.get("pages"), SINGLE_SPACE, PERIOD));
                }
            }
            else
            {
                char lastchar = subpreview.charAt(subpreview.length()-1);

                if (lastchar == ',')
                {
                    subpreview.deleteCharAt(subpreview.length()-1);
                    String previewString = subpreview.toString();
                    if (! previewString.matches(".*\\.</span>$"))
                    {
                        subpreview.append(PERIOD);
                    }
                }
            }

            subpreview
                .append(insertValue("publisher", m.get("publisher"), SINGLE_SPACE, COMMA))
                .append(insertValue("city", m.get("CityProvinceCountry"), SINGLE_SPACE, "", true));

            preview
                .append("<div class=\"subrecordentry\">")
                .append(removeTrailingComma(subpreview.toString()))
                .append("</div>")
                /*.append("<div class=\"creative-activities-links\">")*/;

        }
        else if (category == MEDIA_REVIEW)
        {
            preview
                .append(insertValue("date", m.get("reviewdate"), "<div class=\"daterange\">", "</div>", true));

            subpreview
                .append(insertValue("reviewer", m.get("reviewer"), SINGLE_SPACE, ""))
                .append(insertValue("venue", hasText(m.get("venue")) ? m.get("venue") : "<span class=\"ERR_MISSING\">Missing: Review Venue</span>",
                                    SINGLE_SPACE, PERIOD))
                .append(insertValue("description", m.get("description"), SINGLE_SPACE, PERIOD))
                .append(insertValue("city", m.get("CityProvinceCountry"), SINGLE_SPACE, "", true));

           preview
                .append("<div class=\"subrecordentry\">")
                .append(removeTrailingComma(subpreview.toString()))
                .append("</div>")
                /*.append("<div class=\"creative-activities-links\">")*/;
        }
        else
        {
            // A valid category ID wasn't found.
            // category is still == 0 either because categoryid wasn't in the map or didn't parse to an int,
            // or category is a number we don't know about ... new one added but forgot to update this code?
            preview
                .append(insertValue("date", m.get("reviewdate"), "<div class=\"daterange\">", "</div>", true));

            subpreview
                .append(insertValue("reviewer", m.get("reviewer"), SINGLE_SPACE, ""))
                .append(SINGLE_SPACE+"<span class=\"ERR_MISSING\">Missing: Review Details</span>");

            preview
                .append("<div class=\"subrecordentry\">")
                .append(removeTrailingComma(subpreview.toString()))
                .append("</div>")
                /*.append("<div class=\"creative-activities-links\">")*/;
        }

        final String previewString = preview.toString();

        m.put("preview", previewString);
        m.put("sequencepreview", previewString);

        return previewString;
    }

    @Override
    public java.util.Map<String,String> format(java.util.Map<String,String> m)
    {
        String preview = buildPreview(m);

        m.put("preview", preview);
        m.put("sequencepreview", preview);
        return m;
    }
}
