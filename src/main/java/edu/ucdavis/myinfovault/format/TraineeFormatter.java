package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * The TraineeFormatter adds a "preview" field and <strong>removes</strong> the "year" field.
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class TraineeFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder resequencePreview = new StringBuilder();

        // Name, Type, and Year are required fields and should always be present.
        // Flag them if the data is missing.
        preview .append( insertValue("name", m.get("name"), "<span class=\"prefix\">", "</span>,"));
        resequencePreview.append( insertValue("degree", m.get("degree"), SINGLE_SPACE, COMMA))
                .append( insertValue("type", m.get("type"), "", COMMA_SPACE))
                .append( insertValue("year", m.get("year")))
                .append( insertValue("position", m.get("position"), COMMA_SPACE, ""))
                ;
        m.put("sequencepreview", resequencePreview.toString());
        m.put("preview", preview.append(resequencePreview).toString());
        // Eliminate 'year' because it's not an actual date to sort on, and we don't want
        // it to display in the reserved year area.
        m.remove("year");

        return m;
    }
}
