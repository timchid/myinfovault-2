package edu.ucdavis.myinfovault.format;

import java.util.Arrays;
import java.util.Collection;

/**
 * This class simplifies implementing the many formatters that build a "preview" line.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public abstract class PreviewBuilder extends FormatAdapter implements RecordFormatter
{
    private static final Collection<String> addedFields = Arrays.asList(new String[] {"preview"});

    @Override
    public java.util.Map<String,String> format(java.util.Map<String,String> m)
    {
        String preview = removeTrailingComma(buildPreview(m));

        m.put("preview", preview);
        m.put("sequencepreview", preview);
        m.put("year", insertValue("year", m.get("year")));

        return m;
    }

    protected abstract String buildPreview(java.util.Map<String,String> m);

    @Override
    public Iterable<String> getAddedFields()
    {
        return addedFields;
    }
}
