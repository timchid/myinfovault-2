package edu.ucdavis.myinfovault.designer;


public class UserUploadDisplayState extends BaseDisplayState
{
    static final String tableName = "UD";

    public UserUploadDisplayState(String cbName, String recID, String recType, String displayFieldName, String displayFlag)
    {
        super(cbName, recID, recType, displayFieldName, displayFlag);
    }

    public String toString()
    {
        return super.toString() + "," + tableName;
    }
}
