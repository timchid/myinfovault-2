package edu.ucdavis.myinfovault.designer;

import java.util.Arrays;
import java.util.List;

import edu.ucdavis.myinfovault.data.DocumentIdentifier;

/**
 * This is simple java class which holds the (the what?) related to particular document id for design extensive page.
 *
 * @author venkat
 *
 */
public class DesignDocument
{
    private String name; // name is document description.
    private String annotationLegend = "";
    private DocumentIdentifier documentIdentifier = DocumentIdentifier.INVALID;
    private static final List<DocumentIdentifier> annotatedDocuments = Arrays.asList(DocumentIdentifier.PUBLICATIONS,
    																	DocumentIdentifier.EXTENDING_KNOWLEDGE,
    																	DocumentIdentifier.EVALUATIONS,
    																	DocumentIdentifier.CREATIVE_ACTIVITIES,
    																	DocumentIdentifier.SERVICE);


    List<DisplayOptions> displayOptionsList;

    public DesignDocument(int id, String name, List<DisplayOptions> displayOptionsList)
    {
        this(DocumentIdentifier.convert(id), name, displayOptionsList);
    }

    public DesignDocument(DocumentIdentifier documentIdentifier, String name, List<DisplayOptions> displayOptionsList)
    {
        this.name = name;
        this.displayOptionsList = displayOptionsList;
        this.documentIdentifier = documentIdentifier;

		if (annotatedDocuments.contains(documentIdentifier))
        {
            annotationLegend = "<strong>[M] = Master Annotation Present, [P] = Packet Annotation Present</strong>";
        }
    }

    public List<DisplayOptions> getDisplayOptionsList()
    {
        return displayOptionsList;
    }

    public String getName()
    {
        return name;
    }

    public String getAnnotationLegend()
    {
        return annotationLegend;
    }

    public void setAnnotationLegend(String annotationLegend)
    {
        this.annotationLegend = annotationLegend;
    }

    public DocumentIdentifier getDocumentIdentifier()
    {
        return this.documentIdentifier;
    }
}
