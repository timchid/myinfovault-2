
package edu.ucdavis.myinfovault;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;


/**
 * Show HTTP Session info.<br>
 * This is to help in debugging session related problems.<br>
 * It is currently configured (2014-07-17) to live at
 * <a href="https://localhost/miv/info">https://[machine].ucdavis.edu/miv/info</a>
 */
public class AnHttpSessionInfo extends edu.ucdavis.mw.myinfovault.web.MIVServlet
{
    private static final long serialVersionUID = 1L;

    private String ACCESSDENIED_JSP = "/jsp/accessdenied.jsp";

    private ServletConfig config;
    private ServletContext context;

    @Override
    public void init(ServletConfig c) throws ServletException
    {
        super.init(c);
        this.config = c;
        this.context = c.getServletContext();
    }

    @Override
    public void doGet(final HttpServletRequest req, final HttpServletResponse res) throws IOException, ServletException
    {
        this.doPost(req, res);
    }

    @Override
    public void doPost(final HttpServletRequest req, final HttpServletResponse res) throws IOException, ServletException
    {
        boolean failtest = false;
        MIVSession s = super.getSession(req, res);
        if (! s.getUser().getUserInfo().getPerson().hasRole(MivRole.SYS_ADMIN) || failtest ) {
            super.dispatch(ACCESSDENIED_JSP, req, res);
            return;
        }

        ServletOutputStream out = null;
        try {
            res.setHeader("Content-Type", "text/html; charset=UTF-8");
            out = res.getOutputStream();

            prologue(out);

            out.println("<p>Context Path: "+req.getContextPath()+"</p>");

            showContext(out);
            showConfig(out);

            // Show Session Objects
            showSessionObjects(req, out);

            // Show request/form parameters
            showParameters(req, out);

            // Show System Properties
            showProperties(out);

            epilogue(out);
        }
        catch (final IOException ioe) {
            ioe.printStackTrace(System.out);
        }
    }

    /**
     * @param out
     * @throws IOException
     */
    private void showContext(ServletOutputStream out) throws IOException
    {
        out.println("<div id=\"context\">");
        out.println(h2("Servlet Context"));

        out.println("<p>");
        out.println("Server: " + context.getServerInfo() + "<br>");
        out.println(java.net.InetAddress.getLocalHost().getHostName() + " : " +
                    java.net.InetAddress.getLocalHost().getHostAddress() + "<br>");
        out.println("</p>");

        out.println(h3("Init Parameters"));
        out.println("<table border=\"1\" cellpadding=\"4\" cellspacing=\"0\">");
        out.println(" <thead>\n  <tr><th>Init Param</th><th>Value</th></tr>\n </thead>");
        out.println(" <tbody>");

        Enumeration<String> pnames = context.getInitParameterNames();
        while (pnames.hasMoreElements())
        {
            String pname = pnames.nextElement();
            String pval = context.getInitParameter(pname);
            out.println("  <tr><td>" + pname + "</td><td>" + pval + "</td></tr>");
        }

        out.println(" </tbody>\n</table>");

        out.println("</div>");
    }


    /**
     * @param out
     * @throws IOException
     */
    private void showConfig(ServletOutputStream out) throws IOException
    {
        out.println("<div id=\"config\">");
        out.println(h2("Servlet Config"));

        out.println(this.config.toString());
        out.println("</div>");
    }


    /**
     * @param req
     * @param out
     * @throws IOException
     */
    private void showSessionObjects(final HttpServletRequest req, ServletOutputStream out) throws IOException
    {
        final HttpSession ses = req.getSession(false);
        if (ses != null)
        {
            out.println(h2("Session Objects Are..."));

            out.println(table("Object", "Value"));

            final Enumeration<String> e = ses.getAttributeNames();
            while (e.hasMoreElements())
            {
                final String k = e.nextElement();//.toString();
                final String v = ses.getAttribute(k).toString();
                out.println("  <tr><td>"+k+"</td><td>"+v+"</td></tr>");
            }

            out.println(" </tbody>\n</table>");
        }
        else // no HttpSession! This should never happen!
        {
            out.println("<h1>Impossible Error!</h1>");
            out.println("<p>No HttpSession was found in "+getClass().getSimpleName()+"!");
            out.println("Please send email to the MIV Supervisor.</p>");
        }
    }


    /**
     * @param req
     * @param out
     * @throws IOException
     */
    private void showParameters(final HttpServletRequest req, ServletOutputStream out) throws IOException
    {
        out.println(h2("Parameters Are..."));

        out.println(table("Parameter", "Key from Map", "Value from getParameter"));

        final Map<String,String[]> params = req.getParameterMap();
        for (String k : params.keySet())
        {
            Object v1 = params.get(k);
            String[] v2 = req.getParameterValues(k);
            out.print("  <tr><td>" + k.toString() + "</td><td>" + v1.toString() + "</td><td>");
            for (int i = 0; i < v2.length; i++)
            {
                //+v2.toString()+"</td></tr>");
                if (i > 0) out.print(", ");
                out.print(v2[i]);
            }
            out.println("</td>");
        }
        out.println("</tr>\n  </tbody>\n</table>");
    }


    /**
     * @param out
     * @throws IOException
     */
    private void showProperties(ServletOutputStream out) throws IOException
    {
        final Properties sysprops = System.getProperties();
        out.println(h2("System Properties Are..."));

        out.println(table("Object", "Value"));

        final Set<Object> keys = sysprops.keySet();
        for (final Object o : keys)
        {
            final String k = o.toString();
            String v = sysprops.getProperty(k);
            if (v == null || v.length() == 0) v = "&nbsp;";
            out.println("  <tr><td>"+k+"</td><td>"+v+"</td></tr>");
        }
        out.println(" </tbody>\n</table>");
    }


    void prologue(final ServletOutputStream out)
    {
        try {
            out.println("<!DOCTYPE html>");
            out.println("<html>\r\n<head>");
            out.println(" <title>Show Session Attributes</title>");
            out.println(" <link rel='stylesheet' type='text/css' href='/miv/css/miv.css'>");
            out.println(" <style type=\"text/css\">body { padding:1.5em; }</style>");
            out.println("</head>\r\n<body>");
        }
        catch (final IOException e) {
            e.printStackTrace();
        }
    }

    void epilogue(final ServletOutputStream out)
    {
        try {
            out.println("</body>\r\n</html>");
        }
        catch (final IOException e) {
            e.printStackTrace();
        }
    }


    private String h2(String heading)
    {
        return "<h2>" + heading + "</h2>";
    }

    private String h3(String heading)
    {
        return "<h3>" + heading + "</h3>";
    }

    /**
     * Create a String that starts an HTML <code>&lt;table&gt;</code> with the given headings.
     * @param headings any number of String headings to enclose in <code>&lt;th&gt;</code> tags.
     * @return The constructed String, ending with the table body <code>&lt;tbody&gt;</code> open.
     */
    private String table(String... headings)
    {
        StringBuilder sb = new StringBuilder("<table border=\"1\" cellpadding=\"4\" cellspacing=\"0\">");

        sb.append(" <thead>\n  <tr>");
        for (String heading : headings)
        {
            sb.append("<th>").append(heading).append("</th>");
        }
        sb.append("</tr>\n </thead>\n <tbody>");

        return sb.toString();
    }
}
