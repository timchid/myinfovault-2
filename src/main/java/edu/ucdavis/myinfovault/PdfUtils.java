package edu.ucdavis.myinfovault;
/*
 * Created on Nov 21, 2005
 *
 */

/**
 * @author pproctor
 *
 * A class to embody generic Pdf Utilities for the MyInfoVault Application
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedList;

//import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import edu.ucdavis.myinfovault.document.PDFConcatenator;

//import com.corda.CordaEmbedder;

public class PdfUtils
{

    public static void convertPdf(HttpServletResponse res, String inAction, String htmfile, String pdffile, String cvPath)
        throws IOException
    {
        //ServletOutputStream out = res.getOutputStream();
        //res.setContentType("text/html;charset=UTF-8");
        String pdfPath="";
        String htmPath="";
        if (inAction.equals("f"))
        {
            pdfPath=cvPath+"CVfinal"+File.separator+"pdf"+File.separator;
            htmPath=cvPath+"CVfinal"+File.separator+"html"+File.separator;
        }
        else
        {
            pdfPath=cvPath+"pdf"+File.separator;
            htmPath=cvPath+"html"+File.separator;
        }
        File htmlFile = new File(htmPath, htmfile);
        // only convert if the file exists
        if (htmlFile.exists())
        {
            /**
	      	CordaEmbedder myImage = new CordaEmbedder();
	      	myImage.externalServerAddress= "http://psl-116.ucdavis.edu:2001";
	      	myImage.internalCommPortAddress = "http://psl-116.ucdavis.edu:2002";
	      	myImage.loadDoc(htmPath+htmfile);
	      	myImage.saveToAppServer(pdfPath, pdffile);**/
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec("/usr/bin"+File.separator+"htmldoc --webpage --links --bodyfont Arial -f "+pdfPath+pdffile+" "+htmPath+htmfile);
            InputStream stderr=p.getErrorStream();
            InputStreamReader isr = new InputStreamReader(stderr);
            BufferedReader br = new BufferedReader(isr);
            while ((br.readLine()) != null)
                ; //throw lines away

        }
        else
        {
            // If there is no html file, delete any old pdf file that may have been converted
            File pdfFile = new File(pdfPath, pdffile);
            if (pdfFile.exists())
            {
                pdfFile.delete();
            }
        }
    }


    public static void concatPDF(String fileList, String outputFile)
    {
        
        Collection<String> fileset = java.util.Arrays.asList(fileList.split(" "));
        LinkedList<String>files = new LinkedList<String>(fileset); 
        
        PDFConcatenator concatenator = new PDFConcatenator();
        if (!concatenator.concatenateFiles(files, new File(outputFile)))
        {
            System.out.println("Error concatenating PDF's!");
        }

/*        
        try {
            PDFConcat concat = new PDFConcat(fileList, outputFile);
            Thread concatThread = new Thread(concat);
            concatThread.start();
            concatThread.join(20000);

            if (concatThread.isAlive())
            {
                concatThread.interrupt();
            }
        }
        catch (Exception e) {
            System.out.println("Error concatenating PDF's!:"+e);
        }
*/
 
    }

    // Concat PDF is made to concatentate a generated PDF with uploaded PDF's
    // It is not intended to concat any arbitrary list of PDF's
    public static void concatUploadedPdf(String uid, String tableName, String sourcePdf, String inAction, String cvPath)
    {
        String pathString ="";
        if (inAction.equals("f"))
        {
            pathString = cvPath + "CVfinal" + File.separator + "pdf" + File.separator;
        }
        else
        {
            pathString = cvPath + "pdf" + File.separator;
        }
        String pdfOutput = pathString + "tempPDF" + uid + ".pdf";
        File oldFile = new File(pathString + sourcePdf);
        String pdfInput;
        if (oldFile.exists())
        {
            pdfInput = " " + pathString + sourcePdf;
            String dbPdf = "";
            try {
                // Connect to the Database and get additional PDF inputs
                String query = "Select * FROM "+tableName+" where SSN="+uid+" and PkPrt='y' AND UploadName IS NOT NULL ORDER BY Seq";
                DataSource ds = MIVConfig.getConfig().getDataSource();
                Connection conn = ds.getConnection();
                Statement sqlStmt = conn.createStatement();
                ResultSet rSet = sqlStmt.executeQuery (query);
                while (rSet.next())
                {
                    dbPdf = rSet.getString("UploadName");
                    pdfInput = pdfInput + " " +cvPath+dbPdf;
                }
                sqlStmt.close();
                conn.close();

                // Concatenate the list of PDF's
                concatPDF(pdfInput, pdfOutput);

                // Copy the new concatenated pdf file to the location of the original
                File newFile = new File(pdfOutput);
                // First delete the old source file
                oldFile.delete();
                // Now recreate
                oldFile = new File(pathString + sourcePdf);
                //Now copy the contents of the new file to the same file path you just deleted
                InputStream in = new FileInputStream(newFile);
                OutputStream out = new FileOutputStream(oldFile);  
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0)
                {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                // The original file should now contain the additional PDF's
                // Delete the temp file
                newFile.delete();
            }
            catch (Exception e) {
                System.out.println("PDF Concat Exception: "+e);
            }
        }
        else
        {
            // don't include old file in the concatenation
            pdfInput = " ";
            String dbPdf = "";
            try {
                // Connect to the Database and get additional PDF inputs
                String query = "Select * FROM "+tableName+" where SSN="+uid+" and PkPrt='y' AND UploadName IS NOT NULL ORDER BY Seq";
                DataSource ds = MIVConfig.getConfig().getDataSource();
                Connection conn = ds.getConnection();
                Statement sqlStmt = conn.createStatement();
                ResultSet rSet = sqlStmt.executeQuery (query);
                while (rSet.next())
                {
                    dbPdf = rSet.getString("UploadName");
                    pdfInput = pdfInput + " " +cvPath+dbPdf;
                }
                sqlStmt.close();
                conn.close();

                // Concatenate the list of PDF's
                concatPDF(pdfInput, pdfOutput);

                // Copy the new concatenated pdf file to the location of the original
                File newFile = new File(pdfOutput);
                // First delete the old source file
                oldFile.delete();
                // Now recreate
                oldFile = new File(pathString + sourcePdf);
                //Now copy the contents of the new file to the same file path you just deleted
                InputStream in = new FileInputStream(newFile);
                OutputStream out = new FileOutputStream(oldFile);  
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0)
                {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                // The original file should now contain the additional PDF's
                // Delete the temp file
                newFile.delete();
            }
            catch (Exception e) {
                System.out.println("PDF Concat Exception: "+e);
            }	
        }
    }
}
