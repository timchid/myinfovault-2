/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FormFilter.java
 */

package edu.ucdavis.myinfovault;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.mw.myinfovault.util.Duration;

/**<p>
 * Mitigate multiple form submits and cross-site request forgery (CSRF) attacks.</p>
 * <p>
 * This filter injects a hidden input field into forms when it finds any &lt;form&gt;
 * tags in a GET request. It then looks for those injected token values when it
 * sees a POST, and rejects processing the POST if there is anything "wrong" with
 * the token.</p>
 * <p>
 * If the token is seen twice in a short period it's considered a <em>double submit</em>
 * from the user either double-clicking on the submit button or pressing submit again
 * during a processing delay.  The second (and more) POST will be ignored.
 * After a timeout period the token will be accepted if it's seen again.</p>
 * <p>
 * If the token is missing, or does not have a value of a token that was handed out,
 * the POST is rejected and an HTTP 403 "Forbidden" response is issued. If the token
 * is too old this could be a replay attack; if it's too new this could be a robot
 * doing submissions (i.e. not enough time has passed between when the token was
 * created and when it was received for a human to have filled out the form) and
 * these are also rejected.</p>
 * <p>
 * Things that could be made configurable include</p><ul>
 * <li>Name used for the form field.</li>
 * <li>Max age of the token after which it's rejected.</li>
 * <li>Min age of the token before it's accepted.</li>
 * <li>Elapsed time between multiple sightings of the token before it's treated as a separate submission.</li>
 * <li>Cleanup threshold, aka how many tokens before the token store gets cleaned up.</li>
 * </ul>
 * @author Stephen Paulsen
 */
public class FormFilter extends BaseFilter
{
    /** Name of the &lt;init-param> to specify a configuration properties file. */
    private static final String CONFIG_FILE_PARAM = "ConfigurationFile";
    private static final String TOKEN_NAME_PARAM = "TokenName";

    private static final long DEFAULT_MAX_AGE = new Duration(120, TimeUnit.MINUTES).as(TimeUnit.MILLISECONDS);
    private static final long DEFAULT_MIN_AGE = 0L;
    private static final int MIN_ELAPSED_REPLAY = 3500; // aka repeat-gap? // 3500 ms == 3.5 seconds
    private static final int DEFAULT_CLEANUP_THRESHOLD = 5000; // clean the token store when it gets this full

    private TokenGenerator generator;
    /** Generate secure random numbers to use in the form token */
    private SecureRandom rand = new SecureRandom();
    private Map<String,Token> tokenStore = new HashMap<String,Token>();
//  private Map<String,Token> tokenStore = new HashMap<>();  // Java7 style.

//    private boolean debug = false;//true;


    /**
     * Amount of time that must pass between issuing a token and accepting it back in a POST.
     * A person will take some time to fill in and submit a form, but a bot can do it very quickly.
     * Setting a minimum age can prevent bots from submitting forms.
     */
    private long minAge = DEFAULT_MIN_AGE;

    /**
     * Maximum time that can pass before a form is considered abandoned.
     * A person might leave a form up for a long time. Be careful with this, the form
     * can just appear "dead" after this time passes because the POST will just be
     * ignored.
     */
    private long maxAge = DEFAULT_MAX_AGE;

    /**
     * Sweep old tokens out of the token store when it gets this full.
     */
    private int cleanupThreshold = DEFAULT_CLEANUP_THRESHOLD;

    /**
     * Name to use for the form token hidden input field that is injected into forms.
     */
    private String formTokenName = "formTokenId";



    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig config) throws ServletException
    {
        generator = new TokenGenerator();
        // TODO: Also read the init params for min-age, max-age, and repeat-gap
        // Other possible things that could be configurable:
        //  - What to do for condition X ==> ignore the POST, redirect, return a non-200 response
        //     400 Bad Request
        //     403 Forbidden           ** we're using 403 below in filterPost()
        //     408 Request Timeout
        //     409 Conflict  <--- seems good
        //    Conditions are: no token / bad token / old token

// Set extremely low values to aid debugging.
//        maxAge = ONE_MINUTE;
//        cleanupThreshold = 5;

        final String debugSetting = config.getInitParameter(DEBUG_PARAM);
        // Keep debug as true if it was set to true in the source code, else set it according to the init-param.
        this.debug = this.debug || (debugSetting != null && (debugSetting.equalsIgnoreCase("true") || debugSetting.equalsIgnoreCase("on")));

        final String nameParam = config.getInitParameter(TOKEN_NAME_PARAM);
        if (nameParam != null && nameParam.trim().length() > 0) {
            announce("Setting the token input field name to " + nameParam);
            this.formTokenName = nameParam;
        }

        final String configFileName = config.getInitParameter(CONFIG_FILE_PARAM);
        if (configFileName != null)
        {
            loadConfiguration(config.getServletContext().getRealPath(configFileName));
            // Don't look for the individual init-params if a config file was specified.
            return;
        }
    }


    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException
    {
        // Only handle HttpServletRequests - not any other kind
        if (! (request instanceof HttpServletRequest)) {
            chain.doFilter(request, response);
            return;
        }

        try {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;

            if (req.getMethod().equalsIgnoreCase("GET")) {
                filterGet(req, res, chain);
                return;
            }

            if (req.getMethod().equalsIgnoreCase("POST")) {
                try {
                    announce("Entering filterPost");
                    filterPost(req, res, chain);
                }
                catch (IllegalStateException e) {
                    // See the stack trace at the end of this file.
                    // This filter was set up to deal with Writers (getWriter, PrintWriter)
                    // not OutputStreams (getOuptputStream, ServletOutputStream) so if the
                    // eventual servlet this filter chains to gets the OutputStream, trying
                    // to get the Writer causes an illegal state exception.
                    // Servlets should generally use getWriter if they're dealing with pages,
                    // JSPs, HTML, etc. and only use getOutputStream for binary data or
                    // multi-part responses (ref. http://docs.oracle.com/javaee/5/api/javax/servlet/ServletResponse.html)
                    // This filter and the CharResponseWrapper need to be enhanced to deal with
                    // either Writers or OutputStreams. Meanwhile, catching this exception
                    // allows processing to continue, but the filter will not have injected
                    // anything.
                    // NOTE - a similar catch has been added to injectTokens, so this one
                    // should never be hit.
                    announce("Got an Exception: " + e, true);
                    e.printStackTrace();
                }
                finally {
                    announce("Finished filterPost\n");
                }
                return;
            }

            // Pass anything else along
            chain.doFilter(request, response);
            return;
        }
        finally {
            if (this.tokenStore.size() > cleanupThreshold) {
                cleanup();
            }
        }
    }


    /**
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    private void filterGet(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        response.setCharacterEncoding("utf-8"); // very important
        CharResponseWrapper wrapper = new CharResponseWrapper(response);

        chain.doFilter(request, wrapper);

        injectTokens(request, response, wrapper);

// example code from http://www.oracle.com/technetwork/java/filters-137243.html
//
//        PrintWriter out = response.getWriter();
//        CharResponseWrapper wrapper = new CharResponseWrapper((HttpServletResponse) response);
//
//        chain.doFilter(request, wrapper);
//
//        if (wrapper.getContentType().equals("text/html"))
//        {
//            CharArrayWriter caw = new CharArrayWriter();
//            caw.write(wrapper.toString().substring(0, wrapper.toString().indexOf("</body>") - 1));
//            caw.write("<p>\nYou are visitor number <font color='red'>" + (++counter) + "</font>");
//            caw.write("\n</body></html>");
//            response.setContentLength(caw.toString().length());
//            out.write(caw.toString());
//        }
//        else
//            out.write(wrapper.toString());
//        out.close();
    }


    private void filterPost(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        request.setCharacterEncoding("utf-8");

        // Get the token from the form
        String token = request.getParameter("formTokenId");

        // If there's no token ignore the POST
        if (token == null) {
            announce("Reject - no token was passed in POST request.");
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Token Missing");
            return;
        }

        // If the token was never handed out ignore the POST
        Token t = tokenStore.get(token);
        if (t == null) {
            // No token with this string was in the store. Reject the POST.
            announce("Reject - no such token [" + token + "]");
            //response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Token Invalid");
            return;
        }

        // At this point the token t can't be null
        announce("Found form POST token [" + t + "]");


        /* Here we could also get the request IP address and compare it to the "origin" stored in the token.
         * That would help prevent CSRF and Replay attacks.
         */
        if (! t.getOrigin().equals(request.getRemoteAddr()))
        {
            // Just announcing the problem for now, not preventing it.
            String msg = "!!! POST of Token from address " + request.getRemoteAddr() + " but Token was issued to address " + t.getOrigin();
            announce(msg, true);
        }


        // If the token is too new (a bot posted too fast?) ignore the POST
        // If the token is too old ignore the POST, and throw the token away
        long age = t.getAge();
        if (age < this.minAge) {
            announce("Reject - token [" + token + "] is too new: age " + age + " < minAge " + this.minAge);
            return;
        }
        if (age > this.maxAge) {
            announce("Reject - token [" + token + "] is too old: age " + age + " > maxAge " + this.maxAge);
            tokenStore.remove(token);
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Token Expired");
            return;
        }

        // If we saw the same token "recently" ignore the POST - it's probably a double-click double-submit, or an attack.
        long elapsed = t.timeSinceSeen();
        if (elapsed < MIN_ELAPSED_REPLAY) {
            announce("Ignore - we just saw token [" + token + "] " + elapsed + " milliseconds ago");
            // Flag that we saw it again
            t.setLastSeen();
            return;
        }
        else if (debug) {
            long lastSeen = t.getLastSeenTime();
            long now = System.currentTimeMillis();
            announce("Passing token [" + t + "] that was last seen " + elapsed + " milliseconds ago");
            announce("lastSeen: " + lastSeen + "       now: " + now + "    age: " + elapsed);
            announce("   " + elapsed + " = " + now + " - " + lastSeen);
        }

        // Flag when we last saw this token
        announce("Accepting token [" + t + "] and setting last seen to now");
        t.setLastSeen();

        // a POST could be re-displaying a form with errors, or moving on to a 2nd form in a series, etc.,
        // so we need to add tokens for that also.
        response.setCharacterEncoding("utf-8"); // very important
        CharResponseWrapper wrapper = new CharResponseWrapper(response);

        chain.doFilter(request, wrapper);

        injectTokens(request, response, wrapper, t);

        return;
    }


    /**
     * @param request
     * @param response
     * @param wrapper
     * @throws IOException
     */
    private void injectTokens(HttpServletRequest request, HttpServletResponse response, CharResponseWrapper wrapper)
            throws IOException
    {
        injectTokens(request, response, wrapper, null);
    }


    /**
     *
     * @param request
     * @param response
     * @param wrapper
     * @param previousToken a previously issued token to re-use
     * @throws IOException
     */
    private void injectTokens(HttpServletRequest request, HttpServletResponse response, CharResponseWrapper wrapper, Token previousToken)
            throws IOException
    {
        // Can't affect the response if it's already committed
        if (response.isCommitted()) return;

        PrintWriter out = null;
//        ServletOutputStream outS = null;
        try {
            out = response.getWriter();
        }
        catch (IllegalStateException e) {
            // Response is not committed, but illegal ==> OutputStream was used, not Writer
//            outS = response.getOutputStream();
            // skip the whole thing and return
            // TODO: Enhance the CharResponseWrapper and this code to deal with things
            //       that use getOutputStream() instead of only supporting getWriter()
            return;
        }

        String responseMimeType = wrapper.getContentType();
        String content = wrapper.toString();
        StringBuilder formTokenField = new StringBuilder();

        // Redirects etc. don't have a mime type; Images, text/plain, etc. don't have forms. Skip everything that's not html.
        if (responseMimeType != null && htmlMimeTypePattern.matcher(responseMimeType).find())
        {
            // There's at least one form in the HTML being sent to the browser - we'll be adding token(s)
            String origin = request.getRemoteAddr();
            announce("Generating form token(s) for origin: " + origin);

            // Create just one token and put it in every form on the page
            Token t = previousToken != null ? previousToken : generator.getToken(origin);
            String token = t.getTokenId();
            announce("Issuing token [" + t + "]");

            tokenStore.put(token, t);

            formTokenField.append("<input type=\"hidden\" name=\"").append(this.formTokenName)
                    .append("\" value=\"").append(token).append("\">"); // splitting on the tag omits the tag: replace it.

            content = WebPatterns.FORM_CLOSE.getPattern().matcher(content).replaceAll(formTokenField.toString() + "$1");
        }

        // If we somehow got here with no output Writer available we can't do anything.
        // Bail out now.
        // NOTE - this shouldn't happen, because catching the IllegalStateException (above) should prevent us getting here in this state.
        if (out == null) return;

        // If we created some new content, output that; otherwise output whatever came back from the filter chain
        out.write(content);
        out.close();
    }

    /** Clean up old junk from the Token store. */
    private void cleanup()
    {
        announce("Cleaning up token store that has reached size " + tokenStore.size());

        try {
            // Make a copy of the entry set to avoid a ConcurrentModificationException
            Set<Entry<String, Token>> entries = new HashSet<Entry<String, Token>>(tokenStore.entrySet());
            for (Entry<String, Token> e : entries)
            {
                // Only remove when twice as old so expired tokens can be found and
                // reported as "Token Expired" for a while. If they are cleared out
                // too soon they will look like forged tokens.
                if (e.getValue().getAge() > this.maxAge * 2) {
                    tokenStore.remove(e.getKey());
                }
            }
        }
        catch (ConcurrentModificationException e) {
            // This shouldn't be possible any more, since we make a copy of the entrySet
            // Silently ignore it.
        }
        catch (Exception e) {
            // No exceptions should reallly occur, but if they do we do NOT want to abort any higher level.
            // Catch, log, and ignore the exception.
            announce("FormFilter got an exception while trying to clean up the token cache:", true);
            e.printStackTrace();
        }
    }


    /**
     * Generate tokens to use as the token field value.
     *
     * @author Stephen Paulsen
     *
     */
    private class TokenGenerator
    {
        private long token = 0;

        public TokenGenerator()
        {
            // default
        }

        @SuppressWarnings("unused")
        public TokenGenerator(int start)
        {
            token = start;
        }

        public String getTokenSeries()
        {
            return "t" + (++token);
        }

        public Token getToken(String origin)
        {
            return new Token(origin);
        }
    }


    /**
     * Represents a token used in a form.
     * @author Stephen Paulsen
     *
     */
    /*private*/ class Token
    {
        private final String tokenId;
        private final String origin;
        private final long createTime;
        private long lastSeenTime = 0L;


        public Token(String origin)
        {
            // build the tokenId out of
            //    t+sequence
            //    origin
            //    random-number
            this.origin = origin;
            int random = rand.nextInt();
            this.tokenId = generator.getTokenSeries() + "-" + origin + "-" + random;
            this.createTime = System.currentTimeMillis();
        }

        public String getTokenId()
        {
            return this.tokenId;
        }

        public String getOrigin()
        {
            return this.origin;
        }

        public long getCreateTime()
        {
            return this.createTime;
        }

        /**
         * Returns the age of this token in milliseconds.
         * @return milliseconds since the creation of this Token
         */
        public long getAge()
        {
            return System.currentTimeMillis() - this.createTime;
        }

        public Token setLastSeen()
        {
            this.lastSeenTime = System.currentTimeMillis();
            return this;
        }

        /**
         * Get the time when this token was last seen coming in from a client.
         * @return The epoch time in milliseconds when this token was seen.
         */
        public long getLastSeenTime()
        {
            return this.lastSeenTime;
        }

        public long timeSinceSeen()
        {
            return System.currentTimeMillis() - this.lastSeenTime;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode()
        {
            return this.tokenId.hashCode();
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            Token other = (Token) obj;
            if (!getOuterType().equals(other.getOuterType())) return false;
            if (tokenId == null)
            {
                if (other.tokenId != null) return false;
            }
            else if (!tokenId.equals(other.tokenId)) return false;
            return true;
        }

        private FormFilter getOuterType()
        {
            return FormFilter.this;
        }

        @Override
        public String toString()
        {
            Date created = new Date(this.createTime);
            Date seen = new Date(this.lastSeenTime);
            long elapsed = System.currentTimeMillis() - this.lastSeenTime;
            return this.tokenId + " created " + FormFilter.sdf.get().format(created) +
                (this.lastSeenTime > 0 ? (" last seen " + FormFilter.sdf.get().format(seen) + " (" + elapsed + "ms ago)") : " never seen");
        }
    }

    public static final ThreadLocal<DateFormat> sdf =
            new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("H:mm:ss.SSS");
            }
        };
}

/*  See the catch blocks at lines 189 and 391 -- this exception was originally generated at line 341 (now 389) during testing for MIV-4413

|| --- 17:56:19.400 --->  Got an Exception: java.lang.IllegalStateException: getOutputStream() has already been called for this response
java.lang.IllegalStateException: getOutputStream() has already been called for this response
        at org.apache.catalina.connector.Response.getWriter(Response.java:607)
        at org.apache.catalina.connector.ResponseFacade.getWriter(ResponseFacade.java:196)
        at edu.ucdavis.myinfovault.FormFilter.injectTokens(FormFilter.java:341)
        at edu.ucdavis.myinfovault.FormFilter.filterPost(FormFilter.java:322)
        at edu.ucdavis.myinfovault.FormFilter.doFilter(FormFilter.java:171)
        at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:215)
        at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:188)
        at edu.ucdavis.myinfovault.MIVFilter.doFilter(MIVFilter.java:243)
        at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:215)
        at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:188)
        at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:213)
        at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:172)
        at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:127)
        at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:117)
        at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:108)
        at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:174)
        at org.apache.jk.server.JkCoyoteHandler.invoke(JkCoyoteHandler.java:200)
        at org.apache.jk.common.HandlerRequest.invoke(HandlerRequest.java:283)
        at org.apache.jk.common.ChannelSocket.invoke(ChannelSocket.java:773)
        at org.apache.jk.common.ChannelSocket.processConnection(ChannelSocket.java:703)
        at org.apache.jk.common.ChannelSocket$SocketConnection.runIt(ChannelSocket.java:895)
        at org.apache.tomcat.util.threads.ThreadPool$ControlRunnable.run(ThreadPool.java:689)
        at java.lang.Thread.run(Thread.java:619)
*/
