/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivServerAwareMailMessage.java
 */

package edu.ucdavis.myinfovault;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Email message class aware of being a production or development server. Creates email with
 * MIV person objects, correctly setting the 'sender' header if shadow user is proxied.
 *
 * <p>
 * Note: If in a development environment,addresses are replaced with the MIV
 * configuration defined development addresses. A test prefix is also added
 * to the mail subject.
 * </p>
 *
 * <p>
 * Messages content is formatted with the following pattern:
 * <pre>&lt;to&gt;,
 *
 *&lt;message&gt;
 *
 *--
 *&lt;from&gt;
 *</pre>
 * </p>
 *
 * @author Craig Gilmore
 * @since MIV 4.6.1
 */
public class MivServerAwareMailMessage extends MivMailMessage
{
    private static final boolean isProductionServer = MIVConfig.getConfig().isProductionServer();
    private static final Properties configuration = PropertyManager.getPropertySet("email", "config");

    private static final String TEST_PREFIX = "[TEST] ";

    private static final String MESSAGE = "{0},\n\n{1}\n\n-- \n{2}\n";

    private static final String ADDRESS = "{0} <{1}>";
    private static final String TO_ADDRESS = isProductionServer ? ADDRESS : ("{2} Recipient <" + configuration.getProperty("developmentToAddress") + ">");
    private static final String FROM_ADDRESS = isProductionServer ? ADDRESS : ("{2} Tester <" + configuration.getProperty("developmentFromAddress") + ">");
    private static final String FROM_PROXY_ADDRESS = isProductionServer ? ADDRESS : ("{2} Proxy <" + configuration.getProperty("developmentFromAddress") + ">");
    private static final String SENDER_ADDRESS = isProductionServer ? ADDRESS : ("{2} Tester <" + configuration.getProperty("developmentSenderAddress") + ">");

    private final MivPerson to;
    private final MivPerson from;

    /**
     * Creates an email to the target person from the real person on behalf
     * of the shadow person. The shadow person is used for the email 'from' header
     * unless <code>null</code>, in which case it's the real person. If the real
     * and shadow persons are the same the 'sender' header is not set.
     *
     * @param targetPerson the person to which messages will be sent
     * @param shadowPerson the person from which messages will be sent
     * @param realPerson the person from which messages will be sent on behalf of the shadow person
     * @param subject the subject of the message
     * @throws AddressException if either the recipient or senders email addresses do not follow RFC822 syntax rules
     * @throws MessagingException if either the recipient or senders email addresses are <code>null</code>
     */
    public MivServerAwareMailMessage(MivPerson targetPerson,
                                     MivPerson shadowPerson,
                                     MivPerson realPerson,
                                     String subject) throws AddressException, MessagingException
    {
        this.to = targetPerson;
        this.from = shadowPerson != null ? shadowPerson : realPerson;

        // add the test prefix to the subject for development servers
        setSubject((isProductionServer ? StringUtils.EMPTY : TEST_PREFIX) + subject);

        setTo(getAddress(this.to, TO_ADDRESS));

        // set sender only if not the 'from' person
        if (this.from.equals(realPerson))
        {
            setFrom(getAddress(this.from, FROM_ADDRESS));
        }
        else
        {
            setFrom(getAddress(this.from, FROM_PROXY_ADDRESS));
            setSender(getAddress(realPerson, SENDER_ADDRESS));
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.myinfovault.MivMailMessage#send(java.lang.String)
     */
    @Override
    public Date send(String message) throws MessagingException
    {
        return super.send(MessageFormat.format(MESSAGE,
                                               to.getDisplayName(),
                                               message,
                                               from.getDisplayName()));
    }

    /**
     * Get a name-address formatted email address.
     * <p>E.g. "Robert Paulson &lt;rpaulson@projectmayhem.org&gt;"</p>
     *
     * @param person MIV person
     * @param pattern
     * @return formatted email address
     */
    private String getAddress(MivPerson person, String pattern)
    {
        return MessageFormat.format(pattern,
                                    person.getDisplayName(),
                                    person.getPreferredEmail(),
                                    person.getGivenName());
    }
}
