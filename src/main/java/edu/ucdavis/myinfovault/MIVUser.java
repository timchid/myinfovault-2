/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVUser.java
 */

package edu.ucdavis.myinfovault;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CHANCELLOR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_ASSISTANT;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.OCP_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Represents the currently logged-in user.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class MIVUser
{
    private static final Logger logger = LoggerFactory.getLogger(MIVUser.class);

    /** The MIV user id number. */
    private int userID = -1;
    /** Login Name from distauth. */
    private String loginName = null;
    /** Roles that this user acts in. */
    private Set<String> roles = new HashSet<String>();

    /** Information for and about the currently logged-in user. */
    private MIVUserInfo realUserInfo;
    /** Information for the user this user is working on. */
    private MIVUserInfo targetUserInfo;

    private boolean valid = false;
    /** Has {@link #release()} been called on this object? */
    private boolean released = false;

    /**
     * Create an MIVUser object for the given user name and ID number.
     *
     * @param userName The user's <em>login</em> name.
     * @param userid The user ID number for this user.
     */
    public MIVUser(String userName, int userid)
    {
        this.userID = userid;
        this.loginName = userName;
        valid = userID != -1;

        logger.debug("MIVUser constructor - userID:{}, valid:{}", userID, valid);
        realUserInfo = new MIVUserInfo(this);
        targetUserInfo = realUserInfo;
    }

    /**
     * Create an MIVUser object for the given user name and user ID String.
     *
     * @param userName The user's <em>login</em> name.
     * @param uidString A String representing the user ID number for the user.
     * @throws NumberFormatException if the uidString is not a valid number.
     */
    public MIVUser(String userName, String uidString)
    {
        this(userName, Integer.parseInt(uidString));
    }

    /**
     * Is this user proxying as another user?
     *
     * @return if the real, logged-in person is different than the target person
     */
    public boolean isProxying()
    {
        return !realUserInfo.getPerson().equals(targetUserInfo.getPerson());
    }


    /* *
     * Test whether a user acts in a given role.
     * NOTE: This should probably be moved to MIVUserInfo
     *
     * @param role Name of the role
     * @return true if the provided role is one of the roles this user has.
     * /
    // Maybe; 'roles' a Set<Role> where 'Role' is an enum?
    //       or a java.util.EnumSet<Role> might be better.
    public boolean hasRole(String role)
    {
        checkValid();
        return roles.contains(role);
    }
    */

    /* *
     * A convenience to get the old-style MIV access level for this user.
     * Invoking this method as <code>user.getAccess()</code> is equivalent
     * to </code>user.getUserInfo().getAccess()</code>
     *
     * @return The user's old-style MIV access level
     * /
    public AccessLvl getAccess()
    {
        checkValid();
        return this.realUserInfo.getAccess();
    }
    */

    /* TO DO: decide if this is useful. Can a JSP make any sensible use of this?
     * It would be something like:
     *   <c:if test="${user.accessValue > 3}">...</c:if>
     * and what does the 'magic' value 3 mean?  Since the JSP can't use the
     * 'AccessLvl' enum names is there any reason to make the access level
     * available at all? Either this one *or* the 'getAccess()' above.
     *
     * Note: This probably is NOT needed because our custom <miv:permit> tag can handle this.
     *//*
    public int getAccessValue()
    {
        checkValid();
        return this.myUserInfo.getAccess().ordinal();
    }*/

    /**
     * @return Kerberos login ID
     */
    public String getLoginName()
    {
        checkValid();
        return loginName;
    }

    /**
     * @return user's MIV user ID
     */
    public int getUserID()
    {
        checkValid();
        assert userID == realUserInfo.getPerson().getUserId() : "User ID mismatch between MIVUser and MIVUserInfo!";
        return userID;
    }

    /**
     * UserId is an alias for {@link #getUserID} to conform to camel-case convention in JSP.
     *
     * @return user's MIV user ID.
     */
    public int getUserId()
    {
        return getUserID();
    }

    /**
     * @return user information for the real, logged-in user
     */
    public MIVUserInfo getUserInfo()
    {
        checkValid();
        if (realUserInfo == null) logger.warn("Returning null MIVUserInfo from MIVUser.getUserInfo()");
        logger.trace("Returning user info for this user: {}", realUserInfo.getFullName());
        return this.realUserInfo;
    }

    /**
     * @return user information for the target, possibly switched-to user
     */
    public MIVUserInfo getTargetUserInfo()
    {
        checkValid();
        logger.debug("Returning user info for target user: {}", targetUserInfo.getFullName());
        return this.targetUserInfo;
    }

    /**
     * <p>Act as a proxy for a different user, or "become" another user in the old MIV sense.
     * The user ID string passed may be <code>null</code> which makes the "target" user
     * become the currently logged-in user; or "switches back" to the logged-in user.</p>
     *
     * @param newuserid the MIV userID number being switched to, or <code>null</code>
     * @return a new MIVUserInfo object for the new user.
     */
    public MIVUserInfo switchto(String newuserid)
    {
        checkValid();
        logger.info("User {} is switching to user id [{}]", userID, newuserid);

        if (newuserid != null)
        {
            MIVUserInfo mui = new MIVUserInfo(this, Integer.parseInt(newuserid));
            if (targetUserInfo != null && targetUserInfo != realUserInfo)
            {
                logger.debug("Releasing user info for {}/{}", targetUserInfo.getFullName(), targetUserInfo.getPerson().getUserId());
                targetUserInfo.release();
            }
            targetUserInfo = mui;
            // check to see if we need a cloned, modified targetPerson
            fixPersonRoles();
        }
        else
        {
            logger.info("  User {} is switching back to real user!", userID);
            if (targetUserInfo != null && targetUserInfo != realUserInfo)
            {
                logger.debug("Releasing user info for {}/{}", targetUserInfo.getFullName(), targetUserInfo.getPerson().getUserId());
                targetUserInfo.release();
            }
            targetUserInfo = this.realUserInfo;
        }

        return targetUserInfo;
    }


    /**
     * <p>Same as {@link #switchto(String)} but takes a numeric user id. The user ID number passed
     * may be <code>&lt;= 0</code> to switch back to the currently logged-in user.</p>
     *
     * @param newuserid the MIV userID number being switched to
     * @return a new MIVUserInfo object for the new user.
     */
    public MIVUserInfo switchto(int newuserid)
    {
        String newuser = null;
        if (newuserid > 0)
        {
            newuser = Integer.toString(newuserid);
        }
        return switchto(newuser);
    }

    /**
     * <p>
     * Certain roles are added to "Candidates" such as DEPT_CHAIR or DEAN or
     * VICE_PROVOST. Department Helpers and admins. will still need to "proxy"
     * these people as CANDIDATES, BUT should not be given the power of the
     * added roles. Thus we remove the power in a proxied user
     * </p>
     */
    private void fixPersonRoles()
    {
        MivPerson targetPerson = this.getTargetUserInfo().getPerson();
        MivPerson loginPerson = this.getUserInfo().getPerson();

        // Make sure that more powerful role is not given to the proxied person
        if (loginPerson.hasRole(DEPT_ASSISTANT) && targetPerson.hasRole(DEPT_CHAIR, DEAN, VICE_PROVOST))
        {
            targetPerson = targetPerson.proxiedBy(DEPT_ASSISTANT);
        }
        else if (loginPerson.hasRole(DEPT_STAFF) && targetPerson.hasRole(DEAN, VICE_PROVOST))
        {
            targetPerson = targetPerson.proxiedBy(DEPT_STAFF);
        }
        else if (loginPerson.hasRole(SCHOOL_STAFF) && targetPerson.hasRole(VICE_PROVOST))
        {
            targetPerson = targetPerson.proxiedBy(SCHOOL_STAFF);
        }
        else if (loginPerson.hasRole(SENATE_STAFF) && targetPerson.hasRole(VICE_PROVOST))
        {
            targetPerson = targetPerson.proxiedBy(SENATE_STAFF);
        }
        else if (loginPerson.hasRole(OCP_STAFF) && targetPerson.hasRole(CHANCELLOR, PROVOST))
        {
            targetPerson = targetPerson.proxiedBy(OCP_STAFF);
        }
    }

    private void checkValid()
    {
        if (!valid) throw new IllegalStateException("Invalid state for user "+userID+(released ? " : already released" : ""));
    }

    /**
     * Free the resources used by this MIVUser instance.
     * It is safe to call release on the same object more than once, however the user object becomes
     * invalid and any further attempt to use it will throw an {@link IllegalStateException}.
     */
    public void release()
    {
        String releaseMsg = "MIVUser "+loginName+"/"+userID+" being released.\n  ("+this+")";
        Exception e = new Exception(releaseMsg);
        e.fillInStackTrace();
        logger.debug(releaseMsg, e);

        valid = false;
        if (targetUserInfo != null) { targetUserInfo.release(); targetUserInfo = null; }
        if (realUserInfo != null) { realUserInfo.release(); realUserInfo = null; }
        if (roles != null) { roles.clear(); roles = null; }

        released = true;
    }

    @Override
    protected void finalize() throws Throwable
    {
        try {
            this.release();
        }
        finally {
            super.finalize();
        }
    }
}
