/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: XmlTester.java
 */

package edu.ucdavis.myinfovault.htmlcleaner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


public class XmlTester
{
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String thisPackageName = XmlTester.class.getPackage().getName();
    private String factoryPackage = thisPackageName;
    protected DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    //protected DocumentBuilder builder;

    /** How much context to see on each side of an error indication. */
    private int errorContextWidth = 30;

    /**<p>
     * Create an XmlTester with the default (30) amount of context shown around errors.
     *</p>
     */
    public XmlTester()
    {
//        try
//        {
//            builder = factory.newDocumentBuilder();
//            builder.setErrorHandler(new SilentErrorHandler());
//        }
//        catch (ParserConfigurationException e)
//        {
//            // This exception should never happen, because we're using the vanilla DocumentBuilder
//            log.log(Level.SEVERE, "\"Impossible\" ParserConfiguration error: ", e);
//        }
    }


    /**<p>
     * Create an XmlTester with the given amount of context shown around errors.
     *</p>
     * @param errorContextWidth number of characters to see on <em>each side</em> of an error indicator.
     */
    public XmlTester(int errorContextWidth)
    {
        this();
        this.errorContextWidth = errorContextWidth;
    }


    public XmlTester(String factoryPackageName)
    {
        this();
        this.factoryPackage = factoryPackageName;
    }


    public XmlTester(String factoryPackageName, int errorContextWidth)
    {
        this(errorContextWidth);
        this.factoryPackage = factoryPackageName;
    }


    public int getErrorContextWidth()
    {
        return errorContextWidth;
    }

    public void setErrorContextWidth(int errorContextWidth)
    {
        this.errorContextWidth = errorContextWidth;
    }



    protected static final String wrapTagName = "htmlTestWrapper";
    protected static final String wrapTagStart = "<"+wrapTagName+">";
    protected static final String wrapTagEnd   = "</"+wrapTagName+">";
    private static final int overhead = wrapTagStart.length();// + wrapTagEnd.length();

    public String removeTags(String s)
        throws InvalidMarkupException
    {
        if (s == null) return s;

        s = wrapTagStart + s + wrapTagEnd;
        InputStream in = new ByteArrayInputStream(s.getBytes());

        Document document = null;
        DocumentBuilder builder = null;
        try
        {
            builder = builderFactory.newDocumentBuilder();
            builder.setErrorHandler(new SilentErrorHandler());
        }
        catch (ParserConfigurationException e)
        {
            // This exception should never happen, because we're using the vanilla DocumentBuilder
            log.error("\"Impossible\" ParserConfiguration error: ", e);
        }

        builder.reset();
        try
        {
            document = builder.parse(in);
        }
        catch (SAXParseException e)
        {
            int column = e.getColumnNumber();
            int startColumn = Math.max(0, column-errorContextWidth);
            int endColumn = Math.min(s.length(), column+errorContextWidth);
            int indent = column-(Math.max(0, column-errorContextWidth))-1;

            String newline = System.getProperty("line.separator", "\n");

            StringBuilder sb = new StringBuilder();
            sb.append(e.getLocalizedMessage()).append(newline);
            sb.append(s.subSequence(startColumn, endColumn)).append(newline);
            for (int i=0; i<indent; i++) sb.append(' ');
            sb.append('^');

            throw new InvalidMarkupException(sb.toString(), column - overhead);
        }
        catch (SAXException e)
        {
            throw new InvalidMarkupException(e.getLocalizedMessage());
        }
        catch (IOException e)
        {
            // This exception shouldn't ever happen, since we are just dealing with a String
            log.error("\"Impossible\" I/O error on ByteArrayInputStream: ", e);
            return s;
        }


//        NodeRenderer nr = NodeRenderer.createNodeRenderer(document.getDocumentElement());
        NodeRendererFactory factory = NodeRendererFactory.getFactory(factoryPackage, "TagRenderer");
        NodeRenderer nr = factory.createNodeRenderer(document.getDocumentElement());

        return nr.toString();
    }


    /**
     * Entry point for command line use and testing of this class.
     * @param args
     */
    public static void main(String[] args)
    {
        XmlTester me = new XmlTester();
        if (args.length > 0)
        {
            //me.test(args[0]);
            for (String arg : args) {
                me.test(arg);
            }
        }
        else
        {
            System.out.println("An argument string is required");
            String testString = "start <b>foo <i>italic</i></x> then <b foo=\"bar\">bold again</b> finally more junk";
            me.test(testString);
        }
    }


    /**<p>
     * Test the behavior of this class. Called from the main() method, and can be called from a test harness.
     *</p>
     * @param s
     */
    public void test(String s)
    {
        System.out.println();
        System.out.println("Initial string is  [" + s + "]");
        try {
            s = this.removeTags(s);
        }
        catch (InvalidMarkupException e) {
            e.printStackTrace();
            s = "no result -- bad html was found at character "+e.getColumnNumber();
        }
        System.out.println("Result string is   [" + s + "]");
        System.out.println();
    }


    /**
     * A SAX error handler that silently ignores all errors.
     * Subclasses of this class may want to use this inner class.
     * @author Stephen Paulsen
     */
    /*protected*/public class SilentErrorHandler implements org.xml.sax.ErrorHandler
    {
        @Override
        public void error(SAXParseException exception) throws SAXException
        {
            // silently ignore it
        }

        @Override
        public void fatalError(SAXParseException exception) throws SAXException
        {
            // silently ignore it
        }

        @Override
        public void warning(SAXParseException exception) throws SAXException
        {
            // silently ignore it
        }
    }
}
