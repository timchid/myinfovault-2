/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HtmlCleaner.java
 */


package edu.ucdavis.myinfovault.htmlcleaner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.exception.MIVApplicationException;


public class HtmlCleaner extends XmlTester
{
    private static final String thisPackageName = HtmlCleaner.class.getPackage().getName();
    private static final String DEFAULT_RENDERER_SUFFIX = "TagRenderer";
    private static final int DEFAULT_CONTEXT = 30;

    private String factoryPackage = thisPackageName;
    private String rendererSuffix = DEFAULT_RENDERER_SUFFIX;
    private DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    private DocumentBuilder builder = null;

    private NodeRendererFactory rendererFactory;

    /** How much context to see on each side of an error indication. */
    private int errorContextWidth = DEFAULT_CONTEXT;

    /**<p>
     * Create an HtmlCleaner with the default (30) amount of context shown around errors.
     *</p>
     */
    public HtmlCleaner()
    {
        this(thisPackageName, DEFAULT_RENDERER_SUFFIX, DEFAULT_CONTEXT);
    }


    /**<p>
     * Create an HtmlCleaner with the given amount of context shown around errors.
     *</p>
     * @param errorContextWidth number of characters to see on <em>each side</em> of an error indicator.
     */
    public HtmlCleaner(int errorContextWidth)
    {
        this(thisPackageName, DEFAULT_RENDERER_SUFFIX, errorContextWidth);
    }


    public HtmlCleaner(String factoryPackageName)
    {
        this(factoryPackageName, DEFAULT_RENDERER_SUFFIX, DEFAULT_CONTEXT);
    }


    public HtmlCleaner(String factoryPackageName, int errorContextWidth)
    {
        this(factoryPackageName, DEFAULT_RENDERER_SUFFIX, errorContextWidth);
    }


    public HtmlCleaner(String factoryPackageName, String suffix, int errorContextWidth)
    {
        if (factoryPackageName != null) this.factoryPackage = factoryPackageName;
        if (suffix != null) this.rendererSuffix = suffix;
        if (errorContextWidth >= 0) this.errorContextWidth = errorContextWidth;

        rendererFactory = NodeRendererFactory.getFactory(factoryPackage, rendererSuffix);
        rendererFactory.setDefaultRenderer(TagSourceRenderer.class);
    }

    public boolean setDefaultRenderer(Class<? extends NodeRenderer> nodeRendererClass)
    {
        return this.rendererFactory.setDefaultRenderer(nodeRendererClass);
    }


    @Override
    public int getErrorContextWidth()
    {
        return errorContextWidth;
    }



    protected static final String wrapTagName = "htmlTestWrapper";
    protected static final String wrapTagStart = "<"+wrapTagName+">";
    protected static final String wrapTagEnd   = "</"+wrapTagName+">";
    private static final int overhead = wrapTagStart.length();


    /*
     * ATTENTION: The sections below marked with "SDP 2008-03-10" and with lines
     * starting with the commented "A" are error checking for very extreme edge
     * cases that "should never happen".
     *
     * One case was occurring as listed in MIV-1152 when "builder.parse()" was
     * being called again before the previous call had finished. This happened
     * when the "Manage Publication Annotations" link on the Design Extensive
     * page ("Design My Packet") was clicked rapidly so several calls went out
     * to the Annotations servlet, reinvoking it before the previous call had
     * finished.
     *
     * Moving the DocumentBuilder into here as a local variable, instead of being
     * an instance variable, guarantees the document.parse() call is always
     * invoked on a new instance of "builder", so the error doesn't occur.
     *
     * The error checking on the "A" lines can be removed in the future if no
     * further problems occur.
     */

    @Override
    public String removeTags(String s)
        throws InvalidMarkupException
    {
        if (s == null || s.length() < 1) return s;

        s = wrapTagStart + MIVUtil.escapeAmpersand(s) + wrapTagEnd;
// SDP 2008-03-10
/*A*/   byte[] inputBytes = s.getBytes();
/*A*/   if (inputBytes == null) {
/*A*/       String msg = "Input bytes from a non-null string are somehow null!!";
/*A*/       Exception e = new MIVApplicationException(msg);
/*A*/       e.fillInStackTrace();
/*A*/       log.error(msg, e);
/*A*/   }

        InputStream in = new ByteArrayInputStream(inputBytes/*s.getBytes()*/);

// SDP 2008-03-10
/*A*/   if (in == null) {
/*A*/       String msg = "InputStream from ByteArrayInputStream is somehow null!!";
/*A*/       Exception e = new MIVApplicationException(msg);
/*A*/       e.fillInStackTrace();
/*A*/       log.error(msg, e);
/*A*/   }

        Document document = null;
        // get the builder
        if (builder == null)
        {
            try
            {
                builderFactory.setIgnoringComments(true);  //  The MSWord problem line, which is in a comment:  <m:brkBinSub m:val="--"/>
                builder = builderFactory.newDocumentBuilder();
                builder.setErrorHandler(new SilentErrorHandler());
            }
            catch (ParserConfigurationException e)
            {
                // This exception should never happen, because we're using the vanilla DocumentBuilder
                log.error("\"Impossible\" ParserConfiguration error: ", e);
            }
        }

// SDP 2008-03-10
/*A*/   if (builder == null) {
/*A*/       String msg = "DocumentBuilder \"builder\" is somehow null even though constructor completed normally!!";
/*A*/       Exception e = new MIVApplicationException(msg);
/*A*/       e.fillInStackTrace();
/*A*/       log.error(msg, e);
/*A*/   }

        synchronized (builder)
        {
            builder.reset();
            try
            {
                document = builder.parse(in);
            }
            catch (SAXParseException e)
            {
                int column = e.getColumnNumber();
                int startColumn = Math.max(0, column-errorContextWidth);
                int endColumn = Math.min(s.length(), column+errorContextWidth);
                int indent = column-(Math.max(0, column-errorContextWidth))-1;

                String newline = System.getProperty("line.separator", "\n");

                StringBuilder sb = new StringBuilder();
                sb.append(e.getLocalizedMessage()).append(newline);
                sb.append(s.subSequence(startColumn, endColumn)).append(newline);
                for (int i=0; i<indent; i++) sb.append(' ');
                sb.append('^');

                throw new InvalidMarkupException(sb.toString(), column - overhead);
            }
            catch (SAXException e)
            {
                throw new InvalidMarkupException("Caused by: " + e.getLocalizedMessage(), e);
            }
            catch (IOException e)
            {
                // This exception shouldn't ever happen, since we are just dealing with a String
                log.error("\"Impossible\" I/O error on ByteArrayInputStream: ", e);
                return s;
            }
        }

// SDP 2008-03-10
/*A*/   if (document == null) {
/*A*/       String msg = "Parsed document is somehow null even though no exception was thrown!!" +
/*A*/                    "\n\tOriginal string being processed was [" + s + "]" +
/*A*/                    "";
/*A*/       Exception e = new MIVApplicationException(msg);
/*A*/       e.fillInStackTrace();
/*A*/       log.error(msg, e);
/*A*/       return s;
/*A*/   }

        NodeRenderer nr = rendererFactory.createNodeRenderer(document.getDocumentElement());

        return nr.toString();
    }
    // removeTags()


    /**
     * Entry point for command line use and testing of this class.
     * @param args
     */
    public static void main(String[] args)
    {
        HtmlCleaner me = new HtmlCleaner();
        if (args.length > 0)
        {
            //me.test(args[0]);
            for (String arg : args) {
                me.test(arg);
            }
        }
        else
        {
            System.out.println("An argument string is required");
            String testString = "start<div class=\"myclass\"> pradeep </div> <b>foo</b> <i>italic</i> then <b foo=\"bar\">bold again</b> finally more junk";
            me.test(testString);
        }
    }
}
