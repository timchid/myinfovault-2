/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: StatisticsTester.java
 */

package edu.ucdavis.myinfovault.htmlcleaner.statistics;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;
import edu.ucdavis.myinfovault.htmlcleaner.NodeRendererFactory;
import edu.ucdavis.myinfovault.htmlcleaner.XmlTester;

public class StatisticsTester extends XmlTester
{
    TagStatisticsFactory factory = new TagStatisticsFactory();


    public boolean check(String tableName, String recordID, String userID, String fieldName, String fieldContents)
    {
        boolean isValid = false;

        fieldContents = wrapTagStart + fieldContents + wrapTagEnd;
        InputStream in = new ByteArrayInputStream(fieldContents.getBytes());

        Document document = null;
        DocumentBuilder builder = null;
        try
        {
            builder = builderFactory.newDocumentBuilder();
            builder.setErrorHandler(new SilentErrorHandler());
        }
        catch (ParserConfigurationException e)
        {
            // This exception should never happen, because we're using the vanilla DocumentBuilder
            log.error("\"Impossible\" ParserConfiguration error: ", e);
        }

        builder.reset();
        try
        {
            document = builder.parse(in);
            isValid = true;
        }
        // Ignore the following 3 exceptions, allowing 'isValid' to remain false.
        catch (SAXParseException e) {}
        catch (SAXException e)      {}
        catch (IOException e)       {}

        if (isValid)
        {
            //factory = new TagStatisticsFactory();
            NodeRenderer //nr = factory.createNodeRenderer(document.getDocumentElement());
            nr = factory.createNodeRenderer(tableName, recordID, userID, fieldName, document.getDocumentElement());

            String out = nr.toString();
            if (out!=null && out.length()>0) {
//                System.out.println("StatisticsTester.check(): Result from parsing are:\n     ["+out+"]");
            }
        }

        return isValid;
    }


    class TagStatisticsFactory extends NodeRendererFactory
    {
        @Override
        public NodeRenderer createNodeRenderer(Node n)
        {
            /* Alert if this version of the call is used. If a standard/plain
             * node renderer is created all of the nodes created when it
             * processes its children will be plain, and won't gather the
             * statistics.
             */
            System.err.println("******** DON'T USE THIS CALL! ********");
            Exception e = new Exception("******** DON'T USE THIS CALL! ********");
            e.fillInStackTrace();
            e.printStackTrace(System.err);

            NodeRenderer nr = null;
            int nodeType = n.getNodeType();
            if (nodeType == Node.TEXT_NODE) {
                nr = new edu.ucdavis.myinfovault.htmlcleaner.TextNodeRenderer(n);
            }
            else if (n.getNodeName().equals(wrapTagName)) {
                nr = new NodeRenderer(n, this);
            }
            else {
                nr = new TagCounter(n, this);
            }
            return nr;
        }

        public NodeRenderer createNodeRenderer(String tableName, String recordID, String userID, String fieldName, Node n)
        {
            NodeRenderer nr = null;
            int nodeType = n.getNodeType();
            if (nodeType == Node.TEXT_NODE) {
                nr = new edu.ucdavis.myinfovault.htmlcleaner.TextNodeRenderer(n);
            }
            else {
                nr = new TagCounter(n, this, tableName, recordID, userID, fieldName);
            }
            return nr;
        }
    }
}
