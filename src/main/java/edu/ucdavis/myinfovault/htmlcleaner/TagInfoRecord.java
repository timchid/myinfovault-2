package edu.ucdavis.myinfovault.htmlcleaner;

/**
 * Maintain information about HTML tags found in the MIV database.
 * Used for the scanner and possible the HTML migration and cleanup process.
 * @author Stephen Paulsen
 */
public class TagInfoRecord
{
    private int userID;         // ID of user this record belongs to
    private String table;       // name of the table where tag was found
    private int recordID;       // record number within the table
    private String field;       // field name in which the tag was found
    private String tag;         // tag found, e.g. "strong", "u"
    private String contents;    // contents enclosed within the tag
    private boolean valid = true;       // was this field valid?

    public TagInfoRecord(String tag)
    {
        this(tag, null);
    }

    public TagInfoRecord(String tag, String contents)
    {
        assert tag != null : "tag may not be null";
        this.tag = tag;
        this.contents = contents;
    }

    public TagInfoRecord(String tableName, String recordID, String userID, String fieldName, String tagName, String contents)
    {
        this(tagName, contents);
        this.table = tableName;
        this.recordID = recordID == null || recordID.length() < 1 ? 0 :Integer.parseInt(recordID);
        this.userID = userID == null || userID.length() < 1 ? 0 : Integer.parseInt(userID);
        this.field = fieldName;
    }

    public String getTag()
    {
        return this.tag;
    }

    public String getContents()
    {
        return contents;
    }

    public void setContents(String contents)
    {
        this.contents = contents;
    }

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public int getRecordID()
    {
        return recordID;
    }

    public void setRecordID(int recordID)
    {
        this.recordID = recordID;
    }

    public String getTable()
    {
        return table;
    }

    public void setTable(String table)
    {
        this.table = table;
    }

    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int userID)
    {
        this.userID = userID;
    }

    public boolean isValid()
    {
        return this.valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
//        sb.append("[")
//          .append(userID).append("|")
//          .append(table).append("|")
//          .append(recordID).append("|")
//          .append(field).append("|")
//          .append(tag).append("|")
//          .append(contents).append("|")
//          .append(valid)
//          .append("]")
//          ;

        // build a string for a .CSV file
        sb.setLength(0);
        sb.append(valid?1:0).append(",");
        sb.append(userID).append(",");
        sb.append(table).append(",");
        sb.append(recordID).append(",");
        sb.append(field).append(",");
        sb.append(tag).append(",");
        sb.append(escape(contents));

        return sb.toString();
    }

    // Double up quote characters as required by CSV file format.
    private String escape(String in)
    {
        String out = "\"" + in.replaceAll("\"", "\"\"") + "\"";
        return out;
    }
}
