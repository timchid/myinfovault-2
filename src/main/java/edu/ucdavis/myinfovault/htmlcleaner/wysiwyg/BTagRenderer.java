package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;

public class BTagRenderer extends NodeRenderer
{
    public BTagRenderer(Node n)
    {
        super(n);
    }

    public String toString()
    {
        return "<strong>" + processChildren() + "</strong>";
    }
}
