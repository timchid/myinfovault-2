package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;

public class HrTagRenderer extends NodeRenderer
{
    public HrTagRenderer(Node n)
    {
        super(n);
    }

    @Override
    public String toString()
    {
        return "<hr/>";
    }
}
