package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

public class LiTagRenderer extends TagAttributeRenderer
{
    public LiTagRenderer(Node n)
    {
        super(n,"class","style");
    }
}
