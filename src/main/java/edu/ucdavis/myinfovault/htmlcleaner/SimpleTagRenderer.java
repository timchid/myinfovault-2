package edu.ucdavis.myinfovault.htmlcleaner;

import org.w3c.dom.Node;

/**
 * The SimpleTagRenderer produces its children enclosed within this Node's open
 * and close tags, but ignores any attributes the tag may have originally had.
 *
 * @author Stephen Paulsen
 */
public class SimpleTagRenderer extends NodeRenderer
{
    private String startTag = null;
    private String endTag = null;

    public SimpleTagRenderer(Node n)
    {
	super(n);
        String nodeName = n.getNodeName().toLowerCase();
        startTag = "<" + nodeName + ">";
        endTag = "</" + nodeName + ">";
    }

    public String toString()
    {
        return startTag + processChildren() + endTag;
    }
}
