package edu.ucdavis.myinfovault.htmlcleaner;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;

public class ATagRenderer extends NodeRenderer
{
    public ATagRenderer(Node n)
    {
        super(n);
    }

    public String toString()
    {
        StringBuilder s = new StringBuilder();

        if (this.node.hasAttributes())
        {
            // If the anchor tag has an href (it's a link) we're throwing the link text away
            // and rendering the href -- which should be e.g. "http://some.site.com/path/etc.php"
            NamedNodeMap attrMap = this.node.getAttributes();
//Test
            int attrCount = attrMap.getLength();
            for (int i=0; i<attrCount; i++)
            {
                Node testAttr = attrMap.item(i);
                String attrName  = testAttr.getNodeName();
                String attrValue = testAttr.getNodeValue();
                System.out.println("A HREF renderer attr["+attrName+"] value["+attrValue+"]");
            }
//Test

            Node href = attrMap.getNamedItem("href");
            if (href == null) {
                href = attrMap.getNamedItem("HREF");
            }
            if (href != null) {
                s.append(href.getNodeValue());
            }
        }

        // The buffer will still be empty if no href attribute was in the "a" tag.
        // In that case we'll toss the tags and render the contents.
        if (s.length() == 0)
        {
            s.append(processChildren());
        }
        return s.toString();
    }
}
