package edu.ucdavis.myinfovault.htmlcleaner;

import java.lang.reflect.Constructor;
import java.util.WeakHashMap;

import org.w3c.dom.Node;

public class NodeRendererFactory
{
    /** The name of this package. Introspection looks in the factory's own package if none is specified. */
    private static final String thisPackage = NodeRendererFactory.class.getPackage().getName();
    private static final String DEFAULT_BASE_NAME = "NodeRenderer";

    /** Caches the Constructors that are discovered by introspection. */
    private WeakHashMap<String,Constructor<? extends NodeRenderer>> ctorCache
                                    = new WeakHashMap<String,Constructor<? extends NodeRenderer>>();

    private String searchPackage = thisPackage;
    private String baseName = DEFAULT_BASE_NAME;
    private Class<? extends NodeRenderer>  defaultRendererClass = NodeRenderer.class;
    private Constructor<? extends NodeRenderer> defaultConstructor = null;

    private static final NodeRendererFactory instance = new NodeRendererFactory();


    protected NodeRendererFactory()
    {
        try
        {
            defaultConstructor = defaultRendererClass.getConstructor(Node.class);
        }
        catch (Exception e)
        {
            // The default renderer class won't cause an exception unless it's missing
            throw new RuntimeException("Error trying to get the default Node Renderer", e);
        }
    }


    protected NodeRendererFactory(String packageName, String baseName)
    {
        if (packageName != null) this.searchPackage = packageName;
        if (baseName != null) this.baseName = baseName;
    }


    public static NodeRendererFactory getDefaultFactory()
    {
        return instance;
    }


    /**<p>
     * Get a NodeRendererFactory that looks in the named package for classes with the provided base name suffix.
     * <tt>package.name.&lt;NodeName&gt;&lt;baseName&gt;</tt></p>
     * <p>Either parameter may be <code>null</code> in which case the built-in defaults are used.
     * Passing <code>null</code> for <em>both</em> parameters is the same as calling the static
     * {@link #getDefaultFactory()} method.</p>
     *
     * @param packageName Java package name in dotted format
     * @param baseName class name suffix to use
     * @return a factory to create NodeRenderers
     */
    public static NodeRendererFactory getFactory(String packageName, String baseName)
    {
        if (packageName == null && baseName == null) return getDefaultFactory();
        return new NodeRendererFactory(packageName, baseName);
    }


    /**
     * Set the default NodeRenderer class to use when no specific renderer is found for a node.
     * @param nodeRendererClass
     * @return <code>true</code> if the provided class was accepted as the new default class,
     * <code>false</code> if it was not an appropriate class or did not have an appropriate
     * constructor.
     */
    public boolean setDefaultRenderer(Class<? extends NodeRenderer> nodeRendererClass)
    {
        boolean accepted = false;
        try {
            defaultConstructor = nodeRendererClass.getConstructor(Node.class);
            this.defaultRendererClass = nodeRendererClass;
            accepted = true;
        }
        catch (Exception e) {
            // The provided class couldn't be used -- leave the "accepted" flag==false
        }

        return accepted;
    }


    /**
     * Create an appropriate NodeRenderer for a node.
     * @param n Node to create renderer for
     * @return a NodeRenderer or subclass
     */
    public NodeRenderer createNodeRenderer(Node n)
    {
        assert n != null : "a non-null node must be provided";

        NodeRenderer nr = null;

        int nodeType = n.getNodeType();

        switch (nodeType)
        {
            case Node.TEXT_NODE:
                nr = new TextNodeRenderer(n);
                break;
            case Node.ELEMENT_NODE:
                nr = getNodeRendererByName(n);
                break;
            default:
                // TODO: Reconsider this -- we want the "default" for elements with no special name,
                //       but for non-Element / non-Text nodes maybe the plain "NodeRenderer" is best.
                try {
                    nr = this.defaultConstructor.newInstance(n);
                }
                catch (Exception e) {
                    // the "newInstance()" call shouldn't fail because we verified it
                    // when the default was set, but if any exception *is* thrown just
                    // back off to the plain NodeRenderer.
                    nr = new NodeRenderer(n);
                }
                break;
        }

        return nr;
    }


    /**<p>
     * Use the node name to create an appropriate node renderer.</p>
     * <p>Tries to find a class based on the passed in node's name, and instantiate an object
     * of that class if one is found.
     * By default the class name attempted is <code>initialUpper(nodeName) + "NodeRenderer"</code> so,
     * for example, a <code>&lt;strong&gt;</code> tag would use a <code>StrongNodeRenderer</code> if
     * one exists.
     * </p>
     * <p>The default "NodeRenderer" suffix can be changed by providing the "baseName" parameter
     * when acquiring a factory &mdash see {@link #getFactory(String, String)}
     * </p>
     * @param n Node to create renderer for
     * @return a NodeRenderer or subclass
     */
    private NodeRenderer getNodeRendererByName(Node n)
    {
        NodeRenderer nr = null;
        String nodeName = n.getNodeName();

        do // a "once-through" block that allows using "break" for early exit.
        {
            // Always drop the wrapper node.
            if ("htmlTestWrapper".equals(nodeName)) {
                nr = new NodeRenderer(n);
                break;
            }

            Object[] ctorArgs = { n };
            Constructor<? extends NodeRenderer> c = ctorCache.get(nodeName);
            if (c != null)
            {
                //System.out.println("got a constructor from the map for node named \""+nodeName+"\"");
                try
                {
                    nr = c.newInstance(ctorArgs);
                    break;
                }
                catch (Exception e)
                {
                    // Exception to the rule: we don't care why any exception was thrown here...
                    // If anything at all goes wrong we want to ignore it and continue trying to
                    // find an appropriate NodeRenderer.
                    //e.printStackTrace();
                }
            }

            // No constructor for the given name was found in the cache if we arrive here;
            // 1. use introspection to look for a tag-specific renderer, or
            // 2. return a plain NodeRenderer

            // (1) Try to find a renderer via introspection
            // Note we uppercase the first letter of the tag name to conform with usual
            // Java class naming practices, but we cache on the orginal name.
            String rendererClassName = nodeName.substring(0,1).toUpperCase() + nodeName.substring(1, nodeName.length()).toLowerCase();
            String className = this.searchPackage + "." + rendererClassName + this.baseName;
            try
            {
                Class<?> clazz = Class.forName(className);
                Class<? extends NodeRenderer> rendererClass = clazz.asSubclass(NodeRenderer.class);
                c = rendererClass.getConstructor(Node.class);
                nr = c.newInstance(ctorArgs);
                // Cache the constructor if we've found one and successfully created an instance from it. 
                ctorCache.put(nodeName, c);
                break;
            }
            catch (Exception e)
            {
                // Exception to the rule: we don't care why any exception was thrown here...
                // If anything at all goes wrong we want to ignore it and return a vanilla NodeRenderer.
                //e.printStackTrace(); // (but let's see what happened)
            }

            // If we reach this point then no class was found via introspection, or no
            // appropriate constructor was found (etc...  some exception was thrown), so
            // (2) return a plain NodeRenderer
//            nr = new NodeRenderer(n);
            try {
                nr = this.defaultConstructor.newInstance(n);
            }
            catch (Exception e) {
                // the "newInstance()" call shouldn't fail because we verified it
                // when the default was set, but if any exception *is* thrown just
                // back off to the plain NodeRenderer.
                nr = new NodeRenderer(n);
            }
            break;

        } while (false); // end the "once-through" block

        nr.setFactory(this);
        return nr;
    }
    //getNodeRendererByName()
}
