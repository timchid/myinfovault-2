package edu.ucdavis.myinfovault.htmlcleaner;

import org.w3c.dom.Node;

public class BrTagRenderer extends NodeRenderer
{
    public BrTagRenderer(Node n) { super(n); }

    public String toString()
    {
        return "\n";
    }
}
