/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PermitTag.java
 */

package edu.ucdavis.myinfovault;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.evaluator.ExpressionEvaluator;
import edu.ucdavis.mw.myinfovault.util.evaluator.LiteralExpressionEvaluator;
import edu.ucdavis.mw.myinfovault.util.evaluator.PermissionEvaluator;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * <p>Governs behavior of the MIV permit tag.</p>
 *
 * <p>Uses the {@link LiteralExpressionEvaluator} and {@link PermissionEvaluator}
 * to evaluate "role" attribute boolean expression</p>
 *
 * @author Mary Northup
 * @since MIV 3.0
 */
public class PermitTag extends SimpleTagSupport
{
    private static final String REAL_USER_TYPE = "REAL";

    private AuthorizationService authService = MivServiceLocator.getAuthorizationService();

    private String roles = null;
    private String location = null;
    private String action = null;
    private String qualifier = null;
    private String usertype = null;


    /**
     * An expression that can be evaluated to a boolean value. Any {@link MivRole}
     * found in the expression is compared to the user's assigned roles.
     *
     * @param roles boolean expression using {@link MivRole MivRoles} as literals
     * @see LiteralExpressionEvaluator
     */
    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    /**
     * @param action {@link Permission} for which to authorize the target person
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * @param location {@link location} for which to authorize the target person
     */
    public void setLocation(String location)
    {
        this.location = location;
    }

    /**
     * @param location {@link qualifier} for which to authorize the target person
     */
    public void setQualifier(String qualifier)
    {
        this.qualifier = qualifier;
    }


    /**
     * @param usertype user type, either "REAL" or otherwise
     */
    public void setUsertype(String usertype)
    {
        this.usertype = usertype.toUpperCase();
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.SimpleTagSupport#doTag()
     */
    @Override
    public void doTag() throws JspException, IOException
    {
        // get current MIV session
        MIVSession mivSession = MIVSession.getSession(getJspContext());

        // If there's no session we're definitely not allowed. Return immediately.
        if (mivSession == null) return;

        MivPerson targetPerson = resolveTargetPerson(mivSession.getUser());

        // If an action is specified use MIV authorization service...
        if (action != null)
        {
            boolean isNot = false;
            if (action.startsWith("!"))
            {
                isNot = true;
                System.out.print("action ["+action+"] changed to [");
                action = action.replaceFirst("! *", "");
                System.out.println(action+"]");
            }

            AttributeSet permissionDetails = null;
            if (location != null)
            {
                permissionDetails = new AttributeSet();
                permissionDetails.put("DOSSIERLOCATION", location);
            }

            AttributeSet qualification = null;
            if (qualifier != null && (qualifier.split(":").length == 2))
            {
                qualification = new AttributeSet();
                String [] qualifierArr = qualifier.split(":");

                qualification.put(Qualifier.SCHOOL, qualifierArr[0]);
                qualification.put(Qualifier.DEPARTMENT, qualifierArr[1]);
            }

            // Write the body of the block if the person has the correct permission and no additional qualification
            boolean shouldWrite = false;
            if (qualification == null && (isNot ^ authService.hasPermission(targetPerson, action, permissionDetails))) {
              //  getJspBody().invoke(null);
                shouldWrite = true;//! isNot;
            }
            // Write the body of the block if the person has the correct permission and qualification
            else if (qualification != null && (isNot ^ authService.isAuthorized(targetPerson, action, permissionDetails, qualification))) {
              //  getJspBody().invoke(null);
                shouldWrite = true;//! isNot;
            }
            else
            // Not authorized, but see if this is a test on a non-production system
            {
                if (this.action.equals("test_permit_all"))
                {
                    if (MIVConfig.getConfig().isProductionServer()) {
                        throw new MivSevereApplicationError("A [test_permit_all] was left in the production system!\nThis is for TESTING ONLY!");
                    }
                    //System.out.println("Found a [test_permit_all] for action");
                  //  getJspBody().invoke(null);
                    shouldWrite = true;
                }
                // don't write: do nothing.
            }
            if (shouldWrite) {
                getJspBody().invoke(null);
            }
        }
        // ...if no action, try to use the MivPerson's roles...
        else if (roles != null)
        {
            /*
             * Created to evaluate user role expressions
             */
            ExpressionEvaluator evaluator = new LiteralExpressionEvaluator(new PermissionEvaluator(mivSession.getUser(),
                                                                                                   REAL_USER_TYPE.equals(usertype)),
                                                                           MivRole.values());
            try
            {
                if (evaluator.evaluate(roles)) {
                    getJspBody().invoke(null);
                }
            }
            catch (ParseException e)
            {
                throw new MivSevereApplicationError("Invalid expression in the \"roles\" attribute", e);
            }
        }
        // ...otherwise nothing was specified, which is a programming error.
        else
        {
            throw new MivSevereApplicationError("Invalid use of the permit tag",
                                                new JspTagException("Either \"roles\" or \"action\" must be specified in a \"permit\" tag"));
        }
    }


    /**
     * Resolve the target person to use for permit tag checks.
     *
     * @param mivUser
     * @return MivPerson
     */
    private MivPerson resolveTargetPerson(MIVUser mivUser)
    {
        // just use the real, logged-in person if user type is "REAL" or user is not proxying
        if (REAL_USER_TYPE.equals(usertype) || !mivUser.isProxying())
        {
            return mivUser.getUserInfo().getPerson();
        }
        // else, using target person...

        // using target person
        return mivUser.getTargetUserInfo().getPerson();
    }
}
