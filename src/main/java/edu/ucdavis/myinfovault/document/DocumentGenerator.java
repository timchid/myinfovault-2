/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentGenerator.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;

/**
 * Generates documents.
 * 
 * @author Rick Hendricks
 * @since MIV 2.3
 */
public interface DocumentGenerator
{
    /**
     * Generate the document with the given style and source.
     * 
     * @param xsltFile stylesheet
     * @param xmlFile XML from which document will be generated
     * @param outputFile generated file
     * @return if document was successfully generated
     */
    public boolean generate(File xsltFile, File xmlFile, File outputFile);
}
