/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: XFCGenerator.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableSortedSet;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.RuntimeExec;

/**
 * Generates output documents based on the output format defined in the constructor.
 *
 * @author Rick Hendricks
 * @since MIV 2.3
 */
public class XFCGenerator implements DocumentGenerator
{
    private static final Logger log = LoggerFactory.getLogger(XFCGenerator.class);
    private static final File mivRoot = MIVConfig.getConfig().getApplicationRoot();
    
    // valid output formats
    private static final Set<DocumentFormat> validFormats = ImmutableSortedSet.<DocumentFormat>naturalOrder()
                                                                              .add(DocumentFormat.RTF)
                                                                              .add(DocumentFormat.ODT)
                                                                              .add(DocumentFormat.WML)
                                                                              .add(DocumentFormat.DOCX)
                                                                              .build();
    // Output format of this document
    private final DocumentFormat outputFormat;

    /**
     * Create an XFC document generator. Valid output formats are:
     * <ul>
     * <li>{@link DocumentFormat#RTF}</li>
     * <li>{@link DocumentFormat#ODT}</li>
     * <li>{@link DocumentFormat#WML}</li>
     * <li>{@link DocumentFormat#DOCX}</li>
     * </ul>
     *
     * @param outputFormat output format
     */
    XFCGenerator(DocumentFormat outputFormat)
    {
        // check if output format is valid
        if (!validFormats.contains(outputFormat))
        {
            throw new IllegalArgumentException(outputFormat + " is not a valid output format for this generator");
        }
        
        this.outputFormat = outputFormat;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.myinfovault.document.DocumentGenerator#generate(java.io.File, java.io.File, java.io.File)
     * Generate the outputFile using the input xsltFile and xmlFile
     * @return true file was generated ok, otherwise false.
     */
    @Override
    public boolean generate(File xsltFile, File xmlFile, File outputFile)
    {
        File xmlInput = xmlFile;

        try
        {
            // PreProcess the XML input
            String xsltPreprocessDir = MIVConfig.getConfig().getProperty("document-config-location-xslt-preprocess");
            String xsltPreprocessFileName = MIVConfig.getConfig().getProperty("document-config-location-xfc-preprocessor");
            String xfcJar = MIVConfig.getConfig().getProperty("generator-config-xfc-jar");

            if (xsltPreprocessDir != null && xsltPreprocessFileName != null)
            {
                File preprocessFile = new File(xsltPreprocessDir, xsltPreprocessFileName);
                if (preprocessFile.exists())
                {
                    xmlInput = FOTransformer.transform(preprocessFile, xmlFile, xmlFile.getAbsolutePath()+".preprocess");
                }
            }

            // Generate the FO file
            File foFile = FOTransformer.transform(xsltFile, xmlInput, xmlFile.getAbsolutePath()+".fo");

            String jarName = new File(new File(mivRoot, "WEB-INF/lib/"), xfcJar).getPath();
            if (foFile != null && foFile.exists())
            {
                // Transform the XSL-FO to the specified output format
                String[] execArray = {
                        System.getProperty("java.home") + "/bin/java",
                        "-cp", jarName,
//                        System.getProperty("catalina.home") + "/webapps/miv/WEB-INF/lib/" + xfcJar,
                        "com.xmlmind.fo.converter.Driver",
                        "-outputFormat=" + outputFormat,
                        foFile.getAbsolutePath(),
                        outputFile.getAbsolutePath()
                    };

                RuntimeExec.execute(execArray);
                
                // document generated successfully
                return outputFile.exists();
            }
        }
        catch (FileNotFoundException fnfe)
        {
            log.error("Unable to create FO output: " + fnfe.getMessage(), fnfe);
        }
        catch (TransformerConfigurationException tce)
        {
            log.error("Unable to create FO output: " + tce.getMessage(), tce);
        }
        catch (TransformerException te)
        {
            log.error("Unable to create FO output: " + te.getMessage(), te);
        }
        
        // document NOT generated successfully
        return false;
    }
}
