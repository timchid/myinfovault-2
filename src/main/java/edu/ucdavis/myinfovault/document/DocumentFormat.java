/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentFormat.java
 */

package edu.ucdavis.myinfovault.document;

/**
 * Collection of document output types.
 * 
 * @author Rick Hendricks
 * @since MIV 2.1
 */
public enum DocumentFormat
{
    /**
     * Combined, Portable Document Format.
     */
    COMBINED_PDF ("pdf"),
    
    /**
     * Portable Document Format.
     */
    PDF ("pdf"),
    
    /**
     * Rich Text Format (Compatible with MS Word)
     */
    RTF ("rtf"),
    
    /**
     * Open Document Format (OpenOffice.org native format)
     */
    ODT ("odt"),
    
    /**
     * WordprocessingML (Compatible with MS Word 2003 and later)
     */
    WML ("doc"),
    
    /**
     * Open Office XML (MS Word 2007 native format)
     */
    DOCX ("docx"),
    
    /**
     * Extensible Markup Language.
     */
    XML ("xml");
    
    private final String fileExtension;
    
    /**
     * Initialize the file extension for the output type.
     * 
     * @param fileExtension document format file extension
     */
    private DocumentFormat(String fileExtension)
    {
        this.fileExtension = fileExtension;
    }
    
    /**
     * @return document format file extension
     */
    public String getFileExtension()
    {
        return fileExtension;
    }
}
