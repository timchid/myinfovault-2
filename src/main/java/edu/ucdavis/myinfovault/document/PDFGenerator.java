/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PDFGenerator.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.avalon.framework.configuration.DefaultConfigurationBuilder;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.XSLTCache;
import edu.ucdavis.myinfovault.document.PDFConcatenator;

/**
 * 
 * @author Lawrence Fyfe
 */
public class PDFGenerator implements DocumentGenerator
{
    private static Logger log = Logger.getLogger("MIV");
    private final FopFactory fopFactory = FopFactory.newInstance();
    
    /**
     * Initializes the fopFactory from the configuration file if present. 
     */
    public PDFGenerator() 
    {
        String configFilePath = MIVConfig.getConfig().getApplicationRoot()+"/fopcfg.xml";
        DefaultConfigurationBuilder cfgBuilder = null;
        Configuration cfg = null;

        try 
        {
            if (new File(configFilePath).exists()) {
                cfgBuilder = new DefaultConfigurationBuilder();
                cfg = cfgBuilder.buildFromFile(new File(configFilePath));
            }

            if (cfg != null) {
                fopFactory.setUserConfig(cfg);
            }
        }
        catch (IOException ioe)
        {
           log.log(Level.SEVERE,"Exception loading FOP configuration: "+ioe.getMessage(),ioe);
        }
        catch (ConfigurationException ce)
        {
           log.log(Level.SEVERE,"Exception loading FOP configuration: "+ce.getMessage(),ce);
        }
        catch (SAXException se)
        {
           log.log(Level.SEVERE,"Exception parsing FOP configuration file: "+se.getMessage(),se);
        }
    }
    
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.myinfovault.document.DocumentGenerator#generate(java.io.File, java.io.File, java.io.File)
     */
    public boolean generate(File xsltFile, File xmlFile, File outputFile)
    {
        boolean documentCreated = false;
        try 
        {
            OutputStream out = new FileOutputStream(outputFile);
            try
            {
                Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF/*"application/pdf"*/, out);

                if (xsltFile != null & xmlFile != null & xsltFile.exists() && xmlFile.exists() && (xmlFile.length() > 0))
                {
                    Source xmlSource = new StreamSource(xmlFile);
                    Result pdfResult = new SAXResult(fop.getDefaultHandler());

                    Transformer transformer = XSLTCache.newTransformer(xsltFile.getAbsolutePath());
                    if (transformer != null)
                    {
                        transformer.transform(xmlSource, pdfResult);
                    }
                    if (outputFile.exists() && PDFConcatenator.validatePDFFile(outputFile.getAbsolutePath()))
                    {
                        documentCreated = true;
                    }
                }
                else
                {
                    log.log(Level.SEVERE,"Unable to create PDF document "+outputFile.getAbsolutePath()+
                            ". XSLT File "+xsltFile.getAbsolutePath()+" exists="+xsltFile.exists()+
                            " XML File "+xmlFile.getAbsolutePath()+" exists="+xmlFile.exists());
                }
            } 
            catch (FOPException fe)
            {
               log.log(Level.SEVERE,"FOP Exception creating PDF document "+outputFile.getAbsolutePath()+" : "+fe.getMessage(),fe);
            }
            catch (TransformerConfigurationException tce)
            {
               log.log(Level.SEVERE,"Exception retrieving transfomer object from cache to create PDF document "+outputFile.getAbsolutePath()+" : "+tce.getMessage(),tce);
            }
            catch (TransformerException te)
            {
               log.log(Level.SEVERE,"Exception attempting to transform XML into PDF document "+outputFile.getAbsolutePath()+" : "+te.getMessage(),te);
            }
            finally {
                try{out.close();} catch (IOException ioe){ /*ignore*/ }
            }
        } catch (FileNotFoundException fnfe){
            log.log(Level.SEVERE, "Unable to open PDF document "+outputFile.getAbsolutePath()+": "+fnfe.getMessage(), fnfe);
        }

        return documentCreated;
    }

}
