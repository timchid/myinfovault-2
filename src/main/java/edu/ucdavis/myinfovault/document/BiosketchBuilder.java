/**
 * Copyright
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchBuilder.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.data.DataMap;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;
import edu.ucdavis.myinfovault.data.SectionFetcher;

public class BiosketchBuilder extends Builder implements DocumentBuilder
{
    private static Logger log = Logger.getLogger("MIV");
    private String baseFileName = "biosketch";
    private MIVUserInfo mui = null;
    private LinkedHashMap<Integer, BiosketchDocument> documents = new LinkedHashMap<Integer, BiosketchDocument>();
    private List<Biosketch> biosketchList = null;
    List<Map<String, String>>biosketchTypeMapList = MIVConfig.getConfig().getConstants().get("biosketchtype");

    /**
     * BiosketchBuilder constructor to process a single biosketch object
     * @param mui
     * @param biosketch
     */
    public BiosketchBuilder(MIVUserInfo mui, Biosketch biosketch)
    {
        super(BuilderDocumentType.BIOSKETCH);
        this.mui = mui;
        this.setUserId(mui.getPerson().getUserId());
        if (biosketchList == null)
        {
            biosketchList = new ArrayList<Biosketch>();
        }
        biosketchList.add(biosketch);
        this.init();
    }

    /**
     * BiosketchBuilder constructor to process a list of biosketch objects
     * @param mui
     * @param biosketchList
     */
    public BiosketchBuilder(MIVUserInfo mui, List<Biosketch>biosketchList)
    {
        super(BuilderDocumentType.BIOSKETCH);
        this.biosketchList = biosketchList;
        this.mui = mui;
        this.setUserId(mui.getPerson().getUserId());
        this.init();
    }

    private void init()
    {
        // init file paths
        this.setFilePaths(mui.getUserDir());

        // set the documentFormats
        this.setDocumentFormats();

        // get the document data
        this.gatherDocumentData();
    }

    /**
     * Gather the document data for all of the documents that could potentially be produced
     * initializing a map of document Ids/documents. There is currently only one biosketch document
     * produced in multiple formats
     */
    private void gatherDocumentData()
    {

        // Document uploads currently not supported for biosketch
        boolean concatenateUploads = false;

        for (Biosketch biosketch : biosketchList)
        {
            int documentId = biosketch.getBiosketchType().docIdentifier().getId();    // default

            /*// Only initialize the document id when formatting is required
            //if (biosketch.getBiosketchStyle().isBodyFormatting() && (this.biosketchTypeMapList != null))

            // initialize the document id
            if(this.biosketchTypeMapList != null)
            {
                String biosketchType = "";
                for (Map<String, String>biosketchTypeMap : this.biosketchTypeMapList)
                {

                    if (biosketchTypeMap.containsKey("id") &&
                        biosketchTypeMap.get("id").equalsIgnoreCase(Integer.toString(biosketch.getBiosketchType())))
                    {
                        biosketchType = biosketchTypeMap.get("name");
                        break;
                    }
                }

                // The document ID is not carried with the biosketch data so we are forcing the document ID based on
                // the biosketch type.
                if(biosketchType.equalsIgnoreCase("nih"))
                {
                    documentId = 12;    // NIH
                }
                else if (biosketchType.equalsIgnoreCase("cv"))
                {
                    documentId = 11;    // CV
                }
            }*/

            // Get the XML file. Concatenate the biosketch ID for uniqueness
            File outputXmlFile = new File(this.buildXmlFileObj(this.baseFileName+biosketch.getId()+"_").getAbsolutePath());

            // Get the XSLT file...same name/path as XML file with ".xslt" appended
            File outputXsltFile = new File(outputXmlFile.getAbsolutePath()+".xslt");

            // Get the location of the style sheet(s) and XSLT template for this biosketch
            String xsltDirectory = this.getStyleSheetImplementation(biosketch);
            // Build the xlstTemplate name which is the the same name as the implementation direcory
            File xsltTemplate = this.buildXsltFileObj(xsltDirectory+"/"+xsltDirectory);

            // Build the XSLT file
            if (xsltTemplate.exists())
            {
                XSLTBuilder xsltBuilder = new XSLTBuilder(biosketch,outputXsltFile,xsltTemplate);
                xsltBuilder.buildBiosketchOptions();
            }

            // Create the biosketch document object
            BiosketchDocument document = new BiosketchDocument(
                    biosketch.getUserID(),
                    biosketch.getBiosketchType().getTypeId(),
                    biosketch.getBiosketchData(),
                    biosketch.getBiosketchAttributes(),
                    biosketch.getBiosketchStyle().isBodyFormatting(),
                    documentId,
                    biosketch.getName(),
                    biosketch.getTitle(),
                    this.baseFileName+biosketch.getId()+"_",
                    outputXsltFile,
                    outputXmlFile,
                    this.getOutputFilePath(),
                    concatenateUploads,
                    this.getDocumentFormats());

            // Add to the document map
            documents.put(documentId, document);
        }
    }

    /**
     * Iterate through the list of documents and create each
     * returning a list of document objects
     * @return List of documents
     */
    @Override
    public List<Document> generateDocuments()
    {

        //generate the XML  data for each document in the list
        List<Document>documentList = this.generateXML();
        ArrayList<Document>outputDocumentList = new ArrayList<Document>();

        // Iterate thought the list of documents for which there is XML data
        for (Document document : documentList)
        {
            // Only create if there is an XML file present
            if (document.getXmlFile() != null)
            {
                // Create each document format
                document.create();
                outputDocumentList.add(document);
            }
        }
        return outputDocumentList;
    }

    /**
     * Generate the XML document for each document in the list of documents for which there is data.
     * Only documents which contain data are returned in the list
     * @return List of documents containing data
     */
    private List<Document>generateXML()
    {
        Map<String, Map<String, String>> sectionHeaderMap = null;
        XMLBuilder builder = null;
        BufferedWriter writer = null;
        Iterable<DisplaySection> documentSectionMap = null;
        DataMap recordNameMap = null;

        SectionFetcher sf = this.mui.getSectionFetcher();
        // Get the sectionHeaderMap
        //     sectionHeaderMap = sf.getHeaderMap();
        // Get the display name
        String displayName = sf.getPersonalData().getDisplayName();
        // Get the full name...First and Last
        DataMap wholeNameMap = new DataMap(); /*this.fetcher.getWholeNameMap();*/

        // We want to use the display name on all documents if it exists.
        if (displayName != null && displayName.length() > 0)
        {
            displayName = displayName.replaceAll("&quot;", "\"");
            wholeNameMap.put("fname", displayName);
            wholeNameMap.put("lname", "");
        }

        // Get the fieldPatternMap
        DataMap fieldPatternMap = null;
        DocumentIdentifier docIdentifier = DocumentIdentifier.INVALID;

        // Get the datamap for each document
        Map<BiosketchDocument, Iterable<DisplaySection>>documentList = this.getDocuments();

        // Iterate through each document and build the XML file for each
        for (BiosketchDocument document : documentList.keySet())
        {
            sectionHeaderMap = document.getBiosketchData().getSectionHeaderMap();
            documentSectionMap = documentList.get(document);
            recordNameMap = this.getRecordNameMap();

            docIdentifier = DocumentIdentifier.convert(document.documentId);

            // skip invalid documents
            if(docIdentifier == DocumentIdentifier.INVALID)
            {
                continue;
            }

            if(document.isBodyFormatting())
            {
                fieldPatternMap = this.mui.getUserFieldPatternMap(docIdentifier);
            }

            // Build the XML output
            if (documentSectionMap.iterator().hasNext())
            {

                // Add the attributes to the documentSectionMap. This is done in order to pass the
                // attribute data through the XMLBuilder so the proper formatting (subscript/superscript)
                // may be applied if needed.
                Iterable<BiosketchAttributes>biosketchAttributes = document.getBiosketchAttributeList();
                if (biosketchAttributes != null && biosketchAttributes.iterator().hasNext())
                {
                    Map<String, String> attributeMap = new LinkedHashMap<String,String>();
                    Map<String, Map<String,String>>attributes = new LinkedHashMap<String, Map<String,String>>();
                    for (BiosketchAttributes attribute : biosketchAttributes)
                    {
                        // Default to no data
                        attributeMap.put(attribute.getName(), "");
                        if (attribute.isDisplay())
                        {
                            if (attribute.getName().equalsIgnoreCase("programdirector_name") && attribute.getValue().isEmpty()) {
                                attribute.setValue(mui.getPerson().getSurname()+", "+mui.getPerson().getGivenName()+(mui.getPerson().getMiddleName().isEmpty()? "" : ", "+mui.getPerson().getMiddleName()));
                            }
                            attributeMap.put(attribute.getName(), attribute.getValue());
                        }
                    }
                    // Document title is not a true attribute, but add it to the list in order to pass
                    // through the XMLBuilder for any necessary formatting
                    attributeMap.put("document_title", document.getTitle());
                    attributes.put("attribute-record", attributeMap);
                    DisplaySection displaySection =  new DisplaySection("attributes", "", "", "", attributes);
                    ((LinkedList<DisplaySection>)documentSectionMap).add(displaySection);
                }

                builder = new XMLBuilder(this.getDocumentType(), fieldPatternMap, wholeNameMap, sectionHeaderMap,
                        null, docIdentifier);
                builder.buildBiosketchXmlDoc(documentSectionMap, recordNameMap, this.mui, document.isBodyFormatting());
                try
                {
                    writer = new BufferedWriter(new FileWriter(document.getXmlFile()));
                    writer.write(builder.getXMLString());
                    writer.close();
                }
                catch (IOException ioe)
                {
                    log.log(Level.SEVERE, "Unable create XML output file to produce biosketch document(s):", ioe);
                    ioe.printStackTrace(System.out);
                }
            }
            else
            {
                document.setXmlFile(null);
            }
        }
        // Return the list of documents
        return new ArrayList<Document>(documentList.keySet());
    }

    /**
     * Get the documents and document map for each document in the packet. Only
     * documents which contain data are returned in the list
     *
     * @return Map of documents and document sections
     */
    private Map<BiosketchDocument, Iterable<DisplaySection>> getDocuments()
    {
        BiosketchDocument document = null;
        Iterable<DisplaySection> documentSections = null;
        LinkedHashMap<BiosketchDocument, Iterable<DisplaySection>> documentMap = new LinkedHashMap<BiosketchDocument, Iterable<DisplaySection>>();

        // Get the document section map for each document
        for (Integer documentId : documents.keySet())
        {
            document = documents.get(documentId);

            documentSections = document.getBiosketchData().getDisplaySectionList();

            // Check for personal information records, which are mapped to
            // biosketch attributes. There are no "display" flags for this
            // data so they are added here with a value of '1'. The XSLT style
            // sheet processing will exclude them when necessary based on the
            // setting fot the applicable biosketch attribute.
            for (DisplaySection section : documentSections)
            {
                Map<String, Map<String, String>> recordMap = section.getRecords();
                String key = section.getKey();
                // Personal record types which may be selected for display
                if (key.startsWith("personal"))
                {
                    for (String record : recordMap.keySet())
                    {
                        recordMap.get(record).put("display", "1");
                    }
                }
            }

            if (documentSections != null)
            {
                documentMap.put(document, documentSections);
            }
        }
        return documentMap;
    }

    /**
     * Get any existing documents for this biosketch
     */

    @Override
    public List<Document> getDocumentList()
    {
//        ArrayList<Document> documents = new ArrayList<Document>();
//
//        return documents;
        return new ArrayList<Document>();
    }

    /**
     * Get the style sheet implementation for this biosketch
     * The implementation corresponds to a subdirectory where the style sheets
     * are stored for the defined implementation.
     *
     * @return implementation String
     */
    private String getStyleSheetImplementation(Biosketch biosketch)
    {
        Connection connection = MIVConfig.getConfig().getConnection();
        PreparedStatement documentStatement = null;
        ResultSet documentSet = null;
        String implementation = "miv";      // default
        String name = "Default";
        String query = "Select ID, Name, Implementation from SystemCitationFormat";

        int citationFormatId = biosketch.getBiosketchStyle().getCitationFormatID();

        try
        {
            documentStatement = connection.prepareStatement(query);
            documentSet = documentStatement.executeQuery();

            while(documentSet.next())
            {
                if (documentSet.getInt("ID") == citationFormatId)
                {
                    name = documentSet.getString("Name");
                    implementation = documentSet.getString("Implementation");
                    break;
                }
                if (documentSet.getString("Name").equalsIgnoreCase("default"))
                {
                    name = documentSet.getString("Name");
                    implementation = documentSet.getString("Implementation");
                }
            }
            log.log(Level.INFO, "Stylesheet implementation set to : "+name+"-"+implementation);
        }
        catch (SQLException sqle)
        {
            log.log(Level.WARNING, "Unable to load Style Sheet implementation.", sqle);
            sqle.printStackTrace(System.out);
        }
        finally
        {
            try
            {
                if (documentSet != null)
                {
                    documentSet.close();
                }
                if (documentStatement != null)
                {
                    documentStatement.close();
                }
                MIVConfig.getConfig().releaseConnection(connection);
            }
            catch (SQLException sqle)
            {
                // Ignore
            }
        }
        return implementation;
    }

    /**
     * Build a map if section names/record names.
     *
     * @return Map of sections and record names
     */
    private DataMap getRecordNameMap()
    {
        Connection connection = MIVConfig.getConfig().getConnection();
        PreparedStatement sectionStatement = null;
        ResultSet sectionSet = null;
        String sectionQuery = "Select Name, RecordName, FromTable from Section";
        DataMap sectionNameMap = new DataMap();

        // Add dummy attribute record
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("recordname", "attribute-record");
        map.put("tablename", "");
        sectionNameMap.put("attributes", map);

        try
        {
            sectionStatement = connection.prepareStatement(sectionQuery);
            sectionSet = sectionStatement.executeQuery();

            while(sectionSet.next())
            {
                map = new LinkedHashMap<String,String>();
                map.put("recordname", sectionSet.getString("RecordName"));
                map.put("tablename", sectionSet.getString("FromTable"));
                sectionNameMap.put(sectionSet.getString("Name"), map);
            }
        }
        catch (SQLException sqle)
        {
            log.log(Level.SEVERE, "Unable to load section map for document creation.", sqle);
            sqle.printStackTrace(System.out);
        }
        finally
        {
            try
            {
                if (sectionSet != null)
                {
                    sectionSet.close();
                }
                if (sectionStatement != null)
                {
                    sectionStatement.close();
                }
                MIVConfig.getConfig().releaseConnection(connection);
            }
            catch (SQLException sqle)
            {
                // Ignore
            }
        }
        return sectionNameMap;
    }

}
