/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Document.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * TODO: javadoc
 *
 * @author Rick Hendricks
 * @since MIV 2.3
 */
public abstract class Document
{
    Logger log = Logger.getLogger(Document.class);

    protected int userId = 0;
    protected int collectionId = 0;
    protected boolean concatenateUploads = false;
    protected int documentId = 0;
    protected String description = null;
    protected String title = null;
    protected String baseFileName = null;
    protected String dossierFileName = null;
    protected String outputFilePath = null;
    protected Map<DocumentFormat, DocumentGenerator> documentFormats = null;
    protected List<DocumentFile> outputDocuments = new ArrayList<DocumentFile>();
    protected File xsltFile = null;
    protected File xmlFile = null;
    protected boolean displayContributions = false;
    protected long packetId = 0;
    protected PathConstructor urlBuilder = null;

    /**
     * TODO: javadoc
     *
     * @param userId
     * @param collectionId
     * @param documentId
     * @param description
     * @param title
     * @param baseFileName
     * @param xsltFile
     * @param xmlFile
     * @param outputFilePath
     * @param uploadFilePath
     * @param concatenateUploads
     * @param displayContributions
     * @param finalizePacket
     * @param sequence
     * @param documentFormats
     */
    public Document(int userId,
                    int collectionId,
                    int documentId,
                    String description,
                    String title,
                    String baseFileName,
                    File xsltFile,
                    File xmlFile,
                    String outputFilePath,
                    String uploadFilePath,
                    boolean concatenateUploads,
                    boolean displayContributions,
                    boolean finalizePacket,
                    String sequence,
                    Map<DocumentFormat, DocumentGenerator> documentFormats)
    {
        this(userId,
             collectionId,
             documentId,
             description,
             title,
             baseFileName,
             null,
             xsltFile,
             xmlFile,
             outputFilePath,
             uploadFilePath,
             concatenateUploads,
             displayContributions,
             documentFormats,
             0);
    }

    /**
     * TODO: javadoc
     *
     * @param userId
     * @param collectionId
     * @param documentId
     * @param description
     * @param title
     * @param baseFileName
     * @param dossierFileName
     * @param xsltFile
     * @param xmlFile
     * @param outputFilePath
     * @param uploadFilePath
     * @param concatenateUploads
     * @param displayContributions
     * @param documentFormats
     * @param dossierId
     */
    public Document(int userId,
                    int collectionId,
                    int documentId,
                    String description,
                    String title,
                    String baseFileName,
                    String dossierFileName,
                    File xsltFile,
                    File xmlFile,
                    String outputFilePath,
                    String uploadFilePath,
                    boolean concatenateUploads,
                    boolean displayContributions,
                    Map<DocumentFormat, DocumentGenerator> documentFormats,
                    long packetId)
    {
        this.userId = userId;
        this.collectionId = collectionId;
        this.documentId = documentId;
        this.description = description;
        this.title = title;
        this.baseFileName = baseFileName;
        this.dossierFileName = dossierFileName;
        this.xsltFile = xsltFile;
        this.xmlFile = xmlFile;
        this.outputFilePath = outputFilePath;
        this.concatenateUploads = concatenateUploads;
        this.displayContributions = displayContributions;
        this.documentFormats = documentFormats;
        this.packetId= packetId;
        this.urlBuilder = new PathConstructor(MIVConfig.getConfig().getDocumentUrlRoot());
    }

    /**
     * Iterate through the all formats to be applied to this document and create each.
     */
    public void create()
    {
        boolean success = false;
        if (this.urlBuilder == null) urlBuilder = new PathConstructor(MIVConfig.getConfig().getDocumentUrlRoot());

        for (DocumentFormat format : this.documentFormats.keySet())
        {
            String outputFileName = this.getOutputFileName(format);
            String outputFileUrl = this.getOutputFileUrl(format);

            // Rename the file if it exists
            removeFile(outputFileName, true);

            // Generate the document file for this format
            File outputFile = new File(outputFileName);
            DocumentGenerator generator = this.documentFormats.get(format);
            success = generator.generate(this.xsltFile, this.xmlFile, outputFile);

            // If the current file generated is a PDF file and concat, concatenate any upload files
            // to the file just generated
            if (this.concatenateUploads && format == DocumentFormat.PDF)
            {
                concatenateUploadFiles(this.userId, this.documentId, outputFileName);
                success = PDFConcatenator.validatePDFFile(outputFileName);
            }

            // Add the document to the output file list
            DocumentFile documentFile = new DocumentFile(success, outputFile, outputFileUrl, format);
            this.outputDocuments.add(documentFile);
        }
    }

    /**
     * Collect the output document files for this document based on
     * the formats which are defined by iterating through the
     * documentFormats and checking for the existence of the file.
     *
     * @return if documents exist
     */
    public boolean collectDocumentFiles()
    {
        this.outputDocuments.clear();

        for (DocumentFormat format : this.documentFormats.keySet())
        {
            String outputFileName = this.getOutputFileName(format);
            String outputFileUrl = this.getOutputFileUrl(format);
            File outputFile = new File(outputFileName);

            // Validate the file
            boolean success = false;
            if (outputFile.exists() && (outputFile.length() > 0))
            {
                success = true;
                if (format == DocumentFormat.PDF)
                {
                     success = PDFConcatenator.validatePDFFile(outputFile);
                }
            }

            // Add the document to the output file list
            DocumentFile documentFile = new DocumentFile(success, outputFile, outputFileUrl, format);
            this.outputDocuments.add(documentFile);
        }

        return !outputDocuments.isEmpty();
    }

    /**
     * Get a document output file name based on the input format.
     *
     * @param format input document format
     * @return output filename
     */
    public String getOutputFileName(DocumentFormat format)
    {

        return new StringBuilder().append(this.outputFilePath)
                .append("/")
                .append(format.getFileExtension().toLowerCase())
                .append("/")
                .append(this.baseFileName)
                .append(this.userId)
                .append("_")
                .append(this.packetId)
                .append(".")
                .append(format.getFileExtension())
                .toString();
    }

    /**
     * Get a document output URL name based on the input format.
     *
     * @param format the input document format
     * @return output URL name
     */
    protected final String getOutputFileUrl(DocumentFormat format)
    {
        if (this.urlBuilder == null)
        {
            log.warn("Document.getOutputFileUrl: urlBuilder should have been non-null by now but isn't. Building one.");
            urlBuilder = new PathConstructor(MIVConfig.getConfig().getDocumentUrlRoot());
        }

        return urlBuilder.getUrlFromPath(this.getOutputFileName(format));
    }

    /**
     * Concatenate any uploaded files to the input file.
     *
     * @param userID
     * @param documentID
     * @param fileName
     * @return if there were files to upload
     */
    private boolean concatenateUploadFiles(int userID, int documentID, String inputFile)
    {
        Packet packet = MivServiceLocator.getPacketService().getPacket(this.packetId);

        LinkedList<String> uploadFileList = new LinkedList<String>();
        boolean uploadFiles = false;
        boolean pdfuploadFirst = false;

        try
        {
            // Get the temporary file name for concatenation based on the input file name
            String tempOutputFileName = new File(inputFile).getAbsolutePath() + ".concat";
            File tempOutputFile = new File(tempOutputFileName);

            // Use database input file name for output
            File outputFile = new File(inputFile);

            // Get the upload files if any
            DataFetcherDao dfDao = MivServiceLocator.getBean("dataFetcherDao");
            List<Map<String, Object>> uFiles = dfDao.getPacketUploadFiles(userID, documentID, packet);

            // Check for any uploaded PDFs
            for (Map<String,Object>fileMap : uFiles)
            {
                uploadFiles = true;

                // Note that the filename contains hard-coded directory location (e.g. pdf/[filename])
                String uploadName =(String) fileMap.get("UploadFile");
                pdfuploadFirst = (boolean) fileMap.get("UploadFirst");


                // Build the path to the uploaded file name
                File outputDirectory  = MIVConfig.getConfig().getDossierPdfLocation(userID);

                // Full upload file path
                File uploadFile = new File(outputDirectory,uploadName);

                /*
                 * Make sure file to concatenate is a valid PDF file.
                 *
                 * If any upload files are invalid, clear the list
                 * and remove the existing data-based PDF.
                 */
                if (!PDFConcatenator.validatePDFFile(uploadFile))
                {
                    uploadFileList.clear();

                    if (outputFile.exists())
                    {
                        outputFile.delete();

                        // Create a new empty file to signify error
                        outputFile.createNewFile();
                    }

                    break;
                }

                uploadFileList.add(uploadFile.getAbsolutePath());
            }

            // Check for data-based PDF first
            if (PDFConcatenator.validatePDFFile(inputFile))
            {
                //The uploaded PDF files must go before the data-based PDF
                if (pdfuploadFirst)
                {
                    uploadFileList.addLast(inputFile);
                }
                // data-based file is first for everything else
                else
                {
                    uploadFileList.addFirst(inputFile);
                }
            }

            // concatenate files in the upload list
            if (!uploadFileList.isEmpty())
            {
                PDFConcatenator concatenator = new PDFConcatenator();

                if (concatenator.concatenateFiles(uploadFileList, tempOutputFile))
                {
                    tempOutputFile.renameTo(outputFile);// rename newly concatenated file
                }
            }
        }
        catch (IOException ioe)
        {
            log.log(Level.ERROR, "Unable to concatenate upload files to produce packet document(s):", ioe);
            ioe.printStackTrace(System.out);
        }
        return uploadFiles;
    }

    /**
     * @return documents created in the various formats for this document
     */
    public List<DocumentFile> getOutputDocuments()
    {
        return outputDocuments;
    }

    /**
     * @return base filename
     */
    public String getBaseFileName()
    {
        return baseFileName;
    }

    /**
     * @return this document dossier filename
     */
    public String getDossierFileName()
    {
        return dossierFileName;
    }

    /**
     * @return document ID
     */
    public Integer getDocumentId()
    {
        return documentId;
    }

    /**
     * Backup (rename) or remove a file if it exists.
     *
     * @param fileName filename to process
     * @param rename if <code>true</code>, rename current file, otherwise, delete current file
     */
    public void removeFile(String fileName, boolean rename)
    {
        if (fileName != null)
        {
            File fileObj = new File(fileName);

            // Either rename or delete
            if (fileObj.exists() && rename)
            {
                String bakFileName = fileName + ".bak";

                // see if there is already a backup file
                File bakFileObj = new File(bakFileName);

                if (bakFileObj.exists())
                {
                    log.info(bakFileObj.delete() ? "Deleted: " + bakFileName : "Delete Failed: " + bakFileName);
                }

                log.info(fileObj.renameTo(bakFileObj) ? "Rename: " + fileName + " to " + bakFileName : "Rename Failed: " + fileName
                        + " to " + bakFileName);
            }
            else if (fileObj.exists())
            {
                log.info(fileObj.delete() ? "Deleted: " + fileName : "Delete Failed: " + fileName);
            }
        }
    }

    /**
     * @return the XSLT file for this document
     */
    public File getXsltFile()
    {
        return xsltFile;
    }

    /**
     * @return the XML file for this document
     */
    public File getXmlFile()
    {
        return xmlFile;
    }

    /**
     * @param file the XML file for this document
     */
    public void setXmlFile(File file)
    {
        this.xmlFile = file;
    }

    /**
     * @param description the description for this document
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the description for this document
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param title title for this document
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return title for this document
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @return if concatenate upload records for this document
     */
    public boolean isConcatenateUploads()
    {
        return concatenateUploads;
    }
}
