package edu.ucdavis.myinfovault.document;

import java.util.List;

public interface DocumentBuilder
{
    public List<Document> generateDocuments();
    public List<Document> getDocumentList();
}
