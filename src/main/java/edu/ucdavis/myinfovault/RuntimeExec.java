package edu.ucdavis.myinfovault;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper class for java runtime.exec
 * Currently supports all forms of Runtime.exec
 * See javadoc for {@link java.lang.Runtime#exec(String) Runtime.exec} for details.
 *
 * @author Rick Hendricks
 */
public class RuntimeExec
{
    private static final Logger log = LoggerFactory.getLogger(RuntimeExec.class);

// The Java Runtime .exec() method should never be used with a single String command argument.
// A String[] cmdArray versions should always be used.  Since we're wrapping it, we shouldn't
// even have the single String versions of the method.
//    public static int execute(String cmd)
//    {
//        return execute(cmd.split(" "), null, null);
//    }
//
//    public static int execute(String cmd, String [] envp)
//    {
//        return execute(cmd.split(" "), envp, null);
//    }
//
//    public static int execute(String cmd, String [] envp, File file)
//    {
//        return execute(cmd.split(" "), envp, file);
//    }

    public static int execute(String [] cmd)
    {
        return execute(cmd, null, null);
    }

    public static int execute(String [] cmd, String [] envp)
    {
        return execute(cmd, envp, null);
    }

    public static int execute(String[] cmd, String [] envp, File file )
    {
        int exitStatus = -1;

        StringBuilder cmdString = new StringBuilder();
        for (String param : cmd)
        {
            cmdString.append(param).append(" ");
        }

        try
        {
            log.info("Invoking RuntimeExec: {} {} {}", new Object[] { cmdString, envp, file });
            Runtime rt = Runtime.getRuntime();
            Process proc = null;

            // rt.exec(cmd, envp, file) behaves just like the shorter versions
            // without the envp and/or file parameters when envp or file are null.
            proc = rt.exec(cmd, envp, file);

            // Define error and input stream consumer threads
            InputStreamConsumer errorStream = new InputStreamConsumer(proc.getErrorStream(), cmdString.toString());
            InputStreamConsumer inputStream = new InputStreamConsumer(proc.getInputStream(), cmdString.toString());
            // Start the consumer threads
            errorStream.start();
            inputStream.start();
            // Wait for command to complete
            exitStatus = proc.waitFor();
            log.info("RuntimeExec exitSatus: {}", exitStatus);
        }
        catch (IOException ioe)
        {
            log.error("Unable to exec: " + cmdString, ioe);
            ioe.printStackTrace();
        }
        catch (InterruptedException ue)
        {
            log.error("Unable to exec: " + cmdString, ue);
            ue.printStackTrace();
        }
        return exitStatus;
    }

    /**
     * Inner class to handle consumption of exec input streams
     *
     * @author Rick Hendricks
     */
    static class InputStreamConsumer extends Thread
    {
        InputStream is;
        String commandStr;

        public InputStreamConsumer(InputStream is, String commandStr)
        {
            this.is = is;
            this.commandStr = commandStr;
        }

        @Override
        public void run()
        {
            InputStreamReader isr = null;
            BufferedReader br = null;
            try
            {
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                String line = null;
                while ((line = br.readLine()) != null)
                    System.out.println(commandStr + ">" + line);
            }
            catch (IOException ioe)
            {
                ioe.printStackTrace();
            }
            finally
            {
                try
                {
                    if (br != null) br.close();
                    if (isr != null) isr.close();
                }
                catch (IOException ioe)
                {
                    // ignore
                }
            }
        }
    }
}
