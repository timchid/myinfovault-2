/*
 * This adaptation Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: XSLTCache.java
 */


package edu.ucdavis.myinfovault;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A utility class that caches XSLT stylesheets in memory. Adapted from O'Reilly
 * Java and XSLT By Eric M. Burke.
 *
 * Each instance of Templates is thread-safe, so it is desirable to maintain a
 * single copy shared by many clients. This reduces the number of times
 * stylesheets have to be parsed into memory and compiled, as well as the
 * overall memory footprint the application.
 *
 */
public class XSLTCache
{
    // map xslt file names to MapEntry instances
    // (MapEntry is defined below)
    private static Map<String, MapEntry> cache = new HashMap<String, MapEntry>();

    private static final Logger logger = LoggerFactory.getLogger(XSLTCache.class);

    /**
     * Flush all cached stylesheets from memory, emptying the cache.
     */
    public static synchronized void flushAll()
    {
        cache.clear();
    }

    /**
     * Flush a specific cached stylesheet from memory.
     *
     * @param xsltFileName
     *                the file name of the stylesheet to remove.
     */
    public static synchronized void flush(String xsltFileName)
    {
        cache.remove(xsltFileName);
    }

    /**
     * Obtain a new Transformer instance for the specified XSLT file name. A new
     * entry will be added to the cache if this is the first request for the
     * specified file name.
     *
     * @param xsltFileName the file name of an XSLT stylesheet
     * @return a transformation context for the given stylesheet
     * @throws TransformerConfigurationException 
     */
    public static synchronized Transformer newTransformer(String xsltFileName) throws TransformerConfigurationException
    {
        File xsltFile = new File(xsltFileName);

        Transformer transformer = null;

        // determine when the file was last modified on disk
        long xslLastModified = xsltFile.lastModified();
        MapEntry entry = cache.get(xsltFileName);

        if (entry != null)
        {
            // if the file has been modified more recently than the
            // cached stylesheet, remove the entry reference
            if (xslLastModified > entry.lastModified || entry.templates == null)
            {
                entry = null;
                logger.info("{} refreshing stale cache.", xsltFileName);
            }
            else
            {
                transformer = entry.templates.newTransformer();
                logger.info("{} retrieved from cache.", xsltFileName);
            }
        }

        // create a new entry in the cache if necessary
        if (entry == null)
        {
            Source xslSource = new StreamSource(xsltFile);

            TransformerFactory transFact = TransformerFactory.newInstance();
            Templates templates = transFact.newTemplates(xslSource);

            // Make sure the compilation was completed
            if (templates != null)
            {
                entry = new MapEntry(xslLastModified, templates);
                cache.put(xsltFileName, entry);
                logger.info("{} placed in cache.", xsltFileName);
                transformer = entry.templates.newTransformer();
            }
            else
            {
                logger.error("{} compilation of XSLT failed.", xsltFileName);
            }
        }
        return transformer;
    }

    // prevent instantiation of this class
    private XSLTCache()
    {
    }

    /**
     * This class represents a value in the cache Map.
     */
    static class MapEntry
    {
        long lastModified; // when the file was modified
        Templates templates;

        MapEntry(long lastModified, Templates templates)
        {
            this.lastModified = lastModified;
            this.templates = templates;
        }
    }
}
