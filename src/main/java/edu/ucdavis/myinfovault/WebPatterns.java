/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: WebPatterns.java
 */

package edu.ucdavis.myinfovault;

import java.util.regex.Pattern;

/**
 * Represents the Web Case Sensitive/Insensitive patterns.<br>
 * Patterns begin with prefix <b>_</b> are Case Sensitive Patterns.
 *
 * @author pradeeph
 *
 * @since MIV 4.8
 */
public enum WebPatterns
{
    // Case Insensitive Patterns
    HTML("Html tag pattern", "((<html\\s?>)|(<html\\s+.*?>))", true),
    HTML_CLOSE("Html close tag pattern", "(</html\\s?>)", true),
    HTML_PART("Html tag pattern", "((<html\\s?)|(<html\\s+.*?))(>)", true),
    HTML_CLASS("Html tag with class attribute pattern", "((<html\\s?class=[\"|'])|(<html\\s+.*class=[\"|']))", true),

    BODY("Body tag pattern", "((<body\\s?>)|(<body\\s+.*?>))", true),
    BODY_CLOSE("Body close tag pattern", "(</body\\s?>)", true),
    BODY_PART("Body tag pattern", "((<body\\s?)|(<body\\s+.*?))(>)", true),
    BODY_CLASS("Body tag with class attribute pattern", "((<body\\s?class=[\"|'])|(<body\\s+.*class=[\"|']))", true),

    FORM("Form tag pattern", "((<form\\s?>)|(<form\\s+.*?>))", true),
    FORM_CLOSE("Form close tag pattern", "(</form\\s?>)", true),
    FORM_PART("Form tag pattern", "((<form\\s?)|(<form\\s+.*?))(>)", true),
    FORM_CLASS("Form tag with class attribute pattern", "((<form\\s?class=[\"|'])|(<form\\s+.*class=[\"|']))", true),

    // Case Sensitive Patterns
    _HTML("Html tag pattern", "((<html\\s?>)|(<html\\s+.*?>))", false),
    _HTML_CLOSE("Html close tag pattern", "(</html\\s?>)", false),
    _HTML_PART("Html tag pattern", "((<html\\s?)|(<html\\s+.*?))(>)", false),
    _HTML_CLASS("Html tag with class attribute pattern", "((<html\\s?class=[\"|'])|(<html\\s+.*class=[\"|']))", false),

    // The left alternative, <body\s?> will match:
    //   <body>  <body0x20> ( aka <body > )  <body0x09>   <body0x0a>
    // etc. that is - "body" optionally followed by any single kind of whitespace character,
    // but will fail to match "<body   >" (3 spaces)   or:   <body
    //                                                       >
    // (body, a newline, a bunch of whitespace, close-angle_bracket)
    //
    // The right alternative will improperly match
    //   <body title="this --> that">   as   <body title="this -->
    _BODY("Body tag pattern", "((<body\\s?>)|(<body\\s+.*?>))", false),
    _BODY_CLOSE("Body close tag pattern", "(</body\\s?>)", false),
    _BODY_PART("Body tag pattern", "((<body\\s?)|(<body\\s+.*?))(>)", false),
    _BODY_CLASS("Body tag with class attribute pattern", "((<body\\s?class=[\"|'])|(<body\\s+.*class=[\"|']))", false),

    _FORM("Form tag pattern", "((<form\\s?>)|(<form\\s+.*?>))", false),
    _FORM_CLOSE("Form close tag pattern", "(</form\\s?>)", false),
    _FORM_PART("Form tag pattern", "((<form\\s?)|(<form\\s+.*?))(>)", false),
    _FORM_CLASS("Form tag with class attribute pattern", "((<form\\s?class=[\"|'])|(<form\\s+.*class=[\"|']))", false);

    private String description;
    private String regex;
    private boolean caseInSensitive;
    private Pattern pattern;

    private WebPatterns(String description, String regex, boolean caseInSensitive)
    {
        this.description = description;
        this.regex = regex;
        this.caseInSensitive = caseInSensitive;
        this.pattern = this.caseInSensitive ? Pattern.compile(this.regex, Pattern.CASE_INSENSITIVE) : Pattern.compile(this.regex);
    }

    /**
     * Get the description of the web pattern
     * @return String
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * Get the pattern ragex string
     * @return String
     */
    public String getRegex()
    {
        return this.regex;
    }

    /**
     * Get the pattern
     * @return java.util.regex.Pattern
     */
    public Pattern getPattern()
    {
        return this.pattern;
    }
}
