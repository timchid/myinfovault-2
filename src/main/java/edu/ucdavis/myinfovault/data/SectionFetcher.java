/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SectionFetcher.java
 */


package edu.ucdavis.myinfovault.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.format.AdditionalInfoHelper;
import edu.ucdavis.myinfovault.format.FormatManager;
import edu.ucdavis.myinfovault.format.GreekFixer;
import edu.ucdavis.myinfovault.format.RecordFormatter;

/**
 * Gets a set of records for a given record type, and optionally a named section.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class SectionFetcher extends Fetcher
{
    private static final Logger log = LoggerFactory.getLogger(SectionFetcher.class);

    /**
     * This is the query used to match additional-info headers with the records that belong under it.
     */
    private static final String ADDITIONAL_INFORMATION_QUERY =
        "SELECT AdditionalInformation.ID, AdditionalInformation.HeaderID AS HeaderID," +
        "       Content AS Preview, Content AS AddContent, AdditionalHeader.Value AS Heading," +
        "       AdditionalHeader.Value AS AddHeader, AdditionalInformation.Display," +
        "       AdditionalInformation.InsertTimestamp, AdditionalInformation.UpdateTimestamp" +
        " FROM AdditionalHeader LEFT JOIN AdditionalInformation ON AdditionalInformation.HeaderID = AdditionalHeader.ID" +
        " WHERE AdditionalInformation.HeaderID = ?" +
        " ORDER BY AdditionalInformation.Sequence ASC" +
        "";

    DataSource source = null;


    MIVUserInfo userInfo;
    int userID = -1;    // UserID is used often so keep it handy even though it's available from userInfo.
    private FormatManager formatting = null;
    private RecordFormatter greekFormatter = new GreekFixer();


    /**
     * Create a section fetcher for the given user information
     *
     * @param userInfo MIV user information
     */
    public SectionFetcher(MIVUserInfo userInfo)
    {
        this.userInfo = userInfo;
        this.userID = userInfo.getPerson().getUserId();
        this.formatting = new FormatManager(this.userInfo);
        this.source = getDataSource();
    }


    /**
     * Get data (records) for all sections that have the indicated record type.
     * The record type will have "-record" appended to it.
     *
     * @param rectype
     * @return a list of sections
     */
    public List<DisplaySection> getRecordSections(final String rectype)
    {
        final RecordFormatter[] f = null;
        return getRecordSection(rectype, null, f);
    }


    /**
     * Get data (records) for all sections that have the indicated record type.
     * The record type will have "-record" appended to it.
     *
     * @param rectype
     * @param the packet we're fetching for
     * @return a list of sections
     */
    public List<DisplaySection> getRecordSections(final String rectype, final Long packetId)
    {
        final RecordFormatter[] f = null;
        return getRecordSection(rectype, null, null, f);
    }


    /**
     * Get data (records) for all sections that have the indicated record type.
     * The record type will have "-record" appended to it.
     * Apply the provided formatter to all records after standard formatting has been done.
     *
     * @param rectype
     * @param postFormat an optional formatter to be run after the standard formatters, or <code>null</code>.
     * @return a list of sections
     */
    public List<DisplaySection> getRecordSections(final String rectype, final RecordFormatter postFormat)
    {
        return getRecordSection(rectype, null, postFormat);
    }


    /**
     * @param sectionIds comma separated sections ID numbers.
     * @param ruleSet ruleSet object which contains the record criteria to limit the records retrieved.
     * @return a list of sections
     */
    public List<DisplaySection> getRecordSection(final String sectionIds, final Iterable<Ruleset> ruleSet)
    {
        return getRecordSection(sectionIds, true, ruleSet);
    }


    /**
     * @param sectionIds comma separated sections ID numbers.
     * @param applyFormatting flag to indicate whether or not to format the records
     * @param ruleSet ruleSet object which contains the record criteria to limit the records retrieved.
     * @param postFormat
     * @return a list of sections
     */
    public List<DisplaySection> getRecordSection(final String sectionIds, final boolean applyFormatting, final Iterable<Ruleset> ruleSet, final RecordFormatter... postFormat)
    {
        final SectionQuery[] rs = getRecordTypeQueries(sectionIds, ruleSet);
        final List<DisplaySection> displaylist = new LinkedList<DisplaySection>();

        // Get the headers
        final Map<String,Map<String,String>> sectionHeaderMap = getHeaderMap();
        final String keyColumn = "ID";

        for (SectionQuery r : rs)
        {
            final String q = r.query;
            log.debug("Running query: [{}]", q);
            LinkedHashMap<String,Map<String,String>> m = runQuery(q, keyColumn, Integer.toString(this.userID));
            if (!m.isEmpty())
            {
                if ("additional".equalsIgnoreCase(r.rectype))
                {
                    // this keyset contained the header IDs related to particular additional information section
                    Set<String> keySet = m.keySet();
                    Iterator<String> it = keySet.iterator();
                    String additionalHeading = "";
                    while (it.hasNext())
                    {
                        Map<String, String> headerRecord = m.get(it.next());
                        String additionalHeaderID = headerRecord.get("id");

                        formatting.applyFormats("additionalheader", headerRecord/*, postFormat*/);

                        additionalHeading = headerRecord.get("value");
                        if (additionalHeading == null || "".equals(additionalHeading))
                        {
                            additionalHeading = AdditionalInfoHelper.getMissingAdditionalHeaderPreview();
                        }
                        String displayAdditionalHeaderFlag = headerRecord.get("display");
                        boolean shouldDisplayHeader = "1".equals(displayAdditionalHeaderFlag);

                        LinkedHashMap<String, Map<String, String>> infoRecords =
                            runQuery(ADDITIONAL_INFORMATION_QUERY, keyColumn, additionalHeaderID);

                        // Create the preview with MISSING message for the missing additional record
                        AdditionalInfoHelper.getMissingAdditionalRecordPreview(infoRecords);

                        // Every rectype is supposed to have both "preview" and "sequencepreview". For every rectype except
                        // additional formatters add the "sequencepreview". So add the "sequencepreview" for additional rectype here
                        AdditionalInfoHelper.formatAdditionalRecordType(infoRecords);

                        formatting.applyFormats(r.rectype, infoRecords/*, postFormat*/);

                        // here we are using newly created DisplaySection constructor, because we need to have additionalHeaderId to pull out the records.
                        DisplaySection ds = new DisplaySection(r.key, additionalHeading, r.subheading, infoRecords, additionalHeaderID);
                        ds.setHeaderEditable(true);
                        ds.setHeaderVisible(shouldDisplayHeader);

                        displaylist.add(ds);
                    }
                }
                else
                {
                    if (applyFormatting)
                    {
                        if (postFormat != null)
                        {
                            RecordFormatter[] fArray = new RecordFormatter[postFormat.length+1];
                            System.arraycopy(postFormat, 0, fArray, 0, postFormat.length);
                            fArray[postFormat.length] = greekFormatter;
                            formatting.applyFormats(r.rectype, m, fArray);
                        }
                        else
                        {
                            formatting.applyFormats(r.rectype, m, greekFormatter);
                        }
                    }
                    /* *** Using new header map *********************************************** */
                    String newSectionHeading = "";

                    Map<String, String> sectionMap = sectionHeaderMap.get(r.key);
                    if (sectionMap != null && sectionMap.size() > 0)
                    {
                        newSectionHeading = sectionMap.get("header");
                    }

                    if (newSectionHeading == null || newSectionHeading.trim().length() <= 0)
                    {
                        newSectionHeading = r.heading;
                    }
                    /* ************************************************************************ */

                    DisplaySection ds = new DisplaySection(r.key, newSectionHeading, r.subheading, m);

                    /* At some later time we'll make the Publication headers editable on the ItemList page.
                     * Need to figure a good way to decide if this Section is a Publication type that should
                     * have an editable header, but not things like Teaching or Grants etc.
                     * Also see recordlist.jsp
                     */
                    /*if (something) then ds.setHeaderEditable(true);
                     *  String displayHeading = "";
                     *   displayHeading = (String)sectionMap.get("display");*/

                    displaylist.add(ds);
                }
            }
        }

        return displaylist;
    }


    /**
     * Get data (records) for the individual section requested, or for all sections, that have the indicated record type.
     * The record type will have "-record" appended to it.
     *
     * @param rectype
     * @param section Name of a section, or <code>null</code> to get all sections.
     * @param postFormat an optional formatter to be run after the standard formatters, or <code>null</code>.
     * @return a list of sections
     */
    public List<DisplaySection> getRecordSection(final String rectype, final String section, final RecordFormatter... postFormat)
    {
        return getRecordSection(rectype, section, null, postFormat);
    }


    /**
     * Get data (records) for the individual section requested, or for all sections, that have the indicated record type.
     * The record type will have "-record" appended to it.
     *
     * @param rectype
     * @param section Name of a section, or <code>null</code> to get all sections.
     * @param constraint select constraint or <code>null</code> to default or the current userId.
     * @param postFormat an optional formatter to be run after the standard formatters, or <code>null</code>.
     * @return a list of sections
     */

    public List<DisplaySection> getRecordSection(final String rectype, final String section, final String constraint, final RecordFormatter... postFormat)
    {
        log.trace("Getting records for type {}", rectype);

        final String keyColumn = "ID";
        final SectionQuery[] rs = getRecordTypeQueries(rectype);

        if (rs.length == 0)
        {
            log.warn("No record queries were found for record type \"{}\"", rectype);
            return null;
        }

        List<DisplaySection> displaylist = new LinkedList<DisplaySection>();

        // Get the headers
        Map<String,Map<String,String>> sectionHeaderMap = getHeaderMap();

        for (SectionQuery r : rs)
        {
            log.debug("getRecordSections working on: {}", r);

            // If no particular section was asked for (section==null) add the current section to the set,
            // or if a section *was* requested and the current section matches it, add it.
            if (section == null || section.equals(r.key))
            {
                String q = r.query;
                log.debug("Running query: [{}]", q);
                LinkedHashMap<String,Map<String,String>> m = runQuery(q, keyColumn, constraint != null ? constraint : Integer.toString(this.userID));
                log.debug("getRecordSection found (*{}*)", m);

                if (!m.isEmpty())
                {
                    log.debug("  section:[{}]  rectype:[{}]", section, rectype);

                    if ("additional".equalsIgnoreCase(rectype))
                    {
                        // this keyset contained the header IDs related to particular additional information section
                        Set<String> keySet = m.keySet();
                        Iterator<String> it = keySet.iterator();
                        String additionalHeading = "";
                        while (it.hasNext())
                        {
                            Map<String, String> headerRecord = m.get(it.next());
                            String additionalHeaderID = headerRecord.get("id");

                            formatting.applyFormats("additionalheader", headerRecord, postFormat);

                            additionalHeading = headerRecord.get("value");
                            if (additionalHeading == null || "".equals(additionalHeading))
                            {
                                additionalHeading = AdditionalInfoHelper.getMissingAdditionalHeaderPreview();
                            }

                            String displayAdditionalHeaderFlag = headerRecord.get("display");
                            boolean shouldDisplayHeader = "1".equals(displayAdditionalHeaderFlag);

                            LinkedHashMap<String, Map<String, String>> infoRecords =
                                runQuery(ADDITIONAL_INFORMATION_QUERY, keyColumn, additionalHeaderID);

                            // Create the preview with MISSING message for the missing additional record
                            AdditionalInfoHelper.getMissingAdditionalRecordPreview(infoRecords);

                            // Every rectype is supposed to have both "preview" and "sequencepreview". For every rectype except
                            // additional formatters add the "sequencepreview". So add the "sequencepreview" for additional rectype here
                            AdditionalInfoHelper.formatAdditionalRecordType(infoRecords);
                            setResequenceKey(infoRecords,rectype, r.key);
                            formatting.applyFormats(rectype, infoRecords, postFormat);

                            // here we are using newely created DisplaySection constructor, because we need to have additionalHeaderId to pull out the records.
                            DisplaySection ds = new DisplaySection(r.key, additionalHeading, r.subheading, infoRecords, additionalHeaderID);
                            ds.setHeaderEditable(true);
                            //ds.setHeaderVisible(displAddHeader);
                            ds.setHeaderVisible(shouldDisplayHeader);

                            displaylist.add(ds);
                        }
                    }
                    else
                    {
                        // Run the FORMATTERS
                        if (postFormat != null)
                        {
                            RecordFormatter[] fArray = new RecordFormatter[postFormat.length+1];
                            System.arraycopy(postFormat, 0, fArray, 0, postFormat.length);
                            fArray[postFormat.length] = greekFormatter;
                            formatting.applyFormats(rectype, m, fArray);
                        }
                        else
                        {
                            formatting.applyFormats(rectype, m, greekFormatter);
                        }

                        // Apply SetResequenceKey on formated records
                        setResequenceKey(m,rectype, r.key);

                        log.debug("formatted records are (*{}*)", m);

                        /* *** Using new header map *********************************************** */
                        String newSectionHeading = "";

                        Map<String, String> sectionMap = sectionHeaderMap.get(r.key);
                        if (sectionMap != null && sectionMap.size() > 0)
                        {
                            newSectionHeading = sectionMap.get("header");
                        }

                        if (newSectionHeading == null || newSectionHeading.trim().length() <= 0)
                        {
                            newSectionHeading = r.heading;
                        }
                        /* ************************************************************************ */

                        DisplaySection ds = new DisplaySection(r.key, newSectionHeading, r.subheading, m);

                        /* At some later time we'll make the Publication headers editable on the ItemList page.
                         * Need to figure a good way to decide if this Section is a Publication type that should
                         * have an editable header, but not things like Teaching or Grants etc.
                         * Also see recordlist.jsp
                         */
                        /*if (something) then ds.setHeaderEditable(true);
                         *  String displayHeading = "";
                         *   displayHeading = (String)sectionMap.get("display");*/

                        displaylist.add(ds);
                    }
                } //if map is not empty
            }
        } //for each SectionQuery found

        return displaylist;
    }
    //getRecordSection()


    /**
     * To add sequencekey attribute on each and every section record data, to perform re-sequencing
     * @param dataMap
     * @param rectype
     * @param section
     */
    private void setResequenceKey(final LinkedHashMap<String,Map<String,String>> dataMap, final String rectype, final String section)
    {
        /*System.out.println("*********************** setup Keys ***********************");
        System.out.println("rectype :: "+rectype);
        System.out.println("section :: "+section);
        System.out.println("before :: "+dataMap);*/

        Map<String,String> map = null;
        String sequenceKey = "";
        if (rectype.equals("additional"))
        {
            if (dataMap != null && dataMap.size() > 0)
            {
                sequenceKey = "all-sections";
                for (String key : dataMap.keySet())
                {
                    map = dataMap.get(key);
                    if (map.get("headerid") != null) {
                        sequenceKey = map.get("headerid");
                        map.put("sequencekey", section + "-" + sequenceKey + ":" + sequenceKey);
                    }
                    else {
                        map.put("sequencekey", "additional" + ":" + sequenceKey);
                    }
                }
            }
        }
        else
        {
            String sequencebreak = getSequenceBreakKeys(rectype, section);
            String[] sequenceKeys = StringUtil.splitToArray(sequencebreak);

            boolean hasKeyData = false;
            if (dataMap!=null && dataMap.size() > 0)
            {
                for (String key : dataMap.keySet())
                {
                    sequenceKey = "";
                    map = dataMap.get(key);
                    if(sequenceKeys != null && sequenceKeys.length > 0)
                    {
                        for (String seqKey : sequenceKeys)
                        {
                            if ( StringUtils.isNotEmpty(map.get(seqKey)) )
                            {
                                sequenceKey += sequenceKey.length()>0 ? "_" + map.get(seqKey).toLowerCase().replaceAll(" ", "_") : map.get(seqKey).toLowerCase().replaceAll(" ", "_");
                                hasKeyData = true;
                            }
                        }
                    }

                    // if no sequencebreak attribute define create a fix sequenceKey
                    if (!hasKeyData) {
                        sequenceKey = "all-"+rectype;
                    }

                    if (StringUtils.isNotEmpty(sequenceKey)) {
                        map.put("sequencekey", section+":" + sequenceKey);
                    }
                }
            }
        }
        /*System.out.println("after :: "+dataMap);*/
    }


    /**
     * To get the sequenceBreakKeys by record-type, if key not exist try to find with section-name
     * @param rectype
     * @param section
     * @return
     */
    private String getSequenceBreakKeys(final String rectype, final String section)
    {
    	String sequencebreak = null;

    	Properties p = PropertyManager.getPropertySet(rectype, "config");
        sequencebreak = p.getProperty("sequencebreak");

        if (StringUtils.isEmpty(sequencebreak))
        {
            p = PropertyManager.getPropertySet(section, "config");
            sequencebreak = p.getProperty("sequencebreak");
        }

    	return sequencebreak;
    }


//    /**
//     * This method iterates through each record in the record map finding records which are associated with the
//     * parent record. When located the associated records are bundled with the main record preview with the key
//     * perviewN, previewN+1...previewN+N and then removed from the recMap.
//     * @param recMap
//     * @return LinkedHashMap recMap
//     */
//@SuppressWarnings("unused")    // FIXME: Was this ever used?  It's a recent addition - Is it soon going to be used?
//    private LinkedHashMap<String,Map<String,String>> resolveReferenceRecords(LinkedHashMap<String,Map<String,String>> recMap)
//    {
//        List<String> removeRecs = new ArrayList<String>();
//        for (String key : recMap.keySet())
//        {
//            if (!key.startsWith("associated"))
//            {
//                Map <String,String>parentRecMap = recMap.get(key);
//                String referencerecs = parentRecMap.get("referencetablerecords");
//                if(referencerecs != null)
//                {
//                    String[] referenceRecArr = referencerecs.split(",");
//                    int referenceSeq = 1;
//                    for (String rec: referenceRecArr)
//                    {
//                        String associatedRecKey = parentRecMap.get("referencetable")+":"+rec;
//                        Map <String,String>associatedRecMap = recMap.get(associatedRecKey);
//                        parentRecMap.put("preview"+referenceSeq++,associatedRecMap.get("preview"));
//                        removeRecs.add(associatedRecKey);
//                    }
//                }
//            }
//        }
//        for (String removeRec : removeRecs)
//        {
//            recMap.remove(removeRec);
//        }
//        return recMap;
//    }


    /**
     * This method retrieves data from UserPersonal, UserAddress, UserPhone, UserEmail, UserLink tables
     * and populates the PersonalData object. But it always takes the display name from MIVUserInfo object from the session.
     *
     * @return PersonalData object -- either data filled object or object with recordid=0, means there is no existing record.
     */
    public PersonalData getPersonalData()
    {
        final String keyColumn = "ID";
        try
        {
            PersonalData personalData = null;
            final List<Address> addresses = new ArrayList<Address>();
            final List<Phone> permanentPhones = new ArrayList<Phone>();
            final List<Phone> currentPhones = new ArrayList<Phone>();
            final List<Phone> officePhones = new ArrayList<Phone>();
            final List<Phone> phones = new ArrayList<Phone>();


            /* **************** Personal Related  ***************************************** */
            SectionQuery[] personalSectionQueryArray = getRecordTypeQueries("personal");
            SectionQuery personalSectionQuery = personalSectionQueryArray[0];
            String personalQuery = personalSectionQuery.query;

            HashMap<String,Map<String,String>> mUserPersonal = runQuery(personalQuery, keyColumn, Integer.toString(userID));

            if (mUserPersonal != null && mUserPersonal.size() > 0)
            {
                final Set<String> keySet = mUserPersonal.keySet();
                final Iterator<String> it = keySet.iterator();
                while (it.hasNext())
                {
                    String idnext = it.next();
                    Map<String, String> personalMap = mUserPersonal.get(idnext);

                    if (personalMap != null && personalMap.size() > 0)
                    {
                        boolean citizenshipFlag = false;
                        int personalInfoRecID = Integer.parseInt(idnext);
                        personalData = new PersonalData(personalInfoRecID);

                        String displayName = personalMap.get("displayname");
                        personalData.setDisplayName(displayName);

                        String birthDate = personalMap.get("birthdate");
                        personalData.setBirthDate(birthDate);

                        citizenshipFlag = "1".equals(personalMap.get("citizen"));

                        personalData.setUSCitizen(citizenshipFlag);
                        String dateEntered = personalMap.get("dateentered");
                        personalData.setDateEntered(dateEntered);
                        String visaType = personalMap.get("visatype");
                        personalData.setVisaType(visaType);
                    }
                }
            }
            else
            {
                personalData = new PersonalData();
            }


            // Getting display name from MIVUserInfo object.
            String displayName = personalData.getDisplayName();
            if (displayName == null || displayName.length() < 1)
            {
                displayName = this.userInfo.getDisplayName();
                personalData.setDisplayName(displayName);
            }

            /* ***************************************************************************** */


            /* *********************** Employee Id Related ********************************* */
            personalData.setEmployeeID(this.userInfo.getEmployeeID());
            /* ***************************************************************************** */


            /* ****************** Phone Related  ******************************************* */
            // NOTE: since we have phone as part of Address object we cannot have phone numbers alone without Address.
            final SectionQuery[] phoneSectionQueryArray = getRecordTypeQueries("phone");
            final SectionQuery phoneSectionQuery = phoneSectionQueryArray[0];
            final String phoneQuery = phoneSectionQuery.query;

            final Map<String,Map<String,String>> mUserPhone = runQuery(phoneQuery, keyColumn, Integer.toString(userID));

            if (mUserPhone != null && mUserPhone.size() > 0)
            {
                Set<String> keySet = mUserPhone.keySet();
                Iterator<String> it = keySet.iterator();
                while (it.hasNext())
                {
                    final String idnext = it.next();
                    final Map<String, String> phoneMap = mUserPhone.get(idnext);

                    if (phoneMap != null && phoneMap.size() > 0)
                    {
                        Phone phone = new Phone();
                        int phoneTypeID = Integer.parseInt(phoneMap.get("phonetypeid"));
                        phone.setPhoneTypeID(phoneTypeID);
                        phone.setNumber(phoneMap.get("number"));
                        phone.setPhoneRecID(Integer.parseInt(idnext));
                        phones.add(phone);
                    }
                }
            }
            // In jsp we are using the PersonalData's phonesList and MobilePhone fields to display phone numbers.
            personalData.setPhonesList(phones);
            /* ***************************************************************************** */


            /* **************** Address Related  ******************************************* */
            final SectionQuery[] addressSectionQueryArray = getRecordTypeQueries("address");
            final SectionQuery addressSectionQuery = addressSectionQueryArray[0];
            final String addressQuery = addressSectionQuery.query;

            final Map<String,Map<String,String>> mUserAddress = runQuery(addressQuery, keyColumn, Integer.toString(userID));

            if (mUserAddress != null && mUserAddress.size() > 0)
            {
                Set<String> keySet = mUserAddress.keySet();
                Iterator<String> it = keySet.iterator();
                while (it.hasNext())
                {
                    String idnext = it.next();
                    Map<String, String> addressMap = mUserAddress.get(idnext);

                    if (addressMap != null && addressMap.size() > 0)
                    {
                        Address address = new Address();
                        address.setAddressRecID(Integer.parseInt(idnext));
                        int addressTypeID = Integer.parseInt(addressMap.get("addresstypeid"));
                        address.setAddressTypeID(addressTypeID);
                        address.setStreet1(addressMap.get("street1"));
                        address.setStreet2(addressMap.get("street2"));
                        address.setState(addressMap.get("state"));
                        address.setZipCode(addressMap.get("zipcode"));
                        address.setCity(addressMap.get("city"));
                        address.setAddressRecType("address");

                        switch (addressTypeID)
                        {
                            case Address.PERMANENT_HOME_ADDRESS:
                                address.setPhones(permanentPhones);
                                break;
                            case Address.CURRENT_HOME_ADDRESS:
                                address.setPhones(currentPhones);
                                break;
                            case Address.OFFICE_ADDRESS:
                                address.setPhones(officePhones);
                                break;
                            case Address.POSTAL_ADDRESS:
                                log.info("MyInfoVault does not currently use the POSTAL (aka Mailing) Address type.");
                                break;
                            default:
                                log.warn("An unknown address type [{}] was found in the personal data for user {}", addressTypeID, userID);
                                break;
                        }

                        addresses.add(address);
                    }
                }
            }
            personalData.setAddresses(addresses);
            /* **************************************************************************** */


            /* **************** Email Related  ******************************************** */
            final SectionQuery[] emailSectionQueryArray = getRecordTypeQueries("email");
            final SectionQuery emailSectionQuery = emailSectionQueryArray[0];
            final String emailQuery = emailSectionQuery.query;

            final Map<String,Map<String,String>> mUserEmail = runQuery(emailQuery, keyColumn, Integer.toString(userID));

            if (mUserEmail != null && mUserEmail.size() > 0)
            {
                Set<String> keySet = mUserEmail.keySet();
                Iterator<String> it = keySet.iterator();
                while (it.hasNext())
                {
                    String idnext = it.next();
                    Map<String, String> emailMap = mUserEmail.get(idnext);

                    if (emailMap != null && emailMap.size() > 0)
                    {
                        personalData.setEmailRecID(emailMap.get("id"));
                        personalData.setEmail(emailMap.get("value"));
                    }
                }
            }
            /* **************************************************************************** */


            /* **************** URL Related  ********************************************** */
            final SectionQuery[] urlSectionQueryArray = getRecordTypeQueries("link");
            final SectionQuery urlSectionQuery = urlSectionQueryArray[0];
            final String urlQuery = urlSectionQuery.query;

            final Map<String,Map<String,String>> mUserLink = runQuery(urlQuery, keyColumn, Integer.toString(userID));

            if (mUserLink != null && mUserLink.size() > 0)
            {
                Set<String> keySet = mUserLink.keySet();
                Iterator<String> it = keySet.iterator();
                while (it.hasNext())
                {
                    String idnext = it.next();
                    Map<String, String> linkMap = mUserLink.get(idnext);

                    if (linkMap != null && linkMap.size() > 0)
                    {
                        personalData.setWebsiteRecID(linkMap.get("id"));
                        personalData.setWebsite(linkMap.get("value"));
                    }
                }
            }
            /* **************************************************************************** */

            return personalData;
        }
        catch (Exception e)
        {
            log.error("Error while collecting Personal Information data for user " + userID, e);
            return new PersonalData();
        }
    }
    //getPersonalData()


    /**
     * Get the section headers for this user. A Map is returned, keyed by the section name
     * (e.g. "journals-submitted" or "teaching-curriculum")
     * The standard system headers are retrieved, then any user customized headers replace the standard
     * headers in the map.
     *
     * @return Map keyed by section name, containing fieldname-to-value maps.
     */
    public LinkedHashMap<String, Map<String,String>> getHeaderMap()
    {
        return getHeaderMap("");
    }
    //getHeaderMap()


    public LinkedHashMap<String , Map<String,String>> getHeaderMap(final String strRecordName)
    {
        final QueryTool qt1 = MIVConfig.getConfig().getQueryTool();

        final StringBuilder systemHeaderQuery = new StringBuilder();
        systemHeaderQuery.append("SELECT SystemSectionHeader.id, Section.Name, Header, SystemSectionHeader.display")
                         .append(" FROM Section, SystemSectionHeader")
                         .append(" WHERE Section.ID = SystemSectionHeader.SectionID");
        if (strRecordName != null && !("".equals(strRecordName))) {
            systemHeaderQuery.append(" AND Section.RecordName = '" + strRecordName + "'");
        }

        final StringBuilder userMasterHeaderQuery = new StringBuilder();
        userMasterHeaderQuery.append("SELECT UserSectionHeader.id, Section.Name, Header, UserSectionHeader.display")
                       .append(" FROM Section, UserSectionHeader")
                       .append(" WHERE UserID = " + userID)
                       .append(" AND Section.ID = UserSectionHeader.SectionID");

        final LinkedHashMap<String, Map<String, String>> systemSectionHeaderMap =
            qt1.runQuery(systemHeaderQuery.toString(), "name", (String[]) null);
        final LinkedHashMap<String, Map<String, String>> userSectionHeaderMap =
            qt1.runQuery(userMasterHeaderQuery.toString(), "name", (String[]) null);

        systemSectionHeaderMap.putAll(userSectionHeaderMap);

        Map<String,String> headerRec = null;
        for (String section : systemSectionHeaderMap.keySet())
        {
            headerRec = systemSectionHeaderMap.get(section);
            String header = headerRec.get("header");
            header = MIVUtil.escapeMarkup(header);
            // IE does not honor &apos; so use &#39; instead
            header = header.replaceAll("\"", "&quot;").replaceAll("'", "&#39;");
            headerRec.put("header", header);
        }

        return systemSectionHeaderMap;
    }
    //getHeaderMap(String strRecordName)


    public LinkedHashMap<String , Map<String,String>> getSystemHeaderMap()
    {
        QueryTool qt1 = MIVConfig.getConfig().getQueryTool();

        StringBuilder systemHeaderQuery = new StringBuilder();
        systemHeaderQuery.append("SELECT SystemSectionHeader.id, Section.Name, Header, SystemSectionHeader.display")
                         .append(" FROM Section, SystemSectionHeader")
                         .append(" WHERE Section.ID = SystemSectionHeader.SectionID");


        LinkedHashMap<String, Map<String, String>> systemSectionHeaderMap =
            qt1.runQuery(systemHeaderQuery.toString(), "name", (String[]) null);

        Map<String,String> headerRec = null;
        for (String section : systemSectionHeaderMap.keySet())
        {
            headerRec = systemSectionHeaderMap.get(section);
            String header = headerRec.get("header");
            header = MIVUtil.escapeMarkup(header);
            // IE does not honor &apos; so use &#39; instead
            header = header.replaceAll("\"", "&quot;").replaceAll("'", "&#39;");
            headerRec.put("header", header);
        }
        return systemSectionHeaderMap;
    }
    //getSystemHeaderMap()


    public FormatManager getFormatManager()
    {
        return this.formatting;
    }


    /**
     * Get the DataSource that Connections will use.
     * @return The DataSource
     */
    private DataSource getDataSource()
    {
        DataSource ds = null;
        if (MIVConfig.isInitialized())
        {
            ds = MIVConfig.getConfig().getDataSource();
            if (ds == null)
            {
                log.warn("SectionFetcher failed to retrieve the dataSource");
            }
        }
        return ds;
    }


    /**
     * Get a new connection from the pool
     * @return connection
     * @throws SQLException
     */
    private Connection getConnection() throws SQLException
    {
        if (this.source == null) this.source = this.getDataSource();
        Connection connection = null;
        log.trace("Getting new connection");
        connection = source.getConnection();
        return connection;
    }


    /**
     * Release connection to the pool
     * @param connection The connection being released.
     * @throws SQLException
     */
    private void releaseConnection(Connection connection) throws SQLException
    {
        log.trace("Releasing connection");
        if (connection != null)
        {
            connection.commit();
            connection.close();
            connection = null;
        }
    }


    /**
     * Get the queries available for a given record type.
     * @param rectype the record-type string to look up.
     * @return an array with query information for each section found
     */
    private SectionQuery[] getRecordTypeQueries(final String rectype)
    {
        final String metaquery =
            "SELECT DISTINCT ID, Name, RecordName, SelectFields, FromTable, WhereClause, OrderBy" +
            " FROM Section LEFT JOIN DocumentSection ON ID=SectionID" +
            " WHERE RecordName = ?" +
            " ORDER BY Sequence ASC";
        return getRecordTypeQueries(metaquery, rectype + "-record", null);
    }
    //getRecordTypeQueries(rectype)


    /**
     * Get the queries available for a given section ids.
     *
     * @param sectionIds comma separated sections ID numbers.
     * @param ruleSet ruleSet object which contains the record criteria to limit the records retrieved.
     * @return an array with query information for each section found
     */
    @SuppressWarnings("unused")
    private SectionQuery[] getRecordTypeQueries(final String sectionIds, final Iterable<Ruleset> ruleSet)
    {
        final String [] sections = sectionIds.split(",");

        final StringBuilder metaquery = new StringBuilder(
            "SELECT DISTINCT ID, Name, RecordName, SelectFields, FromTable, WhereClause, OrderBy" +
            " FROM Section LEFT JOIN DocumentSection ON ID=SectionID" +
            " WHERE SectionID IN (");

        for (String section : sections)
        {
            metaquery.append("?,");
        }
        metaquery.deleteCharAt(metaquery.length()-1);

        metaquery.append(")").append(" ORDER BY Sequence ASC");
        return getRecordTypeQueries(metaquery.toString(), sectionIds, ruleSet);
    }
    //getRecordTypeQueries(String sectionIds, Iterable<Ruleset> ruleSet))


    /**
     * Get the queries available for a given record type.
     *
     * @param ruleSet ruleSet object which contains the record criteria to limit the records retrieved.
     * @return an array with query information for each section found
     */
    private SectionQuery[] getRecordTypeQueries(final String metaquery, final String placeHolderValue, final Iterable<Ruleset> ruleSet)
    {
        final List<SectionQuery> q = new LinkedList<SectionQuery>();
        final SectionQuery[] q2 = {}; // used only for the toArray() prototype on return.
        int count = 0;
        log.debug("getRecordTypeQueries : looking for rectype or sections={}", placeHolderValue);

        PreparedStatement statement = null;
        ResultSet rset = null;
        Connection connection = null;
        try
        {
            connection = getConnection();
            statement = connection.prepareStatement(metaquery);
            //statement.setString(1, rectype + "-record");

            String [] placeHolderArray = placeHolderValue.split(",");
            int index = 1;
            for (String placeHolder : placeHolderArray)
            {
                statement.setString(index++, placeHolder);
            }

            rset = statement.executeQuery();
            String noLimitPattern = " limit \\d+";
            while (rset.next())
            {
                String key = rset.getString("Name");
                String rectype = rset.getString("RecordName");
                if (rectype != null && rectype.endsWith("-record")) {
                    rectype = rectype.substring(0, rectype.length() - 7);
                }
                String heading = null;//rset.getString(1); // we don't have headings yet; use the section name for now.
                if (rset.getString("SelectFields") != null)
                {
                    String query = buildQuery(
                            rset.getString("SelectFields"), rset.getString("FromTable"),
                            rset.getString("WhereClause"), rset.getString("OrderBy"), ruleSet);
                    query = query.replaceAll(noLimitPattern, ""); // SDP 2007-03-22 get rid of "limit 5"
                    //String query = rset.getString("RecordQuery").replaceAll(noLimitPattern, ""); // SDP 2007-03-22 get rid of "limit 5"
                    int sectionID = rset.getInt("ID");
                    log.debug("Adding section with key=[{}], id=[{}], heading=[{}], query=[{}]",
                              new Object[] { key, sectionID, heading, query });
                    q.add(new SectionQuery(key, rectype, sectionID, heading, query));
                    count++;
                }
            }
        }
        catch (SQLException sqe)
        {
            log.error("Couldn't obtain record type or sectionids \""+placeHolderValue+"\" query set for display", sqe);
        }
        finally
        {
            if (rset != null)    try { rset.close();   rset = null;   } catch (Exception e) {}
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        log.trace("{}.getRecordTypeQueries returning an array of length {}", this.getClass().getSimpleName(), count);

        return q.toArray(q2);
    }
    //getRecordTypeQueries()


    private static final String sectionQuery = "SELECT DISTINCT ID, Name, RecordName, SelectFields, FromTable, WhereClause, SectionPrimaryKeyID, OrderBy FROM Section WHERE RecordName=? AND Name=? ";

    /**
     * Get section query pieces by record type and section name
     * @param recordType type of the record
     * @param section name of the section for the record
     * @return Map
     */
    protected Map<String,String> getSectionQueryMap(final String recordType, final String section){

        Map<String,String> sectionQueryMap = null;

        final QueryTool qt = MIVConfig.getConfig().getQueryTool();

        final List<Map<String, String>> dataList = qt.getList(sectionQuery, recordType, section);

        if (dataList != null && dataList.size() > 0) {
            sectionQueryMap = dataList.get(0);
        }
        return sectionQueryMap;
    }


    /**
     * Perform a query and return records mapped to a provided key field.
     * @param query
     * @param key The name of the field to use as the map key
     * @param args The arguments to use to set any required query parameters.
     * @return a {@link java.util.LinkedHashMap LinkedHashMap} associating keys to records.
     */
    private LinkedHashMap<String,Map<String,String>> runQuery(final String query, final String key, final String... args)
    {
        final LinkedHashMap<String, Map<String,String>> m = new LinkedHashMap<String, Map<String,String>>();
        PreparedStatement statement = null;
        ResultSet results = null;
        Connection connection = null;

// TODO: use a QueryTool instead - it was generalized from this.
        try
        {
            connection = getConnection();

            statement = connection.prepareStatement(query);

            // Fill in the query parameters from the varargs passed in.
            int lastPosition = 0;
            int parmPosition = 0;
            int count = 0;
            while ((parmPosition = query.indexOf('?', lastPosition)) != -1)
            {
                log.debug("setting '?' #{} to {}", count + 1, args[count]);
                statement.setString(count + 1, args[count]);
                lastPosition = parmPosition + 1;
                count++;
            }

            log.debug("runQuery: query is [{}]", query);

            // these 2 vars only used for time accounting
            final long startTime = System.currentTimeMillis();
            int rowCount = 0;

            results = statement.executeQuery();

            ResultSetMetaData metaData = results.getMetaData();
            int colCount = metaData.getColumnCount();

            while (results.next())
            {
                rowCount++;
                String reckey = results.getString(key);
                Map<String,String> fieldmap = new HashMap<String,String>();

                for (int i = 1; i <= colCount; i++)
                {
                    String colname = metaData.getColumnLabel(i).toLowerCase();
                    String colvalue = results.getString(i);
                    //if ("title".equals(colname)) System.out.println("colname=["+colname+"]  colvalue=["+colvalue+"]");
                    int colType = metaData.getColumnType(i);
                    //        System.out.println("Database type for column "+colname+" is "+colType);
                    switch (colType)
                    {
                        case java.sql.Types.BOOLEAN:
                        case java.sql.Types.BIT:
                            log.debug("  SectionFetcher: PROCESSING BIT/BOOLEAN COLUMN '{}'", metaData.getColumnLabel(i));
                            if ("true".equals(colvalue))
                            {
                                colvalue="1";
                            }
                            else if ("false".equals(colvalue))
                            {
                                colvalue="0";
                            }
                            break;
                        default:
                            // nothing for any other kind of field
                            break;
                    }

                    fieldmap.put(colname, colvalue);
                }

                log.debug("\nPutting { [{}] [{}] }", reckey, fieldmap);
                m.put(reckey, fieldmap);
            }

            // Do a little accounting so we can check performance when we want to tune.
            if (MIVConfig.getConfig().getDoTimings())
            {
                long endTime = System.currentTimeMillis();
                long duration = endTime - startTime;
                long seconds = duration / 1000;
                long msecs = duration % 1000;
                log.info(" {} rows fetched in {}.{} seconds", new Object[] { rowCount, seconds, msecs });
            }
        }
        catch (SQLException sqe)
        {
            log.error("Query Failed for user: " + userID, sqe);
            log.error("  Query was: [{}]", query);
        }
        finally
        {
            if (results != null)    try { results.close();   results = null;   } catch (Exception e) {}
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        return m;
    }
    //runQuery()


    @Override
    protected void finalize() throws Throwable
    {
//        System.out.println("Releasing SectionFetcher for user "+userID+" during finalization. ["+this+"]");
        try {
            this.source = null;
        }
        finally {
            super.finalize();
        }
    }


    /**
     * A convenience for simpler declarations and handling of records internally.
     * This is more readable and uses less code than using {@link java.util.Map Map}s as
     * the more generalized column--&gt;value mappings do.
     */
    class SectionQuery
    {
        final int    sectionID;
        final String key;
        final String rectype;
        final String heading;
        final String subheading;
        final String yearstyle;
        final String query;

        public SectionQuery(final String key, final String rectype, final int sectionID, final String heading, final String query)
        {
            this(key, rectype, sectionID, heading, (String)null, query);
        }

        public SectionQuery(final String key, final String rectype, final int sectionID, final String heading, final String subheading, final String query)
        {
            this(key, rectype, sectionID, heading, subheading, (String)null, query);
        }

        public SectionQuery(final String key, final String rectype, final int sectionID, final String heading, final String subheading, final String yearstyle, final String query)
        {
            this.key = key;
            this.rectype = rectype;
            this.sectionID = sectionID;
            this.heading = heading;
            this.subheading = subheading;
            this.yearstyle = yearstyle;
            this.query = query;
        }

        @Override
        public String toString()
        {
            return "["+this.key+"]["+this.sectionID+"]["+this.heading+(this.subheading!=null?" ("+this.subheading+")":"")+"]["+this.query+"]";
        }
    }
    //SectionQuery inner class
}
