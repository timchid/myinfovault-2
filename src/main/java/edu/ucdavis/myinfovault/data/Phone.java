package edu.ucdavis.myinfovault.data;

/**
 * This is simple java class used to store different phone numbers based on phone types.
 * @author Venkat Vegesna
 */
public class Phone
{
    /* These must match the entries in the PhoneType table. */
    // TODO: make these an enum
    public static final int PERMANENT_HOME_PHONE = 1;
    public static final int PERMANENT_FAX        = 2;
    public static final int CURRENT_HOME_PHONE   = 3;
    public static final int CURRENT_FAX          = 4;
    public static final int OFFICE_PHONE         = 5;
    public static final int OFFICE_FAX           = 6;
    public static final int MOBILE_PHONE         = 7;

    private String number;
    private int phoneRecID;
    private int phoneTypeID;

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        if (number != null) number = number.replaceAll("\"", "&quot;");
        this.number = number;
    }

    public int getPhoneRecID()
    {
        return phoneRecID;
    }

    public void setPhoneRecID(int phoneRecID)
    {
        this.phoneRecID = phoneRecID;
    }

    public int getPhoneTypeID()
    {
        return phoneTypeID;
    }

    public void setPhoneTypeID(int phoneTypeID)
    {
        this.phoneTypeID = phoneTypeID;
    }

    public String toString()
    {
        return this.number;
    }
}
