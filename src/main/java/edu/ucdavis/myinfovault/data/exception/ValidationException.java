/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ValidationException.java
 */


package edu.ucdavis.myinfovault.data.exception;

/**
 * Signals that a field contains an invalid value.
 *
 * @author Stephen Paulsen
 * @since MIV 3.3
 */
public class ValidationException extends Exception
{
    private static final long serialVersionUID = 201001271202L;

    private String field;
    private String value;

    public ValidationException(String field, String value, String message)
    {
        super(message);
        this.field = field;
        this.value = value;
    }

    /**
     * Get the name of the field that failed validation.
     * @return the field name
     */
    public String getField()
    {
        return this.field;
    }

    /**
     * Get the value that was not valid for the field.
     * This value <em>may</em> be altered from the original field contents,
     * for example a String of all spaces may be trimmed leaving a required
     * field blank. The empty String ("") may be returned in place of the
     * original String with the spaces.
     * @return the value of the invalid field
     */
    public String getValue()
    {
        return this.value;
    }
}
