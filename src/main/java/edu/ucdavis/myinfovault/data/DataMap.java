package edu.ucdavis.myinfovault.data;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
//import java.util.Map;
import java.util.Set;

/**
 * <p>
 * @author Lawrence Fyfe
 * </p>
 */
public class DataMap implements Iterable<String>
{
    private LinkedHashMap<String,Object> map;

    public DataMap()
    {
        map = new LinkedHashMap<String,Object>();
    }
    
    public DataMap(Map<String, String>inputMap) {
        map = new LinkedHashMap<String,Object>();
        for (String key : inputMap.keySet()) {
            map.put(key, inputMap.get(key));
        }
    }
    

    public Iterator<String> iterator()
    {
        Iterator<String> iterator = map.keySet().iterator();

        return iterator;
    }

    public Object get(String key)
    {
        return map.get(key);
    }

    public void put(String key, Object value)
    {
        map.put(key, value);
    }

    public int size()
    {
        return map.size();
    }

    public boolean isEmpty()
    {
        return map.isEmpty();
    }

    public Set<String> keySet()
    {
        return map.keySet();
    }

    public boolean containsKey(String key)
    {
        /*boolean contains = false;

        if (map.containsKey(key))
        {
            contains = true;
        }

        return contains;*/
        return map.containsKey(key);
    }

    public Map<String,Object> getMap()
    {
    	return new LinkedHashMap<String,Object>(map);
    }

    @Override
    public String toString()
    {
        return this.map!=null?this.map.toString():null;
    }
}
