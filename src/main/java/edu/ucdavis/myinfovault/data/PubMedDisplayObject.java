package edu.ucdavis.myinfovault.data;

/**
 * Apparantly obsolete following Craig's re-write of PubMed data imports.
 * @author venkatv
 * @deprecated
 */
@Deprecated
public class PubMedDisplayObject
{

    String pubMedRecID;
    String description;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getPubMedRecID()
    {
        return pubMedRecID;
    }

    public void setPubMedRecID(String pubMedRecID)
    {
        this.pubMedRecID = pubMedRecID;
    }
}
