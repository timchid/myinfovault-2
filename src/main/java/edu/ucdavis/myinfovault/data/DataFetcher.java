/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DataFetcher.java
 */

package edu.ucdavis.myinfovault.data;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao;
import edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao;
import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.format.FormatManager;
import edu.ucdavis.myinfovault.htmlcleaner.HtmlCleaner;
import edu.ucdavis.myinfovault.htmlcleaner.InvalidMarkupException;

/**
 * @author Lawrence Fyfe
 * @since MIV 2.0
 */
public class DataFetcher extends Fetcher
{
    private static final Logger log = LoggerFactory.getLogger(DataFetcher.class);
    public static final String ANNOTATIONS_LEGENDS = "annotations-legends";

    private final int userID;
    private final FormatManager formatting;// = null;
    private final HtmlCleaner wysiwygCleaner;// = null;

    private final Map<String, Map<String, String>> documentByAttributeNameMap = MIVConfig.getConfig().getMap("documentsbyattributename");
    private final Map<String, String> creativeActivitiesDocAttributes = documentByAttributeNameMap.get("creativeactivities");
    private final Map<String, String> worksContributionsDocAttributes = documentByAttributeNameMap.get("workscontributions");
    private final AnnotationDao annotationDao = (AnnotationDao) MivServiceLocator.getBean("annotationDao");
    private final DataFetcherDao dataFetcherDao = (DataFetcherDao) MivServiceLocator.getBean("dataFetcherDao");
    private final PacketService packetService = MivServiceLocator.getPacketService();

    public DataFetcher(MIVUserInfo userInfo)
    {
        this.userID = userInfo.getPerson().getUserId();

        formatting = new FormatManager(userInfo);
        wysiwygCleaner = new HtmlCleaner("edu.ucdavis.myinfovault.format.wysiwyg");
    }


    /**
     * Get the DataMap of the First and Last name for this user
     * @return DataMap - User first and last name
     */
    public DataMap getWholeNameMap()
    {
        return dataFetcherDao.getWholeNameMap(userID);
    }

    /**
     * Get the primary department name for this user
     * @return String - department name
     */
    public String getDepartment()
    {
        return dataFetcherDao.getDepartment(userID);
    }

    public List<Map<String, Object>> getSectionById(Integer sectionId)
    {
        return dataFetcherDao.getSectionBySectionId(sectionId, userID);
    }

    /**
     * Get a DataMap of the data for the input section
     * @param section - Map of the section table records from which to build queries to retrieve the data
     * @return DataMap of the section
     */
    private DataMap getSectionMap(Map<String, Object> section, Packet packet)
    {
        return getSectionMap(dataFetcherDao.getSectionData(userID, section, packet), section, packet.getPacketId());
    }

    /**
     * Get a DataMap of the data for the input section
     * @param sectionRecords - List of Maps representing the records retrieved
     * @param section - Map of the section table records from which to build queries to retrieve the data
     * @param packetId - packetId of the section table records
     * @return DataMap of the section
     */
    private DataMap getSectionMap(List<Map<String, Object>> sectionRecords, Map<String, Object> section, long packetId)
    {
        LinkedList<DataMap> sectionRecordList = new LinkedList<DataMap>();
        DataMap sectionRecordMap = null;
        DataMap sectionMap = null;

        // Get any annotations for this record type.
        if (sectionRecords == null || sectionRecords.isEmpty())
        {
            return sectionMap;
        }

        String recordName = (String)section.get("RecordName");
        String recType = recordName.replace("-record", "");
        String sectionBaseTableName = (String)section.get("SectionBaseTable");

        // Get any annotations that may be present for this record
        Map<String,Annotation> annotationRecordMap =
            annotationDao.getAnnotationsForDisplay(userID, packetId, recType);

        for (Map<String, Object> sectionRecord : sectionRecords)
        {
//            if (!displayRecord(sectionRecord.get("Display")))
//            {
//                continue;
//            }

            sectionRecordMap = new DataMap();

            if (sectionRecord.get("ID") != null)
            {
                int recordID = (Integer)sectionRecord.get("ID");
                if (recordID != 0)
                {
                    String annotationKey = recordID + "-" + sectionBaseTableName;
                    Annotation annotationRecord = annotationRecordMap.get(annotationKey);

                    // Footnotes, notations and labels
                    if(annotationRecord!=null)
                    {
                        addAnnotation(annotationRecord, sectionRecordMap);
                    }
                }
            }
            // Build a DataMap for this sectionRecord
            for (String column : sectionRecord.keySet())
            {
                // Skip null and empty values
                Object value = sectionRecord.get(column);
                if (value == null || (value instanceof String && ((String) value).trim().isEmpty()))
                {
                    continue;
                }

                sectionRecordMap.put(column.toLowerCase(), sectionRecord.get(column).toString());
            }
            // Process any markup in the record and add to the section record list
            processMarkup(recordName, sectionRecordMap);
            sectionRecordList.add(sectionRecordMap);
        }

        // Create SectionMap which contains a list of section records keyed by the record name
        if (sectionRecordList != null && sectionRecordList.size() > 0)
        {
            sectionMap = new DataMap();
            sectionMap.put(recordName, sectionRecordList);
        }

        return sectionMap;
    }

    /**
     * TODO: This could be refactored in some manner since the getSectionMap method is nearly identical
     * Get a DataMap of the associated data for the input section
     * @param section - Map of the section table records from which to build queries to retrieve the data
     * @param packet - packet to which the section table records belong
     * @return DataMap of the section
     */
    private DataMap getAssociatedSectionMap(int parentRecordID, Map<String, Object> section, Packet packet)
    {
        return getSectionMap(dataFetcherDao.getSectionData(parentRecordID, section, packet),section, packet.getPacketId());
    }



    /**
     * Get a DataMap of the additional information data for the input section
     * @param section - Map of the additional header section table records from which to build queries to retrieve
     * additional header data.The additional header data is then used to retrieve the additional information records
     * themselves.
     * @return DataMap of the additional information section
     */
    private DataMap getAdditionalInformationSectionMap(Map<String, Object> section, Packet packet)
    {
        DataMap sectionMap = null;

        // This will get a list of additional information header records
        List<Map<String, Object>> sectionData = dataFetcherDao.getSectionData(userID, section, packet);
        if (sectionData == null || sectionData.isEmpty())
        {
            return sectionMap;
        }
        // Now get the data for each header record
        LinkedList<DataMap> list = new LinkedList<DataMap>();
        for (Map<String, Object> sectionRecord : sectionData)
        {
            int headerID = (Integer) sectionRecord.get("ID");
            String value = (String) sectionRecord.get("Value");
            DataMap additionalHeaderMap = new DataMap();
            additionalHeaderMap.put("addheader", MIVUtil.escapeMarkup(value));
            additionalHeaderMap.put("id", Integer.toString(headerID));
            additionalHeaderMap.put("display", ((Boolean)sectionRecord.get("Display")).toString());
            list.add(additionalHeaderMap);

            // Use section ID of 0 (zero) for additional information
            List<Map<String, Object>> additionalInformationRecordList =
                    dataFetcherDao.getAdditionalInformation(userID, headerID, 0 /*(Integer)section.get("SectionID")*/, packet);

            if  (additionalInformationRecordList == null || additionalInformationRecordList.isEmpty())
            {
                continue;
            }

            for (Map<String, Object> additionalInformationRecord : additionalInformationRecordList)
            {
                    DataMap contentMap = new DataMap();
                    String content = (String)additionalInformationRecord.get("Content");
                    Integer id = (Integer)additionalInformationRecord.get("ID");
                         content = cleanWysiwygMarkup("addcontent", content);
                    contentMap.put("addcontent", content);
                    contentMap.put("id", Integer.toString(id));
                    contentMap.put("display",((Boolean)additionalInformationRecord.get("Display")).toString());
                    list.add(contentMap);
            }
        }

        if (list != null && list.size() > 0)
        {
            sectionMap = new DataMap();
            sectionMap.put((String)section.get("RecordName"), list);
        }
        return sectionMap;
    }


    /**
     * Add an annotation record to the input DataMap for a record
     * @param annotationRecord - A record of the annotations to add to the input record DataMap
     * @param dataMap - The DataMap of the input record
     */
    private void addAnnotation(Annotation annotationRecord, DataMap dataMap)
    {
        String footnote;
        String notation;

        if (annotationRecord != null)
        {
            footnote = annotationRecord.getFootnote();
            notation = annotationRecord.getNotation();

            if (StringUtils.isNotEmpty(footnote))
            {
                footnote = MIVUtil.escapeMarkup(footnote);
                dataMap.put("footnote", MIVUtil.replaceNewline(footnote));
            }

            if (StringUtils.isNotEmpty(notation))
            {
                dataMap.put("notation", notation);
            }

            dataMap.put("labelabove", annotationRecord.getLabelAbove());
            dataMap.put("labelbelow", annotationRecord.getLabelBelow());
        }
    }

    /**
     * Fetch the the section data for the input document ID and packetID
     * @param documentID - The documentID of the document for which to retrieve the data
     * @param packetID - The packetID for which to retrieve data.
     * @return DataMap of the records which comprise the document.
     */
    public DataMap fetch(int documentID, long packetID)
    {
        DataMap documentMap = new DataMap();
        DataMap sectionMap = null;
        String oldDset = null;
        String recordName = null;
        Packet packet = packetService.getPacket(packetID);

        List<Map<String, Object>> sections = this.getSections(documentID);

        // If there are no sections, we are done
        if (sections == null || sections.isEmpty())
        {
            return documentMap;
        }

        // Iterate through each of the section queries and select the data
        for (Map<String, Object> section : sections)
        {
            // Build the section query
            oldDset = (String)section.get("OldDset");
            recordName = (String)section.get("RecordName");
            if (recordName.equalsIgnoreCase("additional-record") && oldDset != null)
            {
                sectionMap = this.getAdditionalInformationSectionMap(section, packet);
            }
            else
            {
                sectionMap = this.getSectionMap(section, packet);
            }

            // Process the records in sectionMap
            if (sectionMap != null)
            {
                boolean includeAllParents = true;
                // Resolve any associated records for creative activities or worksContributions/
                if ((creativeActivitiesDocAttributes != null && creativeActivitiesDocAttributes.get("id").equals(""+documentID) ||
                    worksContributionsDocAttributes != null && worksContributionsDocAttributes.get("id").equals(""+documentID)) &&
                        !recordName.equals("additional-record"))
                {
                    // See whether or not to include all parent records when there are creative activities
                    String value = MIVConfig.getConfig().getProperty(recordName+"-dossier-includeallparents");
                  //includeAllParents = value != null ? (value.equalsIgnoreCase("true") ? true : false) : true;
                    includeAllParents = value == null ? true : value.equalsIgnoreCase("true");

                    // The section records must be sorted in a different order from the standard
                    // the display output. The printed output must be sorted in type, date order
                    // to allow the printed output to be organized by type.
                    String sortKey = MIVConfig.getConfig().getProperty(recordName+"-sortfield");
                    sectionMap = this.processAssociatedRecords(sectionMap, packet, includeAllParents, sortKey);
                }
                if (sectionMap != null && !sectionMap.isEmpty())
                {
                    List<String> annotationsLegendsList = null;
                    int sectionID = (Integer)section.get("SectionID");
                    if (includeAllParents)
                    {
                        //Adding annotations legends list into sectionMap
                        annotationsLegendsList =  annotationDao.getAnnotationsLegend(String.valueOf(userID), String.valueOf(sectionID));
                    }
                    else
                    {
                        @SuppressWarnings("unchecked")
                        String[] validParentIDs = getValidParentIDs((List<DataMap>)sectionMap.getMap().get(recordName));
                        annotationsLegendsList = annotationDao.getAnnotationsLegend(String.valueOf(userID), String.valueOf(sectionID), validParentIDs);
                    }

                    if (annotationsLegendsList != null && annotationsLegendsList.size() > 0)
                    {
                        sectionMap.put(ANNOTATIONS_LEGENDS, annotationsLegendsList);
                    }

                    documentMap.put((String)section.get("Name"), sectionMap);
                }
            }
        }
        return documentMap;
    }

    /**
     * Get the sections from the Section/SnapshotSection table for the input documentID and packetID.
     *  A value of 0 for the dossierID indicates that this dossier is not in process and section data
     *  will be read from the standard section table.
     *  A value of > 0 indicates this dossier is in process and the section data will be read from the
     *  snapshotsection table.
     * @param documentID - The documentID of the document for which to retrieve the data
     * @return
     */
    private List<Map<String, Object>> getSections(int documentID)
    {
        // Get the sections for this documentID.
        return dataFetcherDao.getSectionsByDocumentID(documentID);
    }

    /**
     * Get the ids of all records that have associated records.
     * @param dataList   list of DataMaps
     * @return  Ids of records that have associated records.
     */
    private String[] getValidParentIDs(List<DataMap> dataList)
    {
        String validParentIDs = "";

        if (dataList != null && dataList.size() > 0)
        {
            for (DataMap data : dataList)
            {
                if (data != null && data.get("parentrecordid") != null)
                {
                    if (validParentIDs.trim().length() == 0) {
                        validParentIDs = data.get("parentrecordid").toString();
                    }
                    else {
                        validParentIDs += "," + data.get("parentrecordid").toString();
                    }
                }
            }
        }

        if (validParentIDs == null || validParentIDs.trim().length() == 0)
        {
            validParentIDs = "0";
        }
        return validParentIDs.split(",");
    }


    /**
     * Get the records associated with the input parentRecordID and section name
     * @param parentRecordID - The parent record ID
     * @param sectionName - The section name
     * @param packet - packet to which the records belong
     * @return DataMap of the associated records
     */
    private DataMap fetchAssociatedRecords(int parentRecordID, String sectionName, Packet packet)
    {
        DataMap documentMap = new DataMap();
        DataMap sectionMap = null;

        // Get the associatedRecordSections
        List<Map<String, Object>> associatedSectionList = dataFetcherDao.getAssociatedSectionsByName(sectionName);

        if (associatedSectionList == null || associatedSectionList.isEmpty())
        {
            return sectionMap;
        }

        for (Map<String, Object> associatedSection : associatedSectionList)
        {

            sectionMap = this.getAssociatedSectionMap(parentRecordID, associatedSection, packet);
            if (sectionMap != null)
            {
                documentMap.put((String)associatedSection.get("Name"), sectionMap);
            }
        }
        return documentMap;
    }

    /**
     * processAssociatedRecords - This method iterates through an input DataMap and checks each
     * record in the input map to see if it references another record and and if it does the associated records
     * are retrieved. The parent records and associated records are built into separate
     * maps of the parent and associated records. The resulting parent record and associated record
     * maps are then input to the buildSectionMap method which builds a new DataMap which will contain each of the
     * parent records with the nested associated records.
     *
     * The input DataMap will contain multiple parent entries for the same record when that record
     * references multiple records. The DataMap which is built will contain only the single parent
     * record and the nested associated records.
     *
     * @param sectionMap - DataMap of parent records
     * @param packet - packet to which the associated records belong
     * @param includeAllParentRecords - true will include all records regardless of association to other records
     *                                  false only parent records associated with children records will be included
     * @param sortKey - optional sort key to allow the section records to be sorted in a different order from the
     * the display output. This is currently used for creative activities records where the displayed output on the
     * itemlist page is in date order regardless of the type of record. However the printed output must be sorted
     * in type, date order to allow the printed output to be organized by type.
     * @return sectionMap - rebuilt sectionMap which now contains the parent records and with nested child records.
     */
    @SuppressWarnings("unchecked") // for LinkedList<DataMap> cast
    private DataMap processAssociatedRecords(DataMap sectionMap, Packet packet, boolean includeAllParentRecords, final String sortKey)
    {
        DataMap newSectionMap = sectionMap;

        if (sectionMap != null && !sectionMap.isEmpty())
        {
            Map<String, DataMap> parentRecordMap = new LinkedHashMap<String,DataMap>();
            Map<String, DataMap> associatedRecordMap = new LinkedHashMap<String,DataMap>();

            // Iterate through the sectionMap and build maps of the parent and associated children records.
            Map<String,Object> recordMap = sectionMap.getMap();
            for (String recordTypeKey : recordMap.keySet())
            {
                LinkedList<DataMap> records = (LinkedList<DataMap>) recordMap.get(recordTypeKey);

                // Sort the records by the input sortKey if any
                if (sortKey != null && sortKey.length() != 0 && records.contains(sortKey))
                {
                    Comparator<DataMap> comparator = new Comparator<DataMap>() {
                        @Override
                        public int compare(DataMap a, DataMap b) {
                            return ((String) a.get(sortKey)).compareTo((String) b.get(sortKey));
                        }
                    };

                    Collections.sort(records, comparator);
                }

                for (int rec = 0 ; rec < records.size() ; rec++)
                {
                    DataMap record = records.get(rec);

                    // Get the parent recordID to be used as a constraint to the select as well as in the recordMaps being built
                    Integer parentRecordID = Integer.parseInt((String)record.get("parentrecordid"));

                    // If the record contains referencerecordids (there may be multiple), retrieve the associated records from the database
                    String referenceRecordSeq = null;
                    for (String key : record.keySet())
                    {
                        if (key.startsWith("referencerecordid") && (record.get(key) != null && !((String)record.get(key)).isEmpty()))
                        {
                            // Extract the sequence of the referencerecordid
                            referenceRecordSeq = key.replaceAll("referencerecordid", "");
                            // Get the referencerecordtype form this sequence which is the referenceRecordName
                            String referenceRecordName = (String)record.get("referencerecordtype"+referenceRecordSeq);
                            // Skip this record if already present in the parentRecordMap since we will have already done the fetch which would have retrieved all
                            // of the associated records for this parentRecordID
                            if (!parentRecordMap.containsKey(recordTypeKey+":"+parentRecordID+":"+referenceRecordSeq))
                            {
                                parentRecordMap.put(recordTypeKey+":"+parentRecordID+":"+referenceRecordSeq, record);
                                associatedRecordMap = fetchAssociatedRecords(record,
                                                                       referenceRecordName,
                                                                       associatedRecordMap,
                                                                       recordTypeKey+":"+parentRecordID+":"+referenceRecordSeq, packet);
                            }
                        }
                    }
                    // If there were no associated records for this record but we are including all parent records, put the parent record in the parentRecordMap
                    if (record.get("referencerecordid"+referenceRecordSeq) == null && includeAllParentRecords)
                    {
                        parentRecordMap.put(recordTypeKey+":"+parentRecordID, record);
                    }
                }
            }
            // Rebuild the section map to combine parent and associated records
            newSectionMap = buildSectionMap(parentRecordMap, associatedRecordMap);
        }
        return newSectionMap;
    }

//    /**
//     * processAssociatedRecords - This method iterates through the input DisplaySection and checks each
//     * record in the input to see if it references another record and if it does the associated records
//     * are retrieved. The parent records and associated records are placed into maps of parent and associated records.
//     * The resulting parent record and associated record maps are then input to the buildSectionMap
//     * method which builds a DataMap which will contain each of the parent records with the nested associated records.
//     * The DataMap is then traversed to build a newRecords map to be used to construct and return a new DisplaySection.
//     *
//     * @param displaySection DisplaySection
//     * @param includeAllParentRecords - true will include all records regardless of association to other records
//     *                                  false only parent records associated with children records will be included
//     * @return displaySection - new displaySection which now contains the parent records and with nested child records.
//     */
//    @SuppressWarnings("unchecked")
//    public DisplaySection processAssociatedRecords(DisplaySection displaySection, boolean includeAllParentRecords)
//    {
//        Map<String, DataMap> parentRecordMap = new LinkedHashMap<String,DataMap>();
//        Map<String, DataMap> associatedRecordMap = new LinkedHashMap<String,DataMap>();
//
//        Map<String, Map<String, String>> records = displaySection.getRecords();
//        for (String recordTypeKey : records.keySet())
//        {
//            DataMap record = new DataMap(records.get(recordTypeKey));
//
//            // Get the parent recordID to be used as a constraint to the select as well as in the recordMaps being built
//            if (!record.containsKey("parentrecordid"))
//            {
//                continue;
//            }
//
//            Integer parentRecordID = Integer.parseInt((String)record.get("parentrecordid"));
//            // If the record contains referencerecordids (there may be multiple), retrieve the associated records from the database
//            String referenceRecordSeq = null;
//            for (String key : record.keySet())
//            {
//                if (key.startsWith("referencerecordid") && (record.get(key) != null && !((String)record.get(key)).isEmpty()))
//                {
//                    // Extract the sequence of the referencerecordid
//                    referenceRecordSeq = key.replaceAll("referencerecordid", "");
//                    // Get the referencerecordtype form this sequence which is the referenceRecordName
//                    String referenceRecordName = (String)record.get("referencerecordtype"+referenceRecordSeq);
//
//                    // Skip this record if already present in the parentRecordMap since we will have already done the fetch which would have retrieved all
//                    // of the associated records for this parentRecordID
//                    if (!parentRecordMap.containsKey(recordTypeKey+":"+parentRecordID+":"+referenceRecordSeq))
//                    {
//                        parentRecordMap.put(recordTypeKey+":"+parentRecordID+":"+referenceRecordSeq, record);
//                        associatedRecordMap = fetchAssociatedRecords(record,
//                                                               referenceRecordName,
//                                                               associatedRecordMap,
//                                                               recordTypeKey+":"+parentRecordID+":"+referenceRecordSeq);
//                    }
//                }
//            }
//            // If there were no associated records for this record but we are including all parent records, put the parent record in the parentRecordMap
//            if (record.get("referencerecordid"+referenceRecordSeq) == null && includeAllParentRecords)
//            {
//                parentRecordMap.put(recordTypeKey+":"+parentRecordID, record);
//            }
//
//        }
//        // Build the list of records for the new display section
//        Map<String, Map<String,String>> newRecords = new LinkedHashMap<String, Map<String,String>>();
//        DataMap sectionMap = buildSectionMap(parentRecordMap, associatedRecordMap);
//
//        int seq = 0; // for a unique key into the newRecords map.
//        // Iterate throught the section map built above which contains any associated records nested within
//        // their parent records and build the newRecords map for inclusion in the new DisplaySection to be returned/
//        for (String sectionName : sectionMap.keySet())
//        {
//            LinkedList section = (LinkedList)sectionMap.get(sectionName);
//
//            Iterator it = section.iterator();
//            while (it.hasNext())
//            {
//                newRecords.put(""+seq++, (HashMap)((DataMap)it.next()).getMap());
//            }
//        }
//
//        return new DisplaySection(displaySection.getKey(), displaySection.getHeading(), displaySection.getSubHeading(),
//                                  displaySection.getYearStyle(), newRecords, displaySection.getRecordNumber());
//    }

    /**
     * fetchAssociatedRecords - fetch any records which are associated with the input record and add them to the
     * input associated record map and return.
     * @param record - The input record which references other records
     * @param referenceRecordName - The name of the referencedRecord as defined in the section table
     * @param associatedRecordMap - The input associated record map. Any records fetched will be added to this map.
     * @param recordKey - The key to use retrieved records in the associated record map
     * @param packet - packet to which the records belong
     * @return associatedRecordMap
     */
    @SuppressWarnings("unchecked") // For LinkedList<DataMap> cast
    private Map<String, DataMap> fetchAssociatedRecords(DataMap record,
                                                        String referenceRecordName,
                                                        Map<String,DataMap> associatedRecordMap,
                                                        String recordKey, Packet packet)
    {

       // Set the parameters for the fetch() to select the referenceRecords associated with the current parentRecordID
       List<Object> params = new ArrayList<Object>();
       List<Integer> paramTypes = new ArrayList<Integer>();
       Integer parentRecordID = Integer.parseInt((String)record.get("parentrecordid"));
       params.add(parentRecordID);
       paramTypes.add(Types.INTEGER);

       // Get the associated records for this parent record
       DataMap associatedMap = this.fetchAssociatedRecords(parentRecordID, referenceRecordName, packet);
       DataMap associatedRecsMap = (DataMap)associatedMap.get(referenceRecordName);

        if (associatedRecsMap == null)
            return associatedRecordMap;

       LinkedList<DataMap> associatedRecs = (LinkedList<DataMap>)associatedRecsMap.getMap().get(referenceRecordName+"-record");

       // Add the associated records retrieved to any which are already in the associatedRecordMap for the current recordTypeKey, parentId and referenceRecordSeq
       DataMap dMap = associatedRecordMap.get(recordKey);
       if (dMap == null)
       {
           dMap = new DataMap();
       }
       // Get the recently retrieved records
       LinkedList<DataMap> tRecs = (LinkedList<DataMap>) dMap.getMap().get(referenceRecordName+"-record");

       if (tRecs == null)
       {
           tRecs = new LinkedList<DataMap>();
       }
       // Add the recently retrieved records to the map
       tRecs.addAll(associatedRecs);
       dMap.put(referenceRecordName+"-record", tRecs);
       associatedRecordMap.put(recordKey, dMap);
       return associatedRecordMap;
    }

    /**
     * Builds a DataMap object from input maps of parent and associated records combining them into section map
     * containing the parent records with the nested associated records.
     * @param parentRecordMap - Map containing parent records
     * @param associatedRecordMap - Map containing associated records
     * @return
     */
    @SuppressWarnings("unchecked") // LinkedList cast
    private DataMap buildSectionMap(Map<String, DataMap> parentRecordMap, Map<String, DataMap> associatedRecordMap)
    {
        // Build the new sectionMap
        DataMap sectionMap = new DataMap();

        // Iterate through the parentRecordMap
        for (String recordNameKey : parentRecordMap.keySet())
        {
            // Get the parent records from the parentRecordMap
            DataMap record = parentRecordMap.get(recordNameKey);

            // Split the key (recordName:parentrecordID:seq) to get the recordName to use for the newSectionMap
            String recordName = recordNameKey.split(":")[0];

            // Get the current records in the newSectionMap for the recordName
            LinkedList<DataMap> currRecs = (LinkedList<DataMap>) sectionMap.get(recordName);
            if (currRecs == null)
            {
                currRecs = new LinkedList<DataMap>();
            }
            // Add the records from the parent record map to the newSectionMap for the recordName
            // The record may already be there because of an association with a different record type
            // so check for it's presence and retrieve it to add the associated record, otherwise add
            // to the record just added.
            if (currRecs.contains(record))
            {
                int index = currRecs.indexOf(record);
                record = currRecs.get(index);
            }
            else
            {
                currRecs.add(record);
            }

            // Update the newSectionMap for the recordName.
            sectionMap.put(recordName, currRecs);

            // Now check for the any associated records for this recordNameKey
            if (associatedRecordMap.containsKey(recordNameKey))
            {
                // Add the datamap for the associated record with a key of associatedrecord-<sequence>
                // There may be multiple record types associated with the record, therefore each associated datamap
                // is added with a sequence to create a unique key.
                int seq = 0;
                while (record.containsKey("associatedrecord-"+seq))
                {
                    seq++;
                }
                // If there are then add them to the newSectionMap for the current recordName
                record.put("associatedrecord-"+seq, associatedRecordMap.get(recordNameKey).getMap());
            }
        }

        return sectionMap;
    }

    private String cleanWysiwygMarkup(String field, String value)
    {
        try
        {
            value = this.wysiwygCleaner.removeTags(value);
        }
        catch (InvalidMarkupException ime)
        {
            log.info("InvalidMarkupException while handling field [{}] with contents [{}]", field, value);
            log.info(ime.getMessage());
            // Invalid XML, escape any remaining tags.
            value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
        }
        //value = MIVUtil.escapeMarkup(value);
        return value;
    }

    private void processMarkup (String recordName, DataMap recordMap)
    {
        String rectype = null;
        if (recordName.endsWith("-record"))
        {
            rectype = recordName.substring(0, recordName.length() - 7);
        }

        Properties p = PropertyManager.getPropertySet(rectype, "config");
        Collection<String> translateFieldsSet = Collections.emptyList();
        Collection<String> markupFieldsSet = Collections.emptyList();

        boolean allowMarkup = Boolean.parseBoolean(p.getProperty("allowmarkup"));
        if (allowMarkup)
        {
            String markupFields = p.getProperty("markupfields");
            markupFieldsSet = stringToSet(markupFields);
        }
        String translateFields = p.getProperty("translatenewline");

        if (translateFields != null && translateFields.length() > 0)
        {
            translateFieldsSet = stringToSet(translateFields);
        }

        // Get all String values from the map except notation and footnote
        Map<String, String> tempMap = new HashMap<String, String>();
        for (String col : recordMap.keySet())
        {
            if (recordMap.get(col) instanceof String &&
                    !col.equalsIgnoreCase("footnote") &&
                    !col.equalsIgnoreCase("notation"))
            {
                tempMap.put(col, (String)recordMap.get(col));
            }
        }
        // Check String values for markup and newlines
        if (!tempMap.isEmpty())
        {
            formatting.cleanMarkup(tempMap, markupFieldsSet);
            formatting.translateNewLines(tempMap, translateFieldsSet);
            DataMap cleanMap = new DataMap(tempMap);
            for (String col : cleanMap.keySet()) {
                recordMap.put(col, cleanMap.get(col));
            }
        }
    }


    /**
     * TODO: needs javadoc
     * TODO: this is named "stringToSet" but doesn't return a Set
     *       uniqueness is not guaranteed
     * @param markupFields comma separated String
     * @return a Collection of Strings containing each string provided, or null if null was passed in.
     */
    private Collection<String> stringToSet(String markupFields)
    {
        if (markupFields == null) return null;

        String[] fields = markupFields.split(",");
        Collection<String> fieldSet = java.util.Arrays.asList(fields);
        return fieldSet;
    }

    private Boolean displayRecord(Object value)
    {
        boolean display = false;
        if (value != null && value instanceof Boolean)
        {
            display = (Boolean)value;
        }
        else if (value != null && value instanceof String)
        {
            display = ((String)value).equals("true") || ((String)value).equals("1") || ((String)value).isEmpty();
        }
        return display;
    }
}
