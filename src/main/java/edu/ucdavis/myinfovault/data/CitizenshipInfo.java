package edu.ucdavis.myinfovault.data;


public class CitizenshipInfo
{
    private int usCitizenInd;
    private String dateEntered;
    private String visaType;
    private String recordId;
    private String cInfoRecType;


    public String getDateEntered()
    {
        return dateEntered;
    }

    public void setDateEntered(String dateEntered)
    {
        this.dateEntered = dateEntered;
    }

    public String getVisaType()
    {
        return visaType;
    }

    public void setVisaType(String visaType)
    {
        this.visaType = visaType;
    }

    public int getUsCitizenInd()
    {
        return usCitizenInd;
    }

    public void setUsCitizenInd(int usCitizenInd)
    {
        this.usCitizenInd = usCitizenInd;
    }

    public String getCInfoRecType()
    {
        return cInfoRecType;
    }

    public void setCInfoRecType(String infoRecType)
    {
        cInfoRecType = infoRecType;
    }

    public String getRecordId()
    {
        return recordId;
    }

    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }
}
