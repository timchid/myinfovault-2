package edu.ucdavis.myinfovault.data;

import java.util.Hashtable;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * Demonstration of LDAP connection over SSL with service-level account.
 * Tom Poage, UCD IET, 9 June 2004
 */

public class UCDLdap
{
    private static final String PEOPLE_PROVIDER = "ldaps://ldap.ucdavis.edu/ou=People,dc=ucdavis,dc=edu";
    private static final String LISTINGS_PROVIDER = "ldaps://ldap.ucdavis.edu/ou=Listings,dc=ucdavis,dc=edu";

    private static final String[] empIDattributes = { "uid", "employeeNumber" };
    private static final String[] DEFAULT_ATTRIBUTES = null;
    private static Logger log = Logger.getLogger("MIV");


    DirContext ctx;
    SearchControls sc;

    public enum Provider {
        PEOPLE,
        LISTINGS;
    }

    public UCDLdap()
    {
        this(Provider.PEOPLE);
    }

    public UCDLdap(Provider whichProvider)
    {
        String provider = PEOPLE_PROVIDER;
        switch (whichProvider)
        {
            case PEOPLE:
                provider = PEOPLE_PROVIDER;
                break;
            case LISTINGS:
                provider = LISTINGS_PROVIDER;
                break;
        }

        Hashtable<String,String> env = new Hashtable<String,String>(6);
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, provider);
        env.put(Context.SECURITY_AUTHENTICATION,"simple");
        env.put(Context.SECURITY_PRINCIPAL,
                "uid=miv,ou=Special Users,dc=ucdavis,dc=edu");
        env.put(Context.SECURITY_CREDENTIALS,"Mis;droos1");
        env.put(Context.SECURITY_PROTOCOL,"ssl");

        try {
            ctx = new InitialDirContext(env);
            sc = new SearchControls();
            // Set this to limit what we're retrieving. The old code wants *ONLY* the employee number.
            sc.setReturningAttributes(empIDattributes);
            // For development, debugging I want to see everything fetched - clear it back out.
            // We may also want to get much more information from LDAP; maybe to set more of the
            // MIV DB fields (not just Emp Id.) when a user is created, or on each login to MIV
            // rather than storing it ourselves.  Maybe store it just in case LDAP is unavailable
            // at login time.
            sc.setReturningAttributes(DEFAULT_ATTRIBUTES);
        }
        /*catch (SizeLimitExceededException sle) {
            System.out.println("size limit exceeded");
        }*/
        catch (NamingException ne) {
            log.log(Level.WARNING, "NamingException in UCDLdap while trying to set Dir context", ne);
            //ne.printStackTrace(System.out);
        }
        /*The NullPointerException should not occur ideally.
        The SearchControls or DirContext can be NULL in some cases. For eg. if the SSL certificate is expired
        So just catch the NullPointer and log it. Application should work even if it is thrown*/
        catch (NullPointerException e) {
            log.warning("NullPointerException (" + e.getMessage() + ") happened in UCDLdap constructor");
        }
        catch (Exception e) {
            log.warning("Something unexpected happened (" + e.getMessage() + ") while getting an Initial Context to the LDAP Server.");
        }

        return;
    }


    @Deprecated
    public String getEmpId_Old(String distAuthLogin)
    {
        log.fine("UCDLdap.getEmpId checking for user [" + distAuthLogin + "]");
        String result = null;
        try {
            sc.setReturningAttributes(empIDattributes);
            //NamingEnumeration ne = ctx.search("","uid="+distAuthLogin,sc);
            NamingEnumeration<?> ne = ctx.search("", "(|(uid=" + distAuthLogin + ")(mail=" + distAuthLogin + "*))", sc);
            while (ne.hasMore())
            {
                SearchResult sr = (SearchResult) ne.next();
                Attributes attrs = sr.getAttributes();
                if (attrs.size() > 0)
                {
                    NamingEnumeration<?> enumr = attrs.getIDs();
                    while (enumr.hasMoreElements())
                    {
                        String id = (String) enumr.next();
                        BasicAttribute att = (BasicAttribute)attrs.get(id);
                        log.finer("Checking attribute ["+att.getID()+"] with value ["+att.get().toString()+"]");
                        if (att.getID().equals("employeeNumber"))
                        {
                            result = (String)att.get();
                        }
                    }
                }
                else
                {
                    log.info("No attributes found for " + distAuthLogin);
                }
            }
        }
        catch (NamingException e) {
            log.warning("NamingException ("+e.getMessage()+ ") happened in UCDLdap.getEmpId_Old for login "+ distAuthLogin);
        }
        /*The NullPointerException should not occur ideally.
        The SearchControls or DirContext can be NULL in some cases. For eg. if the SSL certificate is expired
        So just catch the NullPointer and log it. Application should work even if it is thrown*/
        catch (NullPointerException e) {
            log.warning("NullPointerException (" + e.getMessage() + ") happened in UCDLdap.getEmpId_Old for login " + distAuthLogin);
        }
        catch (Throwable e) {
            log.warning("Something unexpected happened (" + e.getMessage() + ") in UCDLdap.getEmpId_Old for login " + distAuthLogin);
        }
        finally {
            if (sc != null) sc.setReturningAttributes(DEFAULT_ATTRIBUTES);
        }
        return result;
    }
    // getEmpId_Old()


    public String getEmpId(String login)
    {
        log.fine("UCDLdap.getEmpId checking for user [" + login + "]");
        String result = null;
        Map<String,String> returned = null;
        returned = getUidEntry(login, empIDattributes);
        result = returned.get("employeeNumber");
        if (result == null || result.length() < 1)
        {
            log.info("Employee ID Number not found for \"" + login + "\".");
        }
        return result;
    }


    public String getUid(String login)
    {
        log.fine("UCDLdap.getUid checking for user ["+login+"]");
        String result = null;
        Map<String,String> returned = null;
        returned = getUidEntry(login, empIDattributes);
        result = returned.get("uid");
        if (result == null || result.length() < 1)
        {
            log.info("Employee UID not found.");
        }
        return result;
    }


    public Map<String,String> getEntry(String login)
    {
        return getEntry(login, (String[])null);
    }


    public Map<String,String> getEntry(String login, String... getAttributes)
    {
        log.fine("UCDLdap.getEntry checking for user ["+login+"]");
        return this.search("(uid="+login+")", getAttributes);
    }

    public Map<String,String> search(String ldapCriteria, String... getAttributes)
    {
        SortedMap<String,String> m = new TreeMap<String,String>();

        SearchControls controlAttribs = new SearchControls();
        controlAttribs.setReturningAttributes(getAttributes);

        try
        {
            NamingEnumeration<?> ne = ctx.search("", ldapCriteria, controlAttribs);
            while (ne.hasMore())
            {
                SearchResult sr = (SearchResult) ne.next();
                Attributes attributes = sr.getAttributes();
                if (attributes.size() > 0)
                {
                    NamingEnumeration<?> eIDs = attributes.getIDs();
                    while (eIDs.hasMore())
                    {
                        String attributeID = (String) eIDs.next();
                        BasicAttribute attribute = (BasicAttribute) attributes.get(attributeID);
                        String attributeName = attribute.getID();
                        System.out.print("Checking attribute ["+attributeName+"], value: ");
                        int len = attribute.size();
                        for (int i=0; i<len; i++)
                        {
                            String //attributeValue = attribute.get().toString();
                            attributeValue = attribute.get(i).toString();
                            System.out.println("["+attributeValue+"]");
                            if (m.containsKey(attributeName))
                            {
                                attributeValue = m.get(attributeName) + ", " + attributeValue;
                            }
                            m.put(attributeName, attributeValue);
                        }
                    }
                }
            }
        }
        catch (NamingException e)
        {
            e.printStackTrace();
        }
        /*The NullPointerException should not occur ideally.
        The SearchControls or DirContext can be NULL in some cases. For eg. if the SSL certificate is expired
        So just catch the NullPointer and log it. Application should work even if it is thrown*/
        catch (NullPointerException e) {
            log.warning("NullPointerException (" + e.getMessage() + ") happened in UCDLdap.search for string [" + ldapCriteria + "]");
        }
        catch (Throwable e) {
            log.warning("Something unexpected happened (" + e.getMessage() + ") in UCDLdap.search for string [" + ldapCriteria + "]");
        }

        return m;
    }


    private SortedMap<String,String> getUidEntry(String login, String... getAttributes)
    {
        log.fine("UCDLdap.getEntry checking for user ["+login+"]");
        SortedMap<String,String> m = new TreeMap<String,String>();

        try {
            sc.setReturningAttributes(getAttributes);
            //NamingEnumeration ne = ctx.search("", "uid="+login, sc);
            NamingEnumeration<?> ne = ctx.search("","(uid="+login+")",sc);
            while (ne.hasMore())
            {
                SearchResult sr = (SearchResult) ne.next();
                Attributes attributes = sr.getAttributes();
                if (attributes.size() > 0)
                {
                    NamingEnumeration<?> eIDs = attributes.getIDs();
                    while (eIDs.hasMore())
                    {
                        String attributeID = (String) eIDs.next();
                        BasicAttribute attribute = (BasicAttribute) attributes.get(attributeID);
                        String attributeName = attribute.getID();
                        System.out.print("Checking attribute ["+attributeName+"], value: ");
                        int len = attribute.size();
                        for (int i=0; i<len; i++)
                        {
                            String //attributeValue = attribute.get().toString();
                            attributeValue = attribute.get(i).toString();
                            System.out.print("["+attributeValue+"]");
                            if (m.containsKey(attributeName))
                            {
                                attributeValue = m.get(attributeName) + ", " + attributeValue;
                            }
                            m.put(attributeName, attributeValue);
                        }
                        //System.out.println();
                    }
                }
            }
        }
        catch (NamingException e) {
            e.printStackTrace();
            return null;
        }
        /*The NullPointerException should not occur ideally.
        The SearchControls or DirContext can be NULL in some cases. For eg. if the SSL certificate is expired
        So just catch the NullPointer and log it. Application should work even if it is thrown*/
        catch (NullPointerException e) {
            log.warning("NullPointerException (" + e.getMessage() + ") happened in UCDLdap.getUidEntry for login " + login);
        }
        catch (Throwable e) {
            log.warning("Something unexpected happened (" + e.getMessage() + ") in UCDLdap.getUidEntry for login " + login);
        }
        return m;
    }

    public void close()
    {
        try {
            ctx.close();
        }
        catch (NamingException e) {
            e.printStackTrace();
        }
        return;
    }
}
