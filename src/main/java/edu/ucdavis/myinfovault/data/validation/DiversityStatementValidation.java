/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DiversityStatementValidation.java
 */

package edu.ucdavis.myinfovault.data.validation;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.util.HtmlUtil;
import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;

/**
 * To provide validations to diversity statement application.
 *
 * @author pradeeph
 * @since MIV 4.4.2
 */
public class DiversityStatementValidation
{

    /**
     * To validate teachingcontent and set filtered wysiwyg content
     * only if At least one Description/Elaboration of Diversity Activities available.
     *
     * @param teachingcontent
     * @param reqData
     * @return
     */
    public ResultVO validateTeachingContent(String teachingcontent, Map<String, String[]> reqData)
    {
        teachingcontent = HtmlUtil.filterWysiwygContent(teachingcontent);
        String servicecontent = HtmlUtil.filterWysiwygContent(reqData.get("servicecontent") != null ? reqData.get("servicecontent")[0] : null);
        String researchcontent = HtmlUtil.filterWysiwygContent(reqData.get("researchcontent") != null ? reqData.get("researchcontent")[0] : null);

        return (StringUtils.isEmpty(teachingcontent) && StringUtils.isEmpty(servicecontent) && StringUtils
                .isEmpty(researchcontent)) ? new ResultVO(false, "At least one Description/Elaboration of Diversity Activities is required.") :
                                             new ResultVO(true).setValue(teachingcontent);
    }

    /**
     * To validate servicecontent and set filtered wysiwyg content
     * only if At least one Description/Elaboration of Diversity Activities available.
     *
     * @param servicecontent
     * @param reqData
     * @return
     */
    public ResultVO validateServiceContent(String servicecontent, Map<String, String[]> reqData)
    {
        servicecontent = HtmlUtil.filterWysiwygContent(servicecontent);
        String teachingcontent = HtmlUtil.filterWysiwygContent(reqData.get("teachingcontent") != null ? reqData.get("teachingcontent")[0] : null);
        String researchcontent = HtmlUtil.filterWysiwygContent(reqData.get("researchcontent") != null ? reqData.get("researchcontent")[0] : null);

        return (StringUtils.isEmpty(teachingcontent) && StringUtils.isEmpty(servicecontent) && StringUtils
                .isEmpty(researchcontent)) ? new ResultVO(true) : new ResultVO(true, null, null, null, servicecontent);
    }

    /**
     * To validate researchcontent and set filtered wysiwyg content
     * only if At least one Description/Elaboration of Diversity Activities available.
     *
     * @param researchcontent
     * @param reqData
     * @return
     */
    public ResultVO validateResearchContent(String researchcontent, Map<String, String[]> reqData)
    {
        researchcontent = HtmlUtil.filterWysiwygContent(researchcontent);
        String teachingcontent = HtmlUtil.filterWysiwygContent(reqData.get("teachingcontent") != null ? reqData.get("teachingcontent")[0] : null);
        String servicecontent = HtmlUtil.filterWysiwygContent(reqData.get("servicecontent") != null ? reqData.get("servicecontent")[0] : null);

        return (StringUtils.isEmpty(teachingcontent) && StringUtils.isEmpty(servicecontent) && StringUtils
                .isEmpty(researchcontent)) ? new ResultVO(true) : new ResultVO(true, null, null, null, researchcontent);
    }

}
