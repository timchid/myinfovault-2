/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GrantsAndContractsValidation.java
 */

package edu.ucdavis.myinfovault.data.validation;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.data.Validator;

/**
 * Provide validation for Grants and Contracts form.
 * Grants records need special validation to cross-reference required fields
 * and acceptable dates based on the selected grant type and status.
 *
 * @author pradeeph
 * @since MIV 4.4.2
 */
public class GrantsAndContractsValidation extends BaseValidation
{
    /**
     * Validate start date if status is active or completed
     * @param startDate
     * @param reqData
     * @return
     */
    public ResultVO validateStartDate(String startDate, Map<String, String[]> reqData)
    {
        String statusid = (reqData.get("statusid") != null ? reqData.get("statusid")[0] : "-1");
        StatusType st = StatusType.find(statusid);

        Validator validator = new Validator(reqData);
        validator.validate("startdate", startDate, "date");

        if (st == StatusType.ACTIVE || st == StatusType.COMPLETED)
        {
            if (startDate == null || startDate.length() == 0)
            {
                return new ResultVO(false, "This field is required for a status of &#8216;Active&#8217; or &#8216;Completed&#8217;.");
            }
            if (!validator.isValid())
            {
                String errorMsg = "";
                if(validator.getErrorMessageList() != null && validator.getErrorMessageList().size()>0)
                {
                    errorMsg = validator.getErrorMessageList().get(0);
                }else{
                    errorMsg = "&#8220;" + startDate + "&#8221; is not a valid date.";
                }
                return new ResultVO(false, errorMsg);
            }
        }
        return new ResultVO(true);
    }


    /**
     * Validate end date if status is active or completed
     * @param endDate
     * @param reqData
     * @return
     */
    public ResultVO validateEndDate(String endDate, Map<String, String[]> reqData)
    {
        String statusid = (reqData.get("statusid") != null ? reqData.get("statusid")[0] : "-1");
        StatusType st = StatusType.find(statusid);

        if (st == StatusType.ACTIVE || st == StatusType.COMPLETED)
        {
            Validator validator = new Validator(reqData);
            String enddate = validator.validate("enddate", endDate, "date");
            String errorMsg = "";
            if (!validator.isValid())
            {
                if(validator.getErrorMessageList() != null && validator.getErrorMessageList().size()>0)
                {
                    errorMsg = validator.getErrorMessageList().get(0);
                }else{
                    errorMsg = "&#8220;" + endDate + "&#8221; is not a valid date.";
                }

                return new ResultVO(false, errorMsg);
            }

            String startDate = (reqData.get("startdate") != null ? reqData.get("startdate")[0] : null);

            if (startDate == null || startDate.length() == 0)
            {
                return new ResultVO(true);
            }

            String startdate = validator.validate("startdate", startDate, "date");
            validator = new Validator(reqData); // could just do a validator.reset() here instead of creating a new instance?

            // If startDate is invalid no need to check further
            if (!validator.isValid())
            {
                return new ResultVO(true);
            }

            try
            {
                Date startDateObj = sdf.get().parse(startdate);
                Date endDateObj = sdf.get().parse(enddate);

                // start date must be smaller or equals to end date
                if (startDateObj.after(endDateObj))
                {
                    return new ResultVO(false, "End date must be after the Start date.", "enddate");
                }
                else
                if (startDate.equals(endDate)) // no need to save same end date as start date
                {
                    ResultVO resultVO= new ResultVO(true);
                    resultVO.setValue("");
                    return resultVO;
                }
            }
            // This ParseException can't happen because the startDate and endDate
            // have already been successfully parsed by the Validator
            catch (ParseException e) { }
        }

        return new ResultVO(true);
    }

    /**
     * Validate Submitted Date if status is pending or not awarded
     * @param submittedDate
     * @param reqData
     * @return
     */
    public ResultVO validateSubmittedDate(String submittedDate, Map<String, String[]> reqData)
    {
        String statusid = (reqData.get("statusid") != null ? reqData.get("statusid")[0] : "-1");
        StatusType st = StatusType.find(statusid);

        if (st == StatusType.PENDING || st == StatusType.NOTAWARDED)
        {
            if (submittedDate == null || submittedDate.length() == 0)
            {
                return new ResultVO(false, "This field is required for a status of &#8216;Pending&#8217; or &#8216;Not Awarded&#8217;.");
            }

            Validator validator = new Validator(reqData);
            validator.validate("submitdate", submittedDate, "date");

            if (!validator.isValid())
            {
                return new ResultVO(false, "&#8220;" + submittedDate + "&#8221; is not a valid date.");
            }
        }

        return new ResultVO(true);
    }


    /**
     * Validate status of grants and contracts
     * @param statusId
     * @param reqData
     * @return
     */
    public ResultVO validateStatus(String statusId, Map<String, String[]> reqData)
    {
        StatusType st = StatusType.find(statusId);
        if (st == StatusType.INVALID)
        {
            return new ResultVO(false, "This field is required.");
        }
        else if (st == StatusType.NONE) // At least one of start date or submitted date is required
        {
            String startDate = (reqData.get("startdate") != null ? reqData.get("startdate")[0] : null);
            String submitDate = (reqData.get("submitdate") != null ? reqData.get("submitdate")[0] : null);

            if (startDate == null && submitDate == null)
            {
                return new ResultVO(false, "At least one of start date or submitted/resubmitted date is required.");
            }
        }
        return new ResultVO(true);
    }


    private enum StatusType
    {
        INVALID(-1),
        ACTIVE(1),
        PENDING(2),
        COMPLETED(3),
        NOTAWARDED(4),
        NONE(5);

        private int statusID;

        private StatusType(int statusID)
        {
            this.statusID = statusID;
        }

        public static StatusType find(String statusID)
        {
            try
            {
                int id = Integer.parseInt(statusID);
                for (StatusType st : StatusType.values())
                {
                    if (st.getStatusID() == id) {
                        return st;
                    }
                }

            }
            catch(NumberFormatException nfe) {
                return INVALID;
            }
            return INVALID;
        }


        public int getStatusID()
        {
            return this.statusID;
        }
    }

    public static void main(String[] args)
    {
        System.out.println(StatusType.find("1"));
        System.out.println(StatusType.find("0"));
        System.out.println(StatusType.find(""));
    }
}
