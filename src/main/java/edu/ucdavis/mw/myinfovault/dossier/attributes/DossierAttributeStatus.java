/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAttributeStatus.java
 */

package edu.ucdavis.mw.myinfovault.dossier.attributes;

/**
 * <p>This class represents the status values for the various dossier attributes.</p>
 * <ul>
 * <li>Documents can be ADDED</li>
 * <li> and either SIGNED, SIGNEDIT, REQUESTED, REQUESTEDIT, MISSING</li>
 * <li>Reviews and Voting may be OPEN or CLOSE</li>
 * <li>Several things may be COMPLETE</li>
 * <li>and the Action may be on HOLD or RELEASEd for signatures</li>
 * </ul>
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public enum DossierAttributeStatus
{
    ADDED ("Added"),
    OPEN ("Open"),
    CLOSE ("Closed"),
    CANCEL ("Canceled"),
    SIGNED ("Signed"),
    SIGNEDIT("Signed"),
    REQUEST("Requested"),
    REQUESTED("Requested"),
    REQUESTEDIT("Requested"),
    INVALID ("Invalid"),
    INVALIDEDIT("Invalid"),
    MISSING("Missing"),
    COMPLETE ("Completed"),
    RELEASE ("Released"),
    HOLD ("Required - Not Released");

    private String label;

    /**
     * Initialize the attribute status.
     *
     * @param description attribute status description
     */
    private DossierAttributeStatus(String description)
    {
        this.label = description;
    }

    /**
     * @return attribute status label
     */
    public String getLabel()
    {
        return label;
    }
}
