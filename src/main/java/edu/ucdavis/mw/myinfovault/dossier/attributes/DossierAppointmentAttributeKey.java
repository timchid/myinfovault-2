/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAppointmentAttributeKey.java
 */

package edu.ucdavis.mw.myinfovault.dossier.attributes;

import java.io.Serializable;

/**
 * This class represents a key into the AttributeMap of a Dossier object.
 * The key is a combination of the school and department id's.
 *
 * @author Rick Hendricks
 * @since 3.0
 */
public class DossierAppointmentAttributeKey implements Serializable
{
    private static final long serialVersionUID = 201112231130L;

    private int schoolId = 0;
    private int departmentId = 0;

    /**
     * Create dossier appointment attribute map key for the given scope.
     * 
     * @param schoolId school ID
     * @param departmentId department ID for given school
     */
    public DossierAppointmentAttributeKey(int schoolId, int departmentId)
    {
        this.setSchoolId(schoolId);
        this.setDepartmentId(departmentId);
    }

    /**
     * @param departmentId appointment key department ID
     */
    public void setDepartmentId(int departmentId)
    {
        this.departmentId = departmentId;
    }

    /**
     * @return appointment key department ID
     */
    public int getDepartmentId()
    {
        return departmentId;
    }

    /**
     * @param schoolId appointment key school ID
     */
    public void setSchoolId(int schoolId)
    {
        this.schoolId = schoolId;
    }

    /**
     * @return appointment key school ID
     */
    public int getSchoolId()
    {
        return schoolId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return this.toString().hashCode();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return schoolId + ":" + departmentId;
    }

    /**
     * Compare this object with another. Note that comparisons
     * MUST compare the values of the fields of the object that are
     * of interest (or whatever else is of interest). You can't compare the
     * hashes and get a good result -- hashes are allowed to collide. They just
     * reduce the search space tremendously.
     */
    @Override
    public boolean equals(Object o)
    {
        DossierAppointmentAttributeKey key = (DossierAppointmentAttributeKey) o;
        return ( (key.schoolId == this.schoolId) && (key.departmentId == this.departmentId));
    }
}
