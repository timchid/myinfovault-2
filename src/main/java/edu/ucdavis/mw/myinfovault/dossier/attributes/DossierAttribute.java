/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAttribute.java
 */
package edu.ucdavis.mw.myinfovault.dossier.attributes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierFileDto;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * This class represents a DossierAttribute.
 *
 * @author rhendric
 * @since 3.0
 */
public class DossierAttribute implements Serializable
{
    private static final long serialVersionUID = 201112291303L;

    /**
     * Dossier attribute is optional for each action type mapped to its attribute type name.
     */
    private static final Map<String, Set<DossierActionType>> optionalActionTypes = new HashMap<>();
    static {
        Map<String, Map<String, String>> attributeTypes = MIVConfig.getConfig().getMap("attributetypesbyname");

        for (Entry<String, Map<String, String>> entry : attributeTypes.entrySet())
        {
            Set<DossierActionType> optional = EnumSet.noneOf(DossierActionType.class);

            if (entry.getValue().get("optional") != null)
            {
                for (String actionTypeID : entry.getValue().get("optional").split("\\s*,\\s*"))
                {
                    optional.add(DossierActionType.valueOf(Integer.parseInt(actionTypeID)));
                }
            }

            optionalActionTypes.put(entry.getKey(), optional);
        }
    }

    private int recordId;
    private final int attributeId;
    private final String attributeName;
    private final String attributeDescription;
    private final DossierAttributeType attributeType;
    private final boolean required;
    private boolean display = true;
    private final String displayProperty;
    private List<DossierFileDto> documents = new ArrayList<DossierFileDto>();
    private long dossierId;
    private DossierAppointmentAttributeKey appointmentKey = null;
    private final int sequence;
    private DossierAttributeStatus attributeValue = null;
    private final DossierLocation location;

    /**
     * Create a new dossier attribute for an appointment and location.
     *
     * @param appointmentKey Dossier appointment key containing school and department IDs
     * @param attributeMap Map of default attribute values
     * @param location Dossier workflow location
     * @param actionType action type
     */
    public DossierAttribute(DossierAppointmentAttributeKey appointmentKey,
                            Map <String, String> attributeMap,
                            DossierLocation location,
                            DossierActionType actionType)
    {
        this.appointmentKey = appointmentKey;
        this.location = location;
        this.attributeId = Integer.parseInt(attributeMap.get("id"));
        this.sequence = Integer.parseInt(attributeMap.get("sequence"));
        this.attributeName = attributeMap.get("name");
        this.attributeDescription = attributeMap.get("description");
        this.display = "1".equals(attributeMap.get("display"));
        this.displayProperty = attributeMap.get("displayproperty");
        this.attributeType = DossierAttributeType.valueOf(attributeMap.get("type"));

        /*
         * This attribute is required if required by default
         * and not optional for the given action type.
         */
        this.required = "1".equals(attributeMap.get("required"))
                     && !optionalActionTypes.get(this.attributeName).contains(actionType);
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getAttributeDescription() {
        return attributeDescription;
    }

    public List<DossierFileDto> getDocuments() {
        return documents;
    }

    public void addDocument(DossierFileDto document) {
        this.documents.add(document);
    }

    public boolean isRequired() {
        return this.required;
    }

    /**
     * @return School ID for this attributes corresponding appointment
     */
    public int getSchoolId()
    {
        return appointmentKey.getSchoolId();
    }

    /**
     * @return Department ID for this attributes corresponding appointment
     */
    public int getDeptId()
    {
        return appointmentKey.getDepartmentId();
    }

    /**
     * @param appointmentKey Dossier appointment key containing school and department IDs
     */
    public void setAppointmentKey(DossierAppointmentAttributeKey appointmentKey)
    {
        this.appointmentKey = appointmentKey;
    }

    /**
     * @return Dossier appointment key containing school and department IDs
     */
    public DossierAppointmentAttributeKey getAppointmentKey()
    {
        return appointmentKey;
    }

    public DossierAttributeType getAttributeType() {
        return attributeType;
    }

    public DossierLocation getLocation() {
        return location;
    }

    public int getSequence() {
        return sequence;
    }

    /**
     * @param attributeValue dossier attribute value
     */
    public void setAttributeValue(DossierAttributeStatus attributeValue)
    {
        this.attributeValue = attributeValue;
    }

    /**
     * @return dossier attribute value
     */
    public DossierAttributeStatus getAttributeValue()
    {
        return attributeValue;
    }

    public void setDossierId(long dossierId) {
        this.dossierId = dossierId;
    }

    public long getDossierId() {
        return dossierId;
    }

    public int getAttributeId() {
        return attributeId;
    }

    public void setDisplay(boolean isDisplay) {
        this.display = isDisplay;
    }

    public boolean isDisplay() {
        return display;
    }

    public String getDisplayProperty() {
        return displayProperty;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getRecordId() {
        return recordId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return attributeName + ":" +
               attributeValue +
               appointmentKey +
               (isRequired() ? "(required)" : "(optional)");
    }
}
