/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LoggingAspect.java
 */

package edu.ucdavis.mw.myinfovault.util;


import java.util.Calendar;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.LoggerFactory;

/**
 * A simple generic aspect oriented logging class
 * @author rhendric
 *
 */
@Aspect
public class LoggingAspect
{

//    Logger logger = null;
    private final ThreadLocal<Long> tStartTime = new ThreadLocal<Long>();

    /**
     * Log method entry.
     *
     * @param joinPoint
     */

    public void logEntry(final JoinPoint joinPoint)
    {
      //LoggerFactory.getLogger( joinPoint.getTarget().getClass()).info("Entering method " + joinPoint.getSignature().getName() + "...");
        LoggerFactory.getLogger(joinPoint.getTarget().getClass())
                .info("Entering method {}...", joinPoint.getSignature().getName());
        tStartTime.set(Calendar.getInstance().getTimeInMillis());
    }

    /**
     * Log method exit.
     *
     * @param joinPoint
     */
    public void logExit(final JoinPoint joinPoint)
    {
        Long elapsedTime = Calendar.getInstance().getTimeInMillis() - tStartTime.get();
      //LoggerFactory.getLogger( joinPoint.getTarget().getClass()).info("Exiting method " + joinPoint.getSignature().getName() + " - Elapsed time: "+elapsedTime+"ms.");
        LoggerFactory.getLogger(joinPoint.getTarget().getClass())
                .info("Exiting method {} - Elapsed time: {}ms.", joinPoint.getSignature().getName(), elapsedTime);
    }

    /**
     * Log method exit after returning.
     *
     * @param joinPoint
     */
    public void logExitAfterReturn(final JoinPoint joinPoint)
    {
      //LoggerFactory.getLogger( joinPoint.getTarget().getClass()).info("Exiting method (after return) " + joinPoint.getSignature().getName() + "...");
        LoggerFactory.getLogger(joinPoint.getTarget().getClass())
                .info("Exiting method (after return) {}...", joinPoint.getSignature().getName());
    }
}
