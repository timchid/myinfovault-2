/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HtmlUtil.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.tidy.Tidy;

/**
 * HTML syntax checker and pretty printer.
 *
 * This class was created to deal with the bad markup that results when pasting text copied from MS Word and IE.
 *
 * @author pradeeph
 * @since v4.4.2
 */
public class HtmlUtil
{
    private static Logger log = LoggerFactory.getLogger(HtmlUtil.class);

    /**
     * Replace normal space characters with this string in order to preserve whitespace.
     * Replacement string can later be restored to spaces.
     * Uses Unicode "non-character" characters, which *cannot* appear in user-provided text,
     * as per discussion in the comments of a <a href="http://stackoverflow.com/a/26791459/17300">StackOverflow answer</a>.
     * @see <a href="http://www.unicode.org/faq/private_use.html#nonchar4">Which code points are noncharacters?</a> in the Unicode FAQ.
     */
    private static final String SPACE_HOLDER = "\uFDD1\uFDD2\uFDD3";

    /**
     * Configured properties.
     */
    private static final Properties props = getTidyProperties();

    /**
     * wysiwyg broken tag and correct tags map
     */
    private static final Map<String, String> WysiwygBrokenTagsMap;
    static
    {
        WysiwygBrokenTagsMap = new LinkedHashMap<String, String>();
        WysiwygBrokenTagsMap.put("[\n\r]", "");
        WysiwygBrokenTagsMap.put("(" + SPACE_HOLDER + ")", " ");
    }


    /**
     * Get the config properties for Tidy
     */
    private static Properties getTidyProperties()
    {
        Properties tidyProps = new Properties();

        // This option specifies if Tidy should print only the contents of the body tag as an HTML fragment.
        tidyProps.setProperty("show-body-only", "true");

        // This option specifies if Tidy should suppress warnings.
        // This can be useful when a few errors are hidden in a flurry of warnings.
        tidyProps.setProperty("show-warnings", "false");

        // This option specifies if Tidy should output the summary of the numbers of errors and warnings,
        // or the welcome or informational messages.
        tidyProps.setProperty("quiet", "true");

        // This option specifies if Tidy should produce output even if errors are encountered.
        // Use this option with care -
        // if Tidy reports an error, this means Tidy was not able to, or is not sure how to,
        // fix the error, so the resulting output may not reflect your intention.
        tidyProps.setProperty("force-output", "true");

        // This option specifies the character encoding Tidy uses for the input.
        // See char-encoding for more info.
        tidyProps.setProperty("output-encoding", "utf8");

        // This option specifies if Tidy should generate pretty printed output, writing it as HTML.
        tidyProps.setProperty("output-html", "true");

        // This option specifies if Tidy should strip Microsoft specific HTML from Word 2000 documents,
        // and output spaces rather than non-breaking spaces where they exist in the input.
        tidyProps.setProperty("bare", "true");

        // This option specifies if Tidy should go to great pains to strip out all the surplus stuff
        // Microsoft Word 2000 inserts when you save Word documents as "Web pages".
        // Doesn't handle embedded images or VML. You should consider using Word's "Save As: Web Page, Filtered".
        tidyProps.setProperty("word-2000", "true");

        // This option specifies if Tidy should print out comments.
        tidyProps.setProperty("hide-comments", "true");

        // Don't do any wrapping
        tidyProps.setProperty("wrap", "0");

        return tidyProps;
    }


    /**
     * <p>
     * Purify html and resolve broken tags.
     * </p>
     * <p>
     * We use "Tidy" to attempt repair of broken HTML, however jTidy itself has bugs.
     * One significant bug is introducing new whitespace where there was none originally,
     * which changes the DOM tree. We work around the jTidy bugs by pre- and post-
     * processing the input text.
     * </p>
     * <p>
     * Example: <br>
     * Input --&gt; &lt;TABLE style="WIDTH: 510px; TABLE-LAYOUT1: fixed" border=1 width=510&gt;&lt;tr&gt;&lt;td&gt;HELLO
     * <br>
     * Output --&gt; &lt;table style="WIDTH: 510px; TABLE-LAYOUT1: fixed" border="1"
     * width="510"&gt;&lt;tr&gt;&lt;td&gt;HELLO&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
     * </p>
     *
     * @param rawHtml
     * @return cleaned valid HTML or XHTML
     */
    public static String purifyHTML(String rawHtml)
    {
        if (StringUtils.isEmpty(rawHtml)) {
            return rawHtml;
        }

        // filter raw html before purify
        rawHtml = preHtmlFilter(rawHtml);

        String purifiedHTMLText = "";
        try
        {
            Tidy tidy = new Tidy();
            tidy.setConfigurationFromProps(props);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);

            tidy.parse(new StringReader(rawHtml), ps);

            purifiedHTMLText = baos.toString("UTF8");

            purifiedHTMLText = postHtmlFilter(purifiedHTMLText);
        }
        catch (UnsupportedEncodingException e)
        {
            // This should never happen, because .toString("UTF8") is always a supported encoding.
            log.info("UnsupportedEncodingException while purifyHTML with contents:\n{}\n", rawHtml);
            log.info("     " + e.getMessage(), e);
        }

        return purifiedHTMLText;
    }


    /* Available to compare plain Tidy output to our pre- and post-processed output. *//*
    private static String justTidy(final String rawHtml)
    {
        String purifiedHTMLText = "";
        try {
            Tidy tidy = new Tidy();
            tidy.setConfigurationFromProps(props);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);

            tidy.parse(new StringReader(rawHtml), ps);

            purifiedHTMLText = baos.toString("UTF8");
        }
        catch (UnsupportedEncodingException e)
        {
            // ignore this - it can't happen
        }

        return purifiedHTMLText;
    }
    */


    /**
     * Filter raw html before purifying
     * FIXME: by doing what?
     *
     * @param rawHtml
     * @return FIXME: return what?
     */
    private static String preHtmlFilter(final String rawHtml)
    {
        // To preserve spaces
//        rawHtml = preserveSpaces(rawHtml);
//        rawHtml = rawHtml.replaceAll("[\n\r]", "");
//
//        return rawHtml;
        String newHtml = preserveSpaces(rawHtml)
                            .replaceAll("[\n\r]", "");
        return newHtml;
    }


    /**
     * Filter purified Html to resolve formating issues.
     * FIXME: by doing what?
     *
     * @param purifyHtml
     * @return FIXME: return what?
     */
    private static String postHtmlFilter(final String purifyHtml)
    {
        String cleanHtml = purifyHtml.replaceAll("<br>", "<br />");
        cleanHtml = cleanHtml.replaceAll("<hr>", "<hr />");
        cleanHtml = fixWysiwygTags(cleanHtml);

        return cleanHtml;

        // ...or...
        //return fixWysiwygTags(
        //                      purifyHtml.replaceAll("<br>", "<br />")
        //                                .replaceAll("<hr>", "<hr />")
        //                      );
    }


    /**
     * Replace spaces with SPACE_HOLDER to preserve spaces
     *
     * @param rawHtml
     * @return FIXME: return what?
     */
    private static String preserveSpaces(final String rawHtml)
    {
        List<String> htmlList = splitHtml(rawHtml);
        List<String> tagList = getHtmlTags(rawHtml);

        StringBuilder newHtml = new StringBuilder();
        for (String htmlBlock : htmlList)
        {
            if (!tagList.contains(htmlBlock))
            {
                newHtml.append( htmlBlock.replaceAll(" ", SPACE_HOLDER) );
            }
            else
            {
                newHtml.append( htmlBlock );
            }
        }

        return newHtml.toString();
    }


    /**
     * Fix wysiwyg tags by replacing broken tags with correct tags.
     *
     * @param purifyHTML
     * @return FIXME: return what?
     */
    private static String fixWysiwygTags(final String purifyHTML)
    {
        if (StringUtils.isEmpty(purifyHTML)) {
            return purifyHTML;
        }

        List<String> htmlList = splitHtml(purifyHTML);
        List<String> tagList = getHtmlTags(purifyHTML);

        StringBuilder purifiedHtml = new StringBuilder();
        for (String htmlBlock : htmlList)
        {
            if (tagList.contains(htmlBlock))
            {
                purifiedHtml.append( StringUtil.removeLinefeeds(htmlBlock) );
            }
            else
            {
                for (String tagName : WysiwygBrokenTagsMap.keySet())
                {
                    // this also puts spaces back in the string by replacing the SPACE_HOLDER
                    htmlBlock = htmlBlock.replaceAll(tagName, WysiwygBrokenTagsMap.get(tagName));
                }
                purifiedHtml.append( htmlBlock );
            }
        }

        return purifiedHtml.toString();
    }


    /**
     * Filter wysiwyg Content
     * FIXME: by doing what to it?
     *
     * @param rawHTML
     * @return FIXME: return what?
     */
    public static String filterWysiwygContent(final String rawHTML)
    {
        String purifiedHTML = purifyHTML(rawHTML);
        String htmlContents = removeHTMLTags(purifiedHTML);

        // If content is null or length == 0 set null content
        return StringUtils.isEmpty(htmlContents) ? null : purifiedHTML;
    }


    /**
     * Remove tags and get the text only
     * <p>
     * Example: <br>
     * Input --&gt; &lt;span class="title"&gt;Testing&lt;/span&gt; <br>
     * Output --&gt; Testing
     * </p>
     *
     * @param htmlString
     * @return FIXME: return what?
     */
    public static String removeHTMLTags(final String htmlString)
    {
        if (StringUtils.isEmpty(htmlString)) {
            return htmlString;
        }

        // Remove HTML tag from java String
        String noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");

        // Remove Carriage return from java String
        noHTMLString = noHTMLString.replaceAll("\r", "<br/>");

        // Remove New line from java string and replace html break
        noHTMLString = noHTMLString.replaceAll("\n", " ");
        /*
         * noHTMLString = noHTMLString.replaceAll("\'", "&#39;");
         * noHTMLString = noHTMLString.replaceAll("\"", "&quot;");
         */

        /* Could be done with one declaration and assignment, but this is harder to debug. *//*
        String cleaned = htmlString
                    .replaceAll("\\<.*?\\>", "")
                    .replaceAll("\r", "<br/>")
                    .replaceAll("\n", " ")
                ;
        */

        return noHTMLString.trim();
    }


    /**
     * Get html tags and create a list of html tags.
     * <p>
     * Example: <br>
     * Input --&gt; &lt;span&gt;Testing&lt;/span&gt; <br>
     * Output --&gt; [&lt;span&gt;, &lt;/span&gt;]
     * </p>
     *
     * @param sourceString
     * @return list of html tags.
     */
    public static List<String> getHtmlTags(final String sourceString)
    {
        return splitHtml(sourceString, true);
    }


    /**
     * Split html content and create a list of html tags and contents.
     * <p>
     * Example: <br>
     * Input --&gt; &lt;span&gt;Testing&lt;/span&gt; <br>
     * Output --&gt; [&lt;span&gt;, Testing, &lt;/span&gt;]
     * </p>
     *
     * @param sourceString
     * @return list of html tags and contents.
     */
    public static List<String> splitHtml(String sourceString)
    {
        return splitHtml(sourceString, false);
    }


    /**
     * Split html content and create a list of html tags and contents.
     * <p>
     * Example: <br>
     * Input --&gt; &lt;span&gt;Testing&lt;/span&gt; <br>
     * Output --&gt; [&lt;span&gt;, Testing, &lt;/span&gt;]
     * </p>
     *
     * @param sourceString
     * @param tagsOnly FIXME: what does this parameter do?
     * @return list of html tags and contents.
     */
    private static List<String> splitHtml(final String sourceString, final boolean tagsOnly)
    {
        List<String> tagList = new ArrayList<String>();

        char[] buf = sourceString.toCharArray();
        StringBuilder tag = new StringBuilder();
        char c;

        for (int bufPos = 0; bufPos < buf.length; bufPos++)
        {
            tag.setLength(0); // clear for a new iteration
            c = buf[bufPos];
            if (isTagStart(c))
            {
                tag.append(c);
                for (int tagPos = bufPos + 1; tagPos < buf.length; tagPos++, bufPos++)
                {
                    c = buf[tagPos];
                    tag.append(c);
                    if (isSingleQuote(c))
                    {
                        // Skip single quotes
                        for (int quotePos = tagPos + 1; quotePos < buf.length; quotePos++, tagPos++, bufPos++)
                        {
                            c = buf[quotePos];
                            tag.append(c);
                            if (isSingleQuote(c))
                            {
                                // Move the tag parsing and the overall buffer positions past the quote mark.
                                tagPos++;
                                bufPos++;
                                break;
                            }
                        }
                    }
                    else if (isDoubleQuote(c))
                    {
                        // Skip double quotes
                        for (int quotePos = tagPos + 1; quotePos < buf.length; quotePos++, tagPos++, bufPos++)
                        {
                            c = buf[quotePos];
                            tag.append(c);
                            if (isDoubleQuote(c))
                            {
                                // Move the tag parsing and the overall buffer positions past the quote mark.
                                tagPos++;
                                bufPos++;
                                break;
                            }
                        }
                    }
                    else if (isTagEnd(c))
                    {
                        // add tag into list and lookup for the next tag
                        tagList.add(tag.toString());
                        // Move the buffer position past the tag-closing char.
                        bufPos++;
                        break;
                    }
                    else if (isTagStart(c))
                    {
                        if (!tagsOnly)
                        {
                            tagList.add(tag.toString());
                        }

                        // Start with most recent start tags
                        tag.setLength(0);
                        tag.append(c);
                    }
                }
            }
            else if (!tagsOnly)
            {
                tag.setLength(0);
                // get content
                for (int strPos = bufPos; strPos < buf.length; strPos++, bufPos++)
                {
                    c = buf[strPos];
                    if (!isTagStart(c) && !isBadChar(c)) {
                        tag.append(c);
                    }

                    if (isTagStart(c) || (strPos + 1) >= buf.length)
                    {
                        tagList.add(tag.toString());
                        if ((strPos + 1) < buf.length) bufPos--;
                        break;
                    }
                }
            }
        }

        return tagList;
    }


    private static boolean isSingleQuote(final char c)
    {
        return c == '\'';
    }


    private static boolean isDoubleQuote(final char c)
    {
        return c == '"';
    }


    private static boolean isTagStart(final char c)
    {
        return c == '<';
    }


    private static boolean isTagEnd(final char c)
    {
        return c == '>';
    }


    /**
     * Check for a bad character<br>
     * <b>Some bad characters are</b><ul>
     * <li>&amp;#8204 - Zero-width non-joiner (Unicode 0x200C)</li>
     * <li>&amp;#8205 - Zero-width joiner (Unicode 0x200D)</li>
     * </ul>
     * @param c character to check
     * @return true if the character <tt>c</tt> is in the predefined list of "bad" characters.
     */
    private static boolean isBadChar(final char c)
    {
        return badCharString.contains("" + c);
    }
    // Build the string once, instead of on every method call.
    private static final String badCharString = "\u200C\u200D";



    // TODO: These tests should be done in jUnit
    /* *
     * These tests are being migrated to HtmlUtilTest.java
     * @param args
     * /
    public static void main(String[] args)
    {
        // Migrated.
        //String s1 = "<TABLE style=\"WIDTH: 510px; TABLE-LAYOUT1: fixed\"\nborder=1 width=510><tr><td>™¿©ÄÑìΔΓΒõ";
        //testPurify(s1);

        // Migrated.
        //String s2 = "<span>Line One</span><p>This has U<sup>2</sup> superscript \nin it.</p><p>Line Three ™¿©ÄÑìΔ<br /></p>";
        //testPurify(s2);

        String s3 = "<p>x <sup> 2 </sup></p>";
        testPurify(s3);

        String s4 = "<p>X   <sup> 2   </sup> - y     <sup>2     </sup> = (X-Y) (X+Y)</p>";
        testPurify(s4);

        // Migrated.
        //String s5 = "<p>Line1</p>\n<p><br /></p>\n<p>Line3</p>\n<p><br /></p>\n<p><br /></p>\n<p>Line6</p>";
        //testPurify(s5);

        String s6 = "<p class=\"top\">Line1 has quotes in the tag.</p>\n<p><br></p>\n<p>Line3 has \"quotes\" in it.</p>\n<p><br></p>\n<p><br></p>\n<p>Line6</p>";
        testPurify(s6);

        // Check an open-tag '<' char within a tag "<tag<name>"
        String s7a = "Text <span<b> more text";
        String s7b = "Text <span class=\"foo<bar\"><div> more text";
        String s7c = "Text <span class=\"foo\" i<d=\"bar\"> more text";
        testPurify(s7a);
        testPurify(s7b);
        testPurify(s7c);

        // Migrated.
        //String s8 = "<p>x <sup> 2 </sup></p>";
        //testGetTags(s8);

        testPurify("<p>asf f sdf sd f sdg sdg g dfh r yht‌ry rt u yt d sd<br>fsd f sdsd g sdg sdg ds g dsg sd gsd g sd gsd g sdg sd g sdg sdg sdg sd g sdgetger gfgsertwetiou weiotuweiotueriotuertb t er t t t we t wet ttetetertery. This is an example for diversity activities in scholarly and creative‌ activities.</p>");

        System.out.println("Check that nothing chokes on 'null' :: " + filterWysiwygContent(null));
    }


    /**
     *
     * @param s
     * /
    private static void testPurify(final String s)
    {
        String testOutput = HtmlUtil.purifyHTML(s);

        System.out.println(" Input: [" + s + "]");
        System.out.println("Output: [" + testOutput + "]\n");

        System.out.flush();
        System.err.flush();
    }

// Tests using this have been migrated to HtmlUtilTest and this is no longer needed.
//   /**
//    * @param s
//    */
//   private static void testGetTags(final String s)
//   {
//       List<String> testOutput = HtmlUtil.getHtmlTags(s);
//       System.out.println(" Input: [" + s + "]");
//       System.out.println("Output: [" + testOutput + "]\n");
//
//       System.out.flush();
//       System.err.flush();
//   }

}
