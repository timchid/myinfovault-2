/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivDocument.java
 */

package edu.ucdavis.mw.myinfovault.util;

import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.DEPARTMENT;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.SCHOOL;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.UNKNOWN;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.VICEPROVOST;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Defines MIV document types.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public enum MivDocument
{
    /**
     * Disclosure certificate.
     */
    DISCLOSURE_CERTIFICATE(18, MivRole.CANDIDATE, false, null, DossierLocation.mapLocationToOffice(DEPARTMENT)),

    /**
     * Joint disclosure certificate.
     */
    JOINT_DISCLOSURE_CERTIFICATE(25, MivRole.CANDIDATE, false, null, DossierLocation.mapLocationToOffice(DEPARTMENT)),

    /**
     * Recommended action form.
     */
    RAF(13, MivRole.INVALID_ROLE, false, null, DossierLocation.mapLocationToOffice(UNKNOWN)),

    /**
     * New appointment form.
     */
    AA(74, MivRole.INVALID_ROLE, false, null, DossierLocation.mapLocationToOffice(UNKNOWN)),

    /**
     * Dean's final decision.
     */
    DEANS_FINAL_DECISION(20, MivRole.DEAN, true, "dean_release", DossierLocation.mapLocationToOffice(SCHOOL)),

    /**
     * Dean's appeal decision.
     */
    DEANS_APPEAL_DECISION(57, MivRole.DEAN, true, "dean_appeal_release", DossierLocation.mapLocationToOffice(SCHOOL)),

    /**
     * Vice Provost's final decision.
     */
    VICEPROVOSTS_FINAL_DECISION(21, MivRole.VICE_PROVOST, true, "vp_release", DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Vice Provost's appeal recommendation.
     */
    VICEPROVOSTS_APPEAL_RECOMMENDATION(84, MivRole.VICE_PROVOST, false, "vp_appeal_recommendation_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Vice Provost's appeal decision.
     */
    VICEPROVOSTS_APPEAL_DECISION(61, MivRole.VICE_PROVOST, true, "vp_appeal_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's appeal recommendation.
     */
    PROVOSTS_APPEAL_RECOMMENDATION(85, MivRole.PROVOST, false, "p_appeal_recommendation_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's tenure appeal decision.
     */
    PROVOSTS_TENURE_APPEAL_DECISION(94, MivRole.PROVOST, true, "p_tenure_appeal_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's tenure appeal recommendation.
     */
    PROVOSTS_TENURE_APPEAL_RECOMMENDATION(86, MivRole.PROVOST, false, "p_tenure_appeal_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's appeal decision.
     */
    PROVOSTS_APPEAL_DECISION(87, MivRole.PROVOST, true, "p_appeal_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's appeal decision.
     */
    CHANCELLORS_APPEAL_DECISION(88, MivRole.CHANCELLOR, true, "chancellor_appeal_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Dean's recommendation
     */
    DEANS_RECOMMENDATION(23, MivRole.DEAN, false, "dean_recommendation_release",  DossierLocation.mapLocationToOffice(SCHOOL)),

    /**
     * Joint Dean's recommendation
     */
    JOINT_DEANS_RECOMMENDATION(28, MivRole.DEAN, false, "joint_dean_release",  DossierLocation.mapLocationToOffice(SCHOOL)),

    /**
     * Joint Dean's appeal recommendation
     */
    JOINT_DEANS_APPEAL_RECOMMENDATION(59, MivRole.DEAN, false, "joint_dean_appeal_release",  DossierLocation.mapLocationToOffice(SCHOOL)),

    /**
     * Vice Provost's Recommendation.
     */
    VICEPROVOSTS_RECOMMENDATION(77, MivRole.VICE_PROVOST, false, "vp_recommendation_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's Recommendation.
     */
    PROVOSTS_RECOMMENDATION(78, MivRole.PROVOST, false, "p_recommendation_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's Tenure Decision.
     */
    PROVOSTS_TENURE_DECISION(93, MivRole.PROVOST, true, "p_tenure_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's Tenure Recommendation.
     */
    PROVOSTS_TENURE_RECOMMENDATION(79, MivRole.PROVOST, false, "p_tenure_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Provost's Final Decision.
     */
    PROVOSTS_FINAL_DECISION(80, MivRole.PROVOST, true, "p_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Cancellor's Final Decision.
     */
    CHANCELLORS_FINAL_DECISION(81, MivRole.CHANCELLOR, true, "chancellor_release",  DossierLocation.mapLocationToOffice(VICEPROVOST)),

    /**
     * Represents a null or un-set document.
     */
    NONE(0, MivRole.INVALID_ROLE, false, null,  DossierLocation.mapLocationToOffice(UNKNOWN));

    // initialize document types with attributes from the MIV configuration
    static
    {
        Map<String, Map<String,String>> documentsByID = MIVConfig.getConfig().getMap("documents");
        Map<String, Map<String,String>> documentsByName = MIVConfig.getConfig().getMap("documentsbyattributename");

        for (MivDocument type : MivDocument.values())
        {
            Map<String, String> document = documentsByID.get(Integer.toString(type.id));

            if (document != null)
            {
                String alternateDescription = document.get("alternatedescription");

                type.description = document.get("description");
                type.alternateDescription = StringUtils.isNotBlank(alternateDescription) ? alternateDescription : type.description;
                type.dossierFileName = document.get("dossierfilename");
                type.attributeName = document.get("attributename");

                /*
                 * Load ID and attribute name for the wet signature.
                 */
                Map<String, String> wetDocument = documentsByName.get(type.attributeName + "_wet");
                if (wetDocument != null)
                {
                    type.wetSignatureId = Integer.parseInt(wetDocument.get("id"));
                    type.wetSignatureAttributeName = wetDocument.get("attributename");
                }
            }
        }
    }

    private final int id;
    private final MivRole allowedSigner;
    private final boolean decision;
    private final String releaseAttributeName;
    private final DossierLocation signingOffice;

    private String description = null;
    private String alternateDescription = null;
    private String dossierFileName = null;
    private String attributeName = null;
    private int wetSignatureId = -1;
    private String wetSignatureAttributeName = null;

    /**
     * Instantiate an MIV signable document type.
     *
     * @param id the MIV Document ID number for this document type
     * @param allowedSigner MIV role allowed to sign the document
     * @param decision if this document type represents a decision (otherwise, a recommendation)
     * @param releaseAttributeName name of the dossier attribute that releases this document type to sign
     * @param signatureLocation location at which this document may be signed
     */
    private MivDocument(int id,
                        MivRole allowedSigner,
                        boolean decision,
                        String releaseAttributeName,
                        DossierLocation signingOffice)
    {
        this.id = id;
        this.allowedSigner = allowedSigner;
        this.decision = decision;
        this.releaseAttributeName = releaseAttributeName;
        this.signingOffice = signingOffice;
    }

    /**
     * @return the MIV Document ID number for this document type.
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return MIV role allowed to sign the document
     */
    public MivRole getAllowedSigner()
    {
        return allowedSigner;
    }

    /**
     * @return if this document type represents a decision (otherwise, a recommendation)
     */
    public boolean isDecision()
    {
        return decision;
    }

    /**
     * @return name of the dossier attribute that releases this document type to sign
     */
    public String getReleaseAttributeName()
    {
        return releaseAttributeName;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return the alternateDescription
     */
    public String getAlternateDescription()
    {
        return alternateDescription;
    }

    /**
     * @return the dossierFileName
     */
    public String getDossierFileName()
    {
        return dossierFileName;
    }

    /**
     * @return the dossier attribute name
     */
    public String getAttributeName()
    {
        return attributeName;
    }

    /**
     * @return the MIV Document ID number for this document type's wet signature
     */
    public int getWetSignatureId()
    {
        return wetSignatureId;
    }

    /**
     * @return the dossier attribute name for this type's wet signature
     */
    public String getWetSignatureAttributeName()
    {
        return wetSignatureAttributeName;
    }

    /**
     * @return the office location at which this document may be signed
     */
    public DossierLocation getSigningOffice()
    {
        return this.signingOffice;
    }

    /**
     * Returns the MIV document type corresponding to the type ID.
     *
     * @param id the document type ID
     * @return document type corresponding to the type ID
     */
    public static MivDocument get(int id)
    {
        for (MivDocument type : MivDocument.values())
    	{
            if (type.id == id) return type;
    	}

        return MivDocument.NONE;
    }

    /**
     * Returns the MIV document type corresponding to the dossier attributeName.
     *
     * @param attributeName the dossier attribute name
     * @return document type corresponding to the dossier attribute
     * @see edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute#getAttributeName()
     */
    public static MivDocument get(String attributeName)
    {
        if (!StringUtils.isEmpty(attributeName))
        {
            for (MivDocument type : MivDocument.values())
            {
                if (attributeName.equalsIgnoreCase(type.attributeName)) return type;
            }
        }

        return MivDocument.NONE;
    }

    /**
     * Returns the MIV document type corresponding to the dossier release attribute name.
     *
     * @param releaseAttributeName the dossier release attribute name
     * @return document type corresponding to the dossier release attribute
     * @see edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute#getAttributeName()
     */
    public static MivDocument getForRelease(String releaseAttributeName)
    {
        if (!StringUtils.isEmpty(releaseAttributeName))
        {
            for (MivDocument type : MivDocument.values())
            {
                if (releaseAttributeName.equalsIgnoreCase(type.releaseAttributeName)) return type;
            }
        }

        return MivDocument.NONE;
    }
}
