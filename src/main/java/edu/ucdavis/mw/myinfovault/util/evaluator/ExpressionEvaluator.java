/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ExpressionEvaluator.java
 */

package edu.ucdavis.mw.myinfovault.util.evaluator;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

/**
 * <p>Evaluates a {@link String} expression for a boolean value.</p>
 *
 * <p>
 * <strong>Legal operators:</strong>
 * <ul>
 * <li>"!" (Not)</li>
 * <li>"|" (Or)</li>
 * <li>"&" (And)</li>
 * <li>"(" (Open block)</li>
 * <li>")" (Close block)</li>
 * </ul>
 * </p>
 *
 * <p>The truthiness of tokens outside the set of legal operators
 * are delegated to the subclass via {@link #parseValue()}. In the
 * following example, the tokens "SOMEONE" and "SOMETHING" will be
 * handled by the subclass</p>
 *
 * <p>
 * <strong>Example:</strong>
 * <pre>(SOMETHING|SOMEONE)&!(SOMETHING&SOMEONE)</pre>
 * </p>
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public abstract class ExpressionEvaluator
{
    protected static final String NOT = "!";
    protected static final String OR = "|";
    protected static final String AND = "&";
    protected static final String OPEN = "(";
    protected static final String CLOSE = ")";

    /**
     * Acceptable vocabulary for evaluated expressions.
     */
    protected static final Set<String> LEXICON = new HashSet<String>();
    static {
        LEXICON.add(NOT);
        LEXICON.add(OR);
        LEXICON.add(AND);
        LEXICON.add(OPEN);
        LEXICON.add(CLOSE);
    }

    private final Queue<String> tokens = new LinkedList<String>();

    /**
     * Evaluates an expression to a boolean value.
     *
     * @param expression representation of a boolean expression
     * @return the expression evaluation, either <code>true</code> or <code>false</code>
     * @throws ParseException if unable to parse or evaluate the expression
     */
    public boolean evaluate(String expression) throws ParseException
    {
        tokenize(expression);

        return parseExpression();
    }

    /**
     * Parse an expression, i.e. a series of terms and operators.
     *
     * @return the evaluation of the expression
     * @throws ParseException if unable to parse or evaluate the expression
     */
    private boolean parseExpression() throws ParseException
    {
        return parseOperator(parseTerm());
    }

    /**
     * Parses an 'AND' or 'OR' operation on the given operand and following expression.
     *
     * @param operand term to be passed to the operation
     * @return the evaluation of the operation
     * @throws ParseException if unable to parse or evaluate the operator expression
     */
    private boolean parseOperator(boolean operand) throws ParseException
    {
        while (!tokens.isEmpty())
        {
            if (isNext(AND))
            {
                operand = parseAnd(operand);
            }
            else if (isNext(OR))
            {
                operand = parseOr(operand);
            }
            else
            {
                break;
            }
        }

        return operand;
    }

    /**
     * Parses the next term and 'AND's it with the operand.
     *
     * @param operand term to be 'AND'ed with the following term
     * @return the evaluation of the operation
     * @throws ParseException if unable to parse or evaluate the operation
     */
    private boolean parseAnd(boolean operand) throws ParseException
    {
        read(AND);

        return parseTerm() && operand;
    }

    /**
     * Parses the next expression and 'OR's it with the operand.
     *
     * @param operand term to be 'OR'ed with the following expression
     * @return the evaluation of the operation
     * @throws ParseException if unable to parse or evaluate the operation
     */
    private boolean parseOr(boolean operand) throws ParseException
    {
        read(OR);

        return parseExpression() || operand;
    }

    /**
     * Parses a term, i.e. a not, block or literal.
     *
     * @return the evaluation of the term
     * @throws ParseException if unable to parse or evaluate the term
     */
    private boolean parseTerm() throws ParseException
    {
        if (isNext(NOT))
        {
            return parseNot();
        }
        else if (isNext(OPEN))
        {
            return parseBlock();
        }

        return parseValue();
    }

    /**
     * Parse a block term.
     *
     * @return the evaluation of the block term
     * @throws ParseException if unable to parse or evaluate the block
     */
    private boolean parseBlock() throws ParseException
    {
        read(OPEN);

        boolean result = parseExpression();

        read(CLOSE);

        return result;
    }

    /**
     * Parse a negated term.
     *
     * @return the evaluation of the not term
     * @throws ParseException if unable to parse or evaluate the not term
     */
    private boolean parseNot() throws ParseException
    {
        read(NOT);

        return !parseTerm();
    }

    /**
     * Parse the value of a token or set of tokens beyond the scope of this evaluator.
     *
     * @return the evaluation of the token or set of tokens
     * @throws ParseException if unable to parse or evaluate the value expression
     */
    protected abstract boolean parseValue() throws ParseException;

    /**
     * Return the next token if it exists and is not unexpected.
     *
     * @param expected tokens expected to be read
     * @return the next token read
     * @throws ParseException if next token not the expected
     */
    protected String read(String...expected) throws ParseException
    {
        if (isNext(expected)) return tokens.poll();

        throw new ParseException("Syntax error: Expecting '" + expected + "', got '" + tokens.peek() + "'", 0);
    }

    /**
     * Is the next token one of the expected?
     *
     * @param expected expected tokens
     * @return if the next token belongs to the given expected tokens
     */
    protected boolean isNext(String...expected)
    {
        return !tokens.isEmpty()
            && (expected.length == 0 || Arrays.asList(expected).contains(tokens.peek()));
    }

    /**
     * Tokenize the expression based on the given lexicon.
     *
     * @param expression expression to tokenize
     */
    private void tokenize(String expression)
    {
        tokens.clear();

        StringBuilder buffer = new StringBuilder();

        for (char c : expression.trim().toCharArray())
        {
            String current = Character.toString(c);

            // current character is in the lexicon.
            if (LEXICON.contains(current))
            {
                // add buffer to tokens
                if (buffer.length() > 0)
                {
                    tokens.add(buffer.toString());

                    // reset buffer
                    buffer = new StringBuilder();
                }

                // add current character to tokens
                tokens.add(current);
            }
            // current character is whitespace.
            else if (StringUtils.isBlank(current))
            {
                // add buffer to tokens
                if (buffer.length() > 0)
                {
                    tokens.add(buffer.toString());

                    // reset buffer
                    buffer = new StringBuilder();
                }
            }
            // current character is part a word.
            else
            {
                // add current character to buffer
                buffer.append(current);
            }
        }

        // add buffer to tokens
        if (buffer.length() > 0)
        {
            tokens.add(buffer.toString());
        }
    }
}
