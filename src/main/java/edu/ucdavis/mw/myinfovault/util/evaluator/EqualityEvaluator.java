/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EqualityEvaluator.java
 */

package edu.ucdavis.mw.myinfovault.util.evaluator;

import java.text.ParseException;

/**
 * Implement this interface to evaluate the boolean value of equalities
 * found in expressions, e.g. <pre>some_attribute=value</pre>
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public interface EqualityEvaluator
{
    /**
     * Evaluates the equality for a boolean value.
     *
     * @param LHS left-hand side
     * @param RHS right-hand side
     * @return boolean result of the equality
     * @throws ParseException if the evaluator is unable to parse the equality
     */
    public boolean evaluate(String LHS, String RHS) throws ParseException;
}
