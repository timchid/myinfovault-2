/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationFilter.java
 */

package edu.ucdavis.mw.myinfovault.util;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters a collection of selectable objects
 *
 * @author Craig Gilmore
 * @param <T> Selectable object by which collection is filtered
 * @since MIV 4.4.2
 */
public class SelectedFilter<T extends Selectable> extends SearchFilterAdapter<T>
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(T item)
    {
        return item.isSelected();
    }
}
