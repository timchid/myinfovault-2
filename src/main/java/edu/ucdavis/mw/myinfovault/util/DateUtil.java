/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DateUtil.java
 */
package edu.ucdavis.mw.myinfovault.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

/**
 * Utility class for Date Specific functions
 *
 * @author Pradeep K Haldiya
 * @since v4.7.1
 */
public class DateUtil
{
    /**
     * To get the academic year start date based on date-argument<br>
     * like <b>July 01, YYYY</b>
     * @param date
     * @return
     */
    public static Calendar getAcademicYear(Date date)
    {
        Calendar currentAcademicYear = getAcademicYear(Calendar.getInstance().get(Calendar.YEAR));
        Calendar nextAcademicYear = currentAcademicYear;

        if (currentAcademicYear.getTime().before(date))
        {
            nextAcademicYear.add(Calendar.YEAR, 1);
        }

        return nextAcademicYear;
    }

    /**
     * To get the academic year start date based on today's date<br>
     * like <b>July 01, YYYY</b>
     * @return
     */
    public static Calendar getAcademicYear()
    {
        return getAcademicYear(new Date());
    }

    /**
     * To get the academic year start date based on year-argument<br>
     * like <b>July 01, YYYY</b>
     * @return
     */
    public static Calendar getAcademicYear(int year)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, Calendar.JULY);
        calendar.set(Calendar.DATE, 1);

        return calendar;
    }

    /**
     * To update the year from date
     * @param date
     * @param year
     * @return
     */
    public static Calendar updateYearOfTheDate(Date date, int year)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.YEAR, year);
        return calendar;
    }

    /**
     * To get the year from date
     * @param date
     * @return
     */
    public static int getYear(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.get(Calendar.YEAR);
    }

    /**
     * get the sql safe current date time in string
     *
     * @return
     */
    public static String getSqlSafeCurrentDateTime()
    {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();

        return sdf.format(calendar.getTime());
    }

    /**
     * Null-safe {@link DateUtils#isSameDay(Date, Date)}.
     *
     * @param date1 some date, may be null
     * @param date2 another date, may be null
     * @return if dates represent the same day or are both null
     */
    public static boolean isSameDay(Date date1, Date date2)
    {
        try
        {
            return DateUtils.isSameDay(date1, date2);
        }
        catch (IllegalArgumentException e)
        {
            // dates must be both null
            return date1 == date2;
        }
    }
}
