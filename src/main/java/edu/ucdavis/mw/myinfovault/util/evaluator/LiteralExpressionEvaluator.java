/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ExpressionEvaluator.java
 */

package edu.ucdavis.mw.myinfovault.util.evaluator;

import java.text.ParseException;

import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * <p>Evaluates an expression using the super class {@link ExpressionEvaluator}.
 * Literals found within expressions are evaluated using the {@link LiteralEvaluator}
 * required for instantiation. The literal evaluator is also notified if a literal
 * is found to be "flagged," that is, preceded by the flag operator.</p>
 *
 * <p>
 * <strong>Legal operators:</strong>
 * <ul>
 * <li>{@link ExpressionEvaluator} operators</li>
 * <li>"~" (Flag)</li>
 * </ul>
 * </p>
 *
 * <p>
 * <strong>Example:</strong>
 * <pre>(SOMETHING|~SOMEONE)&!(SOMETHING&~SOMEONE)</pre>
 * </p>
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public class LiteralExpressionEvaluator extends ExpressionEvaluator
{
    private static final String FLAG = "~";

    static {
        LEXICON.add(FLAG);
    }

    private final LiteralEvaluator evaluator;
    private final String[] allowedLiterals;

    /**
     * Create a literal expression evaluator with the given literal evaluator
     * and load the set of allowed literals with string representations
     * of the respective elements of the given array.
     *
     * @param evaluator used to evaluate literals found in expressions
     * @param allowedLiterals objects whose string representations are allowed
     */
    public <T> LiteralExpressionEvaluator(LiteralEvaluator evaluator, T...allowedLiterals)
    {
        this.evaluator = evaluator;
        this.allowedLiterals = StringUtil.convert(allowedLiterals);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.evaluator.ExpressionEvaluator#parseValue()
     */
    @Override
    protected boolean parseValue() throws ParseException
    {
        if (isNext(FLAG))
        {
            return parseFlagged();
        }

        return evaluator.evaluate(read(allowedLiterals));
    }

    /**
     * Parse a flagged literal.
     *
     * @return the evaluation of the flagged literal
     * @throws ParseException if unable to parse or evaluate the literal
     */
    private boolean parseFlagged() throws ParseException
    {
        read(FLAG);

        return evaluator.evaluate(read(allowedLiterals), true);
    }
}
