/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReportUtil.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.ucdavis.myinfovault.message.MivLabels;

/**
 * Utility methods for report creation.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class ReportUtil
{
    /**
     * Prepare a List of report headings given the keys for the report columns.
     * The headers will be returned in the iteration order of the provided keys.
     * @param keys The keys that define a report's columns
     * @return an ordered List of headings to use in a report
     */
    public static List<String> prepareHeaderListByKeyList(Collection<String> keys)
    {
        List<String> headerList = null;

        headerList = new ArrayList<String>();
        for (String key : keys)
        {
            if (key != null && key.trim().length() > 0)
            {
                headerList.add(MivLabels.getInstance().getString(key));
            }
        }

        return headerList;
    }
}
