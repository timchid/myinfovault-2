/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Pager.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.io.Serializable;
import java.util.List;


/**
 * Governs paging over a selectable list of records.
 *
 * @author Craig Gilmore
 * @param <T> Object within collection to be paged
 * @since MIV 4.4.2
 */
public class Pager<T> implements Serializable
{
    private static final long serialVersionUID = 201206131620L;

    //default number of records per page
    private static final int DEFAULT_PAGE_SIZE = 50;

    private final int pageSize;
    private int recordIndex = 0;
    private List<T> records;

    /**
     * Create a paging object with the default page size.
     *
     * @param records List of records over which to page
     * @throws IllegalArgumentException
     */
    public Pager(List<T> records) throws IllegalArgumentException
    {
        this(DEFAULT_PAGE_SIZE, records);
    }

    /**
     * Create a paging object.
     *
     * @param pageSize Maximum size of a page of records
     * @param records List of records over which to page
     * @throws IllegalArgumentException
     */
    public Pager(int pageSize, List<T> records) throws IllegalArgumentException
    {
        if (pageSize <= 0)
        {
            throw new IllegalArgumentException("Page size must be greater than zero");
        }

        this.pageSize = pageSize;
        this.records = records;
    }

    /**
     * @return Maximum size of a page of records
     */
    public int getPageSize()
    {
        return pageSize;
    }

    /**
     * @return The current record index
     */
    public int getRecordIndex()
    {
        return recordIndex;
    }

    /**
     * @return The list of records over which to page
     */
    public List<T> getRecords()
    {
        return records;
    }

    /**
     * @return Total record count
     */
    public int getRecordCount()
    {
        return getRecords().size();
    }

    /**
     * @return The current page index
     */
    public int getPageIndex()
    {
        return recordIndex / pageSize;
    }

    /**
     * @return The total page count
     */
    public int getPageCount()
    {
        return (getRecordCount() + pageSize - 1) / pageSize;
    }

    /**
     * @return The count of records on the current page
     */
    public int getPageRecordCount()
    {
        return isLastPage() ? getRecordCount() - recordIndex : pageSize;
    }

    /**
     * @return Is this page the first page?
     */
    public boolean isFirstPage()
    {
        return getPageIndex() == 0;
    }

    /**
     * @return Is this page the last page?
     */
    public boolean isLastPage()
    {
        return getRecordCount() <= recordIndex + pageSize;
    }

    /**
     * Increment the page index by one unless on the last page
     */
    public void incrementPage()
    {
        if (!isLastPage()) recordIndex += pageSize;
    }

    /**
     * Decrement the page index by one unless on the first page
     */
    public void decrementPage()
    {
        if (!isFirstPage()) recordIndex -= pageSize;
    }
}
