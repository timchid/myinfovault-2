/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReportVO.java
 */

package edu.ucdavis.mw.myinfovault.valuebeans.report;

import java.util.List;
import java.util.Map;

/**
 * Value Object to hold the required data for report view page such as Header and Data list.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class ReportVO
{
    private List<String> keyList = null;
    private List<String> headerList = null;
    private List<Map<String, ResultCellVO>> resultList = null;

    /* Only required when we need check boxes on page */
    private boolean isCheckBoxNeeded = false;
    private int checkBoxMappingColumn = 0;
    private String individualColumnFilters=null;

    /**
     * @return the keyList
     */
    public List<String> getKeyList()
    {
        return keyList;
    }

    /**
     * @param keyList the keyList to set
     */
    public void setKeyList(List<String> keyList)
    {
        this.keyList = keyList;
    }

    /**
     * @return the headerList
     */
    public List<String> getHeaderList()
    {
        return headerList;
    }

    /**
     * @param headerList
     *            the headerList to set
     */
    public void setHeaderList(List<String> headerList)
    {
        this.headerList = headerList;
    }

    /**
     * @return the resultList
     */
    public List<Map<String, ResultCellVO>> getResultList()
    {
        return resultList;
    }

    /**
     * @param resultList
     *            the resultList to set
     */
    public void setResultList(List<Map<String, ResultCellVO>> resultList)
    {
        this.resultList = resultList;
    }

    /**
     * @return the isCheckBoxNeeded
     */
    public boolean isCheckBoxNeeded()
    {
        return isCheckBoxNeeded;
    }

    /**
     * @param isCheckBoxNeeded
     *            the isCheckBoxNeeded to set
     */
    public void setCheckBoxNeeded(boolean isCheckBoxNeeded)
    {
        this.isCheckBoxNeeded = isCheckBoxNeeded;
    }

    /**
     * @return the checkBoxMappingColumn
     */
    public int getCheckBoxMappingColumn()
    {
        return checkBoxMappingColumn;
    }

    /**
     * @param checkBoxMappingColumn
     *            the checkBoxMappingColumn to set
     */
    public void setCheckBoxMappingColumn(int checkBoxMappingColumn)
    {
        this.checkBoxMappingColumn = checkBoxMappingColumn;
    }

    /**
     * @return the individualColumnFilters
     */
    public String getIndividualColumnFilters()
    {
        return individualColumnFilters;
    }

    /**
     * @param individualColumnFilters : comma separate column index list, on which Column Filtering applies
     * <br>i.e 0,1,2 or set all for All columns
     */
    public void setIndividualColumnFilters(String individualColumnFilters)
    {
        this.individualColumnFilters = individualColumnFilters.replaceAll("\\s*,\\s*", ",");
    }
}
