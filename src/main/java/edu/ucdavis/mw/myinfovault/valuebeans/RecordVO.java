/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecordVO.java
 */

package edu.ucdavis.mw.myinfovault.valuebeans;

import java.util.Map;

/**
 * FIXME: Missing Javadoc
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public class RecordVO
{
    Map<String, String[]> requestedData = null;
    boolean validationRequired = true;
    ValidateResultVO validateResultVO = null;
    boolean mainRecord = false;

//    /**
//     * Restrict the use of the no argument constructor (SDP: Why not just leave it out?)
//     */
//    RecordVO()
//    {
//        // Restrict the use of the no-arg constructor. Do nothing here.
//    }


    /**
     * Constructor Map required data to execute this record
     *
     * @param mainRecord to define main record. record type of this record has been used for success message.
     * @param requestedData
     * @param validationRequired default true
     * @param validateResultVO provide only when validationRequired = false
     */
    public RecordVO(boolean mainRecord, Map<String, String[]> requestedData, boolean validationRequired, ValidateResultVO validateResultVO)
    {
        this.mainRecord = mainRecord;
        this.requestedData = requestedData;

        this.validationRequired = validationRequired;

        // Add validateResultVO if validationRequired == true
        if (!validationRequired)
        {
            this.validateResultVO = validateResultVO;
        }
    }

    /* *   FIXME: --------- What does this Javadoc belong to?
     * Constructor Map required data to execute this record
     *
     * @param requestedData
     * @param validationRequired default true
     * @param validateResultVO provide only when validationRequired = false
     */

    /**
     * Constructor Map required data to execute this record
     * Here default validation not call
     *
     * @param mainRecord
     * @param requestedData
     */
    public RecordVO(boolean mainRecord, Map<String, String[]> requestedData)
    {
        this.mainRecord = mainRecord;
        this.requestedData = requestedData;
        this.validationRequired = true;
    }

    /**
     * Constructor Map required data to execute this record
     * Here default validation will not call because validation
     * all ready taken place and validateResultVO provided
     *
     * @param requestedData
     * @param validateResultVO
     */
    public RecordVO(Map<String, String[]> requestedData, ValidateResultVO validateResultVO)
    {

        this.requestedData = requestedData;

        // if validateResultVO found means no need of default validation
        this.validationRequired = false;

        // Add validateResultVO if validationRequired == true
        if (validationRequired)
        {
            this.validateResultVO = validateResultVO;
        }
    }

    /**
     * Constructor Map required data to execute this record
     * Here default validation will call automatically
     *
     * @param requestedData
     */
    public RecordVO(Map<String, String[]> requestedData)
    {
        this.requestedData = requestedData;
    }

    /**
     * @return the requestedData
     */
    public Map<String, String[]> getRequestedData()
    {
        return requestedData;
    }

    /**
     * @return the validationRequired
     */
    public boolean isValidationRequired()
    {
        return validationRequired;
    }

    /**
     * @return the validateResultVO
     */
    public ValidateResultVO getValidateResultVO()
    {
        return validateResultVO;
    }

}
