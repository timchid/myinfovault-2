package edu.ucdavis.mw.myinfovault.daoimpl.biosketch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion.RelOp;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset.Operator;

/**
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchRulesetDaoImpl implements BiosketchRulesetDao
{
    private static final Log logger = LogFactory.getLog(BiosketchRulesetDaoImpl.class);
    private JdbcTemplate jdbcTemplate = new JdbcTemplate();

    /**
     * @param template
     */
    public void setJdbcTemplate(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao#findRulesetListWithCriteria(int,
     *      int) returns a list of biosketchruleset records along with the
     *      biosketchcriteria record list for a particular biosketchid
     */
    @Override
    public List<Ruleset> findRulesetListWithCriteria(int biosketchId, int userId) throws SQLException
    {
        logger.debug("Entering findRulesetListWithCriteria() ... ");

        final String sql = "SELECT * FROM BiosketchRuleset, BiosketchCriteria where BiosketchRuleset.BiosketchID = ? "
                + "AND BiosketchRuleset.UserID = ? AND BiosketchRuleset.ID=BiosketchCriteria.RulesetID ";
        Object[] params = { new Integer(biosketchId), new Integer(userId) };
        // Retrieve query results from database
        List<Ruleset> rulesetList = jdbcTemplate.query(sql.toString(), params, rulesetandcriteriamapper);

        logger.debug("Exiting findRulesetListWithCriteria() ... ");
        return rulesetList;
    }

    /**
     * maps biosketchruleset and biosketchcriteria result set to ruleset and
     * criterion domain objects respectively
     */
    private RowMapper<Ruleset> rulesetandcriteriamapper = new RowMapper<Ruleset>() {
        @Override
        public Ruleset mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            Ruleset ruleset = new Ruleset(rs.getInt("BiosketchRuleset.ID"), rs.getInt("UserID"));
            ruleset.setBiosketchID(rs.getInt("BiosketchID"));
            ruleset.setRecType(rs.getString("RecType"));
            String operator = rs.getString("Operator");
            ruleset.setOperator(Operator.valueOf(operator));

            Criterion criterion = new Criterion(rs.getInt("BiosketchCriteria.ID"));
            criterion.setField(rs.getString("Field"));
            String relop = rs.getString("Comparison");
            criterion.setOperator(RelOp.valueOf(relop));
            criterion.setValue(rs.getString("Value"));

            ruleset.addCriterion(criterion);

            logger.debug("Exiting mapRow() ... ");
            return ruleset;
        }
    };


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao#findRulesetList(int, int)
     *    returns a list of biosketchruleset records.
     */
    @Override
    public List<Ruleset> findRulesetList(int biosketchId, int userId) throws SQLException
    {
        logger.debug("Entering findRulesetListWithCriteria() ... ");

        final String sql = "SELECT * FROM BiosketchRuleset WHERE BiosketchID = ? "
                + "AND UserID = ? ";
        Object[] params = { new Integer(biosketchId), new Integer(userId) };
        // Retrieve query results from database
        List<Ruleset> rulesetList = jdbcTemplate.query(sql.toString(), params, rulesetmapper);

        logger.debug("Exiting findRulesetListWithCriteria() ... ");
        return rulesetList;
    }

    /**
     * maps biosketchruleset result set to ruleset.
     */
    private RowMapper<Ruleset> rulesetmapper = new RowMapper<Ruleset>() {
        @Override
        public Ruleset mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");
            Ruleset ruleset = new Ruleset(rs.getInt("BiosketchRuleset.ID"), rs.getInt("UserID"));
            ruleset.setBiosketchID(rs.getInt("BiosketchID"));
            ruleset.setRecType(rs.getString("RecType"));
            String operator = rs.getString("Operator");
            ruleset.setOperator(Operator.valueOf(operator));
            logger.debug("Exiting mapRow() ... ");
            return ruleset;
        }
    };

    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao#findCriteriaList(int)
     *      returns a list of criterias for a particular ruleset.
     */
    @Override
    public List<Criterion> findCriteriaList(int rulesetid) throws SQLException
    {
        logger.debug("Entering findRulesetListWithCriteria() ... ");

        final String sql = "SELECT * FROM BiosketchCriteria WHERE BiosketchCriteria.RulesetID = ? ";
        Object[] params = { new Integer(rulesetid) };
        // Retrieve query results from database
        List<Criterion> rulesetList = jdbcTemplate.query(sql.toString(), params, criteriamapper);

        logger.debug("Exiting findRulesetListWithCriteria() ... ");
        return rulesetList;
    }

    /**
     * maps criteria result set to criteria set.
     */
    private RowMapper<Criterion> criteriamapper = new RowMapper<Criterion>() {
        @Override
        public Criterion mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            Criterion criterion = new Criterion(rs.getInt("BiosketchCriteria.ID"));
            criterion.setField(rs.getString("Field"));
            String relop = rs.getString("Comparison");
            criterion.setOperator(RelOp.valueOf(relop));
            criterion.setValue(rs.getString("Value"));
            criterion.setRulesetID(rs.getInt("RulesetID"));
            logger.debug("Exiting mapRow() ... ");
            return criterion;
        }
    };



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao#insertBiosketchRuleset(edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset, int)
     * inserts the biosketchruleset record and fetches the biosketchrulesetid.
     */
    @Override
    public int insertBiosketchRuleset(Ruleset ruleset, int userId) throws SQLException
    {
        logger.debug("Entering insertBiosketchRuleset() ... ");

        final String INSERT_BIOSKETCHRULESET_SQL = "INSERT INTO BiosketchRuleset(UserID, BiosketchID, RecType, Operator, InsertTimestamp, InsertUserID, UpdateTimestamp, UpdateUserID)"
                + "VALUES (?,?,?,?,?,?,?,?)";

        final int userID = userId;
        final int biosketchID = ruleset.getBiosketchID();
        final String recType = ruleset.getRecType();
        final String operator = ruleset.getOperator().toString();
        final Date insertTimeStamp = new Date();
        final int insertUserID = userId;
        final Date updateTimeStamp = new Date();
        final int updateUserID = userId;

        KeyHolder keyHolder = new GeneratedKeyHolder();
        // inserts the biosketchruleset record into the the database and
        // collects the biosketchrulesetid
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(INSERT_BIOSKETCHRULESET_SQL, new String[] { "id" });
                ps.setInt(1, userID);
                ps.setInt(2, biosketchID);
                ps.setString(3, recType);
                ps.setString(4, operator);
                ps.setTimestamp(5, new Timestamp(insertTimeStamp.getTime()));
                ps.setInt(6, insertUserID);
                ps.setTimestamp(7, new Timestamp(updateTimeStamp.getTime()));
                ps.setInt(8, updateUserID);
                return ps;
            }
        }, keyHolder);

        logger.debug("Exiting insertBiosketchRuleset() ... ");
        return (keyHolder.getKey().intValue());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao#insertBiosketchCriteria(edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion, int)
     * inserts the biosketchcriteria record associated with a biosketchruleset.
     */
    @Override
    public void insertBiosketchCriteria(Criterion criteria, int userId) throws SQLException
    {
        logger.debug("Entering insertBiosketchCriteria() ... ");

        final String INSERT_BIOSKETCHCRITERIA_SQL = "INSERT INTO BiosketchCriteria(RulesetID, Field, Comparison, Value, InsertTimestamp, InsertUserID, UpdateTimestamp, UpdateUserID)"
                + "VALUES (?,?,?,?,?,?,?,?)";

        final int rulesetID = criteria.getRulesetID();
        final String field = criteria.getField();
        final String comparison = criteria.getOperator().toString();
        final String value = criteria.getValue();
        final Date insertTimeStamp = new Date();
        final int insertUserID = userId;
        final Date updateTimeStamp = new Date();
        final int updateUserID = userId;

        // inserts the biosketchcriteria record associated with the
        // ruleset into the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(INSERT_BIOSKETCHCRITERIA_SQL);
                ps.setInt(1, rulesetID);
                ps.setString(2, field);
                ps.setString(3, comparison);
                ps.setString(4, value);
                ps.setTimestamp(5, new Timestamp(insertTimeStamp.getTime()));
                ps.setInt(6, insertUserID);
                ps.setTimestamp(7, new Timestamp(updateTimeStamp.getTime()));
                ps.setInt(8, updateUserID);
                return ps;
            }
        });

        logger.debug("Exiting insertBiosketchCriteria() ... ");
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao#deleteBiosketchCriteria(int)
     *      deletes the biosketchcriteria records for a particular rulesetid
     */
    @Override
    public void deleteBiosketchCriteria(int rulesetId) throws SQLException
    {
        logger.debug("Entering deleteBiosketchCriteria() ... ");

        final String DELETE_BIOSKETCHCRITERIA_SQL = "DELETE FROM BiosketchCriteria WHERE RulesetID=?";

        final int rulesetID = rulesetId;

        // delets the biosketchcriteria records from the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(DELETE_BIOSKETCHCRITERIA_SQL);
                ps.setInt(1, rulesetID);

                return ps;
            }
        });

        logger.debug("Exiting deleteBiosketchCriteria() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao#deleteBiosketchRuleset(int)
     * deletes the biosketchruleset records of a particular biosketch
     * associated with a user
     */
    @Override
    public void deleteBiosketchRuleset(int rulesetID) throws SQLException
    {
        logger.debug("Entering deleteBiosketchRuleset() ... ");

        final String DELETE_BIOSKETCHRULESET_SQL = "DELETE FROM BiosketchRuleset WHERE ID=?";

        final int ID = rulesetID;

        // deletes the biosketchruleset records from the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(DELETE_BIOSKETCHRULESET_SQL);
                ps.setInt(1, ID);

                return ps;
            }
        });

        logger.debug("Exiting deleteBiosketchRuleset() ... ");
    }
}
