/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierReviewClosed.java
 */

package edu.ucdavis.mw.myinfovault.events;

import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;

/**
 * TODO: Add Javadoc
 * @author stephenp
 *
 */
public class DossierReviewClosed extends DossierReviewStateChange
{
    /**
     * @param dossierId
     * @param actorId
     */
    public DossierReviewClosed(long dossierId, String actorId)
    {
        super(dossierId, actorId, DossierAttributeStatus.OPEN, DossierAttributeStatus.CLOSE);
    }
}
