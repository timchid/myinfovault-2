/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupPersistEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;


/**
 * Group persistence event. Group is either created or updated.
 *
 * @author Craig Gilmore
 */
public class GroupPersistEvent extends GroupEvent
{
    private static final String PATTERN = "User {0,number,#} persisted group {1,number,#}";

    /**
     * Create a group persistence event.
     *
     * @param actorId ID of the real user creating/updating the group
     * @param affectedGroupId ID of the created/updated group
     */
    public GroupPersistEvent(int actorId, int affectedGroupId)
    {
        super(actorId, affectedGroupId, PATTERN);
    }
}
