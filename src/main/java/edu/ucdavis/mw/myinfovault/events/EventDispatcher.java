/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventDispatcher.java
 */

package edu.ucdavis.mw.myinfovault.events;

import com.google.common.eventbus.EventBus;


/**
 * @author Stephen Paulsen
 * @since MIV 4.5
 */
public class EventDispatcher
{
    private static final EventBus bus = new EventBus("MivEventBus");

    private EventDispatcher() {} // prevent instantiation

    public static EventBus getDispatcher()
    {
        return EventDispatcher.bus;
    }
}
