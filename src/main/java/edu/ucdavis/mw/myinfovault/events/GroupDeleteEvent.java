/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupDeleteEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;


/**
 * Group deletion event.
 *
 * @author Craig Gilmore
 */
public class GroupDeleteEvent extends GroupEvent
{
    private static final String PATTERN = "User {0,number,#} deleted group {1,number,#}";

    /**
     * Create a group deletion event.
     *
     * @param actorId ID of the real user deleting the group
     * @param affectedGroupId ID of the deleted group
     */
    public GroupDeleteEvent(int actorId, int affectedGroupId)
    {
        super(actorId, affectedGroupId, PATTERN);
    }
}
