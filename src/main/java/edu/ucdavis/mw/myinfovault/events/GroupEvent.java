/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

import java.text.MessageFormat;

/**
 * Any group event.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public abstract class GroupEvent extends MivEvent
{
    private final String messagePattern;
    private int affectedGroupId;

    /**
     * Create a group event.
     *
     * @param actorId ID of the real user affecting the group
     * @param affectedGroupId ID of the affected group
     * @param messagePattern {@link MessageFormat} pattern for {@link #toString()}
     */
    public GroupEvent(int actorId,
                      int affectedGroupId,
                      String messagePattern)
    {
        super(Integer.toString(actorId));

        this.affectedGroupId = affectedGroupId;
        this.messagePattern = messagePattern;
    }

    /**
     * @return ID of the affected group
     */
    public int getAffectedGroupId()
    {
        return this.affectedGroupId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(messagePattern, Integer.getInteger(getActorId()), getAffectedGroupId());
    }
}
