/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierCancelEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

/**
 * @author Stephen Paulsen
 *
 */
public class DossierCancelEvent extends DossierEvent
{
    private final String location;

    /**
     * @param dossierId
     * @param actorId
     */
    public DossierCancelEvent(long dossierId, String actorId, final String location)
    {
        super(dossierId, actorId);
        this.location = location;
    }


    /**
     * Get the location of the dossier at the time it was cancelled.
     * @return the location
     */
    public String getLocation()
    {
        return this.location;
    }
}
