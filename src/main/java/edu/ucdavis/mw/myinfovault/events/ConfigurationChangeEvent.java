/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ConfigurationChangeEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

import edu.ucdavis.myinfovault.ConfigReloadConstants;

/**
 * TODO: Needs more stuff. This is just a generated shell.
 * @author Stephen Paulsen
 */
public class ConfigurationChangeEvent extends MivEvent
{
    // TODO: need, like, What Thing Changed? What's it's Original Value? What's it's New Value?
    private final ConfigReloadConstants whatChanged;

    /**
     * @param actorId
     * @param what 
     */
    public ConfigurationChangeEvent(String actorId, ConfigReloadConstants what)
    {
        super(actorId);
        // TODO Auto-generated constructor stub
        this.whatChanged = what;
    }

    /**
     * @return
     */
    public ConfigReloadConstants getWhatChanged()
    {
        return this.whatChanged;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return this.whatChanged + " changed by " + this.getActorId();
    }
}
