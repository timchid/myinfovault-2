/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;

/**
 * TODO: Make this a top-level abstract class, with two immediate subclasses -
 *   SignatureRequestEvent and
 *   SigningEvent (or) DocumentSignedEvent (or)
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class SignatureEvent extends MivEvent implements SigEvent
{
    private MivElectronicSignature signature;
    
    public SignatureEvent(MivElectronicSignature signature)
    {
        super(Integer.toString(signature.getSignerId()));
        
        this.signature = signature;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("UserId: ")
          .append(super.getActorId())
          .append(" signed document #")
          .append(signature.getDocument().getDocumentId())
          .append(" with type ")
          .append(signature.getDocument().getDocumentType());

        if (super.getActorId().intern() != signature.getRequestedSigner().intern())
        {
            sb.append(" on behalf of ")
              .append(signature.getRequestedSigner());
        }

        return sb.toString();
    }
}
