/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * @author Rick Hendricks
 * @since MIV 4.6.1
 */
public class RafEvent extends DossierEvent
{
    private RafBo rafBo = null;
    private MivPerson targetPerson = null;
    private MivPerson loggedInPerson = null;
    private MivPerson dossierOwner = null;
    private Dossier dossier = null;

    /**
     * Create recommended action form event.
     * 
     * @param dossier
     * @param rafBo 
     * @param targetPerson 
     * @param loggedInPerson 
     */
    public RafEvent(Dossier dossier, RafBo rafBo, MivPerson targetPerson, MivPerson loggedInPerson)
    {
        super(dossier.getDossierId(), loggedInPerson.getPersonId());
        this.rafBo = rafBo;
        this.targetPerson = targetPerson;
        this.loggedInPerson = loggedInPerson;
        this.dossierOwner = dossier.getCandidate();
        this.dossier = dossier;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("RafEvent for UserId: ").append(this.getActorId());
        sb.append("DossierId: ").append(this.getDossierId());
        return sb.toString();
    }

    /**
     * @return recommended action form for this event
     */
    public RafBo getRafBo()
    {
        return this.rafBo;
    }

    /**
     * @return dossier associated with the recommended action form
     */
    public Dossier getDossier()
    {
        return this.dossier;
    }

    /**
     * @return switched-to, target person
     */
    public MivPerson getTargetPerson()
    {
        return targetPerson;
    }

    /**
     * @return logged-in, real person
     */
    public MivPerson getLoggedInPerson()
    {
        return loggedInPerson;
    }

    /**
     * @return RAF dossier candidate
     */
    public MivPerson getDossierOwner()
    {
        return dossierOwner;
    }
}
