/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivEvent.java
 */


package edu.ucdavis.mw.myinfovault.events;

import java.lang.reflect.Field;


/**
 * ...
 * All events can have an annotation, or "message", attached to them by calling {@link #setAnnotation(String)}
 * or {@link #setMessage(String)} (aliases for the same thing)
 * ...
 * @author Stephen Paulsen
 *
 */
public abstract class MivEvent
{
    private final String actorId;
    private String annotation = null;


    /**
     * Create a new event in MyInfoVault.
     * The actor should be set to the unique Person ID of the person who
     * caused this event. This should be a PersonUUID, that is, person.getPersonId()
     *
     * @param actorId the Person ID of the actor
     */
    public MivEvent(String actorId)
    {
        this.actorId = actorId;
    }


    /**
     * Get the identifier for the person who caused this event.
     * @return the unique Person ID of the actor
     */
    public String getActorId()
    {
        return this.actorId;
    }


    /**
     * Set an annotation on this event, which can be retrieved by the receiver using {@link #getAnnotation()}
     * @param annotation the Annotation to add to this event.
     * @return this
     */
    public MivEvent setAnnotation(String annotation)
    {
        this.annotation = annotation;
        return this;
    }


    /**
     * Retrieve the annotation, if any, added to this event.
     * @return the annotation String, or null if none was set.
     */
    public String getAnnotation()
    {
        return this.annotation;
    }


    /** A synonym for {@link #setAnnotation(String)} */
    public MivEvent setMessage(String message)
    {
        this.annotation = message;
        return this;
    }


    /** A synonym for {@link #getAnnotation()} */
    public String getMessage()
    {
        return this.annotation;
    }


    /**
     * Trying to make a toString that uses reflection so each Event subclass
     * doesn't need its own toString, but all it gets are IllegalAccessExceptions
     * because the fields are private.
     * Could try using field.getName() then generate a getter based on that name;
     * see if the getter method exists and invoke it if so:
     *   fieldName = field.getName();
     *   getter = "get" + initCaps(fieldName);
     *      ... unless the field is a boolean
     *   getter = "is" + initCaps(fieldName);
     *
     * This also doesn't work because getDeclaredFields only lists THIS class'
     * fields, not the superclass fields, and the super.toString() doesn't climb
     * up to the superclass. Each subclass would actually have to declare this
     * same toString method.
     */
    public String toStringDoesntWork()
    {
        StringBuilder s = new StringBuilder();

        // Put the parent Event data in the string first, unless this
        // is the top-level MivEvent class, whose parent is Object and
        // we don't want that toString invoked.
        if (this.getClass() != MivEvent.class) {
            s.append(super.toString()).append(',');
        }

        for (Field field : this.getClass().getDeclaredFields())
        {
            String fieldName = field.getName();
            try
            {
                s.append(fieldName).append(':').append(field.get(this));
            }
            catch (IllegalArgumentException e)
            {
                s.append("bad-arg");
            }
            catch (IllegalAccessException e)
            {
                s.append("can't access");
            }
            s.append(",");
        }
        s.deleteCharAt(s.length()-1); // remove the last trailing comma

        return s.toString();
    }

}
