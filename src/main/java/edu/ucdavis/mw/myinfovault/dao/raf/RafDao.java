/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.raf;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;

/**
 * Data access object for the recommended action form.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public interface RafDao
{
    /**
     * Gets RAF from database associated with the dossier.
     *
     * @param dossier Dossier associated with the RAF
     * @return RAF business object
     */
    public RafBo getRaf(Dossier dossier);

    /**
     * Persists RAF to database.
     *
     * @param raf recommended action form
     * @param realUserId ID of the real, logged-in user effecting changes to the RAF
     * @return <code>true</code> on successful persistence, <code>false</code> if no update occurred
     */
    public boolean persistRaf(RafBo raf, int realUserId);

    /**
     * Removes RAF from database associated with the dossier ID.
     *
     * @param dossierId Dossier ID associated with the RAF
     */
    public void deleteRaf(Long dossierId);
}
