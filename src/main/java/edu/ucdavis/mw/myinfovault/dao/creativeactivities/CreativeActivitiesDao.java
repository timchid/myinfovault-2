/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CreativeActivitiesDao.java
 */
package edu.ucdavis.mw.myinfovault.dao.creativeactivities;

import java.util.List;
import java.util.Map;

/**
 * Creative Activities interface
 * Methods used to access creative activities data
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public interface CreativeActivitiesDao
{
    /**
     * to get the list of non associated events for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    public List<Map<String, String>> getNonAssociatedEventsByUserIdAndWorkIds(int UserId, int...workids);

    /**
     * to get the list of associated events for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    public List<Map<String, String>> getAssociatedEventsByUserIdAndWorkIds(int UserId, int...workids);

    /**
     * to get the list of associated publicationevents for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    public List<Map<String, String>> getAssociatedPublicationEventsByUserIdAndWorkIds(int UserId, int... workids);

    /**
     * to get the list of non associated publicationevents for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    public List<Map<String, String>> getNonAssociatedPublicationEventsByUserIdAndWorkIds(int UserId, int... workids);


    /**
     * to get the list of events by user
     * @param UserId
     * @return List map of events data
     */
    public List<Map<String, String>> getEventsByUserId(int UserId);

    /**
     * to get the list of publicationevents by user
     * @param UserId
     * @return List map of events data
     */
    public List<Map<String, String>> getPublicationEventsByUserId(int UserId);


    /**
     * to get the list of non associated works for event records.
     * @param UserId
     * @param eventids array of eventids
     * @return List map of works data
     */
    public List<Map<String, String>> getNonAssociatedWorksByUserIdAndEventIds(int UserId, int...eventids);

    /**
     * to get the list of associated works for event records.
     * @param UserId
     * @param eventids array of eventids
     * @return List map of works data
     */
    public List<Map<String, String>> getAssociatedWorksByUserIdAndEventIds(int UserId, int...eventids);

    /**
     * to get the list of non associated works for event records.
     * @param UserId
     * @param eventids array of eventids
     * @return List map of works data
     */
    public List<Map<String, String>> getNonAssociatedWorksByUserIdAndPublicationEventIds(int UserId, int...eventids);

    /**
     * to get the list of associated works for event records.
     * @param UserId
     * @param eventids array of eventids
     * @return List map of works data
     */
    public List<Map<String, String>> getAssociatedWorksByUserIdAndPublicationEventIds(int UserId, int...eventids);


    /**
     * to get the list of works by user
     * @param UserId
     * @return List map of works data
     */
    public List<Map<String, String>> getWorksByUserId(int UserId);

    /**
     * to get the list of non associated events for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    public List<Map<String, String>> getNonAssociatedReviewEventsByUserIdAndReviewIds(int UserId, int...reviewids);

    /**
     * to get the list of associated events for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    public List<Map<String, String>> getAssociatedReviewEventsByUserIdAndReviewIds(int UserId, int...reviewids);

    /**
     * to get the list of non associated works for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    public List<Map<String, String>> getNonAssociatedReviewWorksByUserIdAndReviewIds(int UserId, int...reviewids);

    /**
     * to get the list of associated works for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    public List<Map<String, String>> getAssociatedReviewWorksByUserIdAndReviewIds(int UserId, int...reviewids);

    /**
     * to get the list of event ids for a give user and selected year
     * @param UserId
     * @param year
     * @return list of ids
     */
    public List<String> getValidEventIdsByUserIdAndWorkYear(int UserId, int year);

    /**
     * to get the list of work ids for a give user and selected year
     * @param UserId
     * @param year
     * @return list of ids
     */
    public List<String> getValidWorkIdsByUserIdAndEventYear(int UserId, int year);

    /**
     * to get the list of work and event ids for a give user and selected year
     * @param UserId
     * @param year
     * @return list of ids
     */
    public List<String> getValidWorkAndEventIdsByUserIdAndReviewYear(int UserId, int year);

    /**
     * to get the list of events and publicationevents ids for a give user and selected year
     * @param UserId
     * @param year
     * @return events and publication event IDs for the given user and selected year
     */
    public List<String> getValidEventAndPublicationEventsIdsByUserIdAndWorkYear(int UserId, int year);

}
