/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MockEventLogDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.action.Route;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry;
import edu.ucdavis.mw.myinfovault.events2.EventCategory;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Mock implementation of the event log DAO for testing.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class MockEventLogDaoImpl extends EventLogDaoImpl implements EventLogDao
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.event.EventLogDaoImpl#getEntries(long)
     */
    @Override
    public List<EventLogEntry> getEntries(long dossierId) throws DataAccessException
    {
        Calendar cal = Calendar.getInstance();

        Dossier dossier = null;
        try {
            dossier = MivServiceLocator.getDossierService().getDossier(dossierId);
        } catch (WorkflowException e) {
            throw new MivSevereApplicationError("couldn't get dossier in mock event log dao", e);
        }

        MivPerson mivAdmin = MivServiceLocator.getUserService().getPersonByMivId(20001);//craig gilmore
        MivPerson deptAdmin = MivServiceLocator.getUserService().getPersonByMivId(20004);//pete peterson

        List<EventLogEntry> mockEntries = new ArrayList<EventLogEntry>();

        Map<EventCategory, Object> route_packetrequest_department = new HashMap<EventCategory, Object>();
        route_packetrequest_department.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        route_packetrequest_department.put(EventCategory.ROUTE, new Route(DossierLocation.PACKETREQUEST, DossierLocation.DEPARTMENT));

        mockEntries.add(new EventLogEntry(new byte[]{1},
                                          "Dossier Routed",
                                          mivAdmin,
                                          dossier.getAction().getCandidate(),
                                          cal.getTime().getTime(),
                                          "had to route this dossier",
                                          route_packetrequest_department,
                                          new byte[0]));

        Map<EventCategory, Object> create_raf = new HashMap<EventCategory, Object>();
        create_raf.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        create_raf.put(EventCategory.RAF, StringUtil.EMPTY_STRING);

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{2},
                                          "RAF Created",
                                          mivAdmin,
                                          deptAdmin,
                                          cal.getTime().getTime(),
                                          "initial create, I'll make changes later",
                                          create_raf,
                                          new byte[0]));

        Map<EventCategory, Object> edit_raf = new HashMap<EventCategory, Object>();
        edit_raf.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        edit_raf.put(EventCategory.RAF, StringUtil.EMPTY_STRING);

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{3},
                                          "RAF Edited",
                                          mivAdmin,
                                          deptAdmin,
                                          cal.getTime().getTime(),
                                          "Added something",
                                          edit_raf,
                                          new byte[0]));

        Map<EventCategory, Object> upload_letter = new HashMap<EventCategory, Object>();
        upload_letter.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        upload_letter.put(EventCategory.UPLOAD, MIVConfig.getConfig().getMap("documents").get("14"));

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{4},
                                          "Department Letter Uploaded",
                                          mivAdmin,
                                          deptAdmin,
                                          cal.getTime().getTime(),
                                          StringUtil.EMPTY_STRING,
                                          upload_letter,
                                          new byte[0]));

        Map<EventCategory, Object> create_dc = new HashMap<EventCategory, Object>();
        create_dc.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        create_dc.put(EventCategory.DISCLOSURE, StringUtil.EMPTY_STRING);

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{5},
                                          "Disclosure Created",
                                          mivAdmin,
                                          deptAdmin,
                                          cal.getTime().getTime(),
                                          StringUtil.EMPTY_STRING,
                                          create_dc,
                                          new byte[0]));

        Map<EventCategory, Object> request_dc = new HashMap<EventCategory, Object>();
        request_dc.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        request_dc.put(EventCategory.SIGNATURE, MivDocument.DISCLOSURE_CERTIFICATE);
        request_dc.put(EventCategory.REQUEST, dossier.getAction().getCandidate());
        request_dc.put(EventCategory.DISCLOSURE, StringUtil.EMPTY_STRING);

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{6},
                                          "Disclosure Signature Requested",
                                          mivAdmin,
                                          deptAdmin,
                                          cal.getTime().getTime(),
                                          StringUtil.EMPTY_STRING,
                                          request_dc,
                                          new byte[0]));

        Map<EventCategory, Object> sign_dc = new HashMap<EventCategory, Object>();
        sign_dc.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        sign_dc.put(EventCategory.SIGNATURE, MivDocument.DISCLOSURE_CERTIFICATE);
        sign_dc.put(EventCategory.DISCLOSURE, StringUtil.EMPTY_STRING);

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{7},
                                          "Disclosure Signed",
                                          mivAdmin,
                                          dossier.getAction().getCandidate(),
                                          cal.getTime().getTime(),
                                          StringUtil.EMPTY_STRING,
                                          sign_dc,
                                          new byte[0]));

        Map<EventCategory, Object> route_department_packetrequest = new HashMap<EventCategory, Object>();
        route_department_packetrequest.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        route_department_packetrequest.put(EventCategory.ROUTE, new Route(DossierLocation.DEPARTMENT, DossierLocation.PACKETREQUEST));

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{8},
                                          "Dossier Returned",
                                          mivAdmin,
                                          dossier.getAction().getCandidate(),
                                          cal.getTime().getTime(),
                                          "We need to add an appointment to the candidate",
                                          route_department_packetrequest,
                                          new byte[0]));


        /*
         * START: CHILD EVENTS
         */
        Map<EventCategory, Object> child1 = new HashMap<EventCategory, Object>();
        child1.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        child1.put(EventCategory.SIGNATURE, MivDocument.DISCLOSURE_CERTIFICATE);
        child1.put(EventCategory.DISCLOSURE, StringUtil.EMPTY_STRING);

        cal.add(Calendar.MILLISECOND, 1);
        mockEntries.add(new EventLogEntry(new byte[]{9},
                                          "Disclosure Signature Removed",
                                          mivAdmin,
                                          dossier.getAction().getCandidate(),
                                          cal.getTime().getTime(),
                                          StringUtil.EMPTY_STRING,
                                          child1,
                                          new byte[]{8}));

        Map<EventCategory, Object> child2 = new HashMap<EventCategory, Object>();
        child2.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        child2.put(EventCategory.UPLOAD, MIVConfig.getConfig().getMap("documents").get("14"));

        cal.add(Calendar.MILLISECOND, 1);
        mockEntries.add(new EventLogEntry(new byte[]{10},
                                          "Department Letter Removed",
                                          mivAdmin,
                                          deptAdmin,
                                          cal.getTime().getTime(),
                                          StringUtil.EMPTY_STRING,
                                          child2,
                                          new byte[]{8}));
        /*
         * END: CHILD EVENTS
         */


        Map<EventCategory, Object> update_user = new HashMap<EventCategory, Object>();
        update_user.put(EventCategory.USER, dossier.getAction().getCandidate());

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{11},
                                          "User Appointment Added",
                                          mivAdmin,
                                          dossier.getAction().getCandidate(),
                                          cal.getTime().getTime(),
                                          "updated user appointment",
                                          update_user,
                                          new byte[0]));



        Map<EventCategory, Object> route_packetrequest_department2 = new HashMap<EventCategory, Object>();
        route_packetrequest_department2.put(EventCategory.DOSSIER, StringUtil.EMPTY_STRING);
        route_packetrequest_department2.put(EventCategory.ROUTE, new Route(DossierLocation.PACKETREQUEST, DossierLocation.DEPARTMENT));

        cal.add(Calendar.DAY_OF_MONTH, 1);
        mockEntries.add(new EventLogEntry(new byte[]{12},
                                          "Dossier Routed",
                                          mivAdmin,
                                          dossier.getAction().getCandidate(),
                                          cal.getTime().getTime(),
                                          "had to route this dossier, again",
                                          route_packetrequest_department2,
                                          new byte[0]));

        return mockEntries;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.event.EventLogDaoImpl#persistEntry(long, edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry)
     */
    @Override
    public void persistEntry(long dossierId,
                             EventLogEntry entry) throws DataAccessException
    {
        // ignore
    }
}
