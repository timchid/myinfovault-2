/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiEntityDao.java
 */
package edu.ucdavis.mw.myinfovault.dao.apientity;

import edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity;

/**
 * Handles database interactions involving ApiEntities.
 *
 * @author japorito
 * @since 5.0
 */
public interface ApiEntityDao
{
    /**
     * Gets the entity with the specified name.
     *
     * @param entityName
     * @return The entity with the specified name or null, if one doesn't exist.
     */
    public ApiEntity getApiEntity(String entityName);

    /**
     * Gets the entity with the specified id.
     *
     * @param entityId
     * @return The entity with the specified ih or null, if one doesn't exist.
     */
    public ApiEntity getApiEntity(int entityId);

    /**
     * Updates or creates the specified entity, including it's scopes.
     *
     * @param entity
     * @return Whether the persist was successful.
     */
    public Boolean persistApiEntity(ApiEntity entity);

    /**
     * Deletes the specified entity.
     *
     * @param entity
     * @return the number of affected rows.
     */
    public int deleteApiEntity(ApiEntity entity);
}
