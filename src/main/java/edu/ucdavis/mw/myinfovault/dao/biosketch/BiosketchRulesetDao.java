/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchRulesetDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;

/**
 * TODO: missing javadoc
 * 
 * @author dreddy
 * @since MIV 2.1
 */
public interface BiosketchRulesetDao
{
    /**
     * Returns a list of biosketch rule sets records along with the biosketch criteria record list for a particular biosketch ID.
     * 
     * @param biosketchId
     * @param userId
     * @return
     * @throws SQLException
     */
    public List<Ruleset> findRulesetListWithCriteria(int biosketchId, int userId) throws SQLException;

    /**
     * Inserts the biosketch rule set record and fetches the biosketch rule set ID.
     * 
     * @param ruleset
     * @param userID
     * @return
     * @throws SQLException
     */
    public int insertBiosketchRuleset(Ruleset ruleset, int userID) throws SQLException;

    /**
     * Inserts the biosketch criteria record associated with a biosketch rule set.
     * 
     * @param criteria
     * @param userID
     * @throws SQLException
     */
    public void insertBiosketchCriteria(Criterion criteria, int userID) throws SQLException;

    /**
     * Deletes the biosketch criteria records for a particular rule set ID.
     * 
     * @param rulesetID
     * @throws SQLException
     */
    public void deleteBiosketchCriteria(int rulesetID) throws SQLException;

    /**
     * Deletes the biosketch rule set records of a particular biosketch associated with a user.
     * 
     * @param rulesetID
     * @throws SQLException
     */
    public void deleteBiosketchRuleset(int rulesetID) throws SQLException;    
    
    /**
     * Returns a list of biosketch rule set records.
     * 
     * @param biosketchId
     * @param userId
     * @return
     * @throws SQLException
     */
    public List<Ruleset> findRulesetList(int biosketchId, int userId) throws SQLException;
    
    /**
     * Returns a list of criteria records for a rule set.
     * 
     * @param rulesetid
     * @return
     * @throws SQLException
     */
    public List<Criterion> findCriteriaList(int rulesetid) throws SQLException;
}
