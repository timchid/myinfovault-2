/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiEntityDaoImpl.java
 */
package edu.ucdavis.mw.myinfovault.dao.apientity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiEntityImpl;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiPermission;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiResourceType;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiScope;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiSecurityScheme;

/**
 * @author japorito
 * @since 5.0
 */
public class ApiEntityDaoImpl extends DaoSupport implements ApiEntityDao
{

    private final String selectEntityById;
    private final String selectEntityByName;
    private final String selectEntityScopesByEntityId;
    private final String deleteEntityScopesByEntityId;
    private final String deleteEntityById;
    private final String insertEntity;
    private final String updateEntity;
    private final String insertEntityScope;

    public ApiEntityDaoImpl()
    {
        selectEntityById = getSQL("apientity.select.entityid");
        selectEntityByName = getSQL("apientity.select.entityname");
        selectEntityScopesByEntityId = getSQL("apientityscope.select.entityid");
        deleteEntityScopesByEntityId = getSQL("apientityscope.delete");
        deleteEntityById = getSQL("apientity.delete");
        insertEntity = getSQL("apientity.insert");
        updateEntity = getSQL("apientity.update");
        insertEntityScope = getSQL("apientityscope.insert");
    }

    @Override
    public ApiEntity getApiEntity(String entityName)
    {
        ApiEntity entity = null;
        Object[] args = { entityName };

        try
        {
            entity = this.getJdbcTemplate().queryForObject(selectEntityByName, args, entityMapper);
        }
        catch (EmptyResultDataAccessException e)
        {
            return null;
        }

        entity.setScopes(this.getApiEntityScopes(entity.getEntityId()));

        return entity;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.apientity.ApiEntityDao#getApiEntity(int)
     */
    @Override
    public ApiEntity getApiEntity(int entityId)
    {
        ApiEntity entity = null;
        Object[] args = { entityId };

        try
        {
            entity = this.getJdbcTemplate().queryForObject(selectEntityById, args, entityMapper);
        }
        catch (EmptyResultDataAccessException e)
        {
            return null;
        }

        entity.setScopes(this.getApiEntityScopes(entity.getEntityId()));

        return entity;
    }

    private Map<ApiResourceType, List<ApiScope>> getApiEntityScopes(int entityId)
    {
        Map<ApiResourceType, List<ApiScope>> scopes = new HashMap<ApiResourceType, List<ApiScope>>();
        Object[] args = { entityId };

        try
        {
            for(ApiScope scope : this.getJdbcTemplate().query(selectEntityScopesByEntityId, args, scopeMapper))
            {
                List<ApiScope> relevantScopes = scopes.get(scope.getResource());

                if (relevantScopes == null)
                {
                    relevantScopes = new ArrayList<ApiScope>();
                }

                relevantScopes.add(scope);

                scopes.put(scope.getResource(), relevantScopes);
            }
        }
        catch (EmptyResultDataAccessException e)
        {
            //return empty scopes map
        }

        return scopes;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.apientity.ApiEntityDao#persistApiEntity(edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity)
     */
    @Override
    public Boolean persistApiEntity(final ApiEntity entity)
    {
        final boolean isUpdate = entity.getEntityId() > 0;
        final String query = isUpdate ? updateEntity : insertEntity;

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int rowsAffected = this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(query,
                                                                   Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, entity.getEntityName());
                ps.setString(2, entity.getKeyHash());
                ps.setString(3, entity.getSalt());
                ps.setString(4, entity.getSecurityScheme().toString());
                ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));

                if (isUpdate)
                {
                    ps.setInt(6, entity.getEntityId());
                }

                return ps;
            }
        }, keyHolder);

        // persist occurred
        if (rowsAffected > 0)
        {
            // update decision record ID
            if (!isUpdate)
            {
                entity.setEntityId(keyHolder.getKey().intValue());
            }

            return persistApiEntityScopes(entity);
        }

        return false;

    }

    private Boolean persistApiEntityScopes(final ApiEntity entity)
    {
        final boolean isUpdate = entity.getEntityId() > 0;
        boolean allScopesAdded = true;

        if (isUpdate)
        {
            deleteAllScopesByEntityId(entity.getEntityId());
        }

        for (ApiScope scope : entity.getAllScopes())
        {
            if (!insertApiEntityScope(entity.getEntityId(), scope)) allScopesAdded = false;
        }

        return allScopesAdded;
    }

    private Boolean insertApiEntityScope(final int entityId, final ApiScope scope)
    {
        int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(insertEntityScope);

                ps.setInt(1, entityId);
                ps.setInt(2, scope.getSchoolId());
                ps.setInt(3, scope.getDepartmentId());
                ps.setInt(4, scope.getUserId());
                ps.setBoolean(5, scope.getPermissions().contains(ApiPermission.CREATE));
                ps.setBoolean(6, scope.getPermissions().contains(ApiPermission.READ));
                ps.setBoolean(7, scope.getPermissions().contains(ApiPermission.UPDATE));
                ps.setBoolean(8, scope.getPermissions().contains(ApiPermission.DELETE));
                ps.setString(9, scope.getResource().toString());

                return ps;
            }
        });

        return rowsAffected > 0;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.apientity.ApiEntityDao#deleteApiEntity(edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity)
     */
    @Override
    public int deleteApiEntity(ApiEntity entity)
    {
        deleteAllScopesByEntityId(entity.getEntityId());

        return this.getJdbcTemplate().update(deleteEntityById, entity.getEntityId());
    }

    private void deleteAllScopesByEntityId(int entityId)
    {
        update(deleteEntityScopesByEntityId, entityId);
    }

    private RowMapper<ApiEntity> entityMapper = new RowMapper<ApiEntity>() {
        @Override
        public ApiEntity mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            int entityId = rs.getInt("ID");
            String entityName = rs.getString("entityName");
            String secret = rs.getString("KeyHash");
            String salt = rs.getString("Salt");
            ApiSecurityScheme scheme = ApiSecurityScheme.valueOf(rs.getString("SecurityScheme"));

            ApiEntity entity = new ApiEntityImpl(entityId, entityName, secret, salt, scheme);
            return entity;
        }
    };

    private RowMapper<ApiScope> scopeMapper = new RowMapper<ApiScope>() {
        @Override
        public ApiScope mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            ApiScope scope = new ApiScope();

            scope.setSchoolId(rs.getInt("schoolID"));
            scope.setDepartmentId(rs.getInt("departmentID"));
            scope.setUserId(rs.getInt("userID"));
            Set<ApiPermission> permissions = new HashSet<ApiPermission>();

            String[] operations = { "Create", "Read", "Update", "Delete" };
            for (String operation : operations)
            {
                if (rs.getBoolean(operation))
                {
                    permissions.add(ApiPermission.valueOf(operation.toUpperCase()));
                }
            }
            scope.setPermissions(permissions);

            scope.setResource(ApiResourceType.valueOf(rs.getString("Resource")));

            return scope;
        }
    };
}
