/*

 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.aa;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.PolarResponse;
import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDetails;
import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDuration;
import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.SalaryPeriod;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Step;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handles recommended action persistence.
 *
 * @author Rick Hendricks
 * @since MIV 4.8
 */
public class AcademicActionDaoImpl extends DaoSupport implements AcademicActionDao
{
    /**
     * Creates the AA DAO object and initializes it with SQL
     * for handling the persistence.
     */
    public AcademicActionDaoImpl()
    {
        super("academicaction", true);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.aa.AcademicActionActionDao#persistRaf(edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionFormBo, int)
     */
    @Override
    @Transactional // Will rollback for runtime exceptions (DataAccessException)
    @SuppressWarnings("unchecked")  // For CollectionUtils
    public boolean persistAa(AcademicActionBo updatedAa, int realUserId)
    {
        // Get the old (current) academic action assignments from the database
        List<Assignment> oldAssignments = getActionAssignments(updatedAa.getDossier().getAcademicActionID());

        // Get the new academic action assignments
        List<Assignment> newAssignments = updatedAa.getAssignments();

        // Get the assignments to delete
        Collection<Assignment> assignmentsToDelete = CollectionUtils.subtract(oldAssignments, newAssignments);

        // Get the assignments which were potentially updated
        Collection<Assignment> updateAssignments = CollectionUtils.intersection(newAssignments, oldAssignments);

        // Check each updated assignment for any titles to remove
        Collection<Title>titlesToDelete = new ArrayList<Title>();
        for (Assignment assignment : updateAssignments)
        {
            for (Assignment oldAssignment : oldAssignments)
            {
                if (oldAssignment.equals(assignment))
                {
                    titlesToDelete.addAll(CollectionUtils.subtract(oldAssignment.getTitles(), assignment.getTitles()));
                }
            }
        }

        try
        {
            this.deleteTitles(titlesToDelete);
            this.deleteAssignments(assignmentsToDelete);
            this.saveAcademicAction(updatedAa, realUserId);
            this.saveAppointmentDetails(updatedAa.getDossier().getAcademicActionID(), updatedAa.getApptDetails(), realUserId);
            this.saveAssignments(updatedAa, realUserId);
            return true;
        }
        catch (DataAccessException dae)
        {
            return false;
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.aa.AcademicActionDao#delete(int)
     */
    @Override
    public void delete(int academicActionID)
    {
        update( getSQL("appointmentdetails.delete"), academicActionID );
        update( getSQL("actionassignment.delete.action"), academicActionID );
    }


    /**
     * @param assignments
     */
    private void deleteAssignments(Collection<Assignment>assignments)
    throws DataAccessException
    {
        for (Assignment assignment : assignments)
        {
            this.deleteAssignment(assignment);
        }
    }


    /**
     * @param assignment
     */
    private void deleteAssignment(Assignment assignment)
    throws DataAccessException
    {
        Object[] params = {assignment.getID()};
        int[] paramTypes = {Types.INTEGER };
        this.getJdbcTemplate().update( getSQL("actionassignment.delete"), params, paramTypes );
    }


    /**
     * @param titlesToDelete
     */
    private void deleteTitles(Collection<Title> titlesToDelete)
    throws DataAccessException
    {
        for (Title title : titlesToDelete)
        {
            this.deleteTitle(title);
        }
    }


    /**
     * @param title
     */
    private void deleteTitle(Title title)
    throws DataAccessException
    {
        Object[] params = {title.getID()};
        int[] paramTypes = {Types.INTEGER };
        this.getJdbcTemplate().update( getSQL("assignmenttitles.delete"), params, paramTypes );
    }


    /**
     * @param aa
     * @param updateUserId
     * @throws DataAccessException
     */
    private void saveAcademicAction(final AcademicActionBo aa, final int updateUserId)
    throws DataAccessException
    {
        final AcademicAction academicAction = aa.getDossier().getAction();
        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("update.dossierid") );
                ps.setInt(1, academicAction.getActionType().getActionTypeId());
                ps.setInt(2, academicAction.getDelegationAuthority().getDelegationAuthorityId());
                ps.setDate(3, new java.sql.Date(academicAction.getEffectiveDate().getTime()));
                ps.setDate(4, academicAction.getRetroactiveDate() != null ? new java.sql.Date(academicAction.getRetroactiveDate().getTime()) : null);
                ps.setDate(5, academicAction.getEndDate() != null ? new java.sql.Date(academicAction.getEndDate().getTime()) : null);
                ps.setInt(6, academicAction.getAccelerationYears());
                ps.setInt(7, updateUserId);
                ps.setLong(8, aa.getDossier().getAcademicActionID());
                return ps;
            }
        });
    }


    /**
     * @param apptDetails
     * @param userId
     * @throws DataAccessException
     */
    private void saveAppointmentDetails(int academicActionID, final AppointmentDetails apptDetails, final int userId)
    throws DataAccessException
    {
        // Determine whether this is an update or an insert
        if (apptDetails.getID() <= 0)
        {
            this.insertAppointmentDetails(academicActionID, apptDetails, userId);
        }
        else
        {
            this.updateAppointmentDetails(academicActionID, apptDetails, userId);

        }
    }


    /**
     * @param apptDetails
     * @param userId
     * @throws DataAccessException
     */
    private void insertAppointmentDetails(final int academicActionID, final AppointmentDetails apptDetails, final int userId)
    throws DataAccessException
    {
        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("appointmentdetails.insert") );
                ps.setInt(1, academicActionID);
                ps.setString(2, apptDetails.getCurrentEmployee().toString());
                ps.setString(3, apptDetails.getRepresented().toString());
                ps.setString(4, apptDetails.getNoticeToUnion().toString());
                ps.setString(5, apptDetails.getNoticeToLaborRelations().toString());
                ps.setInt(6, userId);
                return ps;
            }
        });
    }


    /**
     * @param apptDetails
     * @param userId
     * @throws DataAccessException
     */
    private void updateAppointmentDetails(final int academicActionID, final AppointmentDetails apptDetails, final int userId)
    throws DataAccessException
    {
        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("appointmentdetails.update") );
                ps.setString(1, apptDetails.getCurrentEmployee().name());
                ps.setString(2, apptDetails.getRepresented().name());
                ps.setString(3, apptDetails.getNoticeToUnion().name());
                ps.setString(4, apptDetails.getNoticeToLaborRelations().name());
                ps.setInt(5, userId);
                ps.setInt(6, academicActionID);
                return ps;
            }
        });
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.aa.AcademicActionDao#getAppointmentDetails(int)
     */
    @Override
    public AppointmentDetails getAppointmentDetails(int academicActionID)
    {
        try {
            return getFirst( get( getSQL("appointmentdetails.select"), appointmentDetailsRowMapper, academicActionID ) );
        }
        catch (DataAccessException e) {
            throw new MivSevereApplicationError("errors.aaf.dao.appointment.read", e, academicActionID);
        }
    }


    /**
     * Maps records from the AppointmentDetails table to {@link AppointmentDetails} objects.
     */
    private RowMapper<AppointmentDetails> appointmentDetailsRowMapper = new RowMapper<AppointmentDetails>()
    {
        @Override
        public AppointmentDetails mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new AppointmentDetails(rs.getInt("ID"),
                                          PolarResponse.valueOf(rs.getString("Employee")),
                                          PolarResponse.valueOf(rs.getString("Represented")),
                                          PolarResponse.valueOf(rs.getString("NoticeToUnion")),
                                          PolarResponse.valueOf(rs.getString("NoticeToLaborRelation")));
        }
    };


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.aa.AcademicActionDao#getActionAssignments(int, edu.ucdavis.mw.myinfovault.domain.action.StatusType)
     */
    @Override
    public List<Assignment> getActionAssignments(int academicActionID, StatusType status)
    {
        try {
            return get( getSQL("actionassignment.select.status"), actionAssignmentRowMapper, academicActionID, status.name() );
        }
        catch (DataAccessException e) {
            throw new MivSevereApplicationError("errors.aaf.dao.assignments.read", e, status, academicActionID);
        }
    }


    /**
     * Get both present and proposed assignments for an academic action.
     *
     * @param academicActionID academic action record ID
     * @return academic action assignments
     */
    private List<Assignment> getActionAssignments(int academicActionID)
    {
        try {
            return get( getSQL("actionassignment.select"), actionAssignmentRowMapper, academicActionID );
        }
        catch (DataAccessException e) {
            throw new MivSevereApplicationError("errors.aaf.dao.assignments.read", e, academicActionID);
        }
    }


    /**
     * ActionAssignmentRowMapper
     * @author rhendric
     */
    private RowMapper<Assignment> actionAssignmentRowMapper = new RowMapper<Assignment>()
    {
        @Override
        public Assignment mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            int assignmentID = rs.getInt("ID");
            int schoolID = rs.getInt("SchoolID");
            String schoolName = rs.getString("SchoolName");
            int departmentID = rs.getInt("DepartmentID");
            String departmentName = rs.getString("DepartmentName");
            boolean primaryAssignment = rs.getBoolean("PrimaryAssignment");
            BigDecimal percentageOfTime = rs.getBigDecimal("PercentageOfTime");

            Assignment assignment = new Assignment(assignmentID,
                    getTitles(assignmentID),
                    primaryAssignment,
                    new Scope(schoolID, departmentID),
                    schoolName,
                    departmentName,
                    percentageOfTime);

            return  assignment;
        }
    };


    /**
     * Get titles for the given assignment.
     *
     * @param assignmentID record ID of the assignment
     * @return assignment titles
     */
    private List<Title> getTitles(int assignmentID)
    {
        return get( getSQL("assignmenttitles.select"), assignmentTitleRowMapper, assignmentID );
    }


    /**
     * Maps 'AssignmentTitles' table records to {@link Title} objects.
     */
    private RowMapper<Title> assignmentTitleRowMapper = new RowMapper<Title>()
    {
        @Override
        public Title mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new Title(rs.getInt("ID"),
                             rs.getInt("AssignmentID"),
                             rs.getString("RankTitle"),
                             new Step(rs.getString("Step")),
                             rs.getString("TitleCode"),
                             rs.getBigDecimal("PercentageOfTime"),
                             rs.getBoolean("WithoutSalary"),
                             AppointmentDuration.valueOf(rs.getString("AppointmentType")),
                             SalaryPeriod.valueOf(rs.getString("SalaryType")),
                             rs.getBigDecimal("Salary"),
                             rs.getBigDecimal("AnnualSalary"),
                             rs.getInt("YearsAtRank"),
                             rs.getInt("YearsAtStep"));
        }
    };


    /**
     * @param aa
     * @param userID
     * @throws DataAccessException
     */
    private void saveAssignments(final AcademicActionBo aa, final int userID)
    throws DataAccessException
    {
        for (StatusType status : aa.getAssignmentStatuses().keySet())
        {
            for (final Assignment assignment : aa.getAssignmentStatuses().get(status))
            {
                final Map<String, String> department = MIVConfig.getConfig().getMap("departments").get(assignment.getScope().toString());

                if (assignment.getID() <= 0)
                {
                    this.insertAssignments(aa.getDossier().getAcademicActionID(), status, assignment,  department, userID);
                }
                else
                {
                    this.updateAssignments(assignment, department, userID);
                }
            }
        }
    }


    /**
     * @param academicActionID
     * @param assignment
     * @param department
     * @param userId
     * @throws DataAccessException
     */
    private void insertAssignments(final int academicActionID, final StatusType status, final Assignment assignment, final Map<String, String> department, final int userID)
    throws DataAccessException
    {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("actionassignment.insert"), Statement.RETURN_GENERATED_KEYS );
                ps.setInt(1,0);
                ps.setInt(2, academicActionID);
                ps.setInt(3, assignment.getScope().getSchool());
                ps.setString(4, department.get("school"));
                ps.setInt(5, assignment.getScope().getDepartment());
                ps.setString(6, department.get("description"));
                ps.setString(7, status.name());
                ps.setBoolean(8, assignment.isPrimary());
                ps.setBigDecimal(9, assignment.getPercentOfTime());
                ps.setInt(10, userID);
                return ps;
            }
        }, keyHolder);

        // Save the associated titles
        this.saveTitles(assignment.getTitles(), keyHolder.getKey().intValue(), userID);
    }


    /**
     * @param assignment
     * @param department
     * @param userID
     * @throws DataAccessException
     */
    private void updateAssignments(final Assignment assignment, final Map<String, String> department, final int userID)
            throws DataAccessException
            {
            this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("actionassignment.update") );
                ps.setInt(1, assignment.getScope().getSchool());
                ps.setString(2, department.get("school"));
                ps.setInt(3, assignment.getScope().getDepartment());
                ps.setString(4, department.get("description"));
                ps.setBoolean(5, assignment.isPrimary());
                ps.setBigDecimal(6, assignment.getPercentOfTime());
                ps.setInt(7, userID);
                ps.setInt(8, assignment.getID());
                return ps;
            }
        });

        // Save the associated titles
        this.saveTitles(assignment.getTitles(), assignment.getID(), userID);
    }


    /**
     * @param titles
     * @param assignmentID
     * @param userID
     * @throws DataAccessException
     */
    private void saveTitles(final List<Title> titles, final int assignmentID, final int userID) throws DataAccessException
    {
        for (final Title title : titles)
        {
            // is this an existing title?
            final boolean isUpdate = title.getID() > 0;

            this.getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement( getSQL(isUpdate ? "assignmenttitles.update" : "assignmenttitles.insert") );

                    ps.setString(1, title.getDescription());
                    if (title.getStep() == null || title.getStep().getValue() == null)
                    {
                        ps.setNull(2, Types.TINYINT);
                    }
                    else
                    {
                        ps.setBigDecimal(2, title.getStep().getValue());
                    }
                    ps.setString(3, title.getCode());
                    ps.setBigDecimal(4, title.getPercentOfTime());
                    ps.setBoolean(5, title.isWithoutSalary() && title.getPercentOfTime().compareTo(BigDecimal.ZERO) == 0);
                    ps.setString(6, title.getAppointmentDuration() == null ? AppointmentDuration.NA.name() : title.getAppointmentDuration().name());
                    ps.setString(7, title.getSalaryPeriod().name());
                    ps.setBigDecimal(8, title.getPeriodSalary());
                    ps.setInt(9, title.getAnnualSalary().intValue());
                    ps.setInt(10, title.getYearsAtRank());
                    ps.setInt(11, title.getYearsAtStep());
                    ps.setInt(12, userID);
                    ps.setInt(13, isUpdate ? title.getID() : assignmentID);
                    return ps;
                }
            });
        }
    }
}
