/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.disclosurecertificate;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;

/**
 * Data access object for the disclosure certificate. Unless
 * stated, all queries ignore archived disclosure certificates.
 *
 * @author Jed Whitten
 * @since MIV 3.0
 */
public interface DCDao
{
    /**
     * Gets the most recent draft of the DC from database associated with
     * the dossier and scope.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     * @return DC business object or <code>null</code> if DNE
     */
    public DCBo getLatestDC(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets the most recent draft of the DC from database that has a signature/request
     * and is associated with the dossier and scope.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     * @return DC business object or <code>null</code> if DNE
     */
    public DCBo getLatestWithSignatureDC(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets all DCs from database that have a signature/request
     * and is associated with the dossier and scope.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     * @return DC business object or <code>null</code> if DNE
     */
    public List<DCBo> getAllWithSignatureDC(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets the most recent draft of the DC from database that has an unsigned
     * signature/request and is associated with the dossier and scope.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     * @return DC business object or <code>null</code> if DNE
     */
    public DCBo getLatestRequestedDC(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets the most recent draft of the DC from database that has a signed
     * signature associated with the dossier, school, and department IDs.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     * @return DC business object or <code>null</code> if DNE
     */
    public DCBo getLatestSignedDC(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets the second most recent draft of the DC from database that has a
     * signed signature associated with the dossier, school, and department IDs.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     * @return DC business object or <code>null</code> if DNE
     */
    public DCBo getPreviouslySignedDC(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets DC from database for the given record ID.
     *
     * @param recordId The disclosure certificate record ID
     * @return DC business object or <code>null</code> if DNE
     */
    public DCBo getDC(int recordId);

    /**
     * Gets the two most recent records from the DC from database associated with the
     * dossier, school, and department IDs. Archived records are ignored.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     * @return List of DC business objects
     */
    public List<DCBo> getLatestTwoDCs(Dossier dossier, int schoolId, int departmentId);

    /**
     * Persists DC to database.
     *
     * @param dc business object
     * @param realUserId ID of the MIV person effecting changes
     * @return if persistence was successful
     */
    public boolean persistDC(DCBo dc, int realUserId);

    /**
     * Gets every DC for the given dossier and scope.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DCs
     * @param departmentId Department ID associated with the DCs
     * @return list of DC business objects
     */
    public List<DCBo> getDCs(Dossier dossier, int schoolId, int departmentId);

    /**
     * Mark given disclosure certificate as 'Archived'.
     *
     * @param recordId disclosure certificate record ID
     */
    public void archiveDC(int recordId);

    /**
     * Mark latest signed DC as Archived.
     *
     * @param dossier Dossier associated with the DC
     * @param schoolId School ID associated with the DC
     * @param departmentId Department ID associated with the DC
     */
    public void archiveSignedDC(Dossier dossier, int schoolId, int departmentId);

    /**
     * Delete a DC of a given record ID.
     *
     * @param recordId Record ID corresponding to the DC
     */
    public void deleteDC(int recordId);
}
