/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivUserReportDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.myinfovault.data.QueryConditionBuilder;
import edu.ucdavis.myinfovault.data.QueryResultMapper;

/**
 * This class implements all methods required for user report. it implements all
 * methods of interface MivUserReportDao.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class MivUserReportDaoImpl extends JdbcDaoSupport implements MivUserReportDao
{
    // Partial statement! Will be appended with conditions in findUsers()
    private static final String FIND_USERS_PARTIAL_SQL = " SELECT UserID, SortName, MivCode, "
            + " getSchoolNameBySchoolID(SchoolID) AS SchoolName,  "
            + " getDepartmentNameByDepartmentID(SchoolID,DepartmentID) AS DepartmentName, PrimaryRole "
            + " FROM UserAssignment WHERE RoleID not in (1,7,8,9,10,12,13,14,15,16) ";

    /**
     * To find user by AttributeSet. Result also contains assignments details
     *
     * @param criteria
     * @return List of Map
     */
    @Override
    public List<Map<String, Object>> findUsers(final AttributeSet criteria)
    {
        final StringBuilder sql = new StringBuilder(FIND_USERS_PARTIAL_SQL);
        final List<String> args = new ArrayList<String>();

        if (criteria != null && criteria.size() > 0)
        {
            for (final String key : criteria.keySet())
            {
                final QueryConditionBuilder c = searches.get(key);
                if (c != null)
                {
                    sql.append(c.getSql());
                    c.addArgs(args, criteria.get(key).replace('*', '%'));
                }
            }
        }
        else
        // Default: It will returns only Primary Assignments
        {
            final QueryConditionBuilder c = searches.get("DEFAULT");
            if (c != null)
            {
                sql.append(c.getSql());
            }
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Script:: " + sql.toString());
            logger.debug("Args:: " + args.toArray().length);
            int count = 0;
            for (String string : args) {
                logger.debug("Args[" + ++count + "] :: " + string);
            }
        }

        @SuppressWarnings("unchecked")
        List<Map<String, Object>> resultList = this.getJdbcTemplate().query(sql.toString(), args.toArray(), QueryResultMapper.resultMapper);
        return resultList;
    }

    /*
     * findUsers support for building a query based on an AttributeSet
     */

    private static Map<String, QueryConditionBuilder> searches = new HashMap<String, QueryConditionBuilder>();
    static
    {
        searches.put("DEFAULT", new QueryConditionBuilder(" AND PrimaryRole = true", 0));
        searches.put(MivSearchKeys.Person.MIV_ACTIVE_USER, new QueryConditionBuilder(" AND Active=true", 0));
        searches.put(MivSearchKeys.Person.PERSON_UUID, new QueryConditionBuilder(" AND PersonUUID=?", 1));
        searches.put(MivSearchKeys.Person.MIV_SCHOOL_CODE, new QueryConditionBuilder(" AND SchoolID=?", 1));
        searches.put(MivSearchKeys.Person.MIV_DEPARTMENT_CODE, new QueryConditionBuilder(" AND DepartmentID=? ", 1));
        // Use this when we want to show deans in search by department
        // " AND ( CASE WHEN RoleID NOT IN (3) THEN DepartmentID=? ELSE SchoolID=SchoolID END ) ",
        // 1));
        searches.put(MivSearchKeys.Person.NAME, new QueryConditionBuilder(
        // " AND (PreferredName LIKE ? OR Surname LIKE ? OR GivenName LIKE ? OR MiddleName LIKE ?)",
        // 4));
                " AND ( PreferredName LIKE ? OR SortName LIKE CONVERT(? USING utf8) )", 2));
        searches.put(MivSearchKeys.Person.DISPLAY_NAME, new QueryConditionBuilder(" AND PreferredName LIKE ?", 1));
        searches.put(MivSearchKeys.Person.FIRST_NAME, new QueryConditionBuilder(" AND GivenName LIKE ?", 1));
        searches.put(MivSearchKeys.Person.MIDDLE_NAME, new QueryConditionBuilder(" AND MiddleName LIKE ?", 1));
        searches.put(MivSearchKeys.Person.LAST_NAME, new QueryConditionBuilder(" AND Surname LIKE ?", 1));
        searches.put(MivSearchKeys.Person.EMAIL_ADDRESS, new QueryConditionBuilder(" AND Email LIKE ?", 1));
        searches.put(MivSearchKeys.Person.MIV_ROLE, new QueryConditionBuilder(" AND RoleID=?", 1));
    }
}
