/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AnnotationDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.annotation;

import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;

/**
 * Handles retrieval of annotation records. Note that old publication
 * and presentation summary records will have a null section ID.
 *
 * @author Rick Hendricks
 * @since MIV 4.4
 */
public interface AnnotationDao
{
    /**
     * Get annotation by database unique ID and packet ID.
     *
     * @param id database unique ID
     * @param packetId
     * @return annotation
     */
    Annotation getAnnotationByPacket(int id, long packetId);

    /**
     * Get all annotations for the given user.
     *
     * @param userId MIV user ID
     * @param packetId
     * @return Map of Annotation values keyed to "<record ID>-<section base table>"
     */
    Map<String, Annotation> getAnnotations(int userId, long packetId);

    /**
     * Get annotation for the given user and record ID.
     *
     * @param userId MIV user ID
     * @param recordId record ID
     * @param packetId
     * @return annotation
     */
    Annotation getAnnotation(int userId, int recordId, long packetId);

    /**
     * Get annotation for the given user, record ID, and record type.
     *
     * @param userId MIV user ID
     * @param recordId record ID
     * @param packetId
     * @param recType record type
     * @return the matching Annotation or <code>null</code> if none.
     */
    Annotation getAnnotation(int userId, int recordId, long packetId, String recType);

    /**
     * Get annotations for the given user and record types.
     *
     * @param userId MIV user ID
     * @param packetId
     * @param recTypes record types
     * @return Map of Annotation values keyed to "<record ID>-<section base table>"
     */
    Map<String, Annotation> getAnnotations(int userId, long packetId, String...recTypes);

    /**
     * Get annotations for the given user and record types marked to be displayed.
     *
     * @param userId MIV user ID
     * @param packetId
     * @param recTypes record types
     * @return Map of Annotation values keyed to "<record ID>-<section base table>"
     */
    Map<String, Annotation> getAnnotationsForDisplay(int userId, long packetId,  String ... recTypes);

    /**
     * Get annotation legends for the given user and appearing in the given section.
     *
     * @param userId MIV user ID
     * @param sectionId section ID
     * @return legends
     */
    List<String> getAnnotationsLegend(String userId, String sectionId);

    /**
     * Get annotation legends for the given user,record IDs, and appearing in the given section.
     *
     * @param userId MIV user ID
     * @param sectionId section ID
     * @param recordIds record IDs
     * @return legends
     */
    List<String> getAnnotationsLegend(String userId, String sectionId, String ... recordIds);

    /**
     * Delete annotation line for the given database unique ID.
     *
     * @param id unique ID
     * @return if deleted
     */
    boolean deleteAnnotationLine(int id);


    /**
     * Add an annotation.
     *
     * @param Annotation
     * @return boolean true if successful
     */
    boolean insertAnnotation(final Annotation annotation, final int insertUserId);

    int updateAnnotation(final Annotation annotation, final int updateUserId);

    int deleteAnnotation(final Annotation annotation, final int ownerId);

    int deleteAnnotation(final int annotationId, final long packetId, final int ownerId);

    /**
     *
     * @param annotation
     * @param packetId
     * @param ownerId
     * @param actorId
     */
    void persistAnnotation(final Annotation annotation, final long packetId, final int ownerId, final int actorId);
}
