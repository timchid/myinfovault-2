/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AppointmentDao.java
 */


package edu.ucdavis.mw.myinfovault.dao.person;

import java.util.Collection;

import edu.ucdavis.mw.myinfovault.service.person.Appointment;

/**
 * Interface for reading/writing MIV person appointments to the database.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public interface AppointmentDao
{
    /**
     * Get appointment for the given record ID.
     * 
     * @param recId appointment record ID
     * @return MIV person appointment for the given record ID
     */
    public Appointment getAppointment(int recId);
    
    /**
     * Get all appointments for the given user.
     * 
     * @param userId MIV user ID
     * @return MIV person appointments for the given user ID
     */
    public Collection<Appointment> getAppointments(int userId);
    
    /**
     * Persist appointments to the database.
     * 
     * @param appointments person appointments to persist.
     */
    public void persistAppointments(Collection<Appointment> appointments);
    
    /**
     * Delete the given appointment.
     * 
     * @param appointment person appointment to delete
     */
    public void deleteAppointment(Appointment appointment);
}
