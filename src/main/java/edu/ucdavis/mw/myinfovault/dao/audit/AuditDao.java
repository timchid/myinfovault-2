/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuditDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.audit;

import java.util.List;

import edu.ucdavis.myinfovault.audit.AuditObject;

/**
 * Interface for Audittrail
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public interface AuditDao
{
    /**
     * to create audit trail
     * @param auditObject
     * //@//throws MivAuditTrailException
     */
    public void createAuditTrail(AuditObject auditObject); // throws MivAuditTrailException;

    /**
     * to get the audit trail by entityObjectId
     * @param entityObjectId
     * @return audit trail for the entity
     */
    public List<AuditObject> getAuditTrailByEntityObjectId(int entityObjectId);
}
