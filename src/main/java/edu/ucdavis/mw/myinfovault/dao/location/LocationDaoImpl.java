/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LocationDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.location;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.QueryTool;

/**
 * Implementation of LocationDao
 * @author pradeeph
 * @since MIV v4.4
 */
public class LocationDaoImpl implements LocationDao
{
    private QueryTool qt = MIVConfig.getConfig().getQueryTool();
//    private boolean debug = MIVConfig.getConfig().isDebug();
    private static final Logger logger = LoggerFactory.getLogger(LocationDaoImpl.class);

    private static final String ProvincesByCountryCode = "SELECT `Name` As ID, CONCAT(`Name`,' [',`CountryCode`,']') AS Value "+
                                " From `GeoState` "+
                                " Where `CountryCode` = ? ORDER BY `Name`";

    private static final String CitiesByCountryCode = "SELECT `Name` As ID, `Name` As Value"+
                                " From `GeoCity` "+
                                " Where `CountryCode` = ? ORDER BY `Name`";

    private static final String CitiesByProvinceAndCountry = "SELECT `GC`.`Name` As ID, `GC`.`Name` As Value, `GS`.`Name` As StateName "+
                                " From `GeoState` `GS` , `GeoCity` `GC` "+
                                " Where `GS`.`CountryCode` = `GC`.`CountryCode` AND `GS`.`StateID` = `GC`.`StateID` AND `GS`.`Name`=? AND `GS`.`CountryCode`=? "+
                                " ORDER BY `GC`.`Name`";


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#getProvincesByCountryCode(java.lang.String)
     */
    @Override
    public List<Map<String,String>> getProvincesByCountryCode(String countryCode)
    {
        return qt.getList(ProvincesByCountryCode, countryCode);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#getCitiesByCountryCode(java.lang.String)
     */
    @Override
    public List<Map<String,String>> getCitiesByCountryCode(String countryCode)
    {
        return qt.getList(CitiesByCountryCode, countryCode);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#getCitiesByProvinceAndCountry(java.lang.String, java.lang.String)
     */
    @Override
    public List<Map<String,String>> getCitiesByProvinceAndCountry(String province, String countryCode)
    {
        return qt.getList(CitiesByProvinceAndCountry, province, countryCode);
    }

    private static final String VALIDATE_COUNTRY = "SELECT COUNT(1) AS `Result` "+
                                                " From `GeoCountry` `GC` "+
                                                " Where `GC`.`CountryCode` = ?";

    private static final String VALIDATE_COUNTRY_STATE = "SELECT COUNT(1) AS `Result` "+
                                                        " From `GeoState` `GS` "+
                                                        " Where `GS`.`CountryCode` = ? AND ( `GS`.`Name`=? OR (`GS`.`StateID` = ? AND `GS`.`CountryCode` IN ( 'US' )) ) ";

    private static final String VALIDATE_COUNTRY_STATE_CITY = "SELECT COUNT(1) AS `Result` "+
                                                        " FROM `GeoCity` `GC` left join `GeoState` `GS` ON `GS`.`CountryCode` = `GC`.`CountryCode` AND `GS`.`StateID` = `GC`.`StateID` "+
                                                        " Where `GC`.`CountryCode`=? " +
                                                        " AND ( `GS`.`Name`=? OR (`GC`.`StateID` = ? AND `GC`.`CountryCode` IN ( 'US' )) ) " +
                                                        " AND `GC`.`Name`=?";

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#validateLocation(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public boolean validateLocation(String countryCode, String province, String city)
    {
        boolean hasCountry = (countryCode != null && countryCode.trim().length() > 0 && !countryCode.trim().equals("0"));
        boolean hasProvince = (province != null && province.trim().length() > 0 && !province.trim().equals("0"));
        boolean hasCity = (city != null && city.trim().length() > 0 && !city.trim().equals("0"));
        List<Map<String, String>> result = null;

        if (hasCountry && hasProvince && hasCity)
        {
            result = qt.getList(VALIDATE_COUNTRY_STATE_CITY, countryCode, province, province, city);
        }
        else if (hasCountry && hasProvince)
        {
            result = qt.getList(VALIDATE_COUNTRY_STATE, countryCode, province, province);
        }
        else
        {
            result = qt.getList(VALIDATE_COUNTRY, countryCode);
        }

        logger.debug("result :: {}", result);
        return (result != null && result.size() > 0 && result.get(0).get("result") != null && !result.get(0)
                .get("result").equals("0"));
    }

    /**
     * here 2 characters standard state abbreviations allows for US only.
     */
    private static final String SUGGEST_PROVINCES = " SELECT `Name` As ID, CONCAT( CASE WHEN `CountryCode` = 'US' THEN CONCAT(`StateID`,': ') ELSE '' END ,`Name`) AS Value "+
                                                    " From `GeoState` "+
                                                    " Where `CountryCode` = ? AND (`Name` like ? OR (`StateID` = ? AND `CountryCode` IN ( 'US' ) )) ORDER BY `Name`";

    private static final String SUGGEST_CITIES = " SELECT `GC`.`Name` As ID, `GC`.`Name` As Value, `GS`.`Name` As StateName "+
                                                 " FROM `GeoCity` `GC` left join `GeoState` `GS` ON `GS`.`CountryCode` = `GC`.`CountryCode` AND `GS`.`StateID` = `GC`.`StateID` "+
                                                 " WHERE `GC`.`CountryCode`=? AND `GC`.`Name` like ?  AND (`GS`.`Name`=? OR (`GS`.`StateID` = ? AND `GS`.`CountryCode` IN ( 'US' )) )  "+
                                                 " ORDER BY Value";

    //private static final String SUGGEST_CITIES_BY_COUNTRY_CITY = " SELECT `GC`.`Name` As ID, CONCAT(`GC`.`Name`,CASE WHEN `GS`.`Name` IS NOT NULL THEN CONCAT(' - ',`GS`.`Name`) ELSE '' END)As Value, `GS`.`Name` As StateName "+
    private static final String SUGGEST_CITIES_BY_COUNTRY_CITY = " SELECT `GC`.`Name` As ID, `GC`.`Name`As Value "+
                                                 " FROM `GeoCity` `GC` left join `GeoState` `GS` ON `GS`.`CountryCode` = `GC`.`CountryCode` AND `GS`.`StateID` = `GC`.`StateID` "+
                                                 " WHERE `GC`.`CountryCode`= ? AND `GC`.`Name` like ? "+
                                                 " GROUP BY `GC`.`Name` ORDER BY Value";

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#suggestProvinces(java.lang.String, java.lang.String)
     */
    @Override
    public List<Map<String, String>> suggestProvinces(String countryCode, String province)
    {
        countryCode = countryCode != null && countryCode.trim().length() > 0 ? countryCode : "";
        province = (province != null && province.trim().length() > 0 ? province.trim() : "");

        return qt.getList(SUGGEST_PROVINCES, countryCode, province+"%",province);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#suggestCities(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public List<Map<String, String>> suggestCities(String countryCode, String province, String city)
    {
        countryCode = countryCode != null && countryCode.trim().length() > 0 ? countryCode : "";
        province = province != null && province.trim().length() > 0 ? province : "";
        city = (city != null && city.trim().length() > 0 ? city.trim() : "") + "%";

        if(province == null || province.trim().length()==0)
        {
            return qt.getList(SUGGEST_CITIES_BY_COUNTRY_CITY, countryCode, city);
        }else
        {
            return qt.getList(SUGGEST_CITIES, countryCode, city, province, province);
        }
    }

    private static final String GET_STATE_NAME = "SELECT `Name` From `GeoState` Where `CountryCode` = ? AND (`Name` = ? OR (`StateID` = ? AND `CountryCode` IN ( 'US' )))";
    private static final String GET_CITY_NAME  = "SELECT `Name` From `GeoCity`  Where `CountryCode` = ? AND `Name` = ? ";


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#getValidStateByNameAndCountryCode(java.lang.String, java.lang.String)
     */
    @Override
    public String getValidStateByNameAndCountryCode(String stateName, String countryCode)
    {
        return executeScript(GET_STATE_NAME,"name",countryCode, stateName, stateName);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.location.LocationDao#getValidCityByNameAndCountryCode(java.lang.String, java.lang.String)
     */
    @Override
    public String getValidCityByNameAndCountryCode(String cityName, String countryCode)
    {
        return executeScript(GET_CITY_NAME, "name", countryCode, cityName);
    }


    /**
     * To execute a query and get the value of given columnName
     * @param query
     * @param columnName
     * @param args
     * @return Result string
     */
    private String executeScript(String query, String columnName, String... args)
    {
        String resultData = null;
        List<Map<String, String>> result = qt.getList(query, args);

        if (result != null && result.size() > 0)
        {
            resultData = result.get(0).get(columnName.trim().toLowerCase());
        }

        return resultData != null && resultData.trim().length() > 0 ? resultData : null;
    }

}
