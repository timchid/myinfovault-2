/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserAccountDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.service.person.MivDisplayPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.builder.MivAccountDto;
import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.SimpleAttributeSet;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.QueryConditionBuilder;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;
import edu.ucdavis.myinfovault.message.MivConstants;

/**
 * Retrieve data from the MIV Account table.
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class UserAccountDao extends DaoSupport
{
    /** Partial SQL statement which will be appended with conditions in {@link #findUsers(AttributeSet) findUsers} */
    private static final String FIND_USERS_PARTIAL_SQL =
        "SELECT * FROM UserAccount WHERE 1=1";

    private final String SELECT_BY_USERID_SQL;
    private final String SELECT_BY_PERSONID_SQL;
    private final String INSERT_ACCOUNT_SQL;
    private final String UPDATE_ACCOUNT_SQL;
    private final String UPDATE_ACTIVATION_SQL;
    private final String SELECT_ACTIVE_FOR_ROLE;
    private final String SELECT_ACTIVE_CANDIDATES;

    private static final Map<String, Map<String, String>> schoolMap = MIVConfig.getConfig().getMap("schools");
    private static final Map<String, Map<String, String>> departmentMap = MIVConfig.getConfig().getMap("departments");

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Create the user account DAO bean, initializing SQL statements.
     */
    public UserAccountDao()
    {
        SELECT_BY_USERID_SQL = getSQL("useraccount.select.userid");
        SELECT_BY_PERSONID_SQL = getSQL("useraccount.select.personid");
        INSERT_ACCOUNT_SQL = getSQL("useraccount.insert");
        UPDATE_ACCOUNT_SQL = getSQL("useraccount.update");
        UPDATE_ACTIVATION_SQL = getSQL("useraccount.updateactivation");
        SELECT_ACTIVE_FOR_ROLE = getSQL("useraccount.select.inrole");
        SELECT_ACTIVE_CANDIDATES = getSQL("useraccount.select.activecandidates");
    }


    public MivAccountDto addAccount(final int actorId, final MivAccountDto dto)
    {
        boolean inserted = false;

        final KeyHolder keyHolder = new GeneratedKeyHolder();

        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException
            {
                final String login = dto.getLogin();
                final String personUuid = dto.getPersonUuid();

                final int schoolId = dto.getSchoolId();
                final int deptId = dto.getDepartmentId();

                final String surname = dto.getSurname();
                final String givenName = dto.getGivenName();
                final String middleName = dto.getMiddleName();
                final String preferredName = dto.getPreferredName();
                final String email = dto.getEmail();

                final boolean isActive = true; // inserting a new user, they're always active

                final PreparedStatement ps = connection.prepareStatement(INSERT_ACCOUNT_SQL, new String[] { "UserID" });

                ps.setString(1, login);
                ps.setString(2, personUuid);

                ps.setInt(3, schoolId);
                ps.setInt(4, deptId);

                ps.setString(5, surname);
                ps.setString(6, givenName);
                ps.setString(7, middleName != null ? middleName : "");
                ps.setString(8, preferredName);

                ps.setString(9, email);
                ps.setBoolean(10, isActive);

                ps.setInt(11, actorId);

                return ps;
            }

        }, keyHolder);

        final int recId = keyHolder.getKey().intValue();
        inserted = (recId > 0);

        if (inserted)
        {
            dto.setUserId(recId);
            dto.setActive(true);
            // Add the record for the new user's master data timestamp
            this.getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException
                {
                    final PreparedStatement ps = connection.prepareStatement( MivConstants.getInstance().getString("UPDATE_MASTER_DATA_TIME") );
                    ps.setInt(1, dto.getUserId());
                    return ps;
                }
            });
        }

        return dto;
    }


    public MivAccountDto updateAccount(final int actorId, final MivAccountDto dto)
    {
        final Object[] args = {
                dto.getLogin(),
                dto.getPersonUuid(),
                dto.getSchoolId(),
                dto.getDepartmentId(),
                dto.getSurname(),
                dto.getGivenName(),
                dto.getMiddleName() != null ? dto.getMiddleName() : "",
                dto.getPreferredName(),
                dto.getEmail(),
                dto.isActive(),
                actorId,
                dto.getUserId()
        };

        final int count = this.getJdbcTemplate().update(UPDATE_ACCOUNT_SQL, args);

        if (count != 1) // Exactly one record should have been updated
        {
            if (count > 1) // Too many! Very bad news
            {
                final String msg = "Too many records affected when updating an account!";
                logger.error(msg);
                logger.error("Account [{}] records updated: {} Args were: |{}|", new Object[] { dto, count, args });
                throw new MivSevereApplicationError(msg);
            }
            else
            // no records updated
            {
                logger.error("Failed to update user [{}] Args were: |{}|", dto, args);
            }
        }


        return dto;
    }


    public void updateActivation(final int actorId, final int userId, final boolean isActive)
    {
        final int count = this.getJdbcTemplate().update(UPDATE_ACTIVATION_SQL, new PreparedStatementSetter() {
            @Override
            public void setValues(final PreparedStatement ps) throws SQLException
            {
                ps.setBoolean(1, isActive);
                ps.setInt(2, actorId);
                ps.setInt(3, userId);
            }
        });

        logger.trace("updateActivation for user {} returned update count: {}", userId, count);
    }


    public MivAccountDto getAccountByUserId(final int userId)
    {
        return this.getAccountByUserId(Integer.toString(userId));
    }
    public MivAccountDto getAccountByUserId(final String userId)
    {
        final Object[] params = { userId };
        MivAccountDto dto;

        try {
            dto = this.getJdbcTemplate().queryForObject(SELECT_BY_USERID_SQL, params, accountMapper);
        }
        catch (final EmptyResultDataAccessException e) {
            logger.info("Failed to load person given a userId of {}\n   Query was [{}]", userId, SELECT_BY_USERID_SQL);
            return null;
        }

        return dto;
    }


    public MivAccountDto getAccountByPersonId(final String personId)
    {
        final Object[] params = { personId };
        final MivAccountDto dto;

        try {
            dto = this.getJdbcTemplate().queryForObject(SELECT_BY_PERSONID_SQL, params, accountMapper);
        }
        catch (final EmptyResultDataAccessException e) {
            logger.info("Failed to load person given a userId of {}\n   Query was [{}]", personId, SELECT_BY_PERSONID_SQL);
            return null;
        }

        return dto;
    }


    public List<MivAccountDto> findUsers(final AttributeSet criteria)
    {
        final StringBuilder sql = new StringBuilder(FIND_USERS_PARTIAL_SQL);
        final List<String> args = new ArrayList<String>();

        for (final String key : criteria.keySet())
        {
            final QueryConditionBuilder c = searches.get(key);
            if (c != null)
            {
                sql.append(c.getSql());
                c.addArgs(args, criteria.get(key).replace('*', '%'));
            }
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("SQL Statement: [{}]", sql);
            logger.debug("   Args: {}", args);
        }

        return get(sql.toString(), accountMapper, args.toArray());
    }


//// select * from UserAccount where userid in (select userid from RoleAssignment where roleid=6) and active=true
//    private static final String FIND_ACTIVE_FOR_ROLE =
//        "SELECT * FROM UserAccount" +
//        " WHERE UserID IN (SELECT UserID FROM RoleAssignment WHERE RoleID=?)" +
//        " AND Active=true" +
//        "";
    public List<MivAccountDto> findActiveAccountsForRole(MivRole role)
    {
        return get(SELECT_ACTIVE_FOR_ROLE, accountMapper, role.getRoleId());
    }

    public List<MivDisplayPerson> findActiveCandidates()
    {
        final List<MivDisplayPerson> results = this.getJdbcTemplate().query(SELECT_ACTIVE_CANDIDATES, (Object[]) null, displayPersonMapper);
        return results;
    }


    /** Map a UserAccount table query result to an Account DTO */
    private final RowMapper<MivAccountDto> accountMapper = new RowMapper<MivAccountDto>() {
        @Override
        public MivAccountDto mapRow(final ResultSet rs, final int rowNum) throws SQLException
        {
            final MivAccountDto person = new MivAccountDto();

            person.setUserId(rs.getInt("UserID"));
            person.setLogin(rs.getString("Login").toLowerCase());
            person.setPersonUuid(rs.getString("PersonUUID"));

            person.setSchoolId(rs.getInt("SchoolID"));
            person.setDepartmentId(rs.getInt("DepartmentID"));

            String preferredName = rs.getString("PreferredName");
            final String givenName = rs.getString("GivenName");
            final String middleName = rs.getString("MiddleName");
            final String surName = rs.getString("Surname");
            if (!StringUtils.hasText(preferredName))
            {
                preferredName = givenName + ' ' + middleName + ' ' + surName;
                preferredName = preferredName.replaceAll(" +", " ");
            }

            person.setPreferredName(preferredName);
            person.setSurname(surName);
            person.setGivenName(givenName);
            person.setMiddleName(middleName);

            person.setEmail(rs.getString("Email"));
            person.setActive(rs.getBoolean("Active"));

            return person;
        }
    };



    private static Map<String, QueryConditionBuilder> searches = new HashMap<String, QueryConditionBuilder>();
    static
    {
        searches.put(MivSearchKeys.Person.MIV_ACTIVE_USER, new QueryConditionBuilder(" AND Active=true", 0));
        searches.put(MivSearchKeys.Person.PERSON_UUID, new QueryConditionBuilder(" AND PersonUUID=?", 1));
        searches.put(MivSearchKeys.Person.MIV_SCHOOL_CODE, new QueryConditionBuilder(" AND SchoolID=?", 1));
        searches.put(MivSearchKeys.Person.MIV_DEPARTMENT_CODE, new QueryConditionBuilder(" AND DepartmentID=?", 1));
        searches.put(MivSearchKeys.Person.NAME, new QueryConditionBuilder(
                //" AND (PreferredName LIKE ? OR Surname LIKE ? OR GivenName LIKE ? OR MiddleName LIKE ?)", 4));
                " AND ( PreferredName LIKE ? OR getUserSortNameByUserID(UserID) LIKE CONVERT(? USING utf8) )", 2));

        searches.put(MivSearchKeys.Person.DISPLAY_NAME, new QueryConditionBuilder(" AND PreferredName LIKE ?", 1));
        searches.put(MivSearchKeys.Person.FIRST_NAME, new QueryConditionBuilder(" AND GivenName LIKE ?", 1));
        searches.put(MivSearchKeys.Person.MIDDLE_NAME, new QueryConditionBuilder(" AND MiddleName LIKE ?", 1));
        searches.put(MivSearchKeys.Person.LAST_NAME, new QueryConditionBuilder(" AND Surname LIKE ?", 1));
        searches.put(MivSearchKeys.Person.EMAIL_ADDRESS, new QueryConditionBuilder(" AND Email LIKE ?", 1));
// This next needs to be changed to "WHERE RoleID IN (?,?,?...?)" but the number of "?" placeholders
// must be determined dynamically as "1 + # of commas"
// That can't be done with the simple "searches.get(s)" lookup in findUsers()
        searches.put(MivSearchKeys.Person.MIV_ROLE, new QueryConditionBuilder(
                " AND UserID in (SELECT UserID FROM RoleAssignment WHERE RoleID=?)", 1));
    }

    /** Map a UserAccount table query result to an Account DTO */
    private final RowMapper<MivDisplayPerson> displayPersonMapper = new RowMapper<MivDisplayPerson>() {
        @Override
        public MivDisplayPerson mapRow(final ResultSet rs, final int rowNum) throws SQLException
        {

            Scope wildcard = Scope.AnyScope;
            String scopeString = rs.getString("Scope");
            Scope scope = wildcard; // start scope as a wildcard in case nothing is specified
            if (StringUtils.hasText(scopeString))
            {
                String[] attrs = scopeString.split("[ ]*[,;][ ]*");
                if (attrs.length < 2)
                {
                    String[] attributes = { attrs[0],MivSearchKeys.Person.MIV_DEPARTMENT_CODE + "=" + Scope.ANY };
                    scope = new Scope(new SimpleAttributeSet(attributes));
                }
                else
                {
                    scope = new Scope(new SimpleAttributeSet(attrs));
                }
            }
            Map<String, String>schoolNameMap = schoolMap.get(scope.getSchool()+"");
            Map<String, String>deptNameMap = departmentMap.get(scope.getSchool()+":"+scope.getDepartment());
            String school = schoolNameMap != null ? schoolNameMap.get("description") : "";
            String department = deptNameMap != null ? deptNameMap.get("description") : "";

            return new MivDisplayPerson(rs.getInt("UserId"), rs.getString("SortName"), rs.getString("description"), school, department);
        }
    };


}
