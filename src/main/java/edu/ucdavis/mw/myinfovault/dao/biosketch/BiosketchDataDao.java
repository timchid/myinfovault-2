/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchDataDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchExclude;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSectionLimits;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Section;

/**
 * TODO: missing javadoc
 * 
 * @author dreddy
 * @since MIV 2.1
 */
public interface BiosketchDataDao
{
    /**
     * Returns the list of biosketch excludes associated with a particular biosketch for a user.
     * 
     * @param biosketchId
     * @param userId
     * @return biosketch excludes for the given biosketch and user
     * @throws SQLException
     */
    public List<BiosketchExclude> findBiosketchExcludeListByBiosketchIdAndUserId(int biosketchId, int userId) throws SQLException;

    /**
     * Returns the list of biosketch section limits for a particular biosketch type.
     * 
     * @param biosketchType
     * @return
     * @throws SQLException
     */
    public List<BiosketchSectionLimits> findBiosketchSectionLimitsByBiosketchType(int biosketchType) throws SQLException;

    /**
     * Returns a list of all the sections.
     * 
     * @return
     * @throws SQLException
     */
    public List<Section> findSection() throws SQLException;

    /**
     * Returns a list of biosketch sections associated with a particular biosketch ID.
     * 
     * @param biosketchId
     * @return
     * @throws SQLException
     */
    public List<BiosketchSection> findBiosketchSectionsByBiosketchId(int biosketchId) throws SQLException;

    /**
     * inserts the biosketch section associated with a biosketch.
     * 
     * @param biosketchSection
     * @param userID
     * @throws SQLException
     */
    public void insertBiosketchSection(BiosketchSection biosketchSection, int userID) throws SQLException;

    /**
     * Updates the biosketch section.
     * 
     * @param biosketchSection
     * @param userID
     * @throws SQLException
     */
    public void updateBiosketchSection(BiosketchSection biosketchSection, int userID) throws SQLException;

    /**
     * Deletes all the biosketch section records associated with a biosketch.
     * 
     * @param biosketchID
     * @throws SQLException
     */
    public void deleteBiosketchSection(int biosketchID) throws SQLException;

    /**
     * Insert the exclude list associated with biosketch data.
     * 
     * @param excludeList
     * @param userID
     * @throws SQLException
     */
    public void insertBiosketchExcludeBatch(List<BiosketchExclude> excludeList, int userID) throws SQLException;

    /**
     * Delete the exclude list associated with biosketch data.
     * 
     * @param excludeList
     * @throws SQLException
     */
    public void deleteBiosketchExcludeBatch(List<BiosketchExclude> excludeList) throws SQLException;

    /**
     * Deletes all the biosketch exclude records associated with a biosketch.
     * 
     * @param biosketchID
     * @throws SQLException
     */
    public void deleteBiosketchExclude(int biosketchID) throws SQLException;
}
