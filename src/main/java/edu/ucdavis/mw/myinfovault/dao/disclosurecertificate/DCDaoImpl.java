/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.disclosurecertificate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.Material;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.Rank;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handles disclosure certificate persistence.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class DCDaoImpl extends DaoSupport implements DCDao
{
    private final String selectDCFormByIdSql;
    private final String selectDCFormsByScopeSql;
    private final String selectDCFormLatestSql;
    private final String selectDCFormLatestWithSignatureSql;
    private final String selectDCFormWithSignatureSql;
    private final String selectDCFormLatestRequestedSql;
    private final String selectDCFormLatestSignedSql;
    private final String selectDCFormPreviouslySignedSql;
    private final String selectDCFormLatestTwoSql;
    private final String selectDCMaterialsSql;
    private final String selectDCRanksSql;
    private final String insertDCFormsSql;
    private final String updateDCFormsSql;
    private final String insertDCRanksSql;
    private final String deleteDCFormsSql;
    private final String deleteDCRanksSql;
    private final String markArchivedDCFormSql;
    private final String markArchivedSignedDCFormsSql;

    /**
     * Creates the DC DAO object and initializes it with SQL
     * for handling the persistence of {@link DCBo} objects.
     */
    public DCDaoImpl()
    {
        selectDCFormByIdSql = getSQL("dc.select.id");
        selectDCFormsByScopeSql = getSQL("dc.select.scope");
        selectDCFormLatestSql = getSQL("dc.select.latest");
        selectDCFormLatestWithSignatureSql = getSQL("dc.select.latestwithsignature");
        selectDCFormWithSignatureSql = getSQL("dc.select.withsignature");
        selectDCFormLatestRequestedSql = getSQL("dc.select.requested");
        selectDCFormLatestSignedSql = getSQL("dc.select.latestsigned");
        selectDCFormPreviouslySignedSql = getSQL("dc.select.previouslysigned");
        selectDCFormLatestTwoSql = getSQL("dc.select.latesttwo");
        selectDCMaterialsSql = getSQL("dc.materials.select");
        selectDCRanksSql = getSQL("dc.ranks.select");
        insertDCFormsSql = getSQL("dc.insert");
        updateDCFormsSql = getSQL("dc.update");
        insertDCRanksSql = getSQL("dc.ranks.insert");
        deleteDCFormsSql = getSQL("dc.delete");
        deleteDCRanksSql = getSQL("dc.ranks.delete");
        markArchivedDCFormSql = getSQL("dc.update.archive");
        markArchivedSignedDCFormsSql = getSQL("dc.update.archivesigned");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getLatestDC(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getLatestDC(Dossier dossier, int schoolId, int departmentId)
    {
        return getFirst(getDCs(dossier, selectDCFormLatestSql, dossier.getDossierId(), schoolId, departmentId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getLatestRequestedDC(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getLatestWithSignatureDC(Dossier dossier, int schoolId, int departmentId)
    {
        return getFirst(getDCs(dossier, selectDCFormLatestWithSignatureSql, dossier.getDossierId(), schoolId, departmentId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getLatestRequestedDC(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public List<DCBo> getAllWithSignatureDC(Dossier dossier, int schoolId, int departmentId)
    {
        return getDCs(dossier, selectDCFormWithSignatureSql, dossier.getDossierId(), schoolId, departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getLatestRequestedDC(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getLatestRequestedDC(Dossier dossier, int schoolId, int departmentId)
    {
        return getFirst(getDCs(dossier, selectDCFormLatestRequestedSql, dossier.getDossierId(), schoolId, departmentId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getLatestSignedDC(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getLatestSignedDC(Dossier dossier, int schoolId, int departmentId)
    {
        return getFirst(getDCs(dossier, selectDCFormLatestSignedSql, dossier.getDossierId(), schoolId, departmentId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getPreviouslySignedDC(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getPreviouslySignedDC(Dossier dossier, int schoolId, int departmentId)
    {
        return getFirst(getDCs(dossier, selectDCFormPreviouslySignedSql, dossier.getDossierId(), schoolId, departmentId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getDC(int)
     */
    @Override
    public DCBo getDC(int recordId)
    {
        return getFirst(getDCs(null, selectDCFormByIdSql, recordId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getLatestTwoDCs(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public List<DCBo> getLatestTwoDCs(Dossier dossier, int schoolId, int departmentId)
    {
        return getDCs(dossier, selectDCFormLatestTwoSql, dossier.getDossierId(), schoolId, departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#getDCs(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public List<DCBo> getDCs(Dossier dossier, int schoolId, int departmentId)
    {
        return getDCs(dossier, selectDCFormsByScopeSql, dossier.getDossierId(), schoolId, departmentId);
    }

    /**
     * Query for list of disclosure certificate business objects.
     *
     * @param dossier dossier associated with DCs
     * @param sql SQL string to use
     * @param params Parameters used in the SQL statement
     * @return list of disclosure certificate business objects
     */
    private List<DCBo> getDCs(final Dossier dossier, String sql, Object...params)
    {
        try
        {
            return get(sql, new RowMapper<DCBo>()
            {
                @Override
                public DCBo mapRow(ResultSet rs, int rowNum) throws SQLException
                {
                    try
                    {
                        /*
                         * Use the given dossier if not null,
                         * otherwise get new from dossier service.
                         */
                        Dossier dcDossier = dossier != null
                                          ? dossier
                                          : MivServiceLocator.getDossierService().getDossier(rs.getLong("DossierID"));

                        return new DCBo(rs.getInt("ID"),
                                        dcDossier,
                                        rs.getInt("SchoolID"),
                                        rs.getInt("DepartmentID"),
                                        rs.getString("CandidateName"),
                                        rs.getString("SchoolName"),
                                        rs.getString("DepartmentName"),
                                        rs.getBoolean("Revised"),
                                        rs.getDate("EnteredDate"),
                                        rs.getBoolean("ActionAppraisal"),
                                        rs.getBoolean("ActionAcceleration"),
                                        rs.getBoolean("ActionDeferral"),
                                        rs.getBoolean("ActionOther"),
                                        rs.getString("AdditionalInfo"),
                                        rs.getString("ChangesAdditions"),
                                        getMaterials(rs.getInt("ID")),
                                        getRanks(rs.getInt("ID")),
                                        rs.getBoolean("Archived"));
                    }
                    catch (WorkflowException exception)
                    {
                        throw new MivSevereApplicationError("errors.dc.dossier.read", exception);
                    }
                }
            }, params);
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.dc.dao.read", exception, params);
        }
    }

    /**
     * Get list of materials for the DC form ID.
     *
     * @param formId disclosure certificate form ID
     * @return list of associated materials
     * @throws DataAccessException if unable to retrieve materials from database
     */
    @SuppressWarnings("deprecation")
    private List<Material> getMaterials(int formId) throws DataAccessException
    {
        return get(selectDCMaterialsSql, materialsMapper, formId);
    }

    /**
     * Creates and maps 'DC_Materials' table records to {@link Material} objects.
     */
    @SuppressWarnings("deprecation")
    private RowMapper<Material> materialsMapper = new RowMapper<Material>()
    {
        @Override
        public Material mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new Material(rs.getString("Material"),
                                rs.getBoolean("Checked"),
                                rs.getInt("InsertUserID"),
                                rs.getTimestamp("InsertTimestamp"));
        }
    };

    /**
     * Get list of ranks for the DC form ID.
     *
     * @param formId disclosure certificate form ID
     * @return list of associated ranks
     * @throws DataAccessException if unable to retrieve materials from database
     */
    private List<Rank> getRanks(int formId) throws DataAccessException
    {
        return get(selectDCRanksSql, ranksMapper, formId);
    }

    /**
     * Creates and maps 'DC_Ranks' table records to {@link Rank} objects.
     */
    private RowMapper<Rank> ranksMapper = new RowMapper<Rank>() {
        @Override
        public Rank mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new Rank(rs.getString("PresentRankAndStep"),
                            rs.getString("PresentPercentOfTime"),
                            rs.getString("ProposedRankAndStep"),
                            rs.getString("ProposedPercentOfTime"));
        }
    };

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#persistDC(edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo, int)
     */
    @Override
    public boolean persistDC(final DCBo dcBo, final int realUserId)
    {
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        // should this be an update?
        final boolean update = dcBo.getDocumentId() > 0;

        try
        {
            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(update ? updateDCFormsSql : insertDCFormsSql,
                                                                       Statement.RETURN_GENERATED_KEYS);

                    ps.setBoolean(1, dcBo.isRevised());
                    ps.setString(2, dcBo.getAdditionalInfo());
                    ps.setString(3, dcBo.getChangesAdditions());
                    ps.setInt(4, realUserId);

                    if (update)
                    {
                        ps.setInt(5, dcBo.getDocumentId());
                    }
                    else// insert
                    {
                        ps.setInt(5, realUserId);

                        ps.setLong(6, dcBo.getDossier().getDossierId());
                        ps.setInt(7, dcBo.getSchoolId());
                        ps.setInt(8, dcBo.getDepartmentId());
                    }

                    return ps;
                }}, keyHolder);

            if (rowsAffected > 0)
            {
                //update the DC business object with the inserted record ID
                if (!update)
                {
                    dcBo.setDocumentId(keyHolder.getKey().intValue());
                }

                //persist ranks
                persistRanks(dcBo, realUserId, update);
            }
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.dc.dao.write", exception, dcBo);
        }

        return true;
    }

    /**
     * Persist DC ranks.
     *
     * @param dc disclosure certificate with ranks to persist
     * @param realUserId ID of user effecting changes
     * @param update if this is an update of an existing DC
     * @throws DataAccessException if unable to write to the database
     */
    private void persistRanks(final DCBo dc, final int realUserId, final boolean update) throws DataAccessException
    {
        //delete current ranks in favor of new if updating
    	if (update)
        {
            update(deleteDCRanksSql, dc.getDocumentId());
        }

    	for(final Rank rs : dc.getRanks())
    	{
    		getJdbcTemplate().update(new PreparedStatementCreator()
            {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(insertDCRanksSql);

                    ps.setInt(1, dc.getDocumentId());
                    ps.setString(2, rs.getPresentRankAndStep());
                    ps.setString(3, rs.getPresentPercentOfTime());
                    ps.setString(4, rs.getProposedRankAndStep());
                    ps.setString(5, rs.getProposedPercentOfTime());
                    ps.setInt(6, realUserId);
                    ps.setInt(7, realUserId);

                    return ps;
                }
            });
    	}
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#deleteDC(int)
     */
    @Override
    public void deleteDC(int recordId)
    {
        try
        {
            update(deleteDCFormsSql, recordId);
        }
        catch(DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.dc.dao.delete", exception, recordId);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#archiveDC(int)
     */
    @Override
    public void archiveDC(int recordId)
    {
        try
        {
            update(markArchivedDCFormSql, recordId);
        }
        catch(DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.dc.dao.archive", exception, recordId);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao#archiveSignedDC(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public void archiveSignedDC(Dossier dossier, int schoolId, int departmentId)
    {
        try
        {
            update(markArchivedSignedDCFormsSql, dossier.getDossierId(), schoolId, departmentId);
        }
        catch(DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.dc.dao.archivesigned",
                                                exception,
                                                dossier.getDossierId(),
                                                schoolId,
                                                departmentId);
        }
    }
}
