/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RoleAssignmentDaoSupport.java
 */

package edu.ucdavis.mw.myinfovault.dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.dao.person.RoleAssignmentDao;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.util.SimpleAttributeSet;

/**
 * JdbcSupport DAO for the AssignedRole object. Sub-classing
 * this requires defining the private final SQL strings.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public abstract class RoleAssignmentDaoSupport extends DaoSupport implements RoleAssignmentDao
{
    private final String DELETE_A_ROLE_SQL;
    private final String DELETE_ROLES_SQL;
    private final String INSERT_ROLES_SQL;
    private final String SELECT_ROLES_SQL;
    private final String SELECT_A_ROLE_SQL;

    /**
     * Creates the role assignment DAO support object and initializes it with SQL that
     * corresponds to the given property keys in the default SQL properties file. Sub-
     * classing this allows handling of the persistence of the {@link AssignedRole} object.
     *
     * @param deleteRoleById Delete an assigned role for a given record ID
     * @param deleteRolesByForeignKey Delete assigned roles for a given foreign key ID
     * @param insertRoles Insert a collection of assigned roles
     * @param selectRoleById Select an assigned role by record ID
     * @param selectRolesByForeignId Select assigned roles for a given foreign key ID
     * @throws IOException If unable to access the default SQL properties file
     */
    protected RoleAssignmentDaoSupport(String deleteRoleById,
                                       String deleteRolesByForeignKey,
                                       String insertRoles,
                                       String selectRoleById,
                                       String selectRolesByForeignId) throws IOException
    {
        DELETE_A_ROLE_SQL = getSQL(deleteRoleById);
        DELETE_ROLES_SQL = getSQL(deleteRolesByForeignKey);
        INSERT_ROLES_SQL = getSQL(insertRoles);
        SELECT_A_ROLE_SQL = getSQL(selectRoleById);
        SELECT_ROLES_SQL = getSQL(selectRolesByForeignId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAssignedRole(int recId)
    throws DataAccessException
    {
        // Delete the assigned role
        final Object[] params = {recId};
        final int[] paramTypes = {Types.INTEGER};
        this.getJdbcTemplate().update(DELETE_A_ROLE_SQL, params, paramTypes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAssignedRoles(int foreignId)
    throws DataAccessException
    {
        // Delete the assigned role
        final Object[] params = {foreignId};
        final int[] paramTypes = {Types.INTEGER};
        this.getJdbcTemplate().update(DELETE_ROLES_SQL, params, paramTypes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AssignedRole getAssignedRole(int recId)
    throws DataAccessException
    {
        List<AssignedRole>assignedRoles =
            get(SELECT_A_ROLE_SQL, assignedRoleMapper, recId);
        if (assignedRoles == null || assignedRoles.isEmpty())
        {
            return null;
        }
        return assignedRoles.get(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<AssignedRole> getAssignedRoles(int foreignId)
    throws DataAccessException
    {
        return get(SELECT_ROLES_SQL, assignedRoleMapper, foreignId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertAssignedRoles(final int foreignId, Collection<AssignedRole> assignedRoles, final int actingUserId)
    throws DataAccessException
    {
        final int batchSize = assignedRoles.size();
        final List<Integer> roleIds = new ArrayList<Integer>(batchSize);
        final List<Boolean> primaryRoles = new ArrayList<Boolean>(batchSize);
        final List<String> scopes = new ArrayList<String>(batchSize);

        for (AssignedRole assignedRole : assignedRoles)
        {
            roleIds.add(assignedRole.getRole().getRoleId());
            primaryRoles.add(assignedRole.isPrimary());
            // Set up the scope - save a 'null' if it's "ANY" scope.
            Scope scope = assignedRole.getScope();
            if (scope.equals(Scope.AnyScope)) {
                scopes.add(null);
            }
            else if (scope.getDepartment() == Scope.ANY) {
                scopes.add(MivSearchKeys.Person.MIV_SCHOOL_CODE+"="+scope.getSchool());
            }
            else {
                scopes.add(scope.getAttributes());
            }
        }

        this.getJdbcTemplate().batchUpdate(INSERT_ROLES_SQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException
            {
                ps.setInt(1, foreignId);
                ps.setInt(2, roleIds.get(i));
                ps.setString(3, scopes.get(i));
                ps.setBoolean(4, primaryRoles.get(i));
                ps.setInt(5, actingUserId);
            }

            @Override
            public int getBatchSize()
            {
                return batchSize;
            }
        });
    }

    /**
     * RoleAssignmentRowMapper - map RoleAssignment records to AssignedRole object
     * @author rhendric
     */
    private RowMapper<AssignedRole> assignedRoleMapper = new RowMapper<AssignedRole>()
    {
        @Override
        public AssignedRole mapRow(ResultSet rs, int rowNum) throws SQLException
        {

            String mivCode = rs.getString("MivCode");
            Scope wildcard = Scope.AnyScope;
            String scopeString = rs.getString("Scope");
            Scope scope = wildcard; // start scope as a wildcard in case nothing is specified
            if (StringUtils.hasText(scopeString)) {
                String[] attrs = scopeString.split("[ ]*[,;][ ]*");
                if (attrs.length < 2)
                {
                    String[] attributes = {attrs[0],MivSearchKeys.Person.MIV_DEPARTMENT_CODE+"="+Scope.ANY};
                    scope = new Scope(new SimpleAttributeSet(attributes));
                }
                else
                {
                    scope = new Scope(new SimpleAttributeSet(attrs));
                }
            }
            AssignedRole assignedRole = new AssignedRole(MivRole.valueOf(mivCode), scope, rs.getBoolean("PrimaryRole"));
            assignedRole.setRecId(rs.getInt("ID"));
            return assignedRole;

        }
    };
}
