/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionSignatureEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * TODO: Add Javadoc
 *
 * @author Stephen Paulsen
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class DecisionSignatureEvent extends DossierEventStub implements SignatureEvent, DecisionEvent, DossierEvent
{
    private static final long serialVersionUID = 201307311025L;

    private final MivElectronicSignature signature;

    /**
     * TODO: add Javadoc
     *
     * @param realPerson real event initiator
     * @param signature
     */
    public DecisionSignatureEvent(MivPerson realPerson, MivElectronicSignature signature)
    {
        super(realPerson, signature.getDocument().getDossier());

        this.signature = signature;
    }

    /**
     * TODO: add Javadoc
     *
     * @param parent event that caused this event
     * @param realPerson real event initiator
     * @param signature
     */
    public DecisionSignatureEvent(MivEvent parent, MivPerson realPerson, MivElectronicSignature signature)
    {
        super(parent, realPerson, signature.getDocument().getDossier());

        this.signature = signature;
    }

    /**
     * TODO: add Javadoc
     *
     * @param parent event that caused this event
     * @param realPerson real event initiator
     * @param decision
     */
    public DecisionSignatureEvent(MivEvent parent, MivPerson realPerson, Decision decision)
    {
        super(parent, realPerson, decision.getDossier());

        this.signature = MivServiceLocator.getSigningService().getSignedSignatureByDocument(decision);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.SignatureEvent#getSignature()
     */
    @Override
    public MivElectronicSignature getSignature()
    {
        return this.signature;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DecisionEvent#getDecision()
     */
    @Override
    public Decision getDecision()
    {
        return signature.getDocument();
    }

    @Override
    public String getEventTitle()
    {
        return "Decision Signature";
    }

}
