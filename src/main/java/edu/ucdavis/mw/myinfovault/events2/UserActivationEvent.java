/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserActivationEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Event to indicate that an activation event (activate/deactivate) has occurred for a user.
 * @author stephenp
 *
 */
public class UserActivationEvent extends UserEventStub
{

    private static final long serialVersionUID = 201412031146L;

    boolean activating;
    /**
     * @param realPerson
     * @param targetPerson
     * @param activating
     */
    public UserActivationEvent(MivPerson realPerson, MivPerson targetPerson, boolean activating)
    {
        this(null, realPerson, targetPerson, activating);
    }

    /**
     * @param parentEvent
     * @param realPerson
     * @param targetPerson
     * @param activating
     */
    public UserActivationEvent(MivEvent parentEvent, MivPerson realPerson, MivPerson targetPerson, boolean activating)
    {
        super(parentEvent, realPerson, targetPerson);
        this.activating = activating;
    }
    public boolean isActivating()
    {
        return this.activating;
    }

    public boolean isDeactivating()
    {
        return ! isActivating();
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return this.activating ? "Activate User" : "Deactivate User";

    }
}
