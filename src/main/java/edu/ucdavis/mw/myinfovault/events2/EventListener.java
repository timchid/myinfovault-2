/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventListener.java
 */

package edu.ucdavis.mw.myinfovault.events2;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.event.EventLogService;
import edu.ucdavis.mw.myinfovault.util.MivThreadPoolExecutor;
import edu.ucdavis.myinfovault.ConfigReloadConstants;

/**<p>
 * This is the central MIV event listener.
 * @author rhendric
 *
 */
public class EventListener
{
    private static final Logger logger = LoggerFactory.getLogger(EventListener.class);
    private static EventLogService eventLogService = null;

    
    // Get an instance of the MivThreadPoolExecutor
    // MivThreadPoolExecutor uses an ArrayBlockingQueue(FIFO) to asynchronously process event log entries
     private static MivThreadPoolExecutor mivTpe = MivThreadPoolExecutor.getInstance("eventlog");
    
    public EventListener()
    {
        logger.info(" || --------------> EventListener being created and registering with the EventDispatcher");
        EventDispatcher2.getDispatcher().register(this);
    }

    /**
     * Listen for an MivEvent and log using the EventLogService.
     * The EventLogService will identify the various categories of an
     * MivEvent and log the details accordingly.
     * @param event
     */
    @Subscribe
    public void MivEvent(final MivEvent event)
    {
        logger.info("Received Event="+event.getEventTitle());
        
        // Get the eventLogService
        eventLogService = eventLogService == null
                ? (EventLogService) MivServiceLocator.getBean("eventLogService")
                : eventLogService;

        // Place the logEvent on the queue for execution 
        mivTpe.getExecutor().execute(
                new Runnable() {
                    @Override
                    public void run() {
                        eventLogService.logEvent(event);
                    }
                }
            );
        
        // Show some stats for the thread pool
        logger.info(mivTpe.getThreadpoolStats());

    }
    
    @Subscribe
    public void ReportOtherEvents(final DeadEvent event)
    {
        logger.info("Got another kind of event: {}", event.getEvent());
    }

    /**
     * Reinitialize the ThreadPoolExecutor when a ConfigChangeEvent is received
     * @param cce
     */
    @Subscribe
    public void ConfigurationChangeEvent(final ConfigurationChangeEvent cce)
    {
        if (cce.getWhatChanged() == ConfigReloadConstants.ALL)
        {
            mivTpe.reinitialize();
        }
    }

    /**
     * Get the ThreadPoolExecutor
     * @return
     */
    public MivThreadPoolExecutor getMivThreadPoolExecutor()
    {
        return mivTpe;
    }
 
}
