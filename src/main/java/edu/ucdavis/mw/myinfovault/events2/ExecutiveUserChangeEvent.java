/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ExecutiveUserChangeEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Describes the event of an executive role given to
 * the target user and taken from the former user.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.2
 */
public class ExecutiveUserChangeEvent extends UserEventStub implements UserEvent
{
    private static final long serialVersionUID = 201403191758L;

    private final MivPerson formerUser;
    private final MivRole executiveRole;


    /**
     * Create an executive user change event.
     *
     * @param realPerson real, logged-in person
     * @param newExecutive new executive
     * @param formerExecutive former executive
     * @param executiveRole former occupant of executive role
     */
    public ExecutiveUserChangeEvent(MivPerson realPerson,
                                    MivPerson newExecutive,
                                    MivPerson formerExecutive,
                                    MivRole executiveRole)
    {
        super(null, realPerson, newExecutive);

        this.formerUser = formerExecutive;
        this.executiveRole = executiveRole;
    }

    /**
     *
     * @return former occupant of executive role
     */
    public MivPerson getFormerExecutive()
    {
        return formerUser;
    }

    /**
     * @return executive role
     */
    public MivRole getExecutiveRole()
    {
        return executiveRole;
    }
}
