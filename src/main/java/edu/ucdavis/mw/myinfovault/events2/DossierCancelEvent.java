/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierCancelEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * An event to indicate a Dossier was cancelled.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class DossierCancelEvent extends DossierRouteEvent implements DossierEvent, RouteEvent
{
    private static final long serialVersionUID = 201308231423L;

    /**
     * TODO: javadoc
     *
     * @param realPerson
     * @param dossier
     */
    public DossierCancelEvent(MivPerson realPerson,
                              Dossier dossier)
    {
        super(null,
              realPerson,
              dossier,
              dossier.getDossierLocation(),
              DossierLocation.COMPLETE);
    }

    /**
     * TODO: javadoc
     *
     * @param realPerson
     * @param dossier
     */
    public DossierCancelEvent(MivEvent parentEvent, MivPerson realPerson,
                              Dossier dossier)
    {
        super(parentEvent,
              realPerson,
              dossier,
              dossier.getDossierLocation(),
              DossierLocation.COMPLETE);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Cancelled";
    }
}
