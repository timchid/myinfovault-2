/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierEventStub.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Provide basic support for the common requirements of all Dossier related events.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public abstract class DossierEventStub extends MivEventStub implements DossierEvent
{
    private static final long serialVersionUID = 201308301619L;

    private final Dossier dossier;

    /**
     * TODO: add Javadoc
     *
     * @param realPerson real event initiator
     * @param dossier dossier for which this event occurred
     */
    public DossierEventStub(final MivPerson realPerson, final Dossier dossier)
    {
        this(null, realPerson, dossier);
    }

    /**
     * TODO: add Javadoc
     *
     * @param parent event that caused this event
     * @param realPerson real event initiator
     * @param dossier dossier for which this event occurred
     */
    public DossierEventStub(final MivEvent parent, final MivPerson realPerson, final Dossier dossier)
    {
        super(parent, realPerson);

        this.dossier = dossier;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DossierEvent#getDossierId()
     */
    @Override
    public long getDossierId()
    {
        return this.dossier.getDossierId();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DossierEvent#getDossier()
     */
    @Override
    public Dossier getDossier()
    {
        return this.dossier;
    }
}
