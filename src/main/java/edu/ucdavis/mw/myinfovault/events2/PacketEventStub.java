/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecordChangeEventStub.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequestActionType;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Support for Packet events.
 *
 * @author rhendric
 * @since MIV 5.0
 */
public abstract class PacketEventStub extends DossierEventStub implements PacketEvent
{
    private static final long serialVersionUID = 201503301257L;

    private final PacketRequestActionType action;

    /**
     * @param realPerson real event initiator
     * @param dossier to which this packet event applies
     * @param PacketAction - request, submit...
     */
    public PacketEventStub(final MivPerson realPerson, final Dossier dossier, PacketRequestActionType action)
    {
        this(null, realPerson, dossier, action);
    }

    /**
     * @param parent event that caused this event
     * @param realPerson real event initiator
     * @param dossier to which this packet event applies
     * @param PacketAction - request, submit...
     */
    public PacketEventStub(final MivEvent parent, final MivPerson realPerson, Dossier dossier, PacketRequestActionType action)
    {
        super(parent, realPerson, dossier);
        this.action = action;
    }



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.PacketEvent#getAction()
     */
    @Override
    public PacketRequestActionType getAction()
    {
        return this.action;
    }

}
