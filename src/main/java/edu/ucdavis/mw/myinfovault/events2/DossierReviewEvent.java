/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierReviewEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * TODO: add javadoc
 *
 * @author Rick Hendricks
 * @since MIV 4.7.2
 */
public class DossierReviewEvent extends DossierEventStub implements DossierEvent, ReviewEvent
{
    private static final long serialVersionUID = 201308071000L;

    private final DossierLocation reviewLocation;
    private final DossierAttributeStatus reviewStatus;

    /**
     * TODO: add javadoc
     *
     * @param realPerson
     * @param dossier
     * @param reviewLocation
     * @param reviewStatus
     */
    public DossierReviewEvent(final MivPerson realPerson,
                              final Dossier dossier,
                              final DossierLocation reviewLocation,
                              final DossierAttributeStatus reviewStatus)
    {
        super(realPerson, dossier);

        this.reviewLocation = reviewLocation;
        this.reviewStatus = reviewStatus;
    }

    /**
     * TODO: add javadoc
     *
     * @param parent
     * @param dossier
     * @param reviewLocation
     * @param reviewStatus
     */
    public DossierReviewEvent(final MivEvent parent,
                              final Dossier dossier,
                              final DossierLocation reviewLocation,
                              final DossierAttributeStatus reviewStatus)
    {
        super(parent, parent.getRealPerson(), dossier);

        this.reviewLocation = reviewLocation;
        this.reviewStatus = reviewStatus;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.ReviewEvent#getReviewStatus()
     */
    @Override
    public DossierAttributeStatus getReviewStatus()
    {
        return this.reviewStatus;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.ReviewEvent#getReviewLocation()
     */
    @Override
    public DossierLocation getReviewLocation()
    {
        return this.reviewLocation;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Review Event";
    }
}
