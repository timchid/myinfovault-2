/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisclosureSignatureRequestEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * A signature was requested on a candidate's disclosure certificate.
 * Inherits most of its methods from the parent {@link DisclosureSignatureEvent}.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class DisclosureSignatureRequestEvent extends DisclosureSignatureEvent implements DossierEvent, SignatureEvent, RequestEvent
{
    private static final long serialVersionUID = 201308051440L;

    /**
     * TODO: add Javadoc
     *
     * @param realPerson
     * @param signature
     */
    public DisclosureSignatureRequestEvent(MivPerson realPerson, MivElectronicSignature signature)
    {
        super(null, realPerson, signature);

        // must set changes/additions as comments
        this.setComments(((DCBo)signature.getDocument()).getChangesAdditions());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RequestEvent#getRequestedUserId()
     *
     *  "RequestEvent" is a little experimental -- now it's pretty redundant, it's only
     *  user for signature requests, and those already have getRequestedSignerId();
     *  There are other things that *could* be requested, e.g. a request for a candidate
     *  to submit their packet (when Action, Dossier, Packet are separate concepts)
     */
    @Override
    public int getRequestedUserId()
    {
        return Integer.parseInt(getSignature().getRequestedSigner());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Disclosure Signature Request";
    }
}
