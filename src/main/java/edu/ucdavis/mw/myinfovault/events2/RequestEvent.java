/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RequestEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;


/**
 * RequestEvent is a little experimental -- now it's pretty redundant, it's only
 * user for signature requests, and those already have getRequestedSignerId();
 * There are other things that *could* be requested, e.g. a request for a candidate
 * to submit their packet (when Action, Dossier, Packet are separate concepts)
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
@LoggableEvent(EventCategory.REQUEST)
public interface RequestEvent extends DossierEvent
{
    /**
     * Get the identifier of the person who was requested to perform some action.<br>
     * Used primarily for Signature Requests.
     * @return the MIV user ID of the person being asked to do something.
     * @see SignatureEvent#getRequestedSignerId()
     */
    public int getRequestedUserId();
}


