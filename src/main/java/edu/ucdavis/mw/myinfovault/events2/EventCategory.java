/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventCategory.java
 */


package edu.ucdavis.mw.myinfovault.events2;


/**
 * TODO: Add Javadoc
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public enum EventCategory
{
    ROUTE("Routing"),

    /** An event on the Recommended Action Form (aka 'RAF') */
    RAF("Action Form"),

    REVIEW("Review"),

    VOTE("Voting"),

    /** An event that is related to signing documents */
    SIGNATURE("Signature"),

    DECISION("Decision"),

    /** An event on the Candidate's Disclosure Certificate (aka 'DC' or 'CDC'). */
    DISCLOSURE("Candidate Disclosure"),

    /**
     * Signature request.
     */
    REQUEST("Request"),

    HOLD("Hold"),

    RELEASE("Release"),

    UPLOAD("Document Upload"),

    DOSSIER("Dossier"),

    USER("User"),

    CONFIG("Configuration"),

    ERROR("Error"),

    RECORDCHG("Record Change"),

    PACKET("Packet");


    private final String description;

    /**
     * Instantiate an event category.
     *
     * @param description category description
     */
    private EventCategory(String description)
    {
        this.description = description;
    }

    /**
     * @return category description
     */
    public String getDescription()
    {
        return description;
    }
}
