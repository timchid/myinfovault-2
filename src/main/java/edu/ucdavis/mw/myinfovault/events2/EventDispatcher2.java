/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventDispatcher2.java
 */


package edu.ucdavis.mw.myinfovault.events2;


import com.google.common.eventbus.EventBus;


/**
 * When using the refactored event hierarchy this dispatcher must be used.
 * Only classes that implement the {@link edu.ucdavis.mw.myinfovault.events2.MivEvent MivEvent} interface
 * may be posted to the event bus.
 * This class will be renamed and replace the original {@link EventDispatcher} when
 * all events are converted to the new hierarchy.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class EventDispatcher2
{
    private static final EventBus bus = new MivEventBus("MivEventBus2");

    private EventDispatcher2() {} // prevent instantiation


    public static EventBus getDispatcher()
    {
        return EventDispatcher2.bus;
    }


    /**
     * Provide a version of the Guava EventBus that accepts only MivEvents.
     * @author Stephen Paulsen
     * @since MIV 4.7.1
     */
    private static class MivEventBus extends EventBus
    {
        /*public MivEventBus() // We could need this constructor if this class is no longer private.
        {
            super();
        }*/
        public MivEventBus(String identifier)
        {
            super(identifier);
        }

        @Override
        public void post(Object event)
        {
            if (! (event instanceof MivEvent)) {
                throw new IllegalArgumentException("Only MivEvents may be posted to this event bus");
            }
            super.post(event);
        }
    }
}
