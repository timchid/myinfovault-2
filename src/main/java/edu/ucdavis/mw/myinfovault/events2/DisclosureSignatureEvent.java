/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisclosureSignatureEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * TODO: Add Javadoc
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class DisclosureSignatureEvent extends DossierEventStub implements DisclosureEvent, SignatureEvent, DossierEvent
{
    private static final long serialVersionUID = 201307311025L;

    private final MivElectronicSignature signature;

    /**
     * TODO: add javadoc
     *
     * @param realPerson
     * @param signature
     */
    public DisclosureSignatureEvent(final MivPerson realPerson, final MivElectronicSignature signature)
    {
        this(null, realPerson, signature);
    }

    /**
     * TODO: add javadoc
     *
     * @param parent
     * @param realPerson
     * @param signature
     */
    public DisclosureSignatureEvent(final MivEvent parent, final MivPerson realPerson, final MivElectronicSignature signature)
    {
        super(parent, realPerson, signature.getDocument().getDossier());

        this.signature = signature;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.SignatureEvent#getSignature()
     */
    @Override
    public MivElectronicSignature getSignature()
    {
        return this.signature;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DisclosureEvent#getDisclosure()
     */
    @Override
    public DCBo getDisclosure()
    {
        return this.signature.getDocument();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Disclosure Signature";
    }
}
