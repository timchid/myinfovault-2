/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecordChangeEventStub.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Support for RecordChange events.
 *
 * @author rhendric
 * @since MIV 5.0
 *
 */
public abstract class RecordChangeEventStub extends MivEventStub implements RecordChangeEvent
{
    private static final long serialVersionUID = 201503040825L;

    private final String tableName;
    private final int recordId;
    private final ChangeAction action;

    /**
     * Create a RecordChange event for the supplied change action
     *
     * @param realPerson real event initiator
     * @param table name in which the record was changed
     * @param record id which was changed
     * @param the action - insert, update, delete
     */
    public RecordChangeEventStub(final MivPerson realPerson, final String tableName, int recordId, ChangeAction action)
    {
        this(null, realPerson, tableName, recordId, action);
    }

    /**
     * Create a RecordChange event for the supplied change action
     *
     * @param parent event that caused this event
     * @param realPerson real event initiator
     * @param table name in which the record was changed
     * @param record id which was changed
     * @param the action - insert, update, delete
     */
    public RecordChangeEventStub(final MivEvent parent, final MivPerson realPerson, final String tableName, int recordId, ChangeAction action)
    {
        super(parent, realPerson);

        this.tableName = tableName;
        this.recordId = recordId;
        this.action = action;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RecordChangeEvent#getRecordId()
     */
    @Override
    public long getRecordId()
    {
        return this.recordId;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RecordChangeEvent#getTableName()
     */
    @Override
    public String getTableName()
    {
        return this.tableName;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RecordChangeEvent#getAction()
     */
    @Override
    public ChangeAction getAction()
    {
        return this.action;
    }

}
