/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierErrorEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;

/**
 * TODO: add Javadoc
 *
 * @author Rick Hendricks
 * @since MIV 4.7.2
 */
public class DossierErrorEvent extends DossierEventStub implements DossierEvent, ErrorEvent
{
    private static final long serialVersionUID = 20130905160500L;

    private final Exception exception;

    /**
     * TODO: add Javadoc
     *
     * @param parent event that caused this event
     * @param dossier
     * @param exception
     */
    public DossierErrorEvent(final MivEvent parent, final Dossier dossier, final Exception exception)
    {
        super(parent, parent.getRealPerson(), dossier);

        this.exception = exception;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Error Event";
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.ErrorEvent#getException()
     */
    @Override
    public Exception getException()
    {
        return this.exception;
    }
}

