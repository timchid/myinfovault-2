/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivEventStub.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Provides basic support for MIV user-initiated events.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public abstract class MivEventStub implements MivEvent
{
    private static final long serialVersionUID = 201307311025L;

    private final Date eventTime = new Date();
    private final UUID eventId = new UUID();
    private final MivEvent parent;
    private String comments = null;
    private final MivPerson realPerson;
    private MivPerson shadowPerson;

    /**
     * TODO: add Javadoc
     *
     * @param parent event that caused this event
     * @param realPerson real event initiator
     */
    public MivEventStub(MivEvent parent, MivPerson realPerson)
    {
        this.parent = parent;
        this.realPerson = realPerson;

        if (parent != null && parent.getShadowPerson() != null)
        {
            this.shadowPerson = parent.getShadowPerson();
        }
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getEventId()
     */
    @Override
    public UUID getEventId()
    {
        return this.eventId;
    }


    /**
     * TODO: Each concrete event class should implement this.
     * It's only here now so everything compiles right away.
     */
    @Override
    public String getEventTitle()
    {
        return "Not Implemented Yet.";
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getEventTime()
     */
    @Override
    public Date getEventTime()
    {
        return this.eventTime;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getParentEvent()
     */
    @Override
    public MivEvent getParentEvent()
    {
        return this.parent;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getCategories()
     */
    @Override
    public final Set<EventCategory> getCategories()
    {
        return getCategories(this.getClass());
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getRealUserId()
     */
    @Override
    public String getRealUserId()
    {
        return Integer.toString(this.realPerson.getUserId());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getRealPerson()
     */
    @Override
    public MivPerson getRealPerson()
    {
        return this.realPerson;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getShadowUserId()
     */
    @Override
    public String getShadowUserId()
    {
        return this.shadowPerson != null ? Integer.toString(this.shadowPerson.getUserId()) : null;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#getShadowPerson()
     */
    @Override
    public MivPerson getShadowPerson()
    {
        return this.shadowPerson;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEvent#setShadowPerson(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public MivEvent setShadowPerson(MivPerson shadowPerson)
    {
        this.shadowPerson = shadowPerson;
        return this;
    }


    @Override
    public String getComments()
    {
        return this.comments;
    }

    @Override
    public MivEvent setComments(String comments)
    {
        this.comments = comments;
        return this;
    }



    /**
     * Retain a map of event categories by event class so introspection only has to happen once per class.
     */
    private static final Map<Class<? extends MivEventStub>, Set<EventCategory>> categoryMap = new HashMap<>();

    /**
     * Get the categories for the given event class.
     *
     * @param eventClass MIV event class
     * @return categories for the given event class
     */
    @SuppressWarnings("unchecked")
    private static Set<EventCategory> getCategories(Class<? extends MivEventStub> eventClass)
    {
        // return from category map if available
        if (categoryMap.containsKey(eventClass)) return categoryMap.get(eventClass);

        // otherwise, create a new set of categories for the event class
        Set<EventCategory> categories = EnumSet.noneOf(EventCategory.class);

        // harvest event categories from the event class interfaces
        for (Class<?> eventInterface : eventClass.getInterfaces())
        {
            // Add this interface if it's annotated
            LoggableEvent loggableEventAnnotation = eventInterface.getAnnotation(LoggableEvent.class);

            if (loggableEventAnnotation != null)
            {
                categories.add(loggableEventAnnotation.value());
            }
        }

        // add super class categories if not MivEventStub
        if (eventClass.getSuperclass() != null && !eventClass.getSuperclass().equals(MivEventStub.class))
        {
            categories.addAll(getCategories((Class<? extends MivEventStub>) eventClass.getSuperclass()));
        }

        // key categories to the event class
        categoryMap.put(eventClass, categories);

        return categories;
    }
}
