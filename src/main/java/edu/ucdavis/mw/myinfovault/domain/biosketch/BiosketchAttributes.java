package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * Domain Object representing the BiosketchAttributes
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchAttributes extends DomainObject
{
    private int biosketchID;
    private String name;
    private String value;
    private int sequence;
    private boolean display;

    /**
     * @param biosketchAttributes 
     */
    public BiosketchAttributes(BiosketchAttributes biosketchAttributes)
    {
        super(0,0);        
        this.name = biosketchAttributes.name;
        this.value = biosketchAttributes.value;
        this.biosketchID = biosketchAttributes.biosketchID;
        this.sequence = biosketchAttributes.sequence;
        this.display = biosketchAttributes.display;
    }
    
    
    /**
     * @param id
     */
    public BiosketchAttributes(int id)
    {
        super(id);
    }

    /**
     * @return the biosketchID
     */
    public int getBiosketchID()
    {
        return biosketchID;
    }

    /**
     * @param biosketchID
     *            the biosketchID to set
     */
    public void setBiosketchID(int biosketchID)
    {
        this.biosketchID = biosketchID;
    }

    /**
     * @return the display
     */
    public boolean isDisplay()
    {
        return display;
    }

    /**
     * @param display
     *            the display to set
     */
    public void setDisplay(boolean display)
    {
        this.display = display;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the sequence
     */
    public int getSequence()
    {
        return sequence;
    }

    /**
     * @param sequence
     *            the sequence to set
     */
    public void setSequence(int sequence)
    {
        this.sequence = sequence;
    }

    /**
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(String value)
    {
        this.value = value;
    }
}
