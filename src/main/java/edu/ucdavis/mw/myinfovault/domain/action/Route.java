/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventLogDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;


/**
 * TODO: add javadoc
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class Route
{
    private static final String POINTS_TO = " \u2192 ";

    private final DossierLocation origin;
    private final DossierLocation destination;
    private final String representation;

    public Route(DossierLocation origin, DossierLocation destination)
    {
        this.origin = origin;
        this.destination = destination;
        this.representation = origin.getDescription() + POINTS_TO + destination.getDescription();
    }

    public DossierLocation getOrigin() {
        return origin;
    }

    public DossierLocation getDestination() {
        return destination;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return representation;
    }
}
