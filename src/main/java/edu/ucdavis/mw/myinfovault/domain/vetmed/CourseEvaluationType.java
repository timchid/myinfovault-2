/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CourseEvaluationType.java
 */

package edu.ucdavis.mw.myinfovault.domain.vetmed;

/**
 * Course evaluation record type.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public enum CourseEvaluationType
{
    COMPLETE(1, "Complete"),
    SUMMARY(2, "Summary"),
    NA(3, "Not Available");

    private final int id;
    private final String description;

    /**
     * Instantiate the course evaluation type.
     *
     * @param id type ID
     * @param description type description
     */
    private CourseEvaluationType(int id, String description)
    {
        this.id = id;
        this.description = description;
    }

    /**
     * @return course evaluation type ID
     */
    public int getId() {
        return id;
    }

    /**
     * @return course evaluation type description
     */
    public String getDescription() {
        return description;
    }
}
