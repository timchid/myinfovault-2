package edu.ucdavis.mw.myinfovault.domain.biosketch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;



/**
 * A collection of conditions used to select records.
 * @author Stephen Paulsen
 * @since MIV 2.1
 */
public class Ruleset extends DomainObject
{
    public enum Operator {
        AND, // "match ALL of the following"
        OR
        // "match ANY of the following"
    };

    // Most of the time we'll do things like
    // Year>=1993 AND Year<=2008 so start w/size 2
    private List<Criterion> criteria = new ArrayList<Criterion>(2);
    private int biosketchID;
    private String recType;
    private Operator operator = Operator.AND;

    /**
     * Create a new Ruleset for the identified user.
     * 
     * @param userId
     *            owner of this ruleset
     */
    public Ruleset(int userId)
    {
        this(0, userId);
    }

    /**
     * Create a Ruleset from a stored copy.
     * 
     * @param id
     *            Unique ID of this ruleset
     * @param userId
     *            owner of this ruleset
     */
    public Ruleset(int id, int userId)
    {
        super(id, userId);
    }

    /**
     * 
     * @param c
     * @return the modified Ruleset
     */
    public Ruleset addCriterion(Criterion c)
    {
        this.criteria.add(c);
        return this;
    }

    /**
     * @return the criterion list
     */
    public List<Criterion> getCriterion()
    {
        return this.criteria;
    }

    /**
     * 
     * @param c
     * @return the modified Ruleset
     */
    public Ruleset removeCriterion(Criterion c)
    {
        this.criteria.remove(c);
        return this;
    }

    /**
     * Set the operator on the criteria to logical 'AND' or logical 'OR'
     * 
     * @param o
     * @return the modified Ruleset
     */
    public Ruleset setOperator(Operator o)
    {
        this.operator = o;
        return this;
    }

    /**
     * @return
     */
    public Operator getOperator()
    {
        return operator;
    }

    /**
     * Returns a String representing this Ruleset and its Criteria in a form
     * that may be used in SQL statements.
     */
    @Override
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        StringBuilder rule = new StringBuilder();

        Iterator<Criterion> i = criteria.iterator();
        while (i.hasNext())
        {
            Criterion c = i.next();
            s.append("(").append(c.toString()).append(")");
            if (i.hasNext())
            {
                s.append(" ").append(this.operator).append(" ");
            }
        }
        if(s.length()>0)
        {            
            rule.append("(").append(s.toString()).append(")");
        }
        return rule.toString();
    }

    /**
     * @param recType
     *            the recType to set
     */
    public void setRecType(String recType)
    {
        this.recType = recType;
    }

    /**
     * @return the recType
     */
    public String getRecType()
    {
        return recType;
    }

    /**
     * @return the biosketchID
     */
    public int getBiosketchID()
    {
        return biosketchID;
    }

    /**
     * @param biosketchID the biosketchID to set
     */
    public void setBiosketchID(int biosketchID)
    {
        this.biosketchID = biosketchID;
    }

    public List<Criterion> getCriteria()
    {
        return criteria;
    }

    public void setCriteria(List<Criterion> criteria)
    {
        this.criteria = criteria;
    }
}
