/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Packet.java
 */

package edu.ucdavis.mw.myinfovault.domain.packet;

// import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * This class represents a Packet created by the Candidate
 * @author rhendric
 *
 * @since MIV 5.0
 */
@XmlRootElement(name="packet")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Packet extends ResourceSupport
{
    /** Never set the packetId directly like <code>this.packetId = 42L;</code><br>
        Instead, always call <code>this.setPacketId(42L);</code> */
    private Long packetId;
    private String packetName;
    private Integer userId;
    private Date createTime;
    private Date modifyTime;
    private boolean newPacket;


    // The key into the packetItemsMap = packetId+":"+recordId+":"+sectionId
    private Map<String,PacketContent>packetItemsMap;

    public Packet()
    {
        //Used by spring mvc for databinding
    }

    /**
     * Construct a Packet object
     * @param userId
     * @param packetName
     */
    public Packet(int userId, String packetName)
    {
        this(0L, userId, packetName, new ArrayList<PacketContent>());
        this.newPacket = true;
    }

    /**
     * Construct a Packet object
     * @param userId
     * @param packetName
     * @param packetItems
     */
    public Packet(int userId, String packetName, Collection<PacketContent>items)
    {
        this(0L, userId, packetName, items);
        this.newPacket = true;
    }

    /**
     * Construct a Packet object
     * @param packetId
     * @param userId
     * @param packetName
     * @param packetItems
     */
    public Packet(Long packetId, int userId, String packetName, Collection<PacketContent> packetItems)
    {
        this.setPacketId(packetId);
        this.userId = userId;
        this.packetName = packetName;
        this.packetItemsMap = new ConcurrentHashMap<String, PacketContent>();
        this.buildContentMap(packetItems);
    }

    public Packet(Long packetId, int userId, String packetName, Date insert, Date update, Collection<PacketContent> packetItems)
    {
        this(packetId, userId, packetName, packetItems);
        this.createTime = insert;
        this.modifyTime = update;
    }

    /**
     * @param item
     */
    public void addContentItem(final PacketContent packetItem)
    {
        this.packetItemsMap.put(packetItem.getPacketId()+":"+packetItem.getRecordId()+":"+packetItem.getSectionId(), packetItem);
    }

    /**
     * @param items
     */
    public void addContentItems(Collection<PacketContent>packetItems)
    {
        for (PacketContent packetItem : packetItems)
        {
            this.addContentItem(packetItem);
        }
    }

    /**
     * @param item
     */
    public void removeContentItem(PacketContent packetItem)
    {
        this.packetItemsMap.remove(packetItem.getPacketId()+":"+packetItem.getRecordId()+":"+packetItem.getSectionId());
    }

    /**
     * @param items
     */
    public void removeContentItems(Collection<PacketContent>packetItems)
    {
       for (PacketContent packetItem : packetItems)
       {
           this.removeContentItem(packetItem);
       }
    }

    /**
     * @param packetName
     */
    public void setPacketName(String packetName)
    {
        this.packetName = packetName;
    }

    /**
     * @return packetName
     */
    public String getPacketName()
    {
        return this.packetName;
    }

    /**
     * Assign the Packet ID number to this packet.
     * @param packetId The packet number, or <tt>0L</tt> or <tt>-1L</tt> to indicate a <em>New</em> packet.
     */
    public void setPacketId(long packetId)
    {
        this.packetId = packetId;
        newPacket = (packetId == 0L || packetId == -1L);
    }

    /**
     * @return packetId
     */
    public Long getPacketId()
    {
        return this.packetId;
    }

    public boolean isNewPacket()
    {
        return this.newPacket;
    }

    public Date getCreateTime()
    {
        return this.createTime;
    }

    public Date getModifiedTime()
    {
        return this.modifyTime;
    }

    /**
     * Get time this Packet was created as an epoch time-stamp.
     * @return the number of milliseconds since January 1, 1970, 00:00:00 GMT that this packet was created.
     */
    public Long getCreateTimestamp()
    {
        return this.createTime == null ? null : this.createTime.getTime();
    }

    /**
     * Get time this Packet was last modified as an epoch time-stamp.
     * @return the number of milliseconds since January 1, 1970, 00:00:00 GMT that this packet was modified.
     */
    public Long getModifiedTimestamp()
    {
        return this.modifyTime == null ? null : this.modifyTime.getTime();
    }


    /**
     * @return packetItems
     */
    public Collection<PacketContent> getPacketItems()
    {
        return this.packetItemsMap.values();
    }

    /**
     * The key into the packetItemsMap is the colon delimited string:
     * packetId+":"+sectionId+":"+recordId
     *
     * @return packetItemsMap
     */
    @JsonIgnore
    public Map<String, PacketContent> getPacketItemsMap()
    {
        return packetItemsMap;
    }

    public void setPacketItems(List<PacketContent> packetItems)
    {
        if (packetItems == null)
        {
            //for the purposes of the API, not specifying packetItems (i.e. no packetItems member in the JSON object)
            //is different from explicitly specifying no items (i.e. an empty array in the JSON).
            this.packetItemsMap = null;
            return;
        }
        else
        {
            this.packetItemsMap = new ConcurrentHashMap<String, PacketContent>();
            this.buildContentMap(packetItems);
        }
    }

    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    /**
     * @return userId
     */
    public Integer getUserId()
    {
        return this.userId;
    }

    /**
     * Build a map of the packet items.
     * @param packetItems
     */
    private void buildContentMap(Collection<PacketContent>packetItems)
    {
        for (PacketContent packetItem : packetItems)
        {
            this.packetItemsMap.put((packetItem.getPacketId() == null ? packetId : packetItem.getPacketId()) +
                                    ":" + packetItem.getRecordId() + ":" + packetItem.getSectionId(),
                                    packetItem);
        }

    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("Packet #%s named \"%s\" belongs to user #%s", packetId, packetName, userId);
    }

}
