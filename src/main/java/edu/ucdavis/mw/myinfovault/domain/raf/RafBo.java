/*

 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafBo.java
 */

package edu.ucdavis.mw.myinfovault.domain.raf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.dao.raf.RafDaoImpl;
import edu.ucdavis.mw.myinfovault.domain.PolarResponse;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.Department.DepartmentType;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Recommended action form business object.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class RafBo implements Serializable
{
    private static final long serialVersionUID = 200904101607L;

    private static Logger logger = LoggerFactory.getLogger(RafBo.class);

    private int id = 0;
    private final Dossier dossier;
    private final String candidateName;
    private int updateUserId = 0;
    private boolean AppointmentType9mo = false;
    private boolean AppointmentType11mo = true;
    private Acceleration acceleration = Acceleration.NORMAL;
    private int accelerationYears = 0;
    private int decelerationYears = 0;
    private int yearsAtRank = 0;
    private int yearsAtStep = 0;
    private boolean recommendationDept = true;
    private PolarResponse recommendationCSD = PolarResponse.NA;
    private PolarResponse recommendationJAS = PolarResponse.NA;
    private PolarResponse recommendationAFP = PolarResponse.NA;
    protected List<Department> departments = new ArrayList<Department>();
    private final List<Status> statuses;

    /**
     * Create RAF from database record.
     *
     * @param id database record ID
     * @param dossier
     * @param candidateName dossier candidate's display name at the time of creation
     * @param schoolId
     * @param departmentId
     * @param AppointmentType9mo
     * @param AppointmentType11mo
     * @param acceleration
     * @param yearsAtRank
     * @param yearsAtStep
     * @param recommendationDept
     * @param recommendationCSD
     * @param recommendationJAS
     * @param recommendationAFP
     * @param departments
     * @param statuses
     * @param updateUserId
     */
    public RafBo(int id,
                 Dossier dossier,
                 String candidateName,
                 int schoolId,
                 int departmentId,
                 boolean AppointmentType9mo,
                 boolean AppointmentType11mo,
                 int acceleration,
                 int yearsAtRank,
                 int yearsAtStep,
                 boolean recommendationDept,
                 PolarResponse recommendationCSD,
                 PolarResponse recommendationJAS,
                 PolarResponse recommendationAFP,
                 List<Department> departments,
                 List<Status> statuses,
                 int updateUserId)
    {
        this.id = id;
        this.dossier = dossier;
        this.candidateName = candidateName;
        this.AppointmentType9mo = AppointmentType9mo;
        this.AppointmentType11mo = AppointmentType11mo;
        this.acceleration = Acceleration.valueOf(acceleration);
        this.accelerationYears = acceleration;
        this.decelerationYears = acceleration;
        this.yearsAtRank = yearsAtRank;
        this.yearsAtStep = yearsAtStep;
        this.recommendationDept = recommendationDept;
        this.recommendationCSD = recommendationCSD;
        this.recommendationJAS = recommendationJAS;
        this.recommendationAFP = recommendationAFP;
        this.departments = departments;
        this.statuses = statuses;
        this.updateUserId = updateUserId;

        // add blank statuses if DNE
        if (this.statuses.size() < 2)
        {
            this.statuses.add(new Status(StatusType.PRESENT));
            this.statuses.add(new Status(StatusType.PROPOSED));
        }
    }

    /**
     * Create a new RAF for the given dossier.
     *
     * @param dossier MIV dossier for this RAF
     */
    public RafBo(Dossier dossier)
    {
        this.dossier = dossier;

        for (final Map<String,String> department: dossier.getDepartments())
        {
            boolean isPrimaryDepartment = Boolean.parseBoolean(department.get("primary"));

            // Add primary department to top, others to bottom of list
            departments.add(isPrimaryDepartment ? 0 : departments.size(),
                            new Department(dossier,
                                           isPrimaryDepartment,
                                           Integer.parseInt(department.get("schoolid")),
                                           Integer.parseInt(department.get("departmentid"))));
        }

        this.candidateName = dossier.getAction().getCandidate().getDisplayName();

        // new blank present and proposed statuses
        this.statuses = new ArrayList<Status>(StatusType.values().length);
        this.statuses.add(new Status(StatusType.PRESENT));
        this.statuses.add(new Status(StatusType.PROPOSED));
    }

    /**
     * @return RAF record ID
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * @param id RAF record ID
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return dossier to which this recommended action belongs
     */
    public Dossier getDossier()
    {
        return this.dossier;
    }

    /**
     * @return dossier candidate's display name at the time of creation
     */
    public String getCandidateName()
    {
        return candidateName;
    }

    /**
     * @return ID of real logged-in user who last updated this RAF
     */
    public int getUpdateUserId()
    {
        return updateUserId;
    }

    /**
     * @return if this a nine month appointment
     */
    public boolean isAppointmentType9mo()
    {
        return AppointmentType9mo;
    }

    /**
     * @param appointmentType9mo if this a nine month appointment
     */
    public void setAppointmentType9mo(final boolean appointmentType9mo)
    {
        AppointmentType9mo = appointmentType9mo;
    }

    /**
     * @return if this an eleven month appointment
     */
    public boolean isAppointmentType11mo()
    {
        return AppointmentType11mo;
    }

    /**
     * @param appointmentType11mo if this an eleven month appointment
     */
    public void setAppointmentType11mo(final boolean appointmentType11mo)
    {
        AppointmentType11mo = appointmentType11mo;
    }

    /**
     * @return acceleration type, either normal, acceleration, or deceleration
     */
    public Acceleration getAcceleration()
    {
        return acceleration;
    }

    /**
     * @param acceleration positive years for acceleration or Negative years for deceleration
     */
    public void setAcceleration(Acceleration acceleration)
    {
        this.acceleration = acceleration;
    }

    /**
     * @return absolute value of acceleration years
     */
    public int getAccelerationYears()
    {
        return acceleration == Acceleration.ACCELERATION
             ? Math.abs(accelerationYears)
             : 0;
    }

    /**
     * @param accelerationYears number of acceleration years
     */
    public void setAccelerationYears(int accelerationYears)
    {
        this.accelerationYears = accelerationYears;
    }

    /**
     * @return absolute value of deceleration years
     */
    public int getDecelerationYears()
    {
        return acceleration == Acceleration.DECELERATION
             ? Math.abs(decelerationYears)
             : 0;
    }

    /**
     * @param decelerationYears number of deceleration years
     */
    public void setDecelerationYears(int decelerationYears)
    {
        this.decelerationYears = decelerationYears;
    }

    /**
     * @return number of years at rank (not less than zero)
     */
    public int getYearsAtRank()
    {
        return Math.max(0, yearsAtRank);
    }

    /**
     * @param yearsAtRank number of years at rank
     */
    public void setYearsAtRank(int yearsAtRank)
    {
        if (yearsAtRank < 0)
        {
            logger.warn("RAF Years at RANK is not supposed to be negative: Dossier #{}, RAF #{}", getDossier().getDossierId(), getId());
        }

        this.yearsAtRank = yearsAtRank;
    }

    /**
     * @return number of years at step (not less than zero)
     */
    public int getYearsAtStep()
    {
        return Math.max(0, yearsAtStep);
    }

    /**
     * @param yearsAtStep number of years at step
     */
    public void setYearsAtStep(int yearsAtStep)
    {
        this.yearsAtStep = yearsAtStep;
    }

    /**
     * @return if the action is recommended by the department
     * @deprecated use only for legacy signature validity
     */
    @Deprecated
    public boolean isRecommendationDept()
    {
        return recommendationDept;
    }

    /**
     * @return CSD committee recommendation
     * @deprecated use only for legacy signature validity
     */
    @Deprecated
    public PolarResponse getRecommendationCSD()
    {
        return recommendationCSD;
    }

    /**
     * @return JAS committee recommendation
     * @deprecated use only for legacy signature validity
     */
    @Deprecated
    public PolarResponse getRecommendationJAS()
    {
        return recommendationJAS;
    }

    /**
     * @return AFP committee recommendation
     * @deprecated use only for legacy signature validity
     */
    @Deprecated
    public PolarResponse getRecommendationAFP()
    {
        return recommendationAFP;
    }

    /**
     * @return list of RAF departments ordered by type
     */
    public List<Department> getDepartments()
    {
        Collections.sort(departments);

        return departments;
    }

    /**
     * Get the first department matching the given scope.
     *
     * @param scope department scope
     * @return matching department
     */
    public Department getDepartment(Scope scope)
    {
        for (Department department : departments)
        {
            if (department.getScope().matches(scope)) return department;
        }

        // not found
        return null;
    }

    /**
     * @return RAF statuses, either 'present' or 'proposed'
     */
    public List<Status> getStatuses()
    {
        return statuses;
    }

    /**
     * @param type status type
     *
     * @return status for the given type
     */
    public Status getStatus(StatusType type)
    {
        for (Status status : statuses)
        {
            if (status.getStatusType() == type) return status;
        }

        throw new MivSevereApplicationError("RAF doesn't contain a '" + type + "' type status");
    }

    /**
     * Possible acceleration types for a RAF.
     *
     * @author Craig Gilmore
     * @since MIV 3.0
     */
    public enum Acceleration
    {
        /**
         * Acceleration is 'normal'
         */
        NORMAL ("Normal", 0),

        /**
         * Acceleration is an 'acceleration'
         */
        ACCELERATION ("Accel.", 1),

        /**
         * Acceleration is a 'deceleration'
         */
        DECELERATION ("Decel.", -1);

        private final String description;
        private final int coefficient;

        private Acceleration(String description, int coefficient)
        {
            this.description = description;
            this.coefficient = coefficient;
        }

        /**
         * @return formatted acceleration description
         */
        public String getDescription()
        {
            return this.description;
        }

        /**
         * @return acceleration years multiplier (used in {@link RafDaoImpl#persistRaf(RafBo, int)}
         */
        public int getCoefficient()
        {
            return this.coefficient;
        }

        /**
         * Get the type of acceleration for the given number of years.
         *
         * @param accelerationYears either positive, negative, or zero
         * @return the acceleration associated with the year value
         */
        public static Acceleration valueOf(int accelerationYears)
        {
            if (accelerationYears > 0)
            {
                return ACCELERATION;
            }
            else if (accelerationYears < 0)
            {
                return DECELERATION;
            }

            return NORMAL;
        }
    }

    /**
     * @return the primary department
     */
    public Department getPrimaryDepartment()
    {
        List<Department> primaryDepartments = getDepartments(DepartmentType.PRIMARY);

        if (primaryDepartments.size() != 1) return null;

        return primaryDepartments.get(0);
    }

    /**
     * @return joint departments
     */
    public List<Department> getJointDepartments()
    {
        return getDepartments(DepartmentType.JOINT);
    }

    /**
     * @return primary and joint departments
     */
    public List<Department> getPrimaryAndJointDepartments()
    {
        return getDepartments(DepartmentType.PRIMARY,
                              DepartmentType.JOINT);
    }

    /**
     * @param departmentTypes department type(s)
     *
     * @return list containing only departments of the given type(s)
     */
    private List<Department> getDepartments(DepartmentType...departmentTypes)
    {
        return (new DepartmentTypeFilter(departmentTypes)).apply(departments);
    }
}
