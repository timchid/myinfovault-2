package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;
import edu.ucdavis.myinfovault.designer.Availability;

/**
 * Domain Object representing Biosketch Sections
 * @author Brij Garg
 * @since MIV 2.1
 */
public class BiosketchSection extends DomainObject
{
    private int biosketchID;
    private int sectionID;
    private String sectionName;
    private boolean isMainSection;
    private boolean inSelectList;
    private boolean mayHideHeader;
    private int displayInBiosketch;
    private boolean display;
    private Availability availability = Availability.OPTIONAL_OFF;
    private boolean displayHeader;


    public BiosketchSection(int id)
    {
        super(id);
    }
    
    /**
     * @param biosketchSectionLimits 
     */
    public BiosketchSection(BiosketchSectionLimits biosketchSectionLimits)   {
        super(0,0);        
        this.sectionID = biosketchSectionLimits.getSectionID();
        this.sectionName = biosketchSectionLimits.getSectionName();
        this.isMainSection = biosketchSectionLimits.isMainSection();
        this.inSelectList = biosketchSectionLimits.isInSelectList();
        this.mayHideHeader = biosketchSectionLimits.isMayHideHeader();
        this.displayInBiosketch = biosketchSectionLimits.getDisplayInBiosketch();
        this.availability = biosketchSectionLimits.getAvailability();
    }
    
    /**
     * @return the availability
     */
    public Availability getAvailability()
    {
        return availability;
    }

    /**
     * @param availability the availability to set
     */
    public void setAvailability(Availability availability)
    {
        this.availability = availability;
    }

    /**
     * @return the biosketchID
     */
    public int getBiosketchID()
    {
        return biosketchID;
    }

    /**
     * @param biosketchID the biosketchID to set
     */
    public void setBiosketchID(int biosketchID)
    {
        this.biosketchID = biosketchID;
    }

    /**
     * @return the display
     */
    public boolean isDisplay()
    {
        return display;
    }

    /**
     * @param display if display is true include section in packet
     */
    public void setDisplay(boolean display)
    {
        this.display = display;
    }

    /**
     * @return the sectionID
     */
    public int getSectionID()
    {
        return sectionID;
    }

    /**
     * @param sectionID the sectionID to set
     */
    public void setSectionID(int sectionID)
    {
        this.sectionID = sectionID;
    }

    /**
     * @return the displayHeader
     */
    public boolean isDisplayHeader()
    {
        return displayHeader;
    }

    /**
     * @param displayHeader the displayHeader to set
     */
    public void setDisplayHeader(boolean displayHeader)
    {
        this.displayHeader = displayHeader;
    }

    
    public String getSectionName()
    {
        return sectionName;
    }

    public void setSectionName(String sectionName)
    {
        this.sectionName = sectionName;
    }

    public boolean isMainSection()
    {
        return isMainSection;
    }

    public void setMainSection(boolean isMainSection)
    {
        this.isMainSection = isMainSection;
    }

    public boolean isInSelectList()
    {
        return inSelectList;
    }

    public void setInSelectList(boolean inSelectList)
    {
        this.inSelectList = inSelectList;
    }

    public boolean isMayHideHeader()
    {
        return mayHideHeader;
    }

    public void setMayHideHeader(boolean mayHideHeader)
    {
        this.mayHideHeader = mayHideHeader;
    }

    public int getDisplayInBiosketch()
    {
        return displayInBiosketch;
    }

    public void setDisplayInBiosketch(int displayInBiosketch)
    {
        this.displayInBiosketch = displayInBiosketch;
    }
}
