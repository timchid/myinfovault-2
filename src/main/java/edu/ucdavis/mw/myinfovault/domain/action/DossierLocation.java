/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierLocation.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

/**
 * This class represents the location or workflow node of a dossier in the workflow system.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public enum DossierLocation
{
    PACKETREQUEST ("Department (Packet Requested)"),
    DEPARTMENT ("Department"),
    SCHOOL ("School/College"),
    VICEPROVOST ("Vice Provost's Office"),
    SENATE_OFFICE ("Senate"),
    FEDERATION ("Federation"),
    SENATEFEDERATION ("Federation/Senate"),
    SENATEAPPEAL ("Senate Appeal"),
    FEDERATIONAPPEAL ("Federation Appeal"),
    FEDERATIONSENATEAPPEAL ("Federation/Senate Appeal"),
    POSTSENATESCHOOL ("School/College (Post Senate)"),
    POSTSENATEVICEPROVOST ("Vice Provost (Post Senate)"),
    POSTAPPEALSCHOOL ("School (Post Appeal)"),
    POSTAPPEALVICEPROVOST ("Vice Provost (Post Appeal)"),
    POSTAUDITREVIEW ("Post Audit"),
    READYFORPOSTREVIEWAUDIT ("Ready For Post Review Audit"),
    ARCHIVE ("Archive"),
    COMPLETE ("Complete"),
    UNKNOWN ("Unknown");

    private final String description;

    // Initialize the description.
    private DossierLocation(String description)
    {
        this.description = description;
    }


    /**
     * Returns the corresponding display string.
     *
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }


    /**
     * Map the MIV DossierLocation to the corresponding
     * kuali workflow node. The names returned must match the
     * definition in the workflow document configuration xml
     * file. (DossierDocument.xml)
     *
     * @return String representing the workflow node name
     */
    public String mapLocationToWorkflowNodeName()
    {
        switch (this)
        {
            case PACKETREQUEST:
                return "PacketRequest";
            case DEPARTMENT:
                return "Department";
            case SCHOOL:
                return "School";
            case VICEPROVOST:
                return "ViceProvost";
            case SENATE_OFFICE:
                return "Senate";
            case FEDERATION:
                return "Federation";
            case SENATEFEDERATION:
                return "SenateFederation";
            case SENATEAPPEAL:
                return "SenateAppeal";
            case FEDERATIONAPPEAL:
                return "FederationAppeal";
            case FEDERATIONSENATEAPPEAL:
                return "FederationSenateAppeal";
            case POSTAPPEALSCHOOL:
                return "PostAppealSchool";
            case POSTAPPEALVICEPROVOST:
                return "PostAppealViceProvost";
            case POSTSENATESCHOOL:
                return "PostSenateSchool";
            case POSTSENATEVICEPROVOST:
                return "PostSenateViceProvost";
            case POSTAUDITREVIEW:
                return "PostAuditReview";
            case READYFORPOSTREVIEWAUDIT:
                return "ReadyForPostReviewAudit";
            case ARCHIVE:
                return "Archive"; // Archive is the final node, same as complete
            case COMPLETE:
                return "Archive";
            default:
                return "Unknown";
        }
    }




    /**
     * Map a kuali workflow node to a MIV DossierLocation.
     *
     * @param workflowNodeName
     * @return ENUM representing the MIV DossierLocation
     */
    public static DossierLocation mapWorkflowNodeNameToLocation(String workflowNodeName)
    {
        if (!StringUtils.hasText(workflowNodeName)) return UNKNOWN;

        DossierLocation location = nameToLocationMap.get(workflowNodeName.toUpperCase());

        return location != null ? location : UNKNOWN;
    }
    private static final Map<String,DossierLocation> nameToLocationMap = new HashMap<String,DossierLocation>();
    static {
        nameToLocationMap.put(PACKETREQUEST.mapLocationToWorkflowNodeName().toUpperCase(), PACKETREQUEST);
        nameToLocationMap.put(DEPARTMENT.mapLocationToWorkflowNodeName().toUpperCase(), DEPARTMENT);
        nameToLocationMap.put(SCHOOL.mapLocationToWorkflowNodeName().toUpperCase(), SCHOOL);
        nameToLocationMap.put(VICEPROVOST.mapLocationToWorkflowNodeName().toUpperCase(), VICEPROVOST);
        nameToLocationMap.put(SENATE_OFFICE.mapLocationToWorkflowNodeName().toUpperCase(), SENATE_OFFICE);
        nameToLocationMap.put(FEDERATION.mapLocationToWorkflowNodeName().toUpperCase(), FEDERATION);
        nameToLocationMap.put(SENATEFEDERATION.mapLocationToWorkflowNodeName().toUpperCase(), SENATEFEDERATION);
        nameToLocationMap.put(POSTSENATESCHOOL.mapLocationToWorkflowNodeName().toUpperCase(), POSTSENATESCHOOL);
        nameToLocationMap.put(POSTSENATEVICEPROVOST.mapLocationToWorkflowNodeName().toUpperCase(), POSTSENATEVICEPROVOST);
        nameToLocationMap.put(SENATEAPPEAL.mapLocationToWorkflowNodeName().toUpperCase(), SENATEAPPEAL);
        nameToLocationMap.put(FEDERATIONAPPEAL.mapLocationToWorkflowNodeName().toUpperCase(), FEDERATIONAPPEAL);
        nameToLocationMap.put(FEDERATIONSENATEAPPEAL.mapLocationToWorkflowNodeName().toUpperCase(), FEDERATIONSENATEAPPEAL);
        nameToLocationMap.put(POSTAPPEALSCHOOL.mapLocationToWorkflowNodeName().toUpperCase(), POSTAPPEALSCHOOL);
        nameToLocationMap.put(POSTAPPEALVICEPROVOST.mapLocationToWorkflowNodeName().toUpperCase(), POSTAPPEALVICEPROVOST);
        nameToLocationMap.put(POSTAUDITREVIEW.mapLocationToWorkflowNodeName().toUpperCase(), POSTAUDITREVIEW);
        nameToLocationMap.put(READYFORPOSTREVIEWAUDIT.mapLocationToWorkflowNodeName().toUpperCase(), READYFORPOSTREVIEWAUDIT);
        nameToLocationMap.put(ARCHIVE.mapLocationToWorkflowNodeName().toUpperCase(), ARCHIVE);
    }


    /**
     * Get the "Office" that handles an action at the various workflow locations.
     * For example, all of the senate and federation work is done at the "SENATE_OFFICE"
     * @param location the location an action in workflow
     * @return the equivalent DossierLocation of the "Office"
     */
    public static DossierLocation mapLocationToOffice(DossierLocation location)
    {
        DossierLocation office = locationToOfficeMap.get(location);
        return office != null ? office : UNKNOWN;
    }
    private static final Map<DossierLocation, DossierLocation> locationToOfficeMap = new HashMap<>();
    static {
        locationToOfficeMap.put(PACKETREQUEST, PACKETREQUEST);


        locationToOfficeMap.put(DEPARTMENT, DEPARTMENT);


        locationToOfficeMap.put(SCHOOL, SCHOOL);
        locationToOfficeMap.put(POSTSENATESCHOOL, SCHOOL);
        locationToOfficeMap.put(POSTAPPEALSCHOOL, SCHOOL);


        locationToOfficeMap.put(SENATE_OFFICE, SENATE_OFFICE);
        locationToOfficeMap.put(FEDERATION, SENATE_OFFICE);
        locationToOfficeMap.put(SENATEFEDERATION, SENATE_OFFICE);
        // Appeal versions of the senate offices
        locationToOfficeMap.put(SENATEAPPEAL, SENATE_OFFICE);
        locationToOfficeMap.put(FEDERATIONAPPEAL, SENATE_OFFICE);
        locationToOfficeMap.put(FEDERATIONSENATEAPPEAL, SENATE_OFFICE);


        locationToOfficeMap.put(VICEPROVOST, VICEPROVOST);
        locationToOfficeMap.put(POSTSENATEVICEPROVOST, VICEPROVOST);
        locationToOfficeMap.put(POSTAPPEALVICEPROVOST, VICEPROVOST);


        // The rest map to themselves because they're different kinds of cases; more status based than location based.
        //locationToOfficeMap.put(POSTAUDITREVIEW, /*??*/ DossierLocation.UNKNOWN);
        //locationToOfficeMap.put(READYFORPOSTREVIEWAUDIT, /*??*/ DossierLocation.UNKNOWN);
        locationToOfficeMap.put(POSTAUDITREVIEW, POSTAUDITREVIEW);
        locationToOfficeMap.put(READYFORPOSTREVIEWAUDIT, READYFORPOSTREVIEWAUDIT);


        locationToOfficeMap.put(ARCHIVE, ARCHIVE);
        locationToOfficeMap.put(COMPLETE, COMPLETE);
    }
}
