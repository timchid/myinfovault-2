/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicAction.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Contains academic action type and delegation authority.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
public class AcademicAction implements Comparable<AcademicAction>, Serializable
{
    private static final long serialVersionUID = 201305021650L;

    /**
     * {@link MessageFormat} pattern used in {@link #toString()}.
     */
    private static final String TO_STRING = "{1}: {0}";

    /**
     * Effective date dossier description {@link MessageFormat} pattern used in {@link #getDescription()} and  {@link #getFullDescription()}.
     */
    private static final String ACTION_DESCRIPTION = "{0} {1,date,yyyy}";

    /**
     * Retroactive date dossier description {@link MessageFormat} pattern used in {@link #getDescription()}.
     */
    private static final String RETROACTIVE_DESCRIPTION = ACTION_DESCRIPTION + " (Retro {2,date,yyyy})";

    private final MivPerson candidate;
    private final DossierActionType actionType;
    private final DossierDelegationAuthority delegationAuthority;
    private final Date effectiveDate;

    private Date retroactiveDate = null;
    private Date endDate = null;
    private int accelerationYears;

    /**
     * Create a new academic action.
     *
     * @param candidate who belongs to the action
     * @param actionType dossier action type
     * @param delegationAuthority action delegation authority
     * @param effectiveDate action effective on this date
     * @param accelerationYears years accelerated
     * @throws IllegalArgumentException if delegation authority is not valid for the action type
     */
    public AcademicAction(MivPerson candidate,
                          DossierActionType actionType,
                          DossierDelegationAuthority delegationAuthority,
                          Date effectiveDate,
                          int accelerationYears) throws IllegalArgumentException
    {
        this.candidate = candidate;
        this.actionType = actionType;
        this.delegationAuthority = delegationAuthority;
        this.effectiveDate = effectiveDate;
        this.accelerationYears = accelerationYears;

        if (!actionType.isValid(delegationAuthority))
        {
            throw new IllegalArgumentException(this + " is not a legal dossier action");
        }

    }

    /**
     * Create an academic action.
     *
     * @param candidate candidate who belongs to the action
     * @param actionType dossier action type
     * @param delegationAuthority action delegation authority
     * @param effectiveDate action effective on this date
     * @param retroactiveDate action retroactively effective on this date
     * @param endDate date upon which action ends
     * @param accelerationYears years accelerated
     * @throws IllegalArgumentException if delegation authority is not valid for the action type
     */
    public AcademicAction(MivPerson candidate,
                          DossierActionType actionType,
                          DossierDelegationAuthority delegationAuthority,
                          Date effectiveDate,
                          Date retroactiveDate,
                          Date endDate,
                          int accelerationYears) throws IllegalArgumentException
    {
        this(candidate,
             actionType,
             delegationAuthority,
             effectiveDate,
             accelerationYears);

        this.retroactiveDate = retroactiveDate;
        this.endDate = endDate;
    }

    /**
     * @return Action candidate
     */
    public MivPerson getCandidate()
    {
        return this.candidate;
    }

    /**
     * @return dossier action type
     */
    public DossierActionType getActionType()
    {
        return actionType;
    }

    /**
     * @return action delegation authority
     */
    public DossierDelegationAuthority getDelegationAuthority()
    {
        return delegationAuthority;
    }

    /**
     * @return effective date
     */
    public Date getEffectiveDate()
    {
        return effectiveDate;
    }

    /**
     * @return retroactive date
     */
    public Date getRetroactiveDate()
    {
        return retroactiveDate;
    }

    /**
     * @param retroactiveDate retroactive date
     */
    public void setRetroactiveDate(Date retroactiveDate)
    {
        this.retroactiveDate = retroactiveDate;
    }

    /**
     * @return date upon which action ends
     */
    public Date getEndDate()
    {
        return endDate;
    }

    /**
     * @param endDate date upon which action ends
     */
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * @return accelerationYears for this action
     */
    public int getAccelerationYears()
    {
        return accelerationYears;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(TO_STRING, actionType.getDescription(), delegationAuthority.getDescription());
    }

    /**
     * Delegation authority, action type, and effective year, e.g.:
     * <blockquote>Non-Redelegated: Merit 2014</blockquote>
     *
     * @return delegation authority, action type, and effective year
     */
    public String getFullDescription()
    {
        return MessageFormat.format(ACTION_DESCRIPTION, this, effectiveDate);
    }

    /**
     * Action type, effective year, and retroactive year (if available), e.g.:
     * <blockquote>Merit 2014</blockquote>
     * And if there's a retroactive date:
     * <blockquote>Merit 2014 (Retro 2013)</blockquote>
     *
     * @return dossier description
     */
    public String getDescription()
    {
        return retroactiveDate != null
             ? MessageFormat.format(RETROACTIVE_DESCRIPTION, actionType.getDescription(), effectiveDate, retroactiveDate)
             : MessageFormat.format(ACTION_DESCRIPTION, actionType.getDescription(), effectiveDate);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(AcademicAction o)
    {
        int userComparison = this.candidate.getUserId() - o.candidate.getUserId();
        if (userComparison != 0) return userComparison;

        int actionTypeComparison = this.actionType.compareTo(o.actionType);
        if (actionTypeComparison != 0) return actionTypeComparison;

        int delegationAuthorityComparison = this.delegationAuthority.compareTo(o.delegationAuthority);
        if (delegationAuthorityComparison != 0) return delegationAuthorityComparison;

        return 0;
    }
}
