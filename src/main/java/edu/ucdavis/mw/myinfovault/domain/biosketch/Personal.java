/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Personal.java
 */

package edu.ucdavis.mw.myinfovault.domain.biosketch;

/**
 * @author dreddy
 * @since MIV 2.1
 */
public class Personal
{
    private String userId;
    private String displayName;
    private String birthDate;
    private boolean citizen;
    private String usCitizen;
    private String visaType;
    private String website;
    private String currentAddress;
    private String permanentAddress;
    private String officeAddress;
    private String permanentPhone;
    private String homePhone;
    private String cellPhone;
    private String officePhone;
    private String permanentFax = null;
    private String currentFax = null;
    private String officeFax = null;
    private String email;
    private String dateEntry;

    /**
     * @return
     */
    public String getBirthDate()
    {
        return birthDate;
    }

    /**
     * @param birthDate
     */
    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    /**
     * @return
     */
    public boolean isCitizen()
    {
        return citizen;
    }

    /**
     * @param citizen
     */
    public void setCitizen(boolean citizen)
    {
        this.citizen = citizen;
        if(citizen)
        {
            this.usCitizen = "Yes";
        }
        else
        {
            this.usCitizen = "No";
        }
    }

    /**
     * @return
     */
    public String getDisplayName()
    {
        return displayName;
    }

    /**
     * @param displayName
     */
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    /**
     * @return
     */
    public String getUserId()
    {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    /**
     * @return
     */
    public String getVisaType()
    {
        return visaType;
    }

    /**
     * @param visaType
     */
    public void setVisaType(String visaType)
    {
        this.visaType = visaType;
    }

    public String getCurrentAddress()
    {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress)
    {
        this.currentAddress = currentAddress;
    }

    public String getPermanentAddress()
    {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress)
    {
        this.permanentAddress = permanentAddress;
    }

    public String getOfficeAddress()
    {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress)
    {
        this.officeAddress = officeAddress;
    }

    public String getHomePhone()
    {
        return homePhone;
    }

    public void setHomePhone(String homePhone)
    {
        this.homePhone = homePhone;
    }

    public String getCellPhone()
    {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone)
    {
        this.cellPhone = cellPhone;
    }

    public String getOfficePhone()
    {
        return officePhone;
    }

    public void setOfficePhone(String officePhone)
    {
        this.officePhone = officePhone;
    }

    public String getPermanentFax()
    {
        return permanentFax;
    }

    public void setPermanentFax(String permanentFax)
    {
        this.permanentFax = permanentFax;
    }

    public String getCurrentFax()
    {
        return currentFax;
    }

    public void setCurrentFax(String currentFax)
    {
        this.currentFax = currentFax;
    }

    public String getOfficeFax()
    {
        return officeFax;
    }

    public void setOfficeFax(String officeFax)
    {
        this.officeFax = officeFax;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPermanentPhone()
    {
        return permanentPhone;
    }

    public void setPermanentPhone(String permanentPhone)
    {
        this.permanentPhone = permanentPhone;
    }


    public String getDateEntry()
    {
        return dateEntry;
    }

    public void setDateEntry(String dateEntry)
    {
        this.dateEntry = dateEntry;
    }

    public String getWebsite()
    {
        return website;
    }

    public void setWebsite(String website)
    {
        this.website = website;
    }

    public String getUsCitizen()
    {
        return usCitizen;
    }

    public void setUsCitizen(String usCitizen)
    {
        this.usCitizen = usCitizen;
    }

}
