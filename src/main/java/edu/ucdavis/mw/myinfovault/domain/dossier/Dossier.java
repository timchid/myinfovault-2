/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Dossier.java
 */

package edu.ucdavis.mw.myinfovault.domain.dossier;

import java.io.File;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PathConstructor;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * This class represents a Dossier in the workflow system. The Dossier object is
 * comprised of data from both the workflow system and MIV database.
 * @author rhendric
 *
 */
public class Dossier implements Serializable
{
    private static final long serialVersionUID = 20090326075502L;


    /**
     * Path constructor for dossier URLs.
     */
    protected static final PathConstructor pathConstructor = new PathConstructor();

    /**
     * {@link MessageFormat} pattern for the dossier URL used in {@link #getDossierPdfUrl(MivRole, int, int)}.
     */
    private static final String DOSSIER_URL = "{0}?roleId={1}&schoolId={2,number,#}&departmentId={3,number,#}";

    /**
     * Filename pattern for a dossier PDF .
     */
    private static final String FILE_PATTERN = "{0}_{1,number,#}_{2,number,#}.{3}";

    private int id;
    private int academicActionID;
    private final long dossierId;
    private MivPerson routingPerson = null;
    private boolean isBaseDataLoaded = false;
    private boolean isAttributeDataLoaded = false;
    private boolean isRecommendedActionFormPresent = false;
    private boolean isAcademicActionFormPresent = false;
    private int primarySchoolId;
    private int primaryDepartmentId;
    private Date submittedDate = null;
    private Date lastRoutedDate = null;
    private Date archivedDate = null;
    private Date completedDate = null;
    private AcademicAction action;
    private DossierReviewType reviewType;
    private String location = null;
    private List<String> previousLocations = new ArrayList<String>();
    private String workflowStatus = null;
    private Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> appointmentAttributes = new LinkedHashMap<>();


    /**
     * Create a new dossier.
     *
     * @param action
     * @param dossierId
     */
    private Dossier(AcademicAction action, long dossierId)
    {
        this.action = action;
        this.dossierId = dossierId;
    }

    /**
     * Constructs a Dossier WorkflowDocument representing a new document in
     * the workflow system. Creation/committing of the new document is accomplished
     * by the dossier service routeDocument/approveDocument methods.
     *
     * @param action
     * @param dossierId
     * @param annotation
     */
    public Dossier(AcademicAction action,
                   long dossierId,
                   String annotation)
    {
        this(action, dossierId);

        initializeAppointmentAttributeMap();
    }

    /**
     * Create dossier from database record.
     *
     * @param id
     * @param academicActionID
     * @param dossierId
     * @param primarySchoolId
     * @param primaryDepartmentId
     * @param submittedDate
     * @param lastRoutedDate
     * @param archivedDate
     * @param completedDate
     * @param action
     * @param reviewType
     * @param isRecommentedActionFormPresent
     * @param isAcademicActionFormPresent
     * @param location
     * @param previousLocations
     * @param workflowStatus
     */
    public Dossier(int id,
                   int academicActionID,
                   long dossierId,
                   int primarySchoolId,
                   int primaryDepartmentId,
                   Date submittedDate,
                   Date lastRoutedDate,
                   Date archivedDate,
                   Date completedDate,
                   AcademicAction action,
                   DossierReviewType reviewType,
                   boolean isRecommentedActionFormPresent,
                   boolean isAcademicActionFormPresent,
                   String location,
                   List<String> previousLocations,
                   String workflowStatus)
    {
        this(action, dossierId);

        this.id = id;
        this.academicActionID = academicActionID;
        this.primarySchoolId = primarySchoolId;
        this.primaryDepartmentId = primaryDepartmentId;
        this.submittedDate = submittedDate;
        this.lastRoutedDate = lastRoutedDate;
        this.archivedDate = archivedDate;
        this.completedDate = completedDate;
        this.reviewType = reviewType;
        this.location = location;
        this.previousLocations = previousLocations;
        this.workflowStatus = workflowStatus;
        this.isRecommendedActionFormPresent = isRecommentedActionFormPresent;
        this.isAcademicActionFormPresent = isAcademicActionFormPresent;
    }

    /**
     * @return dossier record ID
     */
    public int getId()
    {
        return id;
    }

    /**
     * @param id dossier record ID
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return academicAction ID
     */
    public int getAcademicActionID()
    {
        return this.academicActionID;
    }

    /**
     * @return dossier ID for this Dossier
     */
    public long getDossierId()
    {
        return this.dossierId;
    }

    /**
     * Get the a dossier attribute and create it if it doesn't exist.
     *
     * @param appointmentKey attributes appointment key
     * @param name name of attribute to retrieve
     * @return the dossier attribute for the given appointment and type
     */
    public DossierAttribute getDossierAttribute(DossierAppointmentAttributeKey appointmentKey, String name)
    {
        //get the map of all attributes associated with this dossier appointment
        Map<String, DossierAttribute> dossierAttributes = getAttributesByAppointment(appointmentKey).getAttributes();

        //get the attribute to update
        DossierAttribute attribute = dossierAttributes.get(name);

        //if the attribute is null
        if (attribute == null)
        {
            //create an attribute for this appointment
            attribute = new DossierAttribute(appointmentKey,
                                             MIVConfig.getConfig().getMap("attributetypesbyname").get(name),
                                             getDossierLocation(),
                                             getAction().getActionType());

            //add new attribute to the attributes map
            dossierAttributes.put(name,
                                  attribute);
        }

        return attribute;
    }

    /**
     * Get the workflow node location of this dossier.
     * ** NOTE - This call is a performance killer.
     * Only call when you absolutely need to and avoid calls in a loop.
     *
     * @return String location
     */
    public String getLocation()
    {
//        try
//        {
//            String[] nodes = workflowInfo.getCurrentNodeNames(this.getDossierId());
//            StringBuilder nodeStr = new StringBuilder();
//            for (String node : nodes)
//            {
//                nodeStr.append(node).append(" ");
//            }
//            this.location = nodeStr.toString().trim();
//        }
//        catch (WorkflowException wfe)
//        {
//            this.location = "Unknown";
//            log.log(Level.SEVERE, "Unable to determine current workflow location of dossier " + this.getDossierId(), wfe);
//        }
        return this.location;
    }

    /**
     * Static list of all review locations
     */
    private static final EnumSet<DossierLocation> allReviewCommittees = EnumSet.of(
            DossierLocation.FEDERATION,
            DossierLocation.SENATE_OFFICE,
            DossierLocation.SENATEFEDERATION,
            DossierLocation.FEDERATIONAPPEAL,
            DossierLocation.SENATEAPPEAL,
            DossierLocation.FEDERATIONSENATEAPPEAL
        );

    /**
     *  @return the dossier review committees of this dossier
     */
    public String getReviewCommittees()
    {
        StringBuilder locations = new StringBuilder();
        for (String location : this.previousLocations)
        {
            DossierLocation dossierLocation = DossierLocation.mapWorkflowNodeNameToLocation(location);
            if (Dossier.allReviewCommittees.contains(dossierLocation))
            {
                locations.append(dossierLocation.getDescription()).append(", ");
            }
        }

        return StringUtils.stripEnd(locations.toString(), ", ");
    }

    /**
     * @return the dossier location from the workflow node location of this dossier
     */
    public DossierLocation getDossierLocation()
    {
        return DossierLocation.mapWorkflowNodeNameToLocation(getLocation());
    }

    /**
     * @param location the workflow node location of this dossier
     */
    public void setLocation(String location)
    {
        this.location = location;
    }

    /**
     * @return the previous workflow node locations of this dossier
     */
    public List<String> getPreviousLocationsList()
    {
        return this.previousLocations;
    }

    /**
     * @return the previous workflow node locations of this dossier
     */
    public String getPreviousLocations()
    {
        StringBuilder locations = new StringBuilder();
        String locationsString = null;
        for (String location : this.previousLocations)
        {
            locations.append(location).append(",");
        }
        if (locations.length() != 0)
        {
            locationsString = locations.substring(0, locations.length()-1).toString();
        }
        return locationsString;
    }

    /**
     * @param previousLocations the previous workflow node locations of this dossier
     */
    public void setPreviousLocations(String previousLocations)
    {
        this.previousLocations.clear();
        if (previousLocations != null)
        {
            for (String location : previousLocations.split(","))
            {
                this.previousLocations.add(location);
            }
        }
    }

    /**
     * Add to the previous workflow location nodes of this dossier.
     *
     * @param location location to add
     */
    public void updatePreviousLocations(String location)
    {
        if (!previousLocations.contains(location))
        {
            this.getPreviousLocationsList().add(location);
        }
        else
        {
            // If the previouslocations contains location, remove it and any locations which may be left
            // beyond it
            int locationIdx = previousLocations.indexOf(location);
            if (locationIdx>0)
            {
                previousLocations = previousLocations.subList(0, locationIdx);
            }
            else
            {
                previousLocations.clear();
            }
        }
    }

    /**
     * Get the workflow node location description of the input dossier workflow node location.
     *
     * @param location the workflow node location for which to return a description
     * @return location description
     */
    public String getLocationDescription(String location)
    {
       return DossierLocation.mapWorkflowNodeNameToLocation(location).getDescription();
    }

    /**
     * @param workflowStatus the workflow status of this dossier
     */
    public void setWorkflowStatus(String workflowStatus)
    {
        this.workflowStatus = workflowStatus;
    }

    /**
     * Get the workflow status of this dossier
     * @return String workflowStatus
     */
    public String getWorkflowStatus()
    {
        return this.workflowStatus;
    }

    /**
     * Get the workflow status description of the input workflow status code
     * @param String - the workflow status code
     * @return String workflow status description
     * /
    public String getWorkflowStatusDescription(String workflowStatus)
    {
        return CodeTranslator.getRouteStatusLabel(workflowStatus);
    }*/

    /**
     * @param action dossier action (type, option, and delegation authority)
     */
    public void setAction(AcademicAction action)
    {
        this.action = action;
    }

    /**
     * @return dossier action (type, option, and delegation authority)
     */
    public AcademicAction getAction()
    {
        return action;
    }

    /* *
     * @return the DossierActionType for the dossier
     * @deprecated use {@link #getAction()} to access the action type
     * /
    @Deprecated
    public DossierActionType getActionType()
    {
        return this.action.getActionType();
    }
    */

    /* *
     * @return the DossierDelegationAuthority for the dossier
     * @deprecated use {@link #getAction()} to access the delegation authority
     * /
    @Deprecated
    public DossierDelegationAuthority getDelegationAuthority()
    {
        return this.action.getDelegationAuthority();
    }
    */

    /**
     * @return the delegation authority description for the current review type
     */
    public String getDelegationAuthorityDescription()
    {
        return this.action.getDelegationAuthority().getDescription(reviewType);
    }

    /**
     * @param reviewType the DossierReviewType for the dossier
     */
    public void setReviewType(DossierReviewType reviewType)
    {
        this.reviewType = reviewType;
    }

    /**
     * Get the DossierReviewType for the dossier
     * @return DossierReviewType reviewType
     */
    public DossierReviewType getReviewType()
    {
        return reviewType;
    }

    /**
     * Get the entire attribute set (all schools and departments) for the dossier
     * @return Map<DossierAppointmentAttributeKey, DossierAttributeMap> a map of all attributes by school and department
     */
    public Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> getAppointmentAttributes()
    {
        return appointmentAttributes;
    }

    /**
     * Get attributes for the input school and department key
     * @param appointmentAttributeKey
     * @return DossierAttributeMap
     */
    public DossierAppointmentAttributes getAttributesByAppointment(DossierAppointmentAttributeKey appointmentAttributeKey)
    {
        return this.appointmentAttributes.get(appointmentAttributeKey);
    }

    /**
     * Get the attribute for the given appointment and name.
     *
     * @param appointmentAttributeKey school and department key in the form "school:department"
     * @param attributeName  the name of the attribute
     * @return dossier attribute map containing all data associated with the attribute or <code>null</code> if doesn't exist
     */
    public DossierAttribute getAttributeByAppointment(DossierAppointmentAttributeKey appointmentAttributeKey,
                                                      String attributeName)
    {
        DossierAppointmentAttributes schoolDepartmentAttributes = getAttributesByAppointment(appointmentAttributeKey);

        return schoolDepartmentAttributes != null
             ? schoolDepartmentAttributes.getAttributes().get(attributeName)
             : null;
    }

    /**
     * Set the IsComplete flag for this appointment
     * @param appointmentAttributeKey
     * @param isComplete
     */
    public void setIsAppointmentComplete(DossierAppointmentAttributeKey appointmentAttributeKey, boolean isComplete)
    {
        getAttributesByAppointment(appointmentAttributeKey).setComplete(isComplete);
    }

    /**
     * Get the IsComplete flag for this appointment
     * @param appointmentAttributeKey
     * @return if the appointment is complete
     */
    public boolean isAppointmentComplete(DossierAppointmentAttributeKey appointmentAttributeKey)
    {
        return getAttributesByAppointment(appointmentAttributeKey).isComplete();
    }

    /**
     * get boolean indicating if the dossier base data has been loaded.
     * @return boolean true if the data has been loaded, false otherwise.
     */
    public boolean isBaseDataLoaded()
    {
        return isBaseDataLoaded;
    }

    /**
     * Set boolean indicating if the dossier base data has been loaded.
     * @param isBaseDataLoaded <code>true</code> if added, <code>false</code> otherwise
     */
    public void setBaseDataLoaded(boolean isBaseDataLoaded)
    {
        this.isBaseDataLoaded = isBaseDataLoaded;
    }

    /**
     * Get boolean indicating if the dossier attributes have been loaded.
     * @return <code>true</code> if added, <code>false</code> otherwise
     */
    public boolean isAttributedataLoaded()
    {
        return isAttributeDataLoaded;
    }

    /**
     * Set boolean indicating if the dossier attributes have been loaded.
     * @param isAttributedataLoaded <code>true</code> if loaded, <code>false</code> otherwise
     */
    public void setAttributedataLoaded(boolean isAttributedataLoaded)
    {
        this.isAttributeDataLoaded = isAttributedataLoaded;
    }

    /**
     * Get the attributes for the provided school department key in the form "school:department"
     * @return Map<String, DossierAttribute> Map of attribute data by attribute type name.
     */
    public Map<String, DossierAttribute> getPrimaryDepartmentAttributes()
    {
        return this.appointmentAttributes.get(this.getPrimaryAppointmentKey()).getAttributes();
    }

    /**
     * Set the attributes for the provided school and department
     * @param schoolId
     * @param deptId
     * @param primary
     */
    public void setAppointmentAttribute(int schoolId, int deptId, boolean primary)
    {
        DossierAppointmentAttributeKey appointmentAttributeKey = new DossierAppointmentAttributeKey(schoolId, deptId);
        this.appointmentAttributes.put(appointmentAttributeKey, new DossierAppointmentAttributes(schoolId, deptId, primary));
    }

    /**
     * Set the attributes for the provided DossierAppointmentAttributeKey
     * @param appointmentAttributeKey
     * @param primary
     */
    public void setAppointmentAttribute(DossierAppointmentAttributeKey appointmentAttributeKey, boolean primary)
    {
        this.appointmentAttributes.put(appointmentAttributeKey,
                new DossierAppointmentAttributes(appointmentAttributeKey.getSchoolId(), appointmentAttributeKey.getDepartmentId(), primary));
    }

    /**
     * Get the primaryAppointmentKey
     * @return DossierAppointmentAttributeKey
     */
    public DossierAppointmentAttributeKey getPrimaryAppointmentKey()
    {
        return getAppointmentKey(this.getPrimarySchoolId(),this.getPrimaryDepartmentId());
    }

    /**
     * @param schoolId
     * @param departmentId
     * @return the dossier appointment attribute key
     */
    public static DossierAppointmentAttributeKey getAppointmentKey(int schoolId, int departmentId)
    {
        return new DossierAppointmentAttributeKey(schoolId, departmentId);
    }

    /**
     * Set a dossier appointment attribute to a new status.
     *
     * @param attributeName name of attribute to set
     * @param attributeStatus new status to set for the dossier attribute
     * @param schoolId appointment school ID
     * @param departmentId appointment department ID
     * @return if change was made
     */
    public boolean setAttributeStatus(String attributeName,
                                      DossierAttributeStatus attributeStatus,
                                      int schoolId,
                                      int departmentId)
    {
        //get the attribute to update
        DossierAttribute attribute = getDossierAttribute(Dossier.getAppointmentKey(schoolId,
                                                                                   departmentId),
                                                         attributeName);

        //proceed if the new status is different from the current
        if (attributeStatus != attribute.getAttributeValue())
        {
            //set the dossier attribute to the new status value
            attribute.setAttributeValue(attributeStatus);

            return true;// changed
        }

        return false;// no change
    }

    /**
     * Set boolean indicating if the recommended action form has been added.
     * @param isRecommendedActionFormAdded <code>true</code> if present, <code>false</code> otherwise
     */
    public void setRecommendedActionFormPresent(boolean isRecommendedActionFormAdded)
    {
        this.isRecommendedActionFormPresent = isRecommendedActionFormAdded;
    }

    /**
     * Get boolean indicating if the recommended action form has been added.
     * @return boolean true of added false otherwise.
     */
    public boolean isRecommendedActionFormPresent()
    {
        return isRecommendedActionFormPresent;
    }

    /**
     * Set boolean indicating if the academic action form has been added.
     * @param isAcademicActionFormAdded <code>true</code> if present, <code>false</code> otherwise
     */
    public void setAcademicActionFormPresent(boolean isAcademicActionFormAdded)
    {
        this.isAcademicActionFormPresent = isAcademicActionFormAdded;
    }

    /**
     * Get boolean indicating if the recommended action form has been added.
     * @return boolean true of added false otherwise.
     */
    public boolean isAcademicActionFormPresent()
    {
        return isAcademicActionFormPresent;
    }


    /**
     * @param primarySchoolId the primary school ID
     */
    public void setPrimarySchoolId(int primarySchoolId)
    {
        this.primarySchoolId = primarySchoolId;
    }

    /**
     * @return the primary school ID
     */
    public int getPrimarySchoolId()
    {
        return primarySchoolId;
    }

    /**
     * @param primaryDepartmentId the primary department ID
     */
    public void setPrimaryDepartmentId(int primaryDepartmentId)
    {
        this.primaryDepartmentId = primaryDepartmentId;
    }

    /**
     * @return the primary department ID
     */
    public int getPrimaryDepartmentId()
    {
        return primaryDepartmentId;
    }

    /**
     * Get the workflow DocumentTypelId of the Dossier
     * @return
     * /
    public Long getDocumentTypeId()
    {
        return documentTypeId;
    }*/

    /**
     * getUserId
     * @return int userId
     * @deprecated use {@link #getAction()#getCandidate()#getUserId()} instead
     */
    @Deprecated
    public int getUserId()
    {
        return this.action.getCandidate().getUserId();
    }

    /**
     * @return the candidate for this dossier
     * @deprecated use {@link #getAction()#getCandidate()} instead
     */
    @Deprecated
    public MivPerson getCandidate()
    {
        return this.action.getCandidate();
    }

    /**
     * @return dossier routing person
     */
    public MivPerson getRoutingPerson()
    {
        return this.routingPerson;
    }

    /**
     * @param routingPerson dossier routing Person
     * @return self-reference for chaining
     */
    public Dossier setRoutingPerson(MivPerson routingPerson)
    {
        this.routingPerson = routingPerson;
        return this;
    }

    /**
     * setSubmittedDate
     * @param submittedDate
     */
    public void setSubmittedDate(Date submittedDate)
    {
        this.submittedDate = submittedDate;
    }

    /**
     * getSubmittedDate
     * @return submittedDate
     */
    public Date getSubmittedDate()
    {
        return this.submittedDate;
    }

    /**
     * setLastRoutedDate
     * @param lastRoutedDate
     */
    public void setLastRoutedDate(Date lastRoutedDate)
    {
        this.lastRoutedDate = lastRoutedDate;
    }

    /**
     * getLastRoutedDate
     * @return lastRoutedDate
     */
    public Date getLastRoutedDate()
    {
        return this.lastRoutedDate;
    }

    /**
     * setArchivedDate
     * @param archivedDate
     */
    public void setArchivedDate(Date archivedDate)
    {
        this.archivedDate = archivedDate;
    }

    /**
     * getArchivedDate
     * @return archivedDate
     */
    public Date getArchivedDate()
    {
        return this.archivedDate;
    }

    /**
     * Get the date when the action for this Dossier was completed.
     * This is essentially when the dossier was moved to the archive queue (the <em>ReadyToArchive</em> state).
     * @return completeDate
     */
    public Date getCompletedDate()
    {
        return this.completedDate;
    }

    /**
     * @param completedDate completed date
     */
    public void setCompletedDate(Date completedDate)
    {
        this.completedDate = completedDate;
    }

    /**
     * @return The dossier directory path for this user
     */
    public File getDossierDirectory()
    {
        // Get the relative loation of the dossier directory
        Properties documentProperties = PropertyManager.getPropertySet("document", "config");
        String relativeDossierLocation = documentProperties.getProperty("location-dossier");
        if (relativeDossierLocation == null)
        {
            relativeDossierLocation = "dossier";
        }
        // Get the user directory and add the relative dossier location
        File userDirectory = new File(MIVConfig.getConfig().getUserDir(this.getUserId()),relativeDossierLocation);
        // Add the last portion of the path which is "d" plus the dossier id
        return new File(userDirectory,"d"+this.getDossierId());
    }

    /**
     * Files are stored in a sub-directory named for the document type extension. This
     * method will return a file object of the complete path taking into consideration
     * the DocumentFormat.
     *
     * @param documentFormat format of the document for which to get the directory
     * @return the dossier directory path for this user
     */
    public File getDossierDirectoryByDocumentFormat(DocumentFormat documentFormat)
    {
        return new File(this.getDossierDirectory(), documentFormat.getFileExtension());
    }

    /**
     * @return file instance of the dossier PDF.
     */
    public File getDossierPdf()
    {
        return new File(this.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF), "dossier.pdf");
    }

    /**
     * Get a file instance of a dossier PDF for the given type and primary scope.
     *
     * @param documentType document type
     * @return dossier document PDF file instance
     */
    public File getDossierPdf(MivDocument documentType)
    {
        return getDossierPdf(documentType, primarySchoolId, primaryDepartmentId);
    }

    /**
     * Get a file instance of a dossier PDF for the given type and scope.
     *
     * @param documentType document type
     * @param schoolId school ID
     * @param departmentId department ID
     * @return dossier document PDF file instance
     */
    public File getDossierPdf(MivDocument documentType, int schoolId, int departmentId)
    {
        return new File(getDossierDirectoryByDocumentFormat(DocumentFormat.PDF),
                        MessageFormat.format(FILE_PATTERN,
                                             documentType.getDossierFileName(),
                                             schoolId,
                                             departmentId,
                                             DocumentFormat.PDF.getFileExtension()));
    }

    /**
     * Get the dossier PDF URL qualified by role and scope.
     *
     * @param role MIV role
     * @param schoolId ID of the school
     * @param departmentId ID of the department
     * @return URL representation of the dossier PDF
     */
    public String getDossierPdfUrl(MivRole role,
                                   int schoolId,
                                   int departmentId)
    {
        return MessageFormat.format(DOSSIER_URL,
                                    pathConstructor.getUrlFromPath(this.getDossierPdf()),
                                    role,
                                    schoolId,
                                    departmentId);
    }

    /**
     * getArchiveDirectory
     * @return File - The archive directory path for this user
     */
    public File getArchiveDirectory()
    {
        // Get the relative loation of the dossier directory
        Properties documentProperties = PropertyManager.getPropertySet("document", "config");
        String archiveLocation = documentProperties.getProperty("archivelocation");
        if (archiveLocation == null)
        {
            archiveLocation = "/opt/ucd/mivarchives";
        }
        String relativeDossierLocation = documentProperties.getProperty("location-dossier");

        // Get the location of the dossier directory relative to the user directory
        if (relativeDossierLocation == null)
        {
            relativeDossierLocation = "dossier";
        }
        // Get the user directory...only the final name in the path sequence
        String userDirectoryName = MIVConfig.getConfig().getUserDir(this.getUserId()).getName();
        // Add the last portion of the path which is "d" plus the dossier id
        return new File(archiveLocation+"/"+userDirectoryName+"/"+relativeDossierLocation,"d"+this.getDossierId());
    }

    /**
     * Files are stored in a subdirectory named for the document type extension. This
     * method will return a file object of the complete path taking into consideration
     * the DocumentFormat.
     *
     * @param documentFormat format of the document for which to get the directory
     * @return the archive directory path for this user
     */
    public File getArchiveDirectoryByDocumentFormat(DocumentFormat documentFormat)
    {
        return new File(this.getArchiveDirectory(), documentFormat.getFileExtension());
    }

    /**
     * Initialize map for all appointments for the current user's dossier.
     */
    public void initializeAppointmentAttributeMap()
    {
        this.appointmentAttributes.clear();

        if (this.action.getCandidate().getPrimaryRoleType() == MivRole.CANDIDATE ||
            this.action.getCandidate().hasRole(MivRole.APPOINTEE))
        {
            Collection <Appointment> appointments = this.action.getCandidate().getAppointments();
            // Get the primary appointment in the map first
            for (Appointment appointment : appointments)
            {
                if (appointment.isPrimary())
                {
                    final Scope scope = appointment.getScope();
                    this.setAppointmentAttribute(scope.getSchool(), scope.getDepartment(), appointment.isPrimary());
                    this.setPrimarySchoolId(scope.getSchool());
                    this.setPrimaryDepartmentId(scope.getDepartment());
                    break;
                }
            }

            // Get the rest
            for (Appointment appointment : appointments)
            {
                // Already have primary in map
                if (appointment.isPrimary()) {
                    continue;

                }
                this.setAppointmentAttribute(appointment.getScope().getSchool(), appointment.getScope().getDepartment(), appointment.isPrimary());
            }
        }
        // Non CANDIDATE roles do not have appointments and will not normally be routing a dossier, but we should allow them to do so anyway for testing
        else
        {
            // Get the schoolId and DepartmentId from the MIV_USER assigned role in order to set an appointment attribute
            for (AssignedRole assignedRole : this.action.getCandidate().getAssignedRoles())
            {
                // Appointee dosen't have MIV_User Role
                if (assignedRole.getRole() == MivRole.MIV_USER || assignedRole.getRole() == MivRole.APPOINTEE)
                {
                    int schoolId = assignedRole.getScope().getSchool();
                    int departmentId = assignedRole.getScope().getDepartment();
                    this.setAppointmentAttribute(schoolId, departmentId, true);
                    this.setPrimarySchoolId(schoolId);
                    this.setPrimaryDepartmentId(departmentId);
                    break;
                }
            }
        }
    }

    /**
     * @return the dossier primary department attribute map.
     */
    public Map<String, String> getPrimaryDepartment()
    {
        for (Map<String, String> department : this.getDepartments())
        {
            if (Boolean.parseBoolean(department.get("primary")))
            {
                return department;
            }
        }

        throw new MivSevereApplicationError("No primary department found for dossier " + dossierId);
    }

    /**
     * @return map of appointment departments for this Dossier
     */
    public List<Map<String, String>> getDepartments()
    {
        List <Map<String, String>> departmentMapList = new ArrayList <Map<String, String>>();

        Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> appointmentAttributes = this.getAppointmentAttributes();
        for (DossierAppointmentAttributeKey key : appointmentAttributes.keySet())
        {
            departmentMapList.add(appointmentAttributes.get(key).getDepartmentMap());
        }
        return departmentMapList;
    }

    /**
     * getDepartmentsString - Gets a comma separated string of all appointment department/schools for this dossier in
     * alphabetic order
     *
     * @return String
     */
    public String getDepartmentsString()
    {
        // Get the departments for this dossier
        Map <String, String> departmentNames = new HashMap<String, String>();
        String schoolDepartmentName = "";

        // Build a map with school and department descriptions
        for (Map<String,String>schoolDepartment : this.getDepartments())
        {
            schoolDepartmentName = StringUtils.isBlank(schoolDepartment.get("description")) ? schoolDepartment.get("school") : schoolDepartment.get("school")+" - "+schoolDepartment.get("description");
            departmentNames.put(schoolDepartmentName,schoolDepartmentName);
        }

        // Sort
        departmentNames = new TreeMap<String, String>(departmentNames);

        // Build a comma separated string
        StringBuilder departments = new StringBuilder();
        for (String departmentName : departmentNames.keySet())
        {
            departments.append(departmentName).append(", ");
        }

       departments.delete(departments.length()-2, departments.length());
       return departments.toString();
    }

    /**
     * getDepartmentList - Gets list of all appointment department/schools for this dossier in
     * alphabetic order
     *
     * @return List
     */
    public List<String>getDepartmentList()
    {
        // Get the departments for this dossier
        Map <String, String> departmentNames = new HashMap<String, String>();
        String schoolDepartmentName = "";

        // Build a map with school and department descriptions
        for (Map<String,String>schoolDepartment : this.getDepartments())
        {
            schoolDepartmentName = StringUtils.isBlank(schoolDepartment.get("description")) ? schoolDepartment.get("school") : schoolDepartment.get("school")+" - "+schoolDepartment.get("description");
            departmentNames.put(schoolDepartmentName,schoolDepartmentName);
        }

        // Sort
        departmentNames = new TreeMap<String, String>(departmentNames);

        // Build a list of departments
        List<String> departments = new ArrayList<String>();
        for (String departmentName : departmentNames.keySet())
        {
            departments.add(departmentName);
        }
       return departments;
    }

    /**
     * areJointAppointmentsCompleted - gets a list of all appointments and determines if all joint appointments are completed
     *                            will return "N/A" if there are no joint appointments
     *                            otherwise returns either "In Progress" or "Completed"
     * @return String
     */
    public String areJointAppointmentsCompleted()
    {
        String status = "N/A";
        if (this.isAttributeDataLoaded)
        {
            Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> appointments = this.getAppointmentAttributes();
            if (appointments.size()>1) {
                for (DossierAppointmentAttributeKey apptKey : appointments.keySet()) {
                 // See if this is a joint appointment and if it has been marked as complete (except at Vice Provost)
                    if (!appointments.get(apptKey).isComplete() &&        // not complete
                            !appointments.get(apptKey).isPrimary() &&     // not primary
                            !appointments.get(apptKey).getAttributes().isEmpty() &&     // there must be attributes
                            !DossierLocation.VICEPROVOST.mapLocationToWorkflowNodeName().equalsIgnoreCase(this.getLocation()))   // must not be at ViceProvost location{
                    {
                        status = "incomplete";
                    }
                }
                status = status=="incomplete" ? "In Progress":"Completed";
            }
        }

        return status;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Long.valueOf(dossierId).hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Dossier)) return false;
        Dossier other = (Dossier) obj;
        if (dossierId != other.dossierId) return false;
        return true;
    }

}
