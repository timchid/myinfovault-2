/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Step.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * Assignment title step.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.4.1
 */
public class Step implements Serializable
{
    private static final long serialVersionUID = 201410011124L;

    /**
     * {@link MessageFormat} pattern used in {@link #toString()}.
     */
    private static final String TO_STRING = "{0,choice,-1#AS|0#NA|0<'{0,number,#0.#}'}";

    /**
     * Pattern matches step entries considered above scale.
     *
     * @see <a href="https://www.debuggex.com/r/AdVFX3GrvBLOrAiS">Pattern at Debuggex</a>
     */
    private static final Pattern ABOVE_SCALE = Pattern.compile("^\\s*(?:A(/?S)?|-1(\\.0*)?)\\s*$");

    /**
     * Pattern matches step entries considered not applicable.
     *
     * @see <a href="https://www.debuggex.com/r/AkovwPuaqef5lE5c">Pattern at Debuggex</a>
     */
    private static final Pattern NOT_APPLICABLE = Pattern.compile("^\\s*(?:N(/?A)?|0+(\\.0*)?)\\s*$");

    /**
     * Pattern matches leading and trailing whitespace, leading zeros,
     * and trailing zeros (if following a decimal point).
     */
    private static final Pattern TRIM = Pattern.compile("^[\\s|0]*|(?<=(?<=\\.).*)[0|\\s]+$|\\s*$");

    private final BigDecimal value;

    /**
     * Create an assignment title step for the given value. Acceptable values are:
     * <ul>
     * <li>'A', 'AS', 'A/S' or -1 (denoting step is above scale)</li>
     * <li>'N', 'NA', 'N/A', or 0 (denoting step is not applicable)</li>
     * <li>A representation of a number palatable to {@link BigDecimal#BigDecimal(String)}</li>
     * </ul>
     *
     *
     * @param value step value
     * @throws NumberFormatException if the value is unacceptable
     */
    public Step(String value) throws NumberFormatException
    {
        if (StringUtils.isBlank(value))
        {
            this.value = null;
        }
        else if (ABOVE_SCALE.matcher(value).find())
        {
            this.value = BigDecimal.ONE.negate();
        }
        else if (NOT_APPLICABLE.matcher(value).find())
        {
            this.value = BigDecimal.ZERO;
        }
        else
        {
            /*
             * Trimming insignificant zeros and whitespace so that the resultant decimal
             * has the smallest scale required to represent the given value.
             */
            this.value = new BigDecimal(TRIM.matcher(value).replaceAll(StringUtils.EMPTY));
        }
    }

    /**
     * @return step numeric value
     */
    public BigDecimal getValue()
    {
        return this.value;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return this.value == null
             ? StringUtils.EMPTY
             : MessageFormat.format(TO_STRING, this.value);
    }
}
