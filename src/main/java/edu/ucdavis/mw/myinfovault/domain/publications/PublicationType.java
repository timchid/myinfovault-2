/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationType.java
 */

package edu.ucdavis.mw.myinfovault.domain.publications;

/**
 * Defines MIV publication type.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public enum PublicationType
{
    /**
     * TODO: javadoc
     */
    ABSTRACT(1, "Abstract"),

    /**
     * TODO: javadoc
     */
    JOURNAL(2, "Journal"),

    /**
     * TODO: javadoc
     */
    BOOK_AUTHORED(3, "Book Authored"),

    /**
     * TODO: javadoc
     */
    BOOK_EDITED(4, "Book Edited"),

    /**
     * TODO: javadoc
     */
    BOOK_CHAPTER(5, "Book Chapter"),

    /**
     * TODO: javadoc
     */
    BOOK_REVIEWED(6, "Book Reviewed"),

    /**
     * TODO: javadoc
     */
    LETTER_TO_THE_EDITOR(7, "Letter to the Editor"),

    /**
     * TODO: javadoc
     */
    LIMITED_DISTRIBUTION(8, "Limited Distribution"),

    /**
     * TODO: javadoc
     */
    ALTERNATIVE_MEDIA(9, "Alternative Media");

    private int id;
    private String description;

    private PublicationType(int id, String description)
    {
        this.id = id;
        this.description = description;
    }

    /**
     * @return Identifier associated with the publication type
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return Publication type description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Get the publication type value associated with the given ID.
     *
     * @param id Identifier associated with the publication type
     * @return The associated publication type
     * @throws IllegalArgumentException If no type associated with the ID
     */
    public static PublicationType valueOf(int id) throws IllegalArgumentException
    {
        for (PublicationType publicationType : PublicationType.values())
        {
            if (publicationType.getId() == id) return publicationType;
        }

        throw new IllegalArgumentException("No PublicationType exists for the ID '" + id + "'.");
    }
}
