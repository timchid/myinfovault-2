/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignableScopeFilter.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import edu.ucdavis.mw.myinfovault.service.ScopeFilter;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Filters out signable documents with the given school and department IDs.
 *
 * @author Craig Gilmore
 * @since 4.3
 * @param <T> Signable document by which to filter
 */
public class SignableScopeFilter<T extends Signable> extends ScopeFilter<T>
{
    /**
     * @param schoolId school ID belonging to each department of the filtered collection
     * @param departmentId department ID belonging to each department of the filtered collection
     */
    public SignableScopeFilter(int schoolId, int departmentId)
    {
        super(schoolId, departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.ScopeFilter#getScope(java.lang.Object)
     */
    @Override
    protected Scope getScope(T item)
    {
        return new Scope(item.getSchoolId(), item.getDepartmentId());
    }
}
