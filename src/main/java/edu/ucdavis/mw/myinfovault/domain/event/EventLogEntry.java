/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventLogEntry.java
 */

package edu.ucdavis.mw.myinfovault.domain.event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.events2.EventCategory;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * Event log entry (DTO).
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class EventLogEntry implements Serializable
{
    private static final long serialVersionUID = 201307231432L;

    private final byte[] id;
    private final byte[] parentId;
    private final String title;
    private final MivPerson realUser;
    private final MivPerson shadowUser;
    private final long posted;
    private final String comment;
    private final Map<EventCategory, Object> details;

    /**
     * Construct an event log entry.
     *
     * Event details mapped thus:
     * <ul>
     * <li>{@link EventCategory#ROUTE ROUTE} &#8594; {@link edu.ucdavis.mw.myinfovault.domain.action.Route Dossier location route}</li>
     * <li>{@link EventCategory#DECISION DECISION} &#8594; {@link edu.ucdavis.mw.myinfovault.domain.decision.DecisionType Decision choice}</li>
     * <li>{@link EventCategory#SIGNATURE SIGNATURE} &#8594; {@link MivDocument document signed/requested}</li>
     * <li>{@link EventCategory#REQUEST REQUEST} &#8594; {@link MivPerson One Requested}</li>
     * <li>{@link EventCategory#UPLOAD UPLOAD} &#8594; {@link edu.ucdavis.myinfovault.MIVConfig#getMap(String) MIV config "documents" map entry}</li>
     * <li>{@link EventCategory#USER USER} &#8594; {@link MivPerson Target User}</li>
     * <li>All other categories are associated with <code>null</code></li>
     * </ul>
     *
     * @param id binary representing the UUID of the event
     * @param title event title
     * @param realUser the real, logged-in user
     * @param shadowUser the proxied user
     * @param posted date event was posted
     * @param comment
     * @param details detail for each event log category
     * @param parentId binary representing the UUID of the parent event or <code>null</code> if no parent
     */
    public EventLogEntry(byte[] id,
                         String title,
                         MivPerson realUser,
                         MivPerson shadowUser,
                         long posted,
                         String comment,
                         Map<EventCategory, Object> details,
                         byte[] parentId)
    {
        this.id = id;
        this.title = title;
        this.realUser = realUser;
        this.posted = posted;
        this.comment = comment;
        this.details = details;
        this.parentId = parentId;

        /*
         * If the real user is the same as the shadow user, there was no proxy.
         */
        this.shadowUser = realUser.equals(shadowUser) ? null : shadowUser;
    }

    /**
     * @return binary representing the UUID of the event.
     */
    public byte[] getId() {
        return id;
    }

    /**
     * @return binary representing the UUID of the parent event or <code>null</code> if no parent.
     */
    public byte[] getParentId() {
        return parentId;
    }

    /**
     * @return event title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the real, logged-in user
     */
    public MivPerson getRealUser() {
        return realUser;
    }

    /**
     * @return the proxied user or <code>null</code> if real user was acting as self
     */
    public MivPerson getShadowUser() {
        return shadowUser;
    }

    /**
     * @return raw date in long when event was posted
     */
    public long getPosted() {
        return posted;
    }

    /**
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     * @return detail object for the given event log category
     */
    @SuppressWarnings("unchecked")
    public <T> T getDetail(EventCategory category) {
        return (T) details.get(category);
    }

    /**
     * @return event log categories in order declared in {@link Enum} declaration.
     */
    public Set<EventCategory> getCategories() {
        return EnumSet.copyOf(details.keySet());
    }

    /**
     * @return if this is a child event log entry
     */
    public boolean isChild() {
        return parentId != null;
    }

    /**
     * @return the String representations of the details for this event log entry.
     */
    public List<String> getDetails()
    {
        List<String> details = new ArrayList<String>();

        for (EventCategory category : getCategories())
        {
            String representation = getDetailRepresentation(category);

            if (StringUtils.hasText(representation)) details.add(representation);
        }

        return details;
    }

    /**
     * Get the String representation of the detail for the given category.
     *
     * @param category detail event category
     * @return category detail representation
     */
    private String getDetailRepresentation(EventCategory category)
    {
        Object o = getDetail(category);

        if (o == null) return StringUtil.EMPTY_STRING;

        switch (category)
        {
            case SIGNATURE:
                return ((MivDocument)o).getDescription();
            case HOLD:
            case RELEASE:
                return ((DossierLocation)o).getDescription();
            default:
                return o.toString();
        }
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("EventLogEntry [realUser=").append(realUser)
               .append(", shadowUser=").append(shadowUser)
               .append(", title=").append(title)
               .append(", posted=").append(new Date(posted))
               .append(", comment=").append(comment)
               .append(", details=").append(details).append("]");
        return builder.toString();
    }
}
