/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupType.java
 */

package edu.ucdavis.mw.myinfovault.domain.group;

/**
 * The type to be associated with an MIV group.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public enum GroupType
{
    /**
     * Default group type
     */
    GROUP(0),

    /**
     * Assigned to dossier review
     */
    REVIEW(1);

    private int id;

    /**
     * The ID associated with the group type in the database.
     *
     * @return the group type ID
     */
    public int getId()
    {
        return id;
    }

    private GroupType(int id)
    {
        this.id = id;
    }
}
