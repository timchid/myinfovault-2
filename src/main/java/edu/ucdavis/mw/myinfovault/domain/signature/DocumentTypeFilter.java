/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentTypeFilter.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Filters signable documents by the given document types.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class DocumentTypeFilter extends TypeFilter<Signable>
{
    /**
     * @param types document types each item of the filtered collection may be
     */
    public DocumentTypeFilter(MivDocument...types)
    {
        super(types);
    }
    
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.TypeFilter#getDocumentType(java.lang.Object)
     */
    @Override
    MivDocument getDocumentType(Signable item)
    {
        return item.getDocumentType();
    }
}
