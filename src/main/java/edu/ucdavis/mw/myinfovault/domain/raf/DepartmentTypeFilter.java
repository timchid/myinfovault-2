/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DepartmentTypeFilter.java
 */

package edu.ucdavis.mw.myinfovault.domain.raf;

import java.util.Arrays;

import edu.ucdavis.mw.myinfovault.domain.raf.Department.DepartmentType;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters out departments with the given department type.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class DepartmentTypeFilter extends SearchFilterAdapter<Department>
{
    private final DepartmentType[] types;

    /**
     * @param types department types each item of the filtered collection may have
     */
    public DepartmentTypeFilter(DepartmentType...types)
    {
        Arrays.sort(types);

        this.types = types;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(Department item)
    {
        return Arrays.binarySearch(types, item.getDepartmentType()) > -1;
    }
}
