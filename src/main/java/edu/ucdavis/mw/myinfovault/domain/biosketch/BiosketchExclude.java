package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * Domain Object representing Biosketch Exclude
 * @author Brij Garg
 * @since MIV 2.1
 */
public class BiosketchExclude extends DomainObject
{
    public enum ExcludeStatus { EXISTING, ADDED, DELETED };

    private int biosketchID;
    private String recType;
    private int recordID;
//    private String indicator = "same";
    private ExcludeStatus status = ExcludeStatus.EXISTING;

    public BiosketchExclude(int id, int userId)
    {
        super(id, userId);
    }

    public BiosketchExclude(String recType, int id, int userId)
    {
        this(id,userId);
        this.recType = recType;
    }

    /**
     * @return the biosketchID
     */
    public int getBiosketchID()
    {
        return biosketchID;
    }

    /**
     * @param biosketchID the biosketchID to set
     */
    public void setBiosketchID(int biosketchID)
    {
        this.biosketchID = biosketchID;
    }

    /**
     * @return the recordID
     */
    public int getRecordID()
    {
        return recordID;
    }

    /**
     * @param recordID the recordID to set
     */
    public void setRecordID(int recordID)
    {
        this.recordID = recordID;
    }

    /**
     * @return the recType
     */
    public String getRecType()
    {
        return recType;
    }

    /**
     * @param recType the recType to set
     */
    public void setRecType(String recType)
    {
        this.recType = recType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recType == null) ? 0 : recType.hashCode());
        result = prime * result + recordID;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (!(obj instanceof BiosketchExclude)) return false;
        final BiosketchExclude other = (BiosketchExclude) obj;
        if (recType == null)
        {
            if (other.recType != null) return false;
        }
        else if (!recType.equals(other.recType)) return false;
        if (recordID != other.recordID) return false;
        return true;
    }

//    @Deprecated
//    public String getIndicator()
//    {
//        return indicator;
//    }
//
//    @Deprecated
//    public void setIndicator(String indicator)
//    {
//        this.indicator = indicator;
//    }

    public ExcludeStatus getStatus()
    {
        return this.status;
    }
    public void setStatus(ExcludeStatus status)
    {
        this.status = status;
    }
}
