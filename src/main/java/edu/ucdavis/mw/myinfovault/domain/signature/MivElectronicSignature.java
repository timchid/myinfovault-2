/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivElectronicSignature.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.service.signature.representation.DocumentRepresentation;
import edu.ucdavis.mw.myinfovault.service.signature.representation.InvalidDocumentException;
import edu.ucdavis.mw.myinfovault.util.HashingUtil;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * An electronic signature specific to MyInfoVault. Provides all necessary getters and setters so this can
 * be persisted in a table. Adds the relationship of this signature to a specific MIV document by the
 * document type and document ID. The type determines the table in which a document is stored, and the ID
 * is the record number within that table.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class MivElectronicSignature extends ElectronicSignatureImpl implements ElectronicSignature, Serializable
{
    private static final long serialVersionUID = 201303061415L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Represents the ID of a <code>null</code> signer.
     */
    private static final int NO_SIGNER = -1;

    /**
     * Format used in {@link #toString()}
     */
    private static final String TO_STRING = "\n*** MivElectronicSignature ***" +
    		                                "\nrecordId: {0,number,#}\t{1}" +
    		                                "\n{2}";

    private int recordId = 0;
    private final Signable document;
    private int documentVersion = 0;

    /**
     * Constructor to re-create a signature object from saved data.
     *
     * @param requestedSigner
     * @param document
     * @param documentVersion version of the document representation that this signature signs
     * @param recordId
     * @param signed
     * @param signerId
     * @param signingDate
     * @param digestType
     * @param compatibleVersion
     * @param signature
     * @param createdDate
     * @throws IllegalArgumentException if document is <code>null</code>
     */
    public MivElectronicSignature(int requestedSigner,
                                  Signable document,
                                  int documentVersion,
                                  int recordId,
                                  boolean signed,
                                  String signerId,
                                  long signingDate,
                                  String digestType,
                                  String compatibleVersion,
                                  byte[] signature,
                                  Timestamp createdDate) throws IllegalArgumentException
    {
        this(requestedSigner, document);



        if (signed)
        {
            // signed signatures must have a supported digest type
            if (HashingUtil.Digest.getDigest(digestType) == null)
            {
                throw new IllegalArgumentException("unsupported digest type: " + digestType);
            }

            // signed signatures must have a proper document version (at least version one)
            if (documentVersion < 1)
            {
                throw new IllegalArgumentException("Invalid document representation version: " + documentVersion);
            }
        }

        this.documentVersion = documentVersion;
        this.recordId = recordId;
        this.signer = signerId;
        this.signingDate = new Date(signingDate);
        this.signed = signed;
        this.digestType = digestType;
        this.compatibleVersion = compatibleVersion;
        this.createdDate = new Date(createdDate.getTime());

        /*
         * Copy the array provided, don't just reference it, else the supplier
         * still has access to it and can tamper with the signature.
         */
        if (signature != null)
        {
            this.signature = Arrays.copyOf(signature, signature.length);
        }
    }

    /**
     * Create a new electronic signature for a requested signer and document.
     *
     * @param requestedSignerId MIV person ID requested to sign given document
     * @param document document requested to sign
     * @throws IllegalArgumentException if document is <code>null</code>
     */
    public MivElectronicSignature(int requestedSignerId, Signable document) throws IllegalArgumentException
    {
        super(Integer.toString(requestedSignerId));

        /*
         * Every MIV signature must have signable document that to which it applies!
         */
        if (document == null) throw new IllegalArgumentException("Cannot create an MIV electronic signature without a signable document");

        this.document = document;
    }

    /**
     * @return the signable document for which this signature applies
     */
    @SuppressWarnings("unchecked")
    public <T extends Signable> T getDocument()
    {
        return (T) document;
    }

    /**
     * @return Version of the document representation that this signature signs.
     */
    public int getDocumentVersion()
    {
        return documentVersion;
    }

    /**
     * @return ID for the signing user
     */
    public int getSignerId()
    {
    	try
    	{
    	    if (getSigner() != null)
    	    {
    	        return Integer.parseInt(getSigner());
    	    }
    	}
    	catch(NumberFormatException exception)
    	{
    	    throw new MivSevereApplicationError("Failed getting signer id from signature", exception);
    	}

    	return NO_SIGNER;
    }

    /**
     * @return ID for the requested signing user
     */
    public int getRequestedSignerId()
    {
        try
        {
            if (getRequestedSigner() != null)
            {
                return Integer.parseInt(getRequestedSigner());
            }
        }
        catch(NumberFormatException exception)
        {
            throw new MivSevereApplicationError("Failed getting requested signer ID from signature", exception);
        }

        return NO_SIGNER;
    }

    /**
     * @return the recordId
     */
    public int getRecordId()
    {
        return recordId;
    }

    /**
     * Set the recordId (used after persisting the signature)
     *
     * @param recordId signature database record ID
     */
    public void setRecordId(int recordId)
    {
    	this.recordId = recordId;
    }

    /**
     * @return the signature
     */
    public byte[] getSignature()
    {
        return signature;
    }

    /**
     * @return if signed
     */
    public boolean isSigned()
    {
    	return signed;
    }

    /**
     * @return the compatible MIV version
     */
    public String getCompatibleVersion()
    {
    	return compatibleVersion;
    }

    /**
     * invalidate signature
     */
    public void invalidate()
    {
    	signature = null;
    }

    /**
     * Is the signature valid for the signable document to which it applies.
     *
     * @return <code>true</code> if signature is valid, <code>false</code> otherwise
     */
    public boolean isValid()
    {
        boolean isValid = false;
        try
        {
            isValid = isValid(DocumentRepresentation.build(document, documentVersion));
        }
        catch (IllegalArgumentException|InvalidDocumentException e)
        {
            /*
             * There's no representation defined for the document type and version
             * or, the representation builder failed to build the document.
             *
             * Although this shouldn't happen, we'll just consider this document invalid.
             */
            logger.warn("Unable to validate signature:" + this, e);
        }

        // the signature is signed and not valid for this document
        if (signed && !isValid)
        {
            // log this occurrence
            logger.info("_DC_AUDIT: Signature is both SIGNED and INVALID" + this);
        }

        return isValid;
    }

    /**
     * Sign the signable document to which this signature applies.
     *
     * @param realUserId real, logged-in user signing
     */
    public void sign(int realUserId)
    {
        try
        {
            // sign with the latest document representation version for the document type
            sign(DocumentRepresentation.build(document), Integer.toString(realUserId));

            // set to latest version
            this.documentVersion = DocumentRepresentation.getLatestVersion(document.getDocumentType());
        }
        catch (IllegalArgumentException|InvalidDocumentException e)
        {
            throw new MivSevereApplicationError("errors.signature.sign", e, realUserId, document);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.ElectronicSignatureImpl#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(TO_STRING, recordId, super.toString(), document);
    }

    /*
     * Governs signature serialization.
     */
    private void writeObject(ObjectOutputStream stream) throws IOException
    {
        stream.defaultWriteObject();

        //write super class constructor argument
        stream.writeObject(requestedSigner);

        /*
         * java.util.Date doesn't serialize properly not sure why
         */
        stream.writeLong(signingDate != null ? signingDate.getTime() : 0L);
    }

    /*
     * Governs signature deserialization.
     */
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException
    {
        stream.defaultReadObject();

        //read super class constructor argument
        requestedSigner = (String) stream.readObject();

        signingDate = new Date(stream.readLong());
    }
}
