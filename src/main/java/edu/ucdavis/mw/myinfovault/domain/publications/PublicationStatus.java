/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationStatus.java
 */

package edu.ucdavis.mw.myinfovault.domain.publications;

/**
 * Defines MIV publication status.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public enum PublicationStatus
{
    /**
     * Publication has been published.
     */
    PUBLISHED(1, "Published"),

    /**
     * Publication is currently in press.
     */
    IN_PRESS(2, "In Press"),

    /**
     * Publication has been submitted for publishing.
     */
    SUBMITTED(3, "Submitted");

    private int id;
    private String description;

    private PublicationStatus(int id, String description)
    {
        this.id = id;
        this.description = description;
    }

    /**
     * @return Identifier associated with the publication status
     */
    public int getId()
    {
        return id;
    }

    /**
     * @return Publication status description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Get the publication status value associated with the given ID.
     *
     * @param id Identifier associated with the publication status
     * @return The associated publication status
     * @throws IllegalArgumentException If no status associated with the ID
     */
    public static PublicationStatus valueOf(int id) throws IllegalArgumentException
    {
        for (PublicationStatus publicationStatus : PublicationStatus.values())
        {
            if (publicationStatus.getId() == id) return publicationStatus;
        }

        throw new IllegalArgumentException("No PublicationType exists for the ID '" + id + "'.");
    }
}
