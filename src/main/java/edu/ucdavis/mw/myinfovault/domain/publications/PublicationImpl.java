/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationImpl.java
 */

package edu.ucdavis.mw.myinfovault.domain.publications;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.data.Validator;
import edu.ucdavis.myinfovault.format.PeriodicalFormatter;

/**
 * MIV publication record associated with an MIV user.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class PublicationImpl implements Publication, Comparable<Publication>, Serializable
{
    private static final long serialVersionUID = 201105161103L;
    private static PeriodicalFormatter pf = new PeriodicalFormatter();

    private int id;
    private int sequence;
    private int userId;
    private PublicationStatus status;
    private PublicationType type;
    private PublicationSource source;
    private String externalId;
    private String year;
    private String authors;
    private String title;
    private String editor;
    private String journal;
    private String volume;
    private String issue;
    private String pages;
    private String publisher;
    private String city;
    private String isbn;
    private URL link;
    private boolean selected = false;

    /**
     * Create a new publication.
     *
     * @param userId The ID of the user associated with the publication
     * @param status Code representing the publication status
     * @param type Code representing the publication type
     * @param source Code representing the publication source
     * @param externalId Identifier used from originating source
     * @param year Publication year
     * @param authors Comma delimited list of authors
     * @param title Publication title
     * @param editor Publication editor
     * @param journal Journal in which publication was published
     * @param volume Volume of journal in which publication was published
     * @param issue Issue of volume in which publication was published
     * @param pages Pages cited in the issue in which the publication was published
     * @param publisher Publication publisher
     * @param city Publisher location
     * @param isbn ISBN associated with the publication or ISSN (in the case of a serial publication)
     * @param link URL to resource associated with the publication
     * @throws IllegalArgumentException If unable to create publication from given arguments
     */
    public PublicationImpl(int userId,
                           String status,
                           String type,
                           String source,
                           String externalId,
                           String year,
                           String authors,
                           String title,
                           String editor,
                           String journal,
                           String volume,
                           String issue,
                           String pages,
                           String publisher,
                           String city,
                           String isbn,
                           String link) throws IllegalArgumentException
    {
        this(0,
             0,
             userId,
             status,
             type,
             source,
             externalId,
             year,
             authors,
             title,
             editor,
             journal,
             volume,
             issue,
             pages,
             publisher,
             city,
             isbn,
             link);
    }

    /**
     * Create publication from the database.
     *
     * @param id Unique identifier of this record from the database or zero if new
     * @param sequence Order in which this publication appears in the user's dossier
     * @param userId The ID of the user associated with the publication
     * @param status Code representing the publication status
     * @param type Code representing the publication type
     * @param source Code representing the publication source
     * @param externalId Identifier used from originating source
     * @param year Publication year
     * @param authors Comma delimited list of authors
     * @param title Publication title
     * @param editor Publication editor
     * @param journal Journal in which publication was published
     * @param volume Volume of journal in which publication was published
     * @param issue Issue of volume in which publication was published
     * @param pages Pages cited in the issue in which the publication was published
     * @param publisher Publication publisher
     * @param city Publisher location
     * @param isbn ISBN associated with the publication or ISSN (in the case of a serial publication)
     * @param link URL to resource associated with the publication
     * @throws IllegalArgumentException If unable to create publication from given arguments
     */
    public PublicationImpl(int id,
                           int sequence,
                           int userId,
                           String status,
                           String type,
                           String source,
                           String externalId,
                           String year,
                           String authors,
                           String title,
                           String editor,
                           String journal,
                           String volume,
                           String issue,
                           String pages,
                           String publisher,
                           String city,
                           String isbn,
                           String link) throws IllegalArgumentException
    {
        this.id = id;
        this.sequence = sequence;
        this.userId = userId;

        try
        {
            this.status = StringUtils.hasText(status)
                        ? PublicationStatus.valueOf(Integer.parseInt(status))
                        : PublicationStatus.PUBLISHED;

            this.type = StringUtils.hasText(type)
                      ? PublicationType.valueOf(Integer.parseInt(type))
                      : PublicationType.JOURNAL;

            this.source = PublicationSource.valueOf(StringUtils.hasText(source) ? Integer.parseInt(source) : 0);
        }
        catch (NumberFormatException e)
        {
            throw new IllegalArgumentException("Publication status, type, and source must be integers", e);
        }

        if (!StringUtils.hasText(authors)
         || !StringUtils.hasText(title)
         || !StringUtils.hasText(year))
        {
            throw new IllegalArgumentException("Publication must have at least one author, a title, and year");
        }

        if (this.source == PublicationSource.PUBMED
         && !StringUtils.hasText(externalId))
        {
            throw new IllegalArgumentException("PubMed publications must have an external ID");
        }

        this.year = parseYear(year);
        this.authors = authors;
        this.title = title;
        this.externalId = externalId;
        this.editor = editor;
        this.journal = journal;
        this.volume = volume;
        this.issue = issue;
        this.pages = pages;
        this.publisher = publisher;
        this.city = city;
        this.isbn = isbn;

        try
        {
            this.link = StringUtils.hasText(link) ? new URL(link) : null;
        }
        catch(MalformedURLException e)
        {
            throw new IllegalArgumentException("Publication URL must be a valid link", e);
        }
    }


    /**
     * @param year year to parse
     * @return the first valid four digit number in the string, otherwise left as is
     */
    private String parseYear(String year)
    {
        year = year.trim();

        int substrings = year.length() - 3;

        String yearSub = null;

        for (int i=0; i < substrings; i++)
        {
            if (Validator.isInteger(yearSub = year.substring(i, i + 4))) return yearSub;
        }

        return year;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getCitation()
     */
    @Override
    public String getCitation()
    {
        Map<String, String> m = new HashMap<String, String>();

        m.put("year", getYear());
        m.put("author", getAuthors());
        m.put("title", getTitle());
        m.put("journal", getPublisher());
        m.put("volume", getVolume());
        m.put("issue", getIssue());
        m.put("pages", getPages());

        return pf.format(m).get("preview");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getId()
     */
    @Override
    public int getId() {
        return id;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getSequence()
     */
    @Override
    public int getSequence() {
        return sequence;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getUserId()
     */
    @Override
    public int getUserId() {
        return userId;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getStatus()
     */
    @Override
    public int getStatus() {
        return status.getId();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getType()
     */
    @Override
    public int getType() {
        return type.getId();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getSource()
     */
    @Override
    public PublicationSource getSource() {
        return source;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getExternalId()
     */
    @Override
    public String getExternalId() {
        return externalId;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getYear()
     */
    @Override
    public String getYear() {
        return year;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getAuthors()
     */
    @Override
    public String getAuthors() {
        return authors;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getTitle()
     */
    @Override
    public String getTitle() {
        return title;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getEditor()
     */
    @Override
    public String getEditor() {
        return editor;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getJournal()
     */
    @Override
    public String getJournal() {
        return journal;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getVolume()
     */
    @Override
    public String getVolume() {
        return volume;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getIssue()
     */
    @Override
    public String getIssue() {
        return issue;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getPages()
     */
    @Override
    public String getPages() {
        return pages;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getPublisher()
     */
    @Override
    public String getPublisher() {
        return publisher;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getCity()
     */
    @Override
    public String getCity() {
        return city;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getIsbn()
     */
    @Override
    public String getIsbn() {
        return isbn;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.publications.Publication#getLink()
     */
    @Override
    public String getLink() {
        return link != null ? link.toString() : "";
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object ob)
    {
        if (this == ob) return true;

        if (ob instanceof Publication)
        {
            Publication pub = (Publication) ob;

            //if both publications are from PUBMED, they are equal if the externalIds match
            if (source == PublicationSource.PUBMED
             && pub.getSource() == PublicationSource.PUBMED
             && externalId.equals(pub.getExternalId()))
            {
                return true;
            }

            //otherwise, ISBN, publisher, title, and year must match for equality
            if (StringUtil.nullComparator.compare(title, pub.getTitle()) == 0
             && StringUtil.nullComparator.compare(isbn,pub.getIsbn()) == 0
             && StringUtil.nullComparator.compare(publisher,pub.getPublisher()) == 0
             && StringUtil.nullComparator.compare(year, pub.getYear()) ==  0)
            {
                return true;
            }
        }

        return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = 1;

        //corresponds to the conditions of equals
        if (source == PublicationSource.PUBMED)
        {
            hash = hash * 31 + externalId.hashCode();
        }
        else
        {
            if (StringUtils.hasText(isbn)) hash = hash * 31 + isbn.hashCode();
            if (StringUtils.hasText(publisher)) hash = hash * 31 + publisher.hashCode();
            hash = hash * 31 + title.hashCode();
            hash = hash * 31 + year.hashCode();
        }
        return hash;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Publication pub)
    {
        switch (source)
        {
            case PUBMED:
            case ENDNOTE:
                return compareExternId(pub);
            default:
                return comparePubInfo(pub);
        }
    }

    /**
     * @param pub publication to compare
     * @return comparison for PubMed and EndNote publications
     */
    private int compareExternId(Publication pub)
    {
        if (source == pub.getSource()) {
            return StringUtil.nullComparator.compare(externalId, pub.getExternalId());
        }

        return 1; // Different sources are different, even if they reference the same pub.
    }

    /**
     * @param pub publication to compare
     * @return comparison by publication attributes
     */
    private int comparePubInfo(Publication pub)
    {
        // Return date comparison if not equal
        int comparison = StringUtil.nullComparator.compare(year, pub.getYear());
        if (comparison != 0) return comparison;

        // Return isbn comparison if not equal
        comparison = StringUtil.nullComparator.compare(isbn, pub.getIsbn());
        if (comparison != 0) return comparison;

        // Return title comparison if not equal
        comparison = StringUtil.nullComparator.compare(title, pub.getTitle());
        if (comparison != 0) return comparison;

        // Return publisher comparison (should not be equal at this point)
        comparison = StringUtil.nullComparator.compare(publisher, pub.getPublisher());
        if (comparison != 0) return comparison;

        // Should be equal then
        assert this.equals(pub) : "compareTo is inconsistent with equal";

        return 0;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#isSelected()
     */
    @Override
    public boolean isSelected()
    {
        return selected;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#setSelected(boolean)
     */
    @Override
    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
}
