/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MemberComparator.java
 */

package edu.ucdavis.mw.myinfovault.domain.group;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Compares group members by sort name.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class MemberComparator extends NameComparator<MivPerson> implements Serializable
{
    private static final long serialVersionUID = 201202221106L;

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.group.NameComparator#getName(java.lang.Object)
     */
    @Override
    protected String getName(MivPerson person)
    {
        return person.getSortName();
    }
}
