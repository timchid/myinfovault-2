/**
 * Copyright
 * Copyright (c) University of California, Davis, 2009
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ElectronicSignatureBase.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import java.util.Date;

import edu.ucdavis.myinfovault.MIVConfig;



/**<p>
 * A stub to front implementations of the ElectronicSignature interface.
 * This leaves out the getters and setters that the real implementation
 * needs so it can be persisted, because "client" code that uses Electronic
 * Signatures has no business using them.</p>
 * <p>This class may not be needed since the interface limits what methods can
 * be used anyway. If web frameworks allowed "real" OO this and the interface
 * would be public, and the actual implementation class would be package
 * scoped, with the DAO getting access so it could load and save the impl.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public abstract class ElectronicSignatureBase implements ElectronicSignature
{

    /** The digest to use for the signature */
    protected String digestType =
            MIVConfig.getConfig().getProperty("electronicSignature-digest") == null ? "SHA-256" : MIVConfig.getConfig().getProperty("electronicSignature-digest");

    /** Person asked to sign the document. */
    protected String requestedSigner = null;

    /** Person that actually signed the document. */
    protected String signer = null;

    /** Date/time when signature requested. */
    protected Date createdDate = null;

    /** Date/time document was signed. */
    protected Date signingDate = null;

    /** Optional description for this signature. */
    protected String description = null;


    /**
     * Create a new signature for the requested signer.
     *
     * @param requestedSigner ID of the requested signer
     */
    public ElectronicSignatureBase(String requestedSigner)
    {
        this.requestedSigner = requestedSigner;
    }

    /**
     * No-arg constructor for sub-class serialization
     */
    protected ElectronicSignatureBase(){}

    @Override
    public String getDigestType() { return this.digestType; }

    @Override
    public String getRequestedSigner() { return this.requestedSigner; }

    @Override
    public String getSigner() { return this.signer; }

    @Override
    public Date getSigningDate() { return this.signingDate; }

    @Override
    public Date getCreatedDate() { return this.createdDate; }

    @Override
    public void setDescription(CharSequence description) { this.description = description.toString(); }

    @Override
    public String getDescription() { return this.description; }


    @Override
    public abstract boolean isValid(byte[] document);

    @Override
    public abstract boolean isValid(String document);

    @Override
    public abstract void sign(byte[] document, String signerId);

    @Override
    public abstract void sign(String document, String signerId);

}
