/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Notation.java
 */


package edu.ucdavis.mw.myinfovault.domain.annotation;


/**
 * Represents the four notation types and specifies the characters they use.
 * There are two for Master annotations and two for Packet annotations.
 * @author Stephen Paulsen
 * @since MIV 5.0
 */
public enum Notation
{
    //
    // Keep these definitions in this order!
    // When multiple notations appear their order in the text should be: *x+@
    // Notation.values() will return an array ordered by the declaration order.
    //

    /**
     * This item is something that occurred within the period under review.<br>
     * INCLUDED is a <em>packet specific</em> notation.
     */
    INCLUDED    ( '*', false )
    ,
    /**
     * This item is considered one of the most significant works listed.<br>
     * SIGNIFICANT is a <em>packet specific</em> notation.
     */
    SIGNIFICANT ( 'x', false )
    ,
    /**
     * A major mentoring role was played relating to this item.<br>
     * MENTORING is a <em>master</em> notation.
     */
    MENTORING   ( '+', true )
    ,
    /**
     * This item was <em>refereed</em> <small>(Whatever <em><strong>that</strong></em> means)</small><br>
     * REFEREED is a <em>master</em> notation.
     */
    REFEREED    ( '@', true )
    ;

    private final char notation;
    private final String noteString;
    private final boolean master; // vs. packet


    /**
     *
     * @param notation character to use for the notation
     * @param master <code>true</code> if this is for <em>master</em> annotations,
     *               <code>false</code> if for <em>packet specific</em> annotations.
     */
    private Notation(final char notation, final boolean master)
    {
        this.notation = notation;
        this.noteString = Character.toString(this.notation);
        this.master = master;
    }


    public char getMark()
    {
        return this.notation;
    }


    public String getMarkString()
    {
        return this.noteString;
    }


    public boolean isMaster()
    {
        return this.master;
    }


    /**
     * Get the Notation object for the given character.
     * @param c the character for which to find a Notation.
     * @return the Notation, or <code>null</code> if the character does not correspond to a notation.
     */
    public static Notation valueOf(final char c)
    {
        for (Notation n : Notation.values())
        {
            if (n.notation == c) return n;
        }
        return null;
    }


    public static Notation valueOf(final CharSequence str)
    {
        return str.length() == 1
                ? valueOf(str.charAt(0))
                : Enum.valueOf(Notation.class, str.toString());
    }
}
