/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PermissionAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Interface used by the MivStandaloneAuthorizationServiceImpl to make reflection calls to configured authorization classes.
 *
 * @author Stephen Paulsen
 * @since MIV 3.5?
 */
public interface PermissionAuthorizer
{
    /**
     * TODO: Add Javadoc comments!
     * @param person
     * @param permissionName
     * @param permissionDetails
     * @return
     */
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails);


    /**
     * TODO: Add Javadoc comments!
     * @param person
     * @param permissionName
     * @param permissionDetails
     * @param qualification
     * @return
     */
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification);
}
