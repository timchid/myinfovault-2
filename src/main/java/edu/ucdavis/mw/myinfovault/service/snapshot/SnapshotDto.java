/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SnapshotDto.java
 */
package edu.ucdavis.mw.myinfovault.service.snapshot;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The class represents a Snapshot object
 * @author rhendric
 * @since MIV 3.0
 */
public class SnapshotDto implements Serializable
{
    private static final long serialVersionUID = 200906050802L;

    private int snapshotId;
    private long dossierId;
    private String mivRoleId = null;
    private int schoolId;
    private int departmentId;
    private String description = null;
    private String snapshotFileName = null;
    private String snapshotLocation = null;
    private Timestamp insertTimestamp = null;
    private int insertUserID;
    private int updateUserID;
    private Timestamp updateTimestamp = null;


    /**
     * getSnapshotId
     * @return int snapshotId
     */
    public int getSnapshotId()
    {
        return snapshotId;
    }

    /**
     * @param snapshotId snapshot ID
     */
    public void setSnapshotId(int snapshotId)
    {
        this.snapshotId = snapshotId;
    }

    /**
     * @return dossier ID
     */
    public long getDossierId()
    {
        return dossierId;
    }

    /**
     * @param dossierId dossier ID
     */
    public void setDossierId(long dossierId)
    {
        this.dossierId = dossierId;
    }

    /**
     * @return MIV role ID
     */
    public String getMivRoleId()
    {
        return mivRoleId;
    }

    /**
     * @param mivRoleId MIV role ID
     */
    public void setMivRoleId(String mivRoleId)
    {
        this.mivRoleId = mivRoleId;
    }

    /**
     * @return school ID
     */
    public int getSchoolId()
    {
        return schoolId;
    }

    /**
     * @param schoolId school ID
     */
    public void setSchoolId(int schoolId)
    {
        this.schoolId = schoolId;
    }

    /**
     * @return department ID
     */
    public int getDepartmentId()
    {
        return departmentId;
    }

    /**
     * @param departmentId department ID
     */
    public void setDepartmentId(int departmentId)
    {
        this.departmentId = departmentId;
    }

    /**
     * getDescription
     * @return String description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description snapshot description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return snapshot filename
     */
    public String getSnapshotFileName()
    {
        return snapshotFileName;
    }

    /**
     * @param snapshotFileName snapshot filename
     */
    public void setSnapshotFileName(String snapshotFileName)
    {
        this.snapshotFileName = snapshotFileName;
    }

    /**
     * @param snapshotId snapshot ID
     */
    public void setId(int snapshotId)
    {
        this.snapshotId = snapshotId;
    }

    /**
     * @return snapshot ID
     */
    public int getId()
    {
        return this.snapshotId;
    }

    /**
     * @return snapshot location
     */
    public String getSnapshotLocation()
    {
        return snapshotLocation;
    }

    /**
     * @param snapshotLocation snapshot location
     */
    public void setSnapshotLocation(String snapshotLocation)
    {
        this.snapshotLocation = snapshotLocation;
    }

    /**
     * @return insert time stamp
     */
    public Timestamp getInsertTimestamp()
    {
        return insertTimestamp;
    }

    /**
     * @param insertTimestamp insert time stamp
     */
    public void setInsertTimestamp(Timestamp insertTimestamp)
    {
        this.insertTimestamp = insertTimestamp;
    }

    /**
     * @return insert user ID
     */
    public int getInsertUserID()
    {
        return insertUserID;
    }

    /**
     * @param insertUserID insert user ID
     */
    public void setInsertUserID(int insertUserID)
    {
        this.insertUserID = insertUserID;
    }

    /**
     * @return update user ID
     */
    public int getUpdateUserID()
    {
        return updateUserID;
    }

    /**
     * @param updateUserID update user ID
     */
    public void setUpdateUserID(int updateUserID)
    {
        this.updateUserID = updateUserID;
    }

    /**
     * @return update time stamp
     */
    public Timestamp getUpdateTimestamp()
    {
        return updateTimestamp;
    }

    /**
     * @param updateTimestamp update time stamp
     */
    public void setUpdateTimestamp(Timestamp updateTimestamp)
    {
        this.updateTimestamp = updateTimestamp;
    }
}
