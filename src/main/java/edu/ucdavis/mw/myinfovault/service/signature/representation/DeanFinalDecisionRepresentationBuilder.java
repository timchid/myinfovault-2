/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DeanFinalDecisionRepresentationBuilder.java
 */

package edu.ucdavis.mw.myinfovault.service.signature.representation;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;
import edu.ucdavis.mw.myinfovault.domain.raf.Department;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.domain.raf.Rank;
import edu.ucdavis.mw.myinfovault.domain.raf.Status;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;

/**
 * Builds document representations for dean final decision documents.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
class DeanFinalDecisionRepresentationBuilder extends DocumentRepresentationBuilder
{
    private static final ThreadLocal<DateFormat> effectiveDateFormat =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyy-dd-MM");
            }
        };

    private static final ThreadLocal<DateFormat> retroactiveDateFormat =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyy-dd-MM");
            }
        };

    /* MIV-3244 : Added formatter to guarantee the String representation of a Salary is always the same */
    private static final NumberFormat salaryFormat = NumberFormat.getNumberInstance(Locale.US);
    static {
        // set up the salary formatter
        salaryFormat.setMinimumFractionDigits(2);
        salaryFormat.setMaximumFractionDigits(2);
        salaryFormat.setGroupingUsed(false);
    }

    /**
     * Version one for dean final decision document representations.
     *
     * @param rafDepartment recommended action form department
     * @return document representation
     */
    @SuppressWarnings("deprecation")
    @Version(1)
    protected byte[] getVersionOne(Decision decision)
    {
        RafBo raf = MivServiceLocator.getRafService().getBo(decision.getDossier());

        StringBuilder representation = new StringBuilder();

        representation.append(raf.getAcceleration().getCoefficient() * (raf.getAccelerationYears() + raf.getDecelerationYears()))
                      .append(raf.getUpdateUserId())
                      .append(raf.getDossier().getAction().getActionType())
                      .append(raf.getCandidateName())
                      .append(raf.getDossier().getAction().getDelegationAuthority())
                      .append(raf.getDossier().getDossierId() > 0L ? raf.getDossier().getDossierId() : "")
                      .append(raf.getDossier().getAction().getEffectiveDate() != null ? effectiveDateFormat.get().format(raf.getDossier().getAction().getEffectiveDate()) : "")
                      .append(raf.getDossier().getAction().getRetroactiveDate() != null ? retroactiveDateFormat.get().format(raf.getDossier().getAction().getRetroactiveDate()) : "")
                      .append(raf.getId())
                      .append(raf.getRecommendationAFP())
                      .append(raf.getRecommendationCSD())
                      .append(raf.getRecommendationJAS())
                      .append(raf.getDossier().getAction().getCandidate().getUserId())
                      .append(raf.getYearsAtRank())
                      .append(raf.getYearsAtStep());

        for (final Status status : raf.getStatuses())
        {
            representation.append(status.getStatusType() != null ? status.getStatusType().toString() : "");

            for (final Rank rank : status.getRanks())
            {
                representation.append(rank.getRankAndStep())
                              .append(rank.getPercentOfTime())
                              .append(salaryFormat.format(rank.getAnnualSalary()))
                              .append(salaryFormat.format(rank.getMonthlySalary()))
                              .append(rank.getTitleCode());
            }
        }

        for (final Department department : raf.getDepartments())
        {
            representation.append(department.getDepartmentName())
                          .append(department.getSchoolName())
                          .append(department.getPercentOfTime());
        }

        return representation.toString().getBytes();
    }

    /**
     * Version two for dean final decision document representations.
     *
     * @param rafDepartment recommended action form department
     * @return document representation
     */
    @Version(2)
    protected byte[] getVersionTwo(Decision decision)
    {
        RafBo raf = MivServiceLocator.getRafService().getBo(decision.getDossier());

        StringBuilder representation = new StringBuilder();

        representation.append(raf.getAcceleration().getCoefficient() * (raf.getAccelerationYears() + raf.getDecelerationYears()))
                      .append(raf.getDossier().getAction().getActionType())
                      .append(raf.getDossier().getAction().getDelegationAuthority())
                      .append(raf.getDossier().getDossierId() > 0L ? raf.getDossier().getDossierId() : "")
                      .append(raf.getDossier().getAction().getEffectiveDate() != null ? effectiveDateFormat.get().format(raf.getDossier().getAction().getEffectiveDate()) : "")
                      .append(raf.getDossier().getAction().getRetroactiveDate() != null ? retroactiveDateFormat.get().format(raf.getDossier().getAction().getRetroactiveDate()) : "")
                      .append(raf.getId())
                      .append(raf.getDossier().getAction().getCandidate().getUserId())
                      .append(raf.getYearsAtRank())
                      .append(raf.getYearsAtStep());

        for (final Status status : raf.getStatuses())
        {
            representation.append(status.getStatusType() != null ? status.getStatusType().toString() : "");

            for (final Rank rank : status.getRanks())
            {
                representation.append(rank.getRankAndStep())
                              .append(rank.getPercentOfTime())
                              .append(salaryFormat.format(rank.getAnnualSalary()))
                              .append(salaryFormat.format(rank.getMonthlySalary()))
                              .append(rank.getTitleCode());
            }
        }

        for (final Department department : raf.getDepartments())
        {
            representation.append(department.getDepartmentName())
                          .append(department.getSchoolName())
                          .append(department.getPercentOfTime());
        }

        return representation.toString().getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Version three for dean final decision document representations.
     *
     * @param decision dean's final decision
     * @return document representation
     */
    @Version(3)
    protected byte[] getVersionThree(Decision decision)
    {
        /*
         * All final decisions will now use version 3 by default, however there will still be some decisions which will
         * continue to use the legacy RAF. If the RAF is present, get version 2 of the representation.
         */
        if (decision.getDossier().isRecommendedActionFormPresent())
        {
            return getVersionTwo(decision);
        }

        StringBuilder representation = new StringBuilder();

        AcademicActionBo academicAction = MivServiceLocator.getAcademicActionService().getBo(decision.getDossier());

        representation.append(decision.getDocumentType().name())
                      .append(decision.getDecisionType().name())
                      .append(decision.isContraryToCommittee())
                      .append(decision.getComments())
                      .append(academicAction.getDossier().getAction().getCandidate().getUserId())
                      .append(effectiveDateFormat.get().format(academicAction.getDossier().getAction().getEffectiveDate()))
                      .append(academicAction.getDossier().getAction().getActionType().name())
                      .append(academicAction.getDossier().getAction().getDelegationAuthority().name());

        for (Assignment assignment : academicAction.getAssignmentStatuses().get(StatusType.PROPOSED))
        {
            representation.append(assignment.getScope())
                          .append(assignment.isPrimary())
                          .append(salaryFormat.format(assignment.getPercentOfTime()));

            for (Title title : assignment.getTitles())
            {
                representation.append(title.getDescription())
                              .append(title.getStep().getValue())
                              .append(salaryFormat.format(title.getPercentOfTime()))
                              .append(title.getAppointmentDuration().name())
                              .append(title.getSalaryPeriod().name())
                              .append(salaryFormat.format(title.getPeriodSalary()))
                              .append(salaryFormat.format(title.getAnnualSalary()));
            }
        }

        return representation.toString().getBytes(StandardCharsets.UTF_8);
    }
}
