/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiScope.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;

/**
 * Object which represents scope and permissions for use with the API.
 *
 * @author japorito
 * @since 5.0
 */
public class ApiScope
{
    private ApiResourceType resource;
    private Collection<ApiPermission> permissions;
    private int schoolId, departmentId, userId;

    public static final int ANY = -1;
    public static final int NONE = 0;

    public static final ApiScope ALLOW_ALL = new ApiScope(ApiResourceType.ALL,
                                                          ApiScope.ANY,
                                                          ApiScope.ANY,
                                                          ApiScope.ANY,
                                                          EnumSet.allOf(ApiPermission.class));

    public static final ApiScope DENY_ALL = new ApiScope(ApiResourceType.ALL,
                                                         ApiScope.NONE,
                                                         ApiScope.NONE,
                                                         ApiScope.NONE,
                                                         EnumSet.noneOf(ApiPermission.class));

    public ApiScope()
    {

    }

    public ApiScope(ApiResourceType resource,
                    int schoolId,
                    int deptId,
                    int userId,
                    Collection<ApiPermission> permissions)
    {
        this.resource = resource;
        this.schoolId = schoolId;
        this.departmentId = deptId;
        this.userId = userId;
        this.permissions = permissions;
    }

    /**
     *
     * @param scope - The scope that must be contained within this scope.
     * @return
     */
    public Boolean containsScope(ApiScope otherScope)
    {
        if (otherScope != null)
        {
            if (this.equals(otherScope)) return true;

            if (this.resource != otherScope.getResource() &&
                this.resource != ApiResourceType.ALL &&
                otherScope.getResource() != ApiResourceType.ALL) return false;

            if (!this.permissions.containsAll(otherScope.getPermissions())) return false;

            if (this.schoolId == ANY || otherScope.getSchoolId() == ANY) return true;

            if (this.schoolId == otherScope.getSchoolId())
            {
                if (this.departmentId == ANY || otherScope.getDepartmentId() == ANY) return true;

                if (this.departmentId == otherScope.getDepartmentId()) {
                    if (this.userId == ANY ||
                        otherScope.getUserId() == ANY ||
                        this.userId == otherScope.getUserId()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static Set<ApiScope> getApiScopes(ApiResourceType type,
                                             Collection<ApiPermission> permissions,
                                             Collection<AssignedRole> assignedScopes,
                                             Integer userId)
    {
        HashSet<ApiScope> scopes = new HashSet<ApiScope>();

        AppointmentScope scope;
        for (AssignedRole role : assignedScopes)
        {
            scope = role.getScope();
            scopes.add(new ApiScope(type, scope.getSchool(), scope.getDepartment(), userId, permissions));
        }

        return scopes;
    }

    public static Set<ApiScope> getApiScopes(ApiResourceType type,
                                             Collection<ApiPermission> permissions,
                                             Integer userId)
    {
        Set<AssignedRole> assignedScopes = MivServiceLocator.getUserService().getPersonByMivId(userId).getAssignedRoles();
        return getApiScopes(type, permissions, assignedScopes, userId);
    }

    /**
     * @return the permissions
     */
    public Collection<ApiPermission> getPermissions()
    {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(Collection<ApiPermission> permissions)
    {
        this.permissions = permissions;
    }

    /**
     * @return the userId
     */
    public int getUserId()
    {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    /**
     * @return the schoolId
     */
    public int getSchoolId()
    {
        return schoolId;
    }

    /**
     * @param schoolId the schoolId to set
     */
    public void setSchoolId(int schoolId)
    {
        this.schoolId = schoolId;
    }

    /**
     * @return the departmentId
     */
    public int getDepartmentId()
    {
        return departmentId;
    }

    /**
     * @param departmentId the departmentId to set
     */
    public void setDepartmentId(int departmentId)
    {
        this.departmentId = departmentId;
    }

    /**
     * @return the resource
     */
    public ApiResourceType getResource()
    {
        return resource;
    }

    /**
     * @param resource the resource to set
     */
    public void setResource(ApiResourceType resource)
    {
        this.resource = resource;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) { return true; }
        if (!super.equals(obj)) { return false; }
        if (ApiScope.class.isAssignableFrom(obj.getClass()))
        {
            ApiScope other = (ApiScope) obj;
            if (this.userId != other.getUserId() ||
                this.departmentId != other.getDepartmentId() ||
                this.schoolId != other.getSchoolId() ||
                this.resource != other.getResource() ||
                !this.permissions.equals(other.getPermissions()))
            {
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }
}
