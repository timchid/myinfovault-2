package edu.ucdavis.mw.myinfovault.service.person;

/**
 * Always returns ID info for Stephen.<br>
 * Temporary, just so there is an implementation of the PersonIdMapper during development.
 * @author Stephen Paulsen
 */
public class FakePersonMapper implements PersonIdMapper
{

    @Override
    public String getPersonIdForIamId(String iamId)
    {
        return "00409149";
    }


    @Override
    public String getPersonIdForPrincipalName(String principalName)
    {
        return "00409149";
    }


    @Override
    public String getPersonIdForEmployeeId(String employeeId)
    {
        return "00409149";
    }


    @Override
    public String getPersonIdForStudentId(String studentId)
    {
        return "00409149";
    }


    @Override
    public String getIamIdForPersonId(String personId)
    {
        return "1000017567";
    }


    @Override
    public String getIamIdForPrincipalName(String principalName)
    {
        return "1000017567";
    }


    @Override
    public String getIamIdForEmployeeId(String employeeId)
    {
        return "1000017567";
    }


    @Override
    public String getIamIdForStudentId(String studentId)
    {
        return "1000017567";
    }


    @Override
    public String getEmployeeIdForPersonId(String personId)
    {
        return "502732654";
    }


    @Override
    public String getEmployeeIdForIamId(String iamId)
    {
        return "502732654";
    }


    @Override
    public String getEmployeeIdForPrincipalName(String principalName)
    {
        return "502732654";
    }


    @Override
    public String getEmployeeIdForStudentId(String studentId)
    {
        return "502732654";
    }


    @Override
    public String getStudentIdForPersonId(String personId)
    {
        return "502732654";
    }


    @Override
    public String getStudentIdForIamId(String iamId)
    {
        return "502732654";
    }


    @Override
    public String getStudentIdForPrincipalName(String principalName)
    {
        return "502732654";
    }


    @Override
    public String getStudentIdForEmployeeId(String studentId)
    {
        return "502732654";
    }


    @Override
    public String getPersonIdForUserId(String studentId)
    {
        return "00409149";
    }


    @Override
    public String getIamIdForUserId(String studentId)
    {
        return "1000017567";
    }


    @Override
    public String getEmployeeIdForUserId(String studentId)
    {
        return "502732654";
    }


    @Override
    public String getStudentIdForUserId(String studentId)
    {
        return "502732654";
    }


    @Override
    public String getUserIdForPersonId(String personId)
    {
        return "18099";
    }


    @Override
    public String getUserIdForIamId(String iamId)
    {
        return "18099";
    }


    @Override
    public String getUserIdForPrincipalName(String principalName)
    {
        return "18099";
    }


    @Override
    public String getUserIdForEmployeeId(String employeeId)
    {
        return "18099";
    }


    @Override
    public String getUserIdForStudentId(String studentId)
    {
        return "18099";
    }

}
