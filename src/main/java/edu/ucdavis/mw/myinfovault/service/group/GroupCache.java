/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupCache.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import edu.ucdavis.mw.myinfovault.dao.group.GroupDaoImpl.DBGroupCallback;
import edu.ucdavis.mw.myinfovault.domain.group.Group;

/**
 * GroupCache caches the groups built from records in the database.
 * Entries will only need eviction/refresh if there are modifications to a group.
 *
 * @author rhendric
 */
public class GroupCache {

    private static final long MAX_SIZE = 1000;
    private static GroupCache instance = null;
    private static final Logger logger = LoggerFactory.getLogger(GroupCache.class);

    private final LoadingCache<Integer, Group>cache;

    /**
     * Get an instance of the group cache
     * @param callback - callback to DAO when entry is not in the cache
     * @return
     */
    public static GroupCache getInstance(DBGroupCallback callback)
    {
        if (instance == null)
        {
            instance = new GroupCache(callback);
        }
        return instance;
    }

    /**
     * Create an instance of the GroupCache
     * @param callback - callback to DAO when entry is not in the cache
     */
    protected GroupCache(final DBGroupCallback callback) {
        cache = CacheBuilder.newBuilder().maximumSize( MAX_SIZE )
            .recordStats()
            .build( new CacheLoader<Integer, Group>() {
            @Override
            public Group load( Integer groupId ) {
                logger.info("Placing group {} into cache...", groupId);
                return callback.getGroup(groupId);
            }
        });
    }

    /**
     * Retrieve an entry from cache
     * @param key
     * @return Group
     */
    public Group getEntry( Integer key ) {
        return getCache().getUnchecked( key );
    }

    /**
     * @return cache
     */
    public LoadingCache<Integer, Group> getCache()
    {
        return cache;
    }

    /**
     * Show the stats for this cache
     */
    public void showCacheStats()
    {
        logger.info("<<<< Group Cache Stats >>>>");
        logger.info(MessageFormat.format("Loads={0} Hits={1} Misses={2} Evictions={3}",
                cache.stats().loadCount(),
                cache.stats().hitCount(),
                cache.stats().missCount(),
                cache.stats().evictionCount()));
    }


}