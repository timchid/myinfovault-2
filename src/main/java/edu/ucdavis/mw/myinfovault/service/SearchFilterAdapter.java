/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchFilterAdapter.java
 */

package edu.ucdavis.mw.myinfovault.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.SortedSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * The SearchFilterAdapter implements a useful version of the
 * {@link SearchFilter#apply(Collection) apply()} method defined
 * by the SearchFilter interface. Subclasses need only provide an
 * {@link SearchFilter#include(Object) include()} method.
 *
 * @author Stephen Paulsen
 * @param <T> Object by which collections will filtered
 * @since MIV 3.0
 */
public abstract class SearchFilterAdapter<T> implements SearchFilter<T>
{
    private List<SearchFilter<T>> filters = new ArrayList<SearchFilter<T>>();
    private boolean exclude = false;

    /** Logger available for subclasses to use. */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * Add search filter that will be applied on the result in addition to
     * this filter. Filters will be applied in the order added.
     *
     * @param filter Additional filter
     * @return reference to self for chaining
     */
    public <F extends SearchFilter<T>> SearchFilterAdapter<T> and(F filter)
    {
        filters.add(filter);

        return this;
    }

    /**
     * Invert and add search filter that will be applied on the result in addition to
     * this filter. Filters will be applied in the order added.
     *
     * @param filter Additional filter
     * @return reference to self for chaining
     */
    public <F extends SearchFilterAdapter<T>> SearchFilterAdapter<T> not(F filter)
    {
        return and(filter.not());
    }

    /**
     * Invert the filter to exclude items returning <code>true</code> from {@link #include(Object)}.
     *
     * @return self reference for chaining
     */
    public SearchFilterAdapter<T> not()
    {
        exclude = true;
        return this;
    }

    /**
     * Apply this and all additional 'AND'ed filters to all the items presented.
     *
     * @param items The collection to be filtered
     * @return A new collection of the same type provided, containing only the filtered elements
     * or an empty collection if null or an empty collection is passed in.
     */
    @SuppressWarnings({"unchecked"}) // for assigning c.newInstance()
    @Override
    public <C extends Collection<T>> C apply(C items)
    {
        // If null was passed there is no "same type" of collection - you get an empty list back.
        if (items == null) return (C) Collections.EMPTY_LIST;

        C result = null;

        Class<?> c = items.getClass();
        try
        {
            result = (C) c.newInstance();
            
            if (items.isEmpty()) return result;
            
            /*
             * For sorted sets with comparators, attempt to construct
             * a new instance with the same comparator.
             */
            if (items instanceof SortedSet)
            {
                Comparator<?> comparator = ((SortedSet<?>) items).comparator();
                
                if (comparator != null)
                {
                    try
                    {
                        result = (C) c.getDeclaredConstructor(Comparator.class)
                                      .newInstance(comparator);
                    }
                    catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
                    {
                        throw new MivSevereApplicationError("Unable to create new SortedSet instance with comparator", e);
                    }
                }
            }

            for (T item : items)
            {
                if (exclude ? !this.include(item) : this.include(item)) {
                    result.add(item);
                }
            }
        }
        catch (InstantiationException e)
        {
            /*
             * FIXME: What should we do here?
             *
             * This exception is thrown on a calls to:
             *  - Class<AutoPopulatingList>#newInstance()
             *  - Class<SynchronizedCollection<?>>#newInstance()
             */
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            // Chaining filters using and() with applyAdditionalFilters() can result in
            // an EMPTY_SET or EMPTY_LIST being returned by an intermediate filter.
            // If we've got one of those it will cause this exception, so return a
            // zero-element empty equivalent.
            if (items == Collections.EMPTY_SET) return (C) new HashSet<T>(0);

            //if(items == Collections.EMPTY_LIST)
            //or, if items is any other Collection with a private constructor
            return (C) new ArrayList<T>(0);
        }

        //also apply additional filters
        return applyAdditionalFilters(result, filters);
    }

    /**
     * Apply given additional filters to the given item collection.
     *
     * @param items Collection to filter
     * @param additionalFilters Additional filters to apply to the given items
     * @return The filtered collection
     */
    protected <C extends Collection<T>> C applyAdditionalFilters(C items, List<SearchFilter<T>> additionalFilters)
    {
        for (SearchFilter<T> filter : additionalFilters)
        {
            // No further filtering necessary if result is empty
            if (items.isEmpty()) break;

            items = filter.apply(items);
        }

        return items;
    }
}
