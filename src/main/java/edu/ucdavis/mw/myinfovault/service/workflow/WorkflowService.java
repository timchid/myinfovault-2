/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: WorkflowService.java
 */

package edu.ucdavis.mw.myinfovault.service.workflow;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;

/**
 * This service class is used to retrieve the workflow nodes and locations available to a dossier based
 * on the DelegationAuthority, ActionType and location of the dossier.
 * This class builds a map of the the workflownodes retrieved from the database.
 * @author rhendric
 * @since MIV 4.6.1
 */
public interface WorkflowService
{
    /**
     * Get the next DossierLocations available to this dossier
     * @param dossier the dossier for which to get the next locations.
     * @return List of DossierLocations available to the dossier.
     * There may be none or many locations available. If there are no
     * next locations available the list will be empty or contain a single UNKNOWN location.
     */
    public List<DossierLocation> getNextLocations(Dossier dossier);

    /**
     * Get the current WorkflowNode for this dossier
     * @param dossier the dossier for which to get the current WorkflowNode.
     * @return WorkflowNode
     */
    public WorkflowNode getCurrentWorkflowNode(Dossier dossier);

    /**
     * Get the next WorflowNodes available to the input dossier
     * @return List of WorkflowNodes available for this dossier.
     * @throws WorkflowException if no next nodes are available.
     */
    public List<WorkflowNode> getNextWorkflowNodes(Dossier dossier)
    throws WorkflowException;

    /**
     * Get the INITIAL workflow node for the input dossier
     * @param dossier
     * @return WorkflowNode the initial workflow node.
     * @throws WorkflowException if there is no INITIAL workflow node located
     */
    public WorkflowNode getInitialNode(Dossier dossier) throws WorkflowException;

    /**
     * Get the FINAL workflow node for the input dossier
     * @param dossier
     * @return WorkflowNode the initial workflow node.
     * @throws WorkflowException if there is no FINAL workflow node located
     */
    public WorkflowNode getFinalNode(Dossier dossier) throws WorkflowException;

    /**
     * Get the workflow node for the specified location for a dossier
     * @param dossier
     * @param location
     * @return WorkflowNode the workflow node for this location.
     * @throws WorkflowException if there is no workflow node located
     */
    public WorkflowNode getWorkflowNodeForLocation(Dossier dossier, DossierLocation location)
    throws WorkflowException;

}
