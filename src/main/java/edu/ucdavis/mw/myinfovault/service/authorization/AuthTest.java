/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuthTest.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Class exists only to test initial development of AuthorizationService implementation.
 * Impl uses reflection to load configured classes.
 *
 * @author Stephen Paulsen
 * @since MIV 3.5?
 */
public class AuthTest implements PermissionAuthorizer
{
    /** Return value for the method calls. This can be modified in the debugger to assist testing. */
    boolean isAllowed = false;


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.PermissionAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        // TODO Auto-generated method stub
        System.out.println("?????????? Checking hasPermission [" + permissionName + "] for " + person.getDisplayName());
        return isAllowed;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.PermissionAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        // TODO Auto-generated method stub
        System.out.println("?????????? Checking isAuthorized [" + permissionName + "] for " + person.getDisplayName());
        return isAllowed;
    }
}
