package edu.ucdavis.mw.myinfovault.service.dossier;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNodeType;

public class DossierRoutingDefinitionDto
{
    private final DossierLocation currentWorkflowLocation;
    private final DossierLocation previousWorkflowLocation;
    private final DossierLocation nextWorkflowLocation;
    private final DossierDelegationAuthority delegationAuthority;
    private final DossierActionType actionType;
    private final WorkflowNodeType workflowNodeType;
    private final String locationPrerequisiteAttributes;
    private final boolean displayOnlyWhenAvailable;

    public DossierRoutingDefinitionDto(DossierLocation currentWorkflowLocation,
                                       DossierLocation previousWorkflowLocation,
                                       DossierLocation nextWorkflowLocation,
                                       DossierDelegationAuthority delegationAuthority,
                                       DossierActionType actionType,
                                       WorkflowNodeType workflowNodeType,
                                       String locationPrerequisiteAttributes,
                                       boolean displayOnlyWhenAvailable)
    {
        this.currentWorkflowLocation = currentWorkflowLocation;
        this.previousWorkflowLocation = previousWorkflowLocation;
        this.nextWorkflowLocation = nextWorkflowLocation;
        this.delegationAuthority = delegationAuthority;
        this.actionType = actionType;
        this.workflowNodeType = workflowNodeType;
        this.locationPrerequisiteAttributes = locationPrerequisiteAttributes;
        this.displayOnlyWhenAvailable = displayOnlyWhenAvailable;
    }

    public DossierLocation getCurrentWorkflowLocation() {
        return currentWorkflowLocation;
    }

    public DossierLocation getPreviousWorkflowLocation() {
        return previousWorkflowLocation;
    }

    public DossierLocation getNextWorkflowLocation() {
        return nextWorkflowLocation;
    }

    public DossierDelegationAuthority getDelegationAuthority() {
        return delegationAuthority;
    }

    public DossierActionType getActionType() {
        return actionType;
    }

    public WorkflowNodeType getWorkflowNodeType() {
        return workflowNodeType;
    }

    public String getLocationPrerequisiteAttributes() {
        return locationPrerequisiteAttributes;
    }

    public boolean isDisplayOnlyWhenAvailable() {
        return displayOnlyWhenAvailable;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if (o instanceof DossierRoutingDefinitionDto)
        {
            DossierRoutingDefinitionDto definition = (DossierRoutingDefinitionDto) o;

            return this.delegationAuthority == definition.getDelegationAuthority()
                && this.actionType == definition.getActionType()
                && this.currentWorkflowLocation == definition.getCurrentWorkflowLocation()
                && this.nextWorkflowLocation == definition.getNextWorkflowLocation()
                && this.previousWorkflowLocation == definition.getPreviousWorkflowLocation();
        }

        return false;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = 1;

        hash = hash * 31 + this.delegationAuthority.hashCode();
        hash = hash * 31 + this.actionType.hashCode();
        hash = hash * 31 + this.currentWorkflowLocation.hashCode();
        hash = hash * 31 + this.nextWorkflowLocation.hashCode();
        hash = hash * 31 + this.previousWorkflowLocation.hashCode();

        return hash;
    }
}
