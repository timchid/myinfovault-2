/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentRepresentation.java
 */

package edu.ucdavis.mw.myinfovault.service.signature.representation;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import edu.ucdavis.mw.myinfovault.domain.signature.Signable;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.message.ErrorMessages;

/**
 * Builds document representations for {@link Signable} documents.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
public class DocumentRepresentation
{
    private static final DocumentRepresentationBuilder dcBuilder = new DisclosureCertificateRepresentationBuilder();
    private static final DocumentRepresentationBuilder finalDecisionBuilder = new DeanFinalDecisionRepresentationBuilder();
    private static final DocumentRepresentationBuilder decisionBuilder = new DecisionRepresentationBuilder();

    private static final Map<MivDocument, DocumentRepresentationBuilder> builders = ImmutableMap.<MivDocument, DocumentRepresentationBuilder>builder()
                                                                                                .put(MivDocument.DISCLOSURE_CERTIFICATE, dcBuilder)
                                                                                                .put(MivDocument.JOINT_DISCLOSURE_CERTIFICATE, dcBuilder)
                                                                                                .put(MivDocument.DEANS_FINAL_DECISION, finalDecisionBuilder)
                                                                                                .put(MivDocument.DEANS_APPEAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.VICEPROVOSTS_FINAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.VICEPROVOSTS_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_TENURE_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_TENURE_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_FINAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.CHANCELLORS_FINAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.VICEPROVOSTS_APPEAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.VICEPROVOSTS_APPEAL_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_APPEAL_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_TENURE_APPEAL_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_TENURE_APPEAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.PROVOSTS_APPEAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.CHANCELLORS_APPEAL_DECISION, decisionBuilder)
                                                                                                .put(MivDocument.DEANS_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.JOINT_DEANS_RECOMMENDATION, decisionBuilder)
                                                                                                .put(MivDocument.JOINT_DEANS_APPEAL_RECOMMENDATION, decisionBuilder)
                                                                                                .build();
    /**
     * Get the number of the latest document representation version.
     *
     * @param documentType signable document type
     * @return number of the latest document representation version
     * @throws IllegalArgumentException if no representation is defined for the given document type
     */
    public static int getLatestVersion(MivDocument documentType) throws IllegalArgumentException
    {
        return getBuilder(documentType).getLatestVersion();
    }

    /**
     * Get the latest version of the document representation for the given signable document.
     *
     * @param document signable document for which to build representation
     * @return latest version of the document representation
     * @throws IllegalArgumentException  if no representation is defined for the type of the given document
     * @throws InvalidDocumentException if the a representation cannot be built for the given document
     */
    public static byte[] build(Signable document) throws IllegalArgumentException, InvalidDocumentException
    {
        return build(document, getLatestVersion(document.getDocumentType()));
    }

    /**
     * Get the given version of the document representation for the given signable document.
     *
     * @param document signable document for which to build representation
     * @param version version number of the document representation
     * @return document representation of the given version
     * @throws IllegalArgumentException if no representation is defined for the type of the given document and version
     * @throws InvalidDocumentException if the a representation cannot be built for the given document
     */
    public static byte[] build(Signable document, int version) throws IllegalArgumentException, InvalidDocumentException
    {
        try
        {
            return getBuilder(document.getDocumentType()).getDocumentRepresentation(document, version);
        }
        catch (InvocationTargetException e)
        {
            // unwrap invocation target exception cause
            throw new InvalidDocumentException("errors.representation.build", e.getTargetException(), version, document);
        }
    }

    /**
     * Get the document representation builder associated with this document type.
     *
     * @param documentType signable document type
     * @return the document representation builder associated with this document type
     * @throws IllegalArgumentException if no builder exists for the given document type
     */
    private static DocumentRepresentationBuilder getBuilder(MivDocument documentType) throws IllegalArgumentException
    {
        DocumentRepresentationBuilder builder = builders.get(documentType);

        if (builder != null) return builder;

        throw new IllegalArgumentException(ErrorMessages.getInstance().getString("errors.representation.documentType", documentType));
    }
}
