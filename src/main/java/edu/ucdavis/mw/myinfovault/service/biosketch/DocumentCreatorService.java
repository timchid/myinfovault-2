package edu.ucdavis.mw.myinfovault.service.biosketch;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.document.BiosketchBuilder;
import edu.ucdavis.myinfovault.document.Document;

/**
 * @author dreddy
 * @since MIV 2.1
 */
public class DocumentCreatorService implements IDocumentCreatorService
{

    @Override
    public List<Document> createBiosketch(MIVUserInfo mui, Biosketch biosketch)
    {

        BiosketchBuilder biosketchBuilder = new BiosketchBuilder(mui, biosketch);

        // Generate the biosketch documents
        List<Document>documents = biosketchBuilder.generateDocuments();
        return (documents);
    }

    @Override
    public List<Document> createPacket(MIVUserInfo mui, int collectionId, long packetId)
    {
        PacketService packetService = MivServiceLocator.getPacketService();
        Packet packet = packetId <= 0 ? new Packet(mui.getPerson().getUserId(), "Master") : packetService.getPacket(packetId);
        return MivServiceLocator.getPacketService().createPacket(packet);
    }

    @Override
    public List<Document> viewPacket(MIVUserInfo mui, int collectionId, long packetId)
    {
        PacketService packetService = MivServiceLocator.getPacketService();
        Packet packet = packetId <= 0 ? new Packet(mui.getPerson().getUserId(), "Master") : packetService.getPacket(packetId);
        return MivServiceLocator.getPacketService().previewPacket(packet);
    }

    @Override
    public List<Document> createPacket(MIVUserInfo mui, int collectionId)
    {
        return this.createPacket(mui, collectionId, 0);
    }

    @Override
    public List<Document> viewPacket(MIVUserInfo mui, int collectionId)
    {
        return viewPacket(mui, collectionId, 0);
    }

}

