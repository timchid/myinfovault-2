/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierCreatorImpl.java
 */


package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PDFConcatenator;
import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * TODO: Add Javadoc comments!
 *
 * @author rhendric
 * @since ?
 */
public class DossierCreatorServiceImpl implements DossierCreatorService
{
    private Logger log = Logger.getLogger(DossierCreatorServiceImpl.class);
    private static final DateFormat extensionFormat = new SimpleDateFormat("MMddyyyy_hhmmss");

    private DossierDao dossierDao = null;
    private DossierService dossierService = null;

    /**
     * Spring injected DossierDao bean
     * @param dossierDao
     */
    public void setDossierDao(DossierDao dossierDao)
    {
        this.dossierDao = dossierDao;
    }

    /**
     * Spring injected DossierService bean
     * @param dossierService
     */
    public void setDossierService(DossierService dossierService)
    {
        this.dossierService = dossierService;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.File, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public DossierFiles createDossier(File outputFile, Dossier dossier, MivPerson person) throws WorkflowException
    {
        MivRole roleType = determinePersonRoleType(person, dossier);
        return this.createDossier(outputFile, dossier, roleType,this.getSchoolDepartmentCriteriaMap(person, roleType));
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.File, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public DossierFiles createDossier(File outputFile, Dossier dossier, MivRole role) throws WorkflowException
    {
        return this.createDossier(outputFile, dossier, role, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.OutputStream, long, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public DossierFiles createDossier(OutputStream outputStream, long dossierId, MivPerson mivPerson) throws WorkflowException
    {
        Dossier dossier = dossierService.getDossier(dossierId);
        MivRole roleType = determinePersonRoleType(mivPerson, dossier);

        return this.createDossier(outputStream, dossier, roleType, this.getSchoolDepartmentCriteriaMap(mivPerson, roleType));
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.OutputStream, long, edu.ucdavis.mw.myinfovault.service.person.MivRole, java.util.Map)
     */
    @Override
    public DossierFiles createDossier(OutputStream outputStream, long dossierId, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap) throws WorkflowException
    {
        return this.createDossier(outputStream, dossierService.getDossier(dossierId), role, schoolDepartmentCriteriaMap);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.OutputStream, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public DossierFiles createDossier(OutputStream outputStream, Dossier dossier, MivPerson mivPerson)
    {
        MivRole roleType = determinePersonRoleType(mivPerson, dossier);
        return this.createDossier(outputStream, dossier, roleType, this.getSchoolDepartmentCriteriaMap(mivPerson,roleType));
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.OutputStream, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public DossierFiles createDossier(OutputStream outputStream, Dossier dossier, MivRole role)
    {
        return this.createDossier(outputStream, dossier, role, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.OutputStream, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole, java.util.Map)
     */
    @Override
    public DossierFiles createDossier(OutputStream outputStream, Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap)
    {
        boolean success = false;
        // Gather the dossier files this role is allowed to view.
        // DocumentAccess documentAccess = new DocumentAccess();
        List<File>dossierFileList = new ArrayList<File>();

        Map<String, String>dossierFileUrlMap = this.getDossierFileUrlMap(dossier, role, schoolDepartmentCriteriaMap);

        PathConstructor pathConstructor = new PathConstructor();
        pathConstructor.setUrlPathPrefix("/miv/documents");

        // Build the file path for each of the URL's
        for (String urlStr : dossierFileUrlMap.keySet())
        {
            try
            {
                dossierFileList.add(pathConstructor.getPathFromUrl(urlStr));
            }
            catch (Exception e)
            {
                String msg = "Unable to construct a file path from url "+urlStr+" when creating dossier "+dossier.getDossierId();
                log.error(msg, e);
            }
        }

        DossierFiles dossierFile = new DossierFiles();
        dossierFile.setPdfFiles(dossierFileList);
        dossierFile.setCreated(success);
        dossierFile.setDossierId(dossier.getDossierId());
        dossierFile.setUserId(dossier.getAction().getCandidate().getUserId());

        // Make sure there is something to concatenate
        if (!dossierFileList.isEmpty())
        {
            // Concatenate the list of files found into a single pdf file
            PDFConcatenator pdfConcatenator = new PDFConcatenator();
            success = pdfConcatenator.concatenateFiles(dossierFileList, outputStream);
        }
        else
        {
            String msg = "No files found to create dossier "+dossier.getDossierId()+" for "+role.getShortDescription()+" role";
            log.error(msg);
        }
        dossierFile.setCreated(success);
        return dossierFile;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.OutputStream, long, edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public DossierFiles createDossier(OutputStream outputStream, long dossierId, MivRole role) throws WorkflowException
    {
        return this.createDossier(outputStream, dossierService.getDossier(dossierId), role, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createDossier(java.io.File, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public DossierFiles createDossier(File outputFile, Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap) throws WorkflowException
    {
        OutputStream outputStream = null;

        // Create the outputstream for the input file
        try
        {
            // If the specified outputfile exists, rename and log
            if (outputFile.exists())
            {
                if (this.renameFile(outputFile))
                {
                    String msg = "Renamed existing file "+outputFile.getAbsolutePath()+" for dossier "+dossier.getDossierId()+".";
                    log.info(msg);
                }
                else
                {
                    String msg = "Unable to create dossier/snapshot for "+dossier.getDossierId()+" - Rename of existing file "+outputFile.getAbsolutePath()+" for dossier failed!";
                    log.error(msg);
                    throw new WorkflowException(msg);
                }
            }
            // Create the dossier
            outputStream = new FileOutputStream(outputFile);
            return this.createDossier(outputStream, dossier, role, schoolDepartmentCriteriaMap);
        }
        catch (FileNotFoundException fnfe)
        {
            String msg = "Unable to create output file "+outputFile.getAbsolutePath()+" for dossier "+dossier.getDossierId()+".";
            log.error(msg, fnfe);
            throw new WorkflowException(msg, fnfe);
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createSnapshot(java.io.File, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public SnapshotDto createSnapshot(File outputFile, Dossier dossier, MivRole role) throws WorkflowException
    {
        return this.createSnapshot(outputFile, dossier, role, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#createSnapshot(java.io.File, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole, java.util.Map)
     */
    @Override
    public SnapshotDto createSnapshot(File outputFile, Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap) throws WorkflowException
    {
        SnapshotDto snapshot = null;

        // If the snapshot is created successfully, write an entry to the dossier snapshot table
        DossierFiles dossierFiles = this.createDossier(outputFile, dossier, role, schoolDepartmentCriteriaMap);
        if (dossierFiles.isCreated())
        {
            SchoolDepartmentCriteria schoolDepartmentCriteria = null;

            if (schoolDepartmentCriteriaMap != null) {
                // Should only be a single SchoolDepartmentCriteria object in the map
                for (String key : schoolDepartmentCriteriaMap.keySet()) {
                    schoolDepartmentCriteria = schoolDepartmentCriteriaMap.get(key);
                    break;
                }
            }
            snapshot = dossierDao.updateSnapshot(dossier, role, outputFile, schoolDepartmentCriteria);
        }
        return snapshot;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#getDossierFileUrlMap(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Map<String, String> getDossierFileUrlMap(Dossier dossier, MivPerson mivPerson)
    {
        MivRole roleType = determinePersonRoleType(mivPerson, dossier);
        return this.getDossierFileUrlMap(dossier, roleType, this.getSchoolDepartmentCriteriaMap(mivPerson, roleType));
    }


    @Override
    public Map<String, String> getDossierFileUrlMap(Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap)
    {

        PathConstructor pathConstructor = new PathConstructor();
        Map<String, String>dossierFileUrlMap = new LinkedHashMap<String, String>();

        Map<File, DossierDocumentDto> dossierFiles = this.getDossierFiles(dossier, role, schoolDepartmentCriteriaMap);

        for (File file : dossierFiles.keySet())
        {
            DossierDocumentDto dossierDocumentDto = dossierFiles.get(file);
            dossierFileUrlMap.put(pathConstructor.getUrlFromPath(file), dossierDocumentDto.getDescription());
        }

//        File dossierDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);
//        Map<String, String>dossierFileMap = new LinkedHashMap<String, String>();
//        PathConstructor pathConstructor = new PathConstructor();
//
//        // Get a list of documents the input role is allowed to view.
//        List<DossierDocumentDto>dossierDocumentList = this.documentAccess.getDocumentsAuthorizedForRole(role);
//
//        // Get any uploaded files
//        Map<String, List<UploadDocumentDto>> uploadDocumentMap = this.dossierDao.getDocumentUploadFiles(dossier);
//
//        // Iterate through the list of files allowed to view building a list of those which are present to concatenate
//        for (DossierDocumentDto dossierDocumentDto : dossierDocumentList)
//        {
//            // Candidate file is a "packet" file. No viewing Restrictions
//            if (dossierDocumentDto.isCandidateFile())
//            {
//                // Find all files which start with the dossierFileName and have a PDF extension
//                final String dossierFileName = dossierDocumentDto.getFileName();
//                File [] fileList = dossierDirectory.listFiles(new FilenameFilter(){
//                    public boolean accept(File dir, String name)
//                    {
//                        if (name.startsWith(dossierFileName) && name.endsWith("."+DocumentFormat.PDF.fileExtension))
//                        {
//                            return true;
//                        }
//                        return false;
//                    }});
//
//                // Add all of the files found to the list
//                for (File file : fileList)
//                {
//                    dossierFileMap.put(pathConstructor.getUrlFromPath(file), dossierDocumentDto.getDescription());
//                }
//                continue;
//            }
//
//            // Process the uploaded document first
//            if (!uploadDocumentMap.isEmpty() &&
//                (dossierDocumentDto.getDossierAttributeType().equals(DossierAttributeType.DOCUMENTUPLOAD) ||
//                 dossierDocumentDto.getDossierAttributeType().equals(DossierAttributeType.DOCUMENTUPLOADSIGNATURE)))
//            {
//                for (File file :getUploadedDocuments(dossier, dossierDocumentDto, uploadDocumentMap, searchCriteria, role))
//                {
//                    dossierFileMap.put(pathConstructor.getUrlFromPath(file),dossierDocumentDto.getDescription());
//                }
//            }
//
//            // Process data based document.
//            if (dossierDocumentDto.getDossierAttributeType().equals(DossierAttributeType.DOCUMENT) ||
//                dossierDocumentDto.getDossierAttributeType().equals(DossierAttributeType.DOCUMENTUPLOADSIGNATURE))
//            {
//                for (File file :getDocuments(dossier, dossierDocumentDto, dossierDirectory, searchCriteria, role))
//                {
//                    dossierFileMap.put(pathConstructor.getUrlFromPath(file),dossierDocumentDto.getDescription());
//                }
//            }
//
//        }
        return dossierFileUrlMap;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#getDossierFiles(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Map<File, DossierDocumentDto> getDossierFiles(Dossier dossier, MivPerson mivPerson)
    {
        MivRole roleType = determinePersonRoleType(mivPerson, dossier);
        return this.getDossierFiles(dossier, roleType, this.getSchoolDepartmentCriteriaMap(mivPerson, roleType));
    }


    @Override
    public Map<File, DossierDocumentDto> getDossierFiles(Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap)
    {
        File dossierDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);
        Map<File, DossierDocumentDto>dossierFileMap = new LinkedHashMap<File, DossierDocumentDto>();
//        PathConstructor pathConstructor = new PathConstructor();

        // Get a list of documents the input role is allowed to view.
        List<DossierDocumentDto>dossierDocumentList = getAuthorizedDocuments(role);

        // Get any uploaded files
        Map<String, List<UploadDocumentDto>> uploadDocumentMap = this.dossierDao.getDocumentUploadFiles(dossier);

        // Iterate through the list of files allowed to view building a list of those which are present to concatenate
        for (DossierDocumentDto dossierDocumentDto : dossierDocumentList)
        {
            // Candidate file is a "packet" file. No viewing Restrictions
            if (dossierDocumentDto.isCandidateFile())
            {
                // Find all files which start with the dossierFileName and have a PDF extension
                final String dossierFileName = dossierDocumentDto.getFileName();
                File [] fileList = dossierDirectory.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name)
                    {
                        if (name.startsWith(dossierFileName) && name.endsWith("."+DocumentFormat.PDF.getFileExtension()))
                        {
                            return true;
                        }
                        return false;
                    }});

                if (fileList == null || fileList.length == 0)
                {
                    continue;
                }

                // Add all of the files found to the list
                for (File file : fileList)
                {
                    dossierFileMap.put(file, dossierDocumentDto);
                }
                continue;
            }

            // Process the uploaded document first
            if (!uploadDocumentMap.isEmpty() &&
                dossierDocumentDto.getDossierAttributeType() == DossierAttributeType.DOCUMENTUPLOAD)
            {
                for (File file :getUploadedDocuments(dossier, dossierDocumentDto, uploadDocumentMap, schoolDepartmentCriteriaMap, role))
                {
                    if (!dossierFileMap.containsKey(file))
                    {
                        dossierFileMap.put(file, dossierDocumentDto);
                    }
                }
            }

            // Process data based document.
            if (dossierDocumentDto.getDossierAttributeType() == DossierAttributeType.DOCUMENT)
            {
                for (File file : getDocuments(dossier, dossierDocumentDto, dossierDirectory, schoolDepartmentCriteriaMap, role))
                {
                    if (!dossierFileMap.containsKey(file))
                    {
                        dossierFileMap.put(file, dossierDocumentDto);
                    }
                }
            }

        }
        return dossierFileMap;
    }


    /**
     * Get the uploaded documents for this DossierDocument.
     *
     * @param dossier Dossier
     * @param dossierDocumentDto DTO representing a distinct dossier document
     * @param uploadDocumentMap map of uploaded documents by documentId for this dossier
     * @param schoolDepartmentCriteriaMap search criteria containing the school and department of qualifying documents
     * @param role role for which the dossier is being built
     * @return List of files located for this document
     */
    private List<File> getUploadedDocuments(Dossier dossier,
                                            DossierDocumentDto dossierDocumentDto,
                                            Map<String, List<UploadDocumentDto>> uploadDocumentMap,
                                            Map<String, SchoolDepartmentCriteria> schoolDepartmentCriteriaMap,
                                            MivRole role)
    {
        List<File>dossierFileList = new ArrayList<File>();
        boolean isPrimaryViewer = this.determineViewerType(role, dossier, schoolDepartmentCriteriaMap);

        // Get the uploaded documents for this document Id, if any
        List <UploadDocumentDto> uploadDocumentList = uploadDocumentMap.get(dossierDocumentDto.getDocumentId()+"");
        if (uploadDocumentList != null)
        {
            // Get the redacted document ids this role is allowed to view
            boolean viewNonRedacted = false;

            String viewNonRedactedDocIds = dossierDocumentDto.getViewNonRedactedDocIds();
            if (viewNonRedactedDocIds != null)
            {
                List<String> viewNonRedactedDocIdList = Arrays.asList(viewNonRedactedDocIds.split(","));

                if (viewNonRedactedDocIdList.contains(dossierDocumentDto.getDocumentId()+""))
                {
                    viewNonRedacted = true;
                }
            }

            // Iterate through the document list
            for (UploadDocumentDto uploadDocument : uploadDocumentList)
            {
//                // If this document type is redactable is not redacted and role is not allowed to view non redacted, skip
//                if (dossierDocumentDto.isAllowRedaction() &&
//                        !uploadDocument.isRedacted() &&
//                        /* !dossierDocumentDto.isViewNonRedacted() */ !viewNonRedacted)
//                {
//                    continue;
//                }
//                //MIV-2998 - skip redacted documents if role is allowed to view non-redacted
//                // If this document type is redactable, is redacted and role is allowed to view non redacted, skip
//                else if (dossierDocumentDto.isAllowRedaction() && uploadDocument.isRedacted() &&
//                        /* dossierDocumentDto.isViewNonRedacted() */ viewNonRedacted)
//                {
//                    continue;
//                }

                // If this is a redactable document, check if is redacted
                if (dossierDocumentDto.isAllowRedaction())
                {
                    // The document is redacted, skip if allowed to view nonRedacted (MIV-2998)
                    if (uploadDocument.isRedacted() && viewNonRedacted)
                    {
                        continue;
                    }
                    // The document is not redacted, skip if not allowed to view nonRedacted
                    else if (!uploadDocument.isRedacted() && !viewNonRedacted)
                    {
                        continue;
                    }
                }

                // Check the school and department schoolDepartmentCriteria for the uploads if this is not a primary viewer
                File uploadFile = uploadDocument.getFile();
                if (schoolDepartmentCriteriaMap != null && !dossierDocumentDto.isCrossDepartmentPermission() && !isPrimaryViewer)
                {
                    if (this.qualifySchoolAndDepartment(schoolDepartmentCriteriaMap, uploadDocument.getSchoolId(), uploadDocument.getDepartmentId()))
                    {
                        dossierFileList.add(uploadFile);
                    }

                }
                // No need to qualify
                else
                {
                    dossierFileList.add(uploadFile);
                }
            }
        }
        return dossierFileList;
    }


    /**
     * Get the documents for this DossierDocument by searching for files with the
     * appropriate name on the file system.
     *
     * @param dossier Dossier
     * @param dossierDocumentDto DTO representing a distinct dossier document
     * @param dossierDirectory Directory to search
     * @param schoolDepartmentCriteriaMap - search criteria containing the school and department of qualifying documents
     * @param role role for which the dossier is being built
     * @return List of files located for this document
     */
    private List<File> getDocuments(Dossier dossier,
            DossierDocumentDto dossierDocumentDto,
            File dossierDirectory, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap, MivRole role)
    {
        List<File>dossierFileList = new ArrayList<File>();
        boolean isPrimaryViewer = this.determineViewerType(role, dossier, schoolDepartmentCriteriaMap);

        // Find all files which start with the dossierFileName_<schoolId>_<departmentId> and have a PDF extension
        final String dossierFileName = dossierDocumentDto.getFileName();
        File [] fileList = dossierDirectory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name)
            {
                if (name.startsWith(dossierFileName + "_") && name.endsWith("."+DocumentFormat.PDF.getFileExtension()))
                {
                    return true;
                }
                return false;
            }});

        if (fileList == null || fileList.length == 0)
        {
            return dossierFileList;
        }

        // Add all of the qualified files found to the list to be concatenated
        for (File file : fileList)
        {
            int schoolId = 0;
            int departmentId = 0;
                String filename = file.getName();
                filename = filename.replace(dossierFileName, "");                   // get rid of the filename
                filename = filename.substring(0, filename.indexOf(".")); // get rid of the suffix
                String filenameParts[] = filename.split("_");      // split out the school and department
                if (filenameParts.length != 3)
                {
                    continue;
                }
                else
                {
                    try
                    {
                        schoolId = Integer.parseInt(filenameParts[1]);
                        departmentId = Integer.parseInt(filenameParts[2]);
                    }
                    catch (NumberFormatException nfe)
                    {
                        String msg = "Unable to parse school and department from filename " + file.getName() + " for dossier attribute "
                                + dossierDocumentDto.getAttributeName() + ".";
                        log.error(msg, nfe);
                        continue;
                    }
                }
//            // If this is the J_SCHOOL_REVIEWER role, do not include the primary disclosure certificate.
//            //
//            // The disclosure certificate is handled differently for joint school reviewers only. Perhaps
//            // the joint school reviewer should really see the primary disclosure certificate, since they can see
//            // the other primary documents. If that were the case we can eliminate this check.
//            // TODO: If the elimination of the DC is valid for this case, the better solution is to set
//            // up another collectionDocument type rather than this check.
//            if (role == MivRole.J_SCHOOL_REVIEWER && dossierDocumentDto.getAttributeName().startsWith("disclosure"))
//            {
//                continue;
//            }

            // Check the school and department schoolDepartmentCriteria for the document if this is not a primary viewer
            if (schoolDepartmentCriteriaMap != null && !dossierDocumentDto.isCrossDepartmentPermission() && !isPrimaryViewer)
            {
                if (this.qualifySchoolAndDepartment(schoolDepartmentCriteriaMap, schoolId, departmentId))
                {
                    dossierFileList.add(file);
                }
            }
            // No need to qualify
            else
            {
                dossierFileList.add(file);
            }
        }
        return dossierFileList;
    }


    /**
     * Determine if this viewer is from the primary school/department.
     *
     * @param role
     * @param dossier
     * @param schoolDepartmentCriteriaMap
     * @return <code>true</code> if this dossier viewer is from the primary department, otherwise <code>false</code>
     */
    private boolean determineViewerType(MivRole role, Dossier dossier, Map<String, SchoolDepartmentCriteria> schoolDepartmentCriteriaMap)
    {
        boolean isPrimaryViewer = false;
        // IF this is a CRC_REVIEWER, SCHOOL_REVIEWER, J_SCHOOL_REVIEWER consider them a primary viewer since they can see documents
        // from all departments.
        //
        // MIV-3430
        // Add SCHOOL_STAFF to the group which can see documents from all departments and comment out MIV-3360 code below
        if (role == MivRole.CRC_REVIEWER ||
            role == MivRole.SCHOOL_REVIEWER ||
            role == MivRole.J_SCHOOL_REVIEWER ||
            role == MivRole.SCHOOL_STAFF ||
            role == MivRole.DEAN ||
            role == MivRole.VICE_PROVOST ||
            role == MivRole.VICE_PROVOST_STAFF)
        {
            isPrimaryViewer = true;
        }
//        // MIV-3260
//        // If this is a SCHOOL_STAFF role, see if it is for the primary school
//        else if(schoolDepartmentCriteria != null &&
//                role == MivRole.SCHOOL_STAFF &&
//                dossier.getPrimarySchoolId() == schoolDepartmentCriteria.getSchoolId())
//
//        {
//            isPrimaryViewer = true;
//        }
        // See if the search criteria for school and department matches the primary department of this
        // dossier, if yes and this is not for a CANDIDATE or DEPT_REVIEWER role then this is a primary department viewer.
        else if (schoolDepartmentCriteriaMap != null &&
                role != MivRole.CANDIDATE &&
                role != MivRole.DEPT_REVIEWER)
        {
            for (String key : schoolDepartmentCriteriaMap.keySet())
            {
                SchoolDepartmentCriteria schoolDepartmentCriteria = schoolDepartmentCriteriaMap.get(key);
                if ((dossier.getPrimarySchoolId() == schoolDepartmentCriteria.getSchool())
                        && (dossier.getPrimaryDepartmentId() == schoolDepartmentCriteria.getDepartment()))
                {
                    isPrimaryViewer = true;
                }
            }
        }
        return isPrimaryViewer;
    }


    /**
     * Rename a file.
     *
     * @param inputFile file to process
     * @return if successful
     */
    private boolean renameFile(File inputFile)
    {
//        SimpleDateFormat extensionFormat = new SimpleDateFormat("MMddyyyy_hhmmss"); // moved to a static final
        //extensionFormat.format(Calendar.getInstance().getTimeInMillis());
        synchronized(extensionFormat) {
            String bakFileName = inputFile.getAbsolutePath() + "_" + extensionFormat.format(Calendar.getInstance().getTimeInMillis());
            File bakFileObj = new File(bakFileName);
            return inputFile.renameTo(bakFileObj);
        }
    }


//    /**
//     * Build a search criteria map based on the input MivPerson. The map will contain a SchoolDepartmentCriteria object for
//     * each appointment of the input MivPerson.
//     * @param Dossier
//     * @return SchoolDepartmentCriteriaMap
//     * @deprecated
//     */
//    @Deprecated
//    private Map<String, SchoolDepartmentCriteria> getSchoolDepartmentCriteriaMap(MivPerson mivPerson)
//    {
//        Map<String,SchoolDepartmentCriteria>schoolDepartmentCriteriaMap = new HashMap<String,SchoolDepartmentCriteria>();
//        Collection<AssignedRole>assignedRoles  = mivPerson.getAssignedRoles();
//        for (AssignedRole assignedRole : assignedRoles) {
//            SchoolDepartmentCriteria schoolDepartmentCriteria = new SchoolDepartmentCriteria();
//
//            // Qualify further by school and department based on the person role
//            // Only need to qualify the school.
//            if (mivPerson.hasRole(MivRole.SCHOOL_STAFF, MivRole.DEAN))
//            {
//                schoolDepartmentCriteria.setSchool(assignedRole.getScope().getSchool());
//            }
//            // Must qualify both school and department
//            else if (mivPerson.hasRole(MivRole.DEPT_STAFF, MivRole.DEPT_CHAIR))
//            {
//                schoolDepartmentCriteria.setSchool(assignedRole.getScope().getSchool());
//                schoolDepartmentCriteria.setDepartment(assignedRole.getScope().getDepartment());
//            }
//            schoolDepartmentCriteriaMap.put(schoolDepartmentCriteria.getSchool()+":"+schoolDepartmentCriteria.getDepartment(), schoolDepartmentCriteria);
//        }
//        return schoolDepartmentCriteriaMap;
//    }


    /**
     * Build a search criteria map based on the input MivPerson and MivRole.
     * The map will contain a SchoolDepartmentCriteria object for each
     * appointment of the input MivPerson.
     *
     * @param mivPerson
     * @param role
     * @return SchoolDepartmentCriteriaMap
     */
    private Map<String, SchoolDepartmentCriteria> getSchoolDepartmentCriteriaMap(MivPerson mivPerson, MivRole role)
    {
        Map<String,SchoolDepartmentCriteria>schoolDepartmentCriteriaMap = new HashMap<String,SchoolDepartmentCriteria>();
        Collection<AssignedRole>assignedRoles  = mivPerson.getAssignedRoles();
        for (AssignedRole assignedRole : assignedRoles) {
            SchoolDepartmentCriteria schoolDepartmentCriteria = new SchoolDepartmentCriteria();

            // Qualify further by school and department based on the person role
            // Only need to qualify the school.
            if (role == MivRole.SCHOOL_STAFF || role == MivRole.DEAN)
            {
                schoolDepartmentCriteria.setSchool(assignedRole.getScope().getSchool());
            }
            // Must qualify both school and department
            else if (role == MivRole.DEPT_STAFF || role == MivRole.DEPT_CHAIR)
            {
                schoolDepartmentCriteria.setSchool(assignedRole.getScope().getSchool());
                schoolDepartmentCriteria.setDepartment(assignedRole.getScope().getDepartment());
            }
            schoolDepartmentCriteriaMap.put(schoolDepartmentCriteria.getSchool()+":"+schoolDepartmentCriteria.getDepartment(), schoolDepartmentCriteria);
        }
        return schoolDepartmentCriteriaMap;
    }


    /**
    * Qualify the input school and department against the search criteria
    *
    * @param schoolDepartmentCriteriaMap contains the schoolId and departmentId to qualify against
    * @param schoolId
    * @param departmentId
    * @return if qualified
    */
    private boolean qualifySchoolAndDepartment(Map<String, SchoolDepartmentCriteria> schoolDepartmentCriteriaMap, int schoolId, int departmentId)
    {

        boolean qualified = false;

        // If no school or department specified, qualified
        if (schoolDepartmentCriteriaMap.containsKey("0:0"))
        {
            qualified = true;
        }
        // All schools, match department only
        else if (schoolDepartmentCriteriaMap.containsKey("0:"+departmentId))
        {
            qualified = true;
        }
        // All departments, match school only
        else if (schoolDepartmentCriteriaMap.containsKey(schoolId+":0"))
        {
            qualified = true;
        }
        // Match school and department - Skip if school and department
        // don't match
        else if (schoolDepartmentCriteriaMap.containsKey(schoolId+":"+departmentId))
        {
            qualified = true;
        }

        return qualified;
    }


    private MivRole determinePersonRoleType(MivPerson mivPerson, Dossier dossier)
    {
        MivRole roleType = mivPerson.getPrimaryRoleType();

        // If the primary role is CANDIDATE, check if the user is a VICE_PROVOST, PROVOST or CHANCELLOR
        if (roleType == MivRole.CANDIDATE && (mivPerson.hasRole(MivRole.VICE_PROVOST, MivRole.PROVOST, MivRole.CHANCELLOR)))
        {
           return  mivPerson.hasRole(MivRole.CHANCELLOR) ? MivRole.CHANCELLOR : mivPerson.hasRole(MivRole.PROVOST) ? MivRole.PROVOST : MivRole.VICE_PROVOST;
        }

        // If the primary role is CANDIDATE, check if the user is either a DEAN or DEPT_CHAIR
        if (roleType == MivRole.CANDIDATE && mivPerson.hasRole(MivRole.DEAN, MivRole.DEPT_CHAIR))
        {
            // If the user has both the DEAN or DEPT_CHAIR, role, determine which one to use to build the dossier
            if (mivPerson.hasRole(MivRole.DEAN) && mivPerson.hasRole(MivRole.DEPT_CHAIR))
            {
                // Default to DEPT_CHAIR
                roleType = MivRole.DEPT_CHAIR;
                Map<Scope,AssignedRole>deanAssignments = new HashMap<Scope,AssignedRole>();
                for (AssignedRole assignedRole : mivPerson.getAssignedRoles())
                {
                    if (assignedRole.getRole() == MivRole.DEAN)
                    {
                        deanAssignments.put(assignedRole.getScope(),assignedRole);
                    }
                }
                List<Map<String,String>> departments = dossier.getDepartments();
                final String schoolKEY  = "schoolid";
                // Check to use the DEAN role
                for (Map<String,String> dept : departments)
                {
                    int schoolId = Integer.parseInt(dept.get(schoolKEY));
                    int deptId = -1;
                    Scope scope = new Scope(schoolId,deptId);
                    if (deanAssignments.containsKey(scope))
                    {
                        roleType = MivRole.DEAN;
                        break;
                    }
                }
            }
            else
            {
                roleType = mivPerson.hasRole(MivRole.DEAN) ? MivRole.DEAN : MivRole.DEPT_CHAIR;
            }
        }
        return roleType;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService#getAuthorizedDocuments(edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public List<DossierDocumentDto> getAuthorizedDocuments(MivRole role)
    {
        return dossierDao.getDocumentListByRole(role);
    }
}
