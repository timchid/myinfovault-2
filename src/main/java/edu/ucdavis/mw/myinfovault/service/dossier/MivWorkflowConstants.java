/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivWorkflowConstants.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

/**
 * Workflow Constants
 */
public class MivWorkflowConstants
{
    public static final String DOSSIER_DOCUMENT_TYPE = "DossierDocument";
    public static final String MIV_NAMESPACE_CODE = "UCD-MIV";

    // Note that these constant values must match the NM column of the KRIM_ATTR_DEFN_T table.
    public static final String SCHOOL_CODE = "schoolCode";
    public static final String DEPARTMENT_CODE = "departmentCode";

    // Note that these constant values must match the ROLE_NM of the KRIM_ROLE_T table.
    public static final String MIV_ADMIN_ROLE_NAME = "MIV-ADMIN";
    public static final String MIV_DEPT_ROLE_NAME = "MIV-DEPT";
    public static final String MIV_DEPT_ASST_ROLE_NAME = "MIV-DEPT-ASST";
    public static final String MIV_VP_ROLE_NAME = "MIV-VP";
    public static final String MIV_SCH_ROLE_NAME = "MIV-SCH";
    public static final String MIV_ARCH_ROLE_NAME = "MIV-ARCH";


    // This value is prepended to the application specific XML document content by the Kuali workflow engine
    public static final String DOCUMENT_CONTENT_KUALI_PROLOG = "/documentContent/applicationContent";

    // These are the various XML node names used in MIV specific document content
    public static final String DOCUMENT_CONTENT_MIV_XMLDATA_NODENAME = "xmlData";
    public static final String DOCUMENT_CONTENT_MIV_DELEGATIONAUTHORITY_NODENAME = "delegationAuthority";
    public static final String DOCUMENT_CONTENT_MIV_SCHOOLDEPARTMENT_NODENAME = "schoolDepartment";
    public static final String DOCUMENT_CONTENT_MIV_ACTIONTYPE_NODENAME = "actionType";

    // Replacements for the KEWConstants
    public static final String ROUTE_HEADER_EXCEPTION_CD = "E";
    public static final String ROUTE_HEADER_ENROUTE_CD = "R";
    public static final String ROUTE_HEADER_FINAL_CD = "F";
    public static final String ROUTE_HEADER_CANCEL_CD = "X";
    public static final String ROUTE_HEADER_PROCESSED_CD = "P";
}
