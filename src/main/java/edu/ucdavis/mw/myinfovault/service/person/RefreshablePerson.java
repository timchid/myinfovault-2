/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RefreshablePerson.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import edu.ucdavis.mw.myinfovault.service.person.PersonDataSource.PersonDTO;


/**
 * Indicate that a Person implementation keeps track of its Object age and can
 * refresh its internal data from a data transfer object.<br>
 * This is used so a long-lived Person object can be updated with new information
 * from the external identity management system without having to destroy and
 * replace it, and so there won't be copies of the Person - some with the old
 * data and some with the new.
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public interface RefreshablePerson
{
    /**
     * Return the system time when this object was created / loaded / or refreshed.
     * @return the time in milliseconds since the epoch
     */
    public long getLoadTime();

    /**
     * Update this person with the data from another person object, and update the freshness timestamp.
     * WARNING! This should only be used internally within the User Service implementation!
     *
     * @param p
     * @return the Person that was refreshed
     */
    public RefreshablePerson freshen(PersonDTO p);
}
