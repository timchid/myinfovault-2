/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationService.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import edu.ucdavis.mw.myinfovault.domain.publications.Publication;
import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;

/**
 * Service for parsing, importing, and retrieving publications.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public interface PublicationService
{
    /**
     * Parses XML within the given file from given publication source.
     *
     * @param content The file pointing to the content to parse
     * @param source The publication source for the content
     * @param targetUser The user ID of the switched-to user
     * @return Parse result and statistics
     * @throws IOException If there's a problem reading/writing files
     * @throws SAXException If unable to parse the given files XML
     * @throws XPathExpressionException If unable to evaluate XML nodes with implementation's XPath expressions
     */
    public ParseResult parsePublications(File content,
                                         PublicationSource source,
                                         int targetUser) throws IOException, SAXException, XPathExpressionException;

    /**
     * Parses and persists all publications from XML within the given file from given publication source.
     *
     * @param content The file pointing to the content to parse
     * @param source The publication source for the content
     * @param realUser The user ID of the real, logged-in user
     * @param targetUser The user ID of the switched-to user
     * @return Import result and statistics
     * @throws IOException If there's a problem reading/writing files
     * @throws SAXException If unable to parse the given files XML
     * @throws XPathExpressionException If unable to evaluate XML nodes with implementation's XPath expressions
     */
    public ImportResult importPublications(File content,
                                           PublicationSource source,
                                           int realUser,
                                           int targetUser) throws IOException, SAXException, XPathExpressionException;

    /**
     * Persists previously parsed publications.
     *
     * @param results List of parse results generated from previous parsing
     * @param realUser The user ID of the real, logged-in user
     * @param targetUser The user ID of the switched-to user
     * @return The updated import result and statistics
     */
    public ImportResult importPublications(List<ParseResult> results,
                                           int realUser,
                                           int targetUser);

    /**
     * Gets the set of publications for the given user ID.
     *
     * @param userId The user ID associated with the publications
     * @return Set of publications associated with the given user ID
     */
    public Set<Publication> getPublications(int userId);

    /**
     * Gets the set of publications from the given source for the given user ID.
     *
     * @param userId The user ID associated with the publications
     * @param source The source from which the publications were originally retrieved
     * @return Set of publications associated with the given user ID
     */
    public Set<Publication> getPublications(int userId, PublicationSource source);
}
