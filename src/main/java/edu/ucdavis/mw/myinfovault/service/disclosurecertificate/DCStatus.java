/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCStatus.java
 */

package edu.ucdavis.mw.myinfovault.service.disclosurecertificate;


/**
 * Represents the status of the disclosure certificate as it pertains to its existence and signature.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public enum DCStatus
{
    /** Has not yet been created. */
    MISSING("Not Added"),

    /** Created and exists, but has not yet been sent to the candidate. */
    ADDED("Added but Not Emailed"),

    /** Sent to the candidate, but has not yet been signed. */
    REQUESTED("Not Signed"),

    /** Sent to the candidate, but a newer edited version exists. */
    REQUESTEDIT("Not Signed"),

    /** Signed and valid. */
    SIGNED("Signed"),

    /** Signed and valid, but a newer edited version exists. */
    SIGNEDIT("Signed"),

    /** Signed and invalid */
    INVALID("Invalid"),

    /** Signed and invalid, but a new edited version exists */
    INVALIDEDIT("Invalid");


    /**
     * Disclosure Certificate status description.
     */
    private String description;

    /**
     * Internally create the enum values.
     */
    private DCStatus(String description)
    {
        this.description = description;
    }

    /**
     * @return the description of this enum value
     */
    public String getDescription()
    {
        return this.description;
    }
}
