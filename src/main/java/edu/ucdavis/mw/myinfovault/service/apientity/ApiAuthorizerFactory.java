/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiAuthorizerFactory.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.security.auth.login.CredentialException;
import javax.security.auth.login.CredentialNotFoundException;

import org.apache.commons.lang.StringUtils;

import com.Ostermiller.util.Base64;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import edu.ucdavis.myinfovault.PropertyManager;

/**
 * Generates authorizers which are designed to authorize for a specific entity/person.
 *
 * @author japorito
 * @since 5.0
 */
public class ApiAuthorizerFactory
{
    private static final Properties authorizers = PropertyManager.getPropertySet("apiauthorizer", "config");
    // Cache to hold loaded ApiEntity objects.
    private static Cache<String, ApiAuthorizer> authorizerCache = CacheBuilder.newBuilder()
        .expireAfterAccess(10, TimeUnit.MINUTES)
        .build();

    /**
     * Return the appropriate authorizer for the scheme and entity name specified in the auth header.
     * Does not validate the API key, it just returns an Authorizer which can.
     *
     * @param authHeader
     * @return An appropriate API authorizer.
     * @throws CredentialException
     */
    public static ApiAuthorizer getAuthorizer(String authHeader) throws CredentialException
    {
        if (StringUtils.isEmpty(authHeader)) throw new CredentialNotFoundException("No Authorization header supplied.");

        ApiSecurityScheme scheme = ApiSecurityScheme.valueOf(StringUtils.substringBefore(authHeader, " ").toUpperCase());
        String entityName = Base64.decode(StringUtils.substringAfter(authHeader, " "));
        entityName = StringUtils.substringBefore(entityName, ":");

        return getAuthorizer(entityName, scheme);
    }

    /**
     * Returns an authorizer for the API entity with the specified name, of the type specified.
     *
     * @param entityName; if an entity does not exist with that name, a blank entity initialized with only the name and scheme specified here will be used.
     * @param scheme
     * @return An appropriate API authorizer.
     * @throws CredentialException
     */
    public static ApiAuthorizer getAuthorizer(String entityName, ApiSecurityScheme scheme) throws CredentialException
    {
        ApiAuthorizer authorizer = authorizerCache.getIfPresent(entityName);

        if (authorizer == null)
        {
            try
            {
                String className = authorizers.getProperty(scheme.toString().toLowerCase());
                Constructor<?> c = Class.forName(className).getConstructor(String.class);
                authorizer = (ApiAuthorizer) c.newInstance(entityName);
            }
            catch (SecurityException |
                   InstantiationException |
                   IllegalAccessException |
                   IllegalArgumentException |
                   InvocationTargetException |
                   NoSuchMethodException |
                   ClassNotFoundException e)
            {
                //authorizer does not seem to exist
                throw new CredentialException();
            }

            authorizerCache.put(entityName, authorizer);
        }

        return authorizer;
    }
}
