/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierDocumentDto.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;


/**
 * This class represents the various properties of a dossier document as defined by a particular MivRole. 
 * 
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class DossierDocumentDto
{
    private String description = null;
    private String fileName = null;
    private String packetFileName = null;
    private String attributeName = null;
    private int collectionId;
    private int documentId;
    private String code = null;
    private String redactedCode = null;
    private Boolean crossDepartmentPermission = false;
    private Boolean allowRedaction = false;
    private Boolean candidateFile = false;
    private DossierAttributeType dossierAttributeType = null;
    private String viewNonRedactedDocIds = null;

    /**
     * @param description the description of this dossier document
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the description of this dossier document
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * @param collectionId the document collection id of this dossier document
     */
    public void setCollectionId(int collectionId)
    {
        this.collectionId = collectionId;
    }

    /**
     * @return the document collection id of this dossier document
     */
    public int getCollectionId()
    {
        return this.collectionId;
    }

    /**
     * @param documentId the document type ID of this dossier document
     */
    public void setDocumentId(int documentId)
    {
        this.documentId = documentId;
    }

    /**
     * @return the document type ID of this dossier document
     */
    public int getDocumentId()
    {
        return this.documentId;
    }

    /**
     * @param crossDepartmentPermission whether or not this document allows viewers from other schools/departments
     */
    public void setCrossDepartmentPermission(Boolean crossDepartmentPermission)
    {
        this.crossDepartmentPermission = crossDepartmentPermission;
    }

    /**
     * @return crossDepartmentPermission whether or not this document allows viewers from other schools/departments
     */
    public Boolean isCrossDepartmentPermission()
    {
        return this.crossDepartmentPermission;
    }
    
    /**
     * @param viewNonRedactedDocIds the non-redacted document IDs allowed to be viewed
     */
    public void setViewNonRedactedDocIds(String viewNonRedactedDocIds)
    {
        this.viewNonRedactedDocIds = viewNonRedactedDocIds;
    }

    /**
     * @return the non-redacted document IDs allowed to be viewed
     */
    public String getViewNonRedactedDocIds()
    {
        return this.viewNonRedactedDocIds;
    }

    /**
     * @param fileName the file name for this dossier document
     */
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    /**
     * @return fileName the file name for this dossier document
     */
    public String getFileName()
    {
        return this.fileName;
    }

    /**
     * @param attributeName the attribute name associated with this dossier document 
     */
    public void setAttributeName(String attributeName)
    {
        this.attributeName = attributeName;
    }

    /**
     * @return the attribute name associated with this dossier document 
     */
    public String getAttributeName()
    {
        return this.attributeName;
    }

    /**
     * setCandidateFile - set whether or not this is a candidate file. A candidate file is a "packet" file created from 
     * the candidate data as opposed to a letter or recommendation, etc.
     * @param candidateFile
     */
    public void setCandidateFile(Boolean candidateFile)
    {
        this.candidateFile = candidateFile;
    }

    /**
     * isCandidateFile - get whether or not this is a candidate file. A candidate file is a "packet" file created from 
     * the candidate data as opposed to a letter or recommendation, etc.
     * @return candidateFile
     */
    public Boolean isCandidateFile()
    {
        return this.candidateFile;
    }

    /**
     * @param dossierAttributeType the dossier attribute type associated with this dossier document
     */
    public void setDossierAttributeType(DossierAttributeType dossierAttributeType)
    {
        this.dossierAttributeType = dossierAttributeType;
    }

    /**
     * @return the dossier attribute type associated with this dossier document
     */
    public DossierAttributeType getDossierAttributeType()
    {
        return this.dossierAttributeType;
    }
    
    /**
     * @param allowRedaction whether or not this dossier document is allowed to be redacted
     */
    public void setAllowRedaction(Boolean allowRedaction)
    {
        this.allowRedaction = allowRedaction;        
    }

    /**
     * @return whether or not this dossier document is allowed to be redacted
     */
    public Boolean isAllowRedaction()
    {
        return this.allowRedaction;        
    }

    /**
     * @param packetFileName packet filename
     */
    public void setPacketFileName(String packetFileName)
    {
        this.packetFileName = packetFileName;
        
    }
    
    /**
     * @return packet filename
     */
    public String getPacketFileName()
    {
        return this.packetFileName;
        
    }

    /**
     * @param code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * @return code
     */
    public String getCode()
    {
        return this.code;
    }
    
    /**
     * @param redactedCode
     */
    public void setRedactedCode(String redactedCode)
    {
        this.redactedCode = redactedCode;
    }

    /**
     * @return redactedCode
     */
    public String getRedactedCode()
    {
        return this.redactedCode;
    }
}
