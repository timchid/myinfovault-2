/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SigningService.java
 */

package edu.ucdavis.mw.myinfovault.service.signature;

import java.util.Collection;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.domain.signature.Signable;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Handles signing and requesting signatures for MIV documents.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public interface SigningService
{
    /**
     * Request a signature from the user specified on a document identified by
     * the unique combination of the document type and the ID of the document.
     *
     * @param document document to be signed
     * @param requestedUserId the MIV user ID of the user requested to sign the document
     * @param realUserId ID of real, logged-in user actually effecting change
     * @return requested signature
     * @throws IllegalArgumentException if the document is <code>null</code>
     */
    public MivElectronicSignature requestSignature(Signable document,
                                                   int requestedUserId,
                                                   int realUserId) throws IllegalArgumentException;

    /**
     * Get all the signatures requested for the indicated user.
     *
     * @param userId MIV user ID
     * @return requested signatures for the user
     */
    public List<MivElectronicSignature> getRequestedSignaturesByUser(int userId);

    /**
     * Get all the signatures requested for the given dossier.
     *
     * @param dossier dossier on which signatures are requested
     * @return requested signatures for the dossier
     */
    public List<MivElectronicSignature> getRequestedSignatures(Dossier dossier);

    /**
     * Get signature requests for the given capacity.
     *
     * @param role capacity of the requested signer (e.g. DEAN, VICE_PROVOST)
     * @return requested signatures
     */
    public List<MivElectronicSignature> getRequestedSignatures(MivRole role);

    /**
     * Get the list of signature requests for the requested signer in the given capacity.
     *
     * @param requested Requested signer
     * @param capacity capacity of the requested signer (e.g. DEAN, VICE_PROVOST)
     * @return requested signatures
     */
    public List<MivElectronicSignature> getRequestedSignatures(MivPerson requested, MivRole capacity);

    /**
     * Get the signature for the given dossier, scope, document type, and user.
     *
     * @param dossier dossier to which the signatures belongs
     * @param schoolId ID of the school to which the signatures belongs
     * @param departmentId ID of the department to which the signatures belongs
     * @param documentType type of document to which the signatures must belong
     * @param userId user ID of the requested signer
     * @return signature with the given parameters
     */
    public MivElectronicSignature getSignature(Dossier dossier,
                                               int schoolId,
                                               int departmentId,
                                               MivDocument documentType,
                                               int userId);

    /**
     * Get the electronic signature for a given recordId
     *
     * @param recordId the signature record ID
     * @return MIV electronic signature or <code>null</code> if DNE
     */
    public MivElectronicSignature getSignatureById(int recordId);

    /**
     * Get the electronic signature for a given document and user.
     *
     * @param document document being signed
     * @param userId MIV user ID
     * @return MIV electronic signature
     */
    public MivElectronicSignature getSignatureByDocumentAndUser(Signable document, int userId);

    /**
     * Get the signed electronic signature for the given document.
     *
     * @param document signed document
     * @return MIV electronic signature or <code>null</code> if DNE
     */
    public MivElectronicSignature getSignedSignatureByDocument(Signable document);

    /**
     * Sign the document.
     *
     * @param signature signature being signed
     * @param realUserId ID of real, logged-in user effecting change
     */
    public void sign(MivElectronicSignature signature, int realUserId);

    /**
     * Invalidate the signature. The signature will remain signed, but not valid.
     *
     * @param signature signature being invalidated
     * @param realPerson real, logged-in user effecting change
     */
    public void invalidate(MivElectronicSignature signature, MivPerson realPerson);

    /**
     * Delete all signatures for the given document.
     *
     * @param document signable document
     * @return signatures deleted from the document
     */
    public Collection<MivElectronicSignature> deleteSignatures(Signable document);

    /**
     * Remove an electronic signature from the system.
     *
     * @param signature electronic signature
     * @return if a signature was deleted
     */
    public boolean deleteSignature(MivElectronicSignature signature);

    /**
     * To remove signature request by primary user (to whom dossier belongs to).
     *
     * @param mivPerson MIV person
     * @return if signature request was removed successfully
     */
    public boolean removeSignatureRequestByPrimaryUser(MivPerson mivPerson);

    /**
     * Get the document for a given type and ID.
     *
     * @param documentType document type
     * @param documentId document ID
     * @return document with matching document type and ID
     */
    public <T extends Signable> T getDocument(MivDocument documentType, int documentId);

    /**
     * Get the document for a given dossier, scope, and type.
     *
     * @param dossier dossier to which the document belongs
     * @param schoolId ID of the school
     * @param departmentId ID of the department
     * @param documentType document type
     * @return signable document matching the criteria
     */
    public <T extends Signable> T getDocument(Dossier dossier,
                                              int schoolId,
                                              int departmentId,
                                              MivDocument documentType);

    /**
     * Get the status of the signable document for the given document type and ID.
     *
     * @param documentType document type
     * @param documentId document ID
     * @return status of the signable document
     */
    public SignatureStatus getSignatureStatus(MivDocument documentType, int documentId);

    /**
     * Get the status of the given signable document.
     *
     * @param document signable document
     * @return status of the signable document
     */
    public SignatureStatus getSignatureStatus(Signable document);
}
