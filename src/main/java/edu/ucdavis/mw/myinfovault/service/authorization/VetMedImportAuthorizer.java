/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: VetMedImportAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;

/**
 * Handles authorization for attempts to view the event log.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class VetMedImportAuthorizer extends RoleBasedAuthorizer
{
    /**
     * Only system administrators may import VetMed course evaluations.
     */
    public VetMedImportAuthorizer()
    {
        super(SYS_ADMIN);
    }
}
