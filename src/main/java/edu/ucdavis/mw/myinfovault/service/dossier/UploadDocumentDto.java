/**
 * Copyright
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UploadDocumentDto.java
 */
package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.File;
import java.text.MessageFormat;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;

/**
 * This class represents an uploaded document
 * @author rhendric
 */
public class UploadDocumentDto
{
    /**
     * Format patter used in {@link #toString()}.
     */
    private static final String TO_STRING = "{0}: dossier={1,number,#}, upload-location={2}, school={3,number,#}, department={4,number,#}";

    private int id;
    private int userId;
    private int updateUserId;
    private int insertUserId;
    private int documentId;
    private int sequence;
    private Boolean display = true;
    private String uploadName = null;
    private String uploadFileName;
    private String year = null;
    private int schoolId;
    private int departmentlId;
    private Boolean redacted = false;
    private Boolean confidential = false;
    private Boolean uploadFirst = false;
    private Integer dossierId;
    private DossierLocation uploadLocation = DossierLocation.UNKNOWN;
    private final File file;

    /**
     * Create an upload document for the given file.
     *
     * @param file abstract path to the uploaded file
     */
    public UploadDocumentDto(File file)
    {
        this.file = file;
        this.uploadFileName = file.getName();
    }

    /**
     * @return abstract path to the uploaded file
     */
    public File getFile()
    {
        return file;
    }

    /**
     * setId - set the id
     * @param id
     */
    public void setId(Integer id)
    {
        this.id = id;
    }

    /**
     * getId - get the id
     * @return id
     */
    public Integer getId()
    {
        return this.id;
    }

    /**
     * setUserId - set the userId
     * @param userId
     */
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    /**
     * getUserId - get the userId
     * @return userId
     */
    public Integer getUserId()
    {
        return this.userId;
    }

    /**
     * setDocumentId - set the documentId
     * @param documentId
     */
    public void setDocumentId(Integer documentId)
    {
        this.documentId = documentId;
    }

    /**
     * getDocumentId - get the documentId
     * @return documentId
     */
    public Integer getDocumentId()
    {
        return this.documentId;
    }

    /**
     * setSequence - set the sorting sequence
     * @param sequence
     */
    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    /**
     * getSequence - get the sorting sequence
     * @return sequence
     */
    public Integer getSequence()
    {
        return this.sequence;
    }

    /**
     * setDisplay - set the display flag
     * @param display
     */
    public void setDisplay(Boolean display)
    {
        this.display = display;
    }

    /**
     * getDisplay - get the display flag
     * @return display
     */
    public Boolean isDisplay()
    {
        return this.display;
    }

    /**
     * setUploadName - set the uploaded file description
     * @param uploadName
     */
    public void setUploadName(String uploadName)
    {
        this.uploadName = uploadName;
    }

    /**
     * getUploadName - get the uploaded file description
     * @return uploadName
     */
    public String getUploadName()
    {
        return this.uploadName;
    }


    /**
     * setUploadFileName - set the uploaded file name
     * @param uploadFileName
     */
    public void setUploadFileName(String uploadFileName)
    {
        this.uploadFileName = uploadFileName;
    }

    /**
     * getUploadFileName - get the uploaded file name
     * @return uploadFileName
     */
    public String getUploadFileName()
    {
        return this.uploadFileName;
    }

    /**
     * setYear - set the uploaded year
     * @param year
     */
    public void setYear(String year)
    {
        this.year = year;
    }

    /**
     * getYear - get the uploaded file year
     * @return uploadFileName
     */
    public String getYear()
    {
        return this.year;
    }

    /**
     * setSchoolId - set the schoolId of the uploaded file
     * @param schoolId
     */
    public void setSchoolId(Integer schoolId)
    {
        this.schoolId = schoolId;
    }

    /**
     * getSchoolId - get the schoolId of the uploaded file
     * @return schoolId
     */
    public int getSchoolId()
    {
        return this.schoolId;
    }

    /**
     * setDepartmentId - set the departmentId of the uploaded file
     * @param departmentId
     */
    public void setDepartmentId(Integer departmentId)
    {
        this.departmentlId = departmentId;
    }

    /**
     * @return the department ID of the uploaded file
     */
    public int getDepartmentId()
    {
        return this.departmentlId;
    }

    /**
     * setRedacted - set the redacted flag for this document
     * @param redacted
     */
    public void setRedacted(Boolean redacted)
    {
        this.redacted = redacted;
    }

    /**
     * setConfidential - set the confidential flag for this document
     * @param confidential
     */
    public void setConfidential(Boolean confidential)
    {
        this.confidential = confidential;
    }

    /**
     * isConfidential - get the confidential flag for this document
     * @return confidential
     */
    public Boolean isConfidential()
    {
        return this.confidential;
    }

    /**
     * @param uploadFirst uploadFirst flag for this document
     */
    public void setUploadFirst(Boolean uploadFirst)
    {
        this.uploadFirst = uploadFirst;
    }

    /**
     * isUploadFirst - get the uploadFirst flag for this document
     * @return uploadFirst
     */
    public Boolean isUploadFirst()
    {
        return this.uploadFirst;
    }

    /**
     * isRedacted - get the redacted flag for this document
     * @return redacted
     */
    public boolean isRedacted()
    {
        return this.redacted;
    }

    /**
     * setDossierId - set the dossier (formerly packet) id for this document
     * @param dossierId
     */
    public void setDossierId(Integer dossierId)
    {
        this.dossierId = dossierId;
    }

    /**
     * getDossierId - get the dossier (formerly packet) id for this document
     * @return dossierId
     */
    public Integer getDossierId()
    {
        return this.dossierId;
    }

    /**
     * @param uploadLocation the dossier location where this document was uploaded
     */
    public void setUploadLocation(DossierLocation uploadLocation)
    {
        this.uploadLocation  = uploadLocation;
    }

    /**
     * @return dossier location where this document was uploaded.
     */
    public DossierLocation getUploadLocation()
    {
        return this.uploadLocation;
    }

    /**
     * setUpdateUserId - set the updateUserId
     * @param updateUserId
     */
    public void setUpdateUserId(Integer updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    /**
     * getUpdateUserId - get the updateUserId
     * @return updateUserId
     */
    public Integer getUpdateUserId()
    {
        return this.updateUserId;
    }

    /**
     * setInsertUserId - set the insertUserId
     * @param insertUserId
     */
    public void setInsertUserId(Integer insertUserId)
    {
        this.insertUserId = insertUserId;
    }

    /**
     * @return the insert user ID
     */
    public Integer getInsertUserId()
    {
        return this.insertUserId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(TO_STRING, this.uploadName,
                                               this.dossierId,
                                               this.uploadLocation,
                                               this.schoolId,
                                               this.departmentlId);
    }
}
