/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DepartmentSuggestionFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.suggest;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.service.ScopeFilter;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Filters out department map entries not within the defined scope(s) or not matching the search term.
 *
 * @author Craig Gilmore
 * @since 4.8
 */
public class DepartmentSuggestionFilter extends ScopeFilter<Map.Entry<String, Map<String,String>>>
{
    private final String searchTerm;

    /**
     * Filtering by search term and scope(s).
     *
     * @param searchTerm term which must exist in the school/department description/abbreviation
     * @param scopes scopes of which the departments must match at least one
     */
    public DepartmentSuggestionFilter(String searchTerm, Collection<Scope> scopes)
    {
        super(scopes);

        this.searchTerm = searchTerm.toLowerCase();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.ScopeFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(Map.Entry<String, Map<String,String>> item)
    {
        /*
         * Department has proper scope AND contains the search
         * term one of its descriptions or abbreviations.
         */
        return super.include(item)
            && (
                   StringUtils.containsIgnoreCase(item.getValue().get("school"), searchTerm)
                || StringUtils.containsIgnoreCase(item.getValue().get("description"), searchTerm)
                || StringUtils.containsIgnoreCase(item.getValue().get("schoolabbrev"), searchTerm)
                || StringUtils.containsIgnoreCase(item.getValue().get("abbreviation"), searchTerm));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.ScopeFilter#getScope(java.lang.Object)
     */
    @Override
    protected Scope getScope(Map.Entry<String, Map<String,String>> item)
    {
        return new Scope(item.getKey());
    }
}
