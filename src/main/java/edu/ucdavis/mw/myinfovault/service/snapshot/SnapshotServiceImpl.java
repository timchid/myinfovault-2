/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SnapshotServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.snapshot;

import java.io.File;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.document.DocumentFormat;


/**
 *
 * @author rhendric
 * @since MIV ?.?
 */
public class SnapshotServiceImpl implements SnapshotService
{
    private static final Logger logger = LoggerFactory.getLogger(SnapshotServiceImpl.class);
    // SLF4J "Markers" are not supported by Log4j, but are supported by "logback" (http://logback.qos.ch/)
//    private static final Marker auditMarker = MarkerFactory.getMarker("|| -----*-----> ");

    private DossierCreatorService dossierCreatorService = null;
    private DossierDao dossierDao = null;

//    private static final DateFormat extensionFormat =
//        new SimpleDateFormat("yyyyMMdd_hhmmssSS");  //new SimpleDateFormat("MMddyyyy_hhmmssSS");
    private static final ThreadLocal<DateFormat> extensionFormat =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyyMMdd_hhmmssSS");
            }
        };


    /**
     * Create snapshot service implementation bean.
     *
     * @param service
     * @param dao
     */
    public SnapshotServiceImpl(DossierCreatorService service, DossierDao dao)
    {
        this.dossierCreatorService = service;
        this.dossierDao = dao;
    }


//    /**
//     * Spring injected DossierDao bean
//     * @param dossierDao
//     * @deprecated Use the constructor arguments, not the setters.
//     */
//    @Deprecated
//    public void setDossierDao(DossierDao dossierDao)
//    {
//        this.dossierDao = dossierDao;
//    }
//
//    /**
//     * Spring injected DossierCreatorService bean
//     * @param dossierCreatorService
//     * @deprecated Use the constructor arguments, not the setters.
//     */
//    @Deprecated
//    public void setDossierCreatorService(DossierCreatorService dossierCreatorService)
//    {
//        this.dossierCreatorService = dossierCreatorService;
//    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.SnapshotService#createDossierSnapshot(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public SnapshotDto createDossierSnapshot(Dossier dossier, MivRole role) throws WorkflowException
    {
        return this.createDossierSnapshot(dossier, role, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService#createDossierSnapshot(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole, java.util.Map)
     */
    @Override
    public SnapshotDto createDossierSnapshot(Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria> searchCriteriaMap) throws WorkflowException
    {
        // Get the snapshot location based on the dossier directory...always PDF format.
        File dossierDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);

        // Create the directory if not present
        if (!dossierDirectory.exists() && !dossierDirectory.mkdirs())
        {
            String msg = "Unable create " + role + " snapshot for dossier " + dossier.getDossierId() +
            " - Dossier directory " + dossierDirectory.getAbsolutePath() + " is not present and could not be created.";
            logger.error(msg);
            throw new WorkflowException(msg);
        }

        // If there is a dossier location, use as the snapshot name prefix
        // If there is no location or the role is Candidate, use PacketRequest for the prefix and location
        String snapshotNamePrefix =  StringUtils.isBlank(dossier.getLocation()) || role == MivRole.CANDIDATE ? "PacketRequest": dossier.getLocation();
        String saveLocation = dossier.getLocation();
        dossier.setLocation(snapshotNamePrefix);
        File outputFile = new File(dossierDirectory, snapshotNamePrefix + "_snapshot_" +
                extensionFormat.get().format(new Date()) + "." + DocumentFormat.PDF.getFileExtension());
        SnapshotDto snapshotDto = dossierCreatorService.createSnapshot(outputFile, dossier, role, searchCriteriaMap);
        dossier.setLocation(saveLocation);
        return snapshotDto;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService#createArchiveSnapshot(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole, java.lang.String)
     */
    @Override
    public SnapshotDto createArchiveSnapshot(Dossier dossier, MivRole role, String name) throws WorkflowException
    {
        // Get the snapshot location based on the dossier directory...always PDF format.
        File archiveDirectory = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF);

        // Create the directory if not present
        if (!archiveDirectory.exists() && !archiveDirectory.mkdirs())
        {
            String msg = "Unable create archive snapshot for dossier "+dossier.getDossierId()+
            " - Archive directory "+archiveDirectory.getAbsolutePath()+" is not present and could not be created.";
            logger.error(msg);
            throw new WorkflowException(msg);
        }

        File outputFile = new File(archiveDirectory, name + "_snapshot_" +
                                   extensionFormat.get().format(new Date()) +
                                   "." + DocumentFormat.PDF.getFileExtension());
        // Set the location to Archive, which will be the next workflow location.
        // All other Snapshots are based on the current location.
        dossier.setLocation(DossierLocation.ARCHIVE.mapLocationToWorkflowNodeName());
        // There will be only a single search criteria in the map which will have school and department id's
        Map<String, SchoolDepartmentCriteria>searchCriteriaMap = new HashMap<String, SchoolDepartmentCriteria>();
        SchoolDepartmentCriteria schoolDepartmentCriteria = new SchoolDepartmentCriteria();
        // "0:0" school department key qualifiers for all archivesn
        schoolDepartmentCriteria.setDepartment(0);
        schoolDepartmentCriteria.setSchool(0);
        searchCriteriaMap.put("0:0", schoolDepartmentCriteria);

        return  dossierCreatorService.createSnapshot(outputFile, dossier, role, searchCriteriaMap);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService#getSnapshot(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole, int, int)
     */
    @Override
    public SnapshotDto getSnapshot(Dossier dossier, MivRole mivRole, int schoolId, int departmentId) throws WorkflowException
    {
        return dossierDao.getSnapshot(dossier, mivRole, schoolId, departmentId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService#getAllSnapshots(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List <SnapshotDto> getAllSnapshots(Dossier dossier) throws WorkflowException
    {
        return dossierDao.getAllSnapshots(dossier);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService#deleteSnapshots(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public void deleteSnapshots(Dossier dossier, DossierLocation location) throws WorkflowException, SQLException
    {
        dossierDao.deleteSnapshots(dossier, location);
    }
}
