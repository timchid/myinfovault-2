/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventLogService.java
 */

package edu.ucdavis.mw.myinfovault.service.event;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry;
import edu.ucdavis.mw.myinfovault.events2.MivEvent;

/**
 * TODO: add javadoc
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public interface EventLogService
{
    List<EventLogEntry> getEntries(long dossierId);
    void logEvent(MivEvent event);
}
