/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonAwareSuggestionHandler.java
 */

package edu.ucdavis.mw.myinfovault.service.suggest;

import edu.ucdavis.mw.common.service.suggest.SuggestionHandler;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Handles preparing suggestions for search criteria for an {@link MivPerson}.
 *
 * @author C
 * @since MIV 4.8
 * @param <E> type of returned suggestions
 */
public interface PersonAwareSuggestionHandler<E> extends SuggestionHandler<E>
{
    /**
     * Get suggestions for the given person, topic, and search term.
     *
     * @param person person requesting the suggestions
     * @param topic suggestion topic
     * @param term search term
     * @return suggestions for the given topic and search term
     */
    public Iterable<E> getSuggestions(MivPerson person, String topic, String term);
}
