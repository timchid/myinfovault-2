/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SharedScopeAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**<p>
 * Authorizes an action when the actor and the target share a scope.
 * Scope varies based on the actors role, for example a department administrator has
 * a scope that must be same-school and same-department, while a school administrator
 * has a scope that must be same-school (any department is acceptable)</p>
 * <p>This differs from the {@link SameScopeAuthorizer} in that if the actor has
 * <em>any</em> assigned scope that matches the target the action is permitted,
 * rather than requiring an exact match with the <em>primary</em> assignment.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.5.2
 */
public class SharedScopeAuthorizer extends PermissionAuthorizerBase implements PermissionAuthorizer
{

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.PermissionAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.PermissionAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        // TODO Auto-generated method stub
        return false;
    }

}
