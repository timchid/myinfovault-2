/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureLogAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CANDIDATE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CHANCELLOR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Handles authorization for attempts to view the signature log.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class SignatureLogAuthorizer extends SameScopeAuthorizer
{
    /**
     * Most people can see the signature log, but will be presented with a filtered view.
     */
    public SignatureLogAuthorizer()
    {
        super(SYS_ADMIN, VICE_PROVOST_STAFF, SENATE_STAFF, SCHOOL_STAFF, DEPT_STAFF, CANDIDATE);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails,
                                AttributeSet qualification)
    {
        /*
         * MIV administrators and executives are authorized to
         * view the signature log associated for any dossier.
         */
        if (person.hasRole(VICE_PROVOST_STAFF, VICE_PROVOST, PROVOST, CHANCELLOR, SYS_ADMIN)) return true;

        if (!hasPermission(person, permissionName, permissionDetails)) return false;

        long dossierId = permissionDetails.get(PermissionDetail.TARGET_ID) != null ? Long.parseLong(permissionDetails.get(PermissionDetail.TARGET_ID)) : 0L;
        if (dossierId == 0) return false;

        try
        {
            Dossier dossier = MivServiceLocator.getDossierService().getDossier(dossierId);

// Thinking about whether to do this -- it would limit Dean, Dept Chair, Vice Provost, Provost, and Chancellor
//            // Candidates can only look at their own signature history.
//            // This includes Deans and Dept. Chairs.
//            if (person.hasRole(CANDIDATE)) {
//            // Could also check if dossier location is at the Archive - those are the only ones a Candidate may view.
//                return dossier.getCandidate().getUserId() == person.getUserId();
//            }

            for (DossierAppointmentAttributeKey key : dossier.getAppointmentAttributes().keySet())
            {
                log.debug("appointment attribute key is [{}]", key);
                Scope s = new Scope(key.toString());
                log.debug("created scope object = {}", s);

                if (hasSharedScope(person, s))
                {
                    log.info("hasSharedScope({}, {}) is true, granting permission.", person.getSortName(), s);
                    return true;
                }
            }
        }
        catch (WorkflowException e)
        {
            // Does getDossier() ever actually throw a WorkflowException any more?
            e.printStackTrace();
        }

        log.info("No shared scope found - denying permission on dossier #{} to {}", dossierId, person);
        return false;
    }
}
