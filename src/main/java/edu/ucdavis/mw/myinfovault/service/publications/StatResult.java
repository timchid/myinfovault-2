/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: StatResult.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.domain.publications.Publication;

/**
 * Holds statistics about a publications import or
 * parse. May be made generic for use elsewhere.
 *
 * @author Craig Gilmore
 * @since MIV 4.4.2
 */
public abstract class StatResult implements Serializable
{
    private static final long serialVersionUID = 201206151215L;

    private static final Logger log = Logger.getLogger(StatResult.class);

    protected int processed = 0;
    protected int duplicates = 0;
    protected List<ParseError> errors = new ArrayList<ParseError>();
    protected Set<Publication> publications = new HashSet<Publication>();

    /**
     * @return Number of publications processed
     */
    public int getProcessed()
    {
        return processed;
    }

    /**
     * @param processed Number of publications processed
     */
    protected void setProcessed(int processed)
    {
        this.processed = processed;
    }

    /**
     * @return Number of duplicate publications found
     */
    public int getDuplicates()
    {
        return duplicates;
    }

    /**
     * @return List of parse errors
     */
    public List<ParseError> getErrors()
    {
        return errors;
    }

    /**
     * @return Set of publications to parsed or imported
     */
    public Set<Publication> getPublications()
    {
        return publications;
    }

    /**
     * @param publications Set of publications to parsed or imported
     */
    protected void setPublications(Set<Publication> publications)
    {
        this.publications = publications;
    }

    /**
     * Log the string representation of this import result instance.
     */
    public void report()
    {
        log.info("\n\n" + this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract String toString();
}
