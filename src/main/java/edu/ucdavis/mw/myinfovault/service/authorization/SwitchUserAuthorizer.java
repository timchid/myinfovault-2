/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SwitchUserAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.APPOINTEE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CANDIDATE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CHANCELLOR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_ASSISTANT;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.OCP_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import com.google.common.base.Splitter;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.StringUtil;


/**<p>
 * Authorizes swtiching to another user.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.6
 */
public class SwitchUserAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /** Roles that are allowed to switch to other users. */
    private static final MivRole[] defaultSwitcherRoles = {
        //SYS_ADMIN,
        VICE_PROVOST_STAFF,
        OCP_STAFF,
        SENATE_STAFF,
        SCHOOL_STAFF,
        DEPT_STAFF,
        DEPT_ASSISTANT,
        DEAN,
        DEPT_CHAIR
    };

    private static UserService userService = MivServiceLocator.getUserService();

    private Properties properties;

    private final Set<MivRole> maySwitchRoles = EnumSet.of(MivRole.SYS_ADMIN, defaultSwitcherRoles); { maySwitchRoles.clear(); }
    private Map<MivRole,List<MivRole>> switcherMap = null;

    public SwitchUserAuthorizer()
    {
        super();
        log.info("no-arg constructor called");
        switcherMap = Collections.unmodifiableMap(switchList);
    }

    public SwitchUserAuthorizer(final Properties props)
    {
        super();
        this.properties = props;
        log.info("1-arg constructor called, Properties are:\n{}", this.properties);

        configureAuthorizer();
    }


    private void configureAuthorizer()
    {
        log.info("configureAuthorizer started, using properties: {}", this.properties);
        log.info("this.properties == null ? {} --- this.properties.isEmpty ? {}",
                 this.properties == null, this.properties != null && this.properties.isEmpty());

        if (this.properties == null || this.properties.isEmpty()) {
            log.info("configure exiting due to missing this.properties");
            return;
        }

        String switchRoles = this.properties.get("roles").toString();
        log.info("roles found: {}", switchRoles);

        if (switcherMap == null) {
            switcherMap = new HashMap<>();
        }
        switcherMap.clear();
        maySwitchRoles.clear();

        Splitter splitter = StringUtil.getListSplitter();

        for (String role : splitter.split(switchRoles))
        {
            MivRole switcherRole = null;
            try {
                switcherRole = MivRole.valueOf(role);

                log.info("parsed {} into role {}", role, switcherRole);
                log.info("process role: {}", role);
                log.info("    value is: {}", this.properties.get("roles." + role));

                String switchToList = this.properties.get("roles." + role).toString();
                List<MivRole> targetsList = new ArrayList<>();
                for (String switchTarget : splitter.split(switchToList))
                {
                    try {
                        MivRole targetRole = MivRole.valueOf(switchTarget);
                        targetsList.add(targetRole);
                    }
                    catch  (IllegalArgumentException e) {
                        log.warn("Skipping invalid role [{}] as target for [{}]", switchTarget, switcherRole);
                    }
                }
                switcherMap.put(switcherRole, targetsList);
                maySwitchRoles.add(switcherRole);
            }
            catch (IllegalArgumentException e) {
                log.warn("Skipping invalid role [{}]", role);
            }
        }

        log.info("final map built is:\n[{}]", switcherMap);
    }


    @Override
    public boolean hasPermission(final MivPerson actor, final String permissionName, final AttributeSet permissionDetails)
    {
        return actor.hasRole(maySwitchRoles);
    }


    @Override
    public boolean isAuthorized(final MivPerson actor, final String permissionName, final AttributeSet permissionDetails, final AttributeSet qualification)
    {
        boolean authorized = false;

        if (hasPermission(actor, permissionName, permissionDetails))
        {
            // See if there is a target user we are switching to
            MivPerson switchTargetPerson = null;
            String qualifier = null;

            String userId = qualification.get(Qualifier.USERID);
            if (userId != null) {
                switchTargetPerson = userService.getPersonByMivId(Integer.parseInt(userId));
                qualifier = "UserID = " + userId;
            }

            // Found a qualifer for a target person, see if actor is allowed to switch to that person.
            if (qualifier != null)
            {
                if (switchTargetPerson != null)
                {
                    MivRole targetRole = switchTargetPerson.getPrimaryRoleType();

                    // Set the target role for Executives since those are not primary roles.
                    if (switchTargetPerson.hasRole(VICE_PROVOST)) {
                        targetRole = VICE_PROVOST;
                    }
                    else if (switchTargetPerson.hasRole(DEAN)) {
                        targetRole = DEAN;
                    }
                    else if (switchTargetPerson.hasRole(DEPT_CHAIR)) {
                        targetRole = DEPT_CHAIR;
                    }
                    else if (switchTargetPerson.hasRole(PROVOST)) {
                        targetRole = PROVOST;
                    }
                    else if (switchTargetPerson.hasRole(CHANCELLOR)) {
                        targetRole = CHANCELLOR;
                    }

                    authorized = this.isAuthorizedByRole(actor, targetRole) && this.hasSharedScope(actor, switchTargetPerson);
                    log.debug("{} permission for {} to switch to {}", new Object[] { authorized ? "Granting" : "Denying", actor, switchTargetPerson });
                }
                else
                {
                    log.error("{}.isAuthorized: Unable to getPersonBy... for qualifier {}", this.getClass().getName(), qualifier);
                    authorized = false;
                }
            }
            else
            // If there is no target user qualifier, then we are only checking that the
            // actor is allowed to switch to the department/school in the qualification.
            {
                String schoolStr = qualification.get(Qualifier.SCHOOL);
                String deptStr = qualification.get(Qualifier.DEPARTMENT);

                if (schoolStr == null || deptStr == null) {
                    authorized = false;
                }
                else {
                    authorized = this.isAuthorizedByScope(actor, new Scope(Integer.parseInt(schoolStr),Integer.parseInt(deptStr)));
                }
                // This next line is equivalent to the if/else above...
                //authorized = schoolStr != null && deptStr != null &&
                //        isAuthorizedByScope(actor, new Scope(Integer.parseInt(schoolStr),Integer.parseInt(deptStr)));
            }
        }

        return authorized;
    }


    /**
     * Determine if <em>any</em> Scope the actor has matches the target's <em>primary</em> appointment/assignment Scope
     * @param actor
     * @param target
     * @return
     */
    @Override
    boolean hasSharedScope(final MivPerson actor, final MivPerson target)
    {
        boolean authorized = false;

        // If the actor has the DEAN or DEPT_CHAIR role, then we cannot check the primary role and must check the
        // DEAN or DEPT_CHAIR roles individually.
        if (actor.hasRole(CANDIDATE) && actor.hasRole(DEAN, DEPT_CHAIR))
        {
            for (AssignedRole actorAssignedRole : actor.getAssignedRoles())
            {
                if (actorAssignedRole.getRole() == DEAN || actorAssignedRole.getRole() == DEPT_CHAIR)
                {
                    for (AssignedRole assignedRole : target.getAssignedRoles())
                    {
                        if (actorAssignedRole.getScope().matches(assignedRole.getScope()))
                        {
                            authorized = true;
                            break;
                        }
                    }
                }
            }

        }
        // If the actor has neither the DEAN or DEPT_CHAIR role, check the primary roles
        else
        {
            for (AssignedRole assignedRole : target.getAssignedRoles())
            {
                if (assignedRole.isPrimary() && hasSharedScope(actor, assignedRole.getScope()))
                {
                    authorized = true;
                    break;
                }
            }
        }

        return authorized;
    }


    private boolean isAuthorizedByRole(final MivPerson actor, final MivRole targetRole)
    {
        boolean authorized = false;
        MivRole mivRole = actor.getPrimaryRoleType();
        List<MivRole> allowedTargetRoles = null;

        // Candidate's are not normally allowed to switch accounts, but they may do so if
        // they are a Dean or a Dept. Chair.
        // If this person's primary role is candidate we need to check for either or both
        // of those and determine which to use.
        if (mivRole == CANDIDATE && actor.hasRole(DEAN, DEPT_CHAIR))
        {
            // Check for authorization using the Dean role...
            if (actor.hasRole(DEAN)) {
                allowedTargetRoles = getAllowedList(DEAN);
                authorized = allowedTargetRoles.contains(targetRole);
            }

            // ...if not authorized or didn't have the Dean role, they may be allowed via the Dept Chair role.
            if (!authorized && actor.hasRole(DEPT_CHAIR)) {
                allowedTargetRoles = getAllowedList(DEPT_CHAIR);
                authorized = allowedTargetRoles.contains(targetRole);
            }
        }
        else // not a Dean or Dept Chair, check all normal roles
        {
            allowedTargetRoles = getAllowedList(mivRole);
            authorized = allowedTargetRoles.contains(targetRole);
        }

        return authorized;
    }


//    /**
//     * isAuthorizedByScope - Check the the scope of any of the actors primary roles
//     * match the input scope
//     * @param actor
//     * @param scope
//     * @return
//     */
//    @SuppressWarnings("unused")
//    private boolean isAuthorizedByScope(MivPerson actor, AttributeSet scope)
//    {
//        return isAuthorizedByScope(actor, new Scope(scope));
//    }


    /**
     * isAuthorizedByScope - Check the the scope of any of the actors primary roles
     * match the input scope
     * @param actor
     * @param scope
     * @return true if authorized, otherwise false
     */
    private boolean isAuthorizedByScope(final MivPerson actor, final Scope scope)
    {
        boolean authorized = false;
        for (AssignedRole ar : actor.getAssignedRoles())
        {
            if ((ar.isPrimary() && ar.getScope().matches(scope)) ||
                (ar.getRole() == DEAN && ar.getScope().matches(scope)) ||
                (ar.getRole() == DEPT_CHAIR && ar.getScope().matches(scope)))
                // Original line says "ar.getClass()"  -NOT-  "ar.getRole()"   Was this a (latent) bug?
                // BTW, this is why you use == on enums instead of .equals()
/*              (ar.getClass().equals(MivRole.DEPT_CHAIR) && ar.getScope().matches(scope)))      */
            {
                authorized = true;
                break;
            }
        }

        return authorized;
    }


    /**
     * Get the list of roles that the given role is allowed to switch to.
     * @param role the role that wants to switch users
     * @return the roles that this role is allowed to switch to, or an empty list if there are none.
     */
    @SuppressWarnings("unchecked") // for casting Collections to List<MivRole>
    private List<MivRole> getAllowedList(final MivRole role)
    {
        List<MivRole> roles = switcherMap.get(role);
        return (List<MivRole>) (roles != null ?
                Collections.unmodifiableList(roles) :
                Collections.emptyList());
    }


    /**
     * Map a Role to the list of Roles an actor is allowed to switch to.
     */
    private static final Map<MivRole,List<MivRole>> switchList = new HashMap<MivRole,List<MivRole>>();
    static {

        // Roles that a SYS_ADMIN is allowed to switch to (Sys Admin)
        List<MivRole> l = new ArrayList<MivRole>();
        switchList.put(SYS_ADMIN, l);
        l.add(SYS_ADMIN);
        l.add(VICE_PROVOST_STAFF);
        l.add(VICE_PROVOST);
        l.add(OCP_STAFF);
        l.add(SENATE_STAFF);
        l.add(SCHOOL_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);
        l.add(APPOINTEE);

        // Roles that VICE_PROVOST_STAFF are allowed to switch to (MIV Admin)
        l = new ArrayList<MivRole>();
        switchList.put(VICE_PROVOST_STAFF, l);
        l.add(VICE_PROVOST_STAFF);
        l.add(VICE_PROVOST);
        l.add(CHANCELLOR);
        l.add(PROVOST);
        l.add(OCP_STAFF);
        l.add(SENATE_STAFF);
        l.add(SCHOOL_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);
        l.add(APPOINTEE);

        // Roles that OCP_STAFF are allowed to switch to (OCP Admin)
        l = new ArrayList<MivRole>();
        switchList.put(OCP_STAFF, l);
        l.add(CHANCELLOR);
        l.add(PROVOST);

        // Roles that SENATE_STAFF are allowed to switch to (Senate Admin)
        l = new ArrayList<MivRole>();
        switchList.put(SENATE_STAFF, l);
        l.add(CANDIDATE);
        l.add(DEPT_CHAIR);
        l.add(DEAN);
        l.add(VICE_PROVOST);

        // Roles that SCHOOL_STAFF are allowed to switch to (School/College Admin)
        l = new ArrayList<MivRole>();
        switchList.put(SCHOOL_STAFF, l);
        l.add(DEPT_STAFF);
        l.add(CANDIDATE);
        l.add(DEPT_CHAIR);
        l.add(DEAN);
        l.add(VICE_PROVOST);
        l.add(APPOINTEE);

        // Roles that DEPT_STAFF are allowed to switch to (Dept. Admin)
        l = new ArrayList<MivRole>();
        switchList.put(DEPT_STAFF, l);
        l.add(CANDIDATE);
        l.add(DEPT_CHAIR);
        l.add(DEAN);
        l.add(VICE_PROVOST);
        l.add(APPOINTEE);

        // Roles that DEPT_ASSISTANT are allowed to switch to (Dept. Helper)
        l = new ArrayList<MivRole>();
        switchList.put(DEPT_ASSISTANT, l);
        l.add(CANDIDATE);
        l.add(DEPT_CHAIR);
        l.add(DEAN);
        l.add(VICE_PROVOST);
        l.add(APPOINTEE);

        // Roles that a DEAN is allowed to switch to (Dean)
        l = new ArrayList<MivRole>();
        switchList.put(DEAN, l);
        l.add(CANDIDATE);

        // Roles that a DEPT_CHAIR is allowed to switch to (Dept. Chair)
        l = new ArrayList<MivRole>();
        switchList.put(DEPT_CHAIR, l);
        l.add(CANDIDATE);
        l.add(APPOINTEE);
    }


    /* "Double Brace Initialization"
    Doesn't need temporary variables, but OTOH produces anonymous inner classes.
     private static final Map<MivRole, List<MivRole>> swtchLst2 =
         new HashMap<MivRole, List<MivRole>>() {
             private static final long serialVersionUID = 1L;
             {
                 put(SYS_ADMIN, new ArrayList<MivRole>() {
                     private static final long serialVersionUID = 1L;
                     {
                         add(SYS_ADMIN);
                         add(VICE_PROVOST);
                         add(VICE_PROVOST_STAFF);
                         add(SCHOOL_STAFF);
                         add(DEPT_STAFF);
                         add(DEPT_ASSISTANT);
                         add(CANDIDATE);
                     }
                 });
                 put(VICE_PROVOST_STAFF, new ArrayList<MivRole>() {
                     private static final long serialVersionUID = 1L;
                     {
                         add(VICE_PROVOST);
                         add(VICE_PROVOST_STAFF);
                         add(SCHOOL_STAFF);
                         add(DEPT_STAFF);
                         add(DEPT_ASSISTANT);
                         add(CANDIDATE);
                     }
                 });
             }
     };
 */
}
