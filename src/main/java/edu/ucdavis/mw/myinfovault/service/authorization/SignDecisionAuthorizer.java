/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignDecisionAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CHANCELLOR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.OCP_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.EnumSet;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVConfig;


/**
 * Handles authorization for attempts to view/sign a executive decision/recommendation.
 *
 * @author Stephen Paulsen
 * @since MIV 4.0
 */
public class SignDecisionAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /**
     * Is this the production environment?
     */
    private static final boolean IS_PRODUCTION = MIVConfig.getConfig().isProductionServer();

    /**
     * Dean, Vice Provost, Provost, and Chancellor roles.
     */
    private static final Set<MivRole> EXECUTIVE_ROLES = EnumSet.of(DEAN, VICE_PROVOST, PROVOST, CHANCELLOR);

    /**
     * Provost and Chancellor roles.
     */
    private static final Set<MivRole> OCP_ROLES = EnumSet.of(PROVOST, CHANCELLOR);

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person,
                                 String permissionName,
                                 AttributeSet permissionDetails)
    {
        MivRole capacity = MivRole.valueOf(permissionDetails.get(PermissionDetail.DECISION_TYPE));

        return hasPermission(person, capacity);
    }

    /**
     * Does the MIV person have permission to view/sign decisions in the given capacity?
     *
     * @param person MIV person attempting to view/sign
     * @param capacity role capacity in which they are attempting to sign
     * @return if the person is permitted to view/sign in the given capacity
     */
    private boolean hasPermission(MivPerson person, MivRole capacity)
    {
        if (log.isDebugEnabled())
        {
            boolean result = EXECUTIVE_ROLES.contains(capacity) && person.hasRole(capacity)
                          || OCP_ROLES.contains(capacity) && person.hasRole(OCP_STAFF);

            log.debug(" ---> hasPermission for [{}/{}] will return '{}'",
                      new Object[] { person.getSortName(), capacity, result }
                     );
            log.debug(" ---> logic statement is:\n" +
                      " ---> ({}).contains({})[{}] && ({}).hasRole({})[{}]\n    -- OR --\n" +
                      " ---> ({}).contains({})[{}] && ({}).hasRole(OCP_STAFF)[{}]",
                      new Object[] {
                                    EXECUTIVE_ROLES, capacity, EXECUTIVE_ROLES.contains(capacity)  ,  person.getSortName(), capacity, person.hasRole(capacity)  ,
                                    OCP_ROLES, capacity, OCP_ROLES.contains(capacity)  ,  person.getSortName(), person.hasRole(OCP_STAFF)
                                   }
                    );
        }

        /*
         * An executive or an OCP administrator for the provost/chancellor
         */
        return EXECUTIVE_ROLES.contains(capacity) && person.hasRole(capacity)
            || OCP_ROLES.contains(capacity) && person.hasRole(OCP_STAFF);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person,
                                String permissionName,
                                AttributeSet permissionDetails,
                                AttributeSet qualification)
    {
        // permission details are required
        if (permissionDetails == null) return false;

        // ID and type of the decision document attempting to sign
        int documentId = Integer.parseInt(permissionDetails.get(PermissionDetail.TARGET_ID));

        // decision document attempting to view/sign
        Decision decision = MivServiceLocator.getDecisionService().getDecision(documentId);

        // Make sure this decision document is at the office location at which it may be signed
        if (decision == null ||
            decision.getDocumentType().getSigningOffice() != DossierLocation.mapLocationToOffice(decision.getDossier().getDossierLocation()))
        {
            return false;
        }

        // real, logged-in person
        int realPersonId = Integer.parseInt(permissionDetails.get(PermissionDetail.ACTOR_ID));

        /*
         * OCP administrators may view chancellor and provost decisions.
         */
        log.debug("isAuthorized is testing permission [{}]", permissionName);
        if (Permission.VIEW_DECISION.equals(permissionName))
        {
            MivPerson realPerson = MivServiceLocator.getUserService().getPersonByMivId(realPersonId);
            if (log.isDebugEnabled())
            {
                log.debug(" ---> This is VIEW_DECISION check");
                log.debug(" --->     Person is {} and has roles [{}]", person.getSortName(), person.getAssignedRoles());
                log.debug(" ---> realPerson is {} and has roles [{}]", realPerson.getSortName(), realPerson.getAssignedRoles());

                log.debug(" ---> OCP_ROLES={} — allowedSigner={} — OCP_ROLES.contains({})={} — realPerson.hasRole(VICE_PROVOST_STAFF)={} — whole value is {}",
                          new Object[] {
                                        OCP_ROLES, decision.getDocumentType().getAllowedSigner(), decision.getDocumentType().getAllowedSigner(),
                                        OCP_ROLES.contains(decision.getDocumentType().getAllowedSigner()),
                                        realPerson.hasRole(VICE_PROVOST_STAFF),
                                        person.hasRole(OCP_STAFF) && OCP_ROLES.contains(decision.getDocumentType().getAllowedSigner()) || realPerson.hasRole(VICE_PROVOST_STAFF)
                                       }
                         );
            }

            if (person.hasRole(OCP_STAFF) && OCP_ROLES.contains(decision.getDocumentType().getAllowedSigner())
             || realPerson.hasRole(VICE_PROVOST_STAFF)) {
                log.debug(" ---> returning TRUE for View Decision");
                return true;
            }
        }

        // scope qualification
        qualification = new AttributeSet();
        qualification.put(Qualifier.DEPARTMENT, Integer.toString(decision.getDepartmentId()));
        qualification.put(Qualifier.SCHOOL, Integer.toString(decision.getSchoolId()));

        /*
         * Signer is authorized if:
         *   - not production OR not a proxy OR a signing on behalf of provost or chancellor
         *   - is not the dossier candidate
         *   - has permission to sign decisions
         *   - has the proper scope
         */
        if (log.isDebugEnabled())
        {
            boolean hasPermission = hasPermission(person, decision.getDocumentType().getAllowedSigner());
            System.out.flush(); System.out.println(" ---> Return from isAuthorized ::");
            System.out.flush(); System.out.println("        return (");
            System.out.flush(); System.out.println("                   !IS_PRODUCTION ["+(!IS_PRODUCTION)+" ... this "+(IS_PRODUCTION ? "IS" : "is NOT")+" production]");
            System.out.flush(); System.out.println("                || realPersonId == person.getUserId() ["+(realPersonId==person.getUserId())+"]");
            System.out.flush(); System.out.println("                || person.hasRole(MivRole.PROVOST, MivRole.CHANCELLOR) ["+person.hasRole(MivRole.PROVOST, MivRole.CHANCELLOR)+"]");
            System.out.flush(); System.out.println("               )");
            System.out.flush(); System.out.print  ("            && decision.getDossier().getAction().getCandidate().getUserId() != person.getUserId() [");
            System.out.flush(); System.out.print  ( decision.getDossier().getAction().getCandidate().getUserId() != person.getUserId() );
            System.out.flush(); System.out.println("]");
            System.out.flush(); System.out.println("            && hasPermission(person, decision.getDocumentType().getAllowedSigner()) ["+hasPermission+"]");
            System.out.flush(); System.out.println("            && hasSameScope(person, qualification); ["+hasSameScope(person, qualification)+"]");
            System.out.flush();
        }

        return (
                   !IS_PRODUCTION       // allow if this is not production
                || realPersonId == person.getUserId()
                || person.hasRole(MivRole.PROVOST, MivRole.CHANCELLOR)
               )
            && decision.getDossier().getAction().getCandidate().getUserId() != person.getUserId() // can't sign your own decision
            && hasPermission(person, decision.getDocumentType().getAllowedSigner())
            && hasSameScope(person, qualification);
    }
}
