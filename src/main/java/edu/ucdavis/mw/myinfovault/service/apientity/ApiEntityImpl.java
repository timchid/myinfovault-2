/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiEntityImpl.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Default implementation of the ApiEntity interface. An entity authorized to act on resources with the specified scopes.
 *
 * @author japorito
 * @since 5.0
 */
public class ApiEntityImpl implements ApiEntity
{

    private Map<ApiResourceType, List<ApiScope>> scopes = new HashMap<ApiResourceType, List<ApiScope>>();
    private String entityName;
    private int entityId;
    private String keyHash;
    private String salt;
    private final ApiSecurityScheme securityScheme;
    private MivRole role = MivRole.API_ENTITY;

    public ApiEntityImpl(int id, String entityName, String keyHash, String salt, ApiSecurityScheme scheme)
    {
        this.entityId = id;
        this.entityName = entityName;
        this.keyHash = keyHash;
        this.salt = salt;
        this.securityScheme = scheme;
    }

    /**
     * Creates a new, blank entity with the given name and security scheme.
     *
     * @param entityName
     * @param scheme
     */
    public ApiEntityImpl(String entityName, ApiSecurityScheme scheme)
    {
        this.entityName = entityName;
        this.securityScheme = scheme;
        this.entityId = -1;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#getPermissions()
     */
    @Override
    public List<ApiScope> getScopes(ApiResourceType resource)
    {
        List<ApiScope> entityScopes = new ArrayList<ApiScope>();

        List<ApiScope> resourceScopes = this.scopes.get(resource);
        if (resourceScopes != null)
        {
            entityScopes.addAll(resourceScopes);
        }

        if (resource != ApiResourceType.ALL)
        {
            entityScopes.addAll(this.getScopes(ApiResourceType.ALL));
        }

        return entityScopes;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#setPermissions(java.util.Map)
     */
    @Override
    public void setScopes(Map<ApiResourceType, List<ApiScope>> scopes)
    {
        this.scopes = scopes;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#getEntityName()
     */
    @Override
    public String getEntityName()
    {
        return entityName;
    }

    /**
     * @return the entityId
     */
    @Override
    public int getEntityId()
    {
        return entityId;
    }

    /**
     * @return the secret
     */
    @Override
    public String getKeyHash()
    {
        return keyHash;
    }

    /**
     * @return the scheme
     */
    @Override
    public ApiSecurityScheme getSecurityScheme()
    {
        return securityScheme;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#getSalt()
     */
    @Override
    public String getSalt()
    {
        return salt;
    }

    /**
     * @param entityName the entityName to set
     */
    @Override
    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#getRole()
     */
    @Override
    public MivRole getRole()
    {
        return role;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#setKeyHash(java.lang.String)
     */
    @Override
    public void setKeyHash(String keyHash)
    {
        this.keyHash = keyHash;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#setSalt(java.lang.String)
     */
    @Override
    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#setEntityId(java.lang.Integer)
     */
    @Override
    public void setEntityId(int entityId)
    {
        this.entityId = entityId;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiEntity#getAllScopes()
     */
    @Override
    public Collection<ApiScope> getAllScopes()
    {
        List<ApiScope> allScopes = new ArrayList<ApiScope>();

        for (ApiResourceType key : scopes.keySet())
        {
            allScopes.addAll(scopes.get(key));
        }

        return allScopes;
    }

    @Override
    public String toString()
    {
        String me = "";

        me = me + entityName + ":" + entityId + ":" + securityScheme.toString() + ":" + role.toString() + ":" + keyHash + ":" + salt;

        return me;
    }
}
