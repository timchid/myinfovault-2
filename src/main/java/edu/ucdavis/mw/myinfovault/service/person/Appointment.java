/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Appointment.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * A Primary / Joint Appointment
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class Appointment extends DomainObject implements Comparable<Appointment>, Serializable
{
    private static final long serialVersionUID = 200906291525L;

    private boolean primary;
    private AppointmentScope scope;
    private int percent;


    /**
     * No-arg constructor for de-serialization to use.
     * <strong>Don't use this constructor programatically!</strong>
     * "Private" so it can be used only be serialization/reflection.
     */
    @SuppressWarnings("unused")
    private Appointment() {}


    /**
     * Create a primary or joint appointment to a school and department.
     *
     * @param school
     * @param department
     * @param isPrimary true for primary, false for joint
     */
    public Appointment(int school, int department, boolean isPrimary)
    {
        this(0, -1, school, department, isPrimary);
    }


    /**
     * Create a primary or joint appointment to a school and department with the given Scope.
     *
     * @param scope
     * @param isPrimary true for primary, false for joint
     */
    public Appointment(Scope scope, boolean isPrimary)
    {
        this(scope.getSchool(), scope.getDepartment(), isPrimary);
    }


    /**
     * Create a primary or joint appointment to a school and department.
     *
     * @param recId
     * @param userId
     * @param school
     * @param department
     * @param isPrimary true for primary, false for joint
     */
    public Appointment(int recId, int userId, int school, int department, boolean isPrimary)
    {
        super(recId, userId);

        this.scope = new AppointmentScope(school, department);

        this.primary = isPrimary;
    }


    /* For serialization/reflection */
    @Override
    public void setId(int id)
    {
        super.setId(id);
    }
    @Override
    public void setUserId(int userId)
    {
        super.setUserId(userId);
    }

    /**
     * @return
     * @deprecated use {@link #getScope()} to access school ID
     */
    @Deprecated
    public int getSchool()
    {
        return this.scope.getSchool();
    }

    /**
     * @return
     * @deprecated use {@link #getScope()} to access school description
     */
    @Deprecated
    public String getSchoolName()
    {
        return this.scope.getSchoolDescription();
    }

    /**
     * @return
     * @deprecated use {@link #getScope()} to access school abbreviation
     */
    @Deprecated
    public String getSchoolAbbreviation()
    {
        return this.scope.getSchoolAbbreviation();
    }

    /**
     * @return
     * @deprecated use {@link #getScope()} to access department ID
     */
    @Deprecated
    public int getDepartment()
    {
        return this.scope.getDepartment();
    }

    /**
     * @return
     * @deprecated use {@link #getScope()} to access department description
     */
    @Deprecated
    public String getDepartmentName()
    {
        return  this.scope.getDepartmentDescription();
    }

    /**
     * @return
     * @deprecated use {@link #getScope()} to access department abbreviation
     */
    @Deprecated
    public String getDepartmentAbbreviation()
    {
        return this.scope.getDepartmentAbbreviation();
    }

    
    public AppointmentScope getScope()
    {
        return this.scope;
    }

    public int getPercent()
    {
        return this.percent;
    }

    /**
     * Set the appointment percentage. A full time appointment is 100%, half-time is 50% etc.
     * @param percent amount of full-time this appointment is. Must be from 0 to 100.
     * @throws IllegalArgumentException if the percent is out of range.
     */
    public void setPercent(int percent)
    {
        if (percent < 0 || percent > 100) {
            throw new IllegalArgumentException("percent must be from 0 to 100");
        }

        this.percent = percent;
    }

    public boolean isPrimary()
    {
        return this.primary;
    }

    public void setPrimary(boolean isPrimary)
    {
        this.primary = isPrimary;
    }

    public void makePrimary()
    {
        this.setPrimary(true);
    }

    public void makeJoint()
    {
        this.setPrimary(false);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return (primary ? "Primary" : "Joint") + " " + scope.getFullDescription() + "  " + percent + "%";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        // We like isAssignableFrom rather than instanceof
        //if (!(obj instanceof Appointment)) return false;
        if (!( Appointment.class.isAssignableFrom(obj.getClass()) )) return false;

        // deferring to scope
        Appointment other = (Appointment) obj;
        return this.scope.equals(other.getScope());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        // deferring to scope
        return this.scope.hashCode();
    }

    @Override
    public int compareTo(Appointment other)
    {
        int compare = 0;
        Boolean myPrimary = new Boolean(this.isPrimary());
        Boolean theirPrimary = new Boolean(other.isPrimary());

        // Primary sorts _before_ Joint, so Primary < Joint
        compare = -myPrimary.compareTo(theirPrimary);
        if (compare != 0) return compare;

        compare = this.scope.getSchoolDescription().compareTo(other.getScope().getSchoolDescription());
        if (compare != 0) return compare;

        compare = this.scope.getDepartmentDescription().compareTo(other.getScope().getDepartmentDescription());
        if (compare != 0) return compare;

        // We should never get here, because that would mean there are two
        // appointments to the same school+department; but if we do, sort by
        // whichever has the higher percentage time.
        compare = this.getPercent() - other.getPercent();

        return compare;
    }
}
