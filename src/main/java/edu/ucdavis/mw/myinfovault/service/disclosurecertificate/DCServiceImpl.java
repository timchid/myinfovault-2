/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.disclosurecertificate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.disclosurecertificate.DCDao;
import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.Rank;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.events2.AcademicActionEvent;
import edu.ucdavis.mw.myinfovault.events2.DisclosureRequestDeleteEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.events2.UploadEvent;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.signature.SignableServiceImpl;
import edu.ucdavis.mw.myinfovault.service.signature.SignatureStatus;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Governs the use of {@link DCBo} business objects.
 *
 * @author Jed Whitten
 * @since MIV 3.0
 */
public class DCServiceImpl extends SignableServiceImpl<DCBo> implements DCService
{
    private final ThreadLocal<DateFormat> signDateFormat =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return SimpleDateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
        }
    };

    /**
     * Disclosure certificate form style filename.
     */
    private static String XSLT_FILENAME = MIVConfig.getConfig().getProperty("document-config-location-xslt-dc");

    private final DCDao dcDao;

    /**
     * Create the disclosure certificate service bean.
     *
     * @param dcDao Spring injected disclosure certificate DAO
     * @param signingService Spring-injected signing service
     */
    public DCServiceImpl(DCDao dcDao, SigningService signingService)
    {
        super(signingService);

        //register listeners for AcademicActionEvent and UploadEvent
        EventDispatcher2.getDispatcher().register(this);

        this.dcDao = dcDao;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#getDisclosureCertificateLatestSigned(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getDisclosureCertificateLatestSigned(Dossier dossier,
                                                     int schoolId,
                                                     int departmentId)
    {
        return dcDao.getLatestSignedDC(dossier,
                                       schoolId,
                                       departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#getDisclosureCertificate(int)
     */
    @Override
    public DCBo getDisclosureCertificate(int recordId)
    {
        return dcDao.getDC(recordId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#getDisclosureCertificateLatestRequest(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getDisclosureCertificateLatestRequested(Dossier dossier,
                                                        int schoolId,
                                                        int departmentId)
    {
        return dcDao.getLatestRequestedDC(dossier,
                                          schoolId,
                                          departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#getDisclosureCertificate(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getDisclosureCertificate(Dossier dossier, int schoolId, int departmentId)
    {
        DCBo dc = dcDao.getLatestWithSignatureDC(dossier, schoolId, departmentId);

        // prefer requested dc
        if (dc != null) return dc;

        // otherwise, the latest draft
        return getDisclosureCertificateDraft(dossier, schoolId, departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#getDisclosureCertificateDraft(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCBo getDisclosureCertificateDraft(Dossier dossier,
                                              int schoolId,
                                              int departmentId)
    {
        /*
         * Return the appropriate DC draft to work on.
         */
        switch(getStatus(dossier, schoolId, departmentId))
        {
            /*
             * A working draft of the DC already exists. Return it.
             */
            case ADDED:
            case REQUESTEDIT:
            case SIGNEDIT:
            case INVALIDEDIT:
                return dcDao.getLatestDC(dossier,
                                         schoolId,
                                         departmentId).refresh();
            /*
             * The latest DC has a signature/request. Return a copy of it.
             */
            case REQUESTED:
            case SIGNED:
            case INVALID:
                return dcDao.getLatestDC(dossier,
                                         schoolId,
                                         departmentId).copy();
            /*
             * DC must be missing. Return a new one.
             */
            default:
                return new DCBo(dossier,
                                schoolId,
                                departmentId);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SignableServiceImpl#persist(edu.ucdavis.mw.myinfovault.domain.signature.Signable, int)
     */
    @Override
    public boolean persist(DCBo dcBo, int realUserId)
    {
        /*
         * Save the DC business object to the
         * database and recreate its PDF.
         */
        return dcDao.persistDC(dcBo, realUserId) && createPdf(dcBo);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#deleteDc(edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo)
     */
    @Override
    public void deleteDc(DCBo dc)
    {
        // delete DC PDF
        dc.getDossierFile().delete();

        // archive if signed
        if (signingService.getSignedSignatureByDocument(dc) != null)
        {
            dcDao.archiveDC(dc.getDocumentId());
        }
        // delete otherwise
        else
        {
            // delete signature/requests applied to the DC
            signingService.deleteSignatures(dc);

            // delete DC record
            dcDao.deleteDC(dc.getDocumentId());
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#resetDisclosureCertificates(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public void resetDisclosureCertificates(Dossier dossier,
                                            int schoolId,
                                            int departmentId,
                                            MivPerson realPerson)
    {
        // latest CDC draft for the given scope
        DCBo latestCDC = dcDao.getLatestDC(dossier, schoolId, departmentId);

        // delete all CDCs for the dossier and scope
        for (DCBo dc : dcDao.getDCs(dossier, schoolId, departmentId))
        {
            deleteDc(dc);
        }

        // persist copy of the latest CDC
        persist(latestCDC.copy(), realPerson.getUserId());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#requestSignature(edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public MivElectronicSignature requestSignature(DCBo dc, MivPerson realPerson)
    {
        DCStatus status = getStatus(dc);

        // if a signature has already been requested
        if (DCStatus.REQUESTEDIT == status)
        {
            // remove signature from the latest requested DC
            for (DCBo unarchivedDC : dcDao.getAllWithSignatureDC(dc.getDossier(),
                                                                 dc.getSchoolId(),
                                                                 dc.getDepartmentId()))
            {
                //all signed DCs will be archived, and all superseded requests will be deleted
                deleteDc(unarchivedDC);
            }
        }
        // Archive previous DC if it has already been signed.
        else if (DCStatus.SIGNEDIT == status || DCStatus.INVALIDEDIT == status)
        {
            dcDao.archiveSignedDC(dc.getDossier(), dc.getSchoolId(), dc.getDepartmentId());
        }

        // request signature for the given DC
        try
        {
            MivElectronicSignature signature = signingService.requestSignature(dc,
                                                        dc.getDossier().getAction().getCandidate().getUserId(),
                                                        realPerson.getUserId());

            //log state of signature
            log.info("_DC_AUDIT" + signature);

            // persist DC now in new state
            persist(dc, realPerson.getUserId());

            return signature;
        }
        catch (IllegalArgumentException e)
        {
            throw new MivSevereApplicationError("errors.dc.sign.request", e, dc);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SignableService#createPdf(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public boolean createPdf(final DCBo dc)
    {
        return dc != null
            && createPdf(dc.getDossierFile(),
                         XSLT_FILENAME,
                         getXmlDoc(dc)).exists();
    }

    /**
     * Creates XML document from DC business object.
     *
     * @param dc
     * @return DOM element corresponding to the given DC
     */
    private Document getXmlDoc(DCBo dc)
    {
        //create document
        Document doc = getDocument();

        // append DC element
        doc.appendChild(getDCElement(doc, dc));

        return doc;
    }

    /**
     * Creates and returns the DC node.
     *
     * @param doc
     * @param dc
     * @return DOM element for the DC
     */
    private Element getDCElement(Document doc, DCBo dc)
    {
        //create root node
        Element root = doc.createElement("dc");

        // append DC fields
        root.appendChild(getElement(doc, "documentname", dc.getDocumentType().getAlternateDescription()));
        root.appendChild(getElement(doc, "delegationauthority", dc.getDossier().getAction().getDelegationAuthority().getDescription()));
        root.appendChild(getElement(doc, "actiontype", dc.getDossier().getAction().getActionType().getDescription()));
        root.appendChild(getElement(doc, "actiondescription", dc.getDossier().getAction().getDescription()));
        root.appendChild(getElement(doc, "effectivedate", longDateFormat.get().format(dc.getDossier().getAction().getEffectiveDate())));
        root.appendChild(getElement(doc, "candidatename", dc.getDossier().getAction().getCandidate().getDisplayName()));
        root.appendChild(getElement(doc, "appointmentname", dc.getScope().getFullDescription()));

        if (StringUtils.isNotBlank(dc.getAdditionalInfo()))
        {
            root.appendChild(getElement(doc, "additionalinfo", dc.getAdditionalInfo()));
        }

        //append ranks
        root.appendChild(getRanksElement(doc, dc));

        //append signature
        root.appendChild(getSignatureElement(doc, dc));

        return root;
    }

    /**
     * Creates and returns the ranks node.
     *
     * @param doc
     * @param dc
     * @return DOM element for DC rank and steps element
     */
    private Element getRanksElement(Document doc, DCBo dc)
    {
        // create root node
        Element root = doc.createElement("ranks");

        // append rank and steps
        for (Rank rankAndStep : dc.getRanks()) root.appendChild(getRankElement(doc, rankAndStep));

        return root;
    }

    /**
     * Creates and returns a rank and step node.
     *
     * @param doc
     * @param rank
     * @return DOM element for DC rank and step
     */
    private Element getRankElement(Document doc, Rank rank)
    {
        Element elem = doc.createElement("rank");

        if (StringUtils.isNotBlank(rank.getPresentPercentOfTime()))
        {
            elem.appendChild(getElement(doc, "presentpercentoftime", rank.getPresentPercentOfTime()));
        }

        if (StringUtils.isNotBlank(rank.getPresentRankAndStep()))
        {
            elem.appendChild(getElement(doc, "presentrankandstep", rank.getPresentRankAndStep()));
        }

        if (StringUtils.isNotBlank(rank.getProposedPercentOfTime()))
        {
            elem.appendChild(getElement(doc, "proposedpercentoftime", rank.getProposedPercentOfTime()));
        }

        if (StringUtils.isNotBlank(rank.getProposedRankAndStep()))
        {
            elem.appendChild(getElement(doc, "proposedrankandstep", rank.getProposedRankAndStep()));
        }

        return elem;
    }

    /**
     * Creates and returns the signature node.
     *
     * @param doc
     * @param dc
     * @return DOM element for DC signature
     */
    private Element getSignatureElement(Document doc, DCBo dc)
    {
        //create root node
        Element root = doc.createElement("signature");

        //get signature for this document
        MivElectronicSignature signature = signingService.getSignatureByDocumentAndUser(dc, dc.getDossier().getAction().getCandidate().getUserId());

        //DC signature signed? invalid?
        boolean signed = signature != null && signature.isSigned();
        boolean invalid = signed && !signature.isValid();

        // set name and date signature fields
        if (signed)
        {
            root.appendChild(getElement(doc, "name", dc.getDossier().getAction().getCandidate().getDisplayName()));
            root.appendChild(getElement(doc, "date", signDateFormat.get().format(signature.getSigningDate())));
        }

        // if revised has been selected
        if (dc.isRevised())
        {
            //if signed get the previously signed DC; else get the latest signed
            DCBo signedDcBo = signed
                            ? dcDao.getPreviouslySignedDC(dc.getDossier(), dc.getSchoolId(), dc.getDepartmentId())
                            : getDisclosureCertificateLatestSigned(dc.getDossier(), dc.getSchoolId(), dc.getDepartmentId());

            if (signedDcBo != null)
            {
                //get signature for this document
                signature = signingService.getSignatureByDocumentAndUser(signedDcBo, signedDcBo.getDossier().getAction().getCandidate().getUserId());

                //signature exists and is signed
                if (signature != null && signature.isSigned())
                {
                    root.appendChild(getRevisionElement(doc, dc.getChangesAdditions(), signature.getSigningDate()));
                }
            }
        }

        Date emailedDate = signature != null ? signature.getCreatedDate() : new Date();

        root.appendChild(getElement(doc, "emaileddate", longDateFormat.get().format(emailedDate)));

        // add the invalid signature message?
        if (invalid)
        {
            root.appendChild(getElement(doc, "invalidmessage", this.getInvalidSignatureMessage(dc.getDocumentType())));
        }

        return root;
    }

    /**
     * Creates and returns the revision node.
     *
     * @param doc
     * @param changes changes and additions in revision
     * @param date when signature to be revised was signed
     * @return DOM element for signature revision
     */
    private Element getRevisionElement(Document doc,
                                       String changes,
                                       Date date)
    {
        //create root node
        Element root = doc.createElement("revision");

        root.appendChild(getElement(doc, "changes", changes));
        root.appendChild(getElement(doc, "date", signDateFormat.get().format(date)));

        return root;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService#getStatus(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public DCStatus getStatus(Dossier dossier,
                              int schoolId,
                              int departmentId)
    {
        /*
         * Latest two DCs for the dossier and scope.
         * First in the iteration is the most recent.
         */
        Iterator<DCBo> dcs = dcDao.getLatestTwoDCs(dossier,
                                                   schoolId,
                                                   departmentId).iterator();

        // no DC yet exists
        if (!dcs.hasNext()) return DCStatus.MISSING;

        // latest DC signature status
        SignatureStatus latest = signingService.getSignatureStatus(dcs.next());

        // no signature/request for the latest, defer to the former
        if (latest == SignatureStatus.MISSING)
        {
            // former DC signature status
            SignatureStatus former = dcs.hasNext() ? signingService.getSignatureStatus(dcs.next()) : SignatureStatus.MISSING;

            // former is also missing, now editing
            if (former == SignatureStatus.MISSING) return DCStatus.ADDED;

            // former was requested, now editing
            if (former == SignatureStatus.REQUESTED) return DCStatus.REQUESTEDIT;

            // former was signed, now editing
            if (former == SignatureStatus.SIGNED) return DCStatus.SIGNEDIT;

            // former is invalid, now editing
            return DCStatus.INVALIDEDIT;
        }

        // latest is requested
        if (latest == SignatureStatus.REQUESTED) return DCStatus.REQUESTED;

        // latest is signed
        if (latest == SignatureStatus.SIGNED) return DCStatus.SIGNED;

        // latest is invalid
        return DCStatus.INVALID;
    }

    /**
     * Gets the disclosure certificates status as it pertains to it's existence and signature.
     *
     * @param dc disclosure certificate
     * @return DC status
     */
    private DCStatus getStatus(DCBo dc)
    {
        // assume missing if null
        return dc == null
             ? DCStatus.MISSING
             : getStatus(dc.getDossier(),
                         dc.getSchoolId(),
                         dc.getDepartmentId());
    }

    /**
     * Remove CDC signature requests and regenerate signed PDFs in response to an event.
     *
     * @param parent parent event
     * @param schoolID school ID
     * @param deptID department ID
     */
    private void updateSignature(DossierEvent parent, int schoolID, int deptID)
    {
        // get the latest CDC signature/request for this dossier and scope
        DCBo latest = dcDao.getLatestWithSignatureDC(parent.getDossier(), schoolID, deptID);
        MivElectronicSignature signature = signingService.getSignatureByDocumentAndUser(latest, parent.getDossier().getAction().getCandidate().getUserId());

        if (signature != null)
        {
            // signature is SIGNED
            if (signature.isSigned())
            {
                /*
                 * Uploads and RAF changes now invalidate signed CDCs.
                 * However, signatures validly signed prior to version 3 do
                 * not, and must be invalidated manually.
                 */
                if (signature.getDocumentVersion() < 3 && signature.isValid())
                {
                    signingService.invalidate(signature, parent.getRealPerson());
                }
            }
            // otherwise, signature is a REQUEST
            else
            {
                // rescind the signature request
                signingService.deleteSignature(signature);

                EventDispatcher2.getDispatcher().post(new DisclosureRequestDeleteEvent(parent, signature));
            }

            // regenerate the CDC PDF
            createPdf(latest);
        }
    }

    /**
     * Respond to an upload event.
     *
     * @param event Upload event
     */
    @Subscribe
    public void uploadEventListener(UploadEvent event)
    {
        // upload event occurred at the DEPARTMENT
        if (event.getUploadDocument().getUploadLocation() == DossierLocation.DEPARTMENT)
        {
            updateSignature(event,
                            event.getUploadDocument().getSchoolId(),
                            event.getUploadDocument().getDepartmentId());
        }
    }

    /**
     * Respond to an academic action event
     *
     * @param event Academic action event
     */
    @Subscribe
    public void academicActionEventListener(AcademicActionEvent event)
    {
        AcademicAction formerAction = event.getFormerAction();

        // If the action has changed, reset the DCs
        if (formerAction != null && event.getDossier().getAction().getActionType() != formerAction.getActionType())
        {
            for (DossierAppointmentAttributeKey key : event.getDossier().getAppointmentAttributes().keySet())
            {
                // reset the CDC for this scope
                resetDisclosureCertificates(event.getDossier(),
                                            key.getSchoolId(),
                                            key.getDepartmentId(),
                                            event.getRealPerson());
            }
        }
        // otherwise, if academic action event occurred at the DEPARTMENT
        else if (event.getDossier().getDossierLocation() == DossierLocation.DEPARTMENT)
        {
            for (DossierAppointmentAttributeKey key : event.getDossier().getAppointmentAttributes().keySet())
            {
                updateSignature(event,
                                key.getSchoolId(),
                                key.getDepartmentId());
            }
        }
    }
}
