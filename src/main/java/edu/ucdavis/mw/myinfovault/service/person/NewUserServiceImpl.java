/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: NewUserServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.UncheckedExecutionException;

import edu.ucdavis.mw.myinfovault.dao.person.PersonDao;
import edu.ucdavis.mw.myinfovault.dao.person.UserAccountDao;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events.UserActivatedEvent;
import edu.ucdavis.mw.myinfovault.events.UserDeactivatedEvent;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.person.PersonDataSource.PersonDTO;
import edu.ucdavis.mw.myinfovault.service.person.builder.BuiltPersonImpl;
import edu.ucdavis.mw.myinfovault.service.person.builder.MivAccountDto;
import edu.ucdavis.mw.myinfovault.service.person.builder.PersonBuilder;
import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.Duration;
import edu.ucdavis.myinfovault.ConfigReloadConstants;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.exception.ServiceUnavailableException;

/**
 * Implementation of the UserService interface.
 * Requires a PersonDataSource is provided.
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class NewUserServiceImpl implements UserService, InitializingBean
{
    private static Logger logger = LoggerFactory.getLogger(NewUserServiceImpl.class);

    private final PersonDataSource personDataSource;
    private final PersonIdMapper mapper;
    private final PersonDao personDao;
    private UserAccountDao accountDao;
    private LoginDao loginDao;


    private static final ThreadLocal<Integer> currentUser =
        new ThreadLocal<Integer>() {
            @Override protected Integer initialValue() {
                return -1;
            }
    };


    public NewUserServiceImpl(PersonDataSource dataSource, PersonIdMapper personIdMapper, PersonDao personDao, LoginDao loginDao)
    {
        this.personDataSource = dataSource;
        this.mapper = personIdMapper;
        this.personDao = personDao;
        this.loginDao = loginDao;

        logger.info("|| ----> Registering UserService {} as a Change Listener", this.getClass().getSimpleName());
        EventDispatcher.getDispatcher().register(this);
    }


    public void setUserAccountDao(UserAccountDao dao)
    {
        logger.debug("|| --- Hey! NewUserServiceImpl.setUserAccountDao() got called! It's MAGIC!");
        this.accountDao = dao;
    }


    /* (non-Javadoc)
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception
    {
        logger.debug("|| --- Hey! NewUserServiceImpl.afterPropertiesSet() got called! It's MAGIC!");
    }


    public void init()
    {
        logger.debug("|| --- Hey! NewUserServiceImpl.init() got called! It's MAGIC!");
    }



    /**
     * Can we use AOP to set the current user, rather than adding acting-userId to every API method?
     * @param userId
     */
    public void setCurrentUserId(int userId)
    {
        currentUser.set(userId);
    }






    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getPeopleByMivId(int[])
     */
    @Override
    public Set<MivPerson> getPeopleByMivId(int... userids)
    {
        Set<MivPerson> people = new HashSet<MivPerson>();

        // Get person for each userid
        for (int userid : userids)
        {
            people.add(this.getPersonByMivId(userid));
        }

        // Remove any null people (when a provided userId did not correspond to an MIV User)
        people.remove(null);

        return people;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getMivUserByPersonUuid(java.lang.String)
     */
    @Override
    public MIVUserInfo getMivUserByPersonUuid(String personUuid)
    {
        if (personUuid == null) return null;

        String uid = mapper.getUserIdForPersonId(personUuid);
        if (uid == null) return null;

        MIVUserInfo mui = null;

        int userId = Integer.parseInt(uid);
        if (userId > 0) {
            mui = new MIVUserInfo(userId);
        }

        return mui;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getMivUserByPrincipalName(java.lang.String)
     */
    @Override
    public MIVUserInfo getMivUserByPrincipalName(String principalName)
    {
        if (principalName == null) return null;

        String uid = mapper.getUserIdForPrincipalName(principalName);
        if (uid == null) return null;

        MIVUserInfo mui = null;

        int userId = Integer.parseInt(uid);
        if (userId > 0) {
            mui = new MIVUserInfo(userId);
        }

        return mui;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getMivUserById(int)
     */
    @Override public MIVUserInfo getMivUserById(int userId)
    {
        if (userId <= 0) return null;
        return new MIVUserInfo(userId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getPersonByMivId(int)
     */
    @Override
    public MivPerson getPersonByMivId(int userid)
    {
        //Preconditions.checkArgument(userid>0, "Invalid UserID %s <= 0", userid); // Guava library example.
        if (userid <= 0) return null;
        return getPersonByMivId(Integer.toString(userid));
    }
    private MivPerson getPersonByMivId(String userId)
    {
        logger.debug("|| -- get person for user ID string [{}]", userId);
        return
            StringUtils.isBlank(userId)
                ? null
                : getPersonByMivIdCached(userId);
    }



    /**
     * SDP - Guava-based alternative for {@link #getPersonByMivId(String)} using the Guava CacheBuilder.
     * @param userId
     * @return
     */
    private MivPerson getPersonByMivIdCached(String userId)
    {
        MivPerson person = null;
        try
        {
            // This can throw a ServiceUnavailableException from the depths. How should we handle it?
            person = personCache.get(userId);
//            if (person != nullPerson) {
//                freshen(person);
//            }
        }
        catch (ExecutionException e)
        {
            logger.error("|| -- Problem with cache-loading person " + userId, e.getCause());
        }
        catch (UncheckedExecutionException e)
        {
            logger.error("|| -- Problem with cache-loading person " + userId, e.getCause());
        }
        return person != nullPerson ? person : null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getPersonByPersonUuid(java.lang.String)
     */
    @Override
    public MivPerson getPersonByPersonUuid(String personUuid)
    {
        String userId = mapper.getUserIdForPersonId(personUuid);
        if (userId != null) {
            return this.getPersonByMivId(userId);
        }

        // Given a PersonUUID we did *not* find an MIV person...
        // Get just a non-user person (which won't be cached)
        MivPerson person = null;
        PersonDTO pdto = this.personDataSource.getPersonFromId(personUuid);

        // If we got person info from the data source we can build a non-miv-account Person,
        // otherwise neither MIV account nor Person information was found, and we'll leave
        // the returned "person" as null.
        if (pdto != null)
        {
            // ...build a Person with the pdto and no adto
            person = PersonBuilder.newBuilder()
                 // .withAccount(adto) // no Account, just a Person
                    .withPersonData(pdto)
                    .build();
        }

        return person;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getPersonByPrincipalName(java.lang.String)
     */
    @Override
    public MivPerson getPersonByPrincipalName(String principalName)
    {
        return this.getPersonByMivId(mapper.getUserIdForPrincipalName(principalName));
    }


    /**
     * Not part of the official API yet - this will be the "PersonId" which is EITHER mothraId or iamId
     */
    /* Actually, all other methods ultimately call this one. */
    @Override
    public MivPerson getPersonByPersonId(String personId)
    {
        /* ... Not So Fast!
         * If we ask for a person by PersonId (or PersonUUID) and the mapper doesn't find
         * an MIV UserId for them, we should try to get them from the PersonDataSource
         */
        final String userId = mapper.getUserIdForPersonId(personId);
        return userId != null
                ? this.getPersonByMivId(userId)
                : this.getPersonByPersonUuid(personId);
    }



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#savePerson(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public MivPerson savePerson(MivPerson person, int actorId)
    {
        Boolean saved;
        if (person.getUserId() == -1)
        {
            saved = personDao.insertPerson(person, actorId);
        }
        else
        {
            saved = personDao.updatePerson(person, actorId);

            // Invalidate the cache entry for the saved person.
            this.invalidateCacheEntry(person);
        }

        return saved ? person : null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#addPerson(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    //@/Transactional
    public boolean addPerson(MivPerson person)
    {
        // Have the meta-dao handle the person / roles / scopes
        boolean inserted = personDao.insertPerson(person, currentUser.get());

        // persist Account
//      addAccount(person);
        // persist Roles
        // persist Appointments

        if (inserted) {

            // persist Personal Info
            //   -- EmployeeID
            //   -- Email
            //   -- Display Name
            // persist Login(s)
            {
                Login login = new Login(person.getUserId(), person.getPrincipal().getPrincipalName(), null, null);
                logger.info("Add Person saving Person with Login = [{}]", login);
                loginDao.save(login);
            }
        }

        return inserted;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getUsersBySchool(int)
     */
    @Override
    public List<MivPerson> getUsersBySchool(int mivSchoolId)
    {
        // Finding by school is the same as finding by department with a dept. ID of zero.
        return getUsersByDepartment(mivSchoolId, 0);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getUsersBySchool(int, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<MivPerson> getUsersBySchool(int mivSchoolId, SearchFilter<MivPerson> filter)
    {
        List<MivPerson> people = getUsersBySchool(mivSchoolId);
        return (filter == null ? people : filter.apply(people));
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getUsersByDepartment(int, int)
     */
    @Override
    public List<MivPerson> getUsersByDepartment(int mivSchoolId, int mivDeptId)
    {
        return getUsersByDepartment(mivSchoolId, mivDeptId, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getUsersByDepartment(int, int, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<MivPerson> getUsersByDepartment(int mivSchoolId, int mivDeptId, SearchFilter<MivPerson> filter)
    {
        // This accepts school=0 and/or dept=0 to mean don't-care
        // e.g. school=1 dept=0 returns all in school #1 just like getUsersBySchool(1) does.

        final AttributeSet criteria = new AttributeSet();
        if (mivSchoolId != 0) {
            criteria.put(MivSearchKeys.Person.MIV_SCHOOL_CODE, Integer.toString(mivSchoolId));
        }
        if (mivDeptId != 0) {
            criteria.put(MivSearchKeys.Person.MIV_DEPARTMENT_CODE, Integer.toString(mivDeptId));
        }
        criteria.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");

        List<MivPerson> people = findUsers(criteria);

        // Filter them if a filter was provided
        return (filter == null ? people : filter.apply(people));
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getUsersByRole(edu.ucdavis.mw.myinfovault.service.person.MivRole, java.lang.String)
     */
    @Override
    public List<MivPerson> getUsersByRole(MivRole role, Scope scope)
    {
        //Preconditions.checkArgument(role != null, "role can not be null");//moved into the filtered getUsersByRole
        return getUsersByRole(role, scope, null);
    }
    /*
       FIXME: Oy!  These two ..ByRole() methods never worked right ...
              The passed in SCOPE was never used by either of these!
       The only callers, validateDossierRouting in the MivWorkflowPostProcessor
       and resolveDeansAuthority in EditAction, passed (null) for Scope so it was
       never a problem, and the calls are gone now.
    */
    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#getUsersByRole(edu.ucdavis.mw.myinfovault.service.person.MivRole, java.lang.String, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<MivPerson> getUsersByRole(MivRole role, Scope scope, SearchFilter<MivPerson> filter)
    {
      //if (role == null) throw new IllegalArgumentException("role can not be null");
        Preconditions.checkArgument(role != null, "role can not be null");

        final AttributeSet criteria = new AttributeSet();
        criteria.put(MivSearchKeys.Person.MIV_ROLE, role.getRoleId()+"");
        criteria.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");

        List<MivPerson> people = findUsers(criteria);

        return (filter == null ? people : filter.apply(people));
    }



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#findPersonByEmail(java.lang.String)
     */
    @Override
    public List<MivPerson> findPersonByEmail(String emailAddress)
    {
        if (StringUtils.isBlank(emailAddress)) { // if given nothing return nothing
            return Collections.emptyList();
        }

        Collection<PersonDTO> found = personDataSource.getPersonFromEmail(emailAddress);

        List<MivPerson> foundPeople = (found.size() <= 1 ? new ArrayList<MivPerson>(1) : new LinkedList<MivPerson>());

        // Now build people from the DTOs

        for (PersonDTO pdto : found)
        {
            String personId = pdto.getPersonId();
            MivPerson mivPerson = this.getPersonByPersonId(personId);
            logger.info("Person by email [{}] == {}", emailAddress, mivPerson);

        // Temp, to compare the ADTO with the mivPerson found. The "adto =" line belongs below, after the 'continue' (or ... Not)
            MivAccountDto adto = accountDao.getAccountByPersonId(personId);
        logger.info("Account DTO for person is [{}]", adto);

            // If we DID find this "mivPerson" we should immediately add to foundPeople then 'continue;' the loop
            if (mivPerson != null) {
                foundPeople.add(mivPerson);
                continue;
            }
// In fact, getPersonByPersonId should get someone, MIV user or not, so this code would never be reached.
            adto = accountDao.getAccountByPersonId(personId); // Can this be removed? If adto is non-null we would have found the person above.
            MivPerson person = PersonBuilder.newBuilder()
                               .withAccount(adto)
                               .withPersonData(pdto)
                               .build();
            // Now we should look for an AccountDTO ...  accountDao.getAccountByUserId(userId);
            // see Line 663ish in loadPerson()
            if (person != null) {
                foundPeople.add(person);
            }
        }


        return foundPeople;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#findUsers(edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public List<MivPerson> findUsers(AttributeSet criteria)
    {
        List<MivAccountDto> accounts = this.accountDao.findUsers(criteria);
        List<MivPerson> people = new ArrayList<MivPerson>(accounts.size());

        for (MivAccountDto account : accounts)
        {
            //We were getting people by uuid here, but Appointees might not have their person UUID in the UserAccount table.
            MivPerson person = this.getPersonByMivId(account.getUserId());
            if (person != null)
            {
                people.add(person);
            }
        }

        return people;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#findUsers(edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<MivPerson> findUsers(AttributeSet criteria, SearchFilter<MivPerson> filter)
    {
        List<MivPerson> people = findUsers(criteria);
        return (filter == null ? people : (List<MivPerson>)filter.apply(people));
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#findAppointeesBySimilarName(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<MivPerson>findAppointeesBySimilarName(final MivPerson person)
    {
        return new SimilarNameFilter(person).apply(this.getUsersByRole(MivRole.APPOINTEE, Scope.AnyScope));
    }



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#activate(edu.ucdavis.mw.myinfovault.service.person.MivPerson[])
     */
    @Override
    public Map<MivPerson, Map<String, Boolean>> activate(MivPerson... users)
    {
        return this.setActivation(true, users);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#activate(java.lang.Iterable)
     */
    @Override
    public Map<MivPerson, Map<String, Boolean>> activate(Iterable<MivPerson> users)
    {
        return this.setActivation(true, users);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#deactivate(edu.ucdavis.mw.myinfovault.service.person.MivPerson[])
     */
    @Override
    public Map<MivPerson, Map<String, Boolean>> deactivate(MivPerson... users)
    {
        return this.setActivation(false, users);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#deactivate(java.lang.Iterable)
     */
    @Override
    public Map<MivPerson, Map<String, Boolean>> deactivate(Iterable<MivPerson> users)
    {
        return this.setActivation(false, users);
    }


    /** Used internally by activate and deactivate */
    private Map<MivPerson,Map<String,Boolean>> setActivation(final boolean activating, final MivPerson... users)
    {
        return setActivation(activating, Arrays.asList(users));
    }


    /** Used internally by activate and deactivate */
    private Map<MivPerson,Map<String,Boolean>> setActivation(final boolean activating, final Iterable<MivPerson> users)
    {
        Map<MivPerson,Map<String,Boolean>> activationResults = new HashMap<MivPerson,Map<String,Boolean>>();
        Map<String, Boolean>result = new HashMap<String, Boolean>();
        result.put("success", false);

        for (final MivPerson user : users)
        {

            boolean currentState = user.isActive();
            if (currentState == activating) continue; // no change being made


            final String personId = user.getPersonId();
            boolean okToChange = true;

            if (activating)
            {
                okToChange = personDataSource.isActive(personId);
            }

            if (okToChange)
            {
                user.setActive(activating);
                this.accountDao.updateActivation(currentUser.get(), user.getUserId(), activating);
                result.put("success", true);

                // Invalidate the cache entry for the updated user
                this.invalidateCacheEntry(user);

                //                ...
                // Tell the world about this change
                EventDispatcher.getDispatcher().post(
                    activating ?
                        new UserActivatedEvent("actor", personId)
                    :
                        new UserDeactivatedEvent("actor", personId)
                );
            }
            activationResults.put(user, result);
        }

        return activationResults;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.UserService#refresh()
     */
    @Override
    public void refresh()
    {
        // TODO Auto-generated method stub
        // XXX: SDP - Probably don't need this any more
    }


    /**
     * All calls to get a Person or People must eventually call this "loadPerson" method.
     * @param userId
     * @return
     */
    private MivPerson loadPerson(String userId)
    {
        MivPerson person = null;
        @SuppressWarnings("unused")
        boolean serviceAvailable = true; // ?temp for testing?

        //
        // We might get nothing (null) back for the PersonDTO if this is someone
        // who is no longer in the data source, like when people fall out of LDAP,
        // but they used to be an MIV user.
        //
        // Conversely, we might get a result for the PersonDTO but nothing (null)
        // in the MivAccountDto because this is an active person, but they have
        // never had an MIV account.
        //
        // For currently active MIV users we should get both DTOs back.
        //


        // We're given a String which we'll assume first is an MIV userId.
        // Try to load the account DTO using that userId.
        // If that succeeds we'll get the PersonUUID/PersonId and use the personDataSource to load that info.
        // If that failed (adto==null) we'll try to use that same string as a PersonUUID to get a non-MIV-user's information.

        // If this turns out null then this person is not an MIV user
        MivAccountDto adto = accountDao.getAccountByUserId(userId);

        String personId;
        if (adto != null) {
            personId = adto.getPersonUuid();
        }
        else {
            personId = userId;
        }

        // This can throw a ServiceUnavailableException
        // If it does, we might still be able to continue with only local data
        PersonDTO pdto = null;
        try {
            pdto = personDataSource.getPersonFromId(personId);
        }
        catch (ServiceUnavailableException e) {
            // Leave the PersonDTO as null, let the PersonBuilder deal with it...
            serviceAvailable = false;
        }

        // TODO: Finish writing this

// Temporary Testing Implementation
//try {
//    // In this current Experimental form, we Need to call getPersonBuilder to initialize the PersonBuilder class
//    // and it might throw an IOException when trying to get its DAOs
//        @SuppressWarnings("unused")
//        PersonBuilder builder = getPersonBuilder();
//}
//catch (IOException e) {}


        if (adto != null || pdto != null) {
            person = PersonBuilder.newBuilder()
                .withAccount(adto)
                .withPersonData(pdto)
              //.withRoles(roles) // SDP - PersonBuilder.build() is taking care of the roles/assignments and the appointments
                .build();
        }

        return person;
    } // end of loadPerson


    /**
     * Invalidate a person object in the cache
     * @param person
     */
    private void invalidateCacheEntry(MivPerson person)
    {
        personCache.invalidate(Integer.toString(person.getUserId()));
        weakPersonCache.invalidate(Integer.toString(person.getUserId()));
        weakPersonCache.refresh(Integer.toString(person.getUserId()));

        logger.debug("|| --- Invalidated cache entry for {}", new Object[] {person});
    }


    /**
     * Check how long this Person object has been alive and update it
     * with fresh PersonDataSource information if it is "stale"
     *
     * @param person Person to refresh
     * @param force <code>true</code> to force the refresh even if the person isn't stale.
     * @return the Person passed
     */
    @Override
    public MivPerson freshen(final MivPerson person, boolean force)
    {
        if (person == null) {
            logger.warn("Attempted to freshen a (null) Person reference");
            return null;
        }

        if (person instanceof RefreshablePerson)
        {
            final RefreshablePerson p = (RefreshablePerson) person;

            final long now = System.currentTimeMillis();
            final long loaded = p.getLoadTime();

            if (force || now - loaded > maxAge)
            {
                final long ageInMinutes = TimeUnit.MILLISECONDS.toMinutes(now - loaded);
                logger.debug("|| --- Refreshing {} : last loaded / refreshed {} minutes ago", new Object[] {person, ageInMinutes});

                executor.submit(new Runnable() {
                    @Override
                    public void run()
                    {
                        final PersonDTO freshData = personDataSource.getPersonFromId(person.getPersonId());
                        logger.debug("Asynch getPersonFromId({}) has completed", person.getPersonId());
                        if (freshData != null) {
                            logger.debug("|| --- Asynch refresh got dto {}", freshData);
                            p.freshen(freshData);
                        }
                        else {
                            logger.debug("|| --- Asynch refresh got null dto : maybe someone should deactivate {}", person);
                        }
                    }
                });

            }
        }

        return person;
    }
    @Override
    public MivPerson freshen(final MivPerson person)
    {
        return this.freshen(person, false);
    }





    Duration maximumAge = new Duration(24, TimeUnit.HOURS);
    long maxMillis = maximumAge.as(TimeUnit.MILLISECONDS);
    private long maxAge = maxMillis; // TODO: Make this configurable.
    ExecutorService executor = Executors.newCachedThreadPool(); // Thread Pool used for Asynch Refresh of Person data.



    /**
     * Called when an object is removed from the cache. We're just writing a log message for now.
     * We may or may not want to do additional processing when this happens, but it's called after
     * the fact, and only the key, not the removed object, is available, so this may not be useful.
     */
    private RemovalListener<String, MivPerson> removeListener = new RemovalListener<String, MivPerson>() {
        @Override
        public void onRemoval(RemovalNotification<String, MivPerson> note)
        {
            logger.debug("|| -- MAIN CACHE Removal Notification : Person with ID [{}] (({})) because [{}]",
                        new Object[] { note.getKey(), note.getValue(), note.getCause() });
        }
    };


    /*
     * Caches to hold loaded Person objects.
     */

    private LoadingCache<String, MivPerson> personCache = CacheBuilder.newBuilder()
        // Set some tiny limits for Testing, to see things get kicked out of the cache
        // .maximumSize(2)
        // .expireAfterWrite(2, TimeUnit.MINUTES)
        // .expireAfterAccess(3, TimeUnit.MINUTES)
        .expireAfterAccess(10, TimeUnit.MINUTES) // Do we *ever* want to expire? I'm worried about multiple copies of the same MivPerson
        .removalListener(removeListener)
        .build(
               new CacheLoader<String, MivPerson>() {
                   @Override
                   public MivPerson load(String personId) throws Exception {
                       logger.debug("|| -- PersonCache is getting Person {} from the WEAK CACHE", personId);
                       MivPerson person = weakPersonCache.get(personId);
                       return person;
                   }
               }
        );

    private LoadingCache<String, MivPerson> weakPersonCache = CacheBuilder.newBuilder()
        .weakValues()
        .removalListener(new RemovalListener<String,MivPerson>() {
            @Override
            public void onRemoval(RemovalNotification<String, MivPerson> note)
            {
                logger.debug("|| -- WEAK CACHE Removal Notification : Person with ID [{}] (({})) because [{}]",
                            new Object[] { note.getKey(), note.getValue(), note.getCause() });
            }
        })
        .build(
            new CacheLoader<String, MivPerson>() {
                @Override
                public MivPerson load(String personId) throws Exception {
                    //System.out.println(); // makes catalina.out easier to read. remove this later.
                    logger.debug("|| -- weakPersonCache is loading Person {}", personId);
                    MivPerson person = loadPerson(personId);
                    BuiltPersonImpl p = (BuiltPersonImpl) person; // only doing this (cast) so getObjectId can be called for development/debug
                    String objInfo =  p != null ? p.getObjectId() : "(null object)";
                    logger.debug("|| -- weakPersonCache : Person {} has Object ID {}", personId, objInfo);
                    logger.debug("|| -- weakPersonCache loaded  Person [{}]", person);
                    return person != null ? person : nullPerson; // XXX: SDP - could we use an "Optional" here instead of this "nullPerson"?
                    //
                    // The above does a bunch that's only there to help debug the development of the system.
                    // When all done, this "load()" method can be reduced to:
                    //     MivPerson person = loadPerson(personId);
                    //     return person != null ? person : nullPerson;
                    // unless we still want some debug logging present.
                    // In any case, the "BuiltPersonImpl" and "objInfo" can go away.
                    //
                }
            }
        );

/*
    Case: Person is not in Cache
        1. MIV code external to the UserService asks for Person1
        2. getPersonByBPersonIdCached is (ultimately) called
        3. personCache.get() called
        4. determined person not in cache, so
        5. personCache.load() is called
        6. load calls weakCache.get()
        7. determined person not in cache, so
        8. weakCache.load() is called
        9. load calls loadPerson()
        10. Person1 is loaded, placed in weakCache
        11. weakCache returns Person1 to caller (personCache)
        12. Person1 is placed in personCache
        13. personCache returns Person to caller
        -> weakCache has WeakReference to Person1
        -> personCache has normal Reference to Person1
        -> caller has normal Reference to Person1
*/


    /** The Guava Cache doesn't like null Objects, so this represents a not-found Person */
    private static final MivPerson nullPerson = new MivPerson()
    {
        @Override
        public List<Principal> getPrincipals()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Principal getPrincipal()
        {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public int getUserId()
        {
            return Integer.MIN_VALUE;
        }

        @Override
        public String getPrincipalName()
        {
            return "NULL";
        }

        @Override
        public String getEntityId()
        {
            return "NULL";
        }

        @Override
        public String getPersonId()
        {
            return "NULL";
        }

        @Override
        public String getGivenName()
        {
            return "NULL";
        }

        @Override
        public String getMiddleName()
        {
            return "NULL";
        }

        @Override
        public String getSurname()
        {
            return "NULL";
        }

        @Override
        public String getSuffix()
        {
            return "NULL";
        }

        @Override
        public String getDisplayName()
        {
            return "NULL";
        }

        @Override
        public String getSortName()
        {
            return "NULL";
        }

        @Override
        public String getLoggingName()
        {
            return "NULL";
        }

        @Override
        public String getPreferredEmail()
        {
            return "NULL";
        }

        @Override
        public void setPreferredEmail(String address)
        {
        }

        private final List<String> emptyEmail = Collections.emptyList();
        @Override
        public List<String> getEmailAddresses()
        {
            return emptyEmail;
        }

        private final List<Appointment> emptyAppointments = Collections.emptyList();
        @Override
        public Collection<Appointment> getAppointments()
        {
            return emptyAppointments;
        }

        @Override
        public Appointment getPrimaryAppointment()
        {
            return null;
        }

        @Override
        public boolean addAppointment(Appointment app)
        {
            return false;
        }

        @Override
        public boolean removeAppointment(Appointment app)
        {
            return false;
        }

        @Override
        public MivRole getPrimaryRoleType()
        {
            return MivRole.INVALID_ROLE;
        }

        @Override
        public void setPrimaryRole(AssignedRole role)
        {
        }

        @Override
        public Set<AssignedRole> getAssignedRoles()
        {
            return null;
        }

        @Override
        public List<AssignedRole> getPrimaryRoles()
        {
            return null;
        }

        @Override
        public boolean addRole(AssignedRole role)
        {
            return false;
        }

        @Override
        public boolean removeRole(MivRole role)
        {
            return false;
        }

        @Override
        public boolean hasRole(MivRole... roles)
        {
            return false;
        }

        @Override
        public boolean hasRole(Iterable<MivRole> roles)
        {
            return false;
        }

        @Override
        public void setActive(boolean active)
        {
        }

        @Override
        public boolean isActive()
        {
            return false;
        }

        @Override
        public MivPerson proxiedBy(MivRole role)
        {
            return this;
        }

        @Override
        public boolean loadDetail()
        {
            return false;
        }

        @Override
        public boolean removeRole(AssignedRole role)
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void setName(String givenName, String middleName,
                String surname, String suffix)
        {
            // TODO Auto-generated method stub

        }
    }; // NullPerson



    @Subscribe
    public void resetCache(final ConfigurationChangeEvent event)
    {
        ConfigReloadConstants whatChanged = event.getWhatChanged();
        logger.info("Got a ConfigurationChangeEvent with whatChanged=={}", whatChanged.name());

        switch (whatChanged)
        {
            case ALL:
            case USER_SERVICE:
                logger.info("Invalidating the main- and the weak- person caches");
                personCache.invalidateAll();
                weakPersonCache.invalidateAll();
                break;
            default:
                // do nothing for other config changes
                break;
        }
    }



    @Override
    public MivPerson getAppointee(final int userid)
    {
        MivPerson person = this.getPersonByMivId(userid);
        // Let's see what we get, and how we have to modify it.
        return person;
    }


    @Override
    public MivPerson getNewAppointee(String surName, String givenName, String middleName)
    {
        // start off in IET - Inactive when no scope given: caller will have to set the real school/department later
        return getNewAppointee(surName, givenName, middleName, new Scope(38, 1000));
    }


    @Override
    public MivPerson getNewAppointee(String surName, String givenName, String middleName, Scope scope)
    {
        final MivAccountDto a = new MivAccountDto();
        MivPerson person;

        a.setUserId(-1);
        a.setLogin(surName + randomExtension()); // set a fake login name for them
        //a.setPersonUuid(entityId);// NULL is allowed
        a.setSurname(surName);
        a.setMiddleName(middleName);
        a.setGivenName(givenName);
        //a.setPreferredName();
        //a.setEmail(email);// NULL is allowed
        a.setActive(true);

        a.setSchoolId(scope.getSchool());
        a.setDepartmentId(scope.getDepartment());

        List<AssignedRole> roles = new ArrayList<AssignedRole>(1);
        roles.add(new AssignedRole(MivRole.APPOINTEE, scope, true));
        a.setRoles(roles);

        person = new BuiltPersonImpl(a);

        for (AssignedRole role : person.getAssignedRoles())
        {
            if (role.getRole() == MivRole.MIV_USER)
            {
                person.removeRole(role); // New Appointees aren't MIV users
            }
        }

        return person;
    }
    private static String rngType = "SHA1PRNG";
    private static Random prng;
    static {
        try {
            prng = SecureRandom.getInstance(rngType);
        }
        catch (NoSuchAlgorithmException e) {
            logger.warn("RNG Algorithm {} not supported: Using an INSECURE random number generator!", rngType);
            prng = new Random(System.currentTimeMillis());
        }
    }
    private final String randomExtension()
    {
        StringBuilder ext = new StringBuilder("_");

        int length = prng.nextInt(5) + 1;
        for (int i=0; i<length; i++) {
            ext.append(prng.nextInt(10));
        }
        return ext.toString();
    }


    @Override
    public MivPerson saveAppointee(final MivPerson person, int actorId)
    {
        if (!person.isActive())
        {
            person.setActive(true);
        }

        if (savePerson(person, actorId) != null)
        {
            Login login = new Login(person.getUserId(), person.getPrincipal().getPrincipalName(), null, null);
            logger.info("Saving Appointee with Login = [{}]", login);
            loginDao.save(login);
            return person;
        }
        return null;
    }


    @Override
    public List<MivPerson> getUsers(MivRole role)
    {
        if (role == null) return Collections.emptyList();

        final AttributeSet criteria = new AttributeSet();
        criteria.put(MivSearchKeys.Person.MIV_ROLE, Integer.toString(role.getRoleId()));
        criteria.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");

        return this.findUsers(criteria);
    }


    @Override
    public List<MivDisplayPerson> findActiveCandidates()
    {
        return accountDao.findActiveCandidates();
    }
}
