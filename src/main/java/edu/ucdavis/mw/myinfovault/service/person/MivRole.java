/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivRole.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Represents the MIV Roles and correlates them to the KIM roles.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public enum MivRole
{
    // The role definitions below are grouped by functional area.
    // The numeric order (so we don't duplicate role ID numbers) is:
    //   1 - SYS_ADMIN
    //   2 - SCHOOL_STAFF
    //   3 - DEAN
    //   4 - DEPT_STAFF
    //   5 - DEPT_ASSISTANT
    //   6 - CANDIDATE
    //   7 - MIV_USER
    //   8 - CRC_REVIEWER
    //   9 - DEPT_REVIEWER
    //  10 - VICE_PROVOST
    //  11 - VICE_PROVOST_STAFF
    //  12 - ARCHIVE_ADMIN
    //  13 - SCHOOL_REVIEWER
    //  14 - J_SCHOOL_REVIEWER
    //  15 - CANDIDATE_SNAPSHOT
    //  16 - ARCHIVE_USER
    //  17 - DEPT_CHAIR
    //  18 - SENATE_STAFF
    //  19 - PROVOST
    //  20 - APPOINTEE
    //  21 - J_DEPT_REVIEWER
    //  22 - CHANCELLOR
    //  23 - OCP_STAFF
    //  24 - API_ENTITY


    // All MIV users will have this role, to distinguish between people
    // that are MIV users and otherwise valid people that aren't in MIV.
    MIV_USER            ( 7, "MIV User", "MIV User", Category.AUX),

    // "Dev Team"
    SYS_ADMIN           ( 1, "System Administrator", "Sys. Admin.", Category.STAFF),


    VICE_PROVOST        (10, "Vice Provost", "Vice Prov.", Category.FACULTY),
    VICE_PROVOST_STAFF  (11, "MIV Administrator", "MIV Admin.", Category.STAFF),

    PROVOST             (19, "Provost", "Provost", Category.FACULTY),
    CHANCELLOR          (22, "Chancellor", "Chancellor", Category.FACULTY),
    OCP_STAFF           (23, "Office of Chancellor and Provost Administrator", "OCP Admin.", Category.STAFF),

    SENATE_STAFF        (18, "Senate Administrator", "Senate Admin.", Category.STAFF),

    DEAN                ( 3, "School/College Dean", "Dean", Category.FACULTY),
    SCHOOL_STAFF        ( 2, "School/College Administrator", "School/College Admin.", Category.STAFF),

    DEPT_CHAIR          (17, "Department Chair", "Dept. Chair", Category.FACULTY),
    DEPT_STAFF          ( 4, "Department Administrator", "Dept. Admin.", Category.STAFF),
    DEPT_ASSISTANT      ( 5, "Department Helper", "Dept. Helper", Category.STAFF),

    CANDIDATE           ( 6, "Candidate", "Candidate", Category.FACULTY),

    DEPT_REVIEWER       ( 9, "Department Reviewer", "Dept. Reviewer", Category.AUX),
    CRC_REVIEWER        ( 8, "CRC Reviewer", "CRC Reviewer", Category.AUX),
    ARCHIVE_ADMIN       (12, "Archive Administrator", "Archive Admin.", Category.AUX),
    SCHOOL_REVIEWER     (13, "School Reviewer", "School Reviewer", Category.AUX),
    J_SCHOOL_REVIEWER   (14, "Joint School Reviewer", "Joint School Reviewer", Category.AUX),

    CANDIDATE_SNAPSHOT  (15, "Candidate Snapshot", "Candidate Snapshot", Category.AUX),
    ARCHIVE_USER        (16, "Archive User", "Archive User", Category.AUX),

    APPOINTEE           (20, "Appointee", "Appointee", Category.EXTERNAL),

    J_DEPT_REVIEWER     (21, "Joint Department Reviewer", "Dept. Reviewer", Category.AUX),

    API_ENTITY          (24, "Api Entity", "Api Entity", Category.AUX),

    NO_ROLE             (0, "Not an MIV User", "Not a User", Category.EXTERNAL),

    INVALID_ROLE        (-1, "Invalid Role", "Invalid", Category.INVALID);


    private final int roleId;
    private final String description;
    private final String shortDescription;
    private final Category category;

    private static final Logger logger = LoggerFactory.getLogger(MivRole.class);


    private MivRole(int roleId, String description, String shortDescription, Category category)
    {
        this.roleId = roleId;
        this.description = description;
        this.shortDescription = shortDescription;
        this.category = category;
    }

    public int getRoleId()
    {
        return this.roleId;
    }

    @Deprecated
    public String getKimRole()
    {
        return ""; // removed the now-unused kimRoleName, but don't break callers yet.
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getShortDescription()
    {
        return this.shortDescription;
    }

    public Category getCategory()
    {
        return this.category;
    }

    /** Synonym for the standard enum <code>.name()</code> method, but usable as a bean getter. */
    public String getName()
    {
        return this.name();
    }

    /**
     * Returns the enum corresponding to the roleId
     * @param roleId
     * @return MivRole enum
     */
    public static MivRole getMivRoleById(int roleId)
    {
        MivRole[] enumlist = MivRole.values();
        for (MivRole mivRole : enumlist)
        {
            if (mivRole.getRoleId() == roleId) { return mivRole; }
        }

        return MivRole.INVALID_ROLE;
    }

    /**
     * Get the scope for this role. Useful as scope mask.
     *
     * @return scope for this role
     * @see Scope#or(Scope)
     */
    public Scope getScope()
    {
        switch(this)
        {
            case VICE_PROVOST_STAFF:
            case SENATE_STAFF:
            case VICE_PROVOST:
            case PROVOST:
            case CHANCELLOR:
            case OCP_STAFF:
            case SYS_ADMIN:
                return Scope.AnyScope;
            case SCHOOL_STAFF:
            case DEAN:
                return new Scope(Scope.NONE);
            default:
                return Scope.NoScope;
        }
    }

    /*
     * Support for assigning Roles - what roles may a given user (role) give to people
     */

    /* *
     * @deprecated Moved to the {@link edu.ucdavis.mw.myinfovault.service.authorization.EditUserAuthorizer EditUserAuthorizer}
     * but this must be maintained in parallel until all use of this is eliminated. It looks like this HAS been eliminated.
     * /
    @Deprecated
    private static final Map<MivRole, List<MivRole>> assignableRoles = new HashMap<MivRole, List<MivRole>>();
    static {
        List<MivRole> l;
        MivRole r;

        r = SYS_ADMIN;
        l = new ArrayList<MivRole>();
        // Even sys admins can't assign the sys admin role.
        // It must be done manually in the database.
        l.add(SYS_ADMIN);
        l.add(VICE_PROVOST_STAFF);
        l.add(SENATE_STAFF);
        l.add(SCHOOL_STAFF);
        l.add(DEPT_STAFF);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);
        assignableRoles.put(r, l);

        r = VICE_PROVOST_STAFF;
        l = new ArrayList<MivRole>(l);
        l.remove(SYS_ADMIN);
        assignableRoles.put(r, l);

        r = SCHOOL_STAFF;
        l = new ArrayList<MivRole>(l);
        l.remove(VICE_PROVOST_STAFF);
        l.remove(SENATE_STAFF);
        assignableRoles.put(r, l);

        r = DEPT_STAFF;
        l = new ArrayList<MivRole>(l);
        l.remove(SCHOOL_STAFF);
        assignableRoles.put(r, l);
    }*/


    /**
     * Get the roles that this role is allowed to assign to people.
     * @return A List of MivRoles that may be assigned by this role.
     * @deprecated "assignable" roles is being moved to the Authorization service.
     */
    @Deprecated
    public List<MivRole> getAssignableRoles()
    {
        UnsupportedOperationException e = new UnsupportedOperationException("MivRole.getAssignableRoles has been called!");
        e.fillInStackTrace();
        logger.warn("!!!!! ||-- getAssignableRoles in MivRole has been called !!!!!  This should be replaced by EditUserAuthorizer.getAssignableRoles(role)", e);
        throw e;

//        List<MivRole> roles = assignableRoles.get(this);
//        if (roles == null) roles = Collections.emptyList();
//        return Collections.unmodifiableList(roles);
    }


    // Set up the static list of roles that can be assigned as primaries.
    private static final MivRole[] roleArray = {
        SYS_ADMIN, VICE_PROVOST_STAFF, OCP_STAFF, SENATE_STAFF, SCHOOL_STAFF, DEPT_STAFF, DEPT_ASSISTANT, CANDIDATE
        };
    private static final Collection<MivRole> primaryRoles = Collections.unmodifiableCollection( Arrays.asList(roleArray) );
    /**
     * Returns the list of roles that are allowed to be assigned as a Primary Role type.
     * @return a Collection of primary roles
     */
    public static Collection<MivRole> getPrimaryRoles()
    {
        return MivRole.primaryRoles;
    }


    /**
     * List the additional roles that are automatically added to a primary role.
     * @return the set of MivRoles that a given primary role should have.
     */
    public Collection<MivRole> getAdditionalRoles()
    {
        return MivRole.getAdditionalRoles(this);
    }

    private static Collection<MivRole> getAdditionalRoles(MivRole role)
    {
        List<MivRole> roles = new ArrayList<MivRole>(2);

        switch(role)
        {
            case SYS_ADMIN:
                roles.add(VICE_PROVOST_STAFF);
                roles.add(ARCHIVE_ADMIN);
                break;
            case VICE_PROVOST_STAFF:
                roles.add(ARCHIVE_ADMIN);
                break;
            default:
                // anything else has no additional roles
                break;
        }

        return Collections.unmodifiableList(roles);
    }


    /**
     * List the roles that a given primary role shouldn't have. These roles
     * should be removed from a user when they have a new primary role.
     * @return the set of MivRoles that should be disallowed.
     */
    public Collection<MivRole> getProhibitedRoles()
    {
        return MivRole.getProhibitedRoles(this);
    }

    /**
     * Determine if the input role is a reviewer role
     * @return true if reviewer role, otherwise false
     */
    public boolean isReviewerRole()
    {
        switch(this)
        {
            case DEPT_REVIEWER:
            case J_DEPT_REVIEWER:
            case SCHOOL_REVIEWER:
            case J_SCHOOL_REVIEWER:
            case CRC_REVIEWER:
                return true;
            default:
                return false;
        }

    }

    private static Collection<MivRole> getProhibitedRoles(MivRole role)
    {
        List<MivRole> roles = new ArrayList<MivRole>();

        switch(role)
        {
            case SYS_ADMIN:
                roles.add(SCHOOL_STAFF);
                roles.add(DEPT_STAFF);
                roles.add(DEPT_ASSISTANT);
                roles.add(CANDIDATE);
                roles.add(APPOINTEE);
                break;
            case VICE_PROVOST_STAFF:
                roles.add(SYS_ADMIN);
                roles.add(PROVOST);
                roles.add(OCP_STAFF);
                roles.add(SENATE_STAFF);
                roles.add(SCHOOL_STAFF);
                roles.add(DEPT_STAFF);
                roles.add(DEPT_ASSISTANT);
                roles.add(DEPT_CHAIR);
                roles.add(CANDIDATE);
                roles.add(APPOINTEE);
                break;
            case OCP_STAFF:
                roles.add(SYS_ADMIN);
                roles.add(CHANCELLOR);
                roles.add(PROVOST);
                roles.add(VICE_PROVOST);
                roles.add(VICE_PROVOST_STAFF);
                roles.add(SENATE_STAFF);
                roles.add(DEAN);
                roles.add(SCHOOL_STAFF);
                roles.add(DEPT_STAFF);
                roles.add(DEPT_ASSISTANT);
                roles.add(DEPT_REVIEWER);
                roles.add(DEPT_CHAIR);
                roles.add(CRC_REVIEWER);
                roles.add(ARCHIVE_ADMIN);
                roles.add(CANDIDATE);
                roles.add(APPOINTEE);
                break;
            case SENATE_STAFF:
                roles.add(SYS_ADMIN);
                roles.add(CHANCELLOR);
                roles.add(PROVOST);
                roles.add(VICE_PROVOST);
                roles.add(VICE_PROVOST_STAFF);
                roles.add(OCP_STAFF);
                roles.add(DEAN);
                roles.add(SCHOOL_STAFF);
                roles.add(DEPT_STAFF);
                roles.add(DEPT_ASSISTANT);
                roles.add(DEPT_REVIEWER);
                roles.add(DEPT_CHAIR);
                roles.add(CRC_REVIEWER);
                roles.add(ARCHIVE_ADMIN);
                roles.add(CANDIDATE);
                roles.add(APPOINTEE);
                break;
            case SCHOOL_STAFF:
                roles.add(SYS_ADMIN);
                roles.add(CHANCELLOR);
                roles.add(PROVOST);
                roles.add(VICE_PROVOST);
                roles.add(VICE_PROVOST_STAFF);
                roles.add(OCP_STAFF);
                roles.add(SENATE_STAFF);
                roles.add(DEAN);
                roles.add(DEPT_STAFF);
                roles.add(DEPT_ASSISTANT);
                roles.add(DEPT_REVIEWER);
                roles.add(DEPT_CHAIR);
                roles.add(CRC_REVIEWER);
                roles.add(ARCHIVE_ADMIN);
                roles.add(CANDIDATE);
                roles.add(APPOINTEE);
                break;
            case DEPT_STAFF:
                roles.add(SYS_ADMIN);
                roles.add(CHANCELLOR);
                roles.add(PROVOST);
                roles.add(VICE_PROVOST);
                roles.add(VICE_PROVOST_STAFF);
                roles.add(OCP_STAFF);
                roles.add(SENATE_STAFF);
                roles.add(DEAN);
                roles.add(SCHOOL_STAFF);
                roles.add(DEPT_ASSISTANT);
                roles.add(DEPT_CHAIR);
                roles.add(SCHOOL_REVIEWER);
                roles.add(J_SCHOOL_REVIEWER);
                roles.add(CRC_REVIEWER);
                roles.add(ARCHIVE_ADMIN);
                roles.add(CANDIDATE);
                roles.add(APPOINTEE);
                break;
            case DEPT_ASSISTANT:
                roles.add(SYS_ADMIN);
                roles.add(CHANCELLOR);
                roles.add(PROVOST);
                roles.add(VICE_PROVOST);
                roles.add(VICE_PROVOST_STAFF);
                roles.add(OCP_STAFF);
                roles.add(SENATE_STAFF);
                roles.add(DEAN);
                roles.add(SCHOOL_STAFF);
                roles.add(DEPT_STAFF);
                roles.add(DEPT_REVIEWER);
                roles.add(DEPT_CHAIR);
                roles.add(SCHOOL_REVIEWER);
                roles.add(J_SCHOOL_REVIEWER);
                roles.add(CRC_REVIEWER);
                roles.add(ARCHIVE_ADMIN);
                roles.add(CANDIDATE);
                roles.add(APPOINTEE);
                break;
            case CANDIDATE:
                roles.add(SYS_ADMIN);
                roles.add(VICE_PROVOST_STAFF);
                roles.add(OCP_STAFF);
                roles.add(SENATE_STAFF);
                roles.add(SCHOOL_STAFF);
                roles.add(DEPT_STAFF);
                roles.add(DEPT_ASSISTANT);
                roles.add(ARCHIVE_ADMIN);
                break;
            default:
                // anything else has no role changes
                break;
        }

        return Collections.unmodifiableList(roles);
    }

    /**
     * Get the subset of MIV roles in one of the given categories.
     *
     * @param category role category
     * @return Subset of MIV roles in one of the given category
     */
    public static Set<MivRole> getRoles(Category...categories)
    {
        Set<MivRole> roles = EnumSet.noneOf(MivRole.class);

        for (MivRole role : MivRole.values())
        {
            for (Category category : categories)
            {
                if (category == role.getCategory())
                {
                    roles.add(role);
                    break;
                }
            }
        }

        return roles;
    }


    /** These are experimental and are a first guess. Nothing is using STUDENT or EXTERNAL right now. */
    public enum Category {
        /** Use the INVALID category only for INVALID_ROLE */   INVALID ("Invalid"),
        /** Roles that are assigned to Faculty */               FACULTY ("Academic"),
        /** Roles that are assigned to Staff */                 STAFF ("Staff"),
        /** Roles that are assigned to Students (None) */       STUDENT ("Student"),
        /** Roles for external people (None) */                 EXTERNAL ("External"),
        /** Auxiliary/Utility roles, not given to people */    AUX ("Auxiliary");

        private final String description;

        Category(String description)
        {
            this.description = description;
        }

        public String getDescription()
        {
            return description;
        }

        @Override
        public String toString()
        {
            return description;
        }
    }



    // ----------- TEST ! -----------
    static {
    //  System.out.println("Checking role assignment setup...");
    //  MivRole[] testRoles = { DEPT_STAFF, SCHOOL_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN };
    //  for (MivRole r : testRoles)
    //  {
    //    //List<MivRole> canAssign = MivRole.getAssignableRoles(r);
    //      List<MivRole> canAssign = r.getAssignableRoles();
    //      System.out.println("Role ["+r+"] can assign (("+canAssign+"))");
    //  }
    }
}
