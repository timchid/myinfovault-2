/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketService.java
 */

package edu.ucdavis.mw.myinfovault.service.packet;

import java.util.Date;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.events2.PacketRequestCancellationEvent;
import edu.ucdavis.mw.myinfovault.events2.PacketRequestEvent;
import edu.ucdavis.mw.myinfovault.events2.PacketSubmissionEvent;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.document.Document;

/**
 * Packet Service
 * @author rhendric
 *
 * @since MIV 5.0
 */
public interface PacketService
{
    /**
     * Get a PacketRequest for the input dossierId
     * @param dossierId
     * @return PacketRequest, null if none
     */
    public PacketRequest getPacketRequest(long dossierId);

    /**
     * Get a PacketRequest for the input Dossier
     * @param dossier
     * @return PacketRequest, null if none
     */
    public PacketRequest getPacketRequest(Dossier dossier);

    /**
     * Update an exiting PacketRequest in the database
     * @param PacketRequest
     * @param updateUserId
     * @return true if successful, otherwise false
     */
    public boolean updatePacketRequest(PacketRequest packetRequest, int updateUserId);

    /**
     * Get PacketRequests for the input user
     * @param userId
     * @return List of PacketRequests, empty list if none
     */
    List<PacketRequest> getPacketRequests(int userId);

    /**
     * Get all PacketRequests for the input user
     * @param mivPerson
     * @return List of PacketRequests, empty list if none
     */
    List<PacketRequest> getPacketRequests(MivPerson mivPerson);

    /**
     * Get open PacketRequests for the input user
     * @param targetPerson
     * @return List of PacketRequests, empty list if none
     */
    List<PacketRequest> getOpenPacketRequests(MivPerson targetPerson);

    /**
     * Get open PacketRequests for the input user
     * @param target UserId
     * @return List of PacketRequests, empty list if none
     */
    List<PacketRequest> getOpenPacketRequests(int targetUserId);

    /**
     * Request a packet in response to a PacketRequestEvent
     * @param PacketRequestEvent
     * @return PacketRequest
     */
    PacketRequest requestPacket(PacketRequestEvent event);

    /**
     * Cancel a packet request in response to a PacketRequestCancellationEvent
     * @param PacketRequestCancellationEvent
     */
    PacketRequest cancelPacketRequest(PacketRequestCancellationEvent event);

    /**
     * Handle packet request submission.
     * @param PacketRequestSubmissionEvent
     */
    boolean packetSubmitted(PacketSubmissionEvent event);

    /**
     * Delete a packet request
     * @param PacketRequest
     */
    boolean deletePacketRequest(PacketRequest packetRequest);

    /**
     * Get the packet update time
     * @param packetId
     * @return Date
     */
    public Date getPacketUpdateTime(long packetId);

    /**
     * Get packets for a user
     * @param userId
     * @return List of Packet objects
     */
    List<Packet>getPackets(int userId);

    /**
     * Get specified packet
     * @param packetId
     * @return Packet object
     */
    Packet getPacket(long packetId);

    /**
     * Delete the specified packet for a user
     * @param packetId
     * @return boolean true = success, otherwise false
     */
    boolean deletePacket(long packetId);

    /**
     * Save specified packet for a user
     * @param Packet
     * @param int userId of updating user
     * @return Packet
     */
    Packet updatePacket(Packet packet, int updateUserId);

    /**
     * Copy a packet
     * @param Packet
     * @param int userId of updating user
     * @return Packet
     */
    Packet copyPacket(Packet packet,  int updateUserId);


    /**
     * Submit a Packet for the specified PacketRequest
     * @param Packet
     * @param PacketRequest
     * @param int userId of submitting user
     * @return boolean - true if submitted, otherwise false
     */
    boolean submitPacket(Packet packet, PacketRequest packetRequest, int userId);

    /**
     * Create PDF files for the specified packet
     * @param Packet
     * @return List of Document objects
     */
    List<Document> createPacket(Packet packet);

    /**
     * Preview PDF files for the specified packet
     * @param Packet
     * @return List of Document objects
     */
    List<Document> previewPacket(Packet packet);

    /**
     * Verify that a packet has been submitted for a dossier.
     * @param Packet
     * @return true if packet has been submitted, otherwise false
     */
    public boolean isPacketSubmitted(Dossier dossier);

}
