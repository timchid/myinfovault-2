/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FuzzyMatchFilter.java
 */

package edu.ucdavis.mw.myinfovault.service;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * <p>This class filters strings by comparing the input string against a target for similarity.</p>
 *
 * <p>The similarity is computed by calculating the Levenshtein distance between the source and target and then
 * calculating the percentage of difference between the two.</p>
 *
 * <p>The Levenshtein distance in itself is not sufficient to determine the similarity between strings.
 * If the distance threshold is set too high, then there is an increased likelihood of false matches on shorter strings,
 * conversely if the distance threshold is too low, longer strings which may be similar will be filtered out.
 * To help resolve this problem, the percentage of difference between the two strings is computed based on the
 * distance:
 * <pre>Levenshtein Distance / number of characters in the shorter value × 100 = difference percentage</pre></p>
 *
 * <p>For example, the Levenshtein Distance between “Smith” and “Smythe” is 2, and the shorter of the
 * two values is 5 characters long, producing a difference percentage of 40 (2/5). If the difference
 * percentage is less than or equal to the specified Difference Percentage, the two values are eligible
 * to be included in the output results, assuming they are also within the maximum allowable
 * Levenshtein Distance of each other (the Difference Threshold).</p>
 *
 * @see <a href="http://docs.acl.com/acl/930/index.jsp?topic=%2Fcom.acl.user_guide.help%2Fda_verifying_audit_data%2Fc_how_the_difference_settings_work.html">
 *      Difference Percentage adapted from ACL data analysis application documentation</a>
 *
 * @author Rick Hendricks
 * @since MIV 4.8.5
 * @param <T> Object by which collections will filtered
 */
public class FuzzyMatchFilter<T> extends SearchFilterAdapter<T>
{
    /**
     * Default to 50.00% difference threshold
     */
    private double percentDifferenceThreshold = 50.00;

    /**
     * Default to max Levenshtein distance of 10
     */
    private int distanceThreshold = 10;

    private final String target;

    /**
     * Construct the filter with the target to match against and initialize the configured thresholds.
     *
     * @param target target against which source items are compared
     */
    public FuzzyMatchFilter(T target)
    {
        this.target = getRepresentation(target);

        // Get the configured thresholds
        if (MIVConfig.isInitialized())
        {
            if (MIVConfig.getConfig().getProperty("levenshtein-percentDifferenceThreshold") != null)
            {
                percentDifferenceThreshold = Double.parseDouble(MIVConfig.getConfig().getProperty("levenshtein-percentDifferenceThreshold"));
            }

            if (MIVConfig.getConfig().getProperty("levenshtein-distanceThreshold") != null)
            {
                distanceThreshold = Integer.parseInt(MIVConfig.getConfig().getProperty("levenshtein-distanceThreshold"));
            }
        }
    }

    /**
     * Construct the filter with the target to match against and override the configured threshold values.
     *
     * @param target target against which source items are compared
     * @param distanceThreshold
     * @param percentDifferenceThreshold
     */
    public FuzzyMatchFilter(T target, int distanceThreshold, double percentDifferenceThreshold)
    {
        this(target);
        this.distanceThreshold = distanceThreshold;
        this.percentDifferenceThreshold = percentDifferenceThreshold;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(T item)
    {
        String source = getRepresentation(item);

        // Get the Levenshtein distance
        int distance  = this.getDistance(source);

        // Keep the value if the distance is <= distanceThreshold AND the percent difference <= percentDifferenceThreshhold
        boolean qualified = distance <= this.distanceThreshold &&
                this.getPercentDifference(source, distance) <= this.percentDifferenceThreshold;

        // Dump to log for debug
        System.out.println("Source="+source);
        System.out.println("Target="+target);
        System.out.println("Distance="+distance+" ("+distanceThreshold+")");
        System.out.println("Pct Difference="+this.getPercentDifference(source, distance)+" ("+percentDifferenceThreshold+")");
        System.out.println("Qualified="+qualified+"\n");

        return qualified;
    }

    /**
     * Get the String representation of an element for use in fuzzy match calculations.
     * Override this method if representation is not {@link T#toString()}.
     *
     * @param operand element used in a fuzzy match comparison
     * @return representation of the operand
     */
    protected String getRepresentation(T operand)
    {
        return operand.toString();
    }

    /**
     * Get the Levenshtein distance between this filters target and an input string item
     * The Levenshtein distance computation is case sensitive, so the target and input are
     * normalized to lower case for the computation.
     * @param item
     * @return distance
     */
    private int getDistance(String item)
    {
        return StringUtils.getLevenshteinDistance(item.toLowerCase(), target.toLowerCase());
    }

    /**
     * Get the percent difference between this filters target and an input string item
     * @param item
     * @param levenshtein distance
     * @return percentDifference
     */
    private double getPercentDifference(String item, int distance)
    {
        return new Double(distance) / new Double(item.length() < target.length() ? item.length() : target.length()) * 100.00;
    }

}
