/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationConverter.java
 */

package edu.ucdavis.mw.myinfovault.service.publications.converter;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;
import edu.ucdavis.mw.myinfovault.util.CSVIteration;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.DataMap;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.document.FOTransformer;

/**
 * @author Stephen Paulsen, Craig Gilmore
 * @since MIV 3.9.1
 */
public class PublicationConverter implements StreamConverter
{
    private static Logger logger = Logger.getLogger(PublicationConverter.class);

    private final PublicationSource publicationSource;
    private DataMap replaceMap;

    /**
     * Initialize maps for filtering input/output
     */
    private void initReplaceMap()
    {
        // Replace strings for output
        replaceMap = new DataMap();
        replaceMap.put("&nbsp;", "\u00a0");
        replaceMap.put("&(?!amp;|lt;|gt;|quot|#39;)", "&amp;");
        replaceMap.put("<(?!/|\\w)", " &lt; ");
        replaceMap.put("\\s>\\s", " &gt; ");
    }


    /**
     * This becomes the implicit super constructor for all subclasses and must be called
     * in the explicit constructor of the subclass.
     *
     * @param publicationSource The MIV supported publications source.
     * @throws IllegalArgumentException for invalid publication source
     */
    public PublicationConverter(PublicationSource publicationSource) throws IllegalArgumentException
    {
        if(publicationSource == PublicationSource.USER)
            throw new IllegalArgumentException(PublicationSource.USER + "source may not be converted.");

        this.publicationSource = publicationSource;
        initReplaceMap();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.publications.converter.StreamConverter#convert(java.io.Reader, java.io.Writer)
     */
    @Override
    public void convert(Reader in, Writer out) throws IOException
    {
        switch(publicationSource)
        {
            case PUBMED:
            case ENDNOTE:
                convertXML(in, out);
                break;
            case CSV:
                convertCSV(in, out);
                break;
            default://nothing written
                break;
        }
    }

    /**
     * Convert an XML character stream.
     *
     * @param in
     * @param out
     */
    private void convertXML(Reader in, Writer out)
    {
        File xslt = XSLT.get(publicationSource);

        final String msg = "Unanticipated error while converting XML through " + xslt.getName();

        try
        {
            FOTransformer.transform(xslt, in, out);
        }
        catch (TransformerConfigurationException e)
        {
            logger.log(Level.ERROR, msg, e);
            e.printStackTrace();
        }
        catch (TransformerException e)
        {
            logger.log(Level.ERROR, msg, e);
            e.printStackTrace();
        }
    }

    /**
     * Convert a CSV character stream.
     *
     * @param in
     * @param out
     * @throws IOException
     */
    private void convertCSV(Reader in, Writer out) throws IOException
    {
        out.write("<document>\n");
        out.write("<records>\n");

        for (Map<String, String> row : new CSVIteration(in))
        {
            out.write("<record>\n");
            out.write("<external-source sourceid=\"3\"/>");
            out.write("<external-id>" + System.currentTimeMillis() + "</external-id>");

            for (String label : row.keySet())
            {
                String tag = convertLabel(label);

                boolean isAuthorTag = "author".equals(tag);
                boolean isYearTag = "year".equals(tag);

                //<author> and <year> tag must be within the <authors> and <date> tags, respectively
                if(isAuthorTag)
                    out.write("<authors>");
                else if(isYearTag)
                    out.write("<date>");

                out.write(
                    "  <" + tag + ">" +
                            parseElementText(row.get(label)) +
                    "</" + tag + ">\n"
                );

                if(isAuthorTag)
                    out.write("</authors>");
                else if(isYearTag)
                    out.write("</date>");
            }

            out.write("</record>\n");
        }

        out.write("</records>\n");
        out.write("</document>\n");
    }

    /**
     * To parse element text.
     *
     * @param text
     * @return parsed text
     */
    private String parseElementText(String text)
    {
        if (text != null && text.length() > 0)
        {
            for (String key : this.replaceMap)
            {
                String replacement = (String) this.replaceMap.get(key);
                text = text.replaceAll(key, replacement);
            }
            // Replace any CR/LF with space
            text = text.replaceAll("[\\x0d\\x0a]", " ");
            // Replace any remaining non-printable characters with a bullet.
            text = MIVUtil.replaceNonPrintable(text, "\u2022");
        }

        return text;
    }

    private static Map<String,String> labelToTag = new HashMap<String,String>();
    static {
        labelToTag.put("pub-type", "ref-type");
        labelToTag.put("publication-type", "ref-type");
        labelToTag.put("publication-name", "periodical");
        labelToTag.put("authors", "author");
        labelToTag.put("editors", "editor");
        labelToTag.put("city", "location");
        labelToTag.put("status", "ref-status");
    }

    /**
     * Convert a user-friendly CSV column header into the appropriate XML tag for import.
     *
     * @param label
     * @return XML tag for column header
     */
    private String convertLabel(String label)
    {
        String tag = labelToTag.get(label);

        // Return the original label as the tag if there's no mapping.
        if (tag == null) tag = label;

        return tag;
    }

    private static Map<PublicationSource, File> XSLT = new HashMap<PublicationSource, File>(2);
    static {
        final File xsltDirectory = new File(MIVConfig.getConfig().getProperty("document-config-location-xslt-preprocess"));

        XSLT.put(PublicationSource.PUBMED,
            new File(xsltDirectory,
                MIVConfig.getConfig().getProperty("document-config-location-pubmed-preprocessor")));

        XSLT.put(PublicationSource.ENDNOTE,
            new File(xsltDirectory,
                MIVConfig.getConfig().getProperty("document-config-location-endnote-preprocessor")));
    }
}
