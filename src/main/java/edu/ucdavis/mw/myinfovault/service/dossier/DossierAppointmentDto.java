/**
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAppointmentDto.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * This class represents a dossier appointment.
 * 
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class DossierAppointmentDto implements Serializable
{
    
    private static final long serialVersionUID = -7844229350000136583L;

    int id;
    long dossierId;
    int schoolId;
    int departmentId;
    boolean isComplete = false;
    Timestamp insertTimestamp = null;
    int insertUserID;
    int updateUserID;
    Timestamp updateTimestamp = null;
    boolean isPrimary = false;
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public long getDossierId()
    {
        return dossierId;
    }
    
    public void setDossierId(long dossierId)
    {
        this.dossierId = dossierId;
    }
    
    public int getSchoolId()
    {
        return schoolId;
    }
    
    public void setSchoolId(int schoolId)
    {
        this.schoolId = schoolId;
    }
    
    public int getDepartmentId()
    {
        return departmentId;
    }
    
    public void setDepartmentId(int departmentId)
    {
        this.departmentId = departmentId;
    }
    
    public Timestamp getInsertTimestamp()
    {
        return insertTimestamp;
    }
    
    public void setInsertTimestamp(Timestamp insertTimestamp)
    {
        this.insertTimestamp = insertTimestamp;
    }
    
    public int getInsertUserID()
    {
        return insertUserID;
    }
    
    public void setInsertUserID(int insertUserID)
    {
        this.insertUserID = insertUserID;
    }
    
    public int getUpdateUserID()
    {
        return updateUserID;
    }
  
    public void setUpdateUserID(int updateUserID)
    {
        this.updateUserID = updateUserID;
    }
    
    public Timestamp getUpdateTimestamp()
    {
        return updateTimestamp;
    }
    
    public void setUpdateTimestamp(Timestamp updateTimestamp)
    {
        this.updateTimestamp = updateTimestamp;
    }

    public boolean isPrimary()
    {
        return isPrimary;
    }

    public void setPrimary(boolean isPrimary)
    {
        this.isPrimary = isPrimary;
    }

    public boolean isComplete()
    {
        return isComplete;
    }

    public void setComplete(boolean isComplete)
    {
        this.isComplete = isComplete;
    }
}
