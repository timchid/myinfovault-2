/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Login.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Add Javadoc comments!
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class Login
{
    private Object userIdentifier;
    private String loginName;
    private Date lastLogin;
    private Date lastLogout;
    private Map<String,String> attributes = new HashMap<String,String>();

    public Login()
    {
        //
    }
    public Login(Object userIdentifier, String loginName, Date lastLogin, Date lastLogout)
    {
        if (userIdentifier == null) throw new IllegalArgumentException("User Identifier can not be null");
        if (loginName == null) throw new IllegalArgumentException("Login Name can not be null");

        this.userIdentifier = userIdentifier;
        this.loginName = loginName;
        this.lastLogin = lastLogin;
        this.lastLogout = lastLogout;
    }

    public Object getUserID()
    {
        return this.userIdentifier;
    }

    public String getLoginName()
    {
        return this.loginName;
    }

    public Date getLastLogin()
    {
        return this.lastLogin;
    }

    public Date getLastLogout()
    {
        return this.lastLogout;
    }

    public Map<String,String> getAttributes()
    {
        return this.attributes;
    }

    /**
     * It is an error to add an attribute that is already present. Use {@link #setAttribute(String, String)} instead.
     * @param name
     * @param value
     */
    public void addAttribute(String name, String value)
    {
        if (this.attributes.containsKey(name)) throw new IllegalArgumentException(name + " is already an attribute");
        this.attributes.put(name, value);
    }

    /**
     * Set the value of the named attribute to the given value. Add the attribute if it doesn't exist.
     * @param name
     * @param value
     */
    public void setAttribute(String name, String value)
    {
        if (this.attributes.containsKey(name)) this.attributes.remove(name);
        this.attributes.put(name, value);
    }

    public String getAttribute(String name)
    {
        return this.attributes.get(name);
    }

    public String removeAttribute(String name)
    {
        return this.attributes.remove(name);
    }

    @Override
    public String toString()
    {
        return "" + loginName + ":" + userIdentifier + " Login: " + lastLogin + " Logout: " + lastLogout + " Attributes: " + attributes.toString();
    }
}
