/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierService.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.mw.myinfovault.web.spring.viewdossierstatus.DossierAppointment;

/**
 * DossierService
 *
 * @author Rick Hendricks
 * @since MIV ?
 */
public interface DossierService
{

    /**
     * Initiate the workflow process for a new Dossier by creating and saving a new Kuali workflow
     * document. The Dossier object returned extends the Kuali WorkflowDocument.
     *
     * @param action initial academic action
     * @param workflowAnnotation Annotation to be attached to this workflow document. Set to null if none.
     * @param routingPerson MIV person re-initiating the routing
     * @throws WorkflowException
     */
    public Dossier initiateDossierRouting(AcademicAction action,
                                          String workflowAnnotation,
                                          MivPerson routingPerson) throws WorkflowException;

    /**
     * Initiate the workflow process for a new Dossier.
     *
     * @param action academic action for this dossier
     * @param routingPerson MIV person re-initiating the routing
     * @return initiated dossier
     * @throws WorkflowException
     */
    public Dossier initiateDossierRouting(AcademicAction action,
                                          MivPerson routingPerson) throws WorkflowException;

    /**
     * (Re)Initiate the workflow process for an existing Dossier.
     *
     * @param dossier ID of the dossier to re-initiate
     * @param action new academic action for this dossier
     * @param boolean removeUploads
     * @param routingPerson MIV person re-initiating the routing
     * @return initiated dossier
     * @throws WorkflowException
     */
    public Dossier initiateDossierRouting(long dossierId,
                                          AcademicAction action,
                                          boolean removeUploads,
                                          MivPerson routingPerson) throws WorkflowException;

    /**
     * (Re)Initiate the workflow process for an existing Dossier.
     *
     * @param dossier dossier to re-initiate
     * @param action new academic action for this dossier
     * @param boolean removeUploads
     * @param routingPerson MIV person re-initiating the routing
     * @return initiated dossier
     * @throws WorkflowException
     */
    public Dossier initiateDossierRouting(Dossier dossier,
                                          AcademicAction action,
                                          boolean removeUploads,
                                          MivPerson routingPerson) throws WorkflowException;
    /**
     * (Re)Initiate the workflow process for an existing Dossier.
     *
     * @param dossier ID of the dossier to re-initiate
     * @param action new academic action for this dossier
     * @param listUploadsToRemove for appointment uploads to be deleted
     * @param routingPerson MIV person re-initiating the routing
     * @return initiated dossier
     * @throws WorkflowException
     */
    public Dossier initiateDossierRouting(long dossierId,
                                          AcademicAction action,
                                          List<DossierAppointmentAttributeKey> listUploadsToRemove,
                                          MivPerson routingPerson) throws WorkflowException;

    /**
     * (Re)Initiate the workflow process for an existing Dossier.
     *
     * @param dossier dossier to re-initiate
     * @param action new academic action for this dossier
     * @param listUploadsToRemove for appointment uploads to be deleted
     * @param routingPerson MIV person re-initiating the routing
     * @return initiated dossier
     * @throws WorkflowException
     */
    public Dossier initiateDossierRouting(Dossier dossier,
                                          AcademicAction action,
                                          List<DossierAppointmentAttributeKey> listUploadsToRemove,
                                          MivPerson routingPerson) throws WorkflowException;
    /**
    * Routes a previously created Dossier document to the specified target location in workflow.
    *
     * @param dossier - Dossier object
     * @param annotation - Annotation to be attached to this workflow step. Set to null if none.
     * @param targetLocation - The DossierLocation target location in the workflow.
     * @param currentLocationForValidation - May be set to null if none.
     * This location is used for validation if supplied and would be set to the
     * current location of the dossier by the caller. The location would then be checked against the actual
     * current location of the dossier as it is stored in the database. If there is no match then the dossier
     * location has been updated by a previous routing event and this routing attemmpt will throw a WorkflowExeption.
    * @return Dossier Object
    * @throws WorkflowException
    */
    public Dossier routeDossier(Dossier dossier, String annotation, DossierLocation targetLocation, DossierLocation currentLocationForValidation) throws WorkflowException;


    /**
     * Cancel a previously created Dossier which will remove a workflow document from workflow.
     * @param cancellingPerson - the MivPerson object of the user cancelling this Dossier
     * @param dossierId - The dossier id/document id of the document to be cancelled.
     * @param annotation - Annotation to be attached to this disapproval. Set to null if none.
     * @return Dossier Object
     * @throws WorkflowException
     */
    public Dossier cancelDossier(MivPerson cancellingPerson, Long dossierId, String annotation) throws WorkflowException;

    /**
     * Gets the dossier in workflow for the input dossierId. Only the data in workflow and
     * base local data is loaded. Use getDossierAndLoadAllData(dossierId) to retrieve the workflow
     * data as well as all dossier attribute data.
     * @param dossierId
     * @return Dossier object.
     * @throws WorkflowException
     */
    public Dossier getDossier(long dossierId) throws WorkflowException;

    /**
     * Gets the dossier in workflow for the input dossierId. Only the data in workflow and
     * base local data is loaded. Use getDossierAndLoadAllData(dossierId) to retrieve the workflow
     * data as well as all dossier attribute data.
     * @param dossierId dossier ID
     * @return Dossier object.
     * @throws WorkflowException
     */
    public Dossier getDossier(String dossierId) throws WorkflowException;

    /**
     * Get the dossier in workflow and all local data for the input dossierId
     * @param dossierId
     * @return Dossier object
     * @throws WorkflowException
     */
    Dossier getDossierAndLoadAllData(long dossierId) throws WorkflowException;

    /**
     * Returns a list of active dossiers, scoped appropriately to the given person.
     *
     * @param person MIV person to which dossiers are scoped
     * @return dossiers scoped to the given person
     * @throws WorkflowException if unable to retrieve one or more dossiers
     */
    public List<Dossier> getDossiers(MivPerson person) throws WorkflowException;

    /**
     * Retrieves the currently active dossiers for the given user, or an empty list if
     * nothing is active. A dossier is considered active at any time it is in workflow,
     * except when sitting in the archive queue. Once archived a dossier leaves workflow
     * and is no longer active.
     * This is listed as "Single Dossier" because it will usually return a list
     * with 0 or 1 object. Future enhancement of MIV may allow multiple active
     * actions for a single person. Sending back a dossier that is queued for
     * archiving can also result in multiple dossiers. While we're not supporting
     * that initially in version 3, but this allows for the future capability.
     * @param person The person for whom a dossier is desired
     * @throw WorkflowException, IllegalStateException if more than 1 dossier is found.
     * @return A list containing the currently active dossiers.
     * @throws WorkflowException
     */
    public List<Dossier> getDossierByUser(MivPerson person)
        throws WorkflowException;

    /**
     * Retrieves the currently active dossiers for the given user, or an empty list if
     * nothing is active. A dossier is considered active at any time it is in workflow,
     * except when sitting in the archive queue. Once archived a dossier leaves workflow
     * and is no longer active.
     * This is listed as "Single Dossier" because it will usually return a list
     * with 0 or 1 object. Future enhancement of MIV may allow multiple active
     * actions for a single person. Sending back a dossier that is queued for
     * archiving can also result in multiple dossiers. While we're not supporting
     * that initially in version 3, but this allows for the future capability.
     * @param person The person for whom a dossier is desired
     * @param filter search filter
     * @throw WorkflowException, IllegalStateException if more than 1 dossier is found.
     * @return A list containing the currently active dossiers.
     * @throws WorkflowException
     */
    public List<Dossier> getDossierByUser(MivPerson person, SearchFilter<Dossier> filter)
        throws WorkflowException;

    /**
     * Retrieves the currently active dossiers for the given user, or an empty list if
     * nothing is active. A dossier is considered active at any time it is in workflow,
     * except when sitting in the archive queue. Once archived a dossier leaves workflow
     * and is no longer active.
     * This is listed as "Single Dossier" because it will usually return a list
     * with 0 or 1 object. Future enhancement of MIV may allow multiple active
     * actions for a single person. Sending back a dossier that is queued for
     * archiving can also result in multiple dossiers. While we're not supporting
     * that initially in version 3, but this allows for the future capability.
     * @param person The person for whom a dossier is desired
     * @param overrideIllegalStateException overrideIllegalStateException
     * @throw WorkflowException, IllegalStateException if more than 1 dossier is found and
     *   overrideIllegalStateException is false.
     * @return A list containing the currently active dossiers.
     * @throws WorkflowException
     * @throws IllegalStateException if more than one dossier is found and the override flag is <code>false</code>
     */
    public List<Dossier> getDossier(MivPerson person, boolean overrideIllegalStateException)
    throws WorkflowException, IllegalStateException;

    /**
     * Returns all Dossiers for the given candidate. This list IS NOT SCOPED in any way.
     * @param canidate candidate to which dossiers belong
     * @return candidate's dossiers
     * @throws WorkflowException if unable to retrieve dossiers from the database
     */
    public List<Dossier> getAllDossiersByUser(MivPerson candidate) throws WorkflowException;

    /**
     * Finds dossier in workflow for the input dossierId and loads base data
     * @param dossierId
     * @return Dossier object.
     * @throws WorkflowException
     */
    public Dossier getDossierAndLoadBaseData(long dossierId) throws WorkflowException;

    /**
     * Load the attributes for this dossier
     * @param dossier
     * @return Dossier object
     * @throws WorkflowException
     */
    public Dossier loadDossierAttributes(Dossier dossier) throws WorkflowException;

    /**
     * Load the attributes for this dossier based on the specified location
     * @param dossier
     * @param location
     * @return Dossier
     * @throws WorkflowException
     */
    public Dossier loadDossierAttributesForLocation(Dossier dossier, DossierLocation location) throws WorkflowException;

    /**
     * Delete the attributes for this dossier
     * @param dossier
     * @return Dossier object
     * @throws WorkflowException
     */
    public Dossier deleteDossierAttributes(Dossier dossier) throws WorkflowException;

    /**
     * Save the dossier
     * @param dossier
     * @throws WorkflowException
     */
    public void saveDossier(Dossier dossier) throws WorkflowException;

    /**
     * Returns a list of Dossiers that meet the criteria listed in the AttributeSet.
     * @param criteria
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
     public List<Dossier>  getDossiersWithCriteria(Map<String, String> criteria) throws WorkflowException;

     /**
     * Returns a list of Dossiers that meet the criteria listed in the AttributeSet filtered
     * by the SearchFilter.
     * @param criteria
     * @param filter
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
     public List<Dossier>  getDossiersWithCriteria(Map<String, String> criteria, SearchFilter<Dossier> filter) throws WorkflowException;

     /**
     * Return the dossier to the previous workflow node. This will also reset the review and vote
     * attributes for each appointment.
     *
     * @param returningPerson user approving this Dossier
     * @param realPerson real, logged-in person returning the dossier
     * @param dossierId
     * @param annotation Annotation to be attached to this approval. Set to null if none.
     * @param targetLocation Target location of the return
     * @param currentLocationForValidation May be set to null if none.
     * This location is used for validation if supplied and would be set to the
     * current location of the dossier by the caller. The location would then be checked against the actual
     * current location of the dossier as it is stored in the database. If there is no match then the dossier
     * location has been updated by a previous routing event and this routing attempt will throw a WorkflowExeption.
     * @return Dossier
     * @throws WorkflowException
     */
    public Dossier returnDossierToPreviousNode(MivPerson returningPerson, MivPerson realPerson, long dossierId, String annotation, DossierLocation targetLocation, DossierLocation currentLocationForValidation) throws WorkflowException;

    /**
     * Return the dossier to the previous workflow node. This will also reset the review and vote
     * attributes for each appointment.
     *
     * @param returningPerson the user approving this Dossier
     * @param realPerson real, logged-in person returning the dossier
     * @param dossier
     * @param annotation Annotation to be attached to this approval. Set to null if none.
     * @param targetLocation Target location of the return
     * @param currentLocationForValidation May be set to null if none.
     * This location is used for validation if supplied and would be set to the
     * current location of the dossier by the caller. The location would then be checked against the actual
     * current location of the dossier as it is stored in the database. If there is no match then the dossier
     * location has been updated by a previous routing event and this routing attempt will throw a WorkflowExeption.
     * @return Dossier
     * @throws WorkflowException
     */
    public Dossier returnDossierToPreviousNode(MivPerson returningPerson, MivPerson realPerson, Dossier dossier, String annotation, DossierLocation targetLocation, DossierLocation currentLocationForValidation) throws WorkflowException;

    /**
     * Route the dossier to a specified  workflow node. The node to which the dossier is to be routed
     * must be a future node and not prior to the current node location of the dossier.
     * @param returningPerson the mivperson of the user approving this Dossier
     * @param dossierId
     * @param routeNodeLocation the location to which the dossier is to be routed.
     * @param annotation Annotation to be attached to this approval. Set to null if none.
     * @return Dossier
     * @throws WorkflowException
     */
    public Dossier routeDossierToNode(MivPerson returningPerson, long dossierId, DossierLocation routeNodeLocation,
            String annotation) throws WorkflowException;

    /**
     * Get the dossiers at a specified workflow routeNodeLocation.
     * The results will be scoped to the input person
     * @param person The person for whom a dossier is desired
     * @param routeNodeLocation The route node locations of the dossiers to be located.
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
    public List<Dossier> getDossiersByWorkflowLocations(MivPerson person, DossierLocation ...routeNodeLocation) throws WorkflowException;


    /**
     * Get archived dossiers.
     * The results will be scoped to the input person
     *
     * Note: Archived dossiers are not considered to be at a workflow routeNodeLocation
     * since they are technically out of workflow and at the finalized node.
     *
     * @param person The person for whom a dossier is desired
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
    public List<Dossier> getArchivedDossiers(MivPerson person) throws WorkflowException;

    /**
     * Get dossiers which have been completed and are awaiting archival.
     * The results will be scoped to the input person
     *
     * @param person The person for whom a dossier is desired
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
    public List<Dossier> getCompletedDossiers(MivPerson person) throws WorkflowException;

    /**
     * Get dossiers for which snapshots exist.
     * The results will be scoped to the input person
     *
     * @param person The person for whom a dossier is desired
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
    public List<Dossier> getDossiersForSnapshots(MivPerson person) throws WorkflowException;

    /**
     * Find all dossiers for the input princiaplId for the school, department and location specified.
     * The results will be scoped to those documents which the person would be allowed to view.
     * @param person The person for whom a dossier is desired
     * @param schoolId school id. Set to zero (0) for all
     * @param departmentId department id. Set to zero (0) for all.
     * @param routeNodeLocation The route node location of the dossiers to be located. Set to null for all.
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
    public List<Dossier> getDossiersBySchoolDepartmentAndLocation(MivPerson person, int schoolId, int departmentId, DossierLocation routeNodeLocation)
    throws WorkflowException;

    /**
     * Find all dossiers for the input princiaplId for the school, department specified.
     * The results will be scoped to those documents which the person would be allowed to view.
     *
     * @param person the user requesting the list
     * @param schoolId school ID or zero (0) for all
     * @param departmentId department ID or zero (0) for all
     * @return List of Dossier objects
     * @throws WorkflowException
     */
    public List<Dossier> getDossiersBySchoolAndDepartment(MivPerson person, int schoolId, int departmentId) throws WorkflowException;

    /**
     * Get the dossiers in process for the input person
     * @param person the user requesting the list
     * @return List of Dossier Objects
     * @throws WorkflowException
     */
    public List<Dossier> getDossiersInProcess(MivPerson person) throws WorkflowException;

    /**
     * validateRoutingState - Validate that a dossier is in the correct state to be routed to the next workflow node based on the
     * state of the various dossier attributes.
     * @param dossier
     * @return List of routing exceptions - The list will be empty of there are no exceptions
     * @throws WorkflowException
     */
    public List <String> validateRoutingState(Dossier dossier) throws WorkflowException;

    /**
     * Validate the prerequisite attributes for a dossier at the given workflow node
     * @param dossier The Dossier for which to validate the attributes
     * @param dossierAppointments List of DossierAppointments for this dossier as build in the processDossierAttributes method
     * @param node The workflow node to which the dossier may potentially be routed
     * @return boolean - true if prerequisites are satisfied, otherwise false
     */
    public boolean validateWorkflowNodePrerequisites(Dossier dossier, List<DossierAppointment> dossierAppointments, WorkflowNode node);

    /**
     * Validate the prerequisite attributes for a dossier at the given workflow node
     * @param dossier The Dossier for which to validate the attributes
     * @param node The workflow node to which the dossier may potentially be routed
     * @return <code>true</code> if prerequisites are satisfied, otherwise <code>false</code>
     * @throws WorkflowException
     */
    public boolean validateWorkflowNodePrerequisites(Dossier dossier, WorkflowNode node) throws WorkflowException;

    /**
     * Gets a list of files uploaded for the dossier
     * @param dossier
     * @return List of uploaded files
     */
    public List<File> getUploadFiles(Dossier dossier);

    /**
     * Get files uploaded to the given dossier at the given scope and location.
     *
     * @param dossier dossier to which the uploads belong
     * @param scope upload scope
     * @param location upload location
     * @return uploaded files
     */
    public List<File> getUploadFiles(Dossier dossier, Scope scope, DossierLocation location);

    /**
     * Gets a list of files uploaded of a certain type for the dossier
     * @param dossier
     * @param documentType
     * @return List of uploaded files
     */
    public List<File> getUploadFilesByType(Dossier dossier, int documentType);

    /**
     * Gets a list of files uploaded of a certain type and by a certain user for the dossier
     * @param dossier
     * @param documentType the document type ID
     * @param userId the MIV user ID that uploaded the document
     * @return List of uploaded files
     */
    public List<File> getUploadFilesByTypeAndUser(Dossier dossier, int documentType, int userId);

    /**
     * Gets a list of files uploaded of a certain type and by a certain user for the dossier
     * @param dossierId
     * @param documentType the document type ID
     * @param userId the MIV user ID document owner
     * @return List of uploaded files
     */
    public List<File> getUploadFilesByTypeAndUser(long dossierId, int documentType, int userId);

    /**
     * Gets a list of file uploaded DTOs of a certain type, user, school and department for the dossier
     * @param dossierId
     * @param documentType the document type ID
     * @param userId the MIV user ID document owner
     * @param schoolId school ID
     * @param departmentId department ID
     * @return List of UploadDocumentDtos
     */
    public List<UploadDocumentDto> getUploadFileDtoByTypeUserSchoolAndDepartment(long dossierId, int documentType, int userId, int schoolId, int departmentId);

    /**
     * Delete an upload for the given dossier, scope, name and occurrance at the current location.
     *
     * @param dossier dossier to which upload belongs
     * @param school ID of school associated with the upload
     * @param department ID of department associated with the upload
     * @param attributeName upload dossier attribute name
     * @param occurance occurance of upload if part of a set
     * @param realPerson real, logged-in person
     * @param shadowPerson proxied, switched-to person
     */
    public void deleteUploadFile(Dossier dossier,
                                 int school,
                                 int department,
                                 String attributeName,
                                 int occurance,
                                 MivPerson realPerson,
                                 MivPerson shadowPerson);

    /**
     * Update the dossierReviewers - Add reviewers in the current set that aren't in the
     * previous and remove reviewers in the previous set that aren't in the current.
     *
     * @param actingPerson - Acting, switched-to person performing the update
     * @param currentDossierReviewers - Set of currently selected dossierReviewerDto objects
     * @param previousDossierReviewers - Map of previously selected dossierReviewerDto objects
     */
    public void updateDossierReviewers(MivPerson actingPerson,
                                       Set<DossierReviewerDto> currentDossierReviewers,
                                       Set<DossierReviewerDto> previousDossierReviewers);

    /**
     * Remove the dossierReviewers for a dossier
     * @param dossierId - The dossier id of the dossier
     * @param schoolId - The schoolId of the dossier appointment
     * @param departmentId - The departmentId of the dossier appointment
     */
    public void deleteDossierReviewers(long dossierId, int schoolId, int departmentId);

    /**
     * Get the list of dossier reviewer DTOs corresponding to the dossier and appointment.
     *
     * @param dossier Dossier being reviewed
     * @param appointmentKey Appointment for the dossier being reviewed
     * @return set of dossier reviewer DTOs corresponding to the dossier and appointment.
     * @throws WorkflowException
     */
    public List<DossierReviewerDto> getDossierReviewers(Dossier dossier, DossierAppointmentAttributeKey appointmentKey) throws WorkflowException;

    /**
     * Find dossiers to be reviewed by the input user
     * @param userId the MIV user ID of the reviewer
     * @return List of dossier ids  to be reviewed
     * @throws WorkflowException
     */
    public List<DossierReviewerDto> findDossiersToReview(int userId) throws WorkflowException;

    /**
     * Get the future node through which this document will be routed.
     * @param dossierId the dossierId for which the workflow node is to be determined
     * @return The name of the next node name.
     * @throws WorkflowException
     */
    public WorkflowNode getNextWorkflowNode(long dossierId) throws WorkflowException;


    /**
     * Get the future node through which this document will be routed.
     * @param dossier the dossier for which the workflow node is to be determined
     * @return The name of the next node name.
     * @throws WorkflowException
     */
    public WorkflowNode getNextWorkflowNode(Dossier dossier) throws WorkflowException;

    /**
     * Get the list of next workflow nodes available for the given dossier.
     *
     * @param dossier dossier
     * @return next workflow nodes available for the given dossier
     * @throws WorkflowException
     */
    public List<WorkflowNode> getNextWorkflowNodes(Dossier dossier) throws WorkflowException;

    /**
     * Get the previous node name through which this document was routed.
     * @param dossier the dossier for which the workflow node is to be determined
     * @return The name of the previous node name.
     * @throws WorkflowException if there is an error retrieving the nodes
     */
    public String getPreviousWorkflowNodeName(Dossier dossier) throws WorkflowException;

    /**
     * Get the previous node through which this document was routed.
     * @param dossier the dossier for which the workflow node is to be determined
     * @return The name of the previous node name.
     * @throws WorkflowException if there is an error retrieving the nodes
     */
    public WorkflowNode getPreviousWorkflowNode(Dossier dossier) throws WorkflowException;

    /**
     * Get the previous DossierLocation through which this document was routed.
     * @param dossier the dossier for which the location is to be determined
     * @return DossierLocation The DossierLocatiom of the previous node.
     * @throws WorkflowException if there is an error retrieving the nodes
     */
    public DossierLocation getPreviousWorkflowLocation(Dossier dossier) throws WorkflowException;

    /**
     * Get the locations through which this document was routed.
     * @param dossier the dossier for which the workflow node is to be determined
     * @return A list of the DossierLocations through which this dossier has been routed.
     * @throws WorkflowException if there is an error retrieving the nodes
     */
    public List<DossierLocation> getPreviousWorkflowLocations(Dossier dossier) throws WorkflowException;

    /**
     * Get the current workflow node of this dossier
     * @param dossier the dossier for which the workflow node is to be determined
     * @return WorkflowNode - The current workflow node of this dossier.
     * @throws WorkflowException if there is an error retrieving the nodes
     */
    public WorkflowNode getCurrentWorkflowNode(Dossier dossier) throws WorkflowException;

    /**
     * Retrieve dossiers that the input mivPerson is allowed to view by the name search criteria of the dossier owners.
     *
     * @param mivPerson MivPerson for which to qualify the dossiers
     * @param searchCriteria SearchCriteria containing the name criteria of the dossier owners for which to search
     * @return List of Dossier Objects this person is allowed to view. An empty list is returned if the person
     * is not qualified to view any of the dossiers in the list.
     * @throws WorkflowException if unable to complete the search
     */
    public List<Dossier> getDossiersByUserNameSearchCriteria(MivPerson mivPerson, SearchCriteria searchCriteria) throws WorkflowException;

    /**
     * Retrieve dossiers that the input mivPerson is allowed to view by the name search criteria of the dossier owners and workflow location.
     *
     * @param mivPerson MivPerson for which to qualify the dossiers
     * @param searchCriteria SearchCriteria containing the name criteria of the dossier owners for which to search
     * @param location The workflow location of the dossiers for which to search
     * @return List of Dossier Objects this person is allowed to view. An empty list is returned if the person
     * is not qualified to view any of the dossiers in the list.
     * @throws WorkflowException if unable to complete the search
     */
    public List<Dossier> getDossiersByUserNameSearchCriteriaAndLocation(MivPerson mivPerson, SearchCriteria searchCriteria, DossierLocation location) throws WorkflowException;

    /**
     * Qualify search results for the specified mivPerson.
     *
     * @param mivPerson person for which to qualify the dossiers.
     * @param dossiers List of dossier id's resulting from a search to qualify
     * @return list of dossier objects
     */
    public List<Dossier> qualifyDossiers(MivPerson mivPerson, List<Long> dossiers);

    /**
     * Get archived dossiers by school and department.
     * The results will be scoped to the input person
     *
     * Note: Archived dossiers are not considered to be at a workflow routeNodeLocation
     * since they are technically out of workflow and at the finalized node.
     *
     * @param mivPerson person for which to qualify the dossiers.
     * @param schoolId The schoolId by which to search
     * @param departmentId The schoolId by which to search
     * @return List of Dossier objects.
     * @throws WorkflowException
     */
    public List<Dossier> getArchivedDossiersBySchoolAndDepartment(MivPerson mivPerson, int schoolId, int departmentId) throws WorkflowException;

    /**
     * Remove the PDF uploads for the specified dossier at the upload locations specified.
     *
     * @param dossier dossier from which to remove uploads
     * @param dossierLocations dossier locations where the uploads were acquired
     */
    public void removeUploadsByLocation(Dossier dossier, DossierLocation ... dossierLocations);

    /**
     * Approves a previously created Dossier which will move a the workflow document to
     * the next specified node in the workflow path. This method assumes the person
     * of the user approving the document had been set on the Dossier.
     *
     * @param dossier Dossier object
     * @param annotation Annotation to be attached to this workflow step. Set to null if none.
     * @param routeToLocation The DossierLocation target location in the workflow.
     * @return Dossier Object
     * @throws WorkflowException
     */
    public Dossier approveDossier(Dossier dossier, String annotation, DossierLocation routeToLocation) throws WorkflowException;


    /**
     * Approves a previously created Dossier which will move a the workflow document to the next
     * specified node in the workflow path.
     * This method assumes the approvingPerson of the user approving the document had been set on the Dossier.
     * @param approvingPerson
     * @param dossierId
     * @param annotation Annotation to be attached to this workflow step. Set to null if none.
     * @param routeToLocation The DossierLocation target location in the workflow.
     * @param currentLocationForValidation May be set to null if none.
     * This location is used for validation if supplied and would be set to the
     * current location of the dossier by the caller. The location would then be checked against the actual
     * current location of the dossier as it is stored in the database. If there is no match then the dossier
     * location has been updated by a previous routing event and this routing attempt will throw a WorkflowExeption.
     * @return Dossier Object
     * @throws WorkflowException
     */
    public Dossier approveDossier(MivPerson approvingPerson, long dossierId, String annotation, DossierLocation routeToLocation, DossierLocation currentLocationForValidation) throws WorkflowException;

    /**
     * Approves a previously created Dossier which will move a the workflow document to the next
     * specified node in the workflow path.
     * This method assumes the approvingPerson of the user approving the document had been set on the Dossier.
     * @param dossier Dossier object
     * @param annotation Annotation to be attached to this workflow step. Set to null if none.
     * @param routeToLocation The DossierLocation target location in the workflow.
     * @param currentLocationForValidation May be set to null if none.
     * This location is used for validation if supplied and would be set to the
     * current location of the dossier by the caller. The location would then be checked against the actual
     * current location of the dossier as it is stored in the database. If there is no match then the dossier
     * location has been updated by a previous routing event and this routing attemmpt will throw a WorkflowExeption.
     * @return Dossier Object
     * @throws WorkflowException
     */
    public Dossier approveDossier(Dossier dossier, String annotation, DossierLocation routeToLocation, DossierLocation currentLocationForValidation)
            throws WorkflowException;

    /**
     * Gets the initial workflow node for a Dossier.
     * @param dossier - Dossier object
     * @return WorkflowNode Object
     * @throws WorkflowException
     */
    public WorkflowNode getInitialWorkflowNode(Dossier dossier) throws WorkflowException;

    /**
     * Gets the final workflow node for a Dossier.
     * @param dossier - Dossier object
     * @return WorkflowNode Object
     * @throws WorkflowException
     */
    public WorkflowNode getFinalWorkflowNode(Dossier dossier) throws WorkflowException;

    /**
     * Get a workflow node for the specified location of a dossier.
     * The purpose of this method is to retrieve the specific Workflow node based
     * on the input dossier. This should not be confused with a current or
     * previous workflow nodes. If the current, previous or next nodes are
     * required, use the appropriate methods.
     *
     * @param dossier dossier
     * @param location the location for which to retrieve a WorkflowNode
     * @return workflow node for the specified location of a dossier
     * @throws WorkflowException if there is an error retrieving the nodes
     */
    public WorkflowNode getWorkflowNodeForLocation(Dossier dossier, DossierLocation location)
    throws WorkflowException;

    /**
     * Updates dossier with changes.
     *
     * @param dossier dossier to update
     * @param targetPerson MIV switched-to, target person
     * @param realPerson MIV logged-in, real person performing the update
     */
    public void updateDossier(Dossier dossier,
                              MivPerson targetPerson,
                              MivPerson realPerson);

    /**
     * Get the attribute names applicable for the given criteria.
     *
     * @param dossierLocation dossier workflow location
     * @param delegationAuthority delegation of authority
     * @param actionType dossier action type
     * @param primary if the appointment is primary (otherwise, joint)
     * @return applicable attribute names
     */
    public List<String> getAttributeNames(DossierLocation dossierLocation,
                                          DossierDelegationAuthority delegationAuthority,
                                          DossierActionType actionType,
                                          boolean primary);
}
