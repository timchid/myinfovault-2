/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: InvalidDocumentException.java
 */

package edu.ucdavis.mw.myinfovault.service.signature.representation;

import edu.ucdavis.myinfovault.exception.MIVApplicationException;

/**
 * An invalid document exception is thrown when a document is in an invalid state.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
public class InvalidDocumentException extends MIVApplicationException
{
    private static final long serialVersionUID = 201304171640L;

    /**
     * Create an invalid document exception with the cause and parameterized message.
     *
     * @param messageId the <em>Identifier</em> of an externalized message
     * @param cause cause for raising the exception
     * @param messageParameters parameters associated with the externalized message
     * @see MIVApplicationException#MIVApplicationException(String, Throwable, Object...)
     */
    public InvalidDocumentException(String messageId,
                                    Throwable cause,
                                    Object...messageParameters)
    {
        super(messageId, cause, messageParameters);
    }
}
