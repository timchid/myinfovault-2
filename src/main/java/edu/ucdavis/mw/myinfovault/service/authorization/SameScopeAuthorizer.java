/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SameScopeAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_ASSISTANT;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.EnumSet;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.MivRole.Category;
import edu.ucdavis.mw.myinfovault.service.person.Scope;


/**<p>
 * Authorizes an action when the actor and the target are within the same scope.</p>
 * <p>Scope varies based on the actors role, for example a department administrator has
 * a scope that must be same-school and same-department, while a school administrator
 * has a scope that must be same-school (any department is acceptable).  No other
 * checks are made.</p>
 * <p>Requires either that the <code>PRINCIPAL</code> is supplied in the qualification
 * parameter, or both <code>SCHOOL</code> and <code>DEPARTMENT</code> are supplied.</p>
 * <p>This can be subclassed to test additional conditions by calling the superclass
 * <code>isWithinScope()</code> method first, followed by the additional tests.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.5
 */
public class SameScopeAuthorizer extends PermissionAuthorizerBase implements PermissionAuthorizer
{
    static final int NOT_SET = Integer.MIN_VALUE;
    static final int DONT_CARE = -1;

    /*
     * Default roles that are not scoped; that is, scope is inconsequential
     * when authorizing an actor for any of these roles.
     */
    private static final EnumSet<MivRole> SCOPELESS_ROLES = EnumSet.noneOf(MivRole.class);
    static {
        for (MivRole role : MivRole.values())
        {
            // role is "scopeless" if its scope mask is AnyScope
            if (role.getScope().equals(Scope.AnyScope))
            {
                SCOPELESS_ROLES.add(role);
            }
        }
    }

    /*
     * Default roles that ARE scoped; scope must be considered when
     * when authorizing an actor for any of these roles.
     */
    private static final MivRole[] SCOPED_ROLES = { DEPT_ASSISTANT, DEPT_STAFF, DEPT_CHAIR, SCHOOL_STAFF, DEAN };

    /**
     * Roles that generally have actions that may be scoped. These are used broadly by the hasPermission methods.
     */
    private MivRole[] roles = { DEPT_STAFF, DEPT_CHAIR, SCHOOL_STAFF, DEAN, SENATE_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN };

    /**
     * Roles that ARE scoped; scope must be considered when
     * when authorizing an actor for any of these roles.
     * Subclasses can override these by setting them to a different array.
     */
    protected MivRole[] scopedRoles;

    /**
     * Roles that are not scoped; that is, scope is inconsequential
     * when authorizing an actor for any of these roles.
     * Subclasses can override these by setting them to a different array.
     */
    protected EnumSet<MivRole> unscopedRoles;


    /**
     * TODO: javadoc
     */
    public SameScopeAuthorizer()
    {
        // do nothing, leave defaults
        this.scopedRoles = SCOPED_ROLES;
        this.unscopedRoles = SCOPELESS_ROLES;
    }

    /**
     * TODO: javadoc
     *
     * @param role
     */
    public SameScopeAuthorizer(MivRole... role)
    {
        this();
        roles = new MivRole[role.length];
        System.arraycopy(role, 0, roles, 0, role.length);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.PermissionAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        // In general, is this person allowed to do X?
        // Most users are allowed to do most things, but within a limited scope.
        // Obvious exceptions to this are Add/Edit/Activate users, which candidates
        // and department helpers aren't allowed to do.
        return person.hasRole(this.roles);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.PermissionAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        return hasPermission(person, permissionName, permissionDetails) && hasSharedScope(person, qualification);
    }


    /**
     * hasSameScope
     * @param actor
     * @param qualification
     * @return
     * @throws NumberFormatException
     */
    boolean hasSameScope(final MivPerson actor, final AttributeSet qualification) throws NumberFormatException
    {
        boolean authorized = false;

        final MivPerson targetUser;     // The target of the action, if we're modifying a person.

        final String userId = qualification.get(Qualifier.USERID);
        if (userId != null &&
           (targetUser = MivServiceLocator.getUserService().getPersonByMivId(Integer.parseInt(userId))) != null)
        {
            for (AssignedRole assignedRole : targetUser.getAssignedRoles())
            {
                if (!assignedRole.isPrimary() || !isAuthorizedByRoleAndScope(actor, assignedRole.getScope()))
                {
                    continue;
                }
                authorized = true;
                break;
            }
        }
        else
        {
            /* Get the qualifiers - set the school and department used for scope check. */
            int school = NOT_SET;
            int department = NOT_SET;
            final String qualifiedSchool = qualification.get(Qualifier.SCHOOL);
            final String qualifiedDept   = qualification.get(Qualifier.DEPARTMENT);
            if (qualifiedSchool != null) {
                school = Integer.parseInt(qualifiedSchool);
            }
            if (qualifiedDept != null) {
                department = Integer.parseInt(qualifiedDept);
            }
            authorized = isAuthorizedByRoleAndScope(actor, new Scope(school, department));
        }
        return authorized;
    }


    /*
     * Check that any of the actor's scope(s) match the testScopen based on the actor's role type
     */
    private boolean isAuthorizedByRoleAndScope(final MivPerson actor, final Scope testScope)
    {
        /*
         * These roles are always authorized as the
         * scope for these roles is inconsequential.
         */
        if (actor.hasRole(unscopedRoles)) return true;

        /*
         * These roles are authorized, but only within the same scope
         */
        if (actor.hasRole(scopedRoles))
        {
            // examine each of the actor's assigned roles
            for (AssignedRole assignedRole : actor.getAssignedRoles())
            {
                /*
                 * Scopes must match and one of the following must be true:
                 *  - the actor's assigned role is primary
                 *  - the actor is the dean
                 *  - the actor is the department chair
                 */
                if (assignedRole.getScope().matches(testScope)
                 && (
                         assignedRole.isPrimary()
                      || assignedRole.getRole() == MivRole.DEAN
                      || assignedRole.getRole() == MivRole.DEPT_CHAIR)
                    )
                {
                    return true;
                }
            }
        }

        //else, actor is not authorized by scope
        return false;
    }


    /**
     * Determine if <em>any</em> Scope the actor has matches the target's <em>primary</em> appointment/assignment Scope
     * @param actor
     * @param target
     * @return
     */
    boolean hasSharedScope(final MivPerson actor, final MivPerson target)
    {
        boolean authorized = false;
        for (AssignedRole assignedRole : target.getAssignedRoles())
        {
            if (assignedRole.isPrimary() && hasSharedScope(actor, assignedRole.getScope()))
            {
                authorized = true;
                break;
            }
        }
        return authorized;
    }


    boolean hasSharedScope(final MivPerson actor, final Scope scope)
    {
        for (AssignedRole ar : actor.getAssignedRoles()) {
            if (ar.getRole().getCategory() != Category.AUX && scope.matches(ar.getScope())) return true;
        }

        return false;
    }


    /**
     * Determine if <em>any</em> Scope the actor has matches the qualification provided.
     * @param actor
     * @param qualification
     * @return
     */
    boolean hasSharedScope(final MivPerson actor, final AttributeSet qualification)
    {
        boolean scopeIntersects = false;

        final MivPerson targetUser;     // The target of the action, if we're acting on a person.

        final String userId = qualification.get(Qualifier.USERID);
        if (userId != null &&
           (targetUser = MivServiceLocator.getUserService().getPersonByMivId(Integer.parseInt(userId))) != null)
        {
            switch (targetUser.getPrimaryRoleType().getCategory())
            {
                case STAFF:
                    // Check the Assignments
                    for (AssignedRole assignedRole : targetUser.getAssignedRoles())
                    {
                        if (!assignedRole.isPrimary() || !isAuthorizedByRoleAndScope(actor, assignedRole.getScope()))
                        {
                            continue;
                        }
                        scopeIntersects = true;
                        break;
                    }
                    break;

                case FACULTY:
                    // Check the Appointments
                    for (Appointment appointment : targetUser.getAppointments())
                    {
                        if (!isAuthorizedByRoleAndScope(actor, appointment.getScope()))
                        {
                            continue;
                        }
                        scopeIntersects = true;
                        break;
                    }
                    break;

                default:
                    // Everyone should have either a Faculty or Staff role so this should never be hit.
                    // Nothing. Allow 'intersects' to remain false.
                    break;
            }
//            if (targetUser.getPrimaryRoleType().getCategory() == MivRole.Category.STAFF)
//            {
//                for (AssignedRole assignedRole : targetUser.getAssignedRoles())
//                {
//                    if (!assignedRole.isPrimary() || !isAuthorizedByRoleAndScope(actor, assignedRole.getScope()))
//                    {
//                        continue;
//                    }
//                    scopeIntersects = true;
//                    break;
//                }
//            }
//            else if (targetUser.getPrimaryRoleType().getCategory() == MivRole.Category.FACULTY)
//            {
//                for (Appointment appointment : targetUser.getAppointments())
//                {
//                    if (!isAuthorizedByRoleAndScope(actor, appointment.getScope()))
//                    {
//                        continue;
//                    }
//                    scopeIntersects = true;
//                    break;
//                }
//            }
        }
        else
        {
            /* Get the qualifiers - set the school and department used for scope check. */
            int school = NOT_SET;
            int department = NOT_SET;
            final String qualifiedSchool = qualification.get(Qualifier.SCOPE_SCHOOL);
            final String qualifiedDept   = qualification.get(Qualifier.SCOPE_DEPARTMENT);
            if (qualifiedSchool != null) {
                school = Integer.parseInt(qualifiedSchool);
            }
            if (qualifiedDept != null) {
                department = Integer.parseInt(qualifiedDept);
            }
            scopeIntersects = isAuthorizedByRoleAndScope(actor, new Scope(school, department));
        }

        return scopeIntersects;
    }


//        boolean scopeIntersects = false;
//
//        /* Get the qualifiers - set the school and department used for scope check. */
//        int qualifyingSchool = NOT_SET;
//        int qualifyingDepartment = NOT_SET;
//        final MivPerson targetUser;     // The target of the action, if we're acting on a person.
//
//        final String principalId = qualification.get(Qualifier.PRINCIPAL);
//        if (principalId != null && (targetUser = MivServiceLocator.getUserService().getPersonByPrincipalId(principalId)) != null)
//        {
//            qualifyingSchool = targetUser.getPrimaryAppointment().getSchool();
//            qualifyingDepartment = targetUser.getPrimaryAppointment().getDepartment();
//        }
//        else
//        {
//            final String schoolQualifier = qualification.get(Qualifier.SCHOOL);
//            final String deptQualifier   = qualification.get(Qualifier.DEPARTMENT);
//            if (schoolQualifier != null) {
//                qualifyingSchool = Integer.parseInt(schoolQualifier);
//            }
//            if (deptQualifier != null) {
//                qualifyingDepartment = Integer.parseInt(deptQualifier);
//            }
//        }
//
//
//        /* Get all the appointments the actor has, to compare with the qualifiers. */
//        final Collection<Appointment> appointments = actor.getAppointments();
//
//// FIXME: This needs to check Appointments like this for CANDIDATE, but check AssignedRoles for STAFF
//        /* Check each appointment to see if it matches the school & department qualifiers. */
//        MivRole actorRole = actor.getPrimaryRoleType();
//        for (Appointment appointment : appointments)
//        {
//            switch (actorRole)
//            {
//                case CANDIDATE:
//                case DEPT_ASSISTANT:
//                case DEPT_STAFF:
//                    if (qualifyingDepartment != DONT_CARE && appointment.getDepartment() != qualifyingDepartment) {
//                        // This appointment doesn't match the department, try the next one.
//                        continue;
//                    }
//                    // fall through to also check school
//                case DEAN:
//                case SCHOOL_STAFF:
//                    if (qualifyingSchool != DONT_CARE && appointment.getSchool() != qualifyingSchool) {
//                        // This appointment doesn't match the school, try the next one.
//                        continue;
//                    }
//                    // Otherwise we did match, set true and stop testing.
//                    scopeIntersects = true;
//                    break;
//                case SYS_ADMIN:
//                case VICE_PROVOST:
//                case VICE_PROVOST_STAFF:
//                    // Anything matches, just set true and stop testing.
//                    scopeIntersects = true;
//                    break;
//            }
//
//            if (scopeIntersects) break; // We can stop checking if we've found an intersection
//        }
//
//        return scopeIntersects;
//    }

}
