/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierCreatorService.java
 */
package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.File;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;

/**
 * @author cgilmore
 *
 */
public interface DossierCreatorService
{
    /**
     * Create a dossier based in the input dossier and primary role for the input MivPerson and place in the file specified.
     *
     * @param outputFile file to write dossier
     * @param dossier dossier
     * @param person Person who's role qualification is used for the created dossier
     * @return DossierFiles
     * @throws WorkflowException
     */
    public DossierFiles createDossier(File outputFile, Dossier dossier, MivPerson person) throws WorkflowException;

    /**
     * Create a dossier based in the input dossier and primary role for the input MivPerson and place in the file specified.
     *
     * @param outputStream output stream to write dossier
     * @param dossierId dossierId
     * @param person Person who's role qualification is used for the created dossier
     * @return DossierFiles
     * @throws WorkflowException
     */
    public DossierFiles createDossier(OutputStream outputStream, long dossierId, MivPerson person) throws WorkflowException;

    /**
     * Create a dossier based in the input dossier and primary role for the input MivPerson and write to the specified OutputStream.
     *
     * @param outputStream output stream to write dossier
     * @param dossier dossier
     * @param person Person who's role qualification is used for the created dossier
     * @return DossierFiles
     */
    public DossierFiles createDossier(OutputStream outputStream, Dossier dossier, MivPerson person);


    /**
     * Create a dossier based in the input dossier and role and write to the OutputStream specified.
     *
     * @param outputStream output stream to write dossier
     * @param dossierId dossierId
     * @param role role qualification is used for the created dossier
     * @param schoolDepartmentCriteriaMap School and Role qualification for the created dossier
     * @return DossierFiles
     * @throws WorkflowException
     */
    public DossierFiles createDossier(OutputStream outputStream, long dossierId, MivRole role, Map<String, SchoolDepartmentCriteria> schoolDepartmentCriteriaMap) throws WorkflowException;

    /**
     * Create a dossier based in the input dossier and role and write to the OutputStream specified.
     *
     * @param outputStream  output stream to write dossier
     * @param dossier dossier
     * @param role role qualification is used for the created dossier
     * @param schoolDepartmentCriteriaMap School and Role qualification for the created dossier
     * @return DossierFiles
     * @throws WorkflowException
     */
    public DossierFiles createDossier(OutputStream outputStream, Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap) throws WorkflowException;

    /**
     * Create a dossier based in the input dossier and role and write to the OutputStream specified.
     *
     * @param outputStream output stream to write dossier
     * @param dossierId dossierId
     * @param role role qualification is used for the created dossier
     * @return DossierFiles
     * @throws WorkflowException
     */
    public DossierFiles createDossier(OutputStream outputStream, long dossierId, MivRole role) throws WorkflowException;

    /**
     * Create a dossier based in the input dossier and input MivRole and write to the OutputStream specified.
     *
     * @param outputStream output stream to write dossier
     * @param dossier dossier
     * @param role Role qualification for the created dossier
     * @return DossierFiles
     */
    public DossierFiles createDossier(OutputStream outputStream, Dossier dossier, MivRole role);

    /**
     * Create a dossier based in the input dossier and input MivRole and place in the file specified.
     *
     * @param outputFile file to write dossier
     * @param dossier dossier
     * @param role Role qualification for the created dossier
     * @return DossierFiles
     * @throws WorkflowException
     */
    public DossierFiles createDossier(File outputFile, Dossier dossier, MivRole role) throws WorkflowException;

    /**
     * Create a dossier based in the input dossier and input MivRole and place in the file specified.
     *
     * @param outputFile file to write dossier
     * @param dossier dossier
     * @param role Role qualification for the created dossier
     * @param schoolDepartmentCriteriaMap School and Role qualification for the created dossier
     * @return DossierFiles
     * @throws WorkflowException
     */
    public DossierFiles createDossier(File outputFile, Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap) throws WorkflowException;

    /**
     * Create a dossier snapshot based in the input dossier and role qualifier and place in the file specified.
     * Upon successful snapshot creation, an entry is written to the DossierSnapshot table.
     *
     * @param outputFile file to write snapshot
     * @param dossier dossier
     * @param role role qualification for the created snapshot
     * @return DTO representing the snapshot
     * @throws WorkflowException
     */
    public SnapshotDto createSnapshot(File outputFile, Dossier dossier, MivRole role) throws WorkflowException;

    /**
     * Create a dossier snapshot based in the input dossier and input MivRole and place in the file specified.
     *
     * @param outputFile file to write dossier
     * @param dossier dossier
     * @param role Role qualification for the created dossier
     * @param schoolDepartmentCriteriaMap School and Role qualification for the created dossier
     * @return DTO object representing the snapshot
     * @throws WorkflowException
     */
    public SnapshotDto createSnapshot(File outputFile, Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap) throws WorkflowException;

    /**
     * Get map of the URLs for the dossier files this role is allowed to view.
     *
     * @param dossier dossier
     * @param person Person who's role qualification is used for the created dossier
     * @return URLs for the dossier and person
     */
    public Map<String, String> getDossierFileUrlMap(Dossier dossier, MivPerson person);

    /**
     * Get map of the URLs for the dossier files this role is allowed to view.
     *
     * @param dossier dossier
     * @param role role qualification to be used for the created dossier
     * @param schoolDepartmentCriteriaMap further qualification by school and department, <code>null</code> for no further qualification
     * @return URLs for the dossier, person, and criteria
     */
    public Map<String, String> getDossierFileUrlMap(Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap);

    /**
     * Get map of the DossierDocumentDto's for the dossier files this role is allowed to view.
     *
     * @param dossier dossier
     * @param person Person who's role qualification is used for the created dossier
     * @return dossier document DTOs for the dossier and person
     */
    public Map<File, DossierDocumentDto> getDossierFiles(Dossier dossier, MivPerson person);

    /**
     * Get map of the DossierDocumentDto's for the dossier files this role is allowed to view.
     *
     * @param dossier dossier
     * @param role role qualification to be used for the created dossier
     * @param schoolDepartmentCriteriaMap further qualification by school and department - null for no further qualification
     * @return dossier document DTOs for the dossier, person, and criteria
     */
    public Map<File, DossierDocumentDto> getDossierFiles(Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap);

    /**
     * Get the documents authorized for the given MIV role.
     *
     * @param role MIV role
     * @return list of documents user with given role is allowed to access
     */
    public List<DossierDocumentDto> getAuthorizedDocuments(MivRole role);
}
