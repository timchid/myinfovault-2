package edu.ucdavis.mw.myinfovault.service.biosketch;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchType;

/**
 * @author dreddy
 * @since MIV 2.1
 */
public interface IBiosketchService
{
    /**
     * This method returns the List of biosketch's associated with a particular user.
     * 
     * @param userId
     * @param biosketchType
     * @return
     */
    public List<Biosketch> getBiosketchList(int userId, int biosketchType);

    /**
     * This method returns a biosketch. Other information
     * associated with it is : style, ruleset, section
     * list and attribute list information.
     * 
     * @param biosketchId
     * @return
     */
    public Biosketch getBiosketch(int biosketchId);

    /**
     * This method returns a new biosketch object with default style, default section list and default attribute list information.
     * 
     * @param biosketchType
     * @param userID
     * @return
     */
    public Biosketch createNewBiosketch(BiosketchType biosketchType, int userID);

    /**
     * This method saves/updates a biosketch.
     * 
     * @param biosketch
     * @return
     */
    public Biosketch saveBiosketch(Biosketch biosketch);

    /**
     * This method deletes a biosketch. Information associated with it is : style , ruleset, section list and attributes list
     * 
     * @param biosketch
     */
    public void deleteBiosketch(Biosketch biosketch);

    /**
     * This method duplicates the information associated with a existing biosketch
     * 
     * @param biosketchId
     * @return
     */
    public int duplicateBiosketch(int biosketchId);

    /**
     * This method returns the Biosketch after setting the
     * BiosketchData on it. The information associated with
     * BiosketchData is : Biosketch Exclude list and Display Section list.
     * 
     * @param biosketchId
     * @return
     */
    public Biosketch loadBiosketchDesignData(int biosketchId);

    /**
     * This method returns the Biosketch after setting the
     * BiosketchData on it. The information associated with
     * BiosketchData is : Biosketch Exclude list and Display Section list.
     * 
     * @param biosketch
     * @return
     */
    public Biosketch loadBiosketchDesignData(Biosketch biosketch);

    /**
     * This method returns the Biosketch after setting the
     * BiosketchData on it. The information associated with
     * BiosketchData is : Biosketch Exclude list and Display Section list.
     * 
     * @param biosketchId
     * @return
     */
    public Biosketch loadBiosketchData(int biosketchId);

    /**
     * 
     * 
     * @param biosketch
     * @return
     */
    public Biosketch loadBiosketchData(Biosketch biosketch);

    /**
     * This method returns all the names(duplicate) of same Biosketch for a single user.
     * 
     * @param userID
     * @param originalName
     * @return
     */
    public List<String> getBiosketchNamesForUser(int userID,String originalName);
}
