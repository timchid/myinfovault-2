/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupViewFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Filter out a collection of groups that the acting person has permission to view.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupViewFilter extends SearchFilterAdapter<Group>
{
    private static final AuthorizationService authorizationService = MivServiceLocator.getAuthorizationService();

    private AttributeSet permissionDetails = new AttributeSet();

    private MivPerson actingPerson;

    /**
     * @param actingPerson The switched-to, acting person
     */
    public GroupViewFilter(MivPerson actingPerson)
    {
        this.actingPerson = actingPerson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean include(Group item)
    {
        //put group ID in permission details
        permissionDetails.put(PermissionDetail.TARGET_ID,
                              Integer.toString(item.getId()));

        return authorizationService.isAuthorized(actingPerson,
                                                 Permission.VIEW_GROUP,
                                                 permissionDetails,
                                                 null);
    }
}
