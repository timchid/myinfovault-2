/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiAuthorizerBase.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.util.Collection;
import java.util.Set;

import javax.security.auth.login.CredentialException;

import org.springframework.beans.factory.annotation.Autowired;

import edu.ucdavis.mw.myinfovault.dao.apientity.ApiEntityDao;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Base class providing some shared members and methods common to ApiAuthorizers.
 *
 * @author japorito
 * @since 5.0
 */
public abstract class ApiAuthorizerBase implements ApiAuthorizer
{
    protected static ApiEntityDao entityDao;
    protected final ApiSecurityScheme scheme;
    protected static final String INVALID_ENTITY = "Attempting authenticate invalid entity.";
    protected static final String INVALID_SECURITY_SCHEME = "Invalid security scheme for specified API entity.";
    protected static final String INVALID_ENTITY_NAME = "Entity name ({0}) does not match what was expected ({1}).";
    protected static final String INVALID_CREDENTIAL_FORMAT = "Invalid credential format.";

    public ApiAuthorizerBase(ApiSecurityScheme scheme)
    {
        this.scheme = scheme;
    }

    /**
     * Gets an API entity object with the specified name and this.scheme. Object may be blank if
     * an entity with the given name does not exist in the database.
     *
     * @param entityName
     * @return The Api Entity with the specified entity name, or a new, blank ApiEntity if one does not exist with that name.
     * @throws CredentialException
     */
    protected ApiEntity loadEntity(String entityName) throws CredentialException
    {
        ApiEntity entity = entityDao.getApiEntity(entityName);

        if (entity == null)
        {
            entity = new ApiEntityImpl(entityName, this.scheme);
        }

        if (entity.getSecurityScheme() != this.scheme)
        {
            throw new CredentialException();
        }

        return entity;
    }

    /**
     * Set the entityDao member.
     *
     * @param entityDao the entityDao to set
     */
    @Autowired
    public static void setEntityDao(ApiEntityDao entityDao)
    {
        ApiAuthorizerBase.entityDao = entityDao;
    }

    @Override
    public boolean permitsOneOf(String authHeader,
                                Set<ApiScope> scopesRequired,
                                Collection<MivRole> roles) throws CredentialException
    {
        boolean permitted = false;

        for (ApiScope scopeRequired : scopesRequired)
        {
            if (permits(authHeader, scopeRequired, roles))
            {
                permitted = true;
                break;
            }
        }

        return permitted;
    }

    @Override
    public boolean permitsOneOf(String authHeader,
                                Set<ApiScope> scopesRequired) throws CredentialException
    {
        return permitsOneOf(authHeader, scopesRequired, null);
    }
}
