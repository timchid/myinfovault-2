/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: QueryToolSuggestionHandler.java
 */

package edu.ucdavis.mw.myinfovault.service.suggest;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.common.service.suggest.AbstractSuggestionHandler;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.QueryTool;

/**
 * Handles suggestion service via the query tool.
 *
 * @author Stephen Paulsen
 * @since MIV v4.4
 */
public class QueryToolSuggestionHandler extends AbstractSuggestionHandler<Map<String,String>>
{
    private static final Logger log = LoggerFactory.getLogger(QueryToolSuggestionHandler.class);

    private static final String SQL_STATEMENT = "SELECT {0} FROM {1} WHERE {2} LIKE ? AND {3} ORDER BY {4}";
    private static final String DEFAULT_SQL_STATEMENT = "SELECT {0} FROM {1} WHERE {2} ORDER BY {3}";

    private final String tableName;
    private final String key;
    private final String searchColumn;
    private final String fetchColumns;
    private final String sortColumn;
    private final String conditions;
    private final String defaultconditions;
    private final String sql;

    private String defaultsql = null;

    /**
     * Create a suggestion handler for the given handler name, topic, and properties.
     *
     * @param name handler name
     * @param topic handler topic
     * @param p handler properties
     */
    public QueryToolSuggestionHandler(String name, String topic, Properties p)
    {
        super(name, topic, p);

        this.tableName = getOption("table");
        this.key = getOption("key");
        this.searchColumn = getOption("searchcol");
        this.fetchColumns = getOption("fetchcols");
        this.sortColumn = getOption("sortcol", searchColumn);
        this.conditions = getOption("conditions", Boolean.TRUE.toString());
        this.defaultconditions = getOption("default.conditions", Boolean.TRUE.toString());

        this.sql = MessageFormat.format(SQL_STATEMENT, this.fetchColumns,
                                                       this.tableName,
                                                       this.searchColumn,
                                                       this.conditions,
                                                       this.sortColumn);

        if (this.defaultconditions != null)
        {
            log.info("this.defaultconditions :: {}", this.defaultconditions);

            this.defaultsql = MessageFormat.format(DEFAULT_SQL_STATEMENT, this.fetchColumns,
                                                                          this.tableName,
                                                                          this.defaultconditions,
                                                                          this.sortColumn);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.suggest.AbstractSuggestionHandler#getNewSuggestions(java.lang.String, java.lang.String)
     */
    @Override
    public Iterable<Map<String,String>> getNewSuggestions(String topic, String term)
    {
        if (log.isDebugEnabled())
        {
            log.debug("My handler name is [{}]", this.handlerName);
            log.debug("          topic is [{}]", this.handlerTopic);
            log.debug(" Config Properties are:\n {}", this.config);
        }

        QueryTool qt = MIVConfig.getConfig().getQueryTool();

        // If the term is empty call the default query
        if (StringUtils.isBlank(term))
        {
            if (StringUtils.isNotBlank(this.defaultsql))
            {
                log.debug(this.defaultsql);

                return qt.runQuery(this.defaultsql, this.key).values();
            }

            return Collections.emptyList();
        }

        // Otherwise, the term is not empty, call parameterized query
        log.debug(this.sql);

        return qt.runQuery(this.sql,
                           this.key,
                           MessageFormat.format(this.matchType.getFormatPattern(), term)).values();
    }
}
