/*
 * Copyright © 2007-2010 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SchoolDepartmentCriteria.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.Serializable;

/**
 * This class is used to qualify dossiers by by school and department.
 *
 * @author rhendric
 * @since MIV 3.6
 * //@/deprecated Replace all uses of this class with {@link edu.ucdavis.mw.myinfovault.service.person.Scope Scope} instead.
 */
//@/Deprecated
public class SchoolDepartmentCriteria implements Serializable
{
    private static final long serialVersionUID = 20100914155840L;


    private int department = 0;

    private int school = 0;

    public int getDepartment()
    {
        return this.department;
    }

    public void setDepartment(int department)
    {
        this.department = department;
    }

    public int getSchool()
    {
        return school;
    }

    public void setSchool(int school)
    {
            this.school = school;
    }
}
