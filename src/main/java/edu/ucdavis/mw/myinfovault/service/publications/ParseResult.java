/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ParseResult.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import java.io.File;
import java.io.Serializable;

/**
 * Holds results and statistics about a publications
 * parse. May be made generic for use elsewhere.
 *
 * @author Craig Gilmore
 * @since MIV 4.4.2
 */
public class ParseResult extends StatResult implements Serializable
{
    private static final long serialVersionUID = 201206121033L;

    private final int targetUser;
    private final File source;
    private final File transformedSource;
    private int parsed = 0;

    /**
     * Create a parse result for the original and transformed source.
     *
     * @param targetUser The ID of the switched-to user performing this parse
     * @param source The original source file to parse
     * @param transformedSource The parsed and transformed file
     */
    protected ParseResult(int targetUser,
                          File source,
                          File transformedSource)
    {
        this.targetUser = targetUser;
        this.source = source;
        this.transformedSource = transformedSource;
    }

    /**
     * @return The ID of the switched-to user performing this parse
     */
    protected int getTargetUser()
    {
        return targetUser;
    }

    /**
     * @return The original source file to parse
     */
    protected File getSource()
    {
        return source;
    }

    /**
     * @return The parsed and transformed file
     */
    protected File getTransformedSource()
    {
        return transformedSource;
    }

    /**
     * @return The number of publications parsed
     */
    protected int getParsed()
    {
        return parsed;
    }

    /**
     * Add general parse error.
     *
     * @param exception Error cause
     */
    protected void fail(Exception exception)
    {
        fail("", exception);
    }

    /**
     * Add a parse error referenced by an identifier.
     *
     * @param id Reference identifier for the error
     * @param exception Cause of the error
     */
    protected void fail(String id, Exception exception)
    {
        errors.add(new ParseError(id, exception));
    }

    /**
     * Increment duplicate count
     */
    protected void duplicate()
    {
        duplicate(1);
    }

    /**
     * Increment duplicate count by given count.
     *
     * @param count Amount to increment duplicate count
     */
    protected void duplicate(int count)
    {
        duplicates += count;
    }

    /**
     * Increment parsed count
     */
    protected void parsed()
    {
        parsed++;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "Parse Report" +
               "\n------------" +
               "\nTarget user: " + targetUser +
               "\n\nSource file: " + source.getAbsolutePath() +
               "\nTransformed Source file: " + transformedSource.getAbsolutePath() +
               "\n\nParsed: " + parsed +
               "\nDuplicates: " + duplicates +
               "\nFailures: " + errors.size() +
               "\nTotal processed: " + processed +
               listErrors();
    }

    //return a list of parse errors for report
    private String listErrors()
    {
        StringBuilder list = new StringBuilder();

        if(!errors.isEmpty())
        {
            list.append("\n\nExceptions");

            for(ParseError error : errors)
            {
                list.append("\n* ").append(error);
            }
        }

        return list.toString();
    }
}
