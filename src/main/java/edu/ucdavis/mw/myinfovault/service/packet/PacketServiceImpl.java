/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.packet;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.packet.PacketDao;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.events2.DossierReturnEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.events2.PacketRequestCancellationEvent;
import edu.ucdavis.mw.myinfovault.events2.PacketRequestEvent;
import edu.ucdavis.mw.myinfovault.events2.PacketSubmissionEvent;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.NewDossierServiceImpl;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService;
import edu.ucdavis.mw.myinfovault.util.FileUtil;
import edu.ucdavis.myinfovault.document.Builder.BuilderDocumentType;
import edu.ucdavis.myinfovault.document.Document;
import edu.ucdavis.myinfovault.document.DocumentFile;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PDFConcatenator;
import edu.ucdavis.myinfovault.document.PacketBuilder;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * PacketService Implementation
 * @author rhendric
 *
 * @since MIV 5.0
 */
public class PacketServiceImpl implements PacketService
{

    private static Logger logger = LoggerFactory.getLogger(NewDossierServiceImpl.class);

    private PacketDao packetDao;
    private DossierService dossierService;
    private WorkflowService workflowService;
    private DossierCreatorService dossierCreatorService;
    private UserService userService;

    public PacketServiceImpl(PacketDao packetDao,
                             DossierService dossierService,
                             WorkflowService workflowService,
                             DossierCreatorService dossierCreatorService,
                             UserService userService)
    {
        this.packetDao = packetDao;
        this.dossierService = dossierService;
        this.workflowService = workflowService;
        this.dossierCreatorService = dossierCreatorService;
        this.userService = userService;

        EventDispatcher2.getDispatcher().register(this);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getPacketRequest(int)
     */
    @Override
    public PacketRequest getPacketRequest(long dossierId)
    {

        return packetDao.getPacketRequest(dossierId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getPacketRequests(int)
     */
    @Override
    public List <PacketRequest> getPacketRequests(int userId)
    {
        return packetDao.getPacketRequests(userId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getOpenPacketRequests(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<PacketRequest> getOpenPacketRequests(MivPerson targetPerson)
    {
        return this.getOpenPacketRequests(targetPerson.getUserId());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getOpenPacketRequests(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<PacketRequest> getOpenPacketRequests(int userId)
    {
        return packetDao.getOpenPacketRequests(userId);
    }

    @Override
    public List <PacketRequest> getPacketRequests(MivPerson mivPerson)
    {
        return packetDao.getPacketRequests(mivPerson.getUserId());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getPacketRequest(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public PacketRequest getPacketRequest(Dossier dossier)
    {
        return getPacketRequest(dossier.getDossierId());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#updatePacketRequest(int)
     */
    @Override
    public boolean updatePacketRequest(PacketRequest packetRequest, int updateUserId)
    {
        return packetDao.updatePacketRequest(packetRequest, updateUserId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#requestPacket(edu.ucdavis.mw.myinfovault.events2.PacketRequestEvent)
     */
    @Override
    @Subscribe
    public PacketRequest requestPacket(PacketRequestEvent event)
    {
        // Get any existing packet request for this dossier
        Dossier dossier = event.getDossier();
        PacketRequest packetRequest = this.getPacketRequest(dossier);
        if (packetRequest == null)
        {
            // No packet request exists for this dossier, create a new one.
            packetRequest = new PacketRequest(dossier);
        }
        // Set packetId=0 indicating the request is opened/re-opened.
        packetRequest.setPacketId(0);
        this.updatePacketRequest(packetRequest, event.getRealPerson().getUserId());

        if (workflowService.getCurrentWorkflowNode(dossier).getNodeLocation() == DossierLocation.DEPARTMENT)
        {

            try
            {
                dossier.setRoutingPerson(event.getRealPerson());
                dossierService.returnDossierToPreviousNode(event.getShadowPerson(), event.getRealPerson(), dossier, "Dossier routed in response to a Packet Request", DossierLocation.PACKETREQUEST, DossierLocation.DEPARTMENT);
                EventDispatcher2.getDispatcher().post( new DossierReturnEvent(event,
                        event.getRealPerson(),
                        dossier,
                        DossierLocation.DEPARTMENT,
                        DossierLocation.PACKETREQUEST
                    ).setShadowPerson(event.getShadowPerson()));
            }
            catch (WorkflowException e)
            {
                logger.error("Unable to return dossier "+dossier.getDossierId()+" to PacketRequest location for "+dossier.getAction().getCandidate().getDisplayName(),e);
            }
        }
        return packetRequest;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#requestPacket(edu.ucdavis.mw.myinfovault.events2.PacketRequestEvent)
     */
    @Override
    @Subscribe
    public boolean packetSubmitted(PacketSubmissionEvent event)
    {
        // Get any existing packet request for this dossier
        Dossier dossier = event.getDossier();
        PacketRequest packetRequest = this.getPacketRequest(dossier);
        if (packetRequest == null)
        {
            // No packet request exists for this dossier, something went wrong.
            logger.error("Packet submission event fired, but no packet request found for dossier " + dossier.getDossierId());
            return false;
        }

        if (workflowService.getCurrentWorkflowNode(dossier).getNodeLocation() == DossierLocation.PACKETREQUEST)
        {

            try
            {
                dossier.setRoutingPerson(event.getRealPerson());
                dossierService.routeDossier(dossier,
                                            "Dossier routed on packet submission.",
                                            DossierLocation.DEPARTMENT,
                                            DossierLocation.PACKETREQUEST);

                dossierService.loadDossierAttributes(dossier);
                dossier.setAttributeStatus("packet_request",
                                           DossierAttributeStatus.CLOSE,
                                           dossier.getPrimarySchoolId(),
                                           dossier.getPrimaryDepartmentId());
                dossierService.saveDossier(dossier);
            }
            catch (WorkflowException e)
            {
                logger.error("Unable to route dossier "+dossier.getDossierId()+" to Department location for "+dossier.getAction().getCandidate().getDisplayName(),e);
                return false;
            }
        }

        return true;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#cancelPacketRequest(edu.ucdavis.mw.myinfovault.events2.PacketRequestCancellationEvent)
     */
    @Override
    @Subscribe
    public PacketRequest cancelPacketRequest(PacketRequestCancellationEvent event)
    {
        // Get any existing packet request for this dossier
        Dossier dossier = event.getDossier();
        PacketRequest packetRequest = this.getPacketRequest(dossier);

        if (packetRequest == null)
        {
            // No packet request exists for this dossier, create a new one.
            packetRequest = new PacketRequest(dossier);
        }

        // Set packet id = -1 to indicate a cancelled packet request
        packetRequest.setPacketId(-1);

        this.updatePacketRequest(packetRequest, event.getRealPerson().getUserId());

        if (workflowService.getCurrentWorkflowNode(dossier).getNodeLocation() == DossierLocation.PACKETREQUEST)
        {
            try
            {
                dossier.setRoutingPerson(event.getRealPerson());
                dossierService.routeDossier(dossier, "Dossier routed in response to a Packet Request", DossierLocation.DEPARTMENT, DossierLocation.PACKETREQUEST);
                EventDispatcher2.getDispatcher().post(new DossierRouteEvent(event,
                        event.getRealPerson(),
                        dossier,
                        DossierLocation.PACKETREQUEST,
                        DossierLocation.DEPARTMENT
                    ).setShadowPerson(event.getShadowPerson()));
            }
            catch (WorkflowException e)
            {
                logger.error("Unable to route dossier "+dossier.getDossierId()+" to Department location for "+dossier.getAction().getCandidate().getDisplayName(),e);
            }
        }

        return packetRequest;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#deletePacketRequest(edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest)
     */
    @Override
    public boolean deletePacketRequest(PacketRequest packetRequest)
    {
        return packetDao.deletePacketRequest(packetRequest);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getPacketUpdateTime(java.lang.Integer)
     */
    @Override
    public Date getPacketUpdateTime(long packetId)
    {
        return packetDao.getPacketUpdateTime(packetId);

    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getPackets(int)
     */
    @Override
    public List<Packet> getPackets(int userId)
    {
       return packetDao.getPackets(userId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#getPacket(int, int)
     */
    @Override
    public Packet getPacket(long packetId)
    {
        return packetDao.getPacket(packetId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#deletePacket(int, int)
     */
    @Override
    public boolean deletePacket(long packetId)
    {
        return packetDao.deletePacket(packetId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#updatePacket(edu.ucdavis.mw.myinfovault.domain.packet.Packet, int)
     */
    @Override
    public Packet updatePacket(Packet packet, int updateUserId)
    {
        return packetDao.updatePacket(packet, updateUserId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#copyPacket(edu.ucdavis.mw.myinfovault.domain.packet.Packet)
     */
    @Override
    public Packet copyPacket(Packet sourcePacket,  int updateUserId)
    {

        if (sourcePacket.getPacketId() <= 0)
        {
            throw new MivSevereApplicationError("errors.packet.packetservice.copy", sourcePacket);
        }

        // Filter to only include "Copy of" names for the packet to be copied
        final String patterMatchString = "^Copy(\\s|\\s\\(\\d+\\)\\s)of ";
        Pattern namePattern = Pattern.compile(patterMatchString+sourcePacket.getPacketName().replace("(", "\\(").replace(")", "\\)"));


        PacketNameFilter<Packet> filter = new PacketNameFilter<Packet>(namePattern)
        {
            @Override
            protected String getName(Packet item)
            {
                return item.getPacketName();
            }
        };

        // Filter packets
        List<Packet>packetList = filter.apply(this.getPackets(sourcePacket.getUserId()));

        String targetName = null;

        if (packetList.isEmpty())
        {
            targetName = "Copy of "+sourcePacket.getPacketName();
        }
        else
        {
            // Sort the names descending based on copy number
            Collections.sort(packetList, new Comparator<Packet>(){
                Pattern sortPattern = Pattern.compile("^Copy (\\(\\d+\\))");
                Matcher matcher = null;

                @Override
                public int compare(Packet packet1, Packet packet2)
                {
                    matcher = sortPattern.matcher(packet1.getPacketName());
                    int copyNum1 = matcher.find() ? Integer.parseInt(matcher.group(1).replace("(", "").replace(")", "").trim()) : 0;
                    matcher = sortPattern.matcher(packet2.getPacketName());
                    int copyNum2 = matcher.find() ? Integer.parseInt(matcher.group(1).replace("(", "").replace(")", "").trim()) : 0;
                    return copyNum1 < copyNum2 ? 1 : -1;
                }
            });

            // The highest copy number will be the first in the list
            String sourceName = packetList.get(0).getPacketName();

            // Get the new copy number (N) in "Copy (N) of"
            Matcher m = namePattern.matcher(sourceName);
            int copyNumber = m.find() && !m.group(1).trim().isEmpty() ? Integer.parseInt(m.group(1).replace("(", "").replace(")", "").trim())+1 : 2;

            // If the copy number is 2, there is nothing to replace so we build a new name
            targetName = copyNumber == 2 ?
                    sourceName.replaceFirst("Copy of", "Copy ("+copyNumber+") of") :
                    sourceName.replaceFirst("(\\d+)", Integer.toString(copyNumber));

        }

        Packet targetPacket = new Packet(updateUserId, targetName);

        // Copy the PacketContent items. The 0 (zero) id will be updated to the new packet id number in the DAO
        for (PacketContent sourcePacketItem : sourcePacket.getPacketItems())
        {
            targetPacket.addContentItem(new PacketContent(0, sourcePacketItem.getUserId(), sourcePacketItem.getSectionId(), sourcePacketItem.getRecordId(), sourcePacketItem.getDisplayParts()));
        }

        targetPacket = packetDao.updatePacket(targetPacket, updateUserId);

        packetDao.copyPacketAnnotations(sourcePacket, targetPacket, updateUserId);

        return targetPacket;
    }

    /**
     * Filter the items based on the packetContentMap
     * @author rhendric
     *
     * @param <T> PacketContentMap
     */
    public abstract class PacketNameFilter<T> extends SearchFilterAdapter<T>
    {

        Pattern regexPattern = null;

        /**
         * Creates a filter for the sections in the map.
         *
         * @param sections to filter
         */
        public PacketNameFilter(Pattern regexPattern)
        {
            this.regexPattern = regexPattern;
        }

        /*
         * (non-Javadoc)
         * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
         */
        @Override
        public boolean include(T item)
        {
            return regexPattern.matcher(getName(item)).matches();
        }

        /**
         * Get the name from an item to filter.
         *
         * @param item An item within the collection to filter
         * @return the name for the given item
         */
        protected abstract String getName(T item);

    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#SubmitPacket(edu.ucdavis.mw.myinfovault.domain.packet.Packet, edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest, int)
     */
    @Override
    public boolean submitPacket(Packet packet, PacketRequest packetRequest, int updateUserId)
    {
        // Clean up any previously existing packet files for this dossier
        Dossier dossier = packetRequest.getDossier();
        this.deletePacketFilesFromDossier(packet, dossier);

        List<Document>packetDocuments = this.createPacket(packet);

        // If any of the packet PDF files are invalid, the submission fails
        if (!this.validatePacketFiles(packet.getPacketId(), packetDocuments))
        {
            return false;
        }

        // All files are valid, now copy the packet files into the dossier directory
        boolean success = this.copyPacketFiles(dossier, packetDocuments);

        if (success)
        {
            packetRequest.setPacketId(packet.getPacketId());
            this.updatePacketRequest(packetRequest, updateUserId);
            EventDispatcher2.getDispatcher().post(new PacketSubmissionEvent(userService.getPersonByMivId(updateUserId), dossier)
                .setComments("Satisfied packet request for dossier "+dossier.getDossierId()+" - "+dossier.getAction().getDelegationAuthority()+": "+dossier.getAction().getActionType()));

        }
        return success;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#createPacket(edu.ucdavis.mw.myinfovault.domain.packet.Packet)
     */
    @Override
    public List<Document> createPacket(Packet packet)
    {
        return this.createPacket(BuilderDocumentType.PACKET, packet);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#perviewPacket(edu.ucdavis.mw.myinfovault.domain.packet.Packet)
     */
    @Override
    public List<Document> previewPacket(Packet packet)
    {
        return this.createPacket(BuilderDocumentType.PREVIEW, packet);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.packet.PacketService#isPacketSubmitted(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public boolean isPacketSubmitted(Dossier dossier)
    {
        // At any other location besides the PACKETREUEST, the packet must have been submitted
        return (workflowService.getCurrentWorkflowNode(dossier).getNodeLocation() !=  DossierLocation.PACKETREQUEST);
    }


    /**
     * Helper method to create a packet based on the BuilderDocumentType and Packet
     * @param documentType
     * @param packet
     * @return
     */
    private List<Document> createPacket(BuilderDocumentType documentType, Packet packet)
    {
        // Get the packet owner
        MivPerson person = userService.getPersonByMivId(packet.getUserId());
        if (person == null)
        {
            // no user found
            logger.error("Packet creation failed: Unable to locate user "+packet.getUserId()+" for packet "+packet.getPacketId()+"("+packet.getPacketName()+")");
            return Collections.emptyList();
        }

        // Clean up any previously created packet files
        this.deletePacketFiles(documentType, packet, person);

        return new PacketBuilder(documentType, packet, person).generateDocuments();
    }

    /**
     * Copy packet files to the final location for the dossier.
     *
     * @param dossier dossier object
     * @param Packet
     * @param Dossier
     * @return true if successful
     */
    private boolean copyPacketFiles(Dossier dossier, List<Document> documents)
    {
        Map<File, File> fileMap = new HashMap<File, File>();

        // Loop through the documents and build a map of files to copy
        for (Document document : documents)
        {
            // for each set of document files produced in each format for this document
            for (DocumentFile packetFile : document.getOutputDocuments())
            {
                // There is no actual document to copy for the combined PDF file, so skip
                if (packetFile.getDocumentFormat() == DocumentFormat.COMBINED_PDF)
                {
                    continue;
                }

                // Build input name from the packet output file
                File inputFile = packetFile.getOutputFile();

                // Build output name from the packet output file extension.
                // Files will be stored in the dossier directory by extension
                File outputFile = new File(dossier.getDossierDirectoryByDocumentFormat(packetFile.getDocumentFormat()),
                                           document.getDossierFileName()
                                         + "."
                                         + packetFile.getDocumentFormat().getFileExtension());


                fileMap.put(inputFile, outputFile);
            }
        }

        // Do the copy
        return this.copyFiles(fileMap);
    }

    /**
     * Delete any existing packet files for a packet
     *
     * @param BuilderDocumentType - the type of packet document being built
     * @param Packet - packet object
     * @param MivPerson - person object
     */
    private void deletePacketFiles(BuilderDocumentType documentType, Packet packet, MivPerson person)
    {
        // Get rid of any packet files which may be present
        logger.info("Cleaning up previous packet files for "+person.getDisplayName()+"'s packet: "+packet.getPacketId()+"("+packet.getPacketName()+")");

        // Get the packet files for this user
        PacketBuilder packetBuilder = new PacketBuilder(documentType, packet, person);

        List<Document> documents = packetBuilder.getDocumentList();

        // Loop through the documents for this packet and delete
        for (Document document : documents)
        {
            // Go through the list of document files produced in each format for this document
            for (DocumentFile packetFile : document.getOutputDocuments())
            {
                // Build the file from the packet output file
                File file = packetFile.getOutputFile();
                if (!file.exists() || (file.exists() && file.delete()))
                {
                    continue;
                }
                logger.error("Unable to delete packet file "+file.getName()+" for "+person.getDisplayName()+"'s packet: "+packet.getPacketId()+"("+packet.getPacketName()+")");
            }
        }
    }

    /**
     * Delete any packet files for this dossier
     *
     * @param Packet - packet object
     * @param Dossier - dossier object
     */
    private void deletePacketFilesFromDossier(Packet packet, Dossier dossier)
    {
        // Get rid of any packet files which may be present for this dossier
        logger.info("Deleting packet files for resubmitted dossier " + dossier.getDossierId() + " for user " + dossier.getAction().getCandidate().getUserId());

        File dossierPath = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);

        for (DossierDocumentDto dossierDocument : dossierCreatorService.getAuthorizedDocuments(MivRole.CANDIDATE))
        {
            // Delete all candidate files from the dossier directory,
            // they will be re-copied when the dossier is resubmitted.
            if (dossierDocument.isCandidateFile())
            {
                File dossierFile = new File(dossierPath, dossierDocument.getFileName() + "." + DocumentFormat.PDF.getFileExtension());
                if (dossierFile.exists())
                {
                    if (dossierFile.delete())
                    {
                        logger.info("Deleted packet file " + dossierDocument.getFileName() + " from dossier " + dossier.getDossierId());
                    }
                    else
                    {
                        logger.warn("Failed to delete packet file " + dossierDocument.getFileName() + " from dossier " + dossier.getDossierId());
                    }
                }
            }
        }
    }

    /**
     * Copy files in the input file map from source to target destination.
     *
     * @param fileMap
     *            map of files to copy where the key is the input file and the
     *            value is the output file.
     * @return boolean true if successful, otherwise false
     */
    private boolean copyFiles(Map<File, File> fileMap)
    {

        boolean success = true;

        // Loop through the map and do the copy to the archive location
        for (File inputFile : fileMap.keySet())
        {
            if (!inputFile.exists())
            {
                logger.warn("Dossier file " + inputFile.getAbsolutePath() + " missing");
                continue;
            }

            File outputFile = fileMap.get(inputFile);
            // Make sure the output path exists and create if necessary
            File outputPath = new File(outputFile.getParent());
            if (!outputPath.exists() && !outputPath.mkdirs())
            {
                logger.error("Copy of Dossier file "
                        + inputFile.getAbsolutePath()
                        + " failed. Unable to create output directory "
                        + outputFile.getAbsolutePath());
                success = false;
            }

            if (!success || !FileUtil.copyFile(inputFile, outputFile))
            {
                logger.error("Copy of Dossier file " + inputFile.getAbsolutePath() + " failed");
                success = false;
            }
        }

        return success;
    }

    /**
     * Validate the PDF files created for a packet
     * @param packetId to which the documents belong
     * @param documents
     * @return boolean - true if all files are valid, otherwise false
     */
    private boolean validatePacketFiles(long packetId, List<Document>documents)
    {
        List<DocumentFile>invalidFiles = new ArrayList<>();
        for (Document document : documents)
        {
            for (DocumentFile documentFile :  document.getOutputDocuments())
            {
                // It only takes one file to be invalid, but go through the whole list in case there is more than one invalid
                if (documentFile.getDocumentFormat() == DocumentFormat.PDF && !PDFConcatenator.validatePDFFile(documentFile.getOutputFile()))
                {
                    invalidFiles.add(documentFile);
                }
            }
        }

        // Log any and all invalid packet PDF files
        if (!invalidFiles.isEmpty())
        {
            logger.error("Invalid PDF files for packetId {}:",packetId);
            for (DocumentFile invalidFile : invalidFiles)
            {
                logger.error(">>>>> {} ({})", invalidFile.getOutputFile().getAbsolutePath(), invalidFile.getOutputFile().length());
            }
            return false;
        }

        return true;
    }
}
