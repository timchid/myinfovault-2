/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchFormAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.EditUserAuthorizer;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;


/**
 * Contains the methods used to support various actions executed in the user search web flow.
 * @author Mary Northup
 * @since MIV 3.0
 */
public class SearchFormAction extends MivFormAction
{
    protected static final Map<String, String> ALL_SCHOOLS_MAP = new HashMap<String, String>();
    static {
        ALL_SCHOOLS_MAP.put("ID", "0");
        ALL_SCHOOLS_MAP.put("schoolid", "0");
        ALL_SCHOOLS_MAP.put("description", "All");
    }

    protected static final Map<String, String> ALL_DEPARTMENTS_MAP = new HashMap<String, String>();
    static {
        ALL_DEPARTMENTS_MAP.put("ID", "0");
        ALL_DEPARTMENTS_MAP.put("departmentid", "0");
        ALL_DEPARTMENTS_MAP.put("school", "All");
        ALL_DEPARTMENTS_MAP.put("description", "");
        ALL_DEPARTMENTS_MAP.put("schoolid", "0");
    }

    //request scope results map key
    private static final String RESULTS_KEY = "results";

    //flow IDs
    protected static final String REPORT_SEARCH_FLOW = "report-search-flow";
    protected static final String MANAGEGROUPS_ASSIGNMENT_FLOW = "manageGroups-assignment-flow";
    protected static final String ASSIGNREVIEWERS_ASSIGNMENT_FLOW = "assignReviewers-assignment-flow";
    protected static final String OPENACTIONS_REPORT_SEARCH_FLOW = "openactions-report-search-flow";
    protected static final String MANAGEUSERS_REACTIVATE_FLOW = "manageUsers-reactivate-flow";
    protected static final String VIEWDOSSIER_ARCHIVE_FLOW = "viewdossierarchive-flow";

    protected  SearchStrategy searchStrategy;

    /**
     *
     * @param searchStrategy
     * @param authorizationService
     * @param userService
     */
    public SearchFormAction(SearchStrategy searchStrategy,
                            AuthorizationService authorizationService,
                            UserService userService)
    {
        super(userService, authorizationService);

        this.searchStrategy = searchStrategy;
    }


    /**
     * Method to load Role type passed by URL.
     *
     * @param context the context associated with the current web flow
     * @return Webflow event status
     */
    public Event loadRoleType(RequestContext context)
    {
        // Check role type parameter
        String roleType = getParameters(context).get("roleType");
        MivRole role = null;

        if (roleType != null) // or could catch NullPointerException below, along with IllegalArg
        {
            try {
                role = MivRole.valueOf(roleType);
            }
            catch (IllegalArgumentException e) {
                // The 'roleType' parameter was bad - we could throw an error here
                // but we'll just act like no parameter was passed. Leave 'role' as null.
            }
        }

        logger.debug("Search on roleType :: "+roleType);

        // Save to roleType in flow context
        context.getFlowScope().put("role", role);
        return success();
    }


    /**
     * Method to create the specific drop down lists (depending upon user roles)
     * and page properties for the enter search criteria page.
     *
     * @param context the context associated with the current web flow
     * @return WebFlow event status, either success or failure
     */
    public Event setupSearchForm(RequestContext context)
    {
        MutableAttributeMap<Object> requestScope = context.getRequestScope();

        // Put the general strings in scope for the page to use, based on the Flow ID.
        // Flows that mention "display" are destined for the userlist.jsp but use the
        // same set of strings as the same flow without "display" in it, so translate.
        String myStateId = getStateId(context);
        if (myStateId.indexOf("display") != -1) {
            myStateId = "display";
        }

        // Add role type to retrive the breadcrumbs by role type
        MivRole role = context.getFlowScope().get("role", MivRole.class);
        requestScope.put("user_role", role);

        // If a specific role was passed, alter the flow ID to get the correct set of strings
        if (role != null) {
            myStateId += "-" + role;
        }

        loadProperties(myStateId, context);

        // Populate the School and Department drop-down lists for searching.
        // These are based on the Role(s) of the person searching and will vary
        // from *everything* for top-level users, to listing only the department(s)
        // the user is assigned to for DEPT_* level users.
        // The word "All" is included if there is more than one available choice
        // for any user, to mean "search everything the user is allowed to access".
        Map<String, Map<String,String>> schools = MIVConfig.getConfig().getMap("schools");
        Map<String, Map<String,String>> departments = MIVConfig.getConfig().getMap("departments");

        MivPerson actor = getShadowPerson(context);

        List<Map<String, String>> displaySchools = new ArrayList<Map<String, String>>();
        List<Map<String, String>> displayDepartments = new ArrayList<Map<String, String>>();

        Map<String,List<Map<String,String>>> constants = getConstants();

        // Get the Select list entry for "All" in the department dropdown for possible modification
        Map<String, String> allDeptMap = new HashMap<String, String>(ALL_DEPARTMENTS_MAP);

        //flow ID of inner most flow
        String flowId = getFlowId(context);

        /*
         * If this is the search, report-search, manageGroups-assignment, or assignReviewers-assignment
         * flow show all schools and departments in the drop down list.
         */
        if (REPORT_SEARCH_FLOW.equals(flowId)
         || MANAGEGROUPS_ASSIGNMENT_FLOW.equals(flowId)
         || ASSIGNREVIEWERS_ASSIGNMENT_FLOW.equals(flowId))
        {
            displaySchools.addAll(constants.get("activeschools"));
            displayDepartments.addAll(constants.get("activedepartments"));
        }
        // Build the drop down list based on role for all other flows
        else
        {
            List<Map<String,String>> schoolList = null;
            List<Map<String,String>> departmentList = null;

            // viewdossierarchive needs list of all schools and departments
            if (VIEWDOSSIER_ARCHIVE_FLOW.equals(flowId))
            {
                schoolList = constants.get("schoolname");
                departmentList = constants.get("departmentname");
            }else // they need only active schools and departments
            {
                schoolList = constants.get("activeschools");
                departmentList = constants.get("activedepartments");
            }

            // Someone at the DEPT level gets all their own departments in the list,
            // and no schools. Everyone else gets all departments. At the SCHOOL level
            // they get just their own school in the school list, above that they get
            // all schools.

            switch (actor.getPrimaryRoleType())
            {
                case CANDIDATE:
                    // Candidates will get here if they are a DEAN or DEPT CHAIR
                    if (actor.hasRole(MivRole.DEAN, MivRole.DEPT_CHAIR))
                    {
                        for (AssignedRole assignedRole : actor.getAssignedRoles())
                        {
                            if (assignedRole.getRole() == MivRole.DEAN)
                            {
                                int s = assignedRole.getScope().getSchool();
                                Map<String,String> entry = schools.get(Integer.toString(s));
                                displaySchools.add(entry);
                                for (Map<String,String> deptEntry : departmentList)
                                {
                                    if (Integer.parseInt(deptEntry.get("schoolid")) == s) {
                                        if (!displayDepartments.contains(deptEntry))
                                        {
                                            displayDepartments.add(deptEntry);
                                        }
                                    }
                                }
                            }
                            else if (assignedRole.getRole() == MivRole.DEPT_CHAIR)
                            {
                                int s = assignedRole.getScope().getSchool();
                                int d = assignedRole.getScope().getDepartment();
                                Map<String,String> entry = departments.get(s + ":" + d);
                                if (!displayDepartments.contains(entry))
                                {
                                    displayDepartments.add(entry);
                                }
                            }
                        }
                    }
                    // Vice Provost can search all
                    else if (actor.hasRole(MivRole.VICE_PROVOST))
                    {
                        for (Map<String,String> schoolEntry : schoolList) {
                            displaySchools.add(schoolEntry);
                        }
                        for (Map<String,String> deptEntry : departmentList) {
                            displayDepartments.add(deptEntry);
                        }
                    }
                    else
                    {
                        // make sure the drop-down lists are empty for a pure candidate.
                        displaySchools.clear();
                        displayDepartments.clear();
                    }
                    break;

                case DEPT_ASSISTANT:
                case DEPT_STAFF:
                    // Department personnel get their specific department(s)
                    // and no schools at all.
                    for (AssignedRole a : actor.getAssignedRoles())
                    {
                        // Only consider Dept. Staff and Assistant roles; skip others
//                      if (!(a.getRole().equals(MivRole.DEPT_STAFF) || a.getRole().equals(MivRole.DEPT_ASSISTANT))) { continue; }
                        if (a.getRole() != MivRole.DEPT_STAFF && a.getRole() != MivRole.DEPT_ASSISTANT) { continue; }
                        int s = a.getScope().getSchool();
                        int d = a.getScope().getDepartment();
                        Map<String,String> entry = departments.get(s + ":" + d);
                        displayDepartments.add(entry);
                    }
                    break;

                case SCHOOL_STAFF:
                    // For clustering, School personnel get all their own schools, and all departments
                    // that are in their schools.

                    // Process each appointment for school admin clustering
                    for (AssignedRole a : actor.getAssignedRoles())
                    {
                        // Only consider School Staff roles; skip others
//                      if (!(a.getRole().equals(MivRole.SCHOOL_STAFF))) continue;
                        if (a.getRole() != MivRole.SCHOOL_STAFF) { continue; }

                        int s = a.getScope().getSchool();
                        Map<String,String> entry = schools.get(Integer.toString(s));
                        displaySchools.add(entry);

                        // TODO: For a School Admin, set the school ID in the "All" entry to match
                        // only for their own school. This restriction will be removed when School
                        // admin clustering is in affect
                        allDeptMap.put("schoolid", s+"");

                        for (Map<String,String> deptEntry : departmentList)
                        {
                            if (Integer.parseInt(deptEntry.get("schoolid")) == s) {
                                displayDepartments.add(deptEntry);
                            }
                        }
                    }
                    break;

                case SENATE_STAFF:
                    // No school or department list needed for senate users when de/re-activating each other...
                    if (MANAGEUSERS_REACTIVATE_FLOW.equals(flowId)) {
                        break;
                    }
                    // ...but fall-through and use the "all schools" lists like Sys-and-Miv admins for other flows.
                case SYS_ADMIN:
                case VICE_PROVOST_STAFF:
                    // System Admins and "MIV Admins" (VP Staff) get all schools
                    // and all departments.
                    for (Map<String,String> schoolEntry : schoolList) {
                        displaySchools.add(schoolEntry);
                    }
                    for (Map<String,String> deptEntry : departmentList) {
                        displayDepartments.add(deptEntry);
                    }
                    break;
            }
        }

        // Save the lists prior to "All" being added
        MutableAttributeMap<Object> flashScope = context.getFlashScope();
        flashScope.put("schoolList", displaySchools);
        flashScope.put("departmentList", displayDepartments);

        // If there's more than one entry in either drop-down list
        // add an "All" entry to the list(s)
        if (displaySchools.size() > 1) {
            displaySchools.add(0, ALL_SCHOOLS_MAP);
        }

        if (displayDepartments.size() > 1) {
            displayDepartments.add(0, allDeptMap);
        }

        requestScope.put("schools", displaySchools);
        requestScope.put("departments", displayDepartments);

        return success();
    }


    /**
     * Ensure the search parameters that have been entered are valid for the user making the request.
     * If there has been data tampering, the request may now contain unauthorized requests.
     * Clean up the SearchCriteria object obtained from the form so it contains data appropriate
     * for the actual request that has been made (i.e. for the button that was pressed)
     * Search by Name or Last Initial are always allowed. Search by School or Department is allowed
     * based on Role vs. Scope rules.
     * @param context the context associated with the current web flow.
     * @return <code>success</code> if the search is allowed, <code>error</code> otherwise.
     */
    public Event searchValidator(RequestContext context)
    {
        MivPerson mivPerson = getShadowPerson(context);

        SearchCriteria criteria = (SearchCriteria) getFormObject(context);

        // Add role type into SearchCriteria object
        criteria.setRole(context.getFlowScope().get("role", MivRole.class));

        AttributeSet scope = new AttributeSet();

        // The school and department to search within: (-1) means not set, 0 means All, otherwise the school/dept number.
        int searchSchool = -1;
        int searchDept = -1;

        //get search method from last event and set in search criteria form
        criteria.setMethod(context.getCurrentEvent());

        // Prevent or clean-up conflicting information
        switch(criteria.getMethod())
        {
            // If search by name was chosen clear out the last-name field so it's not used, and allow the search
            case SEARCHNAME:
                criteria.setLname("");
                criteria.setSchool("-1");
                criteria.setDepartment("-1:-1");

                return success();
             // or if search by last name chosen clear out the name field, and allow the search
            case SEARCHLNAME:
                criteria.setInputName("");
                criteria.setSchool("-1");
                criteria.setDepartment("-1:-1");

                return success();
            case SEARCHDEPT:
                criteria.setLname("");
                criteria.setInputName("");
                criteria.setSchool("-1");

                searchSchool = criteria.getDeptSchoolId();
                searchDept = criteria.getDepartmentId();
                if (searchSchool == 0) searchSchool = -1;
                if (searchDept == 0) searchDept = -1;
                scope.put(AuthorizationService.Qualifier.SCHOOL, Integer.toString(searchSchool));
                scope.put(AuthorizationService.Qualifier.DEPARTMENT, Integer.toString(searchDept));

                break;
            case SEARCHSCHOOL:
                criteria.setLname("");
                criteria.setInputName("");
                criteria.setDepartment("-1:-1");

                searchSchool = criteria.getSchoolId();
                if (searchSchool == 0) searchSchool = -1;
                scope.put(AuthorizationService.Qualifier.SCHOOL, Integer.toString(searchSchool));
                scope.put(AuthorizationService.Qualifier.DEPARTMENT, Integer.toString(Scope.ANY));// always ANY when searching by school

                break;
        }

        /*
         * FIXME: remove the code and transfer into the flow
         * No Need to call isSearchAuthorized() for reports
         */
        if (!getFlowId(context).equals(REPORT_SEARCH_FLOW) && !isSearchAuthorized(mivPerson, null, scope))
        {
            logger.warn("Possible Hacking Attempt! User trying to search outside of access level:\n" +
                        "   Current Logged-in user: " + mivPerson.getDisplayName() +
                        "(" + mivPerson.getUserId() + "),  Role: " + mivPerson.getPrimaryRoleType().getDescription() + "\n" +
                        "   search School: " + searchSchool + "  search Dept: " + searchDept + "\n");

            return error();
        }

        return success();
    }


    /**
     * Determine if the user is authorized for this particular search to proceed.
     * Subclasses may override this to impose different conditions or base the search on a more specific Permission.
     * @param person the logged in or proxied MivPerson that is performing the search
     * @param details detail attributes of the permission to check
     * @param scope the school/department scope of the search
     * @return true if the search should be allowed to proceed, false otherwise.
     */
    boolean isSearchAuthorized(MivPerson person, AttributeSet details, AttributeSet scope)
    {
        return authorizationService.isAuthorized(person, Permission.SEARCH, details, scope);
    }


    /**
     * Determine if the user is authorized to use the search page.
     *
     * @param context the context associated with the current web flow
     * @return WebFlow event status, either success or failure
     */
    public Event checkAuthorization(RequestContext context)
    {
        logger.trace("Entering checkAuthorization");

        MivPerson actor = getShadowPerson(context);

        if (authorizationService.hasPermission(actor, Permission.SEARCH, null))
        {
            return success();
        }

        System.out.println("we have unauthorized access");
        logger.warn("User \"" + actor.toString() +  "\" attempted to access MIV User Report page");
        return error();
    }


    /**
     * Sets properties for use by the userlist.jsp so that the breadcrumbs and titles display the appropriate headings.
     * @param context - the context associated with the current web flow.
     * @return Event - either success or failure, used by web flow to control the flow execution.
     * @throws Exception - not sure about this, may force the "catch-all" exception handler".
     */
    public Event pageSetup(RequestContext context) throws Exception
    {
        logger.trace("Entering pageSetup");

        MutableAttributeMap<Object> requestScope = context.getRequestScope();

        String myStateId = getStateId(context);

        // If the state-id contains "display" then this is a state that will end up viewing the userlist.jsp
        // the config.properties are setup as "xxx-flow-display" for the userlist.jsp so we need to look for that.
        // examples of these states are displayNameResults, displaySchoolResults, etc

        if (myStateId.indexOf("display") != -1)
        {
//            myStateId = myStateId.substring(myStateId.indexOf("display"), 7);
            myStateId = "display";
        }

        // Add role type to retrieve the bread crumbs by role type
        MivRole role = context.getFlowScope().get("role", MivRole.class);
        requestScope.put("user_role", role);
        // If a specific role was passed, alter the flow ID to get the correct set of strings
        if (role != null) {
            myStateId += "-" + role;
        }

        loadProperties(myStateId, context);

        if (SearchCriteria.class == getFormObjectClass())
        {
            requestScope.put("searchCriteriaText",
                             prepareSearchCriteriaText((SearchCriteria) getFormObject(context)));
        }

        return success();
    }

    /**
     * Prepare Search Criteria Text
     * @param criteria
     * @return
     */
    private String prepareSearchCriteriaText(SearchCriteria criteria){
        String searchCriteriaText = "";

        Map<String, Map<String,String>> schools = MIVConfig.getConfig().getMap("schools");
        Map<String, Map<String,String>> departments = MIVConfig.getConfig().getMap("departments");

        if (criteria.getInputName() != null && criteria.getInputName().trim().length() > 0)
        {
            searchCriteriaText = "name \"" + criteria.getInputName() + "\"";
        }
        else if (criteria.getLname() != null && criteria.getLname().trim().length() > 0)
        {
            searchCriteriaText = "last initial \"" + criteria.getLname() + "\"";
        }
        else if (criteria.getSchoolId() > -1)
        {
            if (criteria.getSchoolId() == 0)
            {
                searchCriteriaText = "All";
            }
            else
            {
                searchCriteriaText = (schools.get(String.valueOf(criteria.getSchoolId())).get("description"));
            }

            searchCriteriaText = "\""+ searchCriteriaText + "\"";
        }
        else if (criteria.getDepartmentId() > -1 && criteria.getDeptSchoolId() > -1)
        {
            if (criteria.getDepartmentId() == 0 && criteria.getDeptSchoolId() == 0)
            {
                searchCriteriaText = "All";
            }
            else if (criteria.getDepartmentId() == 0)
            {
                searchCriteriaText = (schools.get(String.valueOf(criteria.getDeptSchoolId())).get("description"));
            }
            else
            {
                String deptSchoolId = String.valueOf(criteria.getDeptSchoolId()) + ":" + String.valueOf(criteria.getDepartmentId());
                searchCriteriaText = (departments.get(deptSchoolId)).get("school");

                if (departments.get(deptSchoolId).get("description") != null
                        && departments.get(deptSchoolId).get("description").toString().trim().length() > 0)
                {
                    searchCriteriaText += " - " + (departments.get(deptSchoolId)).get("description");
                }

            }
            searchCriteriaText = "\""+ searchCriteriaText + "\"";
        }
        return searchCriteriaText;
    }


    /**
     * Short-cut method to retrieve users via the search
     * strategy associated with the search method.
     *
     * @param user MIV logged-in user
     * @param criteria Search criteria
     * @return MIV persons via the search strategy
     */
    public List<MivPerson> getUsers(MIVUser user, SearchCriteria criteria)
    {
        switch (criteria.getMethod())
        {
            case SEARCHNAME:
            case SEARCHLNAME:
                return getUsersByName(user, criteria);
            case SEARCHDEPT:
                return getUsersByDepartment(user, criteria);
            case SEARCHSCHOOL:
                return getUsersBySchool(user, criteria);
            default:
                return Collections.emptyList();
        }
    }


    /**
     * Short-cut method to retrieve users via the search strategy by name.
     *
     * @param user MIV logged-in user
     * @param criteria Search criteria containing the search name
     * @return List of MIV persons via the search strategy
     */
    public List<MivPerson> getUsersByName(MIVUser user, SearchCriteria criteria)
    {
        return this.searchStrategy.getUsersByName(user, criteria);
    }


    /**
     * Short-cut method to retrieve users via the search strategy by school.
     *
     * @param user MIV logged-in user
     * @param criteria Search criteria
     * @return List of MIV persons via the search strategy
     */
    public List<MivPerson> getUsersBySchool(MIVUser user, SearchCriteria criteria)
    {
        return this.searchStrategy.getUsersBySchool(user, criteria);
    }


    /**
     * Short-cut method to retrieve users via the search strategy by department.
     *
     * @param user MIV logged-in user
     * @param criteria Search criteria
     * @return List of MIV persons via the search strategy
     */
    public List<MivPerson> getUsersByDepartment(MIVUser user, SearchCriteria criteria)
    {
        return this.searchStrategy.getUsersByDepartment(user, criteria);
    }


    /**
     * Reduce the provided list by removing people with "higher" access than the given role.
     *
     * Notice this uses "getAssignableRoles()" which really only applies to editing a user,
     * but it's the same set as who you can switch to. This is the generic Search Form, so
     * there can be cases where "assignable roles" is not the right thing.  In particular,
     * the MIV User Report really shouldn't be reducing the original list.
     *
     * @param role an MivRole used to limit the returned List
     * @param people the original list of people to be reduced
     * @return a new List with only people this role is allowed to access
     */
    protected List<MivPerson> scopeByRole(MivRole role, List<MivPerson> people)
    {
        List<MivRole> allowedRoles = EditUserAuthorizer.getAssignableRoles(role); // role.getAssignableRoles();
        List<MivPerson> scopedPeople = new ArrayList<MivPerson>(people.size());
        for (MivPerson p : people)
        {
            if (p.hasRole(allowedRoles))
            {
                scopedPeople.add(p);
            }
        }

        return scopedPeople;
    }


    /**
     * Maps the results object to the "results" key in the request scope.
     *
     * @param context Webflow request context
     * @param results object to map to key "results" in the request scope map
     */
    protected <T> void setResults(RequestContext context, T results)
    {
        context.getRequestScope().put(RESULTS_KEY, results);
    }
}
