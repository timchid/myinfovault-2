/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ManageOpenActionSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * ManageOpenActionSearchStrategy - The searching strategy to be used when searching for dossiers or users
 * in ManageOpenActions.
 *
 * @author rhendric
 *
 */
public class ManageOpenActionSearchStrategy extends DefaultSearchStrategy
{
//    protected PathConstructor pathConstructor = new PathConstructor(); // Nothing was using this.

    /**
     * The Manage Open Actions name specific search filtering follows.
     * 
     * @param criteria main search form containing the search "criteria" requested
     * @param user MIVUser requesting the search
     * @return List of MivActionList objects
     */
    @Override
    public List<MivActionList> dossierSearch(SearchCriteria criteria, MIVUser user, List<Dossier>dossiers) throws WorkflowException
    {
        final String tname = criteria.getInputName().toLowerCase();
        final String linit = criteria.getLname().toLowerCase();
        final int realUserId = user.getUserId();
        final int schoolId = criteria.getSchoolId();
        final int deptId = criteria.getDepartmentId();
        List<MivActionList> list = null;

        final MivPerson mivPerson = user.getTargetUserInfo().getPerson();

//        boolean showAll = mivPerson.hasRole(MivRole.SYS_ADMIN);
        boolean showAll = false;

        int[] excludeList = { realUserId, mivPerson.getUserId() };
        if (showAll || mivPerson.hasRole(MivRole.VICE_PROVOST_STAFF, MivRole.SENATE_STAFF))
        {
            excludeList = null;
        }

        // Associate people with the dossiers
        List<MivActionList> people = findPeople(dossiers, mivPerson, showAll, schoolId, deptId, excludeList);

        if (StringUtils.hasText(tname))         // the name was entered so search for that
        {
            list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                @Override
                public boolean include(MivActionList item)
                {
                    return matchMivActionListUserName(item, tname);
                }
            }, people);

            List<MivActionList> exactMatch = new ArrayList<MivActionList>();
            for (MivActionList oneDossier : list)
            {
                if (matchMivActionListUserName(oneDossier,tname))
                {
                    if ( !exactMatch.contains(oneDossier) ) {
                        exactMatch.add(oneDossier);
                    }
                }
            }
            if (exactMatch.size() > 0)
            {
                return exactMatch;
            }
        }
        else  // search for last name
        {
            list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                @Override
                public boolean include(MivActionList item)
                {
                    return "all".equals(linit) || item.getSurname().toLowerCase().indexOf(linit) == 0;
                }
            }, people);
        }

        return list;
    }


    /**
     * getDossiersByDepartment - The Manage Open Actions search by department filtering follows
     * @param criteria - SearchCriteria : the main search form containing the search "criteria" requested
     * @param user - MIVUser requesting the search
     * @return List of MivActionList objects
     */
    @Override
    public List<MivActionList> getDossiersByDepartment(SearchCriteria criteria, MIVUser user)
    {
        final int schoolId = criteria.getDeptSchoolId();
        final int deptId = criteria.getDepartmentId();
        final int realUserId = user.getUserId();
        final MivPerson mivPerson = user.getTargetUserInfo().getPerson();
//        boolean showAll = mivPerson.hasRole(MivRole.SYS_ADMIN);
        boolean showAll = false;

// Would this work? 'showAll' passed to findPeople isn't the same as when tested for the exludeList below.
//      final boolean showAll = mivPerson.hasRole(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF);

        List<Dossier> dossiers = new ArrayList<Dossier>();
        try
        {
            dossiers = dossierService.getDossiersBySchoolAndDepartment(mivPerson, schoolId, deptId);
        }
        catch (WorkflowException e)
        {
            logger.error("getDossiers: got a WorkflowException doing getDossiersBySchoolDepartmentAndLocation", e);
        }

        int[] excludeList = { realUserId, mivPerson.getUserId() };
        if (showAll || mivPerson.hasRole(MivRole.VICE_PROVOST, MivRole.VICE_PROVOST_STAFF))
        {
            excludeList = null;
        }

        List<MivActionList> people = findPeople(dossiers, mivPerson, showAll, schoolId, deptId, excludeList);
        if (schoolId == 0 && deptId == 0) {
            return people;
        }

        // filter by the specified department and school
        return applyFilter(new SearchFilterAdapter<MivActionList>() {
            @Override
            public boolean include(MivActionList item)
            {
                return ((item.getSchoolId() == schoolId) &&
                        (deptId == 0 || item.getDepartmentId() == deptId));
            }
        }, people);
    }


    /**
     * getDossiersBySchool - Manage Open Actions search by school filtering follows
     * @param criteria - SearchCriteria : the main search form containing the search "criteria" requested
     * @param user - MIVUser requesting the search
     * @return List of MivActionList objects
     */
    @Override
    public List<MivActionList> getDossiersBySchool(SearchCriteria criteria, MIVUser user)
    {
        final int schoolId = criteria.getSchoolId();
        final int realUserId = user.getUserId();
        final MivPerson mivPerson = user.getTargetUserInfo().getPerson();
//        boolean showAll = mivPerson.hasRole(MivRole.SYS_ADMIN);
        boolean showAll = false;

        // Would this work? 'showAll' passed to findPeople isn't the same as when tested for the exludeList below.
//      final boolean showAll = mivPerson.hasRole(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF);

        List<Dossier> dossiers = new ArrayList<Dossier>();
        try
        {
            // Get the dossiers and qualify for the input person
            dossiers = dossierService.getDossiersBySchoolAndDepartment(mivPerson, schoolId, 0);
        }
        catch (WorkflowException e)
        {
            logger.error("getDossiers: got a WorkflowException doing getDossiersBySchoolDepartmentAndLocation", e);
        }


        int[] excludeList = { mivPerson.getUserId(), realUserId };
        if (showAll || mivPerson.hasRole(MivRole.VICE_PROVOST, MivRole.VICE_PROVOST_STAFF))
        {
            excludeList = null;
        }

        List<MivActionList> people = findPeople(dossiers, mivPerson, showAll, schoolId, 0, excludeList);

        return people;
    }

}
