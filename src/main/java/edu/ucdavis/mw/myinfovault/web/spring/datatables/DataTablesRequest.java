/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DataTablesRequest.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.datatables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Stores a DataTables request bound from JSON.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.5
 * @see http://www.datatables.net/manual/server-side
 */
public class DataTablesRequest
{
    private int draw = 0;
    private int start = 0;
    private int length = 0;
    private Search search = new Search();
    private List<Order> order = new ArrayList<>();
    private List<Column> columns = new ArrayList<>();

    /**
     * @return Draw counter. This is used by DataTables to ensure that the Ajax
     * returns from server-side processing requests are drawn in sequence by
     * DataTables (Ajax requests are asynchronous and thus can return out of
     * sequence). This is used as part of the draw return parameter (see below).
     */
    public int getDraw()
    {
        return draw;
    }

    /**
     * @return Paging first record indicator. This is the start point in the
     * current data set (0 index based - i.e. 0 is the first record).
     */
    public int getStart()
    {
        return start;
    }

    /**
     * @return Number of records that the table can display in the current
     * draw. It is expected that the number of records returned will be equal
     * to this number, unless the server has fewer records to return. Note that
     * this can be -1 to indicate that all records should be returned (although
     * that negates any benefits of server-side processing!).
     */
    public int getLength()
    {
        return length;
    }

    /**
     * @return global search request
     */
    public Search getSearch()
    {
        return search;
    }

    /**
     * @return global order request
     */
    public List<Order> getOrder()
    {
        return order;
    }

    /**
     * @return columns requested
     */
    public List<Column> getColumns()
    {
        return columns;
    }

    /**
     * @return If this request is searchable. That is, at least one column is
     * {@link Column#isSearchable()}.
     */
    public boolean isSearchable()
    {
        for (Column column : columns)
        {
            if (column.isSearchable())
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Search request.
     */
    public class Search
    {
        private String value = "";
        private boolean regex = false;
        private Pattern pattern = null;

        /**
         * @return Global search value. To be applied to all columns which have
         * searchable as <code>true</code>.
         */
        public String getValue()
        {
            return value;
        }

        /**
         * @return <code>true</code> if the global filter should be treated as a
         * regular expression for advanced searching, <code>false</code> otherwise.
         * Note that normally server-side processing scripts will not perform
         * regular expression searching for performance reasons on large data sets,
         * but it is technically possible and at the discretion of your script.
         */
        public boolean isRegex() {
            return regex;
        }

        /**
         * @return compiled regular expression for this search value
         */
        public Pattern getPattern()
        {
            if (pattern == null)
            {
                pattern = regex
                        ? Pattern.compile(value)
                        : Pattern.compile(Pattern.quote(value), Pattern.CASE_INSENSITIVE);
            }

            return pattern;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            return new StringBuilder().append("value=").append(this.value)
                                      .append(", ")
                                      .append("regex=").append(this.regex)
                                      .toString();
        }
    }

    /**
     * Ordering request.
     */
    public class Order
    {
        private int column = -1;
        private Direction dir = Direction.ASC;

        /**
         * @return Column to which ordering should be applied. This is an index
         * reference to the columns array of information that is also submitted
         * to the server.
         */
        public int getColumn() {
            return column;
        }

        /**
         * @return Ordering direction for this column. It will be asc or desc to
         * indicate ascending ordering or descending ordering, respectively.
         */
        public Direction getDirection() {
            return dir;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            return new StringBuilder().append("column=").append(this.column)
                                      .append(", ")
                                      .append("direction=").append(this.dir)
                                      .toString();
        }
    }

    /**
     * Ordering direction.
     */
    public enum Direction {
        @SerializedName("asc")
        ASC,

        @SerializedName("desc")
        DESC;
    }

    /**
     * Requested column.
     */
    public class Column
    {
        /**
         * Regular expression matching JavaScript dotted notation delimiters for {@link #getData()}.
         */
        private static final String SPLIT_RE = "\\.";

        /**
         * Regular expression matching Data Tables array or index references in JavaScript dotted notation.
         *
         * @see <a href="https://www.debuggex.com/r/amNrIZE_vsGEZ-uE">Pattern at Debuggex</a>
         */
        private static final String REPLACE_RE = "\\[[^\\]\\[]*\\]|\\.\\d";

        /**
         * Rendering key to which the default data source reference is mapped.
         */
        private static final String DEFACTO = "_";

        /**
         * Rendering key to which the filter data source reference is mapped.
         */
        private static final String FILTER = "filter";

        /**
         * Rendering key to which the sort data source reference is mapped.
         */
        private static final String SORT = "sort";

        private String data = "";
        private Map<String, String> render = new HashMap<>();
        private String name = "";
        private boolean searchable = true;
        private boolean orderable = true;
        private Search search = new Search();
        private Map<String, List<String>> tokens = null;

        /**
         * @return Column's data sources, as defined by columns.dataDT and columns.renderDT,
         * stripped of array indices and tokenized by "."
         */
        public Set<Deque<String>> getData()
        {
            // return a set of token list copies
            Set<Deque<String>> copy = new HashSet<>();

            for (List<String> renderTokens : getTokens().values())
            {
                copy.add(new LinkedList<>(renderTokens));
            }

            return copy;
        }

        /**
         * @return Column's default rendering data source, stripped of array indices and tokenized by "."
         * @see <a href="http://datatables.net/reference/option/columns.data#string">String type for Data Tables columns.render initialization option</a>
         * @see <a href="http://datatables.net/reference/option/columns.render#object">Object type for Data Tables columns.render initialization option</a>
         */
        public Deque<String> getDefault()
        {
            return new LinkedList<>(getTokens().get(DEFACTO));
        }

        /**
         * @return Column's data source for filtering, stripped of array indices and tokenized by "."
         * @see <a href="http://datatables.net/reference/option/columns.render#object">Object type for Data Tables columns.render initialization option</a>
         */
        public Deque<String> getFilter()
        {
            List<String> filterTokens = getTokens().get(FILTER);

            return filterTokens == null ? getDefault() : new LinkedList<>(filterTokens);
        }

        /**
         * @return Column's data source for sorting, stripped of array indices and tokenized by "."
         * @see <a href="http://datatables.net/reference/option/columns.render#object">Object type for Data Tables columns.render initialization option</a>
         */
        public Deque<String> getSort()
        {
            List<String> sortTokens = getTokens().get(SORT);

            return sortTokens == null ? getDefault() : new LinkedList<>(sortTokens);
        }

        /**
         * Generate and return data source reference tokens.
         *
         * @return Map of tokens mapped to data source type (display, sort, etc)
         */
        private Map<String, List<String>> getTokens()
        {
            // build token map if not initialized
            if (tokens == null)
            {
                tokens = new HashMap<>();

                // base tokens for all data references
                List<String> baseTokens = tokenize(data);

                // base tokens are default if no rendering specified
                if (render == null || render.isEmpty())
                {
                    tokens.put(DEFACTO, baseTokens);
                }
                else
                {
                    // tokenize each render entry and add to base tokens
                    for (Entry<String, String> entry : render.entrySet())
                    {
                        List<String> renderTokens = new ArrayList<>(baseTokens);

                        renderTokens.addAll(tokenize(entry.getValue()));

                        tokens.put(entry.getKey(), renderTokens);
                    }
                }
            }

            return tokens;
        }

        /**
         * Strip array indices (used by Data Tables, but not necessary for evaluating beans) and tokenize on "."
         *
         * @param reference data reference to tokenize
         * @return data reference tokens or empty list for blank reference
         */
        private List<String> tokenize(String reference)
        {
            return StringUtils.isBlank(reference)
                 ? Collections.<String>emptyList()
                 : Arrays.asList(
                       reference.replaceAll(REPLACE_RE, StringUtils.EMPTY)
                                .split(SPLIT_RE));
        }

        /**
         * @return Column's name, as defined by columns.nameDT.
         */
        public String getName() {
            return name;
        }

        /**
         * @return Flag to indicate if this column is searchable (<code>true</code>)
         * or not (<code>false</code>). This is controlled by columns.searchableDT.
         */
        public boolean isSearchable() {
            return searchable;
        }

        /**
         * @return Flag to indicate if this column is orderable (<code>true</code>)
         * or not (<code>false</code>). This is controlled by columns.orderableDT.
         */
        public boolean isOrderable() {
            return orderable;
        }

        /**
         * @return search request for this column
         */
        public Search getSearch() {
            return search;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            return new StringBuilder().append("\n\tdata\t\t").append(this.data)
                                      .append("\n\tname\t").append(this.name)
                                      .append("\n\tsearchable\t").append(this.searchable)
                                      .append("\n\torderable\t").append(this.orderable)
                                      .append("\n\tsearch\t\t").append(this.search)
                                      .toString();
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder().append("\n*** DataTables Request ***")
                                                   .append("\ndraw\t").append(this.draw)
                                                   .append("\nstart\t").append(this.start)
                                                   .append("\nlength\t").append(this.length)
                                                   .append("\nsearch\t").append(this.search)
                                                   .append("\norder");

        for (Order order : this.order)
        {
            builder.append("\n\t").append(order);
        }

        builder.append("\ncolumns");

        int index = 0;
        for (Column column : this.columns)
        {
            builder.append("\n\t<column ").append(index++).append(">")
                   .append(column)
                   .append("\n");
        }

        return builder.toString();
    }
}
