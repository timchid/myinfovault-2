package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.List;

/**
 * Command class for Design Biosketch
 * @author svidya
 *
 */
public class BiosketchDesignCommand extends BaseCommand
{
    private String biosketchName;
    private List<DesignRecord> recordList = null;    

    public List<DesignRecord> getRecordList()
    {
        return recordList;
    }

    public void setRecordList(List<DesignRecord> recordList)
    {
        this.recordList = recordList;
    }

    public String getBiosketchName()
    {
        return biosketchName;
    }

    public void setBiosketchName(String biosketchName)
    {
        this.biosketchName = biosketchName;
    }
    
    
}
