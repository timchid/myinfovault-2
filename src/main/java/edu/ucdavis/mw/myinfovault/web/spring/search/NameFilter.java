/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: NameFilter.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Filter for a name search.
 * 
 * @author Mary Northup
 * @since MIV 3.0
 */
public class NameFilter extends SearchFilterAdapter<MivPerson>
{
    private final String name;

    /**
     * @param name name term on which to filter (case insensitive)
     */
    public NameFilter(String name)
    {
        this.name = name.toLowerCase();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(MivPerson item)
    {
        return (((item.getGivenName().toLowerCase().indexOf(name)) != -1)
                || (item.getSurname().toLowerCase().indexOf(name) != -1));
    }
}
