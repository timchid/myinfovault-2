/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DataTablesResponse.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.datatables;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Primitives;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.web.spring.datatables.DataTablesRequest.Column;
import edu.ucdavis.mw.myinfovault.web.spring.datatables.DataTablesRequest.Direction;
import edu.ucdavis.mw.myinfovault.web.spring.datatables.DataTablesRequest.Order;

/**
 * Data tables response.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.5
 * @param <T> record type for response data
 */
abstract class DataTablesResponse<T>
{
    private static Logger log = LoggerFactory.getLogger(DataTablesResponse.class);

    /*
     * Cache configuration
     */
    private static final int CACHE_SIZE = 20;
    private static final int SPARES = 2;
    private static final float LOAD_FACTOR = 0.9F;
    private static final int INITIAL_SIZE = (Math.round(CACHE_SIZE / LOAD_FACTOR) + 1) + SPARES;

    /**
     * Response data cache.
     */
    private static final Map<String, List<Map<String, Object>>> dataCache = Collections.synchronizedMap(
        new LinkedHashMap<String, List<Map<String, Object>>>(INITIAL_SIZE, LOAD_FACTOR, true)
        {
            private static final long serialVersionUID = 201311051448L;

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, List<Map<String, Object>>> eldest) {
                return size() > CACHE_SIZE;
            }
        }
    );

    /**
     * Compares Objects as their runtime class or as Strings if they do not implement {@link Comparable}.
     */
    private static Comparator<Object> naturalComparator = new Comparator<Object>()
    {
        @SuppressWarnings({ "rawtypes", "unchecked" })
        @Override
        public int compare(Object o1, Object o2)
        {
            if (o1 == null) o1 = StringUtils.EMPTY;
            if (o2 == null) o2 = StringUtils.EMPTY;

            try
            {
                return ((Comparable) o1).compareTo(o1.getClass().cast(o2));
            }
            catch (ClassCastException e)
            {
                return StringUtil.nullComparator.compare(o1.toString(), o2.toString());
            }
        }
    };

    private List<Map<String, Object>> data;
    private int recordsTotal;
    private int recordsFiltered;
    private String error = null;
    private final Map<T, Set<String>> recordClasses = new HashMap<>();
    private final Map<T, Map<String, String>> recordData = new HashMap<>();

    /**
     * Initialize this data tables response.
     *
     * @param request data tables request
     */
    protected DataTablesResponse(final DataTablesRequest request)
    {
        List<Map<String, Object>> cached = null;

        /*
         * Attempt to retrieve response data from cache.
         */
        if (getKey() != null)
        {
            cached = dataCache.get(getKey());
        }

        /*
         * Otherwise, build data from a new batch of records.
         */
        if (cached == null)
        {
            // create new data list to cache
            cached = new ArrayList<>();

            try
            {
                int index = 0;
                for (T record : getRecords())
                {
                    Map<String, Object> map = new HashMap<>();

                    for (Column column : request.getColumns())
                    {
                        for (Deque<String> data : column.getData())
                        {
                            fill(map, record, data);
                        }
                    }

                    map.put("DT_RowId", index++);
                    map.put("DT_RowClass", StringUtils.join(recordClasses.get(record), StringUtil.SPACE));
                    map.put("DT_RowData", recordData.get(record));

                    cached.add(map);
                }

                // no error building data, add it to the cache
                if (getKey() != null)
                {
                    dataCache.put(getKey(), cached);
                }
            }
            /*
             * Deal with any unchecked/uncaught exceptions from #getRecords().
             *
             * TODO: When we upgrade to Spring MVC 3+, do not catch runtime
             * exceptions, instead, let them propagate up to the controller and
             * define an exception handlers that return proper status codes.
             */
            catch (RuntimeException e)
            {
                this.error = e.getMessage();

                log.error("Unable to get raw records for " + request, e);
            }
        }

        // get copy of cached data to filter, sort, and page.
        ArrayList<Map<String, Object>> data = new ArrayList<>(cached);

        recordsTotal = data.size();

        /*
         * Filter data by global and column specific search terms if searchable.
         */
        if (request.isSearchable())
        {
            data = (new SearchFilterAdapter<Map<String, Object>>() {
                @Override
                public boolean include(Map<String, Object> item)
                {
                    boolean found = false;// if global search term has been found

                    for (Column column : request.getColumns())
                    {
                        if (column.isSearchable())
                        {
                            Object property = DataTablesResponse.getProperty(item, column.getFilter());

                            // consider missing property an empty String
                            String cell = property != null ? property.toString() : StringUtils.EMPTY;

                            // if column specific search term NOT found in cell
                            if (!column.getSearch().getPattern().matcher(cell).find())
                            {
                                // immediately exclude this item
                                return false;
                            }

                            // if global search term not yet found
                            if (!found)
                            {
                                // tentatively include this item
                                found = request.getSearch().getPattern().matcher(cell).find();
                            }
                        }
                    }

                    // include if global search term was found
                    return found;
                }
            }).apply(data);
        }

        recordsFiltered = data.size();

        /*
         * Order data.
         */
        Collections.sort(data, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> record1, Map<String, Object> record2) {
                // loop over the order list for this request
                for (Order order : request.getOrder())
                {
                    // column to order
                    Column column = request.getColumns().get(order.getColumn());

                    // if column is allowed to be ordered
                    if (column.isOrderable())
                    {
                        Object cell1 = DataTablesResponse.getProperty(record1, column.getSort());
                        Object cell2 = DataTablesResponse.getProperty(record2, column.getSort());

                        // compare the records by this column
                        int comparison = naturalComparator.compare(cell1, cell2);

                        // if cells do not compare equal
                        if (comparison != 0)
                        {
                            // reverse comparison for a descending order
                            return order.getDirection() == Direction.DESC ? comparison * -1 : comparison;
                        }
                    }
                }

                // must be equal
                return 0;
            }
        });

        /*
         * Page data.
         */
        this.data = request.getLength() > 0
                  ? data.subList(request.getStart(), Math.min(recordsFiltered, request.getStart() + request.getLength()))
                  : data;
    }

    /**
     * @return new batch of records for response data.
     */
    protected abstract List<T> getRecords();

    /**
     * @return key to which {@link #getRecords() requested records} will map in the cache.
     */
    protected abstract String getKey();

    /**
     * Add the class name for the Data Tables row corresponding to the given record.
     *
     * @param record record from {@link #getRecords()}
     * @param classNames class name(s) for the given record (mapped to "DT_RowClass" in the response)
     */
    protected void addClass(T record, String...classNames)
    {
        Set<String> classes = recordClasses.get(record);

        // create class name set for this record if DNE
        if (classes == null)
        {
            classes = new HashSet<>();

            recordClasses.put(record, classes);
        }

        classes.addAll(Arrays.asList(classNames));
    }

    /**
     * Associate the name-value pair with the given record. This data is
     * included in the response via the "DT_RowData" attribute and made
     * available to each respective record with the jQuery data object).
     *
     * @param record record from {@link #getRecords()}
     * @param name data name
     * @param value data value
     */
    protected void addData(T record, String name, String value)
    {
        Map<String, String> data = recordData.get(record);

        // create class name set for this record if DNE
        if (data == null)
        {
            data = new HashMap<>();

            recordData.put(record, data);
        }

        data.put(name, value);
    }

    /**
     * @return Total records, before filtering (i.e. the total number of records in the database)
     */
    public int getRecordsTotal()
    {
        return recordsTotal;
    }

    /**
     * @return Total records, after filtering (i.e. the total number of records after filtering
     * has been applied - not just the number of records being returned for this page of data)
     */
    public int getRecordsFiltered()
    {
        return recordsFiltered;
    }

    /**
     * @return The data to be displayed in the table. This is an array of data source objects,
     * one for each row, which will be used by DataTables. Note that this parameter's name can
     * be changed using the ajaxDT option's dataSrc property.
     */
    public List<Map<String, Object>> getData()
    {
        return data;
    }

    /**
     * @return Optional: If an error occurs during the running of the server-side processing
     * script, you can inform the user of this error by passing back the error message to be
     * displayed using this parameter. Do not include if there is no error.
     */
    public String getError()
    {
        return error;
    }

    /**
     * Evaluates the bean for the given properties and nests them in a map or list (for iterable or array beans).
     *
     * @param out output object to fill with bean properties
     * @param bean source from which properties are evaluated
     * @param properties property references on given bean
     * @return output object filled with bean properties
     * @see {@link #fillMap(Map, Object, Deque)}
     * @see {@link #fillList(List, Iterable, Deque)}
     * @see {@link #fillObject(Map, Object)}
     */
    private static Object fill(Object out, Object bean, Deque<String> properties)
    {
        // fill list for iterable or array bean
        if (bean instanceof Iterable || bean instanceof Object[])
        {
            return fillList(out == null ? new ArrayList<>() : out, bean, properties);
        }
        // if a String or primitive wrapper, return the bean itself as output
        else if (bean instanceof String || Primitives.isWrapperType(bean.getClass()))
        {
            return bean;
        }
        // fill map with remaining properties
        else if (!properties.isEmpty())
        {
            return fillMap(out == null ? new HashMap<>() : out, bean, properties);
        }

        // otherwise, object fill is finished
        return out;
    }

    /**
     * Evaluates the bean for the given properties and nests them in the given map.
     *
     * E.g. an {@link MivPerson} bean for the properties [ primaryAppointment, schoolName ]
     * would populate the given map with (using JSON notation):
     * <pre>
     * map = {
     *     primaryAppointment : {
     *         schoolName : "School of Medicine"
     *     }
     * }
     * </pre>
     *
     * A subsequent fill of the above map and bean with the properties
     * [ primaryAppointment, departmentName ] yields:
     * <pre>
     * map = {
     *     primaryAppointment : {
     *         schoolName : "School of Medicine",
     *         departmentName : "Surgery"
     *     }
     * }
     * </pre>
     *
     * @param out output map to fill with bean properties
     * @param bean source from which properties are evaluated
     * @param properties property references on given bean
     * @return output map filled with bean properties
     */
    private static Map<String, Object> fillMap(Object out,
                                               Object bean,
                                               Deque<String> properties)
    {
        @SuppressWarnings("unchecked")
        Map<String, Object> outMap = (Map<String, Object>) out;

        String property = properties.pop();

        Object subBean = getProperty(bean, property);

        // add sub bean to map if exists
        if (subBean != null)
        {
            Object subOut = outMap.get(property);

            // ADD sub map/list for the remaining properties
            if (subOut == null)
            {
                outMap.put(property, fill(null, subBean, properties));
            }
            // otherwise, FILL sub map/list for the remaining properties
            else
            {
                fill(subOut, subBean, properties);
            }
        }

        return outMap;
    }

    /**
     * Evaluates the iterable bean for the given properties and nests them in the given list.
     *
     * E.g. an {@link MivPerson#getAppointments()} bean for the properties [ schoolName ]
     * would populate the given list (using JSON notation):
     * <pre>
     * list = [
     *     { schoolName : "School of Medicine" },
     *     { schoolName : "Engineering" }
     * ]
     * </pre>
     *
     * A subsequent fill of the above map and bean with the properties
     * [ schoolId ] yields:
     * <pre>
     * list = [
     *     { schoolName : "School of Medicine", schoolId : 1 },
     *     { schoolName : "Engineering", schoolId : 34 }
     * ]
     * </pre>
     *
     * @param out output list to fill with bean properties
     * @param bean source from which properties are evaluated
     * @param properties property references on given bean
     * @return output list filled with bean properties
     */
    private static List<Object> fillList(Object out,
                                         Object bean,
                                         Deque<String> properties)
    {
        @SuppressWarnings("unchecked")
        List<Object> outList = (List<Object>) out;

        @SuppressWarnings("unchecked")
        List<Object> beanList = bean instanceof Object[] ? Arrays.asList(bean) : ImmutableList.copyOf((Iterable<Object>) bean);

        for (int i = 0; i < beanList.size(); i++)
        {
            // create copy of remaining properties for each bean in this list
            Deque<String> propertiesCopy = new LinkedList<String>(properties);

            // FILL sub map/list for remaining properties
            if (i < outList.size())
            {
                fill(outList.get(i), beanList.get(i), propertiesCopy);
            }
            // otherwise, ADD sub map/list for remaining properties
            else
            {
                outList.add(fill(null, beanList.get(i), propertiesCopy));
            }
        }

        return outList;
    }

    /**
     * Return the property of the given bean for the given path.
     *
     * @param bean source from which property is evaluated
     * @param path property references
     * @return property object or <code>null</code> if unavailable
     */
    private static Object getProperty(Object bean, Deque<String> path)
    {
        // return bean if null or path is complete; recurse otherwise
        return path.isEmpty() || bean == null
             ? bean
             : getProperty(getProperty(bean, path.pop()), path);
    }

    /**
     * Return the property of the given bean for the given name reference.
     *
     * @param bean source from which property is evaluated
     * @param name property reference
     * @return property object or <code>null</code> if unavailable
     */
    private static Object getProperty(Object bean, String name)
    {
        Object property = null;

        /*
         * For maps, attempt to retrieve the property
         * treating the reference as a key value.
         */
        if (bean instanceof Map)
        {
            try
            {
                property = ((Map<?,?>) bean).get(name);
            }
            // leave property null
            catch (ClassCastException | NullPointerException e){}
        }

        /*
         * Otherwise, treating reference as a 'getter'.
         */
        if (property == null)
        {
            try
            {
                property = PropertyUtils.getSimpleProperty(bean, name);
            }
            // leave property null
            catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e){}
        }

        /*
         * Otherwise, get property via reflection.
         */
        if (property == null)
        {
            try
            {
                property = bean.getClass().getMethod(name).invoke(bean);
            }
            //leave property null
            catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e){}
        }

        return property;
    }
}
