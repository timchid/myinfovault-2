/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Resequence.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.QueryTool;

/**
 * Accept resequencing changes.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 * @since Significantly re-architected for MIV 4.7
 */
public class Resequence extends MIVServlet
{
    private static final long serialVersionUID = 200703221054L;

    private static final Map<String, Map<String, String>> sectionidByNameMap = MIVConfig.getConfig().getMap("sectionidbyname");


//    @Override
//    public void init(ServletConfig config) throws ServletException
//    {
//        super.init(config);
//        // this.context = config.getServletContext();
//
//        // NOTE: Grabbing 'debug' here means we won't see changes to its value.
//        // Maybe we should read it in doGet and doPost instead.
//        // String dbgSetting = MIVConfig.getConfig().getProperty("debug");
//        // if (dbgSetting != null) {
//        // this.debug = Boolean.parseBoolean(dbgSetting);
//        // }
//    }


    /**
     * Accept the new record sequence and save it to the database.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        logger.debug("Should be saving Resequence results now.");

        if (logger.isDebugEnabled())
        {
            String referer = req.getHeader("referer");
            logger.debug("Resequence :: referer=[{}]", referer);
        }

        String actiontype = req.getParameter("actiontype");

        // if this is not a "SAVE" request then... uh... something's wrong
        if ("save".equalsIgnoreCase(actiontype))
        {
            logger.debug("Should be doing SAVE because actiontype == {}", actiontype);
            final int userId = mivSession.get().getUser().getTargetUserInfo().getPerson().getUserId();
            saveSequence(req, res, userId);
        }

        return;
    }
    // doPost()


    /**
     * Save re-sequenced records.
     * SDP: FIXME: This method should not read from the request or write the response; it should
     *      do the save and return the number of records saved, or a message, or a status object
     *      that can separately list number of sections and number of records saved.
     *      The "doPost" method should handle the request and response.
     *
     * @param req
     * @param res
     * @param userId
     * @throws IOException
     */
    private void saveSequence(final HttpServletRequest req, final HttpServletResponse res, final int userId) throws IOException
    {
        logger.trace(" **** saveSequence **** ");

        String recordType = MIVUtil.sanitize(req.getParameter("rectype"));

        String sSectionCount = MIVUtil.sanitize(req.getParameter("sectionCount"));
        int sectionCount = Integer.parseInt(sSectionCount);

        String[] sections = req.getParameterValues("sections[]");
        String[] headers = req.getParameterValues("headers[]");

        if (logger.isDebugEnabled())
        {
            if (sections != null) {
                logger.info("count = {},  sections[] = _{}_", sections.length, Arrays.toString(sections));
            }
            if (headers != null) {
                logger.info("Found {} headers", headers.length);
                for (String header : headers) {
                    logger.info("Header = [{}]", header);
                }
            }
        }

        final StringBuilder resultMessage = new StringBuilder();
        final String orderedType; // Indicate what sort of thing we resequenced for the result message -- records or documents?

        // For almost all record types we use the UserID to qualify the record ID numbers so we don't
        // accidentally (or maliciously) update someone elses records.
        // But when re-ordering the documents attached to a dossier it's the Dossier ID we care about.
        final String keyVal;
        if ( ! "dossier-document".equals(recordType) )
        {
            keyVal = Integer.toString(userId);
            orderedType ="records";
        }
        else
        {
            String dossierId = req.getParameter("keyVal");
            if (dossierId == null || dossierId.trim().length() == 0 ||
                dossierId.equals("null") || dossierId.equals("undefined")) // cases where javascript null and undefined get translated into those words
            {
                if (isAjaxRequest())
                {
                    PrintWriter writer = res.getWriter();
                    String message = "Resequencing the documents failed. No Dossier ID number was found.";
                    writer.print("{ \"success\":false, \"message\":\"" + message + "\", \"timestamp\":" + System.currentTimeMillis() + " }");
                }
                logger.error("Missing the Dossier ID when trying to save resequenced documents in an Open Action");
                return;
            }
            keyVal = dossierId;
            orderedType = "documents";
        }


        // Handle each resequence-able block of records for each of the sections that was on the page.
        if (sectionCount > 0)
        {
            logger.debug(" - - - - - -> Processing Sections <- - - - - -");
            for (int i=0; i<sectionCount; i++)
            {
                final String sectionParam = "sections[" + i + "][section]";
                final String sectionName = req.getParameter(sectionParam);
                final String tableName = getTableForSection(sectionName);
                logger.debug("- - - - - -> Section Name: [{}]\n" +
                             "           >   found table name [{}]", sectionName, tableName);

                final String recordsParam = "sections[" + i + "][records][]";
                final String[] recordIds = req.getParameterValues(recordsParam);

                storeSequence(recordType, tableName, keyVal, recordIds);
            }
            resultMessage.append(orderedType);
        }

        // Handle Additional-Information section order if present.
        if (headers != null && headers.length > 0)
        {
            if (resultMessage.length() > 0) resultMessage.append(" and ");
            resultMessage.append("sections");
            String[] headerIds = getHeaderIdList(headers);
            storeSequence("AdditionalHeader", "AdditionalHeader", keyVal, headerIds);
        }

        // Send a JSON response to the callere if this was an AJAX request.
        // TODO: Two things aren't really handled still ...
        //       1. What if this *wasn't* an AJAX request? (we currently don't support non-javascript resequencing so it's always ajax for now)
        //       2. Even if saving the records fails this always returns "success:true"
        if (isAjaxRequest())
        {
            PrintWriter writer = res.getWriter();
            String message = "Resequencing of " + resultMessage.toString() + " has been saved successfully.";
            writer.print("{ \"success\":true, \"message\":\"" + message + "\", \"timestamp\":" + System.currentTimeMillis() + " }");
        }
    }
    // saveSequence()


    /**
     * Get the Section's base table name for the given section name.
     * These are the names used in the Document/Document-Section/Section relationship.
     * @param sectionName Name of the section
     * @return a String naming the primary table where the records for the named section are stored
     */
    private String getTableForSection(final String sectionName)
    {
        String tableName = null;

        // Additional Information records within an Additional Header section are a special case. As usual.
        // The sectionName sent is like 'service-additional-1234' with the section header record number
        // appended, and the additional information table is never mentioned in the Section table in any
        // case, so the lookup in the section map will never find the table name.
        if (sectionName.contains("-additional-")) return "AdditionalInformation";

        Map<String,String> sectionMap = sectionidByNameMap.get(sectionName);

        if (sectionMap != null) {
            tableName = sectionMap.get("sectionbasetable");
        }

        //logger.debug("Found table [{}] for sectionName [{}]", tableName, sectionName);
        return tableName;
    }


    /**
     * Get the list of Additional Information header IDs to be re-sequenced.
     * Finds the numeric header ID suffix.
     * @param headers Full section heading retrieved from the page section.
     * @return an array of the numeric header ID values extracted from the full headings
     */
    private String[] getHeaderIdList(final String[] headers)
    {
        String[] headerIds = new String[headers.length];

        int i = 0;
        for (String header : headers) {
            headerIds[i++] = header.substring(header.lastIndexOf('-') + 1);
        }

        return headerIds;
    }


    /**
     * Update the Sequence column into given table
     * @param recordType
     * @param tableName
     * @param keyVal
     * @param recordIds
     */
    private void storeSequence(final String recordType, final String tableName, final String keyVal, final String[] recordIds)
    {
        QueryTool qt = MIVConfig.getConfig().getQueryTool();

        // Now do the query, getting the sequence numbers for the records
        // in the current section. Assign the sequence numbers in the order
        // they were retrieved (since we ORDERed BY) to the values in their
        // comma-delimited order.

        // TODO: This update could be done better either by
        // + getting the records from the old table and using a cursor to go
        // over the ResultSet
        // and do the update: rs.next(); rs.updateString();
        // or
        // + use statement.addBatch(query) several times followed by a single
        // statement.executeBatch()

        final String updateKey = ("dossier-document".equalsIgnoreCase(recordType) ? "PacketID" : "UserID");

        String updateQuery = //"UPDATE " + tableName + " SET Sequence = ?" + " WHERE UserID = ? AND ID = ?" + "";
                new StringBuilder("UPDATE ").append(tableName)
                          .append(" SET Sequence = ?")
                          .append(" WHERE ID = ? AND ").append(updateKey).append(" = ?")
                .toString();
        logger.debug("Updating with script: {}", updateQuery);

        int newSeqNum = 100;

        for (String recId : recordIds)
        {
            recId = regex.matcher(recId).replaceAll("");
            String seqNum = Integer.toString(newSeqNum);

            logger.debug("UPDATE {} SET Sequence = {} WHERE ID = {} AND {} = {}",
                         new Object[] { tableName, seqNum, recId, updateKey, keyVal });

            qt.runQuery(updateQuery, "", seqNum, recId, keyVal);
            newSeqNum += 10;
        }
    }
    private static final Pattern regex = Pattern.compile("\\p{Alpha}|\\p{Space}"); // maybe just do "[^0-9]" ?
}
