/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserReportSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.report.UserReportService;
import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.ReportUtil;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.valuebeans.report.ReportVO;
import edu.ucdavis.mw.myinfovault.valuebeans.report.ResultCellVO;
import edu.ucdavis.mw.myinfovault.web.spring.report.ReportSearchStrategy;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Search Strategy class for User Role Search on Reports
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class UserReportSearchStrategy implements ReportSearchStrategy
{
    private UserReportService userReportService = MivServiceLocator.getUserReportService();


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.report.ReportSearchStrategy#getUsersByName(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public ReportVO getUsersByName(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();

// This is a Report - The logged-in and switched-to user should *not* be filtered out.
//        // Get the select filter to filter out entries for logged in user as well as the impersonated user
//        // There is not restriction for the Vice Provost role
//        SelectFilter selectFilter = null;
//        if (!actor.hasRole(VICE_PROVOST_STAFF))
//        {
//            selectFilter = CommonUtil.getSelectFilter(user);
//        }
        List<Map<String, Object>> people = this.getUsersByName(actor, criteria, null);

        return prepareResult(people, criteria.getRole());
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.report.ReportSearchStrategy#getUsersByName(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<Map<String, Object>> getUsersByName(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s)
    {

        List<Map<String, Object>> resultList = null;

        String searchName = criteria.getInputName();
        String searchInitial = criteria.getLname();

        String searchKey;
        String searchString;
        if (StringUtils.hasText(searchName))    // The name was entered so search for that
        {
            searchKey = MivSearchKeys.Person.NAME;
            searchString = "*" + searchName.toLowerCase() + "*";
            searchString = StringUtil.prepareTextSearchCriteria(searchString,"*");
        }
        else // Otherwise use the first initial of last name
        {
            // When "all" was chosen blank out the initial so just a wildcard remains
            if ("all".equalsIgnoreCase(searchInitial)) searchInitial = "";
            searchKey = MivSearchKeys.Person.LAST_NAME;
            searchString = searchInitial.toLowerCase() + "*";
        }

        AttributeSet as = new AttributeSet();
        as.put(searchKey, searchString);

        if (criteria.isActiveOnly()) as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");

        if (criteria.getRole() != null)
        {
            String roleID = String.valueOf(criteria.getRole().getRoleId());
            as.put(MivSearchKeys.Person.MIV_ROLE, roleID);
        }

        resultList = userReportService.findUsers(as);

        return resultList;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.report.ReportSearchStrategy#getUsersByDepartment(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public ReportVO getUsersByDepartment(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();
// This is a Report - The logged-in and switched-to user should *not* be filtered out.
//        SelectFilter selectFilter = null;
//        if (!actor.hasRole(VICE_PROVOST_STAFF))
//        {
//            selectFilter = CommonUtil.getSelectFilter(user);
//        }
        List<Map<String, Object>> people = this.getUsersByDepartment(actor, criteria, null/*selectFilter*/);

        return prepareResult(people, criteria.getRole());
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.report.ReportSearchStrategy#getUsersByDepartment(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<Map<String, Object>> getUsersByDepartment(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s)
    {
        List<Map<String, Object>> resultList = null;

        int searchSchool = criteria.getDeptSchoolId();
        int searchDept = criteria.getDepartmentId();
        AttributeSet as = new AttributeSet();//null;

        // "All" was chosen - what "All" means varies depending on the role of the actor
        final MivRole role = criteria.getRole();

        if (searchSchool == 0 && searchDept == 0)
        {
            // Don't set either a school or department code, so the search is not limited
            if (criteria.isActiveOnly()) as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");

        }
        // Some specific school + department was chosen, or "All" as <school>:0 meaning
        // all departments within the given school.
        else
        {
            as.put(MivSearchKeys.Person.MIV_SCHOOL_CODE, Integer.toString(searchSchool));

            // No need to pass department code in case of Dean Report
            if (role != MivRole.DEAN && searchDept != 0)
            {
                as.put(MivSearchKeys.Person.MIV_DEPARTMENT_CODE, Integer.toString(searchDept));
            }

            if (criteria.isActiveOnly()) as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");
        }

        if (criteria.getRole() != null)
        {
            String roleID = Integer.toString(criteria.getRole().getRoleId());
            as.put(MivSearchKeys.Person.MIV_ROLE, roleID);
        }

        resultList = userReportService.findUsers(as);

        return resultList;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.report.ReportSearchStrategy#getUsersBySchool(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public ReportVO getUsersBySchool(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();
// This is a Report - The logged-in and switched-to user should *not* be filtered out.
//        SelectFilter selectFilter = null;
//        if (!actor.hasRole(VICE_PROVOST_STAFF))
//        {
//            selectFilter = CommonUtil.getSelectFilter(user);
//        }
        List<Map<String, Object>> people = this.getUsersBySchool(actor, criteria, null/*selectFilter*/);

        return prepareResult(people, criteria.getRole());
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.report.ReportSearchStrategy#getUsersBySchool(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<Map<String, Object>> getUsersBySchool(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s)
    {
        List<Map<String, Object>> resultList = null;

        AttributeSet as = new AttributeSet();
        int searchSchool = criteria.getSchoolId();

        // If a specific school was chosen from the dropdown, or one was assigned
        // for a SCHOOL_STAFF search, create a search on just that school.
        if (searchSchool != 0)
        {
            as.put(MivSearchKeys.Person.MIV_SCHOOL_CODE, Integer.toString(searchSchool));
        }

        if (criteria.isActiveOnly())
        {
            as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");
        }

        if (criteria.getRole() != null)
        {
            String roleID = Integer.toString(criteria.getRole().getRoleId());
            as.put(MivSearchKeys.Person.MIV_ROLE, roleID);
        }

        resultList = userReportService.findUsers(as);

        return resultList;
    }


    private ReportVO prepareResult(List<Map<String, Object>> people, MivRole reportType)
    {
        ReportVO reportVO = new ReportVO();

        List<String> keyList = new ArrayList<String>();
        if (reportType == null) reportType = MivRole.MIV_USER;
        switch (reportType)
        {
            case DEAN:
                keyList.add("user.report.user");
                keyList.add("user.report.schoolcollege");
                break;
            case DEPT_CHAIR:
                keyList.add("user.report.user");
                keyList.add("user.report.schoolcollege");
                keyList.add("user.report.department");
                break;
            case MIV_USER:
                keyList.add("user.report.user");
                keyList.add("user.report.schoolcollege");
                keyList.add("user.report.department");
                keyList.add("user.report.role");
                break;
        }

        reportVO.setKeyList(keyList);

        List<String> headerList = ReportUtil.prepareHeaderListByKeyList(keyList);
        reportVO.setHeaderList(headerList);

        if (people != null && people.size() > 0)
        {
            List<Map<String, ResultCellVO>> resultList = new ArrayList<Map<String, ResultCellVO>>();
            Map<String, ResultCellVO> resultMap = null;

            for (Map<String, Object> peopleMap : people)
            {
                resultMap = new HashMap<String, ResultCellVO>();

                safeAddToMap(resultMap, "user.report.user", new ResultCellVO(StringUtil.insertValue(peopleMap.get("sortname"))));

                safeAddToMap(resultMap, "user.report.schoolcollege", new ResultCellVO(StringUtil.insertValue(peopleMap.get("schoolname"))));

                safeAddToMap(resultMap, "user.report.department", new ResultCellVO(StringUtil.insertValue(peopleMap.get("departmentname"))));

                final Object mivCode = peopleMap.get("mivcode");
                final MivRole role = MivRole.valueOf(mivCode != null ? mivCode.toString() : MivRole.INVALID_ROLE.toString());
                resultMap.put("user.report.role", new ResultCellVO(role == MivRole.INVALID_ROLE ? "" : role.getShortDescription()));

                resultList.add(resultMap);
            }

            reportVO.setResultList(resultList);
            if(!resultList.isEmpty())
            {
                reportVO.setIndividualColumnFilters("all");
            }
        }

        return reportVO;
    }


    /**
     * Safely adds an Object to a Map.
     *
     * @param map a Map<String, ResultCellVO> in which to add a key-value pair
     * @param key key String with which the specified value is to be associated
     * @param cellVO
     * @return the previous value associated with key, or empty value ResultCellVO if there was no mapping for key.
     */
    public static Object safeAddToMap(Map<String, ResultCellVO> map, String key, ResultCellVO cellVO)
    {
        Object current = map.put(key, cellVO != null ? cellVO : new ResultCellVO("")); // not a 1-liner only so we can set a breakpoint on the return statement
        return current;
    }
}
