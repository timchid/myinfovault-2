package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.List;

/**
 * Command Object representing a Biosketch
 * @author dreddy
 * @since MIV 2.1
 */
public class SelectDataCommand extends BaseCommand
{
    private String name;
    private List<BiosketchSectionCommand> biosketchSection;
    private RulesetCommand ruleset = new RulesetCommand();
    private String startYear;
    private String endYear;

    /**
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return
     */
    public RulesetCommand getRuleset()
    {
        return ruleset;
    }

    /**
     * @param ruleset
     */
    public void setRuleset(RulesetCommand ruleset)
    {
        this.ruleset = ruleset;
    }

    public List<BiosketchSectionCommand> getBiosketchSection()
    {
        return biosketchSection;
    }

    public void setBiosketchSection(List<BiosketchSectionCommand> biosketchSection)
    {
        this.biosketchSection = biosketchSection;
    }

    public String getStartYear()
    {
        return startYear;
    }

    public void setStartYear(String startYear)
    {
        this.startYear = startYear;
    }

    public String getEndYear()
    {
        return endYear;
    }

    public void setEndYear(String endYear)
    {
        this.endYear = endYear;
    }
}
