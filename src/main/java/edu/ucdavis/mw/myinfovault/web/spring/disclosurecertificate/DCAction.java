/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.disclosurecertificate;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.util.StringUtils;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events2.DisclosureSignatureRequestEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierErrorEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.events2.MivEvent;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivPropertyEditors;
import edu.ucdavis.mw.myinfovault.web.spring.signature.DisclosureAction;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MivServerAwareMailMessage;

/**
 * Used from Manage Open Actions to create, manage, and mail a Disclosure Certificate.
 * Signing the disclosure is handled by {@link DisclosureAction}.
 *
 * @author Jed Whitten
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class DCAction extends MivFormAction
{
    private static final ThreadLocal<DateFormat> df =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    private DCService dcService;
    private SigningService signingService;

    private static final String BREADCRUMB_PATTERN = "{0,choice,0#Add|0<Edit} {1}";

    private static final String LOG_PATTERN = "DC email was sent for dossier #{0,number,#} belonging to {1}/{2,number,#}";

    private static final String MESSAGE_PATTERN = "You are receiving this notification from the {0} because the Disclosure Certificate for your " +
                                                  "Dossier is ready to be signed. Please follow these steps to sign your Disclosure Certificate:\n\n" +
                                                  "* Log in to MyInfoVault to review your dossier as outlined in the Candidate's Disclosure " +
                                                  "Certificate, the department's recommendation (and redacted evaluations, if applicable) at {1}.\n" +
                                                  "* Select the \"View My Complete Dossier/Sign My Disclosure Certificate\" link located under the \"Sign Documents\" " +
                                                  "heading, or the top navigation menu \"My Dossier\" tab.\n\n" +
                                                  "If you feel you have received this message in error or have questions, please contact your " +
                                                  "department administrator or the MIV Project Team at miv-help@ucdavis.edu.";

    private static final String ADDITIONAL_PATTERN = "\n\n\n[Additional Information]\n\n{0}";

    private static final String MAIL_SUBJECT = "MyInfoVault Candidate's Disclosure Certificate requires signature";


    /**
     * Create the DC spring form action bean.
     *
     * @param userService Spring-injected user service
     * @param authorizationService Service Spring-injected authorization service
     * @param dcService Spring-injected disclosure certificate service
     * @param signingService Spring-injected signing service
     */
    public DCAction(UserService userService,
                    AuthorizationService authorizationService,
                    DCService dcService,
                    SigningService signingService)
    {
        super(userService, authorizationService);

        this.dcService = dcService;
        this.signingService = signingService;

        EventDispatcher.getDispatcher().register(this);
    }


    /**
     * Checks if user is authorized to edit this disclosure certificate.
     *
     * @param context Request context from webflow
     * @return Event status, success or error
     */
    public Event isAuthorized(RequestContext context)
    {
        DCBo dc = getFormObject(context).getDc();

        AttributeSet qualification = new AttributeSet();

        qualification.put(Qualifier.SCHOOL,
                          Integer.toString(dc.getSchoolId()));

        qualification.put(Qualifier.DEPARTMENT,
                          Integer.toString(dc.getDepartmentId()));

        return authorizationService.isAuthorized(getShadowPerson(context),
                                                 Permission.EDIT_DISCLOSURE,
                                                 null,
                                                 qualification)
             ? success()
             : denied();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DCForm createFormObject(RequestContext context)
    {
        Dossier dossier = (Dossier) context.getConversationScope().get("dossier");

    	int schoolId = getParameterValue(context, "schoolId", 0);
    	int departmentId = getParameterValue(context, "departmentId", 0);

        DCBo dc = dcService.getDisclosureCertificateDraft(dossier, schoolId, departmentId);

        // prepare email message
        String message = MessageFormat.format(MESSAGE_PATTERN,
                                              StringUtils.hasText(dc.getDepartmentName())
                                            ? dc.getDepartmentName().toUpperCase() + " department"
                                            : dc.getSchoolName().toUpperCase(),
                                              MIVConfig.getConfig().getServer());
        /*
         * Load DC form data.
         */
        return new DCForm(dc,
                          getRealPerson(context),
                          getShadowPerson(context),
                          getLatestSigningDate(dc),
                          MAIL_SUBJECT,
                          message);
    }


    /**
     * Get the latest signing date for the disclosure certificate.
     *
     * @param dc disclosure certificate
     * @return latest date the DC was signed or <code>null</code>, if not signed
     */
    private Date getLatestSigningDate(DCBo dc)
    {
        MivElectronicSignature signature = signingService.getSignatureByDocumentAndUser(
                                               dcService.getDisclosureCertificateLatestSigned(
                                                   dc.getDossier(),
                                                   dc.getSchoolId(),
                                                   dc.getDepartmentId()),
                                               dc.getDossier().getAction().getCandidate().getUserId());

        return signature != null ? signature.getSigningDate() : null;
    }


    /**
     * Persist the DC form object.
     *
     * @param context Request context from webflow
     * @return Webflow event status
     */
    public Event saveFormObject(RequestContext context)
    {
        // persist disclosure certificate
        dcService.persist(getFormObject(context).getDc(),
                          getRealPerson(context).getUserId());

        return success();
    }


    /**
     * Make sure a RAF exists for this dossier
     * Also, make sure all required uploads have been added
     * If not, they cannot request a signature.
     *
     * @param context Request context from webflow
     * @return Event status, success or error
     */
    public Event checkRaf(RequestContext context)
    {
        DCBo dc = getFormObject(context).getDc();
        DossierAppointmentAttributeKey key = new DossierAppointmentAttributeKey(dc.getSchoolId(), dc.getDepartmentId());
        Dossier dossier = dc.getDossier();

        if (dc.isPrimary()
         && !dossier.isAcademicActionFormPresent()
         && !dossier.isRecommendedActionFormPresent())
        {
            getFormErrors(context).reject(null, "A Recommended Action Form has not been added for this candidate yet.");

            return error();
        }

        //Loop through appointment attributes associated with the disclosure certificate
        //looking for required uploads that haven't been added.
        for (DossierAttribute attr : dossier.getAttributesByAppointment(key).getAttributes().values())
        {
            if (attr.isRequired() &&
                attr.getAttributeType() == DossierAttributeType.DOCUMENTUPLOAD &&
                attr.getAttributeValue() != DossierAttributeStatus.ADDED)
            {
                getFormErrors(context).reject(null, "Required uploads must be uploaded prior to issuing a disclosure certificate signature request.");

                return error();
            }
        }

        return success();
    }


    /**
     * Create a signature request for this document.
     *
     * @param context Request context from webflow
     * @return Event status, success or error
     */
    public Event requestSignature(RequestContext context)
    {
        MivPerson realPerson = getRealPerson(context);

        /*
         * Request candidate signature for the current DC draft.
         */
        MivElectronicSignature signature = dcService.requestSignature(getFormObject(context).getDc(),
                                                                      realPerson);

        // Post the request event event
        MivEvent event = new DisclosureSignatureRequestEvent(realPerson, signature)
                             .setShadowPerson(getShadowPerson(context));

        EventDispatcher2.getDispatcher().post(event);

        context.getFlowScope().put("parentEvent", event);

        return success();
    }


    /**
     * Send the candidate an email requesting DC signature.
     *
     * @param context Request context from webflow
     * @return Event status, success or error
     */
    public Event sendDisclosureMail(RequestContext context)
    {
        DCForm dcForm = getFormObject(context);

        String message = dcForm.getEmailMessage();

        // user has added text to the additional information text area
        if (StringUtils.hasText(dcForm.getEmailBody()))
        {
            message += MessageFormat.format(ADDITIONAL_PATTERN, dcForm.getEmailBody());
        }

        // send email and note send time in form
        String sentTo = null;
        try
        {
            MivServerAwareMailMessage email = new MivServerAwareMailMessage(dcForm.getDc().getDossier().getAction().getCandidate(),
                                                                            getShadowPerson(context),
                                                                            getRealPerson(context),
                                                                            dcForm.getEmailSubject());
            // add CC address if available
            if (StringUtils.hasText(dcForm.getEmailCc()))
            {
                email.setCc(dcForm.getEmailCc());
            }

            // update TO address if something other than the default was entered
            if (!dcForm.getEmailTo().equals(dcForm.getEmailToDefault()))
            {
                email.setTo(dcForm.getEmailTo());
            }
            sentTo = email.getTo();

            dcForm.setEmailSent(email.send(message));

        }
        catch (MessagingException e)
        {
            logger.error("Failed to send disclosure certificate email", e);

            getFormErrors(context).reject(null, "Failed to send disclosure certificate email to one or more recipients. Make sure the email address(es) are correct.");

            MivEvent dossierErrorEvent =
                    new DossierErrorEvent((MivEvent)context.getFlowScope().get("parentEvent"), getDossier(context), e)
                    .setShadowPerson(getShadowPerson(context))
                    .setComments("Failed to send disclosure certificate email to "
                    +sentTo);
            EventDispatcher2.getDispatcher().post(dossierErrorEvent);

            return error();
        }

        // log DC mail sent
        if (logger.isInfoEnabled())
        {
            logger.info(MessageFormat.format(LOG_PATTERN,
                                             dcForm.getDc().getDossier().getDossierId(),
                                             dcForm.getDc().getCandidateName(),
                                             dcForm.getDc().getDossier().getAction().getCandidate().getUserId()));
        }

        return success();
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(PropertyEditorRegistry registry)
    {
        registry.registerCustomEditor(DossierDelegationAuthority.class, new MivPropertyEditors.DelegationAuthorityPropertyEditor());
        registry.registerCustomEditor(DossierActionType.class, new MivPropertyEditors.ActionTypePropertyEditor());
        registry.registerCustomEditor(Date.class, new CustomDateEditor(df.get(), false));
    }

    /**
     * Add bread crumb for disclosure certificate form.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        DCBo dc = getFormObject(context).getDc();

        addBreadcrumbs(context,
                       MessageFormat.format(BREADCRUMB_PATTERN,
                                            dc.getDocumentId(),
                                            dc.getDocumentType().getDescription()));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#getFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public DCForm getFormObject(RequestContext context)
    {
        return (DCForm) super.getFormObject(context);
    }
}
