package edu.ucdavis.mw.myinfovault.web.spring.search;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Implements the strategy to be used when searching for users to edit as
 * well as deactivate/reactivate. The search strategy is determined by the role of the searching user as follows:
 * Only Department Admins, School Admins, and MIV Admins are allowed to edit users.
 * Users being edited must be at or "below" the editors role.
 * Editing is done within defined scopes:
 * <ul>
 * <li>{@link MivRole#VICE_PROVOST_STAFF} may edit anyone</li>
 * <li>{@link MivRole#SCHOOL_STAFF} may edit anyone at their level and below in their own school, except themselves</li>
 * <li>{@link MivRole#DEPT_STAFF} may edit anyone at their level and below in their own department, except themselves</li>
 * </ul>
 *
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public class EditUserSearchStrategy extends DefaultSearchStrategy
{
    /**
     * Permission name to check when testing authorization for the actor to act on a given person.
     * Subclasses may override this by setting a different value.
     */
    protected String checkPermission = Permission.EDIT_USER;


    /**
     * Gets a list of users by name to edit.
     * Overridden basic search for users by department in order to apply a
     * SelectFilter which will eliminate the logged in user (except Vice Provost Role)
     * and target user from the results as well as further scoping the results to only
     * include persons the current user is allowed to edit via the authorizedPersons helper method.
     *
     * @param user The target user doing the search
     * @param criteria The search criteria to use
     * @return A scoped list of MivPerson objects
     */
    @Override
    public List<MivPerson> getUsersByName(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();

        // Get the select filter to filter out entries for logged in user as well as the impersonated user
        // There is not restriction for the Vice Provost role
        SelectFilter selectFilter = null;
        if (!actor.hasRole(VICE_PROVOST_STAFF)) {
            selectFilter = this.getSelectFilter(user);
        }
        List<MivPerson>people = this.getUsersByName(actor, criteria, selectFilter);
        return this.authorizedPersons(actor, people);
    }


    /**
     * Gets a list of users by department to edit.
     * Overridden basic search for users by department in order to apply a
     * SelectFilter which will eliminate the logged in user (except Vice Provost Role)
     * and target user from the results as well as further scoping the results to only
     * include persons the current user is allowed to edit via the authorizedPersons helper method.
     *
     * @param user The target user doing the search
     * @param criteria The search criteria to use
     * @return A scoped list of MivPerson objects
     */
    @Override
    public List<MivPerson> getUsersByDepartment(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();
        SelectFilter selectFilter = null;
        if (!actor.hasRole(VICE_PROVOST_STAFF)) {
            selectFilter = this.getSelectFilter(user);
        }
        List<MivPerson>people = this.getUsersByDepartment(actor, criteria, selectFilter);
        return this.authorizedPersons(actor, people);

    }


    /**
     * Gets a list of users by school to edit.
     * Overridden basic search for users by department in order to apply a
     * SelectFilter which will eliminate the logged in user (except Vice Provost Role)
     * and target user from the results as well as further scoping the results to only
     * include persons the current user is allowed to edit via the authorizedPersons helper method.
     *
     * @param user target user doing the search
     * @param criteria search criteria to use
     * @return A scoped list of MivPerson objects
     */
    @Override
    public List<MivPerson> getUsersBySchool(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();
        SelectFilter selectFilter = null;
        if (!actor.hasRole(VICE_PROVOST_STAFF)) {
            selectFilter = this.getSelectFilter(user);
        }
        List<MivPerson>people = this.getUsersBySchool(actor, criteria, selectFilter);
        return this.authorizedPersons(actor, people);
    }


    /**
     * Helper method to authorize that target user is allowed to edit
     * each MivPerson in the input list. Those MivPersons to which the user is not authorized to edit
     * will be eliminated from the returned list.
     *
     * @param mivPerson The target user
     * @param people The list of MivPerson objects to be authorized
     * @return A list of MivPerson objects to which the target user is allowed to switch.
     */
    protected List<MivPerson> authorizedPersons(MivPerson mivPerson, List<MivPerson> people)
    {
        List<MivPerson> authorizedPeople = new ArrayList<MivPerson>(people.size());

        for (MivPerson p : people)
        {
            // principal qualifier
            AttributeSet qualification = new AttributeSet();
            qualification.put(Qualifier.USERID, String.valueOf(p.getUserId()));

            if (authorizationService.isAuthorized(mivPerson, checkPermission, null, qualification))
            {
                authorizedPeople.add(p);
            }
        }

        return authorizedPeople;
    }
}
