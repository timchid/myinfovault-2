/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: OpenActionReportSearchStrategy.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto.EntityType;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.group.GroupService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Dossier searchs specific to the Open Actions Reports
 *
 * @author Mary Northup
 * @since 4.8.3
 */

public class OpenActionReportSearchStrategy extends DefaultSearchStrategy
{

    private static final SigningService signingService = MivServiceLocator.getSigningService();
    private static final GroupService groupService = MivServiceLocator.getGroupService();

    private static final String LOG_PATTERN = "Missing decision for dossier -- #{0,number,#} for {1} schoolId: {2,number,#} deptId: {3,number,#}";

    @Override
    public List<MivActionList> dossierSearchByCriteria(Map<String, String> allCriteria, MIVUser user)
    {
        List<MivActionList> people = super.dossierSearchByCriteria(allCriteria,user);
        DossierLocation dl = DossierLocation.mapWorkflowNodeNameToLocation(allCriteria.get("LOCATION"));

        List<MivActionList> peopleExtended = getAdditionalActionListData(people, user, dl);
        return peopleExtended;
    }

    @Override
    public List<MivActionList> getDossiersAtLocation(MIVUser user, DossierLocation dl)
    {
        List<MivActionList> results = new ArrayList<MivActionList>();

        results = super.getDossiersAtLocation(
                user,
                dl);

        // There are multiple 'vice provost' and 'school' locations so we need to get more
        switch (dl)
        {
        case SCHOOL:
            results.addAll(super.getDossiersAtLocation(
                    user,
                    DossierLocation.POSTSENATESCHOOL));
            break;
        case VICEPROVOST:
            results.addAll(super.getDossiersAtLocation(
                    user,
                    DossierLocation.POSTSENATEVICEPROVOST));
            break;
        default: break;
        }
        return getAdditionalActionListData(results,
                user,
                dl);
    }

    /**
     * Get the additional data needed for the Open Actions Report
     *
     * @param actionList the initial list of viewable actions
     * @param user who will be viewing this report
     * @param location the dossier location that the user has requested to see
     * @return MIV Action list objects
     */

    private List<MivActionList> getAdditionalActionListData(List<MivActionList> actionList, MIVUser user, DossierLocation location)
    {
        List<MivActionList> reportList = new ArrayList<MivActionList>();

        for (MivActionList action : actionList)
        {
            try
            {
                Dossier dossier = dossierService.getDossierAndLoadAllData(action.getDossierId());
                DossierAppointmentAttributeKey apptKey = new DossierAppointmentAttributeKey(action.getSchoolId(),
                        action.getDepartmentId());
                DossierAppointmentAttributes daa = dossier.getAppointmentAttributes().get(apptKey);
                Map<String, DossierAttribute> da = daa.getAttributes();
                boolean completed = false;
                MivDocument documentType = MivDocument.NONE;
                switch (location)
                {
                case POSTSENATEVICEPROVOST:
                case VICEPROVOST:
                    documentType = getVPData(action, daa, da);
                    completed = action.hasExecutiveFinalDecision();
                    break;
                case SCHOOL:
                case POSTSENATESCHOOL:
                case SENATE_OFFICE:
                case FEDERATION:
                case SENATEFEDERATION:
                    documentType = getSchoolData(action, dossier, daa, da);
                    completed = isCompleted(dossier,DossierLocation.VICEPROVOST, DossierLocation.READYFORPOSTREVIEWAUDIT);
                    break;
                case POSTAPPEALVICEPROVOST:
                    documentType = getVPAppealData(action, daa, da);
                    completed = isCompleted(dossier, DossierLocation.ARCHIVE);
                    break;
                case POSTAPPEALSCHOOL:
                case SENATEAPPEAL:
                case FEDERATIONAPPEAL:
                case FEDERATIONSENATEAPPEAL:
                    documentType = getSchoolAppealData(action, dossier, daa, da);
                    completed = isCompleted(dossier, DossierLocation.ARCHIVE);
                    break;
                case DEPARTMENT:
                    getDepartmentData(action, daa, da);
                    completed = isCompleted(dossier, DossierLocation.SCHOOL);
                    break;
                }

                if (documentType != MivDocument.NONE)
                {
                    getDecisionData(action, dossier, documentType);
                }

                getReviewData(action, dossier, daa, da, apptKey);

                action.setJointStatus(dossier.areJointAppointmentsCompleted());
                action.setLocationStatus(dossier.getDossierLocation() == DossierLocation.PACKETREQUEST ? "Packet Requested" : (completed ? "Completed" : "In Progress"));
                reportList.add(action);
            }
            catch (WorkflowException e)
            {
                logger.warn("getAdditionalActionListData: got a WorkflowException doing getDossierAndLoadAllData with dossierId = "
                        + String.valueOf(action.getDossierId()));
            }
        }

        return reportList;
    }

    /**
     * This method sets the MivActionList values specific for dossiers at the Department
     *
     * @param action
     * @param daa
     * @param da
     *
     */
    private void getDepartmentData(MivActionList action, DossierAppointmentAttributes daa, Map<String, DossierAttribute> da)
    {
        if (daa.isPrimary() && da.containsKey("disclosure_certificate"))
        {
            action.setDisclosureStatus(da.get("disclosure_certificate").getAttributeValue() == null
                    ? "Not Added"
                            : da.get("disclosure_certificate").getAttributeValue().getLabel());
        }
        else
        {
            action.setDisclosureStatus(da.containsKey("j_disclosure_certificate") && da.get("j_disclosure_certificate").getAttributeValue() != null
                    ? da.get("j_disclosure_certificate").getAttributeValue().getLabel()
                            : "Not Added");
        }
    }

    /**
     * This method sets the MivActionList values specific for dossiers at the School Appeal location
     *
     * @param action
     * @param dossier
     * @param daa
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument getSchoolAppealData(MivActionList action, Dossier dossier, DossierAppointmentAttributes daa, Map<String, DossierAttribute> da)
    {
        /* the column will display the title Dean's Decision but it might actually be the Dean's recommendation
         *  or joint dean's recommendation depending on dossier type     */
        if (daa.isPrimary()
                && (da.containsKey("deans_appeal_decision") || da
                        .containsKey("deans_appeal_recommendation")))
        {
            DossierAttributeStatus deansValue = da.containsKey("deans_appeal_decision")
                    ? da.get("deans_appeal_decision").getAttributeValue()
                            : da.get("deans_appeal_recommendation").getAttributeValue();

                    action.setDecisionStatus(deansValue == null
                            ? "Not Added"
                                    : deansValue.getLabel());
        }
        else if (!daa.isPrimary() && da.containsKey("j_deans_appeal_recommendation"))
        {
            action.setDecisionStatus(da.get("j_deans_appeal_recommendation").getAttributeValue() == null
                    ? "Not Added"
                            : da.get("j_deans_appeal_recommendation").getAttributeValue().getLabel());
        }
        else
        {
            action.setDecisionStatus("Not Added");
        }

        action.setReleasedStatus(da.containsKey("dean_appeal_release") && da.get("dean_appeal_release").getAttributeValue() != null
                ? (da.get("dean_appeal_release").getAttributeValue() == (DossierAttributeStatus.RELEASE)
                ? "Released" : "Hold")
                : "Hold");

        MivDocument documentType = MivDocument.NONE;

        if (dossier.getAction().getDelegationAuthority() != DossierDelegationAuthority.NON_REDELEGATED)
        {
            documentType = daa.isPrimary()
                    ? MivDocument.DEANS_APPEAL_DECISION
                            : MivDocument.JOINT_DEANS_APPEAL_RECOMMENDATION;
        }
        return documentType;
    }

    /**
     * This method sets the MivActionList values specific for dossiers at the Vice Provosts Appeal location
     *
     * @param action
     * @param daa
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument getVPAppealData(MivActionList action, DossierAppointmentAttributes daa, Map<String, DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;
        // Because there could be more than one 'release' we must check for each
        if (!da.isEmpty() &&
                ((da.containsKey("vp_appeal_release") && da.get("vp_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("p_appeal_release") && da.get("p_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("chancellor_appeal_release") && da.get("chancellor_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("vp_appeal_recommendation_release") && da.get("vp_appeal_recommendation_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("p_appeal_recommentdation_release") && da.get("p_appeal_recommentdation_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("p_tenure_appeal_release") && da.get("p_tenure_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE)))
        {
            //something was released now find out what.
            action.setReleasedStatus("Released");
            if ((da.containsKey("vp_appeal_release") && da.get("vp_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                (da.containsKey("p_appeal_release") && da.get("p_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                (da.containsKey("chancellor_appeal_release") && da.get("chancellor_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                (da.containsKey("p_tenure_appeal_release") && da.get("p_tenure_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE))
            {
                action.setRecommendationOrDecision("Decision");
                documentType = findExecutiveAppealDecision(action,da);
            }
            else
            {
                action.setRecommendationOrDecision("Recommendation");
                documentType = findExecutiveAppealRecommendation(action,da);
            }
        }
        else
        {
            //nothing was released yet
            action.setReleasedTo("");
            action.setRecommendationOrDecision("");
            action.setDecisionStatus("Not Added");
            action.setReleasedStatus("Hold");
            action.setExecutiveFinalDecision(false);
        }
        return documentType;
    }

    /**
     * This method sets the MivActionList values specific for dossiers at the School location
     *
     * @param action
     * @param dossier
     * @param daa
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument getSchoolData(MivActionList action, Dossier dossier, DossierAppointmentAttributes daa, Map<String, DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;
        /* the column will display the title Dean's Decision but it might actually be the Dean's recommendation
         *  or joint dean's recommendation depending on dossier type     */
        if (daa.isPrimary()
                && (da.containsKey("deans_final_decision") || da.containsKey("deans_recommendation")))
        {
            DossierAttributeStatus deansValue = da.containsKey("deans_final_decision")
                    ? da.get("deans_final_decision").getAttributeValue()
                            : da.get("deans_recommendation").getAttributeValue();

                    action.setDecisionStatus(deansValue == null ? "Not Added" : deansValue.getLabel());
        }
        else if (!daa.isPrimary() && da.containsKey("j_deans_recommendation"))
        {
            action.setDecisionStatus(da.get("j_deans_recommendation").getAttributeValue() == null
                    ? "Not Added"
                            : da.get("j_deans_recommendation").getAttributeValue().getLabel());
        }
        else
        {
            action.setDecisionStatus("Not Added");
        }

        String releaseAttributeName = daa.isPrimary()
                ? (da.containsKey("dean_release") ? "dean_release" : "dean_recommendation_release")
                        : "joint_dean_release";

                DossierAttribute dean_release = da.get(releaseAttributeName);

                action.setReleasedStatus(dean_release != null && dean_release.getAttributeValue() != null
                        ? (dean_release.getAttributeValue() == (DossierAttributeStatus.RELEASE)
                        ? "Released" : "Hold")
                        : "Hold");

                documentType =  MivDocument.JOINT_DEANS_RECOMMENDATION;

                if (daa.isPrimary())
                {
                    documentType = dossier.getAction().getDelegationAuthority() == DossierDelegationAuthority.NON_REDELEGATED
                            ? MivDocument.DEANS_RECOMMENDATION
                                    : MivDocument.DEANS_FINAL_DECISION;
                }

                return documentType;
    }

    /**
     * This method gets the necessary information for display on the report at the Vice Provost location
     *
     * @param action
     * @param daa
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument getVPData(MivActionList action, DossierAppointmentAttributes daa, Map<String, DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;
        // Because there could be more than one 'release' we must check for each
        if (!da.isEmpty() &&
                ((da.containsKey("vp_release") && da.get("vp_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("p_release") && da.get("p_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("chancellor_release") && da.get("chancellor_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("vp_recommendation_release") && da.get("vp_recommendation_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("p_recommendation_release") && da.get("p_recommendation_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                 (da.containsKey("p_tenure_release") && da.get("p_tenure_release").getAttributeValue() == DossierAttributeStatus.RELEASE)))
        {
            //something was released now find out what.
            action.setReleasedStatus("Released");
            if ((da.containsKey("vp_release") && da.get("vp_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                (da.containsKey("p_release") && da.get("p_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                (da.containsKey("chancellor_release") && da.get("chancellor_release").getAttributeValue() == DossierAttributeStatus.RELEASE))
            {
                action.setRecommendationOrDecision("Decision");
                documentType = findExecutiveDecision(action,da);
            }
            else if ( da.containsKey("p_tenure_release") && da.get("p_tenure_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
            {
                //action.setRecommendationOrDecision("Recommendation");
                documentType = findTenureDecisionRecommendation(action,da);
            }
            else
            {
                action.setRecommendationOrDecision("Recommendation");
                documentType = findExecutiveRecommendation(action,da);
            }
        }
        else
        {
            //nothing was released yet
            action.setReleasedTo("");
            action.setRecommendationOrDecision("");
            action.setDecisionStatus("Not Added");
            action.setReleasedStatus("Hold");
            action.setExecutiveFinalDecision(false);
        }

        return documentType;
    }

    /**
     * If the action has an open review period, this method will get the pertinent information to display on the report
     *
     * @param action
     * @param dossier
     * @param daa
     * @param da
     * @param apptKey
     */
    private void getReviewData(MivActionList action, Dossier dossier, DossierAppointmentAttributes daa, Map<String,
            DossierAttribute> da, DossierAppointmentAttributeKey apptKey)
    {

        action.setReviewStatus(da.containsKey("review") && da.get("review").getAttributeValue() != null
                ? da.get("review").getAttributeValue()
                        : DossierAttributeStatus.CLOSE);

        if (action.getReviewStatus().equals(DossierAttributeStatus.CLOSE)) {
            action.setReviewGroups(null);
        }
        else {
            Integer usercnt = 0;
            List<DossierReviewerDto> dossierReviewers = new ArrayList<DossierReviewerDto>();
            try {
                dossierReviewers = dossierService.getDossierReviewers(dossier, apptKey);
            } catch (WorkflowException e) {
                e.printStackTrace();
                logger.warn("getReviewData: got a WorkflowException doing getDossierReviewers with dossierId = "
                        + String.valueOf(action.getDossierId()));
            }
            for (DossierReviewerDto group : dossierReviewers) {
                if (group.getEntityType() == EntityType.GROUP) {
                    Group reviewGroup = groupService.getGroup(group.getEntityId());
                    action.addReviewGroup(reviewGroup.getName());
                }
                else if (group.getEntityType() == EntityType.USER) {
                    usercnt++;
                }
            }
            if (usercnt>0) {
                action.addReviewGroup("Users("+usercnt+")");
            }
            if (action.getReviewGroups().isEmpty()) {
                action.addReviewGroup("Not Added");
            }
        }

    }

    /**
     * If there is a decision, this method will find it and capture the information needed for the report to display
     *
     * @param action
     * @param dossier
     * @param documentType
     */
    private void getDecisionData(MivActionList action, Dossier dossier, MivDocument documentType)
    {
        Decision decision = signingService.getDocument(dossier,
                action.getSchoolId(),
                action.getDepartmentId(),
                documentType);

        if (decision != null)
        {
            action.setDecisionType(decision.getDecisionTypeDescription());

            MivElectronicSignature signature = signingService.getSignedSignatureByDocument(decision);

            if (signature != null)
            {
                action.setDecisionDate(String.format("%tD", signature.getSigningDate()));
            }
        }
        else
        {
            action.setDecisionType("None");
            logger.info(MessageFormat.format(LOG_PATTERN,
                    dossier.getDossierId(),
                    action.getDisplayName(),
                    action.getSchoolId(),
                    action.getDepartmentId()));
        }

    }

    /**
     * This method will determine which executive decision has been released and to whom and also has it been signed
     *
     * @param action
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument findExecutiveDecision(MivActionList action, Map<String,DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;
        boolean vpDecisionAdded = false;
        boolean pDecisionAdded = false;
        boolean cDecisionAdded = false;
        boolean vpReleased = false;
        boolean pReleased = false;
        boolean cReleased = false;
        if (da.containsKey("chancellor_release") && da.get("chancellor_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            cReleased = true;
            cDecisionAdded = da.containsKey("chancellors_final_decision") &&
                    da.get("chancellors_final_decision").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(cDecisionAdded ? "Added" : "Not Added");
            documentType = MivDocument.CHANCELLORS_FINAL_DECISION;
        }
        if (( da.containsKey("p_release") && da.get("p_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ||
                (da.containsKey("p_tenure_release") && da.get("p_tenure_release").getAttributeValue() == DossierAttributeStatus.RELEASE))
        {
            pReleased = true;
            pDecisionAdded = (da.containsKey("provosts_final_decision") &&
                    da.get("provosts_final_decision").getAttributeValue() == DossierAttributeStatus.ADDED) ||
                    (da.containsKey("provosts_tenure_decision") &&
                    da.get("provosts_tenure_decision").getAttributeValue() == DossierAttributeStatus.ADDED);
            action.setDecisionStatus(pDecisionAdded || cDecisionAdded ? "Added" : "Not Added");
            documentType = cReleased ? documentType :
                ( da.containsKey("p_release") && da.get("p_release").getAttributeValue() == DossierAttributeStatus.RELEASE) ?
                        MivDocument.PROVOSTS_FINAL_DECISION : MivDocument.PROVOSTS_TENURE_DECISION;
        }
        if (da.containsKey("vp_release") && da.get("vp_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            vpReleased = true;
            vpDecisionAdded = da.containsKey("viceprovosts_final_decision") &&
                    da.get("viceprovosts_final_decision").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(pDecisionAdded || cDecisionAdded || vpDecisionAdded ? "Added" : "Not Added");
            documentType = cReleased || pReleased ? documentType : MivDocument.VICEPROVOSTS_FINAL_DECISION ;
        }

        action.setReleasedTo(createReleasedTo(cReleased, pReleased, vpReleased));
        action.setExecutiveFinalDecision(cDecisionAdded || pDecisionAdded || vpDecisionAdded);
        return documentType;
    }

    /**
     * This method will determine which executive recommendation has been released and to whom and also has it been signed
     *
     * @param action
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument findExecutiveRecommendation(MivActionList action, Map<String,DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;
        boolean vpRecommendationAdded = false;
        boolean pRecommendationAdded = false;
        boolean vpReleased = false;
        boolean pReleased = false;
        if (da.containsKey("p_recommendation_release") && da.get("p_recommendation_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            pReleased = true;
            pRecommendationAdded = da.containsKey("provosts_recommendation") &&
                    da.get("provosts_recommendation").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(pRecommendationAdded ? "Added" : "Not Added");
            documentType = MivDocument.PROVOSTS_RECOMMENDATION;
        }
        if (da.containsKey("vp_recommendation_release") && da.get("vp_recommendation_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            vpReleased = true;
            vpRecommendationAdded = da.containsKey("viceprovosts_recommendation") &&
                    da.get("viceprovosts_recommendation").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(pRecommendationAdded || vpRecommendationAdded ? "Added" : "Not Added");
            documentType = pReleased ? documentType : MivDocument.VICEPROVOSTS_RECOMMENDATION;
        }

        action.setReleasedTo(pReleased && vpReleased ? "Provost, Vice Provost" : pReleased ? "Provost" : "Vice Provost");

        return documentType;
    }

    /**
     * This method determines which tenure signature request has been made and whether it has been signed.
     * The signed request will determine if it is a decision or a recommendation.
     *
     * @param action
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument findTenureDecisionRecommendation(MivActionList action, Map<String,DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;

        if (da.containsKey("provosts_tenure_decision") && da.get("provosts_tenure_decision").getAttributeValue() == DossierAttributeStatus.ADDED)
        {
            action.setDecisionStatus( "Added");
            action.setExecutiveFinalDecision(true);
            action.setRecommendationOrDecision("Decision");
            documentType = MivDocument.PROVOSTS_TENURE_DECISION;
        }
        else if (da.containsKey("provosts_tenure_recommendation") && da.get("provosts_tenure_recommendation").getAttributeValue() == DossierAttributeStatus.ADDED)
        {
            action.setDecisionStatus( "Added");
            action.setExecutiveFinalDecision(false);
            action.setRecommendationOrDecision("Recommendation");
            documentType = MivDocument.PROVOSTS_TENURE_RECOMMENDATION;
        }
        else  //we don't know what it is yet since it hasn't been added, it will be a decision only if approved
        {
            action.setDecisionStatus( "Not Added");
            action.setExecutiveFinalDecision(false);
            action.setRecommendationOrDecision("Tenure");
        }
        action.setReleasedTo(action.getReleasedTo() + " Provost");

        return documentType;
    }

    /**
     * This method will determine which decision has been released to whom and also has it been signed
     * Because Provosts tenure can be either a decision or a recommendation it will be checked here
     *
     * @param action
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument findExecutiveAppealDecision(MivActionList action, Map<String,DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;
        boolean vpDecisionAdded = false;
        boolean pDecisionAdded = false;
        boolean pRecommendationAdded = false;
        boolean cDecisionAdded = false;
        boolean vpReleased = false;
        boolean pReleased = false;
        boolean cReleased = false;
        if (da.containsKey("chancellor_appeal_release") && da.get("chancellor_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            cReleased = true;
            cDecisionAdded = da.containsKey("chancellors_appeal_final_decision") &&
                    da.get("chancellors_appeal_final_decision").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(cDecisionAdded ? "Added" : "Not Added");
            documentType = MivDocument.CHANCELLORS_APPEAL_DECISION;
        }
        if ( da.containsKey("p_appeal_release") && da.get("p_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            pReleased = true;
            pDecisionAdded = (da.containsKey("provosts_appeal_final_decision") &&
                    da.get("provosts_appeal_final_decision").getAttributeValue() == DossierAttributeStatus.ADDED);
            action.setDecisionStatus(pDecisionAdded || cDecisionAdded ? "Added" : "Not Added");
            documentType = cReleased ? documentType : MivDocument.PROVOSTS_APPEAL_DECISION;
        }
        else if (da.containsKey("p_tenure_appeal_release") && da.get("p_tenure_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            pReleased = true;
            pDecisionAdded = (da.containsKey("provosts_tenure_appeal_decision") &&
                    da.get("provosts_tenure_appeal_decision").getAttributeValue() == DossierAttributeStatus.ADDED);
            pRecommendationAdded = (da.containsKey("provosts_tenure_appeal_recommendation") &&
                    da.get("provosts_tenure_appeal_recommendation").getAttributeValue() == DossierAttributeStatus.ADDED);
            action.setDecisionStatus(pDecisionAdded || pRecommendationAdded || cDecisionAdded ? "Added" : "Not Added");
            documentType = cReleased ? documentType :
                pDecisionAdded ? MivDocument.PROVOSTS_TENURE_APPEAL_DECISION :
                    pRecommendationAdded ? MivDocument.PROVOSTS_TENURE_APPEAL_RECOMMENDATION : MivDocument.NONE;
            action.setRecommendationOrDecision(pDecisionAdded ? "Decision" : pRecommendationAdded ? "Recommendation" : "tenure");
        }
        if (da.containsKey("vp_appeal_release") && da.get("vp_appeal_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            vpReleased = true;
            vpDecisionAdded = da.containsKey("viceprovosts_appeal_decision") &&
                    da.get("viceprovosts_appeal_decision").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(pDecisionAdded || cDecisionAdded || vpDecisionAdded ? "Added" : "Not Added");
            documentType = cReleased || pReleased ? documentType : MivDocument.VICEPROVOSTS_APPEAL_DECISION ;
        }

        action.setReleasedTo(createReleasedTo(cReleased, pReleased, vpReleased));
        action.setExecutiveFinalDecision(cDecisionAdded || pDecisionAdded || vpDecisionAdded);
        return documentType;
    }

    /**
     * This method will determine which appeal recommendation has been released and to whom and also has it been signed
     * with the exception of the provost tenure decision/recommendation which is handled in the findExecutiveAppealDecision method
     *
     * @param action
     * @param da
     * @return document type specific for this location and attributes
     */
    private MivDocument findExecutiveAppealRecommendation(MivActionList action, Map<String,DossierAttribute> da)
    {
        MivDocument documentType = MivDocument.NONE;
        boolean vpRecommendationAdded = false;
        boolean pRecommendationAdded = false;
        boolean vpReleased = false;
        boolean pReleased = false;
        if (da.containsKey("p_appeal_recommendation_release") && da.get("p_appeal_recommendation_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            pReleased = true;
            pRecommendationAdded = da.containsKey("provosts_appeal_recommendation") &&
                    da.get("provosts_appeal_recommendation").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(pRecommendationAdded ? "Added" : "Not Added");
            documentType = MivDocument.PROVOSTS_APPEAL_RECOMMENDATION;
        }
        if (da.containsKey("vp_appeal_recommendation_release") && da.get("vp_appeal_recommendation_release").getAttributeValue() == DossierAttributeStatus.RELEASE)
        {
            vpReleased = true;
            vpRecommendationAdded = da.containsKey("viceprovosts_appeal_recommendation") &&
                    da.get("viceprovosts_appeal_recommendation").getAttributeValue() == DossierAttributeStatus.ADDED;
            action.setDecisionStatus(pRecommendationAdded || vpRecommendationAdded ? "Added" : "Not Added");
            documentType = pReleased ? documentType : MivDocument.VICEPROVOSTS_APPEAL_RECOMMENDATION;
        }

        action.setReleasedTo(pReleased && vpReleased ? "Provost, Vice Provost" : pReleased ? "Provost" : "Vice Provost");

        return documentType;
    }

    /**
     * This method is a convenience method for creating a string for display on the page
     * It will contain Chancellor and/or Provost and/or Vice Provost depending upon what is released to whom
     *
     * @param cReleased
     * @param pReleased
     * @param vpReleased
     * @return A string with all executives that have this dossier released for signature
     */
    private String createReleasedTo(boolean cReleased, boolean pReleased, boolean vpReleased)
    {
        String releasedTo = "";
        if (cReleased)
        {
            releasedTo = "Chancellor";
        }
        if (pReleased)
        {
            releasedTo += " Provost";
        }
        if (vpReleased)
        {
            releasedTo += " Vice Provost";
        }
        return releasedTo;
    }

    /**
     * Find the match to nextstops in the list of next nodes and verify that the dossier prerequisites are completed at that node
     * @param dossier
     * @param nextstops the locations that this dossier can route to next (ignoring optional locations)
     * @return boolean indicating dossier has (or has not) met the prerequisites to move forward
     */
    private boolean isCompleted(Dossier dossier, DossierLocation...nextstops)
    {
        boolean completed = false;
        try
        {
            for (WorkflowNode node : dossierService.getNextWorkflowNodes(dossier))
            {
                for (DossierLocation nextloc : nextstops)
                {
                    if (nextloc == node.getNodeLocation())
                    {
                        completed = dossierService.validateWorkflowNodePrerequisites(dossier, node);
                        break;
                    }
                }
            }
        }
        catch (WorkflowException e)
        {
            logger.warn("isCompleted: got a WorkflowException doing getNextWorkflowNodes or validateWorkflowNodePrerequisites with dossierId = "
                    + String.valueOf(dossier.getDossierId())
                    + "   WorkflowExeception == " + e.getMessage());
        }
        return completed;
    }

}
