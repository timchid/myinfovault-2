/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DefaultDataTablesController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.datatables;

import java.beans.PropertyEditorSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.ScopeFilter;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.SimilarNameFilter;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handles DataTables JSON requests.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.5
 */
@RestController
public class DataTablesController
{
    private final UserService userService = MivServiceLocator.getUserService();
    private final DossierService dossierService = MivServiceLocator.getDossierService();

    /**
     * Converts JSON to {@link DataTablesRequest}.
     */
    public static class DataTablesRequestPropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException
        {
            setValue((new Gson()).fromJson(value, DataTablesRequest.class));
        }
    }

    /**
     * @param binder binder to which custom editors are registered
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        binder.registerCustomEditor(DataTablesRequest.class, new DataTablesRequestPropertyEditor());
    }

    /**
     * Expose the requester to the model.
     *
     * @param request servlet request
     * @return requester (shadow person)
     */
    @ModelAttribute("requester")
    private MivPerson getRequester(HttpServletRequest request)
    {
        return MIVSession.getSession(request).getUser().getTargetUserInfo().getPerson();
    }

    /**
     * Respond with JSON representing dossiers for the given request.
     *
     * @param dtRequest data tables request
     * @param request HTTP servlet request
     * @return data table JSON response
     */
    @RequestMapping(method=RequestMethod.GET, value="/DataTables/dossier")
    public Map<String,Object> getDossiers(@ModelAttribute("requester") final MivPerson requester,
                                    @RequestParam("command") DataTablesRequest dtRequest)
    {
        return getModel(dtRequest, new DataTablesResponse<Dossier>(dtRequest) {
            @Override
            protected List<Dossier> getRecords() {
                try
                {
                    return dossierService.getDossiers(requester);
                }
                catch (WorkflowException e)
                {
                    throw new MivSevereApplicationError("Unable to get active dossiers for " + requester, e);
                }
            }

            @Override
            protected String getKey() {
                return "dossier-" + requester.getUserId();
            }
        });
    }

    /**
     * Get MIV users for the data tables request.
     *
     * @param dtRequest data tables request
     * @return data table JSON response
     */
    @RequestMapping(method=RequestMethod.GET, value="/DataTables/user")
    public Map<String,Object> getUsers(@RequestParam("command") DataTablesRequest dtRequest)
    {
        return getModel(dtRequest, new DataTablesResponse<MivPerson>(dtRequest) {
            @Override
            protected List<MivPerson> getRecords() {
                return userService.findUsers(new AttributeSet());
            }

            @Override
            protected String getKey() {
                return "user";
            }
        });
    }

    /**
     * Get MivPerson list by name for the data tables request.
     *
     * @param dtRequest data tables request
     * @return data table JSON response
     */
    @RequestMapping(method=RequestMethod.GET, value="/DataTables/candidates")
    public Map<String,Object> getPersonsByName(@ModelAttribute("requester") final MivPerson requester,
                                         @RequestParam("command") DataTablesRequest dtRequest,
                                         @RequestParam("name") final String name,
                                         @RequestParam(value = "scoped",
                                                       defaultValue = "false") final Boolean scoped)
    {
        return getModel(dtRequest, new DataTablesResponse<MivPerson>(dtRequest) {
            @Override
            protected List<MivPerson> getRecords() {
                DefaultSearchStrategy strategy = new DefaultSearchStrategy();
                SearchCriteria criteria = new SearchCriteria();
                criteria.setInputName(name);

                Collection<Scope> scopes = new HashSet<Scope>();

                if (scoped) {
                    for (AssignedRole role : requester.getPrimaryRoles()) {
                        if (role.getRole().getCategory() == MivRole.Category.STAFF) {
                            scopes.add(role.getScope());
                        }
                    }
                }

                List<MivPerson> persons = strategy.getUsersByName(requester, criteria, !scoped ? null :
                    new ScopeFilter<MivPerson>(scopes) {
                        @Override
                        public Scope getScope(MivPerson item) {
                            return item.getPrimaryAppointment().getScope();
                        }
                    }
                );

                Iterator<MivPerson> peopleIt = persons.iterator();
                while (peopleIt.hasNext()) {
                    MivPerson person = peopleIt.next();
                    if (!person.hasRole(MivRole.CANDIDATE)) {
                        peopleIt.remove();
                    }
                }

                return persons;
            }

            @Override
            protected String getKey() {
                return "person-name-" + name + (scoped ? ("-" + requester.getUserId()) : "");
            }
        });
    }

    /**
     * Get MIV and LDAP persons by email address.
     *
     * @param dtRequest data tables request
     * @param role MIV person role
     * @return data table JSON response
     */
    @RequestMapping(method=RequestMethod.GET, value="/DataTables/user/email")
    public Map<String, Object> getUsers(@ModelAttribute("requester") final MivPerson requester,
                                 @RequestParam("command") DataTablesRequest dtRequest,
                                 @RequestParam("email") final String email)
    {
        return getModel(dtRequest, new AddUserResponse(dtRequest) {
            @Override
            protected List<MivPerson> getPeople() {
                return userService.findPersonByEmail(email);
            }

            @Override
            protected MivPerson getRequester() {
                return requester;
            }

            @Override
            protected String getKey() {
                //don't cache the response. The user info will change if they are added.
                return null;
            }
        });
    }

    /**
     * Get MIV persons of the given role {@link SimilarNameFilter fuzzily matching} the person of the given person UUID.
     *
     * @param requester person requesting the records
     * @param dtRequest data tables request
     * @param role MIV person role
     * @param email email address of the selected campus person against which fuzzy matches will be determined
     * @param personUUID person UUID of the person against which fuzzy matches will be determined
     * @return data table JSON response
     */
    @RequestMapping(method=RequestMethod.GET, value="/DataTables/user/decisions")
    public Map<String, Object> getUsers(@ModelAttribute("requester") final MivPerson requester,
                                 @RequestParam("command") DataTablesRequest dtRequest,
                                 @RequestParam("role") final MivRole role,
                                 @RequestParam("email") final String email,
                                 @RequestParam("personUUID") final String personUUID)
    {
        return getModel(dtRequest, new AddUserResponse(dtRequest) {
            @Override
            protected List<MivPerson> getPeople() {
                for (MivPerson targetPerson : userService.findPersonByEmail(email))
                {
                    // find target person against which users of the given role is compared
                    if (targetPerson.getPersonId().equals(personUUID))
                    {
                        return userService.getUsersByRole(role,
                                                          Scope.AnyScope,
                                                          new SimilarNameFilter(targetPerson));
                    }
                }

                return Collections.emptyList();
            }

            @Override
            protected MivPerson getRequester() {
                return requester;
            }

            @Override
            protected String getKey() {
                return "user-decisions-" + requester.getUserId() + "-" + role + "-" + personUUID;
            }
        });
    }

    /**
     * Compose a model and view for the given data tables response.
     *
     * @param request data tables request
     * @param response data tables response
     * @return data table JSON response
     */
    private Map<String,Object> getModel(DataTablesRequest request, DataTablesResponse<?> response)
    {
        Map<String, Object> model = new HashMap<String,Object>();

        model.put("draw", request.getDraw());
        model.put("recordsTotal", response.getRecordsTotal());
        model.put("recordsFiltered", response.getRecordsFiltered());
        model.put("data", response.getData());
        model.put("error", response.getError());

        return model;
    }
}
