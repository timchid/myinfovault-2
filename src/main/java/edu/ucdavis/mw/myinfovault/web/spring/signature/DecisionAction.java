/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Webflow form action for dean and executive decision lists.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 */
public class DecisionAction extends MivFormAction
{
    // request scope parameter key
    private static final String APPOINTMENT_SIGNATURES_PARAM = "appointmentSignatures";
    private static final String ALL_OTHERS_SIGNATURES_PARAM = "otherSignatures";

    // denied log pattern
    private static final String NOT_PERMITTED = "Target user {0} is not permitted to view decisions in the {1} capacity";

    private final SigningService signingService;
    private final DossierService dossierService;

    /**
     * Capacity in which decisions are to be made.
     */
    private MivRole capacity = null;

    /**
     * Create the decision action form bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param signingService Spring injected signature service
     * @param dossierService Spring injected dossier service
     */
    public DecisionAction(UserService userService,
                          AuthorizationService authorizationService,
                          SigningService signingService,
                          DossierService dossierService)
    {
        super(userService, authorizationService);

        this.signingService = signingService;
        this.dossierService = dossierService;
    }

    /**
     * Set the capacity in which decisions are to be made (i.e. the role required for these decisions).
     *
     * @param capacity role capacity
     * @return role capacity
     */
    public MivRole setCapacity(String capacity)
    {
        this.capacity = MivRole.valueOf(capacity);

        return this.capacity;
    }

    /**
     * Check if target person is permitted to view decisions.
     *
     * @param context Webflow request context
     * @return Web flow event status
     */
    public Event hasPermission(RequestContext context)
    {
        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.DECISION_TYPE, capacity.name());

        // is permitted to view decisions within this capacity?
        return authorizationService.hasPermission(getShadowPerson(context),
                                                  Permission.VIEW_DECISION,
                                                  permissionDetails)

             ? allowed()
             : denied(NOT_PERMITTED,
                      getShadowPerson(context),
                      capacity);
    }

    /**
     * Put list of requested signatures into the request scope for use in the JSP.
     *
     * @param context Web flow request context
     */
    public void loadList(RequestContext context)
    {
        List<MivElectronicSignature> requested = getRequestedSignatures(getShadowPerson(context));

        SearchFilterAdapter<MivElectronicSignature> filter = new ActionTypeFilter(DossierActionType.APPOINTMENT,
                                                                                  DossierActionType.APPOINTMENT_INITIAL_CONTINUING);

        context.getRequestScope().put(APPOINTMENT_SIGNATURES_PARAM, filter.apply(requested));
        context.getRequestScope().put(ALL_OTHERS_SIGNATURES_PARAM, filter.not().apply(requested));
    }

    /**
     * Get the requested signatures for the given user in the given capacity.
     *
     * @param shadowPerson person attempting to retrieve the requests
     * @return requested signatures
     */
    private List<MivElectronicSignature> getRequestedSignatures(MivPerson shadowPerson)
    {
        MivPerson executiveUser = shadowPerson;

        /*
         * For OCP staff, get the executive user from the user service.
         */
        if (shadowPerson.hasRole(MivRole.OCP_STAFF))
        {
            try
            {
                executiveUser = userService.getUsers(capacity).iterator().next();
            }
            catch (NoSuchElementException e)
            {
                // no requests for non-existent executive
                return Collections.emptyList();
            }
        }

        return signingService.getRequestedSignatures(executiveUser, capacity);
    }

    /**
     * Add decision list flow bread crumbs.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumb(RequestContext context)
    {
        addBreadcrumbs(context, capacity.getDescription() + " Decision/Recommendations");
    }

    /**
     * Get the dossier and load all data.
     *
     * @param dossierId
     * @return loaded dossier
     */
    public Dossier getDossier(long dossierId)
    {

        try
        {
            return dossierService.getDossierAndLoadAllData(dossierId);
        }
        catch (WorkflowException e)
        {
            throw new MivSevereApplicationError("errors.dossier", e, dossierId);
        }
    }
}
