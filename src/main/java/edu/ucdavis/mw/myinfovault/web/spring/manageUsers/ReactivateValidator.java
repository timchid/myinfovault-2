/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReactivateValidator.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;

/**
 * Validates the manage users add form
 * @author Leo Golubev
 * @since MIV 3.0
 */
public class ReactivateValidator implements Validator
{
    @Override
    public boolean supports(Class clazz)
    {
        return SearchCriteria.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors)
    {
        SearchCriteria scForm = (SearchCriteria) obj;

        //input
        if ( StringUtils.isBlank(scForm.getLname()) && StringUtils.isBlank(scForm.getLastName()) &&
             StringUtils.isBlank(scForm.getFirstName()) && StringUtils.isBlank(""+scForm.getDepartmentId()) &&
             StringUtils.isBlank(""+scForm.getDeptSchoolId()) && StringUtils.isBlank(""+scForm.getSchoolId()) )
        {
            errors.reject(null, "Input is required");
        }
    } //end validate
}
