package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

/**
 * Base Command Object
 * @author dreddy
 * @since MIV 2.1
 */
public class BaseCommand
{
    private String id;
    private String userID;

    public BaseCommand(){};
    public BaseCommand(int id, int userId)
    {
        this.id = Integer.toString(id);
        this.userID = Integer.toString(userId);
    }

    /**
     * @return
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return
     */
    public String getUserID()
    {
        return userID;
    }

    /**
     * @param userID
     */
    public void setUserID(String userID)
    {
        this.userID = userID;
    }
}
