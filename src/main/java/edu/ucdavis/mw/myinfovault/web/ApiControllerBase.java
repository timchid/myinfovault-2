/**
 *
 */
package edu.ucdavis.mw.myinfovault.web;

import javax.security.auth.login.CredentialException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author japorito
 * @since 5.0
 */
@RestController
public abstract class ApiControllerBase
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler
    public ResponseEntity<?> genericExceptionHandler(Exception e)
    {
        logger.error("Internal Server Error", e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<?> authFailureHandler(CredentialException e, HttpServletRequest req)
    {
        logger.error("Authentication Failure with header: " + req.getHeader("Authorization"), e);
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler
    public ResponseEntity<?> handleUnreadableMessage(HttpMessageNotReadableException e) {
        logger.error("Returning HTTP 400 Bad Request", e);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
