/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchCriteria.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;

import org.springframework.webflow.execution.Event;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * This class/formObject is used by all of the search pages to collect the criteria entered for a search.
 * Most things are self explanatory BUT the deptSchool field... it is the school used in a search by department
 * since a department id is meaningless without a school id.
 *
 * @author Mary Northup
 * @since MIV 3.0
 */
public class SearchCriteria implements Serializable
{
    private static final long serialVersionUID = 200903181723L;//-1737933765408912603L;

    private static final String DEPARTMENT_DELIMITER = ":";
    private static final String TOSTRING_FORMAT = "SearchCriteria [firstName=%s, lastName=%s, inputName=%s, lname=%s, deptSchool=%s, department=%s, school=%s, activeOnly=%s, role=%s]";

    private String firstName = StringUtil.EMPTY_STRING;
    private String lastName = StringUtil.EMPTY_STRING;
    private String inputName = StringUtil.EMPTY_STRING;
    private String lname = StringUtil.EMPTY_STRING;

    private SearchMethod method = SearchMethod.SEARCHNAME;

    private int department = -1;

    private int deptSchool = -1;   // this is the school used when searching by department
    private int school = -1;

//    private String roleType = ""; // to View MIV Users by MivRole
    private MivRole role = null;

    private boolean activeOnly = true;

    /**
     * @return given name search term
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     * @param firstName given name search term
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName.trim();
    }

    /**
     * @return surname search term
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * @param lastName surname search term
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName.trim();
    }

    /**
     * @return given and/or surname search
     */
    public String getInputName()
    {
        return inputName;
    }

    /**
     * @param inputName given and/or surname search
     */
    public void setInputName(String inputName)
    {
        this.inputName = inputName.trim();
    }

    /**
     * @return method to be used for search
     */
    public SearchMethod getMethod()
    {
        return method;
    }

    /**
     * Set the last WebFlow event as the method to be used for search.
     * 
     * @param lastEvent latest WebFlow event 
     */
    public void setMethod(Event lastEvent)
    {
        try
        {
            //use default if last event was null
            this.method = lastEvent != null
                        ? SearchMethod.valueOf(lastEvent.getId().toUpperCase())
                        : SearchMethod.SEARCHNAME;
        }
        catch(IllegalArgumentException  e)
        {
            //use default search method
            this.method = SearchMethod.SEARCHNAME;
        }
    }

    /**
     * @return department ID search criteria
     */
    public int getDepartmentId()
    {
        return department;
    }

    /**
     * @return school ID for a given department ID search criteria
     */
    public int getDeptSchoolId()
    {
        return deptSchool;
    }

    /**
     * Sets the school and department search values.
     * Requires that the String passed in be of the form {@code S:D} where
     * S is a school ID number and D is a department ID number.
     * 
     * @param department a department within a school
     */
    public void setDepartment(String department)
    {
        if (department == null || department.length() == 0)
        {
            this.department = 0;
            this.deptSchool = 0;
        }
        else
        {
            String [] myDept = department.split(DEPARTMENT_DELIMITER);  /* the page gives schoolid:departmentid */
            this.deptSchool = Integer.parseInt(myDept[0]);
            this.department = Integer.parseInt(myDept[1]);
        }
    }

    /**
     * @return school and department search values
     */
    public String getDepartment()
    {
        return deptSchool + DEPARTMENT_DELIMITER + department;
    }

    /**
     * This can't be named {@code setDepartment(int)} because it confuses Spring injection from webflow.
     * 
     * @param department 
     */
    public void setDepartmentInt(int department)
    {
        this.department = department;
    }

    /**
     * @return school ID search criteria
     */
    public int getSchoolId()
    {
        return school;
    }

    /**
     * @param school school search parameter
     */
    public void setSchool(String school)
    {
        if (school == null || school.length() == 0)
        {
            this.school = 0;
        }
        else
        {
            this.school = Integer.parseInt(school);
        }
    }

    /**
     * @return school search parameter
     */
    public String getSchool()
    {
        return Integer.toString(school);
    }

    /**
     * This can't be named {@code setSchool(int)} because it confuses Spring injection from webflow.
     * @param school
     */
    public void setSchoolInt(int school)
    {
        this.school = this.deptSchool = school; // MIV-3831 : assign to both school and deptSchool
    }

    /**
     * @return surname initial search criteria 
     */
    public String getLname()
    {
        return lname;
    }
    
    /**
     * @param lname surname initial search criteria
     */
    public void setLname(String lname)
    {
        this.lname = lname.trim();
    }

    /**
     * @return if search limited to active users only
     */
    public boolean isActiveOnly()
    {
        return this.activeOnly;
    }

    /**
     * @param active if search limited to active users only
     */
    public void setActiveOnly(boolean active)
    {
        this.activeOnly = active;
    }

//    /**
//     * FIXME: This should return an MivRole enum, not a String
//     * @return the roleType
//     * @deprecated use {@link #getRole()} instead
//     */
//    @Deprecated
//    public String getRoleType()
//    {
//        return this.roleType;
//    }

//    /**
//     * FIXME: This should accept a String, but use valueOf() to immediately turn it into an MivRole before storing it.
//     * FIXME: There should be a second setRoleType(MivRole role) method that accepts an MivRole instead of a String,
//     * and <em>that</em> should be the preferred way of using this.
//     * @param roleType the roleType to set
//     * @deprecated use {@link #setRole(MivRole)} instead
//     */
//    @Deprecated
//    public void setRoleType(String roleType)
//    {
//        @SuppressWarnings("unused")
//        MivRole role = null;
//        if (StringUtils.hasText(roleType))
//        {
//            try {
//                role = MivRole.valueOf(roleType.toUpperCase().trim());
//            }
//            catch (IllegalArgumentException e) {
//                System.err.println("Failed to convert [" + roleType + "] to an MivRole in SearchCriteria");
//            }
//        }
//
//        this.roleType = roleType;
//    }

    /**
     * @param role MIV role search criteria
     * @return self reference for chaining
     */
    public SearchCriteria setRole(MivRole role)
    {
        this.role = role;
//        this.roleType = role != null ? role.toString() : "";
        return this;
    }

    /**
     * @return MIV role search criteria
     */
    public MivRole getRole()
    {
        return this.role;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format(TOSTRING_FORMAT,
                             firstName, lastName, inputName,
                             lname, deptSchool, department,
                             school, activeOnly, role);
    }
}
