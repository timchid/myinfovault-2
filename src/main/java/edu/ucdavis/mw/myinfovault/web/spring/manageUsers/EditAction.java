/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;


import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CANDIDATE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CHANCELLOR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.MIV_USER;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.OCP_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.myinfovault.ConfigReloadConstants.ALL;
import static edu.ucdavis.myinfovault.ConfigReloadConstants.PROPERTIES;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.Errors;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.collect.Sets;
import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events2.DossierCancelEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.audit.AuditService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.ConvertPersonXml;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.raf.RafService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.SimpleAttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivPropertyEditors;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchFormAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MivEntity;
import edu.ucdavis.myinfovault.audit.AuditAction;
import edu.ucdavis.myinfovault.audit.AuditObject;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;
import edu.ucdavis.myinfovault.message.AuditMessages;


/**
 * Webflow form action for editing user accounts.
 *
 * @author Craig Gilmore
 * @since 3.0
 */
public class EditAction extends SearchFormAction
{
    /**
     * All appointment scopes including 'no scope' ordered as defined by {@link AppointmentScope#compareTo(AppointmentScope)}.
     */
    public static final Set<AppointmentScope> ALL_APPOINTMENT_SCOPES = new TreeSet<AppointmentScope>();
    static {
        EditAction.loadScopes(null);
    }

    /** Roles that may have only one assignment. */
    private static final Set<MivRole> SINGLE_ASSIGNMENT_ROLES = EnumSet.of(SCHOOL_STAFF, SENATE_STAFF, OCP_STAFF);

    /** Track instances so we only register with the EventDispatcher once. */
    private static int instCount = 0;

    private final DossierService dossierService;
    private final RafService rafService;
    private final DCService dcService;
    private AuditService auditService = null;


    /**
     * Create edit form action bean.
     *
     * @param searchStrategy Spring-injected search strategy
     * @param userService Spring-injected user service
     * @param authService Spring-injected authorization service
     * @param dossierService Spring-injected dossier service
     * @param rafService Spring-injected recommended action form service
     * @param dcService Spring-injected disclosure certificate service
     */
    public EditAction(SearchStrategy searchStrategy,
                      UserService userService,
                      AuthorizationService authService,
                      DossierService dossierService,
                      RafService rafService,
                      DCService dcService)
    {
        super(searchStrategy, authService, userService);

        this.dossierService = dossierService;
        this.rafService = rafService;
        this.dcService = dcService;

        if (instCount++ == 0) {         // Register only once
            EventDispatcher.getDispatcher().register(this);
        }
        else {
            logger./*debug*/info("Skipping event registration on instance #{} of EditAction", instCount - 1);
        }

        logger./*debug*/info("Created instance #{} of {}", instCount, this.className);
        //logger.debug("Call chain to create EditAction ...", new Exception("TRACEBACK"));
    }


    /**
     * @param auditService Spring injected (optional) audit service
     */
    public void setAuditService(AuditService auditService)
    {
        logger.debug("\n  SET AuditService in EditAction : service has been set");
        logger.debug("    ACTUAL CLASS is: " + this.className + "\n");
        this.auditService = auditService;
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Object createFormObject(RequestContext context)
    {
        MivPerson targetPerson = (MivPerson)context.getFlowScope().get("targetPerson");

        /*
         * Get the target person from the URL parameter 'id'
         * if not mapped to the flow via 'targetPerson'.
         */
        if (targetPerson == null)
        {
            targetPerson = userService.getPersonByMivId(getParameterValue(context, "id", 0));
        }

        return new EditUserForm(getShadowPerson(context), targetPerson);
    }


    /**
     * Update and existing MIV person.
     *
     * @param context WebFlow request context
     * @return WebFlow event status; success, error, or denied
     */
    public Event updatePerson(RequestContext context)
    {
        MivPerson shadowPerson = getShadowPerson(context);
        MivPerson realPerson = getRealPerson(context);

        // not allowed to edit person
        if (!context.getFlowScope().getBoolean("canEdit"))
        {
            // must be editing self
            return updateEmail(context, shadowPerson.getUserId());
        };

        EditUserForm editForm = (EditUserForm) getFormObject(context);

        MivPerson editedPerson = editForm.getEditPerson();

        // Get the previous appointments, assignments, and role for in process check later
        Set<Appointment> originalAppointments = new HashSet<Appointment>(editedPerson.getAppointments());
        List<AssignedRole> originalAssignments = new ArrayList<AssignedRole>(editedPerson.getAssignedRoles());

        // Do the update
        Event result = updatePersonFromForm(realPerson, shadowPerson, editedPerson, editForm, getFormErrors(context));

        resolveDeansAuthority(editedPerson, originalAssignments);

        // MIV-3312
        // If dossier is in process at the Packet Request node, we will need to remove all uploaded documents
        // and recommended action form for any appointment which may be removed
        // MIV-3369
        // Also remove letters/decision added at the Vice Provost since they reference the now removed Appointment
        Dossier dossier = getDossierInProcess(editedPerson);
        if (dossier != null && dossier.getDossierLocation() == DossierLocation.PACKETREQUEST)
        {
            // if the Candidate has a dossier and any appointments changed, cancel the dossier
            HashSet<Appointment> currentAppointments = new HashSet<Appointment>(editedPerson.getAppointments());
            if (editedPerson.getPrimaryRoleType() == CANDIDATE
              && !CollectionUtils.isEqualCollection(originalAppointments, currentAppointments))
            {
                cancelDossier(dossier, editedPerson);
            }
            else
            {
                removeUploadsReviewersAndRaf(dossier,
                                             originalAppointments,
                                             currentAppointments,
                                             getRealPerson(context));
            }
        }

        // is this an update as opposed to adding a new user?
        boolean isUpdate = editedPerson.getUserId() > 0;

        // save existing person
        if (isUpdate)
        {
            if (userService.savePerson(editedPerson, shadowPerson.getUserId()) == null)
            {
                throw new MivSevereApplicationError("errors.manageusers.saveperson", editedPerson);
            }
        }
        // otherwise, add new person
        else
        {
            if (!userService.addPerson(editedPerson))
            {
                throw new MivSevereApplicationError("errors.manageusers.addperson", editedPerson);
            }
        }

        logger.info("_AUDIT:\naction: User " + (isUpdate ? "Edit" : "Add") + "\nReal:{}\nTarget={}", realPerson, editedPerson);

        createAuditTrail(!isUpdate, editedPerson, realPerson);

        return result;
    }


    /**
     * Interpret the data entered in the Edit User form and modify the Person being edited.
     *
     * @param realPerson the real, logged-in person
     * @param shadowPerson the MivPerson doing the editing
     * @param person the MivPerson being edited
     * @param editForm the form data received from the submit
     * @param errors form Errors
     */
    private Event updatePersonFromForm(MivPerson realPerson, MivPerson shadowPerson, MivPerson person, EditUserForm editForm, Errors errors)
    {
        Event result = success();

        // Collect the relevant form data
        MivRole newRole = editForm.getSelectedRole();

        // IF (editForm.getOriginalRole().getCategory() != newRole.getCategory())
        // THEN THERE'S A faculty <-> staff CHANGE
        // so we may want to do more here (?) re: the discussion about losing all
        // assignments when changing someone between faculty and staff positions.

        // Collect the selections made for each assignment line
        LinkedHashMap<Scope, AssignmentLine>lineMap = new LinkedHashMap<>();

        // Add the home department, stored separately, into the rest of the assignment lines.
        lineMap.put(editForm.getHomeDepartment().getScope(), editForm.getHomeDepartment());

        Set<AssignedRole> currentRoles = person.getAssignedRoles();

        // Add the rest of the lines, skipping any that are set to "None"
        List<AssignmentLine> formLines = editForm.getFormLines();
        Set<AppointmentScope> deptSet = getDeptSet(shadowPerson);
        for (AssignmentLine al : formLines)
        {
            if (al.getScope().equals(Scope.None) || lineMap.containsKey(al.getScope())) continue;  // We won't save "None" or duplicate lines.

            // Drop duplicates or new staff assignments which are outside of the editor's scope.
            if (newRole.getCategory() != MivRole.Category.FACULTY &&//Don't drop any for faculty
                !currentRoles.contains(new AssignedRole(newRole, al.getScope(), true)) &&//don't drop any that the user previously had
                !deptSet.contains(new AppointmentScope(al.getScope()))) {//don't drop unless it is outside of the editor's scope
                continue;
            }
            lineMap.put(al.getScope(), al);
        }

        // Put the results back into the form backing object so the results page sees the right thing.
        List<AssignmentLine> lines = new ArrayList<AssignmentLine>(lineMap.values());
        formLines.clear();
        formLines.addAll(lines);

        // Set person email address
        person.setPreferredEmail(editForm.getSelectedEmail());

        // Deal with the collected Assignment Lines - either Staff style or Faculty style
        if (newRole.getCategory() == MivRole.Category.FACULTY) {
            result = updateLinesFaculty(realPerson, shadowPerson, (MivPersonInternal)person, newRole, lines, errors);
        }
        else {
            result = updateLinesStaff((MivPersonInternal)person, newRole, lines, shadowPerson);
        }
// denied() creates a new Event object and would never be equal to 'result'
//        if (result.equals(denied())) {
//            // what (else) to do?
//            // do we even want to do this?
//            logger.warn("Permission Denied for " + shadowPerson + " editing " + person);
//        }

        return result;
    }


    /**
     * Interpret the assignment selection lines for any {@link MivRole.Category#FACULTY FACULTY} role.
     *
     * @param realPerson the real, logged-in person
     * @param shadowPerson the Person doing the editing
     * @param person the Person being edited
     * @param role the MivRole the edited person is being assigned
     * @param lines all of the AssignmentLines from the {@link EditUserForm}
     */
    private Event updateLinesFaculty(MivPerson realPerson, MivPerson shadowPerson, MivPersonInternal person, MivRole role, List<AssignmentLine> lines, Errors errors)
    {
        // Don't update roles if this is an executive!
        List<MivRole> executiveRoles = getExecutiveRoles(person);

        // Start with a clean slate of appointments and assignments
        person.clearAppointments();
        person.clearAssignments();

        // If this is an executive then give them the role(s) back
        for (MivRole exec : executiveRoles)
        {
            person.addRole(new AssignedRole(exec));
        }

        Appointment firstAppointment = null;
        AssignedRole firstRole = null;

        // Force the first line -- the Primary Appointment line -- to be marked as Candidate
        // regardless of the checkbox state.
        // Doing this counts on the fact that "updatePersonFromForm" (the only caller of this
        // method) puts the Primary Appointment line as element zero of the "lines" list.
        // This really isn't necessary, because Spring binding preserves the 'checked' checkbox
        // state of the primary appointment even if it's tampered with because Spring knows
        // the checkbox was set to 'disabled'. We're just not going to count on that because
        // this is a business rule.
        lines.get(0).setCandidate(true);

        // Handle each assignment line in turn
        for (AssignmentLine al : lines)
        {
            // If this line was set to "None" it will be dropped (this shouldn't happen, the caller already dropped 'None' lines.
            if (al.getScope().equals(Scope.None))
            {
                logger.trace("Dropping Appointment with no scope (" + al.getScope() + ")");
                continue;
            }

            // The "Candidate" checkbox was checked - save as an appointment
            if (al.getCandidate())
            {
                Appointment app = new Appointment(al.getScope(), false);
                // If this is the FIRST candidate line it's the primary appointment
                if (firstAppointment == null)
                {
                    app.setPrimary(true);
                    firstRole = new AssignedRole(CANDIDATE, al.getScope(), true);
                    firstAppointment = app;
                    person.addRole(new AssignedRole(MIV_USER, al.getScope(), false));
                }
                person.addAppointment(app);
            }

            // The "Dept. Chair" checkbox was checked - assign it as a role in the selected scope
            if (al.getDeptChair())
            {
                AttributeSet qualifier = new SimpleAttributeSet(Qualifier.USERID + "=" + String.valueOf(shadowPerson.getUserId()));
                if (! authorizationService.isAuthorized(shadowPerson, Permission.ASSIGN_DEPT_CHAIR, null, qualifier))
                {
                    errors.reject(shadowPerson.getDisplayName() + " is not authorized to assign people to be a Department Chair.");
                    return denied();
                }

                AssignedRole ar = new AssignedRole(DEPT_CHAIR, al.getScope());
                logger.info(shadowPerson.getLoggingName() + ": Assigning DEPT_CHAIR with Scope:" + al.getScope() + "("+ar+")");
                person.addRole(ar);
            }

            // The "Dean" checkbox was checked - make this person a Dean in the selected school
            if (al.getDean())
            {
                // add real, logged-in person user ID to permission detail
                AttributeSet permissionDetails = new AttributeSet();
                permissionDetails.put(PermissionDetail.ACTOR_ID, Integer.toString(realPerson.getUserId()));

                AttributeSet qualifier = new SimpleAttributeSet(Qualifier.USERID + "=" + String.valueOf(person.getUserId()));
                if (! authorizationService.isAuthorized(shadowPerson, Permission.ASSIGN_DEAN, permissionDetails, qualifier))
                {
                    errors.reject(shadowPerson.getDisplayName() + " is not authorized to assign people to be a Dean.");
                    return denied();
                }

                AssignedRole ar = new AssignedRole(DEAN, al.getScope());
                logger.info("Assigning {} to {}", shadowPerson, ar);
                person.addRole(ar);
            }

            /*
             * At this point it's possible that none of the above "al.isXyz" statements
             * were executed.  That's OK, it means none of the checkboxes were checked
             * so the current line isn't being used for any appointment or assignment.
             * We'll just end up dropping it; the same as if "None" were picked for Scope.
             */
        }

        if (firstAppointment == null)
        {
            AssignmentLine al = lines.get(0);
            firstAppointment = new Appointment(al.getScope(), true);
            firstRole = new AssignedRole(CANDIDATE, al.getScope(), true);
            person.addAppointment(firstAppointment);
            // person.setPrimaryRole is done below
        }

        person.setPrimaryRole(firstRole); // setPrimaryRole is still pretty messy in MivPersonImpl
        logger.debug("Done with updateLinesFaculty");

        return success();
    }


    /**
     * Interpret the assignment selection lines for any
     * {@link edu.ucdavis.mw.myinfovault.service.person.MivRole.Category#STAFF Category.STAFF} role.
     * @param person the Person being edited
     * @param role the MivRole the edited person is being assigned
     * @param lines all of the AssignmentLines from the {@link EditUserForm}
     * @param shadowPerson the Person doing the editing
     */
    private Event updateLinesStaff(MivPersonInternal person, MivRole role, List<AssignmentLine> lines, MivPerson shadowPerson)
    {
        Event result = success();

        // Staff should not have Appointments
        person.clearAppointments();
        // Start with a clean slate of assignments
        person.clearAssignments();

        AssignedRole firstRole = null;

        for (AssignmentLine al : lines)
        {
            // If this line was set to "None" it will be dropped (this shouldn't happen, the caller already dropped 'None' lines.
            if (al.getScope().equals(Scope.None))
            {
                // This line was set to "None" so it will be dropped
                logger.info("Dropping Assignment with no scope ({})", al.getScope());
                continue;
            }

            AssignedRole ar = new AssignedRole(role, al.getScope(), true);
            if (firstRole == null)
            {
                firstRole = ar;
                person.addRole(new AssignedRole(MIV_USER, al.getScope(), false));
            }
            person.addRole(ar);

            if (SINGLE_ASSIGNMENT_ROLES.contains(role)) break; // only allow one line to be assigned to certain roles.
        }

        person.setPrimaryRole(firstRole); // don't do this.??
        logger.debug("Done with updateLinesStaff");

        return result;
    }


    /**
     * TODO: javadoc
     *
     * @param context
     * @return
     */
    public Event updateEmail(RequestContext context, int actorId)
    {
        System.out.println("  UPDATE  EMAIL  WAS  CALLED  ");

        EditUserForm editForm = (EditUserForm) getFormObject(context);
        MivPerson person = editForm.getEditPerson();

        // Set person email address
        person.setPreferredEmail(editForm.getSelectedEmail());

        // Persist person via user service
        if (this.userService.savePerson(person, actorId) == null)
        {
            throw new MivSevereApplicationError("errors.manageusers.saveperson", person);
        }

        return success();
    }


    /**
     * Called when the "Add A(ppoint|ssign)ment" button is pressed on the edit form.
     *
     * @param  context The webflow request context
     * @return a webflow event; success (this can never fail)
     */
    public Event addAssignmentLine(RequestContext context)
    {
        ((EditUserForm) getFormObject(context)).addFormLine();

        return success();
    }


    /**
     * Called when an assignment line "Delete" button is pressed on the edit form.
     * @param  context The webflow request context
     * @return a webflow event; success, error, or denied
     */
    public Event deleteAssignmentLine(RequestContext context)
    {
        String eventName = context.getCurrentEvent().getId();
        if (eventName.matches("delete[0-9][0-9]*"))
        {
            String deleteAssignment = eventName.replace("delete", "");
            int assignmentIndex = Integer.parseInt(deleteAssignment);
            EditUserForm editForm = (EditUserForm) getFormObject(context);
            AssignmentLine al = editForm.getFormLines().get(assignmentIndex);
            System.out.println("Deleting line " + (1+assignmentIndex) + " with scope " + al.getScope());

            // TODO: check here that user is authorized to delete this line, return denied() if not.
            //       could use the MODIFY_ASSIGNMENT permission check.
            editForm.getFormLines().remove(assignmentIndex);

            return success();
        }

        return error();
    }


//    /**
//     * THIS HAS BEEN REWRITTEN (see below) BUT IS BEING KEPT AROUND FOR A WHILE
//     * TO COMPARE WITH THE NEW ONE IN CASE 4.0 BEHAVES DIFFERENTLY IN THESE CASES.
//     *
//     * FIX-ME: Rewrite resolveDeansAuthority() in EditAction.java
//     * Refactored out of and used only by saveFormObject
//     * @param mivPerson
//     * @deprecated Replaced by {@link #resolveDeansAuthority(MivPerson, Collection)}
//     */
//    @Deprecated
//    private void resolveDeansAuthority(MivPerson mivPerson)
//    {
//// X X X:  Do we have to do anything here?  We already removed all Appointments and All assignments before we started
//// X X X:  ...but we DO still need to remove any old signature requests if any are outstanding
//
//        // MIV-3354
//        // If this user has deans authorization and the primary school has changed,
//        // or the user no longer has any of the roles which can be authorized, remove the deans
//        // authorization along with any outstanding signature requests.
//
//        // See if this user has deans signature authorization
//        boolean hasDeansSignatureAuthorization = false;
//
//        if (mivPerson.hasRole(SCHOOL_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN))
//        {
//            List<MivPerson> people = this.userService.getUsersByRole(DEAN, null);
//
//            if (people.contains(mivPerson))
//            {
//                hasDeansSignatureAuthorization = true;
//            }
//        }
//
//// X X X: How did this "previous" ever work?
//        Appointment previousPrimaryAppointment = mivPerson.getPrimaryAppointment();
//// It's fetched from person.getPrimaryAppointment() then compared to person.getPrimaryAppointment()
//// with no intervening changes. The comparison would always be true.
//        if (hasDeansSignatureAuthorization &&
//            (mivPerson.getPrimaryAppointment().getSchool() != previousPrimaryAppointment.getSchool() ||
//             !mivPerson.hasRole(SCHOOL_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN)))
//        {
//            // Remove Dean signature authorization for this user.
//            mivPerson.removeRole(DEAN);
//
//            // Get any outstanding signature requests.
//            SigningService signingService = MivServiceLocator.getSigningService();
//            List<MivElectronicSignature> signatureRequests = signingService.getRequestedSignaturesByUser(mivPerson.getUserId());
//
//            // Remove those which are unsigned.
//            for (MivElectronicSignature electronicSignature : signatureRequests)
//            {
//                if (!electronicSignature.isSigned())
//                {
//                    signingService.delete(electronicSignature);
//                }
//            }
//        }
//    }

// The new one....
    /**
     *
     * @param person
     * @param originalAssignments
     */
    private void resolveDeansAuthority(MivPerson person, Collection<AssignedRole> originalAssignments)
    {
        /*
         * Find any DEAN roles the person originally had. If there are none we're done.
         */
        List<AssignedRole> wasDean = new ArrayList<AssignedRole>();
        for (AssignedRole ar : originalAssignments)
        {
            if (ar.getRole() == DEAN) {
                wasDean.add(ar);
            }
        }

        // Done if none were found
        if (wasDean.size() == 0) return;

// FIXME: You can't be the Dean twice in the same school, and this is treating each DEAN school+dept as separate.
// DECISION: Since only MIV Admin (VICE_PROVOST_STAFF) users assign Deans we can be less than perfect on the UI (only a few people to train)

        /*
         * Find any DEAN roles the person used to have that they no longer have.
         */
        Collection<AssignedRole> currentAssignments = person.getAssignedRoles();
        Map<Scope,AssignedRole> lostAssignments = new HashMap<Scope,AssignedRole>();
        for (AssignedRole dean : wasDean)
        {
            boolean found = false;
            for (AssignedRole ar : currentAssignments)
            {
                if (ar.getRole() == DEAN && ar.equals(dean)) {
                    found = true;
                    break;
                }
            }
            // The old Dean role we were looking for wasn't found - the role was lost
            if (!found) {
                lostAssignments.put(dean.getScope(), dean);
            }
        }

        // Done if no DEAN assignments were lost
        if (lostAssignments.size() == 0) return;

        /*
         * Get any outstanding (unsigned) signature requests for the lost assignments and remove them.
         */
        SigningService signingService = MivServiceLocator.getSigningService();
        List<MivElectronicSignature> signatureRequests = signingService.getRequestedSignaturesByUser(person.getUserId());
        for (MivElectronicSignature sig : signatureRequests)
        {
            // if (sig.isSigned()) continue; // getRequestedSignaturesByUser() only returns signed==false signatures
            // MIV-4016 A signature is requested of a person with school-department but a DEAN is
            //          scoped just to a school. We need to use Scope.ANY for the deparment since
            //          we're using standard "containsKey" and not using Scope.matches()
//          Scope sigScope = new Scope(sig.getSchoolId(), sig.getDepartmentId());
            Scope sigScope = new Scope(sig.getDocument().getSchoolId(), Scope.ANY);
            if (lostAssignments.containsKey(sigScope)) {
                signingService.deleteSignature(sig);
            }
        }
    }


    /**
     *
     * @param shadowPerson
     * @param editPerson
     * @return
     */
    public boolean canEdit(MivPerson shadowPerson, MivPerson editPerson)
    {
        AttributeSet qualification = new AttributeSet();
        // TODO: Do I want to list the individual pieces, SCHOOL, DEPARTMENT, and ROLE, or the whole Person/PRINICIPAL ?
        //scope.put(Qualifier.SCHOOL, mivPerson.getPrimaryAppointment().getSchool()+"");
        //scope.put(Qualifier.DEPARTMENT, mivPerson.getPrimaryAppointment().getDepartment()+"");
        //scope.put(Qualifier.ROLE, mivPerson.getPrimaryRole().toString());
        qualification.put(Qualifier.USERID, String.valueOf(editPerson.getUserId()));

        // allowed to edit the qualified user?
        boolean canEdit = authorizationService.isAuthorized(shadowPerson,
                                                            Permission.EDIT_USER,
                                                            null,
                                                            qualification);

        return canEdit;
    }


    /**
     *
     * @param shadowPerson
     * @param editPerson
     * @return
     */
    public boolean selfEdit(MivPerson shadowPerson, MivPerson editPerson)
    {
        return shadowPerson.getUserId() == editPerson.getUserId();
    }


    /**
     *
     * @param editPerson
     * @return
     */
    public boolean isExecutive(MivPerson editPerson)
    {
        return !getExecutiveRoles(editPerson).isEmpty();
    }


    /**
     *
     * @param isExecutive
     * @param canEdit
     * @return
     */
    public boolean roleEditAllowed(boolean isExecutive, boolean canEdit)
    {
        return isExecutive ? false : canEdit;
    }


    /**
     * Allow the flow definition to check if the actor is authorized to edit within a given Scope.
     *
     * @param context webflow request context
     * @return webflow event status, allowed, denied, or error
     */
    public Event isAuthorized(RequestContext context)
    {
        MivPerson shadowPerson = getShadowPerson(context);
        MivPerson editPerson = ((EditUserForm)getFormObject(context)).getEditPerson();

        boolean selfEdit = selfEdit(shadowPerson, editPerson);
        boolean canEdit = canEdit(shadowPerson, editPerson);

        // for use in view
        context.getFlowScope().put("canEdit", canEdit);
        context.getFlowScope().put("selfEdit", selfEdit);

        //Cannot edit the Role if this is an executive, user must first remove the executive role
        boolean isExecutive = isExecutive(editPerson);
        boolean roleEditAllowed = roleEditAllowed(isExecutive, canEdit);
        context.getFlowScope().put("roleEditAllowed", roleEditAllowed);
        context.getFlowScope().put("isExecutive", isExecutive);

        // allowed to proceed if shadow person can edit the target or is the target
        if (canEdit || selfEdit)
        {
        	/*
        	 * MIV-3361 : Make sure there is not a dossier
        	 * in process at any node other than Packet Request.
        	 */
        	Dossier dossier = getDossierInProcess(editPerson);
        	if (dossier != null)
        	{
        		if (dossier.getDossierLocation() !=  DossierLocation.PACKETREQUEST)
        		{
        			context.getFlashScope().put("dossierInProcess", dossier.getDossierId());
        			return error();
        		}
        		else
        		{
        			context.getFlowScope().put("packetRequested", true); //to display warning that dossier will be canceled if user edited
        		}
        	}

        		// allowed, no dossier in process
        		return allowed();
        }

        return denied();
    }


    /**
     * get any executive roles this person has
     * @param editPerson MivPerson from the EditUserForm
     * @return List containing executive roles the person has or empty if not an executive
     */
    private List<MivRole> getExecutiveRoles(MivPerson editPerson)
    {
    	List<MivRole> executiveRoles = new ArrayList<>();
    	if (editPerson.hasRole(CHANCELLOR)) {
		executiveRoles.add(CHANCELLOR);
    	}

    	if (editPerson.hasRole(PROVOST)) {
		executiveRoles.add(PROVOST);
	}

    	if (editPerson.hasRole(VICE_PROVOST)) {
		executiveRoles.add(VICE_PROVOST);
    	}

	return executiveRoles;
    }


    public Set<AppointmentScope> getDeptSet(MivPerson shadowPerson)
    {
        DepartmentListFilter filter = new DepartmentListFilter(shadowPerson.getAssignedRoles());

        // Primary department list is limited by the editing user's access
        Set<AppointmentScope> deptSet = filter.apply(ALL_APPOINTMENT_SCOPES);
        deptSet.add(new AppointmentScope(Scope.NoScope));

        return deptSet;
    }


    public Set<AppointmentScope> getUserScopedDepts(Set<AppointmentScope> deptSet, MivPerson shadowPerson)
    {
        // Save these for proper drop-down when a role change is made on the page (FACULTY->STAFF)
        Set<AppointmentScope> userScopedSet = new TreeSet<AppointmentScope>(deptSet);

        return userScopedSet;
    }


    public Set<AppointmentScope> getScopedDepts(Set<AppointmentScope> deptSet,
                                                MivPerson editedPerson)
    {
        // Now add entries for any existing appointments the edited user already has so
        // they can be preserved even if the editing user can't normally assign them.
        for (Appointment a : editedPerson.getAppointments())
        {
            AppointmentScope scope = new AppointmentScope(a.getScope().getSchool(), a.getScope().getDepartment());
            boolean added = deptSet.add(scope);
            logger.debug("Added {} to the department list = {}", scope, added);
        }

        return deptSet;
    }


    /**
     * Loads user department list into request scope
     * @param context Request context from webflow
     */
    public void loadLists(RequestContext context)
    {
        MivPerson shadowPerson = this.getShadowPerson(context);
        MivPerson editee = ((EditUserForm)this.getFormObject(context)).getEditPerson();

        Set<AppointmentScope> deptSet = getDeptSet(shadowPerson);
        Set<AppointmentScope> userScopedSet = getUserScopedDepts(deptSet, shadowPerson);
        context.getRequestScope().put("userscopeddepartments", userScopedSet);

        deptSet = getScopedDepts(deptSet, editee);

        context.getRequestScope().put("scopeddepartments", deptSet);

        // Joint department list
        context.getRequestScope().put("alldepartments", ALL_APPOINTMENT_SCOPES);
    }


    /**
     * Get the dossier currently in process for the given candidate.
     *
     * @param candidate dossier candidate
     * @return dossier in process or <code>null</code> if none
     */
    private Dossier getDossierInProcess(MivPerson candidate)
    {
        // See if there is a dossier in process for this candidate.
        // There may only be one dossier in process at a time.
        try
        {
            Iterator<Dossier> dossiers = dossierService.getDossiersInProcess(candidate)
                                                       .iterator();

            // return the first
            if (dossiers.hasNext()) return dossiers.next();
        }
        catch (WorkflowException wfe)
        {
            logger.warn("User edit; uable to determine if a Dossier is in process for user" + candidate);
        }

        return null;
    }


    /**
     * FIXME: This doesn't belong in here - the controller for the web UI. This
     * is a business rule and should be in some domain-level code/package.
     *
     * Remove signatures, uploads, and reviewers for any appointment which may have
     * been removed. Remove the RAF if at least one appointment is added or removed.
     *
     * @param dossier
     * @param previousAppointments
     * @param currentAppointments
     * @param realPerson
     */
    private void removeUploadsReviewersAndRaf(Dossier dossier,
                                              Set<Appointment> previousAppointments,
                                              Set<Appointment> currentAppointments,
                                              MivPerson realPerson)
    {
        logger.info("User edit; Checking to remove uploaded documents and RAF for dossier {} for user {} which is in process at Candidate node.",
                    dossier.getDossierId(), dossier.getAction().getCandidate().getUserId());

        // for each appointment to be removed (previous - current)
        for (Appointment appointment : Sets.difference(previousAppointments,
                                                       currentAppointments))
        {
            // Delete the reviewers for this appointment
            dossierService.deleteDossierReviewers(dossier.getDossierId(), appointment.getScope().getSchool(), appointment.getScope().getDepartment());

            // Remove/archive all signatures for the appointment
            dcService.resetDisclosureCertificates(dossier,
                                                  appointment.getScope().getSchool(),
                                                  appointment.getScope().getDepartment(),
                                                  realPerson);

        }

        // Remove the RAF if current appointments are not equal to previous
        if (!CollectionUtils.isEqualCollection(previousAppointments, currentAppointments))
        {
            logger.info("User edit; Removing Recommended Action Form for dossier {} for user {} which is in process at Candidate node.",
                        dossier.getDossierId(), dossier.getAction().getCandidate().getUserId());

            rafService.deleteRaf(dossier);
        }
    }


    /**
     * Cancel the dossier that is at the Candidate
     *
     * @param dossier
     * @param editedPerson
     */
    private void cancelDossier(Dossier dossier,
            MivPerson editedPerson)
    {
        try
        {
            this.dossierService.cancelDossier(editedPerson,dossier.getDossierId(),"Canceling due to appointment change in User Edit");
            EventDispatcher2.getDispatcher().post(new DossierCancelEvent(editedPerson, dossier));
            logger.info("User edit; Appointments changed, Canceling dossier {} for user {} which is in process at Packet Request node.",
                    dossier.getDossierId(), dossier.getAction().getCandidate().getUserId());
        }
        catch (WorkflowException wfe)
        {
            String msg = "Workflow exception canceling dossier " + dossier.getDossierId();
            logger.error(msg, wfe);
        }
    }


    /**
     * Create and write an audit trail entry for adding or modifying people.
     *
     * @param isAdd
     * @param editedPerson
     * @param realPerson
     */
    private void createAuditTrail(boolean isAdd,
                                  MivPerson editedPerson,
                                  MivPerson realPerson)
    {

        final String auditMessage = AuditMessages.getInstance().getString(isAdd ? "audit.user.add" : "audit.user.edit",
                                                                          editedPerson.getDisplayName(),
                                                                          realPerson.getDisplayName());
        final String originalXml = isAdd ? null : ConvertPersonXml.generateXMLString(editedPerson);

        // Audit Trail : Start
        if (auditService != null)
        {
            AuditObject auditObject = null;
            String description = "";

            description = AuditMessages.getInstance().getString(auditMessage, editedPerson.getUserId(), realPerson.getUserId());
            auditObject = new AuditObject(AuditAction.EDIT, MivEntity.USER, String.valueOf(editedPerson.getUserId()),
                                          description, originalXml, String.valueOf(realPerson.getUserId()));
            auditService.createAuditTrail(auditObject);
        }
        // Audit Trail : End
    }


    /**
     * Register custom property editors to convert between form text and {@code MivRole} or {@code Scope}.
     *
     * @param registry the property editor registry to register editors in
     */
    @Override
    protected void registerPropertyEditors(PropertyEditorRegistry registry)
    {
        registry.registerCustomEditor(MivRole.class, new MivPropertyEditors.RolePropertyEditor());
        registry.registerCustomEditor(Scope.class, new MivPropertyEditors.ScopePropertyEditor());
    }


    /**
     * Load all available Appointment scopes.
     * @param event the MivEvent triggering this call. 'event' can be null when called internally.
     */
    @Subscribe
    public static void loadScopes(final ConfigurationChangeEvent event)
    {
        System.out.println("loadScopes got event [" + event + "]");

        if (event == null || event.getWhatChanged() == ALL || event.getWhatChanged() == PROPERTIES)
        {

            synchronized(EditAction.class) {
                if (event == null) {
                    System.out.println(" ----> Initial Loading of ALL_APPOINTMENT_SCOPES");
                }
                else {
                    System.out.println(" ----> Re-loading ALL_APPOINTMENT_SCOPES because of ConfigurationChangeEvent");
                }

                final Set<String> allScopes = MIVConfig.getConfig().getMap("departments").keySet();
                if (allScopes.size() == 0) {
                    System.err.println(" ----> WARNING: MIVConfig returned no departments in its map.");
                    return;
                }

                ALL_APPOINTMENT_SCOPES.clear();
                ALL_APPOINTMENT_SCOPES.add( new AppointmentScope(Scope.NoScope) ); // Include no scope

                for (String scope : allScopes) {
                    ALL_APPOINTMENT_SCOPES.add( new AppointmentScope(new Scope(scope)) );
                }
                //System.out.println(" ----> Appointment Scopes now contains:\n" + ALL_APPOINTMENT_SCOPES);
            }

        }
    }

}
