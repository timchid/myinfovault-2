/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DeanChangeResult.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.io.Serializable;

/**
 * Result of changing the Dean's Signing Authority for a person, to be passed to JSP for display.
 * These are created by the {@link ManageDeansAction} and saved to the {@link ManageDeansForm}
 *
 * @author Stephen Paulsen
 * @since MIV 4.0
 */
public class DeanChangeResult implements Serializable
{
    private static final long serialVersionUID = 20101012101231L;

    /** Person who was modified */
    private String personName;
    /** School where Person is/was Dean */
    private String schoolName;
    /** Result indicating if Person is now <strong>authorized</strong> or <strong>unauthorized</strong> */
    private String resultText;


    DeanChangeResult(String name, String school, String resultText)
    {
        this.personName = name;
        this.schoolName = school;
        this.resultText = resultText;
    }

    public String getPersonName()
    {
        return this.personName;
    }

    public String getSchoolName()
    {
        return this.schoolName;
    }

    public String getResultText()
    {
        return this.resultText;
    }
}
