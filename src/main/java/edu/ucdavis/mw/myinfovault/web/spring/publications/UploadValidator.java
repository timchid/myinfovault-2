/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UploadValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.publications;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;

/**
 * Validates the upload form.
 *
 * @author Stephen Paulsen
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class UploadValidator implements Validator
{
    private static Logger logger = Logger.getLogger(UploadForm.class);

    private static final String[] XML_MIME_TYPES = {"text/xml"};
    private static final String[] CSV_MIME_TYPES = {"text/csv", "application/vnd.ms-excel"};

    /**
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(UploadForm.class);
    }

    /**
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors)
    {
        UploadForm form = (UploadForm) target;

        MultipartFile uploadFile = form.getUploadFile();
        PublicationSource uploadSource = form.getSource();

        //check file exists
        if(uploadFile == null)
        {
            errors.reject(null, "No document was uploaded");
            return;
        }

        String uploadFileName = uploadFile.getOriginalFilename();
        String uploadFileType = uploadFile.getContentType();

        //form was submitted without a file in the field.
        if (uploadFileName.length() <= 0)
        {
            errors.reject(null, "No document was uploaded");
            return;
        }

        // We've got a filename; does the file have any contents?  Is it an acceptable mime-type?
        logger.info("Attempting import validation of file [" + uploadFileName + "] with mime-type [" + uploadFileType + "]");

        //check upload size
        if (uploadFile.isEmpty())
        {
            String msg = "The uploaded document \"" + uploadFileName + "\" was empty";
            logger.warn("Rejecting file: " + msg);
            errors.reject(null, msg);
            return;
        }
        if (uploadFile.getSize() > 10000000)
        {
            String msg = "The uploaded document \"" + uploadFileName + "\" is too large. There is a 10 MB size limit.";
            logger.warn("Rejecting file: " + msg);
            errors.reject(null, msg);
            return;
        }

        /* check content type:
         * error if xml file without an xml mimetype/extension
         * or, if csv file without a csv mimetype/extension
         */
        if((
                (uploadSource == PublicationSource.ENDNOTE || uploadSource == PublicationSource.PUBMED)
             && Arrays.binarySearch(XML_MIME_TYPES, uploadFileType) < 0
             && !uploadFileName.matches(".*\\.[Xx][Mm][Ll]\\Z")
            ) || (
                uploadSource == PublicationSource.CSV
             && Arrays.binarySearch(CSV_MIME_TYPES, uploadFileType) < 0
             && !uploadFileName.matches(".*\\.[Cc][Ss][Vv]\\Z")
        ))
        {
            logger.warn("Rejecting uploaded file \"" + uploadFileName + "\" with mime-type [" + uploadFileType + "]");
            errors.reject(null, "The uploaded document \"" + uploadFileName + "\" is not a proper " + uploadSource + " file");
            return;
        }
    }
}
