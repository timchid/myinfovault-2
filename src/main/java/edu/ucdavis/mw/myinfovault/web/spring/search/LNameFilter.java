/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LNameFilter.java
 */
 
package edu.ucdavis.mw.myinfovault.web.spring.search;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Filter for a search by first initial of surname.
 * 
 * @author Mary Northup
 * @since MIV 3.0
 */
public class LNameFilter extends SearchFilterAdapter<MivPerson> 
{
    private final String name;

    /**
     * @param name name term on which to filter
     */
    public LNameFilter(String name)
    {
        this.name = name.toLowerCase();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(MivPerson item)
    {
        return ((name.indexOf("all") == 0)
                || (item.getSurname().toLowerCase().indexOf(name) == 0));
    }
}
