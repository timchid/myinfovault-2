/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CancelDossierSearchCriteria.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: javadoc
 *
 * @author Rick Hendricks
 * @since MIV 4.7.1
 */
public class CancelDossierSearchCriteria extends SearchCriteria implements Serializable
{
    private static final long serialVersionUID = 6910386036643523922L;

    private List<String> dossiersToCancel = new ArrayList<String>();

    /**
     * @return dossiers to cancel
     */
    public List<String> getDossiersToCancel()
    {
        return this.dossiersToCancel;
    }

    /**
     * @param dossiersToCancel set list of dossiers to cancel
     */
    public void setDossiersToCancel(List<String> dossiersToCancel)
    {
        this.dossiersToCancel = dossiersToCancel;
    }
}
