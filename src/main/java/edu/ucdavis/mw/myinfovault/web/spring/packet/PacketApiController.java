/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketApiController.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.packet;

import static edu.ucdavis.mw.myinfovault.service.apientity.ApiScope.getApiScopes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;

import javax.security.auth.login.CredentialException;

import org.apache.commons.lang.StringUtils;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizerFactory;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiPermission;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiResourceType;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.ApiControllerBase;
import edu.ucdavis.mw.myinfovault.web.spring.search.AssignedRoleFilter;

/**
 * @author japorito
 * @since 5.0
 */
@RestController
public class PacketApiController extends ApiControllerBase
{
    private static final Long NEW_PACKET_ID = -1L;

    private static final PacketService packetService = MivServiceLocator.getPacketService();
    private static final UserService userService = MivServiceLocator.getUserService();
    //We want to check the packet owner is in the editor's scope, specifically the scopes in which
    //they are a candidate. I added MIV_USER so that people who aren't candidates could edit packets
    //of their own to test with.
    private static final AssignedRoleFilter packetOwnerRoleFilter = new AssignedRoleFilter(MivRole.CANDIDATE,
                                                                                           MivRole.APPOINTEE,
                                                                                           MivRole.MIV_USER);


    @ModelAttribute("authorizer")
    public ApiAuthorizer getAuthorizer(@RequestHeader(value="Authorization", required=true) String auth) throws CredentialException
    {
        return ApiAuthorizerFactory.getAuthorizer(auth);
    }



    @RequestMapping(value="/api/packets", method=RequestMethod.POST)
    public ResponseEntity<? extends ResourceSupport> postPackets(@ModelAttribute("authorizer") ApiAuthorizer authorizer,
                                                                 @RequestHeader("Authorization") String authHeader,
                                                                 @RequestBody Packet packet) throws CredentialException
    {
        return savePacket(null, authorizer, authHeader, packet);
    }



    @RequestMapping(value="/api/packets/{packetId}", method={RequestMethod.PUT, RequestMethod.POST})
    public ResponseEntity<? extends ResourceSupport> savePacket(@PathVariable("packetId") Long packetId,
                                                                @ModelAttribute("authorizer") ApiAuthorizer authorizer,
                                                                @RequestHeader("Authorization") String authHeader,
                                                                @RequestBody Packet packet) throws CredentialException
    {
        packetId = packetId == null ? packet.getPacketId() : packetId;//might still be null, if it wasn't specified in the packet.
        boolean creatingPacket = packetId == null && (packet.getPacketId() == null || packet.getPacketId() == NEW_PACKET_ID);

        PacketValidationResource validation;
        if (creatingPacket)
        {
            packet.setPacketId(NEW_PACKET_ID);
            setPacketContentUserId(packet, packet.getUserId());
            validation = new PacketValidationResource(packet);
        }
        else
        {
            Packet originalPacket = packetService.getPacket(packetId);

            //specified packet does not exist
            if (originalPacket == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            fillPacketFromOriginal(originalPacket, packet);
            validation = new PacketValidationResource(originalPacket, packet);
        }

        boolean authorized = authorizer.permitsOneOf(authHeader,
                                                     getApiScopes(ApiResourceType.PACKET,
                                                                  EnumSet.of(creatingPacket ?
                                                                          ApiPermission.CREATE :
                                                                          ApiPermission.UPDATE),
                                                                          this.getOwnerAssignedRoles(packet),
                                                                          packet.getUserId()),
                                                                          this.getAllPermittedRoles(packet, MivRole.API_ENTITY));

        if (authorized)
        {
            if (validation.isValid())
            {
                //assuming that if the authorizer doesn't represent a
                //person that it is the packet owner doing the editing.
                packet = packetService.updatePacket(packet,
                               authorizer.getRepresentedPerson() == null ?
                                   authorizer.getRepresentedPerson().getUserId() :
                                   packet.getUserId());

                return new ResponseEntity<Packet>(packet, creatingPacket ? HttpStatus.CREATED : HttpStatus.OK);
            }
            else //validation errors exist
            {
                return new ResponseEntity<PacketValidationResource>(validation, HttpStatus.BAD_REQUEST);
            }
        }
        else //not authorized
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }



    @RequestMapping(value="/api/packets/{packetId}", method={RequestMethod.GET})
    public ResponseEntity<? extends ResourceSupport> getPacket(@PathVariable("packetId") Long packetId,
                                                               @ModelAttribute("authorizer") ApiAuthorizer authorizer,
                                                               @RequestHeader("Authorization") String authHeader) throws CredentialException
    {
        Packet packet = packetService.getPacket(packetId);

        boolean authorized = authorizer.permitsOneOf(authHeader,
                                                     getApiScopes(ApiResourceType.PACKET,
                                                                  EnumSet.of(ApiPermission.READ),
                                                                  this.getOwnerAssignedRoles(packet),
                                                                  packet.getUserId()),
                                                                  this.getAllPermittedRoles(packet, MivRole.API_ENTITY));

        if (authorized)
        {
            return new ResponseEntity<Packet>(packet, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }



    @RequestMapping(value="/api/packets/{packetId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deletePacket(@PathVariable Long packetId,
                                          @ModelAttribute("authorizer") ApiAuthorizer authorizer,
                                          @RequestHeader("Authorization") String authHeader) throws CredentialException
    {
        Packet packet = packetService.getPacket(packetId);

        if (packet ==  null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        boolean authorized = authorizer.permitsOneOf(authHeader,
                                                     getApiScopes(ApiResourceType.PACKET,
                                                                  EnumSet.of(ApiPermission.DELETE),
                                                                  this.getOwnerAssignedRoles(packet),
                                                                  packet.getUserId()),
                                                                  this.getAllPermittedRoles(packet, MivRole.API_ENTITY));

        if (authorized)
        {
            if (packetService.deletePacket(packetId))
            {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else
            {
                //Bad request? Delete failed, but the packet was found...
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }



    private Collection<AssignedRole> getOwnerAssignedRoles(Packet packet)
    {
        Collection<AssignedRole> packetOwnerRoles = new ArrayList<>();

        if (packet.getUserId() != null)
        {
            MivPerson packetOwner = userService.getPersonByMivId(packet.getUserId());
            packetOwnerRoles.addAll(packetOwner.getAssignedRoles());
        }

        return packetOwnerRoleFilter.apply(packetOwnerRoles);
    }



    /**
     * Get all roles permitted to perform a packet action.
     * The roles will include the roles of the packet owner, plus any additional roles as needed
     *
     * @param additionalRoles - Any additional allowed roles
     * @return Collection of roles allowed
     */
    private Collection<MivRole> getAllPermittedRoles(Packet packet, MivRole...additionalRoles)
    {
        // Get the roles permitted to perform a packet action
        Collection<MivRole>permittedRoles = new ArrayList<>();
        for (AssignedRole role : this.getOwnerAssignedRoles(packet))
        {
            permittedRoles.add(role.getRole());
        }

        // Add any additional roles
        for (MivRole mivRole : additionalRoles)
        {
            permittedRoles.add(mivRole);
        }

        return permittedRoles;
    }



    /**
     * Fill unspecified packet info from the currently stored version of the packet.
     *
     * @param originalPacket The version currently in the database.
     * @param newPacket The version that is going to be saved.
     */
    private void fillPacketFromOriginal(Packet originalPacket, Packet newPacket)
    {
        if (newPacket.getPacketId() == null) {
            newPacket.setPacketId(originalPacket.getPacketId());
        }

        if (StringUtils.isEmpty(newPacket.getPacketName())) {
            newPacket.setPacketName(originalPacket.getPacketName());
        }

        if (newPacket.getUserId() == null) {
            newPacket.setUserId(originalPacket.getUserId());
        }

        if (newPacket.getPacketItemsMap() == null) {
            newPacket.setPacketItems(new ArrayList<>(originalPacket.getPacketItems()));
        }
        else
        {
            setPacketContentUserId(newPacket, originalPacket.getUserId());
        }
    }



    private void setPacketContentUserId(Packet packet, Integer userId)
    {
        for (PacketContent pc : packet.getPacketItems())
        {
            if (pc.getUserId() == null)
            {
                pc.setUserId(userId);
            }
        }
    }
}
