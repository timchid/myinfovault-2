/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchCriteriaValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validates the search criteria form.
 *
 * @author Marry Northup
 * @since 3.0
 */
public class SearchCriteriaValidator implements Validator
{
    @Override
    public boolean supports(Class clazz)
    {
        return SearchCriteria.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors)
    {
        SearchCriteria query = (SearchCriteria) obj;

        /*
         * make sure that data in the form is "clean" (not been tampered with)
         */
        if (query.getLname().length() > 1
         && !query.getLname().equals("All"))
        {
            errors.reject("invalidteCriteria", "Last Name not from dropdown list");
        }
    }
}
