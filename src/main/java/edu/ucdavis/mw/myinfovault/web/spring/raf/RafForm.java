/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.raf;

import java.io.Serializable;
import java.util.EnumSet;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.domain.raf.Rank;
import edu.ucdavis.mw.myinfovault.domain.raf.Status;
import edu.ucdavis.mw.myinfovault.web.spring.action.AcademicActionForm;

/**
 * Form backing for the {@link RafAction}.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class RafForm extends AcademicActionForm implements Serializable
{
    private static final long serialVersionUID = -1737933765408912603L;

    /**
     * Action types associated with the legacy RAF.
     */
    private static final Set<DossierActionType> RAF_ACTION_TYPES =
        EnumSet.of(DossierActionType.APPOINTMENT_CHANGE_DEPARTMENT,
                   DossierActionType.APPOINTMENT_ENDOWED_CHAIR,
                   DossierActionType.APPOINTMENT_ENDOWED_PROFESSORSHIP,
                   DossierActionType.APPOINTMENT_ENDOWED_SPECIALIST,
                   DossierActionType.REAPPOINTMENT_ENDOWED_CHAIR,
                   DossierActionType.REAPPOINTMENT_ENDOWED_PROFESSORSHIP,
                   DossierActionType.REAPPOINTMENT_ENDOWED_SPECIALIST);

    /**
     * Maximum number of RAF status rank rows to display on the form.
     */
    private static final int MAXIMUM_STATUS_RANK_ROWS = 5;

    private final RafBo raf;

    /**
     * Create the RAF form backing bean.
     *
     * @param raf recommended action form business object
     */
    protected RafForm(RafBo raf)
    {
        super(raf.getDossier());

        this.raf = raf;

        if (!isViceProvost())
        {
            // include blank status rank rows
            for (Status status : raf.getStatuses())
            {
                while (status.getRanks().size() < MAXIMUM_STATUS_RANK_ROWS)
                {
                    status.getRanks().add(new Rank());
                }
            }
        }
    }

    /**
     * @return recommended action form business object
     */
    public RafBo getRaf()
    {
        return raf;
    }

    /**
     * @return if the action type bound to this form is to be edited with the legacy RAF.
     */
    public boolean isRAF()
    {
        /*
         * Use the legacy RAF if:
         *   - the RAF already exists
         *   - is not currently at the Department
         *  OR
         *   - the selected action type is one of the legacy RAF types
         */
        return raf.getId() > 0 && raf.getDossier().getDossierLocation() != DossierLocation.DEPARTMENT
            || RAF_ACTION_TYPES.contains(getActionType());
    }
}
