/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditRecordHelper.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.valuebeans.RecordVO;
import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.mw.myinfovault.valuebeans.ValidateResultVO;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.RecordHandler;
import edu.ucdavis.myinfovault.data.Validator;
import edu.ucdavis.myinfovault.message.ErrorMessages;
import edu.ucdavis.myinfovault.message.MivConstants;

/**
 * Record Editor:
 * Get one record from the data source for editing and send that record to an editing page.
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public class EditRecordHelper
{
    private static final Logger logger = LoggerFactory.getLogger(EditRecordHelper.class);


    public Map<String, ResultVO> process(MIVUserInfo actingMui, MIVUserInfo targetMui, RecordVO... reqDataList) throws ServletException, IOException, Exception
    {
        logger.trace("\n ***** Entering EditRecordHelper:process ***************\n");
        Map<String, ResultVO> result = null;

        if (reqDataList != null)
        {
            result = new HashMap<String, ResultVO>();
            ResultVO resultVO = null;
            ValidateResultVO validateResultVO = null;
            Map<String, String[]> requestedData = null;
            Properties cfgProps = null;

            for (RecordVO recordVO : reqDataList)
            {
                if (recordVO != null)
                {
                    requestedData = recordVO.getRequestedData();
                    String recordType = (requestedData.get("rectype") != null ? requestedData.get("rectype")[0] : null);

                    /*// Instead of getting DocumentID from request get it from reqData which has all the request parameters
                    //String documenID = req.getParameter("DocumentID");
                    String documenID = (requestedData.get("DocumentID") != null ? requestedData.get("DocumentID")[0] : null);
                    if (documenID == null || documenID.length() == 0)
                    {
                        // We dont have SectionName column for Additional Record so remove it from request data.
                        // SectionName is used in request for Additional Record just for the purpose of redirecting
                        // the user to the corresponding record page when cancel is clicked.
                        requestedData.remove("sectionname");
                    }*/

                    cfgProps = PropertyManager.getPropertySet(recordType, "config");

                    // Get parent-key to execute this record
                    String iscustomkey = cfgProps.getProperty("iscustomkey");

                    if (Boolean.parseBoolean(iscustomkey))
                    {
                        String parentRecordTypes = cfgProps.getProperty("customkey-parentrecordtypes");
                        Iterable<String> parentRecordTypeArray = StringUtil.getListSplitter().split(parentRecordTypes);

                        // This might be null, if no custom keys were specified, but the splitter is ok with null, returning an empty array
                        String customkeys = cfgProps.getProperty("customkeys");
                        String[] customkeyArray = StringUtil.splitToArray(customkeys);

                        // For each parent record type there must be a custom key specified, or one will be generated
                        // based on the parent record type name; so parent=thing would have key=thingid if a key is
                        // not given.
                        // It's ok to leave off trailing unspecified keys, but leading unspecified keys require leading
                        // commas in order to omit them. For example...
                        //   A. All custom keys specified:
                        //      parentrecordtypes=reviews,events
                        //      customkeys=reviewid,eventid
                        //   B. Trailing key omitted
                        //      parentrecordtypes=reviews,events
                        //      customkeys=reviewid
                        //      -> a key will be generated as "eventsid" - notice "events" vs. "event"
                        //   C. Leading key omitted
                        //      parentrecordtypes=reviews,events
                        //      customkeys=,eventid
                        //      -> a key will be generated as "reviewsid" - notice "reviews" vs. "review"
                        // Because of this, we need to be careful about checking the customkeyArray.length as we increment the index
                        int index = 0;
                        for (String parentRecordType : parentRecordTypeArray)
                        {
                            // Look up into result map for this parent record
                              // Don't need to check "result != null" here because it was explicitly set above as:
                              //    result = new HashMap<String, ResultVO>();
                              // Since we're not checking that for null, we also don't have to check result.get(..) for null, because
                              // we're setting tempResultVO to null if it *IS* null, so just set it
                            //tempResultVO = (result != null && result.get(parentRecordType) != null) ? result.get(parentRecordType) : null;
                            ResultVO tempResultVO = result.get(parentRecordType);
                            if (tempResultVO != null)
                            {
                                if (tempResultVO.getId() != null && !tempResultVO.getId().equals("-1"))
                                {
                                    logger.info/*.debug*/("{} id :: {}", parentRecordType, tempResultVO.getId());
                                    // FIXME: customkeyArray might be size==0 and this would throw an IndexOutOfBoundsException.
                                    // FIXME: check the array.length
                                    String customkey = null;
                                    if (customkeyArray.length > index) {
                                        customkey = customkeyArray[index];
                                    }

                                    // If a parent record type was specified but no custom key was given
                                    // generate a custom key from the parent record type name.
                                    // The custom key will be null if we ran out of array (there were fewer keys than parents given)
                                    // or the key may be blank/empty if leading custom keys were omitted.
                                    if (customkey == null || customkey.length() == 0)
                                    {
                                        customkey = parentRecordType + "id";
                                    }

                                    // I'm still not clear about what this is checking, but it apparently has/had something
                                    // to do with having the custom typeahead-and-add work types etc.
                                    if (requestedData.get(customkey) == null || requestedData.get(customkey).length == 0)
                                    {
                                        requestedData.put(customkey, new String[] { tempResultVO.getId() });
                                    }
                                }
                                else
                                {
                                    logger.warn("{} :: has an invalid parentId of {}", recordType, tempResultVO.getId());
                                    continue;
                                }
                            }
                            index++;
                        }
                    }

                    if (recordVO.isValidationRequired())
                    {
                        validateResultVO = processCollectValidateParameters(requestedData, cfgProps);
                    }
                    else
                    {
                        validateResultVO = recordVO.getValidateResultVO();
                    }

                    if (validateResultVO != null && !validateResultVO.isSuccess())
                    {
                        logger.debug("\n ????? Execution Failed ?????\n");

                        result.put(recordType, new ResultVO(validateResultVO.isSuccess(), validateResultVO.getMessages(), validateResultVO.getErrorfields()));
                        break;
                    }

                    String iscustomclass = cfgProps.getProperty("iscustomclass");
                    //if (iscustomclass != null && iscustomclass.equalsIgnoreCase("true"))
                    if (Boolean.parseBoolean(iscustomclass))
                    {
                        logger.debug(" ============ Need to execute iscustomclass ==================");
                        resultVO = processCustomRecord(requestedData, validateResultVO, actingMui, targetMui);
                    }
                    else
                    {
                        resultVO = processRecord(requestedData, validateResultVO, targetMui);
                    }

                    if (resultVO != null)
                    {
                        result.put(recordType, resultVO);
                        logger.debug("{} :: {}", recordType, resultVO);
                    }
                }
            }
        }

        logger.trace("\n ***** Leaving EditRecordHelper:process ***************\n");
        return result;
    }


    /**
     * to get the valid parameters
     * @param reqData
     * @param cfgProps
     * @return ValidateResultVO
     */
    private ValidateResultVO processCollectValidateParameters(Map <String, String[]> reqData, Properties cfgProps)
    {
        ValidateResultVO validateResultVO = null;
        String className = cfgProps.getProperty("collectValidateParametersClass");
        String methodName = "collectValidateParameters";

        try {
            Class<?> dynaclass = Class.forName( className );
            Object classInstance = dynaclass.newInstance(); // Calling default constructor
            // Method must have two arguments userId as String and reqData as Map
            Method method = dynaclass.getDeclaredMethod(methodName, Map.class, Properties.class);

            logger.debug("method :: {}", method);

            if (method != null)
            {
                Object resultObject = method.invoke(classInstance,reqData, cfgProps);
                if (resultObject instanceof ValidateResultVO)
                {
                    validateResultVO = (ValidateResultVO) resultObject;
                }
            }
        }
        catch (Exception e) { // FIXME: Do NOT catch plain "Exception"
            // call the default validation
            validateResultVO = collectValidateParameters(reqData, cfgProps);
            e.printStackTrace(); // FIXME: Do NOT .printStackTrace()
            // LOG the error if it must be logged. Don't log anything if this is not actually a problem.
//            logger.warn("<Something Bad> happened when <Something> was being done", e);
        }

        return validateResultVO;
    }


    private ResultVO processCustomRecord(Map<String, String[]> reqData, ValidateResultVO validateResultVO, MIVUserInfo actingMui, MIVUserInfo targetMui) throws /*ServletException, IOException,*/ Exception
    {
        ResultVO resultVO = null;

        logger.trace("\n ******* Entering CreativeActivitiesEditRecord:processCustomRecord ******** ");
        logger.debug("request data: [{}]", reqData);

        String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
        Properties cfgProps = PropertyManager.getPropertySet(recordType, "config");


        String className = cfgProps.getProperty("classname");
        String methodName = cfgProps.getProperty("methodname");
        // This is the switched-to userID, not the actual userId
        int targetUserId = targetMui.getPerson().getUserId();
        int actingUserId = actingMui.getPerson().getUserId();

        if (logger.isDebugEnabled())
        {
            System.out.println("\n\n\ncfgProps:: " + cfgProps + "\n\n\n");
            System.out.println("className :: " + className);
            System.out.println("methodName :: " + methodName);
        }

        try
        {
            Class<?> dynaclass = Class.forName( className );
            Object classInstance = dynaclass.newInstance(); // Calling default constructor
            // Method must have two arguments userId as String and reqData as Map
            Method method = dynaclass.getDeclaredMethod(methodName, int.class, int.class, Map.class);

            logger.debug("method :: {}", method);

            if (method != null)
            {
                Object resultObject = method.invoke(classInstance, actingUserId, targetUserId, reqData);
                if (resultObject instanceof ResultVO)
                {
                    resultVO = (ResultVO) resultObject;
                }
            }
        }
// FIXME: all these exceptions should be handled, not re-thrown.
//  One way to handle them would be to throw a single Configuration type of Exception
        catch (SecurityException e)
        {
            throw e;
        }
        catch (IllegalArgumentException e)
        {
            throw e;
        }
        catch (ClassNotFoundException e)
        {
            throw e;
        }
        catch (InstantiationException e)
        {
            throw e;
        }
        catch (IllegalAccessException e)
        {
            throw e;
        }
        catch (NoSuchMethodException e)
        {
            throw e;
        }
        catch (InvocationTargetException e)
        {
            throw e;
        }

        logger.trace("\n ******* Leaving CreativeActivitiesEditRecord:processCustomRecord ******** ");
        return resultVO;
    }


    private ResultVO processRecord(Map<String, String[]> reqData, ValidateResultVO validateResultVO, MIVUserInfo mui) //throws ServletException, IOException
    {
        logger.trace("\n ***** Entering EditRecordHelper:processRecord ***************\n");

        ResultVO resultVO = null;

        String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
        String subType = (reqData.get("subtype") != null ? reqData.get("subtype")[0] : null);

        //MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();
        int userId = mui.getPerson().getUserId();

        Properties cfgProps = null;
        Properties strProps = null;
        String tableName;

        // Instead of getting rectype and subtype from request get it from reqData which has all the request parameters
        //String recordType = req.getParameter("rectype");
        //String subType = req.getParameter("subtype");
        String recType = recordType;
        logger.debug(" EditRecord-doPost record type is {}", recordType);

        // TODO: Should this also show the system error page?
        if (recordType == null || recordType.length() == 0) {
            logger.error("Record type is missing when trying to add/save a record.");
        }

        if (subType != null && !("".equals(subType)))
        {
            recType += "-" + subType;
        }

        strProps = PropertyManager.getPropertySet(recType, "strings");
        cfgProps = PropertyManager.getPropertySet(recordType, "config");
        tableName = cfgProps.getProperty("tablename");

        logger.debug("Config Properties are: [{}]\n  tableName from PropertyManager lookup is {}", cfgProps, tableName);

        // TODO: Should this also show the system error page?
        if (tableName == null || tableName.length() == 0) {
            logger.error("Table name is missing trying to add/save a [{}] record.", recordType);
        }

        // ServletOutputStream out = res.getOutputStream(); Pradeep -- It should be in servlet
        RecordHandler rh = mui.getRecordHandler();

        // Added below line to show the Proper success message while adding for editing a record or section header.
        String recOrSection = "Record";
        // Instead of getting DocumentID from request get it from reqData which has all the request parameters
        String documenID = (reqData.get("DocumentID") != null ? reqData.get("DocumentID")[0] : null);
        if (documenID != null && documenID.length() > 0)
        {
            recOrSection = "Section Header";
        }

        // Instead of getting recid from request get it from reqData which has all the request parameters
        //String recordId = req.getParameter("recid");
        String recordId = (reqData.get("recid") != null ? reqData.get("recid")[0] : null);//reqData.get("recid");
        if (recordId != null && ! recordId.equals(""))
        {
            // It's not empty or blank, we're probably updating an existing record
            int recnum = 0;
            try
            {
                recnum = Integer.parseInt(recordId);
            }
            catch (NumberFormatException nfe)
            {
                logger.warn("Invalid record ID string [{}]", recordId);
            }

            if (recnum != 0) // We're definitely updating an existing record.
            {
                logger.info("  Updating record #{} in {} for UserID {}", new Object[] { recnum, tableName, userId });
                // collect and validate the fields
                String[][] changes = validateResultVO.getValidateParameters();

                // update the data if it's valid
                if (validateResultVO.isSuccess())
                {
                    // UPDATE tableName SET
                    //    parm1=value1,
                    //    parm2=value2,
                    //      ...
                    //    parmN=valueN
                    // WHERE UserID = ? AND ID = ?
                    //  ((where userid==mui.getUserID() and id==recnum))
                    int count = rh.update(tableName, recnum, changes[0], changes[1]);
                    logger.debug("doPost: update count is {}", count);

                    String message = "";
                    boolean success = false;
                    // If no records were updated (an error occurred) set the rec number to -1
                    // which is the client-side flag for the JSP to show an error.
                    if (count < 1)
                    {
                        success = false;
                        int errorCodeDataTruncation = Integer.parseInt(MivConstants.getInstance().getString("MySql.ErrorCode.DataTruncation"));
                        recnum = -1;
                        if (count == errorCodeDataTruncation)
                        {
                            // convert bytes into kilobytes
                            int contentMaxLimit = Integer.parseInt(MivConstants.getInstance().getString("WYSIWYG.CONTENT.MAX_LIMIT"))/1024;
                            message = ErrorMessages.getInstance().getString("errors.content.max.limit", contentMaxLimit, "K");
                            recnum = errorCodeDataTruncation;
                            resultVO = new ResultVO(success, Arrays.asList(message), Arrays.asList("content"), String.valueOf(recnum));
                        }
                        else
                        {
                            message = "update failed";
                            resultVO = new ResultVO(success, Arrays.asList(message), null, String.valueOf(recnum));
                        }

                    }
                    else
                    {
                        success = true;
                        message = recOrSection + " updated for "
                                + MIVUtil.escapeChar('\'', "\\\\", strProps.getProperty("pagehead"));

                        resultVO = new ResultVO(success, Arrays.asList(message), null, String.valueOf(recnum));
                    }

//                    // if the recnum==-1 we should try to provide a better message than "update failed"
//                    // maybe we can get something from the rh.update exception (uh, that was swallowed)
//                    String message = recnum == -1 ? "update failed" :
//                                                    recOrSection + " updated for " +
//                                                    MIVUtil.escapeChar('\'', "\\\\", strProps.getProperty("pagehead"));


                    logger.info("message at validator:: {}", message);
//                    out.print("{ success:"+success+", recnum:"+recnum+", message:'"+message+"', isCustomMsg:'"+isCustomMsg+"', timestamp:" + System.currentTimeMillis() +" }");

                }
            }
            else
            {
                // recnum is 0 *probably* because it didn't convert to an int
            }
        }
        else // recordId string is null or empty - Adding a new record
        {
            logger.info("_AUDIT:  Adding NEW record in {} for UserID {}", tableName, userId);
            String[][] assignments = validateResultVO.getValidateParameters();


            // Do all this next junk only if 'display' wasn't passed
            if (!reqData.containsKey("display"))
            {
                logger.debug("EditRecord-doPost() : Adding 'display' to the parameters.");
                List<String> fields = new LinkedList<String>(Arrays.asList(assignments[0]));
                List<String> values = new LinkedList<String>(Arrays.asList(assignments[1]));

                fields.add("display");
                values.add("1");

                assignments[0] = fields.toArray(new String[0]);
                assignments[1] = values.toArray(new String[0]);
                //rh.insert(tableName, fields.toArray(new String[0]), values.toArray(new String[0]));
            }

            logger.debug("EditRecord-doPost() : Already found 'display'; not adding it to the parameters.");

            if (validateResultVO.isSuccess())
            {
                int newkey = rh.insert(tableName, assignments[0], assignments[1]);
                String message = "";
                boolean success = false;

                // if the newkey==-1 we should try to provide a better message than "save failed"
                // maybe we can get something from the rh.insert exception (uh, that was swallowed)
                if (newkey < 1)
                {
                    success = false;
                    int errorCodeDataTruncation = Integer.parseInt(MivConstants.getInstance().getString("MySql.ErrorCode.DataTruncation"));

                    if (newkey == errorCodeDataTruncation)
                    {
                        // convert bytes into kilobytes
                        int contentMaxLimit = Integer.parseInt(MivConstants.getInstance().getString("WYSIWYG.CONTENT.MAX_LIMIT"))/1024;
                        message = ErrorMessages.getInstance().getString("errors.content.max.limit", contentMaxLimit, "K");
                        resultVO = new ResultVO(success, Arrays.asList(message), Arrays.asList("content"), String.valueOf(newkey));
                    }
                    else
                    {
                        message = "save failed";
                        resultVO = new ResultVO(success, Arrays.asList(message), null, String.valueOf(newkey));
                    }
                }
                else
                {
                    success = true;
                    logger.info("_AUDIT:  Successfully added NEW record #{} in {} for UserID {}", new Object[] { newkey, tableName, userId });
                    message = recOrSection + " added to " +
                            MIVUtil.escapeChar('\'', "\\\\", strProps.getProperty("pagehead"));
                    resultVO = new ResultVO(success, Arrays.asList(message), null, String.valueOf(newkey));

                }
            }

        }

        logger.trace("\n ***** Leaving EditRecordHelper:processRecord ***************\n");
        return resultVO;
    }


    /** Fields that come in the form POST but aren't part of the record. */ // TODO: make this list a config property
    private static final String[] skipList = { "rectype", "recid", "save", "citation", "subtype" };
    protected static final java.util.Set<String> skipFields = new java.util.HashSet<String>();
    static {
        for (String s : skipList) { skipFields.add(s); }
    }


    /**
     * Gather the request parameters and values into parallel arrays.
     * The first array is the list of parameter names; the second is the values.
     *
     * @param reqData Map of request's parameter name and value
     * @return An array of exactly two (2) String arrays.
     */
    ValidateResultVO collectValidateParameters(Map <String, String[]> reqData, Properties cfgProps)
    {
        ValidateResultVO validateResultVO = null;

        Validator validator = new Validator(reqData);
        List<String> fields = new LinkedList<String>();
        List<String> values = new LinkedList<String>();
        String[][] retArray = { {}, {} };
        String[] returnType = {};

        for (String pname : reqData.keySet())
        {
            if (skipFields.contains(pname)) continue;
            //dojo sends _displayed_ fields from IE
            if (pname.endsWith("_displayed_")) continue;

            String key = "constraints-" + pname;
            String constraints = cfgProps.getProperty(key);
            //TODO: Currently the EditRecord only processes one value for a parameter
            String value = reqData.get(pname)[0];

            // to skip manipulation columns, not the part of table columns
            if (constraints != null && constraints.equals("skip")) {
                continue;
            }

            value = MIVUtil.replaceNumericReferences(value);

            if (constraints != null)
            {
                String[] temp = StringUtil.splitToArray(constraints);
                value = validator.validate(pname, value, temp);
            }

            fields.add(pname);
            values.add(value);
        }

        retArray[0] = fields.toArray(returnType);
        retArray[1] = values.toArray(returnType);

        if (validator.isValid()) {
            validateResultVO = new ValidateResultVO(retArray);
        }
        else {
            validateResultVO = new ValidateResultVO(validator.getErrorMessageList(), validator.getErrorFieldList());
        }

        return validateResultVO;
    }


    /**
     * Get the form data into a map of String,String
     * <br> TODO: should probably be changed to return <code>Map&lt;String,String[]></code>
     * rather than <code>Map&lt;String,Object></code> — callers will have to be changed too.
     * @param request
     * @return
     */
    public static Map<String,Object> getFormParameterMap(HttpServletRequest request)
    {
        Map<String,String[]> params = request.getParameterMap();
        Map<String, Object> result = new HashMap<String, Object>();

        for (String key : params.keySet())
        {
            String[] parameterValue = request.getParameterValues(key);

            result.put(key, parameterValue.length > 1 ? parameterValue : parameterValue[0]);
        }

        return result;
    }
}
