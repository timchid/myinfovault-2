/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentCreator.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import edu.ucdavis.mw.myinfovault.service.biosketch.DocumentCreatorService;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.document.Document;
import edu.ucdavis.myinfovault.document.DocumentFile;
import edu.ucdavis.myinfovault.document.DocumentFormat;

/**
 * TODO: Needs javadoc
 * @author Lawrence Fyfe
 * @since MIV 2.0
 */
public class DocumentCreator extends MIVServlet
{
    private static final long serialVersionUID = 200709271544L;

    // used to get SectionFetcher object to call getHeaderMap
    //MIVUserInfo mui;
   //LinkedHashMap<String,Map<String,String>> sectionHeaderMap  =  null;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {

        String listURL = "MIVMain";


        // Get the Spring web application context
        WebApplicationContext ctx =
            WebApplicationContextUtils.getRequiredWebApplicationContext(
                    mivSession.get().getHttpSession().getServletContext());
        // Get the documentCreatorService
        DocumentCreatorService documentCreatorService = (DocumentCreatorService) ctx.getBean("documentCreatorService");

        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        try
        {
            String collectionID = request.getParameter("collectionID");
//            String biosketchID = request.getParameter("biosketchID");
            List<Document>documents = null;

            String action = request.getParameter("action");
            // Default to create
            if (action == null) {
                action = "create";
            }
            // default to "1"
            if (collectionID == null) {
                collectionID = "1";
            }

            // default for non finalized packet
            listURL = "/jsp/packetPreview.jsp?action=Create";
            if (action.equalsIgnoreCase("view")) {
                listURL = "/jsp/packetPreview.jsp?action=View";
            }

            if (collectionID.equals("1"))
            {
//                if (finalize)
//                {
//                    sequence = request.getParameter("sequence");
//                    listURL = "MIVPacketSend?sequence=" + sequence;
//                }

                if (action.equalsIgnoreCase("create")) {
                    documents = documentCreatorService.createPacket(mui,Integer.parseInt(collectionID));
                //    documents = documentBuilder.generateDocuments();
                }
                else
                {
                 // Not creating, just get what is there
                    documents = documentCreatorService.viewPacket(mui,Integer.parseInt(collectionID));
                }
                // get the combined packet document and place in a separate attribute
                Document removeCombinedDocument = null;
                for (Document document : documents)
                {
                    if (removeCombinedDocument != null)
                    {
                        break;
                    }
                    List<DocumentFile>documentFiles = document.getOutputDocuments();
                    for (DocumentFile documentFile : documentFiles)
                    {
                        if (documentFile.getDocumentFormat() == DocumentFormat.COMBINED_PDF)
                        {
                            removeCombinedDocument = document;
                            request.setAttribute("combinedPacketDocument", documentFile);
                            break;
                        }
                    }
                }
                if (removeCombinedDocument != null)
                {
                    documents.remove(removeCombinedDocument);
                }
            }
//            else if (biosketchID != null)
//            {
//                BiosketchService biosketchService = (BiosketchService) ctx.getBean("biosketchService");
//                Biosketch biosketch = biosketchService.loadBiosketchData(Integer.parseInt(biosketchID));
//
//                documents = documentCreatorService.createBiosketch(mui, biosketch, urlPath);
//                listURL = "CvShow?action=cv";
//            }

            request.setAttribute("documents", documents);
          //  request.setAttribute("packetDocument", documentData.getPacketDocument());
            request.setAttribute("hasDocuments",!documents.isEmpty());
            //response.encodeRedirectURL("documents");
            //response.sendRedirect(listURL);
            logger.trace("Dispatch target is {}", listURL);
            dispatch(listURL, request, response);
            //log.exiting(className, "doGet");
            response.setContentType("text/html");
        }
        catch (IOException ioe)
        {
            logger.error("Unable to create packet document(s):", ioe);
            //ioe.printStackTrace(System.out);
        }
    }
}
