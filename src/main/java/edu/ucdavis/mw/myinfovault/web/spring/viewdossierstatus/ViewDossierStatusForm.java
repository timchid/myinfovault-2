package edu.ucdavis.mw.myinfovault.web.spring.viewdossierstatus;

import java.io.Serializable;

/**
 * This form is only used to allow for the standard setup (MivFormAction#setupForm) to be executed at the start-state
 * of a webflow. One important part of the setup is to properly initialize the breadcrumbs in a view.
 *
 * @author Rick Hendricks
 * @since MIV 5.0
 */
public class ViewDossierStatusForm implements Serializable
{
    private static final long serialVersionUID = 2015082609000000L;
}
