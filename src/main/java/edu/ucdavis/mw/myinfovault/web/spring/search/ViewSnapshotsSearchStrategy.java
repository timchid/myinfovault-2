/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewSnapshotsSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.google.common.primitives.Ints;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.viewdossiersnapshots.SnapshotValidator;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * ViewSnapshotsSearchStrategy - Implement the strategy to be used when searching for available
 * snapshots/archives that a user is allowed to view.
 *
 * The search strategy is determined by the role of the searching user.
 * Candidates, Department Admins, School Admins, and MIV Admins are all allowed to view snapshots.
 * However each role is restricted to the type of snapshot which will be available to view as follows:
 *
 *   - An MIV Admin may view snapshots and archives of all types.
 *   - A School Admin may view:
 *       School snapshots snapshots for their own school
 *       Department snapshots for departments within their own school
 *       The Admin archive
 *   - A Department Admin may view:
 *       Department Snapshots for their own department
 *       The Admin archive
 *   - A Candidate may view:
 *       The Candidate archive
 *
 * @author rhendric
 *
 */
public class ViewSnapshotsSearchStrategy extends DefaultSearchStrategy
{
    /**
     * Search for dossiers with archives and snapshots.
     *
     * @param criteria
     * @param user MIVUser requesting to view snapshots
     * @return List of users associated with the dossiers
     * @throws WorkflowException
     */
    @Override
    public List<MivActionList> dossierSearch(SearchCriteria criteria, MIVUser user, List<Dossier>dossiers) throws WorkflowException
    {
        final String tname = criteria.getInputName().toLowerCase();
        final String linit = criteria.getLname().toLowerCase();
        final int realUserId = user.getUserId();
        final int schoolId = criteria.getSchoolId();
        final int deptId = criteria.getDepartmentId();
        final int deptSchoolId = criteria.getDeptSchoolId();

        List<MivActionList> list = null;

        MivPerson mivPerson = user.getTargetUserInfo().getPerson();

        boolean showAll = true;

        int[] excludeList = { mivPerson.getUserId(), realUserId };
        if (showAll || mivPerson.hasRole(MivRole.VICE_PROVOST_STAFF))
        {
            excludeList[0] = 0; // do not exclude from the list
            excludeList[1] = 0;
        }

        List<MivActionList> people = null;

        // findPeople by school and department if they were provided
        if (deptSchoolId > 0 && deptId > 0)
        {
            people = this.findPeople(dossiers, mivPerson, showAll, deptSchoolId, deptId, excludeList);
        }
        // otherwise search by school (dept is zero(?))
        else
        {
            people = this.findPeople(dossiers, mivPerson, showAll, schoolId, deptId, excludeList);
        }

        if (!people.isEmpty())
        {
            // need to filter by school and department
            if (deptSchoolId > 0 && deptId > 0)
            {
                list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                    @Override
                    public boolean include(MivActionList item)
                    {
                        return (item.getSchoolId() == deptSchoolId && item.getDepartmentId() == deptId);
                    }
                }, people);
            }
            // need to filter by school
            else if (schoolId > 0)
            {
                list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                    @Override
                    public boolean include(MivActionList item)
                    {
                        return (item.getSchoolId() == schoolId);
                    }
                }, people);
            }
            // the name was entered so search for that
            else if (StringUtils.hasText(tname))
            {
                list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                    @Override
                    public boolean include(MivActionList item)
                    {
                        return matchMivActionListUserName(item, tname);
                    }
                }, people);

                List<MivActionList> exactMatch = new ArrayList<MivActionList>();
                for (MivActionList oneDossier : list)
                {
                    if (matchMivActionListUserName(oneDossier, tname))
                    {
                        if (!exactMatch.contains(oneDossier))
                        {
                            exactMatch.add(oneDossier);
                        }
                    }
                }

                if (exactMatch.size() > 0)
                {
                    return exactMatch;
                }
            }
            // search for last name
            else
            {
                list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                    @Override
                    public boolean include(MivActionList item)
                    {
                        return ((linit.indexOf("all") == 0) || (item.getSurname().toLowerCase().indexOf(linit) == 0));
                    }
                }, people);
            }

            people = null;
            // See if this dossier was created prior to Jan 19, 2010. There were no snapshots
            // produced for locations where the dossier was routed  prior to that date
            people = applyFilter(new SearchFilterAdapter<MivActionList>() {
                @Override
                public boolean include(MivActionList item)
                {
                    return !SnapshotValidator.isPriorToSnapshotsProducedStartDate(item.getSubmitDate());
                }
            }, list);

        }

        return people;
    }


    @Override
    protected List<MivActionList> findPeople(List<Dossier> dossiers, MivPerson mivPerson, boolean showAll, int schoolId, int deptId, int... dropUsers)
    {
        // apply Fliter -- do not add if dropUser = dossier.getUserId()
        List<MivActionList> people = new ArrayList<MivActionList>();
        for (Dossier ds : dossiers)
        {
            // Skip this dossier if it belongs to someone in the list of users to drop
            if (Ints.contains(dropUsers, ds.getAction().getCandidate().getUserId())) continue;

            String theDossierLocation = ds.getLocation();
            Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAppointments = ds.getAppointmentAttributes();
            // Loop through the appointments
            for (DossierAppointmentAttributeKey key : dossierAppointments.keySet())
            {
                DossierAppointmentAttributes dossierAppointmentAttributes = dossierAppointments.get(key);
                MivActionList mal = fillDossierPerson(ds, theDossierLocation, dossierAppointmentAttributes, schoolId, deptId, true);
                if (mal != null)
                {
                    // Only add one person object for each appointment
                    if (!people.contains(mal)) {
                        people.add(mal);
                    }
                    break;
                }
            }

        }

        return people;
    }



    /**
     * Determine the permission necessary to view a snapshot. The permission is
     * determined by the location and role for which it was created.
     *
     * @param snapshotDto
     * @return permission
     */
    private String getSnapshotPermission(SnapshotDto snapshotDto)
    {
        // Default permission
        String permission = Permission.VIEW_SNAPSHOTS;

        // If the snapshot was created at the Archive location, it is an archive
        if (DossierLocation.mapWorkflowNodeNameToLocation(snapshotDto.getSnapshotLocation()) == DossierLocation.ARCHIVE)
        {
            /// Get the role for which the snapshot was created
            switch (MivRole.valueOf(snapshotDto.getMivRoleId()))
            {
                case ARCHIVE_ADMIN:
                    permission = Permission.VIEW_FULL_ARCHIVES;
                    break;
                case ARCHIVE_USER:
                    permission = Permission.VIEW_ADMIN_ARCHIVES;
                    break;
                case CANDIDATE:
                    // fall-through to default
                default:
                    permission = Permission.VIEW_CANDIDATE_ARCHIVES;
                    break;
            }
        }

        return permission;
    }



    @Override
    SearchFilter<SnapshotDto> getSnapshotFilter(MivPerson person)
    {
        return new SnapshotFilter(person);
    }


    private class SnapshotFilter extends SearchFilterAdapter<SnapshotDto>
    {
        private final MivPerson mivPerson;

        SnapshotFilter(MivPerson person)
        {
            this.mivPerson = person;
        }

        @Override
        public boolean include(SnapshotDto item)
        {
            // Authorize snapshot viewing based on Location
            AttributeSet permissionDetails = new AttributeSet();
            permissionDetails.put("SNAPSHOTLOCATION", item.getSnapshotLocation());

            // Get the permission required to view this item
            String permission = getSnapshotPermission(item);

            // School, department and role qualifiers
            AttributeSet qualification = new AttributeSet();

            // Get the dossier owners userid
            String dossierOwnerId = null;
            try
            {
                dossierOwnerId = dossierService.getDossier(item.getDossierId()).getAction().getCandidate().getUserId()+"";
            }
            catch (WorkflowException e)
            {
                logger.error("Unable to authorize viewing of " + item.getSnapshotLocation() +
                             " snapshot for dossier " + item.getDossierId() + " by " + mivPerson, e);
                return false;
            }

            qualification.put(Qualifier.USERID, dossierOwnerId);
            qualification.put(Qualifier.DEPARTMENT, Integer.toString(item.getDepartmentId()));
            qualification.put(Qualifier.SCHOOL, Integer.toString(item.getSchoolId()));
            qualification.put(Qualifier.ROLE, item.getMivRoleId());

            if (authorizationService.isAuthorized(mivPerson, permission, permissionDetails, qualification)) {
                return true;
            }

            return false;
        }
    }



}
