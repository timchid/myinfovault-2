/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ValidationErrorResource.java
 */
package edu.ucdavis.mw.myinfovault.web;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * Base class which represents a validation error in an API call. Contains a
 * list of errors which should contain any errors caught by validation.
 * Subclasses MUST implement a validation function which validates the resource.
 *
 * @author Jacob Saporito
 * @since 5.0
 */
public abstract class ValidationErrorResource extends ResourceSupport
{
    public List<String> errors = new ArrayList<String>();
    @JsonIgnore protected Boolean valid = false;
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * @return the errors
     */
    public List<String> getErrors()
    {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<String> errors)
    {
        this.errors = errors;
    }

    public void addError(String error)
    {
        errors.add(error);
    }

    public Boolean isValid()
    {
        return valid;
    }

    public abstract Boolean validate(ResourceSupport resource);
}