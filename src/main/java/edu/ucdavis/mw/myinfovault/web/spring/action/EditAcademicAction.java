/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditAcademicAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.PolarResponse;
import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDetails;
import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Step;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.events2.AcademicActionEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.academicaction.AcademicActionService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.decision.DecisionService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService;
import edu.ucdavis.mw.myinfovault.util.DateUtil;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.util.SelectedFilter;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivPropertyEditors;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Create or edit an academic action.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class EditAcademicAction extends MivFormAction
{
    private final DossierService dossierService;
    private final AcademicActionService aafService;
    private final WorkflowService workflowService;
    private final DecisionService decisionService;

    /**
     * Create the edit an academic action spring form action bean.
     *
     * @param userService Spring-injected user service
     * @param authorizationService Spring-injected authorization service
     * @param dossierService Spring-injected dossier service
     * @param aafService Spring-injected academic action form service
     * @param workflowService Spring-injected workflow service
     */
    public EditAcademicAction(UserService userService,
                              AuthorizationService authorizationService,
                              DossierService dossierService,
                              AcademicActionService aafService,
                              WorkflowService workflowService,
                              DecisionService decisionService)
    {
        super(userService, authorizationService);

        this.dossierService = dossierService;
        this.aafService = aafService;
        this.workflowService = workflowService;
        this.decisionService = decisionService;
    }


    /**
     * Checks if real, logged-in person has permission to create/edit academic actions.
     *
     * @param context Request context from webflow
     * @return Webflow event status; allowed or denied
     */
    public Event hasPermission(RequestContext context)
    {
        return authorizationService.hasPermission(getRealPerson(context),
                                                  Permission.EDIT_DOSSIER,
                                                  null)
             ? allowed()
             : denied();
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    protected Object createFormObject(RequestContext context)
    {
        Dossier dossier = getDossier(context);

        DossierActionType actionType = (DossierActionType) context.getFlowScope().get("actionType");
        DossierDelegationAuthority delegationAuthority = (DossierDelegationAuthority) context.getFlowScope().get("delegationAuthority");

        if (dossier != null)
        {
            // create academic action form with dossier in the conversation scope
            AcademicActionForm form = new AcademicActionForm(aafService.getBo(dossier));

            // optionally, bind the action type and delegation authority set in flow scope
            if (actionType != null) form.setActionType(actionType);
            if (delegationAuthority != null) form.setDelegationAuthority(delegationAuthority);

            return form;
        }

        // no dossier, instantiate a new academic action form
        return new AcademicActionForm(actionType);
    }


    /**
     * Save the academic action form.
     *
     * @param context Webflow request context
     * @return Webflow event status, either success or error
     */
    public Event saveAction(RequestContext context)
    {
        Event resultEvent;
        Dossier dossier = getDossier(context);

        // Reject the edit if the dossier is no longer in the location where the user started editing.
        if (dossier != null)
        {
            try {
                Dossier origDossier = dossierService.getDossier(dossier.getDossierId());
                if (dossier.getDossierLocation() != origDossier.getDossierLocation()) {
                    getFormErrors(context).reject(null, "The dossier has been routed since you started editing it. Your changes can not be saved.");
                    return error();
                }
            }
            catch (WorkflowException e) {
                throw new MivSevereApplicationError("Error occurred while trying to verify dossier location", e);
            }
        }

        // Find the (equivalent) office where the dossier is currently located, to determine
        // what subset of the Action/RAF data will be saved.
        DossierLocation office =
                // A null Dossier hasn't been created yet; Dossiers are initiated at the Department.
                dossier == null ? DossierLocation.DEPARTMENT
                                : DossierLocation.mapLocationToOffice(dossier.getDossierLocation());

        logger.debug("\n\n   Figuring out what kind of save to do for location: " + office + "\n\n");
        switch (office) {

            case PACKETREQUEST:
            case DEPARTMENT:
                resultEvent = saveActionDepartment(context);
                break;

            case SCHOOL:
                resultEvent = saveActionSchool(dossier, context);
                break;

            case VICEPROVOST:
                resultEvent = saveActionViceProvost(dossier, context);
                break;

            default:
                throw new MivSevereApplicationError("Action changes can not be saved at location: " + dossier.getDossierLocation());
        }

        return resultEvent;
    }


    /**
     * Save and initiate action in workflow. Additionally, save appointee bound to the form.
     *
     * @param context Webflow request context
     * @return Webflow event status, either success or error
     */
    private Event saveActionDepartment(RequestContext context)
    {
        logger.debug("  Saving at the DEPARTMENT");
        AcademicActionForm form = (AcademicActionForm) getFormObject(context);

        MivPerson realPerson = getRealPerson(context);
        MivPerson shadowPerson = getShadowPerson(context);

        AcademicActionBo aafBo = null;

        try
        {
            MivPerson candidate = form.getCandidate();

            // Get the previous appointments
            Collection <Appointment> prevAppointments = new LinkedHashSet<Appointment>(candidate.getAppointments());

            /*
             * Update the appointee for new appointments only.
             */
            if (form.getActionType() == DossierActionType.APPOINTMENT)
            {
                // update the appointee name only when he is not current employee
                if (form.getCurrentEmployee() == PolarResponse.NO)
                {
                    candidate.setName(form.getGivenName(), form.getMiddleName(), form.getSurname(), null);
                }

                // update and save appointee with new proposed assignments
                updateAppointee(candidate, form.getAssignmentStatuses().get(StatusType.PROPOSED), shadowPerson.getUserId());
            }

            // Get the current appointments
            Collection <Appointment> currentAppointments = candidate.getAppointments();

            // create new academic action
            AcademicAction action = new AcademicAction(form.getCandidate(),
                                                       form.getActionType(),
                                                       form.getDelegationAuthority(),
                                                       form.getEffectiveDateAsDate(),
                                                       form.getRetroactiveDateAsDate(),
                                                       form.getEndDateAsDate(),
                                                       form.getAccelerationYears());

            /*
             * See if there is an existing dossier in process. This dossier will
             * have been set in the conversation scope before entering this flow.
             */
            Dossier dossier = getDossier(context);
            AcademicAction formerAction = null;

            // Initiate dossier a new dossier if one doesn't exist.
            if (dossier == null)
            {
                dossier = dossierService.initiateDossierRouting(action, shadowPerson);

                // create AppointmentDetails
                AppointmentDetails apptDetails = new AppointmentDetails(
                        form.getCurrentEmployee(),
                        form.getRepresentedEmployee(),
                        form.getUnionNoticeRequired(),
                        form.getLaborRelationsNotified());

                aafBo = new AcademicActionBo(dossier, apptDetails, form.getAssignmentStatuses());
            }
            // otherwise, re-initiate the current one
            else
            {
                DossierLocation currentLocation = dossier.getDossierLocation();
                DossierLocation initialLocation = dossierService.getInitialWorkflowNode(dossier).getNodeLocation();
                formerAction = dossier.getAction();

                // Return to reinitialize the dossier with possibly changed appointment
                // if the dossier is at the department and wasn't initialized there.
                if (currentLocation == DossierLocation.DEPARTMENT
                 && initialLocation != DossierLocation.DEPARTMENT)
                {
                    dossier = dossierService.returnDossierToPreviousNode(shadowPerson,
                                                                         realPerson,
                                                                         dossier,
                                                                         "re-initiate apointment",
                                                                         initialLocation,
                                                                         currentLocation);
                    // Re-initiate the dossier, removing uploads if needed
                    dossier = dossierService.initiateDossierRouting(dossier, action, findRemovableUploads(prevAppointments, currentAppointments), shadowPerson);
                }

                // This dossier has already been initiated, get the current AcademicActionForm
                aafBo = aafService.getBo(dossier);

                aafBo.getApptDetails().setCurrentEmployee(form.getCurrentEmployee());
                aafBo.getApptDetails().setRepresented(form.getRepresentedEmployee());
                aafBo.getApptDetails().setNoticeToUnion(form.getUnionNoticeRequired());
                aafBo.getApptDetails().setNoticeToLaborRelations(form.getLaborRelationsNotified());

                // Set the appointment assignments which may have changed
                aafBo.setAssignmentStatuses(form.getAssignmentStatuses());

//                // Re-initiate the dossier only if currently at the initial location
//                if (workflowService.getInitialNode(dossier) == workflowService.getCurrentWorkflowNode(dossier))
//                {
//                    // Initiate the dossier, removing uploads if needed
//                    dossierService.initiateDossierRouting(dossier, action, findRemovableUploads(prevAppointments, currentAppointments), shadowPerson);
//                }

                // The action is set on the dossier *after* the above node check because the delegation authority may have changed
                // making the current location of the dossier invalid. When the dossier is updated later in the process, the location will
                // be validated and updated if necessary.
                dossier.setAction(action);
            }

            // ensure dossier is in the conversation scope
            context.getConversationScope().put(DOSSIER, dossier);

            aafService.saveAcademicAction(aafBo, shadowPerson, realPerson);

            // post academic action event
            EventDispatcher2.getDispatcher().post(new AcademicActionEvent(realPerson, dossier, formerAction));
        }
        catch (WorkflowException e)
        {
            /*
             * FIXME: What should happen here?
             *  - throw a severe application error
             *  - return the error event and form error message
             */
            getFormErrors(context).rejectValue(null, e.getMessage());

            return error();
        }

        return success();
    }


    /**
     * Save an action edited at the School (Dean's Office)
     * Only the Delegatin of Authority may be changed at this location;
     * ignore any other changes from the client.
     * @param dossier
     * @param context Webflow request context
     * @return Webflow event status, either success or error
     */
    private Event saveActionSchool(Dossier dossier, RequestContext context)
    {
        logger.debug("  Saving at the SCHOOL\n");

        AcademicActionForm form = (AcademicActionForm) getFormObject(context);

        AcademicAction action = dossier.getAction();
        DossierDelegationAuthority currentDelegation = action.getDelegationAuthority();

        // save action only if delegation authority has changed date has changed
        if (form.getDelegationAuthority() != currentDelegation)
        {
            //TODO: this should be moved into DelegationAuthorityChangeEvent when it exists
            /*
             * If delegation authority has changed, remove all decision signatures, but exclude the DC
             */
            List<MivDocument>excludeDocs = new ArrayList<MivDocument>();
            excludeDocs.add(MivDocument.DISCLOSURE_CERTIFICATE);
            excludeDocs.add(MivDocument.JOINT_DISCLOSURE_CERTIFICATE);
            decisionService.removeDecisions(dossier, excludeDocs);
            AcademicAction newAction = new AcademicAction(action.getCandidate(),
                                                          action.getActionType(),
                                                          form.getDelegationAuthority(),
                                                          action.getEffectiveDate(),
                                                          action.getRetroactiveDate(),
                                                          action.getEndDate(),
                                                          action.getAccelerationYears());

            return updateAction(dossier, newAction, context);
        }

        // Assume success for the case where we don't even try to save
        return success();
    }


    /**
     * Save an action edited at the Vice Provost's office.
     * Only a retroactive date can be added or edited at this location;
     * ignore any other changes from the client.
     * @param dossier
     * @param context Webflow request context
     * @return Webflow event status, either success or error
     */
    private Event saveActionViceProvost(Dossier dossier, RequestContext context)
    {
        logger.debug("  Saving at the VP OFFICE");

        AcademicActionForm form = (AcademicActionForm) getFormObject(context);

        AcademicAction action = dossier.getAction();

        Date currentRetroDate = dossier.getAction().getRetroactiveDate();
        Date formRetroDate = form.getRetroactiveDateAsDate();

        // save action only if retroactive date has changed
        if (!DateUtil.isSameDay(currentRetroDate, formRetroDate))
        {
            logger.debug("Retro Date in form is: [{}]", formRetroDate);

            AcademicAction newAction = new AcademicAction(action.getCandidate(),
                                                          action.getActionType(),
                                                          action.getDelegationAuthority(),
                                                          action.getEffectiveDate(),
                                                          formRetroDate,
                                                          action.getEndDate(),
                                                          action.getAccelerationYears());

            return updateAction(dossier, newAction, context);
        }

        // Assume success for the case where we don't even try to save
        return success();
    }


    /**
     * Update dossier with new academic action. Re-saving the Action causes PDFs to be regenerated.
     *
     * @param dossier dossier for action update
     * @param newAction new academic action for the dossier
     * @param context Webflow request context
     * @return Webflow event status, either success or error
     */
    private Event updateAction(Dossier dossier,
                               AcademicAction newAction,
                               RequestContext context)
    {
        MivPerson realPerson = getRealPerson(context);

        AcademicAction formerAction = dossier.getAction();

        try
        {
            // update dossier with new action
            dossier.setAction(newAction);
            dossierService.saveDossier(dossier);

            // regenerate the RAF PDFs
            aafService.createPdfs(aafService.getBo(dossier));

            // post academic action event
            EventDispatcher2.getDispatcher().post(new AcademicActionEvent(realPerson, dossier, formerAction));
        }
        catch (WorkflowException e)
        {
            getFormErrors(context).rejectValue(null, e.getMessage());
            return error();
        }

        return success();
    }


    /**
     * Check the previous appointments against the current appointments. IF the appointments or the primary
     * department have changed, return a list of the uploads that must be removed.
     * @param prevAppointments
     * @param currentAppointments
     * @return List of dossier appointment attribute keys, representing the uploaded document that must be removed.
     */
    private List<DossierAppointmentAttributeKey> findRemovableUploads(Collection<Appointment>prevAppointments, Collection<Appointment>currentAppointments)
    {
        List<DossierAppointmentAttributeKey> listRemoveUploads = new ArrayList<DossierAppointmentAttributeKey>();

        // The collections might be equal but the primary scope may have changed
        Scope prevPrimaryScope = Scope.NoScope;

        // See if the appointments have changed, if so remove the uploads from the removed appointments
        // Get the previous primaryScope and check for a removed appointment
        for (Appointment appointment: prevAppointments)
        {
            if (!currentAppointments.contains(appointment))
            {
                listRemoveUploads.add(new DossierAppointmentAttributeKey(appointment.getScope().getSchool(), appointment.getScope().getDepartment()));
            }
            if (appointment.isPrimary())
            {
                prevPrimaryScope = appointment.getScope();
            }
        }

        // Check prevPrimaryScope against current
        for (Appointment appointment : currentAppointments)
        {
            if (appointment.isPrimary() && !appointment.getScope().matches(prevPrimaryScope))
            {
                listRemoveUploads.add(new DossierAppointmentAttributeKey(appointment.getScope().getSchool(), appointment.getScope().getDepartment()));
                listRemoveUploads.add(new DossierAppointmentAttributeKey(prevPrimaryScope.getSchool(),prevPrimaryScope.getDepartment()));
            }
        }
        return listRemoveUploads;
    }


    /**
     * Update appointee with new assignments and save.
     *
     * @param person appointee to update and save
     * @param assignments proposed assignments for the appointee
     */
    public void updateAppointee(MivPerson person, List<Assignment> assignments, int actorId)
    {

        person.loadDetail();

        // See if this appointee is an active staff person
        boolean activeStaff = person.isActive() &&
                              person.getPrimaryRoleType() != null &&
                              person.getPrimaryRoleType().getCategory() == MivRole.Category.STAFF;

        /*
         * Remove any existing role assignments for non-active or non-staff roles members only
         */
        if (!activeStaff)
        {
            ((MivPersonInternal)person).clearRoles();      // Clears both assignments and roles
        }
        // An active staff role keep the MIV_USER role and only have the non-admin roles removed
        else
        {
            for (AssignedRole assignedRole : person.getAssignedRoles())
            {
                if ((assignedRole.getRole() != MivRole.MIV_USER) &&
                    (assignedRole.getRole().getCategory() != MivRole.Category.STAFF))
                {
                    person.removeRole(assignedRole);
                    //TODO: need to remove the statement below, it doesn't work anymore but should it?
                    //person.removeRole(assignedRole.getRole());
                }
            }
        }

        /*
         * Remove any existing appointments for all
         */
        ((MivPersonInternal)person).clearAppointments();

        // Loop through the proposed appointments and add
        // Note that if we are adding the appointee role(s) to an active staff person, the appointee role(s) will NOT be primary
        for (Assignment assignment : assignments)
        {
            person.addAppointment(new Appointment(assignment.getScope(), assignment.isPrimary()));

            AssignedRole role = new AssignedRole(MivRole.APPOINTEE,
                                                 assignment.getScope(),
                                                 !activeStaff);
            if (!activeStaff && assignment.isPrimary())
            {
                person.setPrimaryRole(role);
            }

            person.addRole(role);
        }

        MivPerson savedPerson = userService.saveAppointee(person, actorId);
        if (savedPerson == null) {
            throw new MivSevereApplicationError("Failed to save new appointee " + person.getDisplayName());
        }
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#bindAndValidate(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event bindAndValidate(RequestContext context) throws Exception
    {
        AcademicActionForm form = (AcademicActionForm) getFormObject(context);

        // default binding
        Event bindResult = super.bind(context);

        /*
         * Continue if form binding was successful.
         */
        if ("success".equalsIgnoreCase(bindResult.getId()))
        {
            MivPerson candidate = null;

            // From an existing dossier candidate
            Dossier dossier = getDossier(context);
            if (dossier != null)
            {
                candidate = dossier.getAction().getCandidate();
            }

            final String personUUID = form.getLdapPersonUUID();
            // If a current UCD employee, from MIV user ID or LDAP Person UUID
            if (form.getCurrentEmployee() == PolarResponse.YES)
            {
                // MIV user ID
                if (candidate == null && form.getMivUserID() != 0)
                {
                    candidate = userService.getPersonByMivId(form.getMivUserID());
                }

                // LDAP Person UUID
                if (candidate == null && StringUtils.isNotBlank(personUUID))
                {
                    // This doesn't work. The current UserService only gets MIV_USERS by PersonUUID
                    candidate = userService.getPersonByPersonUuid(personUUID);
                }
            }
        // We can't currently (2014-01-22) get an arbitrary LDAP person from their UUID, so if the candidate
        // is *still* null do the same as if currentEmployee was NO
            // From a given, middle, and surname.
            /*else*/ if (candidate == null)
            {
                // FIXME: Use the 4-arg method instead, to include the home department / primary appointment.
                //        This 3-arg method is going to be removed.
                //        Resolving additions/deletions and getting the proposedPrimaryAssignment will have
                //        to be done before doing this (but there should be no adds/deletes if we're getting
                //        a brand-new appointee here).
                candidate = userService.getNewAppointee(form.getSurname(),
                                                        form.getGivenName(),
                                                        form.getMiddleName());
                if (StringUtils.isNotBlank(personUUID)) {
                    ((MivPersonInternal)candidate).setPersonId(personUUID);
                }
                if (StringUtils.isNotBlank(form.getLogin())) {
                    ((MivPersonInternal)candidate).setPrincipalName(form.getLogin());
                }
            }

            // bind candidate to form
            form.setCandidate(candidate);

            // default validation
            Event validateResult = super.validate(context);

            // If this is an appointment for a non-employee, remove the Present status information
            if (form.getActionType() == DossierActionType.APPOINTMENT
             && form.getCurrentEmployee() == PolarResponse.NO)
            {
               form.getAssignmentStatuses().remove(StatusType.PRESENT);
            }

            /*
             * Remove all assignments and titles marked for removal.
             */
            for (StatusType status : form.getAssignmentStatuses().keySet())
            {
                List<Assignment> assignments = form.getAssignmentStatuses().get(status);

                /*
                 * Remove all assignments marked for removal.
                 *
                 * Note that the assignments are being dumped into a new list before filtering.
                 * SearchFilterAdapter is unable to create a new instance of AutoPopulatingList.
                 */
                assignments.removeAll((new SelectedFilter<Assignment>()).apply(new ArrayList<Assignment>(assignments)));

                /*
                 * Remove titles marked for removal for each assignment.
                 */
                for (Assignment assignment : assignments)
                {
                    assignment.getTitles().removeAll((new SelectedFilter<Title>()).apply(new ArrayList<Title>(assignment.getTitles())));
                }
            }

            Assignment proposedPrimaryAssignment = form.getAssignmentStatuses()
                                                       .get(StatusType.PROPOSED)
                                                       .iterator()
                                                       .next();

            /*
             * Shadow person may only propose assignments
             * matching their assignment scopes.
             */
            for (AssignedRole assignment : getShadowPerson(context).getAssignedRoles())
            {
                // if find matching scope
                if (assignment.getScope().matches(proposedPrimaryAssignment.getScope()))
                {
                    // return validator validation result
                    return validateResult;
                }
            }

            // otherwise, no matching scope found
            getFormErrors(context).reject(null, "You do not have permission to assign candidates to '"
                                              + proposedPrimaryAssignment.getScopeDescription()
                                              + "'");
            return error();
        }

        return bindResult;
    }

    /**
     * Save Appointment variables because this is a new appointment request
     *
     * @param context Web Flow RequestContext
     */
    public Event initAction(RequestContext context)
    {
    	//this(DossierActionType.APPOINTMENT);
    	context.getFlowScope().put("actionType", DossierActionType.APPOINTMENT);
    	context.getFlowScope().put("delegationAuthority", DossierActionType.APPOINTMENT.getDefaultDelegationAuthority());
    	return success();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#loadProperties(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void loadProperties(RequestContext context)
    {
        super.loadProperties(context, DEFAULT_SUBSET, "labels", "tooltips");
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(PropertyEditorRegistry registry)
    {
        registry.registerCustomEditor(BigDecimal.class, new MivPropertyEditors.BigDecimalPropertyEditor());
        registry.registerCustomEditor(Step.class, new MivPropertyEditors.StepPropertyEditor());
        registry.registerCustomEditor(Scope.class, new MivPropertyEditors.ScopePropertyEditor());
        registry.registerCustomEditor(int.class, new MivPropertyEditors.IntegerPropertyEditor());
//        registry.registerCustomEditor(java.lang.Enum.class, new EnumPropertyEditor()); // This doesn't seem to work. Register each enum individually?
      registry.registerCustomEditor(DossierDelegationAuthority.class, new MivPropertyEditors.EnumPropertyEditor<>(DossierDelegationAuthority.class));
    }
}
