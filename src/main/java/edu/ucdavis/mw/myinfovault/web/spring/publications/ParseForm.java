/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ParseForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.publications;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.mw.myinfovault.service.publications.ParseResult;

/**
 * Abstract form backing for publications download and upload parsing.
 *
 * @author Craig Gilmore
 * @since MIV 4.4.2
 */
abstract class ParseForm implements Serializable
{
    private static final long serialVersionUID = 201206151405L;

    private List<ParseResult> parseResults = new ArrayList<ParseResult>();

    /**
     * @return List of parse results from the downloads or upload
     */
    public List<ParseResult> getParseResults()
    {
        return parseResults;
    }
}
