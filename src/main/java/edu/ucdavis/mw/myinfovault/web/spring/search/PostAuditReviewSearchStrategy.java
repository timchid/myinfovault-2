/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PostAuditReviewSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * The searching strategy to be used when searching for dossiers
 * at the post audit location and beyond.
 *
 * @author Rick Hendricks
 * @since MIV 4.7
 */
public class PostAuditReviewSearchStrategy extends ManageOpenActionSearchStrategy
{

    private static final AuthorizationService authorizationService = MivServiceLocator.getAuthorizationService();

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy#findPeople(java.util.List, edu.ucdavis.mw.myinfovault.service.person.MivPerson, boolean, int, int, int[])
     */
    @Override
    protected List<MivActionList> findPeople(List<Dossier> dossiers, MivPerson targetPerson, boolean showAll,
                                             int schoolId, int deptId, int... dropUsers)
    {
        // apply Fliter -- do not add if dropUser = dossier.getUserId()
        List<MivActionList> people = new ArrayList<MivActionList>(dossiers.size());
        for (Dossier ds : dossiers)
        {
            String theDossierLocation = ds.getLocation();
            // Lowercase it for .equals() testing, but keep the unaltered string
            // to pass into the fillDossierPerson() method.
            String location = theDossierLocation.toLowerCase();

            // MIV-4241 this is where we fix for the new MIV Open Action Reports (to display dossiers at candidate &
            // readytoarchive)
            // Don't show archived or ready-to-archive dossiers
            if (location.equals("archive")) /* || location.equals("readytoarchive")) */continue;

            // Senate admin only sees redelegated actions at the post audit location,
            // but all actions at any of the appeal locations (senate, federation, senate/federation).
            if ( (targetPerson.hasRole(MivRole.SENATE_STAFF) && ds.getAction().getDelegationAuthority() == DossierDelegationAuthority.NON_REDELEGATED &&
                 (!location.contains("senate") && !location.contains("federation")))
                 ||
                 (targetPerson.hasRole(MivRole.SENATE_STAFF) && ds.getAction().getDelegationAuthority() == DossierDelegationAuthority.REDELEGATED &&
                 (!location.contains("senate") && !location.contains("federation") && !location.contains("postaudit")))
                 ||
             // School Staff see only Redelegated actions in post-audit and appeal locations: part of MIV-4843
                 (targetPerson.hasRole(MivRole.SCHOOL_STAFF) && ds.getAction().getDelegationAuthority() == DossierDelegationAuthority.NON_REDELEGATED)
               )
            {
                continue;
            }

            // If a drop users list was passed, don't show dossier if this user is on the list
            if (dropUsers != null)
            {
                int dossierUserId = ds.getAction().getCandidate().getUserId();
                boolean drop = false;
                for (int exclude : dropUsers)
                {
                    if (dossierUserId == exclude)
                    {
                        drop = true;
                        break;
                    }
                }
                if (drop) continue;
            }

            // Associate the dossier with a user and add each appointment for the dossier to the list
            Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAppointments = ds
                    .getAppointmentAttributes();
            for (DossierAppointmentAttributeKey key : dossierAppointments.keySet())
            {
                // MIV-5042 - Only display primary appointment line at Post Audit locations and beyond
                if (!dossierAppointments.get(key).isPrimary())
                {
                    continue;
                }

                // School and department qualifiers for authorization checks
                AttributeSet qualification = new AttributeSet();
                qualification.put(Qualifier.USERID, ds.getAction().getCandidate().getUserId() + "");
                qualification.put(Qualifier.DEPARTMENT, Integer.toString(key.getDepartmentId()));
                qualification.put(Qualifier.SCHOOL, Integer.toString(key.getSchoolId()));
                // Make sure that the user is qualified for each appointment
                if (authorizationService.isAuthorized(targetPerson, Permission.VIEW_POST_AUDIT_DOSSIERS, null, qualification))
                {
                    DossierAppointmentAttributes attributes = dossierAppointments.get(key);
                    if (attributes != null)
                    {
                        MivActionList mal = fillDossierPerson(ds, theDossierLocation, attributes, schoolId, deptId);
                        if (mal != null) people.add(mal);
                    }
                    else
                    {
                        logger.error("No dossier attributes for dossier {}  key={} for {} to assign reviewers.",
                                     new Object[] { ds.getDossierId(), key, targetPerson });
                    }
                }
            }
        }

        return people;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.ManageOpenActionSearchStrategy#dossierSearch(edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.myinfovault.MIVUser, java.util.List)
     */
    @Override
    public List<MivActionList> dossierSearch(SearchCriteria criteria, MIVUser user, List<Dossier>dossiers) throws WorkflowException
    {
        List<MivActionList> actions = super.dossierSearch(criteria, user, dossiers);

        for (MivActionList action : actions)
        {
            dossierService.loadDossierAttributesForLocation(action.getDossier(),
                                                            action.getDossier().getDossierLocation());
            /*
             * Add review status if available.
             */
            DossierAttribute da = action.getDossier().getAttributeByAppointment(
                                      Dossier.getAppointmentKey(action.getSchoolId(), action.getDepartmentId()),
                                      "review");
            if (da != null)
            {
                action.setReviewStatus(da.getAttributeValue());
            }

        }

        return actions;
    }
}
