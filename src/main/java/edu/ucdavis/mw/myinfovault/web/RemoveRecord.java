/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RemoveRecord.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.QueryTool;
import edu.ucdavis.myinfovault.data.RecordHandler;


/**
 * Remove one record from the data source, by employing a {@link edu.ucdavis.myinfovault.data.RecordHandler}
 * to do the delete.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class RemoveRecord extends MIVServlet
{
    private static final long serialVersionUID = 200703161543L;

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.trace("\n ***** RemoveRecord doGet called! *****\t\tGET\n");

        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        String recordType = req.getParameter("rectype");
        logger.debug("RemoveRecord record type is {}", recordType);
        // TODO: Error if recordType==null or ==""

        Properties cfgProps = PropertyManager.getPropertySet(recordType, "config");

        // See if we're being asked to delete an uploaded document record.
        boolean deleteUpload = false;
        String uploadFileName = null;
        String uploadFlag = req.getParameter("uploaded");
        if (uploadFlag != null && uploadFlag.length() > 0) {
            deleteUpload = Boolean.parseBoolean(uploadFlag);
        }

        String key = null;
        key = findKey(req);
        // TODO: Error if key==null or ==""
        logger.info("RemoveRecord got key [{}]", key);

        RecordHandler rh = mui.getRecordHandler();

        String tableName = ""; // table to delete the record from
        if (deleteUpload)
        {
            tableName = "UserUploadDocument";

            // Get the uploaded file name
            Map <String, String> recMap = rh.get(tableName, key);
            uploadFileName = recMap.get("uploadfile");

            // Delete the uploaded file
            if (uploadFileName != null)
            {
                File uploadDirectory = MIVConfig.getConfig().getDossierPdfLocation(mui.getPerson().getUserId());
                // Full upload file path
                File uploadFile = new File(uploadDirectory,uploadFileName);
                if (uploadFile.exists())
                {
                    uploadFile.delete();
                    logger.info("Deleted uploaded file {} for user {}", uploadFile.getAbsolutePath(), mui.getPerson().getUserId());
                }
                else
                {
                    logger.info("Uploaded file {} does not exist to delete for user {}", uploadFile.getAbsolutePath(), mui.getPerson().getUserId());
                }
            }
        }
        else
        {
            Properties p = PropertyManager.getPropertySet(recordType, "config");
            tableName = p.getProperty("tablename");

            logger.debug("Config Properties are: [{}]", p);
            logger.debug("  tableName from PropertyManager lookup is {}", tableName);

            // TODO: Error if tableName==null or ==""
        }

        //log.info("Record Delete for user "+mui.getUserID()+": table="+tableName+", key="+key);
        logger.info("Record Delete for user {}: table={}, key={}", new Object[] { mui.getPerson().getUserId(), tableName, key });

        // Legacy letter-upload, redirect back to specified servlet
        if (recordType.equalsIgnoreCase("letter-upload"))
        {
            // Redirect back to referer
            String returnTo = req.getHeader("referer");
            String SSN= req.getParameter("SSN").trim();

            // When dealing with letters the user may not always be in control of a another user, so construct a new MIVUserInfo object
            // with logged in user controlling the input user id (SSN)
            rh = new MIVUserInfo(mivSession.get().getUser(),Integer.parseInt(SSN)).getRecordHandler();

            rh.delete(tableName, Integer.parseInt(key));
            res.sendRedirect(returnTo);
        }
        else
        {
            int count = rh.delete(tableName, Integer.parseInt(key));
            Properties pstr = PropertyManager.getPropertySet(recordType, "strings");
            //System.out.println("Strings available to RemoveRecord are: [[["+pstr+"]]]");
            String where = (String)pstr.get("pagehead");
            String deleteMessage = "Record" + (count == 1 ? "" : "s") + " deleted from " + where;

            Map<String, String[]> reqData = getRequestMap(req);
            try
            {
                String isFixAnnoatation = cfgProps.getProperty("fixannotation");
                if (Boolean.parseBoolean(isFixAnnoatation))
                {
                    deleteAnnotation(mui.getPerson().getUserId(), Integer.parseInt(key), tableName);
                }

                if (isAjaxRequest())
                {
                    ServletOutputStream out = null;
                    out = res.getOutputStream();
                    res.setContentType("text/html");
                    out.print(deleteMessage);
                }
                else
                {

                    String documentID = (reqData.get("documentid") != null ? reqData.get("documentid")[0] : "");

                    if (documentID == null || documentID.trim().length() == 0)
                    {
                        documentID = (reqData.get("DocumentID") != null ? reqData.get("DocumentID")[0] : "");
                    }

                    documentID = MIVUtil.sanitize(documentID);

                    req.setAttribute("recordTypeName", recordType);

                    // Additional Information Related and this documentid is used when we add a new Heading.
                    // These are passed straight through to the JSPs
                    req.setAttribute("documentid", documentID);
                    String sectionName = MIVUtil.sanitize(req.getParameter("sectionname"));
                    String subType = (reqData.get("subtype") != null ? reqData.get("subtype")[0] : null);
                    String recordClass = (reqData.get("class") != null ? reqData.get("class")[0] : null);

                    subType = subType != null ? subType : "";
                    documentID = documentID != null ? documentID : "";
                    sectionName =  sectionName !=null ? sectionName : "";

                    logger.debug("redirecting to list page");
                    if (recordType.equals("additionalheader"))
                    {
                        recordType = "additional";
                    }

                    StringBuilder redirectPath = new StringBuilder(req.getContextPath());
                    redirectPath.append("/ItemList?type=").append(recordType);

                    if (recordClass != null && !("".equals(recordClass)))
                    {
                        redirectPath.append("&class=").append(recordClass);
                    }

                    if (subType != null && !("".equals(subType)))
                    {
                        redirectPath.append("&subtype=").append(subType);
                    }

                    if (sectionName != null && !("".equals(sectionName)))
                    {
                        redirectPath.append("&sectionname=").append(sectionName);
                    }

                    redirectPath.append("&documentid=").append(documentID);

                    redirectPath.append("&success=").append(true);
                    redirectPath.append("&CompletedMessage=").append(deleteMessage);

                    logger.debug("redirectPath :: {}", redirectPath.toString());

                    res.setContentType("text/html");
                    res.sendRedirect(redirectPath.toString());
                    return;
                }

            }
            catch (IOException ioe)
            {
                logger.warn("Tried to write output to xmlhttprequest", ioe);
            }
        }
    }


    private final static String deleteAnnotationSQL = "DELETE FROM Annotations "+
                                                        " WHERE SectionBaseTable = ? AND RecordID = ? AND UserID = ?";

    /**
     * Delete Annotation record for given recordID.
     * @param userID
     * @param recordID
     * @param tableName
     * @return
     */
    public int deleteAnnotation(int userID, int recordID, String tableName)
    {
        logger.trace("\n ***** RemoveRecord deleteAnnotation called! *****\n");
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        return qt.delete(deleteAnnotationSQL, tableName, String.valueOf(recordID), String.valueOf(userID));
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.trace("\n ***** RemoveRecord doPost called! *****\t\tPOST\n");
        this.doGet(req, res);
    }


    /**
     * Find the record ID key in the request parameters.
     * Searches the parmeters for one in the form "D&lt;nnnn&gt;=delete"
     * where &lt;nnnn&gt; is any 1 or more digit number.
     * @return the key parameter, or <code>null</code> if none was found.
     */
    String findKey(HttpServletRequest req)
    {
        @SuppressWarnings({"unchecked"})
        Map<String, String[]> params = req.getParameterMap();
        boolean matched = false;
        String keyval = null;

        for (String p : params.keySet())
        {
            // The delete button uses a 'D' prefix on the record number
            if (p.matches("D[0-9]+"))
            {
                String[] vals = req.getParameterValues(p);
                for (String v : vals)
                {
                    if (v.equalsIgnoreCase("delete")) {
                        matched = true;
                        break;
                    }
                }
                if (matched) {
                    keyval = p;
                    break;
                }
            }
        }

        if (keyval != null && keyval.length() > 0) {
            keyval = keyval.replaceFirst("[a-zA-Z]*", "");
        }

        return keyval;
    }
}
