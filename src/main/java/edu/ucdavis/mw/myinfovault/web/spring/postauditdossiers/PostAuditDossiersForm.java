/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PostAuditDossiersForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.postauditdossiers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PostAuditDossiersForm implements Serializable
{
    private static final long serialVersionUID = 1360610757632L;

    private List<String> toPostAuditReview = new ArrayList<String>();

    /**
     * getToPostAuditReview - get the dossiers numbers to PostAuditReview
     * @return
     */
    public List<String> getToPostAuditReview()
    {
        return this.toPostAuditReview;
    }

    /**
     * @param toPostAuditReview
     */
    public void setToPostAuditReview(List<String> toPostAuditReview)
    {
        this.toPostAuditReview = toPostAuditReview;
    }

    /**
     * Clear toPostAuditReview list
     */
    public void clearToPostAuditReview()
    {
        this.toPostAuditReview.clear();
    }
}
