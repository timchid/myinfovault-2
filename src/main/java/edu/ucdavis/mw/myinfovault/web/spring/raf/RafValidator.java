/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.raf;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.raf.Department;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo.Acceleration;
import edu.ucdavis.mw.myinfovault.domain.raf.Rank;
import edu.ucdavis.mw.myinfovault.domain.raf.Status;
import edu.ucdavis.mw.myinfovault.web.spring.action.AcademicActionValidator;

/**
 * Validates the recommended action form.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class RafValidator extends AcademicActionValidator
{
    private static final BigDecimal MAX_SALARY = new BigDecimal("9999999.99");

    /*
     * Number constraint messages
     */
    private static final String numConstMsg = "This field is restricted to numbers (No letters or punctuation allowed).";
    private static final String numPosConstMsg = "This field is restricted to numbers greater than zero (No letters or punctuation allowed).";
    private static final String percentOfTimeConstMsg = " Must be numeric value from 1 to 100, or three characters: WOS.";
    private static final String requiredMsg = "This field is required.";

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return RafForm.class.isAssignableFrom(clazz);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        RafForm form = (RafForm) obj;
        RafBo raf = form.getRaf();

        /*
         * RAF dosen't support Appointment action.
         */
        if (raf.getDossier().getAction().getActionType() == DossierActionType.APPOINTMENT)
        {
            errors.reject(null, "Action Type : Invalid action.");
            return;
        }

        validateAction(form.getActionType(),
                       form.getDelegationAuthority(),
                       errors);

        // validate effective and retroactive dates
        validateDates(form, errors);

        //appointment type
        if (!raf.isAppointmentType9mo()
         && !raf.isAppointmentType11mo())
        {
            errors.reject(null, "Appointment Type : " + requiredMsg);
        }

        //acceleration
        if (raf.getAcceleration() == Acceleration.ACCELERATION
         && (raf.getAccelerationYears() > 99 || raf.getAccelerationYears() < 1))
        {
            errors.reject(null, "Accel. (Years) : " + numPosConstMsg);
        }
        //deceleration
        else if (raf.getAcceleration() == Acceleration.DECELERATION
              && (raf.getDecelerationYears() > 99 || raf.getDecelerationYears() < 1))
        {
            errors.reject(null, "Decel. (Years) : " + numPosConstMsg);
        }

        //Years at rank
        if (raf.getYearsAtRank() > 99 || raf.getYearsAtRank() < 0)
        {
            errors.reject(null, "Years at rank : " + numPosConstMsg);
        }

        //Years at step
        if (raf.getYearsAtStep() > 99 || raf.getYearsAtStep() < 0)
        {
            errors.reject(null, "Years at Step : " + numPosConstMsg);
        }

        validateDepartments(raf.getPrimaryAndJointDepartments(), errors);
        validateStatuses(raf.getStatuses(), errors);
    }


    /**
     * Validate departments.
     *
     * @param departments
     * @param errors validation errors
     */
    private void validateDepartments(List<Department> departments, Errors errors)
    {
        for (int i=0; i < departments.size(); i++)
        {
            Department department = departments.get(i);

            Boolean isValid = validatePercentOfTime(department.getPercentOfTime());

            //set error
            if (isValid == null || !isValid)
            {
                errors.reject(null, getDepartmentErrorMsg(false, isValid, departments.size(), i));
            }
        }
    }

    /**
     * Get a formatted message for a department error.
     *
     * @param isNameField
     * @param isValid
     * @param listSize
     * @param index
     * @return formatted message for a department error
     */
    private String getDepartmentErrorMsg(boolean isNameField, Boolean isValid, int listSize, int index)
    {
        StringBuilder errMsg = new StringBuilder();

        errMsg.append("Department");

        if (listSize > 1) errMsg.append(", Row " + (index+1));

        errMsg.append(" -");

        if (isNameField)
        {
            errMsg.append(" Department Name : "+requiredMsg);
        }
        else
        {
            errMsg.append(" % of Time");

            if (isValid != null && !isValid)
            {
                errMsg.append(" : ").append(percentOfTimeConstMsg);
            }
            else
            {
                errMsg.append(" : ").append(requiredMsg);
            }
        }

        return errMsg.toString();
    }


    /**
     * Returns <code>true</code> on valid, <code>false</code> on invalid, and <code>null</code> on blank.
     *
     * @param percentOfTime value for percent of time field
     * @return Boolean object for valid percent of time
     */
    private Boolean validatePercentOfTime(String percentOfTime)
    {
        if (StringUtils.isBlank(percentOfTime)) return null;

        boolean isInteger = edu.ucdavis.myinfovault.data.Validator.isInteger(percentOfTime);

        return !(
                    percentOfTime.length() > 3
                 || isInteger
                 && (
                        Integer.parseInt(percentOfTime) > 100
                     || Integer.parseInt(percentOfTime) < 0
                    )
                 || !isInteger
                 && percentOfTime.matches(".*\\d.*")); //StringUtil.containsDigit(percentOfTime));
    }


    /**
     * Validate statuses.
     *
     * @param statuses
     * @param errors
     */
    private void validateStatuses(List<Status> statuses, Errors errors)
    {
        int rowCount = 5;

        //check field row completion
        for (Status status : statuses)
        {
            for (Rank rank : status.getRanks())
            {
                if (rank.isBlank())
                {
                    rowCount--;
                }
            }

            //not one completed row
            if (rowCount < 1)
            {
                errors.reject(null, status.getStatusType().getDescription());
            }
            //continue and validate ranks
            else
            {
                validateRanks(status.getRanks(), errors, status.getStatusType().getDescription());
            }

            rowCount = 5;
        }
    }


    /**
     * Validate ranks.
     *
     * @param ranks
     * @param errors validation errors
     * @param statusType
     */
    private void validateRanks(List<Rank> ranks, Errors errors, String statusType)
    {
        //check field row completion
        for (int i=0; i < ranks.size(); i++)
        {
            //if not blank row
            if (!ranks.get(i).isBlank())
            {
                //rank and step
                if (StringUtils.isBlank(ranks.get(i).getRankAndStep()))
                {
                    errors.reject(null, statusType + " Status, Row " + (i+1) + " - Rank & Step : "+requiredMsg);
                }

                //percent of time
                Boolean isValid = validatePercentOfTime(ranks.get(i).getPercentOfTime());
                if (isValid == null || !isValid)
                {
                    errors.reject(null, statusType + " Status, Row " + (i+1) + " - % of Time : " + (isValid == null ? percentOfTimeConstMsg : ""));
                }

                //title code
                if (StringUtils.isBlank(ranks.get(i).getTitleCode()))
                {
                    errors.reject(null, statusType + " Status, Row " + (i+1) + " - Title Code : "+requiredMsg);
                }
                else if (!edu.ucdavis.myinfovault.data.Validator.isInteger(ranks.get(i).getTitleCode())
                      || (Integer.parseInt(ranks.get(i).getTitleCode()) > 9999
                      || Integer.parseInt(ranks.get(i).getTitleCode()) < 0))
                {
                    errors.reject(null, statusType + " Status, Row " + (i+1) + " - Title Code : " + numConstMsg);
                }

                //monthly salary
                if (ranks.get(i).getMonthlySalary().compareTo(MAX_SALARY) > 0
                 || ranks.get(i).getMonthlySalary().compareTo(BigDecimal.ZERO) < 0)
                {
                    errors.reject(null, statusType + " Status, Row " + (i+1) + " - Monthly Salary : " + numConstMsg.substring(0, numConstMsg.length()-1) + ", except decimals)");
                }

                //annual salary
                if (ranks.get(i).getAnnualSalary().compareTo(MAX_SALARY) > 0
                 || ranks.get(i).getAnnualSalary().compareTo(BigDecimal.ZERO) < 0)
                {
                    errors.reject(null, statusType + " Status, Row " + (i+1) + " - Annual Salary : " + numConstMsg.substring(0, numConstMsg.length()-1) + ", except decimals)");
                }
            }
        }
    }
}
