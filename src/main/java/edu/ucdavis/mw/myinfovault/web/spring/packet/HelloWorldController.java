/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HelloWorldController.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.packet;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Copied from http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html#mvc-controller
 * @author stephenp
 *
 */
@Controller
public class HelloWorldController
{
    @RequestMapping("/helloWorld")
    public String helloWorld(Model model, @RequestParam(value="msg", required=false) String msg)
    {
        System.out.println("HelloWorldController.helloWorld was called.");
        String message = msg != null && msg.trim().length() > 0
                ? msg
                : "Hello, World!";

        model.addAttribute("message", message);
        return "helloWorld";
    }
}