/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignReviewersForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.assignreviewers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.web.spring.assignment.AssignmentForm;

/**
 * Form backing object for the assign reviewers action.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class AssignReviewersForm extends AssignmentForm implements Serializable
{
    private static final long serialVersionUID = 200908141334L; //-3066126442136548308L; //  2009-08-14 13:34:42

    private MivPerson owner;
    private Dossier dossier;
    private DossierAppointmentAttributeKey appointmentKey;
    private boolean reviewOpen;
    private List<Group> copiedGroups = new ArrayList<Group>();

    /**
     * Create the form object, holding onto some dossier details for use
     * in the action and the initial list of associated reviewers.
     *
     * @param owner The owner of the dossier
     * @param dossier The dossier associated with the reviewer assignment
     * @param appointmentKey The appointment key holding the department associated with the dossier
     * @param reviewOpen <code>true</code> if the review period is open, <code>false</code> otherwise
     * @param inactiveReviewers The set of person reviewers assigned to this dossier, but inactive
     * @param personReviewers The current set of person reviewers assigned to the dossier
     * @param groupReviewers The current set of group reviewers assigned to the dossier
     * @param actingPerson The acting MIV person
     */
    public AssignReviewersForm(MivPerson owner,
                               Dossier dossier,
                               DossierAppointmentAttributeKey appointmentKey,
                               boolean reviewOpen,
                               Set<MivPerson> inactiveReviewers,
                               SortedSet<MivPerson> personReviewers,
                               SortedSet<Group> groupReviewers,
                               MivPerson actingPerson)
    {
        super(personReviewers,
              inactiveReviewers,
              groupReviewers,
              actingPerson);

        this.owner = owner;
        this.dossier = dossier;
        this.appointmentKey = appointmentKey;
        this.reviewOpen = reviewOpen;
    }

    /**
     * Get the owner of this dossier.
     *
     * @return Owner of the dossier
     */
    public MivPerson getOwner()
    {
        return owner;
    }

    /**
     * Get the dossier associated with this reviewer assignment.
     *
     * @return Dossier for this assignment
     */
    public Dossier getDossier()
    {
        return dossier;
    }


    /**
     * Get the location of the dossier associated with this reviewer assignment.
     *
     * @return Dossier location description
     */
    public String getDossierLocation()
    {
//        return DossierLocation.valueOf(dossier.getLocation().toUpperCase()).getDescription();
        return DossierLocation.mapWorkflowNodeNameToLocation(dossier.getLocation()).getDescription();
    }


    /**
     * Get the dossier appointment key for this reviewer assignment.
     *
     * @return Dossier appointment key for this assignment
     */
    public DossierAppointmentAttributeKey getAppointmentKey()
    {
        return appointmentKey;
    }


    /**
     * Get if the dossier review period is open.
     *
     * @return <code>true</code> if the review period is open, <code>false</code> otherwise
     */
    public boolean isReviewOpen()
    {
        return reviewOpen;

    }

    /**
     * Set the dossier review period to opened or closed.
     *
     * @param reviewOpen Review period attribute set to OPEN on <code>true</code>, CLOSED otherwise
     */
    public void setReviewOpen(boolean reviewOpen)
    {
        this.reviewOpen = reviewOpen;
    }

    /**
     * Get the list of groups copies during this assignment.
     *
     * @return list of copied groups
     */
    public List<Group> getCopiedGroups()
    {
        return copiedGroups;
    }
}
