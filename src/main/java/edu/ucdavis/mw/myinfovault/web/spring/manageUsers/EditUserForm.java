/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditUserForm.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.util.AutoPopulatingList;
import org.springframework.util.AutoPopulatingList.ElementFactory;
import org.springframework.util.AutoPopulatingList.ElementInstantiationException;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.EditUserAuthorizer;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.MivRole.Category;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Spring Form backing object for editing and adding users
 * @author Stephen Paulsen
 * @since MIV 4.0
 */
public class EditUserForm implements Serializable
{
    private static final long serialVersionUID = 201107201332L;

    private static final AuthorizationService authorizationService = MivServiceLocator.getAuthorizationService();

    private static Logger log = Logger.getLogger(EditUserForm.class);


    /** The string entered for the email in the search box, used only when adding a new user. */
    protected String searchText;

    /** The user's name, to be displayed on the form. */
    private String displayName;

    private List<String> availableEmails;
    private String originalEmail;
    private String selectedEmail;

    private List<MivRole> availableRoles;
    private MivRole originalRole;// = MivRole.CANDIDATE;
    private MivRole selectedRole;// = MivRole.CANDIDATE;

    /** Appointments that an academic user has */
    private List<Appointment> appointments;
    /** Assignments that a staff user has */
    private List<AssignedRole> assignments;

/* See http://eggsylife.co.uk/2009/11/30/spring-forms-dynamic-lists-and-ajax/
   for an example using Spring AutoPopulatingList */
    protected AutoPopulatingList<AssignmentLine> formLines = new AutoPopulatingList<>(new FormLineElementFactory());
    private AssignmentLine homeDepartment;


    // Not used in the form, just carried around by it
    private MivPerson editedPerson;

    // used to 'trick' the jsp into holding a complete list of departments
    private final String jspBind = "";

    /**
     * TODO: add javadoc
     *
     * @param shadowPerson person doing the editing
     * @param editPerson person to edit
     */
    public EditUserForm(MivPerson shadowPerson, MivPerson editPerson)
    {
        initializeForm(shadowPerson, editPerson);
    }

    public EditUserForm()
    {

    }

    public void initializeForm(MivPerson shadowPerson, MivPerson editPerson)
    {
        if (editPerson == null) {
            throw new MivSevereApplicationError("No person was set as the user being edited in the Add/Edit User form!");
        }
        editPerson.loadDetail();

        this.editedPerson = editPerson;

        // Set form display name
        this.displayName = editPerson.getDisplayName();

        // Set up the contents of the role drop-down list.
        // It will contain only Primary roles that this actor is allowed to assign.
        this.availableRoles = EditUserAuthorizer.getAssignableRoles(shadowPerson.getPrimaryRoleType(), true);

        // Set form role
        this.selectedRole = editPerson.getPrimaryRoleType();

        // The person being edited may not have any role yet (if they're being added)
        if (this.selectedRole == null)
        {
            // No role set, pick CANDIDATE if it's available
            // Otherwise pick the first thing on the available list
            this.selectedRole = this.availableRoles.contains(MivRole.CANDIDATE)
                              ? MivRole.CANDIDATE
                              : this.availableRoles.iterator().next();
        }
        this.originalRole = this.selectedRole;


        // Set the chosen email address index
        this.selectedEmail = editPerson.getPreferredEmail();

        this.originalEmail = this.selectedEmail;

        this.availableEmails = editPerson.getEmailAddresses();

        // Set up the existing Appointments and Assignments
        this.appointments = new ArrayList<Appointment>(editPerson.getAppointments());
        this.assignments  = new ArrayList<AssignedRole>(editPerson.getAssignedRoles());
        buildFormLines();

        // Mark lines that the actor can't touch as read-only
        // Start with the "home department" - this *should* never be necessary because
        // the actor shouldn't be allowed to edit the user if this is the case, but this
        // is triple-protection.
        this.homeDepartment.setReadonly(homeDepartment.getScope() != Scope.None
                                     && !authorizationService.isAuthorized(shadowPerson,
                                                                           "MODIFY_ASSIGNMENT",
                                                                           null,
                                                                           this.homeDepartment.getScope()));

        // Go through the other generated form lines marking any read-only as necessary
        // We need to know the kind of person being edited, in case of candidate joint appointments which are wide open.
        AttributeSet details = new AttributeSet(PermissionDetail.TARGET_USER_ROLE, this.originalRole.toString());
        for (Object o : this.getFormLines())
        {
            AssignmentLine al = (AssignmentLine) o;

            al.setReadonly(al != null
                        && !authorizationService.isAuthorized(shadowPerson,
                                                              "MODIFY_ASSIGNMENT",
                                                              details,
                                                              al.getScope()));
        }
    }

    public MivPerson getEditPerson()
    {
        //we're adding a new person that doesn't already exist
        if (this.editedPerson.getUserId() == -1)
        {
            return editedPerson;
        }
        else //we're editing an existing person. We want to modify the cached person, not the deserialized copy.
        {
            UserService userService = MivServiceLocator.getUserService();
            return userService.getPersonByMivId(editedPerson.getUserId());
        }
    }


    public String getSearchText()
    {
        return searchText;
    }

    public void setSearchText(String searchText)
    {
        this.searchText = searchText;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }



    public List<String> getAvailableEmails()
    {
        return Collections.unmodifiableList(this.availableEmails);
    }

    public String getOriginalEmail()
    {
        return originalEmail;
    }

    public String getSelectedEmail()
    {
        return selectedEmail;
    }

    public void setSelectedEmail(String selectedEmail)
    {
        this.selectedEmail = selectedEmail;
    }

    public List<MivRole> getAvailableRoles()
    {
// Spring didn't like getting an unmodifiable list for the <form:options> tag in useredit.jsp
//        return Collections.unmodifiableList(this.availableRoles);
        return this.availableRoles;
    }

    public MivRole getSelectedRole()
    {
        return this.selectedRole;
    }

    public void setSelectedRole(MivRole selectedRole)
    {
        this.selectedRole = selectedRole;
    }


    public MivRole getOriginalRole()
    {
        return this.originalRole;
    }

    public AssignmentLine getHomeDepartment()
    {
        return this.homeDepartment;
    }

    public AutoPopulatingList<AssignmentLine> getFormLines()
    {
        return this.formLines;
    }

    /** Add a new blank line to the form. */
    public void addFormLine()
    {
        this.addFormLine(Scope.None);
    }

    public void addFormLine(Scope scope)
    {
        this.formLines.add(new AssignmentLine(scope, true, false, false));
    }

    /**
     *
     */
    private void buildFormLines()
    {
        MivPerson editedPerson = this.getEditPerson();
        boolean addingUser = editedPerson.getUserId() == -1;//! this.editedPerson.hasRole(MivRole.MIV_USER);
        // TODO: This if and logging is just to test, and to use "addingUser".
        // What we really want is to simplify building the form lines;
        //  Adding is much simpler and most of the sanity checking below
        //  can be skipped. Code for Editing gets simpler too because we
        //  won't have to keep guessing if we're adding or editing.
        // Stubs for prepareFormFor(Add|Edit) follow this method.
        if (addingUser) {
            log.info("Adding person [" + editedPerson + "]");
            prepareFormForAdd();
        }
        else {
            log.info("Editing person [" + editedPerson + "]");
            prepareFormForEdit();
        }

    }


    /**
     * Set up the initial state for the Edit User form when adding a new user.
     */
    private void prepareFormForAdd()
    {
        MivPerson editedPerson = this.getEditPerson();
        log.info("Preparing form for Adding a person [" + editedPerson + "]");

        Scope s = Scope.None;
        this.homeDepartment = new AssignmentLine(s, true, false, false);
    }


    /**
     * Set up the initial state for the Edit User form when editing an existing user.
     */
    private void prepareFormForEdit()
    {
        MivPerson editedPerson = this.getEditPerson();
        log.info("Preparing form for Editing a person [" + editedPerson + "]");
        Map<Scope,AssignmentLine> rowMap = new LinkedHashMap<Scope,AssignmentLine>(this.appointments.size() + this.assignments.size());
        Scope primaryScope = null;

        formLines.clear();

        for (Appointment a : this.appointments)
        {
            Scope s = new Scope(a.getScope().getSchool(), a.getScope().getDepartment());
            AssignmentLine al = new AssignmentLine(s, true, false, false);
            rowMap.put(s, al);
        }

        // Must deal with the Home Department first, so it will be in the rowMap
        // if & when a wildcard scoped role is found. Those include SCHOOL_STAFF,
        // VICE_PROVOST_STAFF, DEAN, etc.
        for (AssignedRole ar : this.assignments)
        {
            if (ar.getRole() != MivRole.MIV_USER) continue;

            // Found the MIV_USER role, set it as the home department
            Scope s = ar.getScope();
            AssignmentLine al = new AssignmentLine(s);
            rowMap.put(s, al);
            primaryScope = s;
            this.homeDepartment = al;
            break;
        }

        // Now process the rest of the assignments one at a time, skipping MIV_USER
        // because we've already dealt with it.
        for (AssignedRole ar : this.assignments)
        {
            MivRole role = ar.getRole();
            // Save the primary Scope to use later, so it will be listed as the First line.
            if (role == MivRole.MIV_USER) {
                // Skip it - we handled it above
                continue;
            }

            Scope s = ar.getScope();
            AssignmentLine al = null;

            // If there's already an AssignmentLine for this scope it's
            // (probably) an Appointment as we're adding information to it.
            al = rowMap.get(s);

            // No line is present for this scope yet, but it might be a DEAN,
            // SCHOOL_ADMIN, etc. for a school that's already represented.
            if (al == null)
            {
                // See if it matches the home department...
                if (homeDepartment != null && homeDepartment.getScope().matches(s))
                {
                    al = homeDepartment;
                }
                // ...otherwise check for a match in other departments.
                else
                {
                    for (AssignmentLine testLine : rowMap.values())
                    {
                        if (testLine.getScope().matches(s))
                        {
                            al = testLine;
                            break;
                        }
                    }
                }
            }

            // There were no matching lines available, add one.
            if (al == null)
            {
                al = new AssignmentLine(s);
                rowMap.put(s, al);
            }

            // For a faculty role type we'll need to set the flags
            if (role.getCategory() == Category.FACULTY)
            {
                switch (role)
                {
                    case CANDIDATE:
                        al.setCandidate(true);
                        break;
                    case DEPT_CHAIR:
                        al.setDeptChair(true);
                        break;
                    case DEAN:
                        al.setDean(true);
                        break;
                    default: break;
                }
            }
        }

// New User handling moved to its own method - do we still need to do this?
//        // Now that we've processed all the lines, if there's nothing available
//        // we must be adding a new user. Give them a dummy home department and
//        // primary scope
//        if (rowMap.size() == 0 && this.homeDepartment == null)
//        {
//            Scope s = Scope.None;
//            this.homeDepartment = new AssignmentLine(s);
//        }

        // If we found a home department earlier, and therefore have set a
        // primary scope, remove the home department from the additional
        // assignment lines. It will be treated separately during editing.
        if (primaryScope != null)
        {
            // Remove the home department, it is handled separately from the rest of the assignment lines.
            AssignmentLine al = rowMap.remove(primaryScope);
            if (al != null) {
                if (this.homeDepartment == null) {
                    log.warn("In EditUserForm.buildFormLines() the homeDepartment was somehow not set in the loop; setting it now.");
                    this.homeDepartment = al;
                }
            }
        }
        else
        {
            // Wow this got ugly.  Some combination of errors could have messed up the database
            // and there are too many equivalent things tracked e.g. primaryScope, homeDepartment
            // If we're here there was no primaryScope/homeDepartment and this didn't look like
            // a newly Added person, so try to correct the missing MIV_USER role that should have
            // been there. (SDP)
            log.warn("No Primary Scope was set - No MIV_USER role found for " + this.displayName + ". Attempting to add one.");
            // Correct the problem - set the Home Department / Primary Scope by taking the first rowMap entry
            AssignmentLine al = null;
            for (Scope s : rowMap.keySet()) // Is iteration the only way to get the first entry?
            {
                al = rowMap.get(s);
                break;
            }

            // STILL null??
            if (al == null) {
                throw new MivSevereApplicationError("errors.person.missingHomeDepartment", editedPerson);
            }
            // Not null, set it as the home dept
            primaryScope = al.getScope();
            editedPerson.addRole(new AssignedRole(MivRole.MIV_USER, primaryScope));
            this.homeDepartment = rowMap.remove(primaryScope);
        }

        formLines.addAll(rowMap.values());
    }




    public String getJspBind() {
		return jspBind;
	}


    public class FormLineElementFactory implements ElementFactory<AssignmentLine>, Serializable
    {
        private static final long serialVersionUID = 201505040856L;

        /* (non-Javadoc)
         * @see org.springframework.util.AutoPopulatingList.ElementFactory#createElement(int)
         */
        @Override
        public AssignmentLine createElement(int paramInt) throws ElementInstantiationException
        {
            return new AssignmentLine(new Scope(Scope.NONE), false, false, false);
        }

    }
}
