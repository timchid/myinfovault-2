/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchMethod.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;

/**
 * Search method employed for SeachCriteria form.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public enum SearchMethod
{
    /**
     * use the main name search criteria
     */
    SEARCHNAME(DossierLocation.UNKNOWN),
    /**
     * use the last name search criteria
     */
    SEARCHLNAME(DossierLocation.UNKNOWN),
    /**
     * use the department search criteria
     */
    SEARCHDEPT(DossierLocation.DEPARTMENT),
    /**
     * use the school search criteria
     */
    SEARCHSCHOOL(DossierLocation.SCHOOL),
    /**
     * use the senate office search criteria
     */
    SEARCHSENATE(DossierLocation.SENATE_OFFICE),
    /**
     * use the academic affairs search criteria
     */
    SEARCHACADEMIC(DossierLocation.VICEPROVOST);

    private final DossierLocation location;

    /**
     * Instantiate a search method.
     *
     * @param location dossier location associated with the search method
     */
    private SearchMethod(DossierLocation location)
    {
        this.location = location;
    }

    /**
     * @return dossier location associated with the search method
     */
    public DossierLocation getLocation()
    {
        return this.location;
    }
}
