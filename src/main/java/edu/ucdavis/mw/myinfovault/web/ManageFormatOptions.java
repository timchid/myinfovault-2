/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ManageFormatOptions.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.FieldFormatData;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * FIXME: Missing Javadoc!
 *
 * @author Rick Hendricks
 * @since MIV 2.0
 */
public class ManageFormatOptions extends MIVServlet
{
    private static final long serialVersionUID = 200710010757L;

//    /**
//     * Constructor of the object.
//     */
//    public ManageFormatOptions()
//    {
//        super();
//    }
//
//    /**
//     * Destruction of the servlet.
//     */
//    @Override
//    public void destroy()
//    {
//        super.destroy(); // Just puts "destroy" string in log
//        // Put your code here
//    }


    /**
     * The doGet method of the servlet.
     *
     * This method is called when a form has its tag value method equals to get.
     *
     * @param req the request send by the client to the server
     * @param res the response send by the server to the client
     * @throws ServletException if an error occurred
     * @throws IOException if an error occurred
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.trace("\n ***** ManageFormatOptions doGet called! *****\t\tGET\n");

        //res.setCharacterEncoding("iso-8859-1");
        req.setCharacterEncoding("utf-8");

        // Get the dataType
        String dataType = getDataType(req,res);

        initForm(dataType,req,res);

        String targetUrl = "/jsp/manageFormatOptions.jsp?dataType="+dataType;
        String success = req.getParameter("success");
        if (req.getParameter("CompletedMessage") != null && req.getParameter("timestamp") != null)
        {
            /*long timeStamp = new Long(req.getParameter("timestamp")).longValue();
            long currentTimeStamp = System.currentTimeMillis();*/

            String completedMessage = "";

            if (success == null || success.length() == 0 || success.equalsIgnoreCase("info"))
            {
                completedMessage = "<div id=\"infobox\">" + req.getParameter("CompletedMessage") + "</div>";
            }
            else if (success.equalsIgnoreCase("true"))
            {
                completedMessage = "<div id=\"successbox\">" + req.getParameter("CompletedMessage") + "</div>";
            }
            else if (success.equalsIgnoreCase("false"))
            {
                completedMessage = "<div id=\"errorbox\">" + req.getParameter("CompletedMessage") + "</div>";
            }

            req.setAttribute("CompletedMessage", completedMessage);

            /*if (currentTimeStamp - timeStamp < 4000)
            {
                req.setAttribute("CompletedMessage", completedMessage);
            }*/
        }

        if (success == null || success/*.trim()*/.length() == 0 || success.equalsIgnoreCase("true"))
        {
            req.setAttribute("isInitForm", true);
        }

        // send the request on to it for final processing.
        logger.debug("Dispatch target is {}", targetUrl);
        dispatch(targetUrl, req, res);
        // FIXME: Isn't it too late to set the content type after dispatch has already been called?
        res.setContentType("text/html");
    }


    /**
     * Get the datatype to be formatted
     * @param req
     * @param res
     * @return
     */
    private String getDataType(HttpServletRequest req,HttpServletResponse res)
    {
        // Get the dataType
        String dataType = MIVUtil.sanitize(req.getParameter("dataType"));
        // Guarantee that it's a valid number
        try {
            Integer.parseInt(dataType);
        }
        catch (NumberFormatException e) {
//            showError("The dataType \"" + dataType + "\" is not a valid number in ManageFormatOptions", req, res);
            throw new MivSevereApplicationError("errors.formatoptions.invalidtype", dataType);
        }

        // Check the valid document number.
        Map<String, Map<String, String>> documentsMap = MIVConfig.getConfig().getMap("documents");
        if (documentsMap == null || documentsMap.get(dataType) == null)
        {
//            showError("The dataType \"" + dataType + "\" is not a valid number in ManageFormatOptions", req, res);
            throw new MivSevereApplicationError("errors.formatoptions.invalidtype", dataType);
        }

        return dataType;
    }


    /**
     * Initialize the page
     * @param dataType
     * @param req
     * @param res
     */
    private void initForm(String dataType,HttpServletRequest req, HttpServletResponse res)
    {
        Map<String,Object> config = new HashMap<String,Object>();
        Properties p = null;

        p = PropertyManager.getPropertySet("default", "labels");
        config.put("labels", p);

        p = PropertyManager.getPropertySet("default", "tooltips");
        config.put("tooltips", p);

        config.put("options", MIVConfig.getConfig().getConstants());
        req.setAttribute("constants", config);

        FieldFormatData fieldFormatData = mivSession.get().getUser().getTargetUserInfo().getFieldFormatData(Integer.parseInt(dataType));

        req.setAttribute("fieldFormatData", fieldFormatData);

        int maxPatterns = getMaxPatterns();
        req.setAttribute("maxpatterns", maxPatterns);

        String documentDescription = MIVConfig.getConfig().getMap("documents").get(dataType).get("description");
        req.setAttribute("documentdescription", documentDescription);
    }


    /**
     * to get the maximum number of allowed Patterns formatting
     * @return
     */
    private int getMaxPatterns()
    {
        int maxPatterns = 250;     // default
        String maxPatternsStr = MIVConfig.getConfig().getProperty("fieldformat-config-maxpatterns");

        if (StringUtils.isNotEmpty(maxPatternsStr))
        //if (maxPatternsStr != null && maxPatternsStr.length() > 0)
        {
            try {
                maxPatterns = Integer.parseInt(maxPatternsStr);
            }
            catch (NumberFormatException nfe) {
                //maxPatterns = 250; // Redundant. maxPatterns was set to 250 and is unchanged if the exception is thrown.
            }
        }

        return maxPatterns;
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        int recsUpdated = 0;
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        int maxPatterns = getMaxPatterns();
        req.setAttribute("maxpatterns", maxPatterns);

        int patternCount = 0;
        String patternCountStr = req.getParameter("patternCount");
        if (StringUtils.isNotEmpty(patternCountStr))
        //if (patternCountStr != null && patternCountStr/*.trim()*/.length() > 0)
        {
            try {
                patternCount = Integer.parseInt("0" + patternCountStr.trim());
            }
            catch (NumberFormatException nfe) {
                //patternCount = 0; // Redundant. patternCount was set to 0 and is unchanged if the exception is thrown.
            }
        }

        // Hide the Add button when the max number of patterns has been reached.
        req.setAttribute("isHideAddButton", patternCount >= maxPatterns);

        logger.trace("\n ***** ManageFormatOptions doPost called! *****\t\tGET\n");

        //res.setCharacterEncoding("iso-8859-1");
        req.setCharacterEncoding("utf-8");
        String message = null;
        String success = null;

        String dataType = getDataType(req, res);
        String deleteKey = "";

        // Check to reset/reload the form
        if (req.getParameter("reset") != null)
        {
            // FIXME: is it too late to set the content type after sendRedirect has been called?
            res.sendRedirect(req.getContextPath() + "/ManageFormatOptions?&dataType=" + dataType);
            res.setContentType("text/html");
            return;
        }
        else if (req.getParameter("AddPatterns") != null || req.getParameter("AddPatternsDown") != null)
        {
            req.setAttribute("isInitForm", false);
            req.setAttribute("isAddPattern", true);
            req.setAttribute("deleteKey", req.getParameter("deleteKey"));
        }
        else if (req.getParameter("save") != null)
        {
            // Gather all of the parameters from the request and build a map of the fields
            LinkedHashMap<String, String> updatedFormatData = getFormData(req);

            ResultVO resultVO = validateForm(updatedFormatData);

            if (resultVO != null && resultVO.isSuccess())
            {
                // do the update
                recsUpdated = mui.getFieldFormatData(Integer.parseInt(dataType)).update(updatedFormatData, dataType);
                mui.loadPatterns();
                switch (recsUpdated)
                {
                    case -1:
                        message = "No Format Options have been altered or added.";
                        success = "info";
                        req.setAttribute("isInitForm", true);
                        break;
                    case 0:
                        String deleteIds = req.getParameter("deleteKey");
                        req.setAttribute("deleteKey", deleteIds);
                        message = "Update failed!";
                        success = "false";
                        req.setAttribute("isInitForm", false);
                        break;
                    default:
                        message = "Your Format Options have been saved.";
                        success = "true";
                        req.setAttribute("isInitForm", true);
                        break;
                }
            }
            else
            {
                String deleteIds = req.getParameter("deleteKey");
                req.setAttribute("deleteKey", deleteIds);
                message = resultVO.getMessages().isEmpty() ? "Update failed!" : StringUtils.join(resultVO.getMessages(), ',');
                success = "false";
                req.setAttribute("isInitForm", false);
            }
        }
        else if ( ( deleteKey = findDeleteKey(req) ) != "-9999" )
        {
            String deleteIds = req.getParameter("deleteKey");

            //if (deleteIds != null && deleteIds/*.trim()*/.length() > 0) {
            if (StringUtils.isNotEmpty(deleteIds)) {
                deleteIds += ",";
            }
            deleteIds += "~" + deleteKey + "~";

            req.setAttribute("deleteKey", deleteIds);
            req.setAttribute("isInitForm", false);
        }

        if (isAjaxRequest() || (success != null && (success.equalsIgnoreCase("true") || success.equalsIgnoreCase("info")) ))
        {
            StringBuilder nextstep = new StringBuilder();
            nextstep.append(req.getContextPath()).append("/ManageFormatOptions?dataType=")
                            .append(dataType).append("&timestamp=").append(System.currentTimeMillis());

            if (StringUtils.isNotEmpty(message))
            //if (message != null && message.trim().length() > 0)
            {
                nextstep.append("&CompletedMessage=").append(message);
            }

            if (StringUtils.isNotEmpty(success))
            //if (success != null && success.trim().length() > 0)
            {
                nextstep.append("&success=").append(success);
            }

            res.sendRedirect(nextstep.toString());
            //log.exiting(className, "doPost");
            res.setContentType("text/html");
            return;
        }
        else // Otherwise Dispatch the form
        {
            RequestDispatcher dispatcher;

            if (StringUtils.isNotEmpty(message))
            //if (message != null && message.trim().length() > 0)
            {
                req.setAttribute("CompletedMessage", message);
            }

            if (StringUtils.isNotEmpty(success))
            //if (success != null && success.trim().length() > 0)
            {
                req.setAttribute("success", success);
            }

            Map<String, Object> record = EditRecordHelper.getFormParameterMap(req);
            req.setAttribute("rec", record);

            initForm(dataType,req,res);

            String destination = "/jsp/manageFormatOptions.jsp?dataType=" + dataType;
            dispatcher = req.getRequestDispatcher(destination);

            // Get the referer and set in the session servlet context.
            // This value will be retrieved by the PDF upload to allow returning to the
            // servlet which referred to this page, which is most likely ItemList servlet.
            String returnTo = req.getHeader("referer");
            req.getSession().setAttribute("returnTo", returnTo);

            res.setHeader("cache-control", "no-cache, must-revalidate");
            res.addHeader("Pragma", "no-cache");
            res.setDateHeader("Expires", System.currentTimeMillis() - 2);
            dispatcher.forward(req, res);
        }
    }


    /**
     * Validate the manage format options form
     *
     * @param formatData
     * @return ResultVO
     */
    private final ResultVO validateForm(LinkedHashMap<String, String> formatData)
    {
        String deleteIds = formatData.get("deleteKey");
        String[] deleteIdsArray = deleteIds.split("\\s*~\\s*");
        List<String> deleteKeys = java.util.Arrays.asList(deleteIdsArray);

        int pfCount = Integer.parseInt("0" + formatData.get("pfCount"));
        String fieldName = null;
        String formatPattern = null;
        boolean bold = false;
        boolean italic = false;
        boolean underline = false;
        String rowID = null;
        String value = "";

        // Hold the unique format patterns string
        List<String> formatDataList = new ArrayList<String>();

        // Hold the correct row number of format patterns. Its used to display duplicate row number.
        List<String> formatDataIdList = new ArrayList<String>();

        String errorMessages = "";
        int errorCount = 0;

        // why doesn't this just go from 1 to N instead of 0 to N-1 ?
        //   e.g. for (int index = 1; index <= pfCount; index++)
        // that all those "(index + 1)" could just be "index"
//        for (int index = 0; index < pfCount; index++)
        for (int index = 1; index <= pfCount; index++)
        {
            // process undeleted formats
            if (deleteKeys == null || deleteKeys.size() == 0 || !deleteKeys.contains(String.valueOf(index)))
            {
                fieldName = formatData.get("pf" + index + ":");

                // If there is no field name, it has been deleted.
                if (fieldName == null)
                {
                    continue;
                }

                formatPattern = formatData.get("pf" + index + ":pattern");
                bold = "on".equals(formatData.get("pf" + index + ":bold"));
                italic = "on".equals(formatData.get("pf" + index + ":italic"));
                underline = "on".equals(formatData.get("pf" + index + ":underline"));
                rowID = formatData.get("pf" + index + ":rowid");
                value = fieldName + "<val/>" + formatPattern + "<val/>" + bold + "<val/>" + italic + "<val/>" + underline;

                boolean hasError = false;
                if (!fieldName.equals("-- Select A Field --") || formatPattern.trim() != "" || (bold || italic || underline))
                {
                    if (fieldName.equals("-- Select A Field --"))
                    {
                        hasError = true;
                    }

                    else if (formatPattern.trim() == "")
                    {
                        hasError = true;
                    }

                    else if (!(bold || italic || underline))
                    {
                        hasError = true;
                    }

                    if (hasError)
                    {
                        String errorRowid = formatData.get("pf" + index + ":rowid");
                        errorMessages += "<li>Format patterns row " + errorRowid +
                                         " : All three fields are required to set a format pattern.</li>";
                        errorCount++;
                    }

                    int rowId = formatDataList.indexOf(value);
                    if (rowId == -1)
                    {
                        formatDataList.add(value);
                        formatDataIdList.add(rowID);
                    }
                    else if (!hasError) // check for duplicate format patterns
                    {
                        /*System.out.println("rowId :: "+rowId);
                        System.out.println("formatDataList :: "+formatDataList);
                        System.out.println("formatDataIdList :: "+formatDataIdList);
                        System.out.println("rowID :: "+(formatDataIdList.get(rowId)).replace(".", ""));*/
                        rowID = formatDataIdList.get(rowId);
                        String errorRowid = formatData.get("pf" + index + ":rowid");
                        errorMessages += "<li>Format patterns row " + errorRowid + " : Duplicate of the format pattern on row " + rowID + "</li>";
                        errorCount++;
                    }
                }
            }
            else
            {
                logger.info("deleted record: {}", index);
            }
        }

        if (errorCount == 0)
        {
            return new ResultVO(true);
        }
        else
        {
            errorMessages = "<div id=\"errorbox\">" + (errorCount > 1 ? "Errors:" : "Error:") + "\n<ul>" + errorMessages + "</ul></div>";
            return new ResultVO(false, errorMessages);
        }
    }


    /**
     * Find the record ID key in the request parameters.
     * Searches the parmeters for one in the form "D&lt;nnnn&gt;=Delete"
     * where &lt;nnnn&gt; is any 1 or more digit number.
     * @return the key parameter, or <code>-9999</code> if none was found.
     */
    private final String findDeleteKey(HttpServletRequest req)
    {
        Map<?,?> params = req.getParameterMap();

        String keyval = "-9999";

        for (Object k : params.keySet())
        {
            String p = (String)k;
            if (p.matches("D[0-9]+"))
            {
                String[] vals = req.getParameterValues(p);
                for (String v : vals)
                {
                    if (v.equalsIgnoreCase("Delete"))
                    {
                        keyval = p;
                        break;
                    }
                }
            }
        }

        if (StringUtils.isNotEmpty(keyval) && !keyval.equals("-9999")) {
            keyval = keyval.replaceFirst("[a-zA-Z]*", "");
        }

        return keyval;
    }


    /**
     * @param req
     * @return
     */
    private LinkedHashMap<String, String> getFormData(HttpServletRequest req)
    {
        logger.debug("* PARAMETERS RECEIVED in ManageFormatOptions.doPost()");

        LinkedHashMap <String,String>updatedFormatData = new LinkedHashMap<String, String>();

        @SuppressWarnings("unchecked")
        Map<String, String[]> parms = req.getParameterMap();
        for (String pname : parms.keySet())
        {
            String pval = req.getParameter(pname);
            logger.debug("     {}=[{}]", pname, pval);
            // updated format field parameter names contain colons in the form
            // "fieldNumber:fieldOption" example: "df1:bold" represents the
            // data field 1 (df1) bold option (bold)
            // Only send the colon delimited parameter names and values to update
            if (pname.contains(":")) {
                updatedFormatData.put(pname, pval);
            }

            updatedFormatData.put("deleteKey", req.getParameter("deleteKey"));
            updatedFormatData.put("pfCount", req.getParameter("pfCount"));
        }

        return updatedFormatData;
    }
}
