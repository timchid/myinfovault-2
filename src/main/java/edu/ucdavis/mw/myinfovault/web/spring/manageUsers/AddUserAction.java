/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AddUserAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.util.List;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;

/**
 * Webflow form action for adding users.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.1
 */
public class AddUserAction extends MivFormAction
{
    /**
     * Create add user form action bean.
     *
     * @param userService Spring-injected user service
     * @param authService Spring-injected authorization service
     */
    public AddUserAction(UserService userService,
                         AuthorizationService authService)
    {
        super(userService, authService);
    }

    /**
     * Checks if user has general permission to add a user.
     *
     * @param context Request context from webflow
     * @return boolean status, success or error
     */
    public Event hasPermission(RequestContext context)
    {
        return authorizationService.hasPermission(getShadowPerson(context),
                                                  Permission.ADD_USER,
                                                  null)
             ? allowed()
             : denied();
    }

    @Override
    public Event initFormAction(RequestContext context)
    {
        return success();
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#bind(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event bind(RequestContext context) throws Exception
    {
        Event status = super.bind(context);

        AddUserForm addForm = (AddUserForm) getFormObject(context);

        List<MivPerson> foundPeople = userService.findPersonByEmail(addForm.getSearchText());

        /*
         * No results and multiple results are errors; there should be only one result.
         */
        addForm.setCampusPerson(foundPeople.size() != 1
                           ? null
                           : foundPeople.get(0));

        return status;
    }

    /**
     * Add the bread crumb appropriate for the child flow.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event addBreadcrumb(RequestContext context)
    {
        //add manage users bread crumb
        addBreadcrumbs(context, "Add a New User");

        return success();
    }
}
