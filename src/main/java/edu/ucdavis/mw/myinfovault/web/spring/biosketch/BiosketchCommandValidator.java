/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchCommandValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class BiosketchCommandValidator implements Validator
{
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(BiosketchCommand.class);
    }

    /* (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object command, Errors errors)
    {
        BiosketchCommand biosketchCommand = (BiosketchCommand) command;
        validateName(biosketchCommand.getName(), errors);
        validateTitle(biosketchCommand.getTitle(), errors);

        if (biosketchCommand.getBiosketchType()!=null && biosketchCommand.getBiosketchType() == BiosketchType.BIOSKETCH)
        {
            validateProgramDirectorName(biosketchCommand.getProgramDirectorName(), errors);
            validateFullName(biosketchCommand.getPositionTitle1(), errors);
            //validateEraUserName(biosketchCommand.getEraUserName(), errors);
            validatePositionTitle1(biosketchCommand.getPositionTitle1(), errors);
        }

    }

    /**
     * @param errors
     */
    private void validateName(String field, Errors errors)
    {
        if (!(field != null && field.length()>0))
        {
                errors.reject("name","Name cannot be empty");
        }
    }

    private void validateTitle(String field, Errors errors)
    {
        if (!(field != null && field.length()>0))
        {
                errors.reject("title","Title cannot be empty");
        }
        if (field.length()>50)
        {
            errors.reject("title","Title cannot be exceed 50 characters");
        }
    }

    private void validateFullName(String field, Errors errors)
    {
        if (!(field != null && field.length()>0))
        {
                errors.reject("fullName","Full Name cannot be empty");
        }
    }

    private void validateProgramDirectorName(String field, Errors errors)
    {
        if (!(field != null && field.length()>0))
        {
                errors.reject("programdirectorname","Program Director/Prinicipal Investigator Name cannot be empty");
        }
    }

    private void validatePositionTitle1(String field, Errors errors)
    {
        if (!(field != null && field.length()>0))
        {
                errors.reject("positionTitle1","Position Title 1 cannot be empty");
        }
    }
}
