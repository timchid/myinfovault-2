/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DepartmentListFilter.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.util.ArrayList;
import java.util.Collection;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;

/**
 * <p>New version: uses only Assigned Roles, not Appointments</p>
 * Filter the "constants" list of school-department to include only the school and department indicated.
 * Expects to be handed Map<String,String> items that include keys for "schoolid" and "departmentid".
 * Setting a school ID of <em>N</em> with department ID of 0 (zero)
 * means get all departments in school <em>N</em>.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class DepartmentListFilter extends SearchFilterAdapter<AppointmentScope>
{
    private final Collection<AssignedRole> assignments = new ArrayList<AssignedRole>();


    public DepartmentListFilter(Collection<AssignedRole> assignments)
    {
        this.assignments.clear();
        this.assignments.addAll(assignments);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(AppointmentScope item)
    {
        logger.debug("DepartmentListFilter.include: item=[{}]", item);
        logger.debug("  Item Scope is {}", item);

        for (AssignedRole ar : assignments)
        {
            switch (ar.getRole())
            {
                case SYS_ADMIN:
                case VICE_PROVOST_STAFF:
                case SCHOOL_STAFF:
                case DEAN:
                case DEPT_STAFF:
                case DEPT_ASSISTANT:
                case DEPT_CHAIR:
                    logger.debug("  Testing role {} with scope {}", ar.getRole(), ar.getScope());
                    if (ar.getScope().matches(item))
                    {
                        logger.debug("    {} matches {}", ar.getScope(), item);
                        return true;
                    }
                    else
                    {
                        logger.debug("    {} and {} do not match", ar.getScope(), item);
                    }
                    break;

                case SENATE_STAFF:
                    return item.getSchool() == 42; // MAGIC NUMBER ALERT!  School #42 is the "Academic Senate"

                default:
                    logger.debug("  Skipping role {}", ar.getRole());
                    continue;
            }
        }

        return false;
    }

}
