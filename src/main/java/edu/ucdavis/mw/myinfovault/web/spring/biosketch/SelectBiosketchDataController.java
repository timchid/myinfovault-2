/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SelectBiosketchDataController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.mapping.biosketch.BiosketchMapper;
import edu.ucdavis.mw.myinfovault.service.biosketch.BiosketchService;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.SectionFetcher;
import edu.ucdavis.myinfovault.format.RecordFormatter;


/**
 * @author dreddy
 * @since MIV 2.1
 *
 * Updated: Pradeep on 08/03/2012
 */
@Controller
@RequestMapping("/biosketch/SelectBiosketchData")
@SessionAttributes("selectDataCommand")
public class SelectBiosketchDataController extends BiosketchFormController
{
    @Autowired
    private BiosketchService biosketchService;

    @Autowired
    public SelectBiosketchDataController(BiosketchService biosketchService)
    {
        this.biosketchService = biosketchService;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder)
    {
        binder.setValidator(new SelectDataCommandValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView setup(HttpServletRequest request)
    {
        String biosketchID = (String) request.getSession().getAttribute("biosketchID");
        if (biosketchID == null)
        {
            return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/BiosketchPreview"));
        }
        ModelMap model = new ModelMap();
        BiosketchType biosketchType = (BiosketchType) request.getSession().getAttribute("biosketchType");
        String showwizard = (String) request.getSession().getAttribute("showwizard");

        Properties p = PropertyManager.getPropertySet(biosketchType.getCode(), "config");
        Map<String, Object> config = new HashMap<String, Object>();
        p = PropertyManager.getPropertySet(biosketchType.getCode(), "labels");
        config.put("labels", p);
        p = PropertyManager.getPropertySet(biosketchType.getCode(), "tooltips");
        config.put("tooltips", p);
        Properties pstr = PropertyManager.getPropertySet(biosketchType.getCode(), "strings");
        config.put("strings", pstr);
        request.setAttribute("constants", config);

        model.addAttribute("showwizard", showwizard);

        SelectDataCommand selectDataCommand = new SelectDataCommand();

        if (biosketchID != null && Integer.parseInt(biosketchID) != 0)
        {
            Biosketch biosketch = biosketchService.getBiosketch(Integer.parseInt(biosketchID));

            (new BiosketchMapper()).mapBiosketchToSelectDataCommand(biosketch,
                                                                    selectDataCommand,
                                                                    biosketchType.getCode());

            //if biosketch is a CV
            if (biosketch.getBiosketchType() == BiosketchType.CV) {
                //clear out the default sections
                selectDataCommand.getBiosketchSection().clear();

                SectionFetcher sf = (new MIVUserInfo(biosketch.getUserID())).getSectionFetcher();

                //decide if each biosketch section is to be added to the section list
                for (BiosketchSection biosketchSection : biosketch.getBiosketchSections())
                {
                    /*
                     * Only include sections  with a 'DisplayInBiosketch' code of 0 or 1000
                     * AND with data in the selection list for this section (MIV-4326)
                     */
                    if ((biosketchSection.getDisplayInBiosketch() == 0 || biosketchSection.getDisplayInBiosketch() == 1000)
                     && !sf.getRecordSection(Integer.toString(biosketchSection.getSectionID()),
                                             false,
                                             biosketch.getRuleset(),
                                             new RecordFormatter[0]).isEmpty())
                    {
                        selectDataCommand.getBiosketchSection().add(new BiosketchSectionCommand(biosketchSection,biosketchType.getCode()));
                    }
                }
            }
        }

        model.addAttribute("selectDataCommand", selectDataCommand);

        return new ModelAndView("selectbiosketchdata", model);
    }


    @RequestMapping(method = RequestMethod.POST)
    protected ModelAndView onSubmit(HttpServletRequest request,
            @Valid @ModelAttribute("selectDataCommand") SelectDataCommand selectDataCommand)
    {

        logger.debug("************************ entering SelectBiosketchDataController ***************************");

        String btnName = request.getParameter("btnSubmit");
        String biosketchID = (String) request.getSession().getAttribute("biosketchID");

        Biosketch biosketch = null;
        BiosketchMapper biosketchMapper = new BiosketchMapper();

        // This is an update if the button is "save", or a new record if button is "proceed"
        boolean isUpdate = btnName.equalsIgnoreCase("save");

        biosketch = biosketchService.getBiosketch(Integer.parseInt(biosketchID));
        if (isUpdate || btnName.equalsIgnoreCase("proceed")) {
            biosketchService.deleteBiosketchRulesetListWithCriteria(biosketch.getRuleset());
        }
        biosketch = biosketchMapper.mapSelectDataCommandToBiosketch(selectDataCommand, biosketch);
        biosketchService.saveBiosketch(biosketch);
        String destination = isUpdate ? "/biosketch/BiosketchMenu" : "/biosketch/DesignMyBiosketch";
        return new ModelAndView(new RedirectView(request.getContextPath() + destination));
    }

    @ExceptionHandler
    public ModelAndView handleException(BindException result)
    {
        return new ModelAndView("selectbiosketchdata");
    }
}
