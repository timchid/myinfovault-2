/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: VetMedImportForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.vetmed;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import edu.ucdavis.mw.myinfovault.domain.vetmed.CourseEvaluation;
import edu.ucdavis.mw.myinfovault.domain.vetmed.CourseEvaluationTerm;
import edu.ucdavis.mw.myinfovault.domain.vetmed.CourseEvaluationType;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.util.CSVIteration;

/**
 * Form backing for VetMed import.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class VetMedImportForm implements Serializable
{
    private static final long serialVersionUID = 201308091529L;
    private static final File upload_directory = new File("/tmp");

    private Set<File> uploaded = new HashSet<File>();
    private Set<CourseEvaluation> processed = new HashSet<CourseEvaluation>();
    private List<CourseEvaluation> persisted = new ArrayList<CourseEvaluation>();
    private List<Map<String, String>> invalid = new ArrayList<Map<String, String>>();

    /**
     * @return uploaded files
     */
    public Set<File> getUploaded()
    {
        return uploaded;
    }

    /**
     * @return process course evaluation records
     */
    public Set<CourseEvaluation> getProcessed()
    {
        return processed;
    }

    /**
     * @return process course evaluation records
     */
    public List<CourseEvaluation> getPersisted()
    {
        return persisted;
    }

    /**
     * @return invalid processed evaluation records
     */
    public List<Map<String, String>> getInvalid()
    {
        return invalid;
    }

    /**
     * @param uploadFile file uploaded
     */
    public void setUpload(CommonsMultipartFile upload) throws IllegalStateException, IOException
    {
        if (upload != null)
        {
            File uploadFile = new File(upload_directory, upload.getOriginalFilename());

            if (!uploaded.contains(uploadFile))
            {
                upload.transferTo(uploadFile);

                List<CourseEvaluation> evaluations = new ArrayList<CourseEvaluation>();

                for (Map<String, String> record : new CSVIteration(uploadFile))
                {
                    String mothraid = record.get("mothraid");
                    String term_code = record.get("term_code");
                    String course_name = record.get("course_name");
                    String course_title = record.get("course_title");
                    String response = record.get("response");
                    String instructor_score = record.get("instructor_score");
                    String course_score = record.get("course_score");
                    String url = record.get("url");

                    Exception exception = null;
                    try
                    {
                        evaluations.add(new CourseEvaluation(MivServiceLocator.getUserService().getPersonByPersonUuid(mothraid),
                                                             CourseEvaluationType.SUMMARY,
                                                             CourseEvaluationTerm.getTerm(term_code.substring(4, 6)),
                                                             Integer.parseInt(term_code.substring(0, 4)),
                                                             course_name,
                                                             course_title,
                                                             response,
                                                             instructor_score,
                                                             course_score,
                                                             new URL(url)));
                    }
                    catch (IndexOutOfBoundsException e)//bad term_code
                    {
                        exception = e;
                    }
                    catch (NumberFormatException e)//bad term_code
                    {
                        exception = e;
                    }
                    catch (MalformedURLException e)//bad url
                    {
                        exception = e;
                    }
                    catch (IllegalArgumentException e)//bad mothraid
                    {
                        exception = e;
                    }
                    finally
                    {
                        // add record to the invalid pile
                        if (exception != null) invalid.add(record);
                    }
                }

                processed.addAll(evaluations);

                uploaded.add(uploadFile);
            }
        }
    }
}
