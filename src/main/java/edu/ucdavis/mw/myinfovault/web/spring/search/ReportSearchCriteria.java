/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReportSearchCriteria.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;

/**
 * Extends SearchCriteria to accommodate
 *
 * @author Craig Gilmore
 * @since MIV 4.5
 */
public class ReportSearchCriteria extends SearchCriteria implements Serializable
{
    private static final long serialVersionUID = 201207091506L;

    private CheckboxCriteria[] searchRequests = {};


    /**
     * @return Set of selected search requests
     */
    public CheckboxCriteria[] getSearchRequests()
    {
        return searchRequests;
    }


    /**
     * @param searchRequests
     *            Set of selected search requests
     */
    public void setSearchRequests(CheckboxCriteria[] searchRequests)
    {
        this.searchRequests = searchRequests;
    }

    /**
     * enum for search criteria values
     */
    public enum CheckboxCriteria
    {
        /**
         * display all actions that are currently at the Federation location
         */
        AllFed,
        /**
         * display all actions that are currently at the Senate location
         */
        AllSenate,
        /**
         * display all actions that are currently at the Senate/Federation location
         */
        AllSenateFed,
        /**
         * display all "completed" actions at the school location - ready to route
         */
        Audit,
        /**
         * display all actions at the Department with an unsigned Disclosure Certificate
         */
        DC,
        /**
         * display all actions at the School/College that are "released" but not signed by the dean
         */
        Dean,
        /**
         * display all actions that have an open review period - location determined by webflow
         */
        OR,
        /**
         * display all actions that have an open review period at the Federation
         */
        ORF,
        /**
         * display all actions that have an open review period at the Senate
         */
        ORS,
        /**
         * display all actions that have an open review period at the Senate/Federation
         */
        ORSF,
        /**
         * display all actions that have an open packet request...at the PacketRequest location
         */
        OPR,
        /**
         * display all actions with an incomplete joint appointment - location determined by webflow
         */
        IJA,
        /**
         * display all actions that are ready to route from their current location - determined by webflow
         */
        Ready,
        /**
         * display all actions that are ready to route from the Federation location
         */
        ReadyFed,
        /**
         * display all actions that are ready to route from the Senate location
         */
        ReadySenate,
        /**
         * display all actions that are ready to route from the Senate/Federation location
         */
        ReadySenateFed,
        /**
         * display all actions at the Vice Provost that are "released" but not signed by the VP
         */
        VP;
    }
}
