package edu.ucdavis.mw.myinfovault.web.spring.reviewdossiers;

import java.io.Serializable;

/**
 * FIXME: this class is not used. Remove setup call in reviewDossiers-flow.xml and invocation in openactions-flow-beans.xml
 * 
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class ReviewDossiersForm implements Serializable
{
    private static final long serialVersionUID = -9127769569124328427L;
}
