package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;

/**
 * Value Object to hold the result of {@link SearchStrategy#getSnapshots(MivPerson, Dossier)}
 * @author pradeeph
 * @since MIV 4.7.1
 */
public class SnapshotResultVO implements Serializable
{
	private static final long serialVersionUID = -4174447943490128814L;

	List <SnapshotDto> snapshotList = null;
	List<Snapshot> qualifiedSnapshotsList = null;
	boolean authorized = false;

	public SnapshotResultVO(boolean authorized)
	{
		this.authorized = authorized;
	}

	public SnapshotResultVO(List <SnapshotDto> snapshotList)
	{
		this.snapshotList = snapshotList;
	}

	public SnapshotResultVO(List <SnapshotDto> snapshotList,List<Snapshot> qualifiedSnapshotsList,boolean authorized)
	{
		this.snapshotList = snapshotList;
		this.qualifiedSnapshotsList = qualifiedSnapshotsList;
		this.authorized = authorized;
	}

	/**
	 * @return the qualifiedSnapshotsList
	 */
	public List<Snapshot> getQualifiedSnapshotsList() {
		return qualifiedSnapshotsList;
	}

	/**
	 * @param qualifiedSnapshotsList the qualifiedSnapshotsList to set
	 * @return self-reference for chaining
	 */
	public SnapshotResultVO setQualifiedSnapshotsList(List<Snapshot> qualifiedSnapshotsList) {
		this.qualifiedSnapshotsList = qualifiedSnapshotsList;
		return this;
	}

	/**
	 * @return the authorized
	 */
	public boolean isAuthorized() {
		return authorized;
	}

	/**
	 * @param authorized the authorized to set
	 * @return self-reference for chaining
	 */
	public SnapshotResultVO setAuthorized(boolean authorized) {
		this.authorized = authorized;
		return this;
	}
}
