/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchLDAP.java
 */

package edu.ucdavis.mw.myinfovault.web.util;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.web.MIVServlet;
import edu.ucdavis.myinfovault.data.UCDLdap;


/**
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class SearchLDAP extends MIVServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        ServletOutputStream out = null;
        try
        {
            out = response.getOutputStream();
            response.setContentType("text/html");

            prologue(out);

            if (! mivSession.get().getUser().getUserInfo().getPerson().hasRole(MivRole.SYS_ADMIN))
            {
                out.println("<p>You are not authorized to use this feature.</p>");
                epilogue(out);
                return;
            }

            out.println("<h2>LDAP Query</h2>");
            out.println("<p>Look up a person\"s Employee ID number from their kerberos login name.</p>");
            out.println("<div id=\"queryform\">");
            out.println(" <form name=\"ldapquery\" method=\"GET\" action=\"SearchLDAP\">");
            out.println("  <label for=\"searchlogin\">Login name to search for:</label>");
            out.println("  <input type=\"text\" name=\"searchlogin\" id=\"searchlogin\" autofocus>");
            out.println("  <input type=\"submit\" value=\"Search\">");
            out.println(" </form>\r\n</div><!--queryform-->\r\n");

            out.println("<p>Look up information from a general LDAP query.</p>");
            out.println("<div id=\"queryform2\">");
            out.println(" <form name=\"ldapquery2\" method=\"GET\" action=\"SearchLDAP\">");
            out.println("  <label for=\"search\">Query string to search for:</label>");
            out.println("  <input type=\"text\" name=\"search\" id=\"search\">");
            out.println("  <input type=\"submit\" value=\"Search\">");
            out.println("  <input type=\"radio\" name=\"provider\" id=\"provider_people\" value=\"people\" checked> <label for=\"provider_people\">People</label>");
            out.println("  <input type=\"radio\" name=\"provider\" id=\"provider_listings\" value=\"listings\"> <label for=\"provider_listings\">Listings</label>");
            out.println(" </form>\r\n</div><!--queryform2-->");

            out.println("<div id=\"results\">");

            String login = request.getParameter("searchlogin");
            String query = request.getParameter("search");
            String search; // will be set to either login or query
            UCDLdap.Provider provider = UCDLdap.Provider.PEOPLE; // use either the People or Listings provider.

            // Search for the login if that field was filled in
            if (login != null)
            {
                search = login;
            }
            // or, login wasn't filled in, get the query and which provider to use
            else
            {
                search = query;
                String p = request.getParameter("provider");
                if (p != null)
                {
                    try {
                        provider = UCDLdap.Provider.valueOf(p.toUpperCase());
                    }
                    catch (IllegalArgumentException e) {
                        // ignore the bad parameter
                    }
                }
            }

            if (search != null && search.length() > 0)
            {
                UCDLdap dir = new UCDLdap(provider);
                String result = null;
                if (query == null || query.length() < 1) {
                    result = dir.getEmpId(search);
                }

                // do the search and print the results here
                out.println(" <h2>Search Results</h2>");
                out.println(" <p>Searched for \"<span id=\"uid\">" + search + "</span>\" in " + provider + "</p>");
                if (result != null && result.length() > 0)
                {
                    out.println(" <p>Found Employee ID Number \"<span id=\"employeeNumber\">"+result+"</span>\"</p>");
                }
                else
                {
                    out.println(" <p>No ID number found</p>");
                }


                Map<String,String> results = null;
                if (query == null || query.length() < 1) {
                    results = dir.getEntry(search);
                }
                else {
                    results = dir.search(query, (String[])null);
                }

                if (results != null && results.size() > 0)
                {
                    out.println("<div class=\"resultTable\" id=\"details\">");
                    beginTable(out, "Attribute", "Value");
                    for (java.util.Map.Entry<String,String> entry : results.entrySet())
                    {
                        out.println("    <tr><td>"+entry.getKey()+"</td><td>"+entry.getValue()+"</td></tr>");
                    }
                    finishTable(out);
                    out.println("</div><!--details-->\r\n");
                }

/* Again, to test getEntry(String,String...) where params 2..N are the attributes to fetch. */
                results = dir.getEntry(search, "ucdPersonUUID", "ucdPersonAffiliation", "mail");

                if (results != null && results.size() > 0)
                {
                    out.println("<div class=\"resultTable\" id=\"details2\">");
                    beginTable(out, "Attribute", "Value");
                    for (java.util.Map.Entry<String,String> entry : results.entrySet())
                    {
                        out.println("    <tr><td>"+entry.getKey()+"</td><td>"+entry.getValue()+"</td></tr>");
                    }
                    finishTable(out);
                    out.println("</div><!--details-->");
                }

                dir.close();
            }

            out.println("</div><!--results-->");

            epilogue(out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }


    void prologue(ServletOutputStream out)
    {
        try {
            out.println("<!DOCTYPE html>");
            out.println("<html>\r\n<head>");
            out.println(" <title>MIV LDAP Lookup</title>");
//            out.println(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../myinfovault.css\">");
            out.println(" <link rel=\"shortcut icon\" href=\"../images/favicon.ico\" type=\"image/x-icon\">");
            out.println(" <style>");
            out.println("  div.resultTable {");
            out.println("    margin-bottom: 1.5em;");
            out.println("  }");
            out.println(" </style>");
            out.println("</head>\r\n<body>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    void epilogue(ServletOutputStream out)
    {
        try {
            out.println("</body>\r\n</html>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    int beginTable(ServletOutputStream out, String... columns)
        throws IOException
    {
        int colcount = 0;
        out.println("\n<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\">");
        out.print("  <thead>\n    <tr>");
        for (String s : columns)
        {
            out.print("<th>" + s + "</th>");
            colcount++;
        }
        out.println("</tr>\n  </thead>\n  <tbody>");
        return colcount;
    }


    void finishTable(ServletOutputStream out) throws IOException
    {
        out.println("  </tbody>\n</table>\n");
    }


}
