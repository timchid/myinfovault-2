/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AddUserFormController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import static edu.ucdavis.myinfovault.MIVSession.getSession;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.EditUserAuthorizer;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.MivRole.Category;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.ControllerBase;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivPropertyEditors;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.PropertyManager;


/**
 * Handles user menu and user add requests.
 *
 * @author Jacob Saporito
 * @since MIV 4.8.5
 */
@Controller
public class AddUserFormController extends ControllerBase
{
    /**
     * Servlet context relative URL to the manage users menu.
     */
    private static final String MENU_URL = "/user";

    /**
     * Servlet context relative URL to the add users page.
     */
    private static final String ADD_URL = MENU_URL + "/add";

    private static final String BREADCRUMB_GROUP = "usermanagement";

    private static final EnumSet<Category> categories = EnumSet.of(Category.STAFF, Category.FACULTY);

    private static final Properties strings = PropertyManager.getPropertySet("manageUsers-add-flow-search", "strings");
    private static final Properties tooltips = PropertyManager.getPropertySet("manageUsers-add-flow-search", "tooltips");
    private static final Properties confirmStrings = PropertyManager.getPropertySet("manageUsers-add-flow-confirm", "strings");

    private static final UserService userService = MivServiceLocator.getUserService();

    AddUserValidator addUserValidator;


    @Autowired
    public AddUserFormController(AddUserValidator addUserValidator)
    {
        this.addUserValidator = addUserValidator;
    }


    /**
     * Get manage users menu.
     *
     * @return redirect view to manage users flow
     */
    @RequestMapping(method=RequestMethod.GET, value=MENU_URL)
    public View getMenu(HttpServletRequest request)
    {
        /*
         * TODO: Temporarily redirecting to the manage users flow.
         * This may change to a more a RESTful solution.
         */
        return new RedirectView("ManageUsers?_flowId=manageUsers-flow", true);
    }


    /**
     * Get add user page.
     *
     * @param request servlet request
     * @return add user model and view
     */
    @RequestMapping(method=RequestMethod.GET, value=ADD_URL)
    public ModelAndView getAddUser(HttpServletRequest request)
    {
        MivPerson shadowPerson = getSession(request).getUser().getTargetUserInfo().getPerson();
        AddUserForm form = new AddUserForm();

        addBreadcrumb("Add a New User", BREADCRUMB_GROUP, request);

        List<MivRole> roles = new ArrayList<>();

        for (MivRole role : EditUserAuthorizer.getAssignableRoles(shadowPerson.getPrimaryRoleType(), true))
        {
            if (role.getCategory() == MivRole.Category.STAFF ||
                role.getCategory() == MivRole.Category.FACULTY)
            {
                roles.add(role);
            }
        }

        ModelAndView model = new ModelAndView("useradd", "form", form);
        model.addObject("strings", strings);
        model.addObject("tooltips", tooltips);
        model.addObject("categories", categories);
        model.addObject("roles", roles);

        return model;
    }


    /**
     * Post add user.
     *
     * @param role new user role
     * @param scope new user scopes
     * @return user add view
     */
    @RequestMapping(method=RequestMethod.POST, value=ADD_URL)
    public ModelAndView postAddUser(@ModelAttribute("form") AddUserForm form,
            BindingResult result,
            HttpServletRequest request)

    {
        MivPerson shadowPerson = getSession(request).getUser().getTargetUserInfo().getPerson();
        form.setShadowPerson(shadowPerson);

        addBreadcrumb("Add a New User: Confirmation", BREADCRUMB_GROUP, request);

        addUserValidator.validate(form, result);

        if (result.hasErrors())
        {
            return getAddUser(request);
        }


        ModelAndView confirmationView = null;

        // If we have a UserId when adding, then we are converting an appointee
        if (form.getUserId() > 1)
        {
            confirmationView = this.convertAppointee(form, shadowPerson.getUserId());
        }
        else
        {
            confirmationView = this.addNewUser(form);
        }

        confirmationView.addObject("strings", confirmStrings);
        return confirmationView;

    }


    /**
     * Return to referring page.
     *
     * @param request servlet request
     * @return referer's view
     */
    @RequestMapping(method=RequestMethod.POST, value=ADD_URL, params="cancel")
    public View getHome(HttpServletRequest request)
    {
        return new RedirectView(getReferrer(request));
    }


    /**
     * Get the referring page saved in the session for the given request.
     *
     * @param request servlet request
     * @return saved referrer
     */
    private static String getReferrer(HttpServletRequest request)
    {
        String referrer = request.getHeader(HttpHeaders.REFERER);
        if (referrer == null) {
            return request.getContextPath() + "/MIVMain";
        }
        return referrer;
        /*return MIVSession.getSession(request)
                         .getHttpSession()
                         .getAttribute(HttpHeaders.REFERER).toString();*/
    }


    /**
     * Save in the session for the given request Set the referring page saved in t.
     * @param request
     * @return saved referrer
     */
    private static String setReferrer(HttpServletRequest request)
    {
        String referrer = request.getHeader(HttpHeaders.REFERER);

        MIVSession.getSession(request)
                  .getHttpSession()
                  .setAttribute(HttpHeaders.REFERER, referrer);

        return referrer;
    }


    /**
     * Initialize the needed property editors
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {

        binder.registerCustomEditor(Category.class,
                new MivPropertyEditors.EnumPropertyEditor<>(Category.class));

        binder.registerCustomEditor(Integer.class,
                new MivPropertyEditors.IntegerPropertyEditor());

        binder.registerCustomEditor(Scope.class,
                new MivPropertyEditors.ScopePropertyEditor());

        binder.registerCustomEditor(Boolean.class,
                new MivPropertyEditors.BooleanPropertyEditor(true));
    }


    /**
     * Convert an appointee
     * @param form
     * @return ModelAndView userchangeconfirmation
     */
    private ModelAndView convertAppointee(AddUserForm form, int shadowPersonId)
    {
        MivPersonInternal addPerson = (MivPersonInternal) form.getCampusPerson();

        MivPersonInternal appointee = (MivPersonInternal) userService.getAppointee(form.getUserId());

        appointee.setPreferredEmail(addPerson.getPreferredEmail());
        appointee.setName(addPerson.getGivenName(), addPerson.getMiddleName(), addPerson.getSurname(), addPerson.getSuffix());
        appointee.setDisplayName(addPerson.getDisplayName());
        appointee.setPersonId(addPerson.getPersonId());
        // FIXME: person.getPrincipal() is documented to return NULL in some cases. The next line may throw a NullPointerException.
        appointee.setPrincipalName(addPerson.getPrincipal().getPrincipalName());

        // Remove all existing role assignments for this appointee
        appointee.clearRoles();

        // Add the CANDIDATE and MIV_USER roles for the primary appointment scope only. Only the CANDIDATE role assignment will be primary
        appointee.addRole(new AssignedRole(MivRole.CANDIDATE, appointee.getPrimaryAppointment().getScope(), true));
        appointee.addRole(new AssignedRole(MivRole.MIV_USER, appointee.getPrimaryAppointment().getScope(), false));

        // Save the appointee
        userService.saveAppointee(appointee, shadowPersonId);
        //Need to reload the equivalence map because we have updated the PersonUUID and Login
        userService.refresh();

        return new ModelAndView("userchangeconfirmation","form", form);
    }


    /**
     * Add a new user
     * @param form
     * @return ModelAndView userchangeconfirmation
     */
    private ModelAndView addNewUser(AddUserForm form)
    {

        // Get the person to add
        MivPerson addPerson =  form.getCampusPerson();

        // get the role selected from the form, which will be the primary role
        MivRole primaryRole = form.getSelectedRole();

        // Add the appointments and roles
        boolean isPrimary = true;      // First appointment is primary
        Map<Scope, AssignmentLine> assignedRoleMap = new HashMap<>();

        for (Object obj : form.getFormLines())
        {
            AssignmentLine aLine = (AssignmentLine) obj;

            // Skip if no scope "0:0"
            if (aLine.getScope().equals(Scope.NoScope))
            {
                continue;
            }

            Scope scope = aLine.getScope();

            // If this scope has already been added, skip it
            if (assignedRoleMap.containsKey(scope))
            {
                continue;
            }
            assignedRoleMap.put(scope, (AssignmentLine)obj);


            //Add the MIV_USER role for the primary role scope only.
            if (isPrimary)
            {
                addPerson.addRole(new AssignedRole(MivRole.MIV_USER, scope));
            }

            switch (primaryRole)
            {
                case CANDIDATE:
                    // Add the selected roles, Dean and Dept_Chair cannot be primary
                    if (aLine.getCandidate())
                    {
                        addPerson.addRole(new AssignedRole(MivRole.CANDIDATE, scope, isPrimary));
                        // add appointment for candidates
                        addPerson.addAppointment(new Appointment(scope, isPrimary));
                    }
                    if (aLine.getDean())
                    {
                        addPerson.addRole(new AssignedRole(MivRole.DEAN, scope));
                    }
                    if (aLine.getDeptChair())
                    {
                        addPerson.addRole(new AssignedRole(MivRole.DEPT_CHAIR, scope));
                    }
                    /*
                     * At this point it's possible that none of the above statements
                     * were executed.  That's OK, it means none of the checkboxes were checked
                     * so the current line isn't being used for any appointment or assignment.
                     * We'll just end up dropping it; the same as if "None" were picked for Scope.
                     */
                    if (!aLine.getCandidate() && !aLine.getDeptChair() && !aLine.getDean())
                    {
                        aLine.setScope(Scope.NoScope); //so they don't display on the confirmation page
                    }
                    break;
                default: //right now that means the primary role is some kind of Staff role
                    addPerson.addRole(new AssignedRole(primaryRole, scope, isPrimary));
                    break;
            }

            if (primaryRole.equals(MivRole.SCHOOL_STAFF))
            {
             // only allow one line to be assigned to School Admins.
                break;
            }

            isPrimary = false;
        }

        userService.addPerson(addPerson);
        return new ModelAndView("userchangeconfirmation","form", form);
    }
}
