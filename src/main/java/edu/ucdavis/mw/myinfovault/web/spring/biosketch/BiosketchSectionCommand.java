package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.designer.Availability;

/**
 * Command Object representing a BiosketchSection.
 *
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchSectionCommand extends BaseCommand
{
    private/* final */String sectionName;
    private/* final */boolean inSelectList;
    private/* final */Availability availability;
    private/* final */int displayInBiosketch;
    private boolean display;

    public BiosketchSectionCommand()
    {
        // Spring needs this
    }

    public BiosketchSectionCommand(BiosketchSection biosketchSection, String biosketchTypeName)
    {
        super(biosketchSection.getId(),
              biosketchSection.getUserID());

        //use section name key as default value
        sectionName = PropertyManager.getPropertySet(biosketchTypeName + "sectionname", "strings")
                                     .getProperty(biosketchSection.getSectionName(),
                                                  biosketchSection.getSectionName());

        inSelectList = biosketchSection.isInSelectList();
        availability = biosketchSection.getAvailability();
        displayInBiosketch = biosketchSection.getDisplayInBiosketch();
        display = biosketchSection.isDisplay();
    }

    public String getSectionName()
    {
        return sectionName;
    }

    public boolean isInSelectList()
    {
        return inSelectList;
    }

    public Availability getAvailability()
    {
        return availability;
    }

    public int getDisplayInBiosketch()
    {
        return displayInBiosketch;
    }

    public boolean isDisplay()
    {
        return availability == Availability.MANDATORY
            || availability == Availability.OPTIONAL_ON && display;
    }

    public void setDisplay(boolean display)
    {
        this.display = display;
    }
}
