/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventLogAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.event;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry;
import edu.ucdavis.mw.myinfovault.domain.event.EventLogFilter;
import edu.ucdavis.mw.myinfovault.events2.EventCategory;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.event.EventLogService;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;

/**
 * Spring Webflow controller that serves sorted and filtered event log entries.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class EventLogAction extends MivFormAction
{
    private static final String CATEGORY = "category";

    private final EventLogService eventLogService;

    /**
     * Create event log action bean.
     *
     * @param userService Spring-injected user service
     * @param authService Spring-injected authorization service
     * @param eventLogService Spring-injected event log service
     */
    protected EventLogAction(UserService userService,
                             AuthorizationService authService,
                             EventLogService eventLogService)
    {
        super(userService, authService);

        this.eventLogService = eventLogService;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#initFormAction(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event initFormAction(RequestContext context)
    {
        // put the person in the flow scope
        context.getFlowScope().put("person", getUser(context).getUserInfo().getPerson());
        // put URL event category parameter in the flow scope
        context.getFlowScope().put(CATEGORY, getParameterValue(context,
                                                               CATEGORY,
                                                               EventCategory.DOSSIER));

        return success();
    }


    /**
     * Is the real, logged-in user authorized to view the event log for this dossier?
     *
     * @param context Webflow request context
     * @return WebFlow event status; allowed or denied
     */
    public Event isAuthorized(RequestContext context)
    {
        //put dossier ID in permission details
        Dossier dossier = this.getDossier(context);

        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.TARGET_ID,
                              Long.toString(dossier.getDossierId()));

        EventCategory cat = context.getFlowScope().get(CATEGORY, EventCategory.class, null);
        String permission = cat == EventCategory.SIGNATURE ? Permission.VIEW_SIGNATURELOG : Permission.VIEW_EVENTLOG;

        return authorizationService.isAuthorized(getRealPerson(context),
                                                 permission,
                                                 permissionDetails,
                                                 null)
             ? allowed()
             : denied();
    }

    /**
     * Get event log entries for the given person.
     *
     * @param realPerson real, logged-in person
     * @param dossier dossier associated with the events
     * @param category event log category by which to filter
     * @return event log entries
     */
    public List<EventLogEntry> getEvents(MivPerson realPerson,
                                         Dossier dossier,
                                         EventCategory category)
    {
        /*
         * The capacity in which the real, logged-in person will view the event log.
         */
        Set<MivRole> capacity = EnumSet.noneOf(MivRole.class);

        /*
         * If the real person is the candidate for
         * this action, capacity must be CANDIDATE only.
         */
        if (dossier.getAction().getCandidate().equals(realPerson))
        {
            capacity.add(MivRole.CANDIDATE);
        }
        else
        {
            /*
             * Otherwise, event log viewed in the capacity of all assigned roles.
             */
            for (AssignedRole assignedRole : realPerson.getAssignedRoles())
            {
                capacity.add(assignedRole.getRole());
            }
        }

        // filter events available for this category and capacity
        return new EventLogFilter(category, capacity).apply(eventLogService.getEntries(dossier.getDossierId()));
    }

    /**
     * Get the event log page title for the given category.
     *
     * @param category event log category
     * @return event log page title
     */
    public String getPageTitle(EventCategory category)
    {
        return category.getDescription() + " Event Log";
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#loadProperties(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void loadProperties(RequestContext context)
    {
        super.loadProperties(context, DEFAULT_SUBSET, "labels", "formats", "fragments", "config", "data");
    }
}
