package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

/**
 * Command Object representing a BiosketchStyle
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchStyleCommand extends BaseCommand
{
    private String styleName;
    private String pageWidth;
    private String pageHeight;
    private String marginTop;
    private String marginRight;
    private String marginBottom;
    private String marginLeft;
    private String titleFontID;
    private String titleSize;
    private String titleAlign;
    private String titleBold;
    private String titleItalic;
    private String titleUnderline;
    private String titleRules;
    private String titleEditable;
    private String displayNameAlign;
    private String headerFontID;
    private String headerSize;
    private String headerAlign;
    private String headerBold;
    private String headerItalic;
    private String headerUnderline;
    private String headerIndent;
    private String bodyFontID;
    private String bodySize;
    private String bodyIndent;
    private String bodyFormatting;
    private String footerOn;
    private String footerPageNumbers;
    private String footerRules;

    /**
     * @return
     */
    public String getBodyFontID()
    {
        return bodyFontID;
    }

    /**
     * @param bodyFontID
     */
    public void setBodyFontID(String bodyFontID)
    {
        this.bodyFontID = bodyFontID;
    }

    /**
     * @return
     */
    public String getBodyFormatting()
    {
        return bodyFormatting;
    }

    /**
     * @param bodyFormatting
     */
    public void setBodyFormatting(String bodyFormatting)
    {
        this.bodyFormatting = bodyFormatting;
    }

    /**
     * @return
     */
    public String getBodyIndent()
    {
        return bodyIndent;
    }

    /**
     * @param bodyIndent
     */
    public void setBodyIndent(String bodyIndent)
    {
        this.bodyIndent = bodyIndent;
    }

    /**
     * @return
     */
    public String getBodySize()
    {
        return bodySize;
    }

    /**
     * @param bodySize
     */
    public void setBodySize(String bodySize)
    {
        this.bodySize = bodySize;
    }

    /**
     * @param footerOn
     */
    public void setFooterOn(String footerOn)
    {
        this.footerOn = footerOn;
    }

    /**
     * @return
     */
    public String getFooterPageNumbers()
    {
        return footerPageNumbers;
    }

    /**
     * @param footerPageNumbers
     */
    public void setFooterPageNumbers(String footerPageNumbers)
    {
        this.footerPageNumbers = footerPageNumbers;
    }

    /**
     * @return
     */
    public String getFooterRules()
    {
        return footerRules;
    }

    /**
     * @param footerRules
     */
    public void setFooterRules(String footerRules)
    {
        this.footerRules = footerRules;
    }

    /**
     * @return
     */
    public String getHeaderAlign()
    {
        return headerAlign;
    }

    /**
     * @param headerAlign
     */
    public void setHeaderAlign(String headerAlign)
    {
        this.headerAlign = headerAlign;
    }

    /**
     * @return
     */
    public String getHeaderFontID()
    {
        return headerFontID;
    }

    /**
     * @param headerFontID
     */
    public void setHeaderFontID(String headerFontID)
    {
        this.headerFontID = headerFontID;
    }

    /**
     * @return
     */
    public String getHeaderIndent()
    {
        return headerIndent;
    }

    /**
     * @param headerIndent
     */
    public void setHeaderIndent(String headerIndent)
    {
        this.headerIndent = headerIndent;
    }

    /**
     * @return
     */
    public String getHeaderSize()
    {
        return headerSize;
    }

    /**
     * @param headerSize
     */
    public void setHeaderSize(String headerSize)
    {
        this.headerSize = headerSize;
    }

    /**
     * @return
     */
    public String getMarginBottom()
    {
        return marginBottom;
    }

    /**
     * @param marginBottom
     */
    public void setMarginBottom(String marginBottom)
    {
        this.marginBottom = marginBottom;
    }

    /**
     * @return
     */
    public String getMarginLeft()
    {
        return marginLeft;
    }

    /**
     * @param marginLeft
     */
    public void setMarginLeft(String marginLeft)
    {
        this.marginLeft = marginLeft;
    }

    /**
     * @return
     */
    public String getMarginRight()
    {
        return marginRight;
    }

    /**
     * @param marginRight
     */
    public void setMarginRight(String marginRight)
    {
        this.marginRight = marginRight;
    }

    /**
     * @return
     */
    public String getMarginTop()
    {
        return marginTop;
    }

    /**
     * @param marginTop
     */
    public void setMarginTop(String marginTop)
    {
        this.marginTop = marginTop;
    }

    /**
     * @return
     */
    public String getPageHeight()
    {
        return pageHeight;
    }

    /**
     * @param pageHeight
     */
    public void setPageHeight(String pageHeight)
    {
        this.pageHeight = pageHeight;
    }

    /**
     * @return
     */
    public String getPageWidth()
    {
        return pageWidth;
    }

    /**
     * @param pageWidth
     */
    public void setPageWidth(String pageWidth)
    {
        this.pageWidth = pageWidth;
    }

    /**
     * @return
     */
    public String getStyleName()
    {
        return styleName;
    }

    /**
     * @param styleName
     */
    public void setStyleName(String styleName)
    {
        this.styleName = styleName;
    }

    /**
     * @return
     */
    public String getTitleAlign()
    {
        return titleAlign;
    }

    /**
     * @param titleAlign
     */
    public void setTitleAlign(String titleAlign)
    {
        this.titleAlign = titleAlign;
    }

    /**
     * @return
     */
    public String getTitleEditable()
    {
        return titleEditable;
    }

    /**
     * @param titleEditable
     */
    public void setTitleEditable(String titleEditable)
    {
        this.titleEditable = titleEditable;
    }

    /**
     * @return
     */
    public String getTitleFontID()
    {
        return titleFontID;
    }

    /**
     * @param titleFontID
     */
    public void setTitleFontID(String titleFontID)
    {
        this.titleFontID = titleFontID;
    }

    /**
     * @return
     */
    public String getTitleRules()
    {
        return titleRules;
    }

    /**
     * @param titleRules
     */
    public void setTitleRules(String titleRules)
    {
        this.titleRules = titleRules;
    }

    /**
     * @return
     */
    public String getTitleSize()
    {
        return titleSize;
    }

    /**
     * @param titleSize
     */
    public void setTitleSize(String titleSize)
    {
        this.titleSize = titleSize;
    }

    /**
     * @return
     */
    public String getFooterOn()
    {
        return footerOn;
    }

    public String getDisplayNameAlign()
    {
        return displayNameAlign;
    }

    public void setDisplayNameAlign(String displayNameAlign)
    {
        this.displayNameAlign = displayNameAlign;
    }

    public String getHeaderBold()
    {
        return headerBold;
    }

    public void setHeaderBold(String headerBold)
    {
        this.headerBold = headerBold;
    }

    public String getHeaderItalic()
    {
        return headerItalic;
    }

    public void setHeaderItalic(String headerItalic)
    {
        this.headerItalic = headerItalic;
    }

    public String getHeaderUnderline()
    {
        return headerUnderline;
    }

    public void setHeaderUnderline(String headerUnderline)
    {
        this.headerUnderline = headerUnderline;
    }

    public String getTitleBold()
    {
        return titleBold;
    }

    public void setTitleBold(String titleBold)
    {
        this.titleBold = titleBold;
    }

    public String getTitleItalic()
    {
        return titleItalic;
    }

    public void setTitleItalic(String titleItalic)
    {
        this.titleItalic = titleItalic;
    }

    public String getTitleUnderline()
    {
        return titleUnderline;
    }

    public void setTitleUnderline(String titleUnderline)
    {
        this.titleUnderline = titleUnderline;
    }
}
