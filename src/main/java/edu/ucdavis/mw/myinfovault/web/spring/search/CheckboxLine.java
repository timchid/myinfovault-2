/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CheckboxLine.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;
import java.io.Serializable;

/**
 * TODO: javadoc
 * 
 * @author Mary Northup
 * @since MIV 3.5
 */
public class CheckboxLine implements Serializable
{
    private static final long serialVersionUID = 200903181723L;
    private static final String TOSTRING_FORMAT = "CheckboxLine [selectKey=%s, selectText=%s, selectionDescription=%s, selected=%s]";
    
    private String selectKey = "";
    private String selectText = "";
    private String selectionDescription = "";
    private String selected = " ";

    /**
     * TODO: javadoc
     */
    public CheckboxLine(){}
    
    /**
     * TODO: javadoc
     * 
     * @param selectKey
     * @param selectText
     * @param selectionDescription
     */
    public CheckboxLine(String selectKey, String selectText, String selectionDescription)
    {
        this.selectKey = selectKey;
        this.selectText = selectText;
        this.selectionDescription = selectionDescription;
    }

    /**
     * @return
     */
    public String getSelectKey()
    {
        return selectKey;
    }

    /**
     * @param selectKey
     */
    public void setSelectKey(String selectKey)
    {
        this.selectKey = selectKey.trim();
    }

    /**
     * @return
     */
    public String getSelectText()
    {
        return selectText;
    }

    /**
     * @param selectText
     */
    public void setSelectText(String selectText)
    {
        this.selectText = selectText.trim();
    }

    /**
     * @return
     */
    public String getSelectionDescription()
    {
        return selectionDescription;
    }

    /**
     * @param selectionDescription
     */
    public void setSelectionDescription(String selectionDescription)
    {
        this.selectionDescription = selectionDescription.trim();
    }

    /**
     * @return
     */
    public String getSelected()
    {
        return selected;
    }

    /**
     * @param selected
     */
    public void setSelected(String selected)
    {
        this.selected = selected;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format(TOSTRING_FORMAT,
                             selectKey, selectText, selectionDescription, selected);
    }
}
