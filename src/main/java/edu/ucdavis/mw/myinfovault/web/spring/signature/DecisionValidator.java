/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import java.io.File;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.ucdavis.mw.myinfovault.domain.decision.DecisionType;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
  * Validates the decision signature form.
  *
  * @author Craig Gilmore
  * @since MIV 3.0
  */
public class DecisionValidator implements Validator
{
    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return DecisionForm.class.isAssignableFrom(clazz);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        DecisionForm form = ((DecisionForm) obj);

        /*
         * Decision type must be valid for the given document type.
         */
        if (form.getDecision().getDecisionType() == DecisionType.NONE)
        {
            errors.reject(null, form.getDecision().getDossier().getDelegationAuthorityDescription() + " Action");
        }

        /*
         * If signing as the provost's or chancellor's proxy, a wet signature must be provided.
         */
        if (form.getRealPerson().getUserId() != form.getSignature().getRequestedSignerId()
         && (
                 form.getDecision().getDocumentType().getAllowedSigner() == MivRole.PROVOST
              || form.getDecision().getDocumentType().getAllowedSigner() == MivRole.CHANCELLOR))
        {
            File wetSignature = form.getDecision().getWetSignature();

            if (wetSignature == null || !wetSignature.exists())
            {
                errors.reject(null, "A " + form.getDecision().getDocumentType().getDescription() + " wet signature must be provided");
            }
        }
    }
}
