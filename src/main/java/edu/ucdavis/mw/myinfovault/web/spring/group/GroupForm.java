/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.web.spring.assignment.AssignmentForm;
import edu.ucdavis.mw.myinfovault.web.spring.search.AssignedRoleFilter;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Form backing for group action.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupForm extends AssignmentForm implements Serializable
{
    private static final long serialVersionUID = 201108081434L;

    private static final Logger log = Logger.getLogger(GroupForm.class);

    private static final String SCHOOL_DEPT_DELIMITER = " &ndash; ";

    //MIV constants map keys
    private static final String ACTIVE_DEPARTMENTS = "activedepartments";
    private static final String SCHOOL_ID = "schoolid";
    private static final String DEPARTMENT_ID = "departmentid";

    //assigned roles map keys
    private static final String DISABLED = "disabled";
    private static final String CHECKED = "checked";
    private static final String LABEL = "label";
    private static final String VALUE = "value";

    private Group group;
    private int[] deactivatePeople = {};
    private int[] activatePeople = {};

    Map<MivRole, List<Map<String,Object>>> assignedRoles;

    /**
     * Create a form backing object for the given MIV group.
     *
     * @param group The group for the form.
     * @param actingPerson
     */
    public GroupForm(Group group,
                     MivPerson actingPerson)
    {
        super(group.getMembers(),
              group.getInactive(),
              actingPerson);

        this.group = group;
        this.assignedRoles = buildAssignedRoles();
    }

    /**
     * Get the group that this form represents.
     *
     * @return The form backing group.
     */
    public Group getGroup()
    {
        return group;
    }

    /**
     * Get user IDs of people to deactivate in the assigned collection.
     *
     * @return User IDs to deactivate
     */
    public int[] getDeactivatePeople()
    {
        return deactivatePeople;
    }

    /**
     * Set the user IDs of people to deactivate in the assigned collection.
     *
     * @param deactivatePeople User IDs to deactivate
     */
    public void setDeactivatePeople(int[] deactivatePeople)
    {
        this.deactivatePeople = deactivatePeople;
    }

    /**
     * Get user IDs of people to activate in the assigned collection.
     *
     * @return User IDs to activate
     */
    public int[] getActivatePeople()
    {
        return activatePeople;
    }

    /**
     * Set the user IDs of people to activate in the assigned collection.
     *
     * @param activatePeople User IDs to activate
     */
    public void setActivatePeople(int[] activatePeople)
    {
        this.activatePeople = activatePeople;
    }

    /**
     * Each role category (SYS_ADMIN, SCHOOL_STAFF, DEPT_STAFF) will have
     * a list of maps of attributes. Each attribute map represents
     * a check box for an assigned role associated with the group
     * and/or the acting person.
     *
     * @return The map of role to group role-scope assignments
     */
    public Map<MivRole, List<Map<String,Object>>> getAssignedRoles()
    {
        return assignedRoles;
    }

    //build the map of group assigned roles and acting person available assigned roles
    private  Map<MivRole, List<Map<String,Object>>> buildAssignedRoles()
    {
        Map<MivRole, List<Map<String,Object>>> assignedRoles = new TreeMap<MivRole, List<Map<String,Object>>>();

        //assigned roles associated with this group
        Set<AssignedRole> selectedRoles = group.getAssignedRoles();

        //assigned roles associated with the acting person and available to add to the group
        Set<AssignedRole> availableRoles = getActingPersonAssignedRoles();

        //get the complete set of all assigned roles for this group and the acting person
        Set<AssignedRole> combinedRoles = new HashSet<AssignedRole>(selectedRoles);
        combinedRoles.addAll(availableRoles);

        //loop thru each possible assigned role that is or can be associated with this group
        for(AssignedRole assignedRole : combinedRoles)
        {
            //role for this assigned role
            MivRole role = assignedRole.getRole();

            /*
             * Get the list of attribute maps for this role.
             *
             * Each role (SYS_ADMIN, SCHOOL_STAFF, DEPT_STAFF) has a
             * set of maps of attributes. Each attribute map represents
             * an assigned role.
             */
            List<Map<String,Object>> assignedRolesSet = assignedRoles.get(role);

            //if there is none yet for this role
            if(assignedRolesSet == null)
            {
                //create new set sorted by the map comparator
                assignedRolesSet = new ArrayList<Map<String,Object>>();

                //add it the role categories map
                assignedRoles.put(role, assignedRolesSet);
            }

            //add attributes entry to assigned roles set for this role
            assignedRolesSet.add(getAssignedRoleAttributes(assignedRole,
                                                           selectedRoles,
                                                           availableRoles));
        }

        return assignedRoles;
    }

    private Set<AssignedRole> getActingPersonAssignedRoles()
    {
        //assigned roles associated with the acting person
        Set<AssignedRole> actingPersonAssignedRoles = new HashSet<AssignedRole>(getActingPerson().getPrimaryRoles());

        //include constituent departments for school administrator roles
        if(getActingPerson().getPrimaryRoleType() == MivRole.SCHOOL_STAFF)
        {
            //get school administrator assigned roles (usually just one)
            Set<AssignedRole> schoolRoles = (new AssignedRoleFilter(MivRole.SCHOOL_STAFF)).apply(actingPersonAssignedRoles);

            //add all department roles for each school role
            actingPersonAssignedRoles.addAll(getDepartmentRoles(schoolRoles));
        }

        return actingPersonAssignedRoles;
    }

    //add to the given assigned roles the constituent department roles for each school administrator assigned role
    private Set<AssignedRole> getDepartmentRoles(Set<AssignedRole> schoolRoles)
    {
        Set<AssignedRole> departmentRoles = new HashSet<AssignedRole>();

        //examine the active departments
        for(Map<String,String> activedepartment : MIVConfig.getConfig().getConstants().get(ACTIVE_DEPARTMENTS))
        {
            //check the active department against each school role
            for(AssignedRole schoolRole : schoolRoles)
            {
                //the school ID for this assigned role
                int schoolId = schoolRole.getScope().getSchool();

                try
                {
                    //if the given school ID matches
                    if(schoolId == Integer.parseInt(activedepartment.get(SCHOOL_ID)))
                    {
                        //create/add new primary assigned role for this department
                        departmentRoles.add(new AssignedRole(MivRole.DEPT_STAFF,
                                                             new Scope(schoolId,
                                                                       Integer.parseInt(activedepartment.get(DEPARTMENT_ID))),
                                                             true));//set as primary
                    }
                }
                catch(NumberFormatException e)
                {
                    /*
                     * this really shouldn't happen, all departments in this map must be valid
                     */
                    log.error("MIV constants map of active departments contains " +
                              "invalid school-department ID of '" + activedepartment.get(SCHOOL_ID) +
                              ":" + activedepartment.get(DEPARTMENT_ID) + "'",
                              e);
                }
            }
        }

        return departmentRoles;
    }

    //Create and return a new attributes map for the given assigned role
    private Map<String,Object> getAssignedRoleAttributes(AssignedRole assignedRole,
                                                         Set<AssignedRole> selectedRoles,
                                                         Set<AssignedRole> availableRoles)
    {
        //Create new attributes map for this assigned role.
        Map<String,Object> attributes = new HashMap<String,Object>();

        //role for this assigned role
        MivRole role = assignedRole.getRole();

        /*
         * Form check box will be disabled if:
         * - the target person does not have the assigned role
         * - and the target person is not the group owner
         */
        Boolean disabled = !availableRoles.contains(assignedRole)
                        && !group.getCreator().equals(getActingPerson());

        /*
         * form check box will be checked if the assigned role
         * is associated both with group or the check box is disabled
         */
        Boolean checked = disabled || selectedRoles.contains(assignedRole);

        /*
         * build label attribute
         */
        StringBuilder label = new StringBuilder();
        switch(role)
        {
            case SYS_ADMIN:
            case VICE_PROVOST_STAFF:
            case SENATE_STAFF:
                /*
                 * Use role as label for the MIV administrator and Senate check
                 * boxes (there'll only be one role check box for these types)
                 */
                label.append(role.getDescription());
                label.append("s");//pluralize
                break;
            case DEPT_STAFF:
                //only the department scoped roles need the department name
                label.append(SCHOOL_DEPT_DELIMITER);
                label.append(assignedRole.getScope().getDepartmentDescription());
            case SCHOOL_STAFF:
                //both school and department scoped roles need the school name
                label.insert(0, assignedRole.getScope().getSchoolDescription());
                break;
            default:
                break;
        }

        //load attributes map for the form check box
        attributes.put(DISABLED, disabled);
        attributes.put(CHECKED, checked);
        attributes.put(LABEL, label);
        attributes.put(VALUE, assignedRole);

        return attributes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset()
    {
        super.reset();

        deactivatePeople = ArrayUtils.EMPTY_INT_ARRAY;
        activatePeople = ArrayUtils.EMPTY_INT_ARRAY;
    }
}
