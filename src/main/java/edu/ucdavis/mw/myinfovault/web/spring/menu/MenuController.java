/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MenuController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.menu;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Sets 'menupath' object in view with the servlet path for top level menu links.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
@Controller
@RequestMapping({"/EnterData",
                 "/MyDossier",
                 "/CVAndBiosketches",
                 "/Actions",
                 "/Reports",
                 "/Preferences"})
public class MenuController

{
    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView get(HttpServletRequest request)
    {
        ModelAndView mav = new ModelAndView("menu");

        mav.addObject("menupath", request.getServletPath());

        return mav;
    }
}
