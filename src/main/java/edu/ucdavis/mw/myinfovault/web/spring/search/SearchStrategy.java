/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivDisplayPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * This class defines all of the currently required user and dossier search methods.
 * The DefaultSearchStrategy class implements all methods in the interface. Specific
 * search strategies subclass the DefaultSearchStrategy and override search methods
 * as required.
 *
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public interface SearchStrategy
{
    /**
     * User service.
     *
     * FIXME: only used in {@link DefaultSearchStrategy} implementation and should be defined protected there.
     */
    public UserService userService = MivServiceLocator.getUserService();

    /**
     * Dossier service.
     *
     * FIXME: only used in the classes that inherit from the {@link DefaultSearchStrategy}
     * implementation and should be defined protected there.
     */
    public DossierService dossierService = MivServiceLocator.getDossierService();

    /**
     * Dossier service.
     *
     * FIXME: only used in the classes that inherit from the {@link DefaultSearchStrategy}
     * implementation and should be defined protected there.
     */
    public Logger logger = LoggerFactory.getLogger(SearchStrategy.class);

    /**
     * Basic search for users by their name with no SearchFilter applied.
     *
     * @param mivUser The target user doing the search
     * @param criteria - The search criteria to use
     * @return - A list of MivPerson objects
     */
    public List<MivPerson> getUsersByName(MIVUser mivUser, SearchCriteria criteria);

    /**
     * Basic search for users by their name.
     *
     * @param actor target user doing the search
     * @param criteria search criteria to use
     * @param s search filter to further filter the results
     * @return MIV person objects
     */
    public List<MivPerson> getUsersByName(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s);

    /**
     * Basic search for users by their department with no SearchFilter applied.
     *
     * @param mivUser target user doing the search
     * @param criteria search criteria to use
     * @return MIV person objects
     */
    public List<MivPerson> getUsersByDepartment(MIVUser mivUser, SearchCriteria criteria);

    /**
     * Basic search for users by their department.
     *
     * @param actor target user doing the search
     * @param criteria search criteria to use
     * @param s search filter to further filter the results
     * @return MIV person objects
     */
    public List<MivPerson> getUsersByDepartment(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s);

    /**
     * Basic search for users by their school with no SearchFilter applied.
     *
     * @param mivUser target user doing the search
     * @param criteria search criteria to use
     * @return MIV person objects
     */
    public List<MivPerson> getUsersBySchool(MIVUser mivUser, SearchCriteria criteria);

    /**
     * Basic search for users by their school.
     *
     * @param actor target user doing the search
     * @param criteria search criteria to use
     * @param s search filter to further filter the results
     * @return MIV person objects
     */
    public List<MivPerson> getUsersBySchool(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s);

    /**
     * Get the select filter to filter out entries for logged in user as well as the impersonated user.
     *
     * @param mivUser MIV user to filter out
     * @return select search filter
     */
    public SelectFilter getSelectFilter(MIVUser mivUser);

    /**
     * Dossier owner name specific search filtering.
     *
     * @param criteria main search form containing the search "criteria" requested
     * @param user MIVUser requesting the search
     * @param dossier dossiers to filter by the search criteria
     * @return MIV Action list objects
     * @throws WorkflowException
     */
    public List<MivActionList> dossierSearch(SearchCriteria criteria, MIVUser user, List<Dossier> dossier) throws WorkflowException;

    /**
     * Dossier search by department filtering.
     *
     * @param criteria - SearchCriteria : the main search form containing the search "criteria" requested
     * @param user - MIVUser requesting the search
     * @return MIV Action list objects
     */
    public List<MivActionList> getDossiersByDepartment(SearchCriteria criteria, MIVUser user);

    /**
     * Dossier search by school filtering.
     *
     * @param criteria - SearchCriteria : the main search form containing the search "criteria" requested
     * @param user - MIVUser requesting the search
     * @return MIV Action list objects
     */
    public List<MivActionList> getDossiersBySchool(SearchCriteria criteria, MIVUser user);

    /**
     * Get the Dossiers that this user is allowed to view to assign reviewers.
     *
     * @param user
     * @return MIV Action list objects
     */
    public List<MivActionList> getDossiersToAssignReviewers(MIVUser user);

    /**
     * Get the Dossiers for the given user at the given location.
     *
     * @param user who will be accessing the dossiers
     * @param location
     * @return MIV Action list objects
     */
    public List<MivActionList> getDossiersAtLocation(MIVUser user, DossierLocation location);

    /**
     * Get the snapshots that this user is allowed to view for the given dossier.
     *
     * @param person
     * @param dossier
     * @return List of MivActionList objects
     * @throws WorkflowException
     */
    public SnapshotResultVO getSnapshots (MivPerson person, Dossier dossier) throws WorkflowException;

    /**
     * Get the qualified reviewers for a given dossier.
     *
     * @param mivUser The current MIVuser
     * @param dossierId the dossier for which the reviewers are being qualified.
     * @return List of MivDisplayPerson objects
     * @throws WorkflowException
     */
    public List<MivDisplayPerson> getQualifiedDossierReviewers(MIVUser mivUser, long dossierId)
    throws WorkflowException;

    /**
     * Uses a list of attribute/value pairs to search
     * for dossiers that contain those attributes with
     * (or without if NOT_ is prefacing the value) those values.
     *
     * @param allCriteria contains the list of attributes and values for which to search
     * @param user the current MIVuser
     * @return MIV Action list objects
     */
    public List<MivActionList> dossierSearchByCriteria(Map<String, String> allCriteria, MIVUser user);
}
