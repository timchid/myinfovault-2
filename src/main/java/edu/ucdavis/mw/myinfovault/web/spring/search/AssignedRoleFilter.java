/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignedRoleFilter.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.Arrays;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Filters out assigned roles that do not contain one the given MIV roles.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class AssignedRoleFilter extends SearchFilterAdapter<AssignedRole>
{
    private final MivRole[] includeRoles;

    /**
     * @param includeRoles roles that each assigned role of the filtered collection must have
     */
    public AssignedRoleFilter(MivRole...includeRoles)
    {
        /*
         * Binary search requires that the search array be sorted.
         */
        Arrays.sort(includeRoles);
        
        this.includeRoles = includeRoles;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(AssignedRole item)
    {
        // include if item has one of the given roles
        return Arrays.binarySearch(includeRoles, item.getRole()) >= 0;
    }

}
