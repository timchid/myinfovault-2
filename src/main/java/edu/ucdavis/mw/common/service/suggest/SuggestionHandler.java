/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SuggestionHandler.java
 */

package edu.ucdavis.mw.common.service.suggest;

/**
 * Handles preparing suggestions for search criteria.
 *
 * @author Stephen Paulsen
 * @since MIV 4.4
 * @param <E> type of returned suggestions
 */
public interface SuggestionHandler<E>
{
    /**
     * Get suggestions for the given topic and search term.
     *
     * @param topic suggestion topic
     * @param term search term
     * @return suggestions for the given topic and search term
     */
    public Iterable<E> getSuggestions(String topic, String term);

    /**
     * Location of search term in matched suggestions.
     */
    enum MatchType {
        /** Find only when starts with search term */   PREFIX("{0}%"),
        /** Synonym for PREFIX */                       START("{0}%"),
        /** Find when search term appears anywhere */   ANY("%{0}%"),
        /** Synonym for ANY */                          ALL("%{0}%"),
        /** Find only exact matches */                  EXACT("{0}");

        private final String formatPattern;

        private MatchType(String pattern)
        {
            this.formatPattern = pattern;
        }

        public String getFormatPattern()
        {
            return formatPattern;
        }
    }
}
