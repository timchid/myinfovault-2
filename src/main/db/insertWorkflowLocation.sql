-- Author: Rick Hendricks
-- Date: 7/23/2009
-- Updated: 08/08/2012
-- Note: Replace 18635 with your internal system ID

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

truncate WorkflowLocation;

INSERT INTO WorkflowLocation (ID, WorkflowNodeName, InsertUserID)
VALUES
(20,'Unknown',18099),
(1,'PacketRequest',18635),
(2,'Department',18635),
(3,'School',18635),
(4,'ViceProvost',18635),
(5,'ReadyForPostReviewAudit',18635),
(6,'Archive',18635),
(7,'FederationSchool',18635),
(8,'FederationViceProvost',18635),
(9,'Senate',18635),
(10,'Federation',18635),
(11,'SenateFederation',18635),
(12,'PostSenateSchool',18635),
(13,'PostSenateViceProvost',18635),
(14,'PostAuditReview',18635),
(15,'SenateAppeal',18635),
(16,'FederationAppeal',18635),
(17,'FederationSenateAppeal',18635),
(18,'PostAppealSchool',18635),
(19,'PostAppealViceProvost',18635);


COMMIT;

UPDATE WorkflowLocation SET ID=0 WHERE ID=20;

COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
