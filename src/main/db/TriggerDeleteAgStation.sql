# Author: Stephen Paulsen
# Date: 2007-02-05

# Deletes on CvStation -- Ag Experiment Station

DELIMITER |

CREATE TRIGGER DeleteCvStation AFTER DELETE on CvStation
FOR EACH ROW BEGIN
  IF OLD.UploadName IS NULL
  THEN
  DELETE FROM ReportSummary
    WHERE
       TypeID=1 AND
       OldID=OLD.AESID
    ;
  ELSE
  DELETE FROM UserUploadDocument
    WHERE
       UserID=OLD.SSN AND
       DocumentID=9 AND
       UploadFile=OLD.UploadName AND
       Year=OLD.Dates
    ;
  END IF;
END;
|

DELIMITER ;

