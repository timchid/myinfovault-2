# Author: Lawrence Fyfe
# Date: 8/28/2006


DROP TABLE IF EXISTS Document;

create table Document
(
   ID int primary key auto_increment,
   Code varchar(10),
   RedactedCode varchar(10) NULL default NULL,
   Description varchar(255),
   AlternateDescription varchar(255),
   FileName varchar(255),
   DossierFileName varchar(255),
   AttributeName varchar(255),
   Uploadable boolean NOT NULL,
   MaxUploads int NOT NULL default -1,
   ConcatUpload boolean NOT NULL,
   CandidateFile boolean NOT NULL,
   AllowRedaction boolean NOT NULL default false ,
   CrossDepartmentPermission boolean NOT NULL default true, 
   Signable boolean NOT NULL default false ,
   UploadFirst boolean NOT NULL default false ,
   Active boolean NOT NULL default true ,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
