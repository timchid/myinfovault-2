# Author: Lawrence Fyfe
# Date: 10/4/2006

truncate TeachingType;

insert into TeachingType (ID,Description,InsertUserID)
values
(1,'Course',14021),
(2,'Lecture',14021),
(3,'Seminar',14021),
(4,'Lab',14021),
(5,'Presentation',14021),
(6,'University Extension',14021),
(7,'Small Group Teaching',14021),
(8,'Case Conference',14021),
(9,'Clinic',14021),
(10,'Operating Room',14021),
(11,'Other',14021);
