-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-05-23

-- Create the EventsReviewsAssociation Table
-- Dependencies: The Events and Reviews tables must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS EventsReviewsAssociation;

create table EventsReviewsAssociation
(
   UserID int NOT NULL,
   EventID int NOT NULL,
   ReviewID int NOT NULL,
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   PRIMARY KEY (`EventID`,`ReviewID`),
   INDEX(`EventID`,`ReviewID`),
   CONSTRAINT  `fk_er_eventid` FOREIGN KEY (`EventID`) REFERENCES `Events`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `fk_er_reviewid` FOREIGN KEY (`ReviewID`) REFERENCES `Reviews`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=INNODB,
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
