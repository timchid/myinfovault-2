-- Author: Rick Hendricks
-- Date: 12/01/2008

-- Change from v2.2 to v2.4
ALTER TABLE `PublicationSummary`
 add Column EngineeringAnnotation varchar(4) default NULL after Link
;
