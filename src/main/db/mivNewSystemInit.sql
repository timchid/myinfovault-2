##
# Top level script to create and initialize all tables and
# insert the original data in the control tables.
# The triggers to synch the old and new tables are then added.
#
# This can be run within MySql by sourcing it:
#   mysql> source mivNewSystemInit.sql;
# or in batch mode from the command line:
#   $ mysql -t -vvv -p < mivNewSystemInit.sql
##
use myinfovault;
source mivCreateTables.sql;
source mivInitData.sql;
#source mivInstallTriggers.sql;
source convertTables.sql;
