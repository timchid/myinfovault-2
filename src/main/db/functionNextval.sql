-- Based on article at http://www.microshell.com/database/mysql/emulating-nextval-function-to-get-sequence-in-mysql
--
-- The function is declared non deterministic because calling it with the same argument doesn't guarantee the same value.
-- As a matter of fact, it should never return the same value except when sequence_cycle is set.
--
-- After declaring variable, we select current sequence value from the table, putting the result to cur_val variable (line 6 - 12).
-- Then, if cur_val is not null (remember from my explanation why I have sequence_cur_value column null-able),
-- we want to increment the cur_val so that next time this function is called, the next sequence is ready to be fetched.
--
-- Updating sequence_cur_val is rather tricky. We have to consider:
-- 1. If the next sequence is still within the limit as specified in sequence_max_value, we'll simply increment sequence_cur_val
--  by value specified in sequence_increment.
--
-- 2. If the next sequence is outside the boundary, we need to check
--    * If sequence_cycle is set, reset sequence_cur_val to sequence_min_val.
--    * If sequence_cycle is not set, assign NULL as sequence_cur_val to identify overflow / error condition.
--
-- Then finally, we return cur_val. Note that the select on line 6 - 12 will return null if the sequence name doesn't exist.
-- Thus, there's only 2 cases when nextval() returns NULL (and both are error condition):
--
--   1. When the sequence doesn't exist
--   2. When current sequence value out of range
--
-- How to call nextval() function in MySQL
--
-- START TRANSACTION;
-- SELECT nextval('sq_my_sequence') as next_sequence FOR UPDATE;
-- COMMIT;

DELIMITER $$

DROP FUNCTION IF EXISTS `nextval` $$
CREATE FUNCTION `nextval` (`seq_name` varchar(100)) RETURNS bigint(20) NOT DETERMINISTIC
BEGIN
        DECLARE cur_val bigint(20);

        SELECT sequence_cur_value INTO cur_val
        FROM `sequence_data`
        WHERE sequence_name = seq_name
        COLLATE UTF8_UNICODE_CI;

        IF cur_val IS NOT NULL THEN
                UPDATE
                `sequence_data`
                SET
                sequence_cur_value = IF (
                        (sequence_cur_value + sequence_increment) > sequence_max_value,
                        IF (sequence_cycle = TRUE,
                                sequence_min_value,
                                NULL
                        ),
                        sequence_cur_value + sequence_increment
                )
                WHERE sequence_name = seq_name COLLATE UTF8_UNICODE_CI;
        END IF;
        RETURN cur_val;
END $$

DELIMITER ;
