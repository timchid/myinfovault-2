-- Author: Pradeep Haldiya
-- Created: 2011-12-16
-- Updated: 2012-01-03

SELECT count(*) FROM PatentStatus INTO @_ORIG_COUNT;

truncate PatentStatus;

INSERT INTO PatentStatus (ID, Status, Description, Active, InsertUserId)
VALUES
(1, 'Filed', 'An application for a patent that has been filed', true, 20720),
(2, 'Granted', 'A patent that has been granted', true, 20720),
(3, 'Denied', 'An application for a patent that has been denied', false, 20720),
(4, 'Expired', 'A patent that has expired', false, 20720),
(5, 'Provisional', 'A provisional application for a patent that has been filed', false, 18099);

SHOW WARNINGS;
commit;

SELECT count(*) FROM PatentStatus INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';