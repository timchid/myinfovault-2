-- Author: Rick Hendricks
-- Date: 07/23/2009
-- Updated: 08/08/2012
-- This table defines the workflow locations that a dossier document will traverse in workflow. The WorkflowNodeName
-- must match the nodes as defined in Kuali Enterprose Workflow (KEW) module.

drop table if exists WorkflowLocation;

create table WorkflowLocation
(
   ID int primary key auto_increment,
   WorkflowNodeName ENUM ('Unknown','PacketRequest','Department','School','FederationSchool','ViceProvost','FederationViceProvost','ReadyForPostReviewAudit','Archive','Senate','Federation','SenateFederation','PostSenateSchool','PostSenateViceProvost','PostAuditReview','SenateAppeal','FederationAppeal','FederationSenateAppeal','PostAppealSchool','PostAppealViceProvost') NOT NULL,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
