-- Author: Rick Hendricks
-- Created: 2012-04-09
-- Updated: 2012-05-23

-- Create the WorksPublicationEventsAssociation Table
-- Dependencies: The Works and PublicationEvents tables must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS WorksPublicationEventsAssociation;

create table WorksPublicationEventsAssociation
(
   UserID int NOT NULL,
   WorkID int NOT NULL,
   PublicationEventID int NOT NULL,
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   PRIMARY KEY (`WorkID`,`PublicationEventID`),
   INDEX(`WorkID`,`PublicationEventID`),
   CONSTRAINT  `fk_wpe_workid` FOREIGN KEY (`WorkID`) REFERENCES `Works`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `fk_wpe_publicationeventid` FOREIGN KEY (`PublicationEventID`) REFERENCES `PublicationEvents`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=INNODB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
