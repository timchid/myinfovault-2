--  Author: Stephen Paulsen
-- Created: 2009-03-05
-- Updated: 2009-03-26

--
-- IMPORTANT!
-- See Jira ticket MIV-2412 before running this script to
-- resolve existing users that have invalid department numbers.
--


-- Migrates data from the old CvMem table to the new UserAccount table
-- Dependencies: SystemSchool table and SystemDepartment table must exist and be populated

truncate UserAccount;

insert into UserAccount
(UserID, Login, SchoolID, DepartmentID, Surname, GivenName, Email, Devteam, CvLevel, InsertUserID)
select Uid, lower(Login), SchId, DeptId, Lname, Fname, EmailAddress, DevTeam, CvLevel, 18099 from CvMem;

-- Inactivate all users in department 1000
-- update UserAccount set active=false where departmentid=1000;
-- Deactivation will be done manually after restoring the user's original school and department.

-- Jump up to 20,000 for new MIV user ID values
ALTER TABLE UserAccount AUTO_INCREMENT=20000;

