-- Author: Rick Hendricks
-- Created: 5/29/2009
-- Updated: 2013-06-05 (SDP)

create table DossierSnapshot
(
   ID int primary key auto_increment,
   DossierID int NOT null,
   MivRoleID varchar(20) NOT null,
   SchoolID int NOT null,
   DepartmentID int NOT null,
   Description varchar(100) default null,
   SnapshotFileName varchar(128) not null,
   SnapshotLocation varchar(40),
   MivVersion varchar(10),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
