-- Author: Rick Hendricks
-- Date: 05/07/2010

-- Table alters and new data for v3.4

ALTER TABLE `DelegationAuthority` 
MODIFY COLUMN `DelegationAuthority` ENUM ('REDELEGATED','NON_REDELEGATED','REDELEGATED_FEDERATION') NOT null;

ALTER TABLE `Dossier` 
MODIFY COLUMN `DelegationAuthority` ENUM ('REDELEGATED','NON_REDELEGATED','REDELEGATED_FEDERATION') NOT null;

ALTER TABLE `Dossier`
ADD COLUMN `ReviewType` ENUM ('CRC_REVIEW') default null after `DelegationAuthority`,
ADD COLUMN `WorkflowStatus` varchar(1) null after `WorkflowLocations`;

ALTER TABLE `WorkflowLocation` 
MODIFY COLUMN `WorkflowNodeName` ENUM ('Candidate','Department','School','FederationSchool','ViceProvost','FederationViceProvost','ReadyToArchive','Archive') NOT NULL;

ALTER TABLE `DossierSnapshot`
MODIFY COLUMN `SnapshotFileName` varchar(128) not null;

ALTER TABLE `ThesisCommitteeSummary`
CHANGE `Year` `StartYear` varchar(4),
Add COLUMN `EndYear` varchar(10) after `StartYear`,
Add COLUMN `CoChair` boolean default false after `StudentName`,
Add COLUMN `DoctorateDegree` boolean default false after `DoctoralDegree`;

UPDATE `ThesisCommitteeSummary` set EndYear = StartYear+1;

source insertDelegationAuthority.sql;
source insertDelegationAuthorityAttributeCollection.sql;
source insertDossierAttributeLocation.sql;
source insertDossierAttributeType.sql;
source insertWorkflowLocation.sql;
source insertDossierView.sql;
source insertSection.sql;

