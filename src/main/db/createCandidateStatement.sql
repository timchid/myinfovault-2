# Author: Lawrence Fyfe
# Date: 10/17/2006

create table CandidateStatement
(
   ID int primary key auto_increment,
   UserID int,
   Year varchar(4),
   Content mediumtext,
   Sequence int,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
