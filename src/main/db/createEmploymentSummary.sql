# Author: Lawrence Fyfe
# Date: 10/23/2006

create table EmploymentSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   Title varchar(255) COMMENT 'Job title',
   Institution varchar(100) COMMENT 'Name of the employer',
   Location varchar(100) COMMENT 'Location of the employer',
   StartDate varchar(20) COMMENT 'Employment start date',
   EndDate varchar(20) COMMENT 'Employment end date',
   Salary varchar(20) COMMENT 'Annual salary',
   ShowSalary boolean COMMENT 'Should salary be displayed?',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   Remark mediumtext COMMENT 'Additional remarks',
   DisplayRemark boolean NOT NULL default true COMMENT 'Should additional remarks be displayed',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Employment history';
