-- Author: Mary Northup
-- Date: 12/17/2008

-- Change for v2.3  MIV-2248
ALTER TABLE `BiosketchAttributes`
 MODIFY COLUMN Value TEXT;
