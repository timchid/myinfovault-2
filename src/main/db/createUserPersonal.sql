# Author: Lawrence Fyfe
# Date: 5/30/2007

create table UserPersonal
(
   ID int primary key auto_increment,
   UserID int NOT NULL,
   DisplayName varchar(255),
   BirthDate varchar(20),
   Citizen boolean NOT NULL default true,
   DateEntered varchar(20),
   VisaType varchar(20),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   unique index (UserID)
)
engine = InnoDB
default character set utf8;
