-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- Procedure to convert Appointments Into Role Assignments
-- @param vUserID
-- @param vRoleID
DROP PROCEDURE IF EXISTS `PROC_CONVERT_APPOINTMENT_INTO_ROLE_ASSIGNMENT`$$
CREATE PROCEDURE `PROC_CONVERT_APPOINTMENT_INTO_ROLE_ASSIGNMENT`(	IN vUserID INT(11),
																	IN vRoleID INT(11),
																	IN vCvLevel CHAR(2))
BEGIN

	/****************************************************************************/
	/* Declare Variables 														*/
	/****************************************************************************/
	DECLARE appointmentCount,appointmentIndex INT DEFAULT 0;
	DECLARE appointment_id,appointment_user_id,appointment_user_schoolId,appointment_user_departmentId INT(11) DEFAULT 0;
	DECLARE primary_appointment BOOLEAN DEFAULT FALSE;
	DECLARE appointment_scope VARCHAR(255) DEFAULT null;
	DECLARE sqlScript VARCHAR(4000) DEFAULT null;
	DECLARE lSCHOOL_STAFF INT(11) DEFAULT 2;
	DECLARE CV_MIV_ADMIN 		CHAR(2) DEFAULT '1';
	DECLARE CV_SCHOOL_ADMIN 	CHAR(2) DEFAULT '2';
	DECLARE CV_DEPT_ADMIN 	CHAR(2) DEFAULT '3';
	DECLARE CV_DEPT_HELPER	CHAR(2) DEFAULT '3H';
	DECLARE CV_CANDIDATE 		CHAR(2) DEFAULT '4';
	/****************************************************************************/
	/* Declare Cursors 													*/
	/****************************************************************************/
	DECLARE userAppointmentCur CURSOR FOR 
				SELECT `ID`,`UserID`, `SchoolID`, `DepartmentID`, `PrimaryAppointment` 
					FROM `Appointment` 
						WHERE `UserID`=vUserID; 

	CALL log_info('------ Comming Into PROC_CONVERT_APPOINTMENT_INTO_ROLE_ASSIGNMENT ---------------');
	CALL log_info(CONCAT('UserID : ',vUserID));
	CALL log_info(CONCAT('RoleID : ',vRoleID));
	CALL log_info(CONCAT('CvLevel : ',vCvLevel));

	CALL log_info('');
	
	OPEN userAppointmentCur;
	SET appointmentCount = (Select FOUND_ROWS());
	CALL log_info(CONCAT('appointmentCount= ',appointmentCount));
	
	/* school staff only has single appointment */
	IF vRoleID = lSCHOOL_STAFF AND appointmentCount > 1 THEN
		-- CALL log_error(CONCAT('User with UserId=',vUserID,' and RoleId=', vRoleID,' has more then one appointment.[Fixed]'));
		CALL log_error(CONCAT('Removed appointment(s) from User with UserId=',vUserID,' because SCHOOL_STAFF must have one appointment. [Fixed]'));
	END IF;

	IF appointmentCount > 0 THEN
		WHILE appointmentIndex<appointmentCount DO
			FETCH userAppointmentCur INTO appointment_id,appointment_user_id,appointment_user_schoolId,appointment_user_departmentId,primary_appointment;
			CALL log_info(CONCAT('Index : [',appointmentIndex+1,']'));
			
			IF NOT primary_appointment THEN				
				CALL log_info(CONCAT('is primary appointment : true'));

				/* No need to convert appointment into roleassignment because 
				   school staff only has single appointment */
				IF vRoleID <> lSCHOOL_STAFF THEN	
					/* prepare scope string */
					SET appointment_scope = getMivScopeString(vRoleID,appointment_user_schoolId,appointment_user_departmentId);
					CALL log_info(CONCAT('appointment_scope is --> ',ifnull(appointment_scope,'null')));
		
					/* Insert appointment data into RoleAssignment */
					CALL PROC_INSERT_ROLE_ASSIGNMENT(appointment_user_id,vRoleID,appointment_scope,true);
				END IF;
				
				/* Delete record from Appoinment */
				CALL PROC_DELETE_APPOINTMENT(appointment_id,appointment_user_id);

			ELSE
				CALL log_info(CONCAT('is primary appointment : false'));

				/* Delete record from Appoinment */
				CALL PROC_DELETE_APPOINTMENT(appointment_id,appointment_user_id);

			END IF;				
			SET appointmentIndex = appointmentIndex + 1; -- increment appointmentIndex by 1
		END WHILE;
	END IF;
	
	/* closing userAppointmentCur*/
	CLOSE userAppointmentCur;
	CALL log_info('------ Exit From PROC_CONVERT_APPOINTMENT_INTO_ROLE_ASSIGNMENT ---------------');
	
END$$

DELIMITER ;
