-- Author: Rick Hendricks
-- Date: 04/27/2010

-- Change column name for v3.3.1.
ALTER TABLE `Dossier`
ADD COLUMN `LastRoutedDate` datetime;
UPDATE Dossier SET LastRoutedDate = UpdateTimestamp;

