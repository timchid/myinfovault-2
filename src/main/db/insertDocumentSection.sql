-- Author: Lawrence Fyfe
-- Created: 8/29/2006
-- Updated: 4/26/2011
-- Note: Replace 14021 with your internal system ID

select count(*) from DocumentSection into @_ORIG_COUNT;

truncate DocumentSection;

insert into DocumentSection (DocumentID,SectionID,Sequence,InsertUserid)
values
-- Doc 1 -- Candidate's Statement
(01, 01, 010, 14021),
(01, 71, 020, 14021),
-- Doc 2 -- Position Description
(02, 02, 010, 14021),
(02, 99, 020, 20508),
-- Doc 3 -- List of Evaluations
(03, 03, 010, 14021),
-- Doc 4  --  Teaching: DESII Report: PDF Upload
(04, 89, 010, 14021),
-- Doc 4 -- Teaching, Advising, Curricular Development
(04, 04, 020, 14021),
(04, 05, 030, 14021),
(04, 06, 040, 14021),
(04, 07, 050, 14021),
(04, 08, 060, 14021),
(04, 09, 070, 14021),
(04, 10, 080, 14021),
(04, 11, 090, 14021),
(04, 12, 100, 14021),
(04, 13, 110, 14021),
(04, 14, 120, 14021),
-- Doc 5 -- List of Service
(05, 22, 010, 14021),
(05, 17, 040, 14021),
(05, 15, 020, 14021),
(05, 20, 070, 14021),
(05, 19, 060, 14021),
(05, 16, 030, 14021),
(05, 18, 050, 14021),
(05, 21, 080, 14021),
(05, 23, 090, 14021),
-- Doc 6 -- Publications
(06, 24, 010, 14021),	-- journals, published
(06, 25, 020, 14021),	-- journals, in-press
(06, 26, 030, 14021),	-- journals, submitted
(06, 27, 160, 14021),	-- books authored, published
(06, 28, 170, 14021),	-- books authored, in-press
(06, 29, 180, 14021),	-- books authored, submitted
(06, 30, 100, 14021),	-- books edited, published
(06, 31, 110, 14021),	-- books edited, in-press
(06, 32, 120, 14021),	-- books edited, submitted
(06, 33, 040, 14021),	-- book chapters, published
(06, 34, 050, 14021),	-- book chapters, in-press
(06, 35, 060, 14021),	-- book chapters, submitted
(06, 36, 130, 14021),	-- book reviews, published
(06, 37, 140, 14021),	-- book reviews, in-press
(06, 38, 150, 14021),	-- book reviews, submitted
(06, 39, 220, 14021),	-- alternative media, published
(06, 40, 230, 14021),	-- alternative media, in-press
(06, 41, 240, 14021),	-- alternative media, submitted
(06, 42, 070, 14021),	-- letter-to-editor, published
(06, 43, 080, 14021),	-- letter-to-editor, in-press
(06, 44, 090, 14021),	-- letter-to-editor, submitted
(06, 45, 190, 14021),	-- limited distribution, published
(06, 46, 200, 14021),	-- limited distribution, in-press
(06, 47, 210, 14021),	-- limited distribution, submitted
(06, 48, 250, 14021),	-- abstracts, published
(06, 49, 260, 14021),	-- abstracts, in-press
(06, 50, 270, 14021),	-- abstracts, submitted
(06, 51, 280, 14021),	-- presentations

/* Adding section documents for Patents*/
(06, 92, 290, 20001),	-- patent, granted
(06, 98, 295, 20001),   -- patent, filed
(06, 93, 300, 20720),   -- disclosure

(06, 52, 310, 14021),	-- publications, additional information
(12, 77, 010, 14021),	-- publications, all for NIH
-- Doc 7 -- Honors and Awards
(07, 53, 010, 14021),
-- Doc 8 -- Grants and Contracts
(08, 54, 010, 14021),
(08, 73, 020, 14021),
(08, 74, 030, 14021),
(08, 75, 040, 14021),
(08, 76, 050, 14021),
-- Doc 9 -- Ag Experiment Station
(09, 55, 010, 14021),
(09, 72, 020, 14021),
-- Doc 10 -- Extending Knowledge
(10, 56, 010, 14021),
(10, 57, 020, 14021),
(10, 58, 030, 14021),
--     -- Extending Knowledge: PDF Upload
(10, 90, 040, 14021),
-- Doc 11 -- CV
(11, 59, 010, 14021),
(11, 60, 020, 14021),
(11, 61, 030, 14021),
(11, 62, 040, 14021),
(11, 63, 050, 14021),
(11, 64, 060, 14021),
(11, 65, 070, 14021),
(11, 66, 080, 14021),
(11, 67, 090, 14021),
(11, 68, 100, 14021),
(11, 69, 110, 14021),
(11, 70, 120, 14021),
(11, 53, 130, 14021),
(11, 83, 140, 14021),
(11, 80, 150, 14021),
(11, 82, 160, 14021),
(11, 81, 170, 14021),
(11, 89, 180, 14021),
(11, 79, 190, 14021),
(11, 90, 200, 14021),
(11, 84, 210, 14021),
(11, 78, 220, 14021),
(11, 51, 230, 14021),
(11, 52, 240, 14021),
(11, 54, 250, 14021),
(11, 04, 260, 14021),
(11, 12, 270, 14021),
(11, 13, 280, 14021),
(11, 14, 290, 14021),
(11, 15, 300, 14021),
(11, 16, 310, 14021),
(11, 17, 320, 14021),
(11, 18, 330, 14021),
(11, 19, 340, 14021),
(11, 20, 350, 14021),
(11, 21, 360, 14021),
(11, 22, 370, 14021),
(11, 23, 380, 14021),
-- Doc 12 -- NIH
(12, 59, 010, 14021),
(12, 68, 020, 14021),
(12, 86, 030, 14021),
(12, 53, 040, 14021),
(12, 66, 050, 14021),
(12, 88, 060, 14021),
(12, 74, 070, 14021),
(12, 87, 080, 14021),
(12, 78, 090, 14021),
(12, 79, 100, 14021),
(12, 80, 110, 14021),
(12, 81, 120, 14021),
(12, 82, 130, 14021),
(12, 83, 140, 14021),
(12, 84, 150, 14021),
(12, 85, 160, 14021),
(12, 21, 170, 14021),

-- Doc 51 -- Contributions to Publications

(51, 24, 010, 18099),	-- journals, published
(51, 25, 020, 18099),	-- journals, in-press
(51, 26, 030, 18099),	-- journals, submitted
(51, 27, 160, 18099),	-- books authored, published
(51, 28, 170, 18099),	-- books authored, in-press
(51, 29, 180, 18099),	-- books authored, submitted
(51, 30, 100, 18099),	-- books edited, published
(51, 31, 110, 18099),	-- books edited, in-press
(51, 32, 120, 18099),	-- books edited, submitted
(51, 33, 040, 18099),	-- book chapters, published
(51, 34, 050, 18099),	-- book chapters, in-press
(51, 35, 060, 18099),	-- book chapters, submitted
(51, 36, 130, 18099),	-- book reviews, published
(51, 37, 140, 18099),	-- book reviews, in-press
(51, 38, 150, 18099),	-- book reviews, submitted
(51, 39, 220, 18099),	-- alternative media, published
(51, 40, 230, 18099),	-- alternative media, in-press
(51, 41, 240, 18099),	-- alternative media, submitted
(51, 42, 070, 18099),	-- letter-to-editor, published
(51, 43, 080, 18099),	-- letter-to-editor, in-press
(51, 44, 090, 18099),	-- letter-to-editor, submitted
(51, 45, 190, 18099),	-- limited distribution, published
(51, 46, 200, 18099),	-- limited distribution, in-press
(51, 47, 210, 18099),	-- limited distribution, submitted
(51, 48, 250, 18099),	-- abstracts, published
(51, 49, 260, 18099),	-- abstracts, in-press
(51, 50, 270, 18099),	-- abstracts, submitted
(51, 51, 280, 18099),	-- presentations
(51, 92, 290, 20001),	-- patents, granted
(51, 98, 295, 20001),   -- patent, filed

-- Doc 52 -- Creative Activities

(52,94,10,18635), -- Works Completed                          
(52,95,20,18635), -- Works Scheduled                          
(52,96,30,18635), -- Works Submitted                          

(52,301,1420,18635),   
(52,302,1430,18635),   
(52,303,1440,18635),   
(52,304,1450,18635),   
(52,305,1460,18635),   
(52,306,1470,18635),   
(52,307,1480,18635),   
(52,308,1490,18635),   
(52,309,1500,18635),   
(52,310,1510,18635),   
(52,311,1520,18635),   
(52,312,1530,18635),   
(52,313,1540,18635),   
(52,314,1550,18635),   
(52,315,1560,18635),   
(52,316,1570,18635),   
(52,317,1580,18635),   
(52,318,1590,18635),   
(52,319,1600,18635),   
(52,320,1610,18635),   
(52,321,1620,18635),   
(52,322,1630,18635),   
(52,323,1640,18635),   
(52,324,1650,18635),   
(52,325,1660,18635),   
(52,326,1670,18635),   
(52,327,1680,18635),   
(52,328,1690,18635),   
(52,329,1700,18635),   
(52,330,1710,18635),   
(52,331,1720,18635),   
(52,332,1730,18635),   
(52,333,1740,18635),   
(52,334,1750,18635),   
(52,335,1760,18635),   
(52,336,1770,18635),   
(52,337,1780,18635),   
(52,338,1790,18635),   
(52,339,1800,18635),   
(52,340,1810,18635),   
(52,341,1820,18635),   
(52,342,1830,18635),   
(52,343,1840,18635),   
(52,344,1850,18635),   
(52,345,1860,18635),   
(52,346,1870,18635),   
(52,347,1880,18635),   
(52,348,1890,18635),   
(52,349,1900,18635),   
(52,350,1910,18635),   
(52,351,1920,18635),   
(52,352,1930,18635),   
(52,353,1940,18635),   
(52,354,1950,18635),   
(52,355,1960,18635),   
(52,356,1970,18635),   
(52,357,1980,18635),   
(52,358,1990,18635),   
(52,359,2000,18635),   
(52,360,2010,18635),   
(52,361,2020,18635),   
(52,362,2030,18635),   
(52,363,2040,18635),   
(52,364,2050,18635),   
(52,365,2060,18635),   
(52,366,2070,18635),   
(52,367,2080,18635),   
(52,368,2090,18635),   
(52,369,2100,18635),   
(52,501,2380,18635),   
(52,502,2390,18635),   
(52,503,2400,18635),   
(52,504,2410,18635), 
(52,509,2420,18635),   

-- Doc 53 -- Creative Activities Contributions

(53,94,10,18635), -- Works Completed                          
(53,95,20,18635), -- Works Scheduled                          
(53,96,30,18635), -- Works Submitted           

(54,97,10,20508), -- Candidate's Diversity Statement

(70,68,010,20508), -- Education: Education and Training
(70,70,020,20508), -- Education: Licenses and Certifications
(70,69,030,20508), -- Education Additional Record
(70,66,040,20508), -- Employment: Employment History
(70,67,050,20508)  -- Employment Additional Record
;

select count(*) from DocumentSection into @_NEW_COUNT;
select @_ORIG_COUNT as 'Original Count', @_NEW_COUNT as 'New Count';
