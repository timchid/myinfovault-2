# Author: Lawrence Fyfe
# Date: 10/25/2006

truncate AddressType;

insert into AddressType (ID,Description,InsertUserID)
values
(1,'Permanent Home',14021),
(2,'Current Home',14021),
(3,'Office',14021);
