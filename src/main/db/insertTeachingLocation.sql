# Author: Lawrence Fyfe
# Date: 10/06/2006

truncate TeachingLocation;

insert into TeachingLocation (ID,Description,InsertUserID)
values
(1,'University',14021),
(2,'Other',14021);
