-- Author : Pradeep Kumar Haldiya
-- Created: 09/21/2011

DELIMITER $$

-- Function to get DepartmentId From Scope String
DROP FUNCTION IF EXISTS `getDepartmentIdFromScope`$$
CREATE FUNCTION `getDepartmentIdFromScope`(vScope VARCHAR(255)) RETURNS VARCHAR(11) CHARSET utf8
BEGIN
	DECLARE lTemp VARCHAR(255) DEFAULT null;
	DECLARE lSubString VARCHAR(255) DEFAULT null;
	DECLARE lResult VARCHAR(11) DEFAULT null;
	SET lTemp = vScope;
	WHILE length(lTemp) > 0 DO
		SET lSubString = SUBSTRING_INDEX(lTemp, ',', 1);
		SET lTemp = TRIM(SUBSTRING(lTemp,length(lSubString)+2));
		IF TRIM(SUBSTRING_INDEX(lSubString,'=',1)) = 'DepartmentID' THEN
			SET lSubString = TRIM('DepartmentID' FROM lSubString );
			SET lSubString = TRIM('=' FROM lSubString );	
			SET lResult = TRIM(lSubString);
			SET lTemp = '';
		END IF;
	END WHILE;
	RETURN lResult;
END$$

-- Function to get DepartmentName By DepartmentID
DROP FUNCTION IF EXISTS `getDepartmentNameByDepartmentID`$$
CREATE FUNCTION `getDepartmentNameByDepartmentID`(vSchoolID int(11),vDepartmentID int(11)) RETURNS VARCHAR(500) CHARSET utf8
BEGIN
	DECLARE departmentName VARCHAR(500) DEFAULT NULL;
	
	SELECT Description INTO departmentName FROM SystemDepartment WHERE DepartmentID=vDepartmentID AND SchoolID=vSchoolID;

	RETURN departmentName; 
END$$

-- Function to get SchoolId From Scope String
DROP FUNCTION IF EXISTS `getSchoolIdFromScope`$$
CREATE FUNCTION `getSchoolIdFromScope`(vScope VARCHAR(255)) RETURNS VARCHAR(11) CHARSET utf8
BEGIN
	DECLARE lTemp VARCHAR(255) DEFAULT null;
	DECLARE lSubString VARCHAR(255) DEFAULT null;
	DECLARE lResult VARCHAR(11) DEFAULT null;
	SET lTemp = vScope;
	WHILE length(lTemp) > 0 DO
		SET lSubString = SUBSTRING_INDEX(lTemp, ',', 1);	
		SET lTemp = TRIM(SUBSTRING(lTemp,length(lSubString)+2));
		IF TRIM(SUBSTRING_INDEX(lSubString,'=',1)) = 'SchoolID' THEN
			SET lSubString = TRIM('SchoolID' FROM lSubString );
			SET lSubString = TRIM('=' FROM lSubString );	
			SET lResult = TRIM(lSubString);
			SET lTemp = '';
		END IF;		
	END WHILE;
	RETURN lResult;
END$$

-- Function to get SchoolName By SchoolID
DROP FUNCTION IF EXISTS `getSchoolNameBySchoolID`$$
CREATE FUNCTION `getSchoolNameBySchoolID`(vSchoolID int(11)) RETURNS VARCHAR(500) CHARSET utf8
BEGIN
	DECLARE schoolName VARCHAR(500) DEFAULT NULL;
	
	SELECT Description INTO schoolName FROM SystemSchool WHERE SchoolID=vSchoolID;
	
	RETURN schoolName; 
END$$

-- Function to get UserName By UserID
DROP FUNCTION IF EXISTS `getUserSortNameByUserID`$$
CREATE FUNCTION `getUserSortNameByUserID`(vUserID int(11) ) RETURNS varchar(500) CHARSET utf8
BEGIN
	DECLARE userName VARCHAR(500) DEFAULT NULL;
	DECLARE vSurname,vGivenName,vMiddleName VARCHAR(50) DEFAULT NULL;
	
	SELECT trim(ifnull(Surname,'')) AS Surname,trim(ifnull(GivenName,'')) AS GivenName, trim(ifnull(MiddleName,'')) AS MiddleName  
		INTO vSurname, vGivenName,vMiddleName  
			FROM UserAccount WHERE UserID = vUserID;

	IF vSurname IS NOT NULL and vSurname <> '' THEN
		SET userName = concat(vSurname,', ');
	END IF;

	IF vGivenName IS NOT NULL and vGivenName <> '' THEN
		SET userName = concat(userName,vGivenName);
	END IF;

	IF vMiddleName IS NOT NULL and vMiddleName <> '' THEN
		SET userName = concat(userName,' ',vMiddleName);
	END IF;

	RETURN userName; 
END$$

-- Function to get Home Department By UserID
DROP FUNCTION IF EXISTS `getHomeDepartmentIDByUserID`$$
CREATE FUNCTION `getHomeDepartmentIDByUserID`(vUserId INT(11)) RETURNS int(11)
BEGIN
	DECLARE lHomeDepartmentID INT(11) DEFAULT -1;

	SELECT ifnull((SELECT getDepartmentIdFromScope(`Scope`) FROM `RoleAssignment`
			WHERE `UserID`=vUserId AND `RoleID`=7 LIMIT 1),-1) INTO lHomeDepartmentID; 

	RETURN lHomeDepartmentID;

END$$

-- Function to get Home SchoolId By UserID
DROP FUNCTION IF EXISTS `getHomeSchoolIDByUserID`$$
CREATE FUNCTION `getHomeSchoolIDByUserID`(vUserId INT(11)) RETURNS int(11)
BEGIN
	DECLARE lHomeSchoolID INT(11) DEFAULT -1;

	SELECT ifnull((SELECT getSchoolIdFromScope(`Scope`) FROM `RoleAssignment`
			WHERE `UserID`=vUserId AND `RoleID`=7 LIMIT 1),-1) INTO lHomeSchoolID; 

	RETURN lHomeSchoolID;

END$$

DELIMITER ;
