Geographical Country, State/Province, and City data was obtained from ( http://www.maxmind.com/app/country )

To create the geographical lookup tables from this data, follow these instructions.

---------------------------------------------------------------------------------------------------------
How to get input data?
1. Download most updated countries list:
	http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip 

2. Download most updated states list ( http://www.maxmind.com/app/iso3166_2 )
	2.1 Outside of the US, FIPS 10-4 code:
		http://www.maxmind.com/fips10-4.csv
	2.2 US ISO 3166-2 Subcountry codes, don't replace CA codes 
		http://www.maxmind.com/app/iso3166_2
		
Note: Copy Step 2.2 codes and replace into fips10-4.csv file.
	
3. Download most updated cities data.
	www.maxmind.com/download/worldcities/worldcitiespop.txt.gz		  				  		
---------------------------------------------------------------------------------------------------------
Installation (Setup workspace)
1. Check out the latest project source code 
	svn checkout http://worlddatapro.googlecode.com/svn/trunk/ worlddatapro-read-only

This will give you a local copy of all files required to run your version of WorldDataPro project.

2. Download input data and placed at data folder.It already have create table scripts and a main script 
to call all table creation and insert data scripts.

3. Go to the source folder at src/main and run GenerateWorldData.java class. 

4. Now insert script files have been generated and placed at /data/sql folder.

5. Last and final task to execute scripts. Just need to execute createGeoTables.sql script.
---------------------------------------------------------------------------------------------------------
