-- Author: Pradeep Haldiya
-- Created: 12/21/2011
-- Note: To Resove the issue for User can't login to MIV

/* Issue Resolved for following users
 * 1. 7653 = Scott Christensen
 * 2. 10392 = Richard Marder, Richard A Marder
 */


-- Step1:  Adding a new entry into UserLogin Table
INSERT INTO `UserLogin`(`UserID`,`Login`,`InsertUser`) VALUES(10392,'ramarder',20720);

-- Step2: Update the UserAccount Login column to match the kerberose login name.
UPDATE `UserAccount` SET `Login` = 'ramarder', `UpdateUserID`=20720, `UpdateTimestamp`=now() WHERE `UserID`=10392 AND `Active`=true;