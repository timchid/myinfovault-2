-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- PROCEDURE to Insert log Details
DROP PROCEDURE IF EXISTS `insert_log`$$
CREATE PROCEDURE `insert_log`(in vType varchar(10),in vMessage varchar(500) )
BEGIN

	IF vType IS NULL THEN
		SET vType = 'INFO';
	END IF;
	
	INSERT INTO `logger`(`type`,`msg`,`logtime`) VALUES (vType,vMessage,now());
END$$

-- PROCEDURE to Delete logs
DROP PROCEDURE IF EXISTS `delete_log`$$
CREATE PROCEDURE `delete_log`()
BEGIN
	DELETE FROM `logger` WHERE 1=1;
END$$

-- PROCEDURE to log errors
DROP PROCEDURE IF EXISTS `log_error`$$
CREATE PROCEDURE `log_error`(in vMessage varchar(500) )
BEGIN
		CALL insert_log('ERROR',vMessage);
END$$

-- PROCEDURE to log info
DROP PROCEDURE IF EXISTS `log_info`$$
CREATE PROCEDURE `log_info`(in vMessage varchar(500) )
BEGIN
		CALL insert_log('INFO',vMessage);
END$$

-- PROCEDURE to log sql scripts
DROP PROCEDURE IF EXISTS `log_sql`$$
CREATE PROCEDURE `log_sql`(in vMessage varchar(500) )
BEGIN
		CALL insert_log('SQL',vMessage);
END$$

-- PROCEDURE to log warnings
DROP PROCEDURE IF EXISTS `log_warn`$$
CREATE PROCEDURE `log_warn`(in vMessage varchar(500) )
BEGIN
		CALL insert_log('WARN',vMessage);
END$$

DELIMITER ;
