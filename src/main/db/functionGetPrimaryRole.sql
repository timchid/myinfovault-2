-- Author : Pradeep Kumar Haldiya
-- Created: 09/02/2011

DELIMITER $$

-- Function to get Primary Role by the UserID
DROP FUNCTION IF EXISTS `getPrimaryRole`$$
CREATE FUNCTION `getPrimaryRole`(vUserId INT(11)) RETURNS int(11)
BEGIN
	DECLARE lPrimaryRoleID INT(11) DEFAULT -1;

	SELECT ifnull((SELECT `RoleID` FROM `RoleAssignment`
			WHERE `UserID`=vUserId AND `PrimaryRole`=true LIMIT 1),-1) INTO lPrimaryRoleID; 

	RETURN lPrimaryRoleID;

END$$

DELIMITER ;
