-- Author: Stephen Paulsen
-- Date: 2015-03-23
-- Updated: yyyy-mm-dd (initials)
-- 
-- Populate the 'System' records for the Packet table.

use `myinfovault`;

-- Set lenient modes for table mods.
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


-- Add a "master" account that can own some things.
-- This has to be inserted as a positive userid (#1 is unused) then changed to UserID zero,
-- since inserting zero causes the next auto-increment value to be used.
INSERT INTO UserAccount (UserID, Login, PersonUUID, SchoolID, DepartmentID, Surname, GivenName, MiddleName, PreferredName, Email, Active, InsertTimestamp, InsertUserID)
     VALUES (1, 'no-login', 0, 38, 1000, 'Account', 'Master', '', 'Master Account', NULL, false, CURRENT_TIMESTAMP, 18099);
UPDATE UserAccount SET UserID=0 WHERE UserID=1;


-- Add a "master" packet that will be used for everybody's "master" data.
-- This has to be inserted as a positive number (#1 is unused) then changed to ID zero,
-- since inserting zero causes the next auto-increment value to be used.
INSERT INTO `myinfovault`.`Packet` (`ID`, `UserId`, `Name`, `InsertTimestamp`, `InsertUserID`, `UpdateTimestamp`, `UpdateUserID`)
     VALUES (1, 0, 'Master Packet', CURRENT_TIMESTAMP, 18099, NULL, NULL);
UPDATE Packet SET ID=0 WHERE ID=1;



-- Restore normal modes.
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

