-- Author: Pradeep Haldiya
-- Created: 2011-12-15
-- Updated: 2012-01-03

-- Create the Patent Jurisdiction table
-- Dependencies: none

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS PatentJurisdiction;

CREATE TABLE PatentJurisdiction (
   ID INT primary key auto_increment,
   Code CHAR(3) DEFAULT '' NOT NULL,
   Jurisdiction VARCHAR(100) NOT NULL,
   Abbreviation VARCHAR(100) NOT NULL,
   ApplicationText VARCHAR(500) NOT NULL,
   PatentText VARCHAR(500) NOT NULL,
   Tooltip VARCHAR(500),
   Active BOOLEAN DEFAULT false NOT NULL,
   InsertTimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
   InsertUserID INT DEFAULT 0 NOT NULL,
   UpdateTimestamp TIMESTAMP NULL,
   UpdateUserID INT
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
