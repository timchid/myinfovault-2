-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- Procedure to convert Appointments Into Role Assignments(Main) 
DROP PROCEDURE IF EXISTS `PROC_CONVERT_MAIN`$$
CREATE PROCEDURE `PROC_CONVERT_MAIN`()
BEGIN

	/****************************************************************************/
	/* Declare Variables 														*/
	/****************************************************************************/
	DECLARE userCount,appointmentCount,raCount,userIndex,appointmentIndex,raIndex INT DEFAULT 0;
	DECLARE user_id,user_schoolId,user_departmentId INT(11) DEFAULT 0;
	DECLARE user_active	BOOLEAN DEFAULT FALSE;
	DECLARE user_CvLevel	CHAR(25) DEFAULT NULL;
	DECLARE appointment_id,appointment_user_id,appointment_user_schoolId,appointment_user_departmentId INT(11) DEFAULT 0;
	DECLARE primary_appointment BOOLEAN DEFAULT FALSE;
	DECLARE ra_id,ra_user_id,ra_roleId INT(11) DEFAULT 0;
	DECLARE needConversion BOOLEAN DEFAULT FALSE;
	DECLARE ra_primary_role BOOLEAN DEFAULT FALSE;
	DECLARE assignment_scope, appointment_scope, ra_scope VARCHAR(255) DEFAULT null;
	DECLARE sqlScript VARCHAR(4000) DEFAULT null; 
	DECLARE primaryRoleId INT(11);  	
	DECLARE datePrint CHAR(25); 
	DECLARE lSCHOOL_STAFF INT(11) DEFAULT 2; 
	DECLARE lDEAN 		INT(11) DEFAULT 3;
	DECLARE lCANDIDATE	INT(11) DEFAULT 6;

	/****************************************************************************/
	/* Declare Cursors 															*/
	/****************************************************************************/
	DECLARE userCur CURSOR FOR 
					SELECT `UserID`, `SchoolID`, `DepartmentID`, `Active`,`CvLevel` 
						FROM `UserAccount`;	
	DECLARE raCur CURSOR FOR 
					SELECT `ID`,`UserID`, `RoleID`, `Scope`, `PrimaryRole` 
						FROM `RoleAssignment` 
							WHERE `UserID`=user_id; 	


	CALL delete_log();
	CALL log_info(CONCAT('---------------- START AT ', CAST(now() as CHAR(25)),'------------------'));

	/* Step1 : droping backup tables */
	CALL log_info('	Step1 : droping backup table');
	drop table if exists `roleassignment_temp`;
	drop table if exists `appointment_temp`;
	
	/* Step2 : creating backup table with full data */
	CALL log_info('	Step2 : creating backup table with full data');
	create table `roleassignment_temp` select * from `RoleAssignment`;
	create table `appointment_temp` select * from `Appointment`;
	ALTER TABLE `appointment_temp` CHANGE COLUMN `ID` `ID` INT(11) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (`ID`) ;
	ALTER TABLE `appointment_temp` CHANGE COLUMN `InsertTimestamp` `InsertTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
	ALTER TABLE `roleassignment_temp` CHANGE COLUMN `ID` `ID` INT(11) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (`ID`) ;		
	ALTER TABLE `roleassignment_temp` CHANGE COLUMN `InsertTimestamp` `InsertTimestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;


	/* Need to update all SCHOOL_STAFF who have the role of DEAN too into CANDIATE Role */
	/* Since v4.0 There is no school staff with dean role any more*/
	UPDATE `RoleAssignment` SET `RoleID` = lCANDIDATE 
		WHERE `RoleID`=lSCHOOL_STAFF 
			AND `UserID` IN (SELECT `R1`.`UserID` FROM (SELECT `UserID` FROM `roles` WHERE `RoleID`=lDEAN) AS `R1` 
														INNER JOIN (SELECT `UserID` FROM `roles` WHERE `RoleID`=lSCHOOL_STAFF) AS `R2`USING (`UserID`));
	
	CALL log_info('		Step2.1 : Update all SCHOOL_STAFF who have the role of DEAN too into CANDIATE Role');

	/* Step3 : process userdata */
	CALL log_info('	Step3 : process userdata');
	OPEN userCur;
	SET userCount = (Select FOUND_ROWS());
	CALL log_info(CONCAT('	Total Users = ',userCount));

	WHILE userIndex<userCount DO
		/* ReSet Variables */
		SET primaryRoleId = -1;
		SET appointment_scope = NULL;
		SET raIndex = 0;
		SET needConversion = false;
		SET appointmentIndex = 0;

		FETCH userCur INTO user_id,user_schoolId,user_departmentId,user_active,user_CvLevel;
		CALL log_info(CONCAT('		$$$$ Processing [',userIndex+1,'] $$$$ UserID:',user_id,' ...'));

		-- Process logger on console
		-- SELECT CONCAT('Index: [',userIndex+1,'] Total Users: [',userCount,'] Current UserID:',user_id,' ...') `Current User`;

		/* process roleassignment table with useraccount scope data*/
		OPEN raCur;
		SET raCount = (Select FOUND_ROWS());
		CALL log_info(CONCAT('			','Role Assignment Count= ',raCount));
		SET raIndex = 0;
		IF raCount > 0 THEN
			WHILE raIndex<raCount DO
				FETCH raCur INTO ra_id,ra_user_id,ra_roleId,ra_scope,ra_primary_role;

				/* ReSet Variables */
				SET assignment_scope = NULL;	
				SET needConversion = false;
				
				/* call functions to determine or calculate scope string and 
				   Conversion from Appointment to RoleAssignment is needed or Not */
				SET assignment_scope = getMivScopeString(ra_roleId,user_schoolId,user_departmentId);

				CALL log_info(CONCAT('				',user_id,'''s assignment_scope is -->',ifnull(assignment_scope,'null'),' need to insert'));				
				
				/* update each record for user in RoleAssignment table from UserAccount tables schoolId and departmentId*/
				SET @vID = ra_id;
				SET @vUserID = user_id;
				SET @vScopeStr = assignment_scope;		
				PREPARE stmt FROM 'UPDATE `RoleAssignment` SET Scope=? WHERE ID=? AND UserID=?';		
				EXECUTE stmt USING @vScopeStr,@vID,@vUserID;

				-- just for log
				SET sqlScript = CONCAT('UPDATE `RoleAssignment` SET Scope=''',ifnull(assignment_scope,'null'),''' WHERE ID=',ra_id,' AND UserID=',user_id);
				CALL log_sql(CONCAT('					',sqlScript));

				/* check for the primary role */
				IF ra_primary_role = true THEN
					SET primaryRoleId = ra_roleId;	
					SET needConversion = isConversionNeeded(primaryRoleId);
					CALL log_info(CONCAT('				needConversion -->',needConversion));	
					IF needConversion THEN
						CALL PROC_CONVERT_APPOINTMENT_INTO_ROLE_ASSIGNMENT(user_id,primaryRoleId,user_CvLevel);
					END IF;
				END IF;
		
				SET raIndex = raIndex + 1; -- increment appointmentIndex by 1
				CALL log_info('');
			END WHILE;
		END IF;
		CLOSE raCur;
		
		/* Validate User Roles*/
		CALL log_info(CONCAT('			Validate User Roles'));
		CALL PROC_VALIDATE_ROLES(user_id);
		
		SET userIndex = userIndex + 1; -- increment userIndex by 1
		
	END WHILE;

	CLOSE userCur;
	CALL log_info(CONCAT('----------------END AT ', CAST(now() as CHAR(25)),'------------------'));
	
END$$

DELIMITER ;
