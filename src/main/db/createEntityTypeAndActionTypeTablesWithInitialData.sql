-- Author : Pradeep Kumar Haldiya
-- Created: 09/09/2011

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DELIMITER $$

-- Create ActionType Table
DROP TABLE IF EXISTS `ActionType`$$
CREATE TABLE `ActionType` (
  `ActionID`			int(11)		NOT NULL,
  `ActionName` 			varchar(100) NOT NULL,
  `Description` 		varchar(500),
  `Active`           	boolean,
  `InsertTimestamp` 	timestamp default current_timestamp,
  `InsertUserID` 		int,
  `UpdateTimestamp`		datetime,
  `UpdateUserID` 		int,	  
  PRIMARY KEY (`ActionID`)
)
engine = InnoDB
default character set utf8$$

-- Create EntityType Table
DROP TABLE IF EXISTS `EntityType`$$
CREATE TABLE `EntityType` (
  `EntityID`			int(11)		NOT NULL,
  `EntityName` 			varchar(100) NOT NULL,
  `Description` 		varchar(500),
  `Active`           	boolean,
  `InsertTimestamp` 	timestamp default current_timestamp,
  `InsertUserID` 		int,
  `UpdateTimestamp`		datetime,
  `UpdateUserID` 		int,	  
  PRIMARY KEY (`EntityID`)
)
engine = InnoDB
default character set utf8$$

-- Configure ActionType with some initial data
INSERT INTO `ActionType` (`ActionID`, `ActionName`, `Description`, `Active`, `InsertUserID`)
VALUES
(100,'AUDIT','Audit',true,20720),
(101,'CREATE','Create, Insert',true,20720),
(102,'EDIT','Edit, Update',true,20720),
(103,'DELETE','Delete',true,20720),
(104,'ROUTINES',null,true,20720),
(105,'SIGNATURE_REQUEST',null,true,20720)
$$

-- Configure EntityType with some initial data
INSERT INTO `EntityType` (`EntityID`, `EntityName`, `Description`, `Active`, `InsertUserID`)
VALUES
(101,'USER','User',true,20720),
(102,'DOSSIER','Dossier',true,20720)
$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
