# Author: Lawrence Fyfe
# Date: 10/04/2006

create table TeachingSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   TypeID int COMMENT 'ID corresponding to the type of teaching', -- TODO: ref TeachingType
   CourseNumber varchar(20) COMMENT 'Course number',
   Title mediumtext COMMENT 'Course title',
   Units varchar(4) COMMENT 'Number of units for the course',
   Year varchar(4) COMMENT 'Course year',
   TermTypeID int COMMENT 'ID corresponding to the course term', -- TODO: ref TermType
   MonthID int COMMENT 'ID corrsponding to the course month', -- TODO: ref MonthName?
   Description mediumtext COMMENT 'Title/description for lecture/seminar/lab/other',
   LocationDescription mediumtext COMMENT 'Tourse location/description for lecture/seminar/lab/other',
   UndergraduateCount int COMMENT 'Undergraduate enrollment count',
   GraduateCount int COMMENT 'Graduate enrollment count',
   Hours varchar(6) COMMENT 'Total number of hours for lecture/seminar/lab/other',
   Duration varchar(30) COMMENT 'Duration of course for university extension',
   Effort varchar(4) COMMENT 'Course percent effort',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Teaching: Courses, lectures, seminars, labs, university extensions, or other';
