# Author: Lawrence Fyfe
# Date: 01/22/2007

create table MediaType
(
   ID int primary key auto_increment,
   Description varchar(40),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
