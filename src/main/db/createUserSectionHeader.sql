# Author: Lawrence Fyfe
# Date: 02/26/2006

create table UserSectionHeader
(
   ID int primary key auto_increment,
   UserID int NOT NULL,
   SectionID int NOT NULL,
   Header varchar(100),
   Sequence int,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
