--  Author: Pradeep K Haldiya
-- Created: 2012-08-17

-- Table and Data updates for v4.6.1

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- set of MIV roles have been updated
source insertMivRole.sql;

-- Create temp UserRecordAnnotation Table
CREATE TABLE UserRecordAnnotationTemp AS SELECT * FROM UserRecordAnnotation;

-- Adding new column SectionBaseTable to resolve annotation issue.
ALTER TABLE `Section` ADD COLUMN `SectionBaseTable` VARCHAR(255) NULL DEFAULT NULL AFTER `FromTable`;
source insertSection.sql;

-- Add SectionBaseTable to indentify record's section type
ALTER TABLE `UserRecordAnnotation` ADD COLUMN `SectionBaseTable` VARCHAR(255) NULL DEFAULT NULL AFTER `RecordID`;

-- Filter records
UPDATE UserRecordAnnotation A SET A.SectionID = IFNULL(A.SectionID, CASE WHEN A.OldTable = 'CvLec' THEN 51 WHEN A.OldTable = 'DontCare' THEN 24 ELSE -1 END);
UPDATE UserRecordAnnotation A LEFT JOIN Section S ON A.SectionID = S.ID SET A.SectionBaseTable = S.SectionBaseTable;
DELETE FROM UserRecordAnnotation WHERE SectionID NOT IN (SELECT ID FROM Section);

-- Delete garbage records from UserRecordAnnotation table.
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='PublicationSummary' AND RecordID NOT IN (SELECT ID FROM PublicationSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='PresentationSummary' AND RecordID NOT IN (SELECT ID FROM PresentationSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='PatentSummary' AND RecordID NOT IN (SELECT ID FROM PatentSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='CommitteeSummary' AND RecordID NOT IN (SELECT ID FROM CommitteeSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='EditorialAdvisoryBoardSummary' AND RecordID NOT IN (SELECT ID FROM EditorialAdvisoryBoardSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='CourseEvaluationSummary' AND RecordID NOT IN (SELECT ID FROM CourseEvaluationSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='AdministrativeActivitySummary' AND RecordID NOT IN (SELECT ID FROM AdministrativeActivitySummary);

DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='ExtendingKnowledgeMedia' AND RecordID NOT IN (SELECT ID FROM ExtendingKnowledgeMedia);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='ExtendingKnowledgeGathering' AND RecordID NOT IN (SELECT ID FROM ExtendingKnowledgeGathering);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='ExtendingKnowledgeOther' AND RecordID NOT IN (SELECT ID FROM ExtendingKnowledgeOther);

DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='Works' AND RecordID NOT IN (SELECT ID FROM Works);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='Events' AND RecordID NOT IN (SELECT ID FROM Events);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='PublicationEvents' AND RecordID NOT IN (SELECT ID FROM PublicationEvents);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='Reviews' AND RecordID NOT IN (SELECT ID FROM Reviews);

DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='TeachingSummary' AND RecordID NOT IN (SELECT ID FROM TeachingSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='ThesisCommitteeSummary' AND RecordID NOT IN (SELECT ID FROM ThesisCommitteeSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='AdvisingSummary' AND RecordID NOT IN (SELECT ID FROM AdvisingSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='CurriculumSummary' AND RecordID NOT IN (SELECT ID FROM CurriculumSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='ContactHourSummary' AND RecordID NOT IN (SELECT ID FROM ContactHourSummary);
DELETE FROM UserRecordAnnotation WHERE SectionBaseTable='TraineeSummary' AND RecordID NOT IN (SELECT ID FROM TraineeSummary);

-- Drop OldID, OldTable and SectionID Columns
ALTER TABLE `UserRecordAnnotation` DROP COLUMN `OldID` , DROP COLUMN `OldTable` , DROP COLUMN `SectionID` ;

-- Create Indexes 
CREATE INDEX `INDX_URA_RECID_BASETABLE` ON `UserRecordAnnotation`(RecordID,SectionBaseTable(255));
CREATE INDEX `INDX_URA_BASETABLE` ON `UserRecordAnnotation`(SectionBaseTable(255));

ALTER TABLE `CourseEvaluationSummary` ADD COLUMN `Enrollment` INT NULL COMMENT 'Number of students enrolled' AFTER `Description`;


-- Below update are for the new Senate locations and routing scheme 

-- Add new column to the Dossier table to track previous workflow locations
alter table Dossier add column PreviousWorkflowLocations varchar(512) after WorkflowLocations;

-- Update the PreviousWorkflowLocations column for all dossiers in the Dossier table
source updateDossierLocations_4.6.1.sql;

-- Add the new locations
source createWorkflowLocation.sql;
source insertWorkflowLocation.sql; 

-- Update the attributes for the new Senate, Federation and SenateFederation locations
source insertDossierAttributeType.sql;
source insertDelegationAuthorityAttributeCollection.sql;
source insertDossierAttributeLocation.sql;

-- Update the DossierView for the SENATE_STAFF role.
source insertDossierView.sql;

-- Create the new RoutingPathDefinitions table and populate
source createDossierRoutingPathDefinitions.sql;
source insertDossierRoutingPathDefinitions.sql;

-- Clean up old Candidate Snapshots - These have never been used
delete from DossierSnapshot where MivRoleId = 'CANDIDATE_SNAPSHOT' and SnapshotLocation != 'Archive';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

