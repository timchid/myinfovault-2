-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-02-03

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM EventType WHERE ID <= 500 INTO @_ORIG_COUNT;

-- truncate EventType;
DELETE FROM EventType WHERE ID <= 500;

INSERT into EventType (ID, Description, ShortDescription, InsertTimestamp, InsertUserID) values 
 (1, 'Book', 'Book', CURRENT_TIMESTAMP, 20720),
 (2, 'Broadcast - TV, Radio, Film, Internet', 'Broadcast', CURRENT_TIMESTAMP, 20720),
 (3, 'Catalogs', 'Catalogs', CURRENT_TIMESTAMP, 20720),
 (4, 'CD/DVD/Video', 'CD/DVD/Video', CURRENT_TIMESTAMP, 20720),
 (5, 'Citation', 'Citation', CURRENT_TIMESTAMP, 20720),
 (6, 'Commission', 'Commission', CURRENT_TIMESTAMP, 20720),
 (7, 'Concert', 'Concert', CURRENT_TIMESTAMP, 20720),
 (8, 'Exhibitions - Group', 'Exhibitions - Group', CURRENT_TIMESTAMP, 20720),
 (9, 'Exhibitions - Solo', 'Exhibitions - Solo', CURRENT_TIMESTAMP, 20720),
(10, 'Structure/Landscape', 'Structure/Landscape', CURRENT_TIMESTAMP, 20720),
(11, 'Fashion Show', 'Fashion Show', CURRENT_TIMESTAMP, 20720),
(12, 'Interview/Commentary', 'Interview', CURRENT_TIMESTAMP, 20720),
(13, 'Performance', 'Performance', CURRENT_TIMESTAMP, 20720),
(14, 'Private Collection', 'Private Collection', CURRENT_TIMESTAMP, 20720),
(15, 'Product', 'Product', CURRENT_TIMESTAMP, 20720),
(16, 'Program Notes', 'Program Notes', CURRENT_TIMESTAMP, 20720),
(17, 'Public Collection', 'Public Collection', CURRENT_TIMESTAMP, 20720),
(18, 'Reading', 'Reading', CURRENT_TIMESTAMP, 20720),
(19, 'Recordings', 'Recordings', CURRENT_TIMESTAMP, 20720),
(20, 'Reproductions', 'Reproductions', CURRENT_TIMESTAMP, 20720),
(21, 'Screening', 'Screening', CURRENT_TIMESTAMP, 20720),
(22, 'Theatre Production', 'Theatre Production', CURRENT_TIMESTAMP, 20720),
(23, 'Curated Exhibition', 'Curated Exhibition', CURRENT_TIMESTAMP, 20720);

SHOW WARNINGS;
commit;

SELECT count(*) FROM EventType WHERE ID <= 500 INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
