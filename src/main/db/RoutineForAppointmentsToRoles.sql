-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011
-- Description: Conversion database routine for appointments to roles. (MIV-3946)  

-- Create Logger Table
source createLoggerTable.sql;

-- Procedures for logger
source procedureLogger.sql;

-- Function to prepare MIVScope String
source functionGetMivScopeString.sql;

-- Function to check for Conversion Needed or Not
source functionIsConversionNeeded.sql;

-- Function to check for User have give role or not
source functionIsUserHaveRole.sql;

-- Function to get Primary Role by the UserID
source functionGetPrimaryRole.sql

-- Procedure to delete Appointments by Id and UserId
source procedureDeleteAppointment.sql;

-- Procedure to Insert Role Assignments
source procedureInsertRoleAssignment.sql;

-- Procedure to Insert Role Assignments
source procedureDeleteRoleAssignment.sql;

-- Procedure to Validate User Role Assignments
source procedureValidateRoles.sql;

-- Procedure to convert Appointments Into Role Assignments
source procedureConvertAppointmentIntoRoleAssignment.sql;

-- Procedure to convert Appointments Into Role Assignments(Main) 
source procedureConvertMain.sql;

-- Execute the Routine
CALL PROC_CONVERT_MAIN();

-- Total Execution time
select min(logtime) `StartTime`, max(logtime) `EndTime`,timediff(max(`logtime`),min(`logtime`)) `Total Execution Time` from `logger`;

-- List errors
select `msg` as `List of Errors` from `logger` where `type` = 'ERROR';
