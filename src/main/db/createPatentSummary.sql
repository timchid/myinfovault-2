-- Author: Pradeep Haldiya
-- Created: 2011-12-15
-- Updated: 2012-01-03

-- Create the Patent Summary Table
-- Dependencies: The PatentStatus and PatentJurisdiction tables must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS PatentSummary;

CREATE TABLE PatentSummary (
   ID INT primary key auto_increment COMMENT 'Record unique identifier',
   UserID INT NOT NULL COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   TypeID INT NOT NULL COMMENT 'ID corresponding to the patent type (e.g. 1=Patent, 2=Disclosure)',
   -- Disclosure columns
   DisclosureTitle TEXT COMMENT 'Title of the disclosure',
   DisclosureAuthor TEXT COMMENT 'Authors/investors of the disclosure',
   DisclosureID VARCHAR(20) COMMENT 'UCD ROI dislosure ID number',
   DisclosureDate DATE COMMENT 'Date of the disclosure', -- VARCHAR(10), sdp changed this to a DATE
   -- Patent columns
   PatentStatusID INT NOT NULL DEFAULT 1 COMMENT 'ID corresponding to the patent status (e.g. 1=Filed, 2=Granted)',
   PatentTitle TEXT COMMENT 'Title of the patent',
   PatentAuthor TEXT COMMENT 'Authors/investors of the disclosure',
   Jurisdiction INT COMMENT 'Code corresponding to the country or patent office where the patent was filed',
   ApplicationID VARCHAR(20) COMMENT 'Patent application identifier',
   ApplicationDate DATE COMMENT 'Patent application date', -- VARCHAR(10), sdp changed this to a DATE
   PatentID VARCHAR(20) COMMENT 'Identifier assigned to the patent by the patent office',
   PatentDate DATE COMMENT 'Patent effective date', -- VARCHAR(10), sdp changed this to a DATE
   Link TEXT COMMENT 'URL to the online resource associated with the patent',
   Contribution TEXT COMMENT 'Description of the research/writing contribution made by each author',
   Significance TEXT COMMENT 'Description of how the research related to the patent is significant',
   Licensing TEXT COMMENT 'Description of licensing/development for the patent',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display BOOLEAN NOT NULL DEFAULT true COMMENT 'Should the record be displayed?',
   DisplayContribution BOOLEAN NOT NULL DEFAULT true COMMENT 'Should the contribution text be displayed?',
   InsertTimestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The date-time the record was inserted',
   InsertUserID INT NOT NULL COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp TIMESTAMP NULL COMMENT 'The date-time the record was last updated',
   UpdateUserID INT COMMENT 'ID corresponding to the real user that last updated the record',
   INDEX (`UserID` ASC),
   INDEX `fk_patent_status` (`PatentStatusID` ASC),
   INDEX `fk_jurisdiction` (`Jurisdiction` ASC),
   CONSTRAINT `fk_patent_status`
     FOREIGN KEY (`PatentStatusID`)
     REFERENCES `myinfovault`.`PatentStatus` (`ID`)
     ON DELETE NO ACTION
     ON UPDATE CASCADE,
   CONSTRAINT `fk_jurisdiction`
     FOREIGN KEY (`Jurisdiction`)
     REFERENCES `myinfovault`.`PatentJurisdiction` (`ID`)
     ON DELETE NO ACTION
     ON UPDATE CASCADE
)
engine = InnoDB
default character set utf8
COMMENT 'Publications: Filed/granted patents';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
