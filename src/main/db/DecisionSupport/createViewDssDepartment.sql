-- Author: Pradeep Haldiya
-- Created: 2012-08-14
-- To the list of miv departments with All option 

-- Depends On: nothing

DROP VIEW IF EXISTS `DssDepartment`;
CREATE VIEW `DssDepartment` AS 
/* SELECT 
	-1 AS ID,
    	null AS SchoolID,
	'All Departments' AS Text,
	'All Departments' AS Description,
	null AS Abbreviation,
	true AS Active
FROM DUAL
UNION */
SELECT 
	SD.DepartmentID AS ID,
    	SD.SchoolID,
	CONCAT(SD.DepartmentID, '-' ,IF(char_length(SD.Description)>0, SD.Description, SS.Description)) AS Text,
	IF(char_length(SD.Description)>0, SD.Description, SS.Description) AS Description,
	IF(char_length(SD.Abbreviation)>0, SD.Abbreviation, null) AS Abbreviation,
	SD.Active 
FROM `myinfovault`.`SystemDepartment` SD 
Left Join `myinfovault`.`SystemSchool` SS ON SD.SchoolID = SS.SchoolID AND SS.Demo=false;
