-- Author: Pradeep Haldiya
-- Created: 2012-08-13
-- Assign permissions to sisadmin and mivadmin user.

-- just assign select permission for now.
grant select on `sisdb`.* to 'sisdev';

-- Assign all rights to mivadmin on sisdb database.
grant all on `sisdb`.* to 'mivadmin';

FLUSH PRIVILEGES;
