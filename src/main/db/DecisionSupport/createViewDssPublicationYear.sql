-- Author: Stephen Paulsen
-- Created: 2013-02-04
-- View to get all publication Years used, from 1900 to CurrentYear+2
-- This is much faster than the equivalent query on the DssPublication view.


-- Index the Year column to really speed this up...
-- CREATE INDEX `IDX_PUB_YEAR` ON `PublicationSummary`(Year);
-- ... but don't do it in this file. Add it to
--     createPublicationSummary.sql and put this
--     CREATE INDEX into an 'AlterTables_vNN.sql' file.

/*
-- Version to eliminate "bad" years
DROP VIEW IF EXISTS `DssPublicationYear`;
CREATE ALGORITHM=MERGE VIEW `DssPublicationYear` AS
  SELECT DISTINCT Year AS year_code, Year AS year_desc, Year AS year_sort
    FROM myinfovault.PublicationSummary
    WHERE  Year <= ( date_format(curdate(),'%Y')+2 ) AND Year >= 1900
  UNION ALL
    SELECT null AS year_code, 'ALL' AS year_desc, 'ZZZZ' AS year_sort
  ORDER BY year_sort DESC;
*/

-- Version to keep all years
DROP VIEW IF EXISTS `DssPublicationYear`;
CREATE ALGORITHM=MERGE VIEW `DssPublicationYear` AS
  SELECT DISTINCT Year AS year_code, Year AS year_desc, Year AS year_sort
    FROM myinfovault.PublicationSummary
    WHERE  Year <= ( date_format(curdate(),'%Y')+2 )
  UNION ALL
    SELECT null AS year_code, 'ALL' AS year_desc, 'ZZZZ' AS year_sort
  ORDER BY year_sort DESC;

