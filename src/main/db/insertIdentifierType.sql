# Author: Lawrence Fyfe
# Date: 10/24/2006

truncate IdentifierType;

insert into IdentifierType (ID,Description,InsertUserID)
values
(1,'Employee ID',14021),
(2,'Mothra ID',18099);
