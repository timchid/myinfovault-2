Update Dossier set PreviousWorkflowLocations='Candidate,Department,School,ViceProvost,ReadyToArchive' where WorkflowLocations = 'Archive' and DelegationAuthority='NON_REDELEGATED' and (ActionType='MERIT' or ActionType='PROMOTION') ;
Update Dossier set PreviousWorkflowLocations='Candidate,Department,School,ViceProvost' where WorkflowLocations = 'ReadyToArchive' and DelegationAuthority='NON_REDELEGATED' and (ActionType='MERIT' or ActionType='PROMOTION') ;
Update Dossier set PreviousWorkflowLocations='Candidate,Department,School' where WorkflowLocations = 'ViceProvost' and DelegationAuthority='NON_REDELEGATED' and (ActionType='MERIT' or ActionType='PROMOTION') ;
Update Dossier set PreviousWorkflowLocations='Candidate,Department,School,ReadyToArchive' where WorkflowLocations = 'Archive' and DelegationAuthority='REDELEGATED' and (ActionType='MERIT' or ActionType='PROMOTION') ;
Update Dossier set PreviousWorkflowLocations='Candidate,Department,School' where WorkflowLocations = 'ReadyToArchive' and DelegationAuthority='REDELEGATED' and (ActionType='MERIT' or ActionType='PROMOTION') ;

-- CRC Review
Update Dossier set DelegationAuthority = 'REDELEGATED', ReviewType=null, PreviousWorkflowLocations=null where WorkflowLocations = 'Candidate' and DelegationAuthority='REDELEGATED_FEDERATION';
Update Dossier set DelegationAuthority = 'REDELEGATED', ReviewType=null, PreviousWorkflowLocations='Candidate' where WorkflowLocations = 'Department' and DelegationAuthority='REDELEGATED_FEDERATION';
Update Dossier set DelegationAuthority = 'REDELEGATED', ReviewType=null, PreviousWorkflowLocations='Candidate,Department' where WorkflowLocations = 'School' and DelegationAuthority='REDELEGATED_FEDERATION';
Update Dossier set DelegationAuthority = 'REDELEGATED', ReviewType=null, PreviousWorkflowLocations='Candidate,Department,School,Senate,PostSenateSchool,ReadyToArchive' where WorkflowLocations = 'Archive' and DelegationAuthority='REDELEGATED_FEDERATION';
Update Dossier set DelegationAuthority = 'REDELEGATED', ReviewType=null, PreviousWorkflowLocations='Candidate,Department,School,Senate,PostSenateSchool' where WorkflowLocations = 'ReadyToArchive' and DelegationAuthority='REDELEGATED_FEDERATION';
Update Dossier set DelegationAuthority = 'REDELEGATED', ReviewType=null, WorkflowLocations = 'PostSenateSchool', PreviousWorkflowLocations='Candidate,Department,School,Senate' where WorkflowLocations = 'FederationSchool' and DelegationAuthority='REDELEGATED_FEDERATION';
Update Dossier set DelegationAuthority = 'REDELEGATED', ReviewType=null, WorkflowLocations = 'Senate', PreviousWorkflowLocations='Candidate,Department,School' where WorkflowLocations = 'FederationViceProvost' and DelegationAuthority='REDELEGATED_FEDERATION';

-- These are the same for all delegation authorities and action types
Update Dossier set PreviousWorkflowLocations='Candidate,Department' where WorkflowLocations = 'School';
Update Dossier set PreviousWorkflowLocations='Candidate' where WorkflowLocations = 'Department';

-- Update the ReviewLocation in DossierReviewers
update DossierReviewers set ReviewLocation='Senate' where ReviewLocation='FederationViceProvost';
update DossierReviewers set ReviewLocation='PostSenateSchool' where ReviewLocation='FederationSchool';