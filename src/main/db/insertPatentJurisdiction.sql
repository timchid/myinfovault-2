-- Author: Pradeep Haldiya
-- Created: 2011-12-16
-- Updated: 2013-12-04

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM PatentJurisdiction INTO @_ORIG_COUNT;

truncate PatentJurisdiction;

-- "Code" field values used here (2nd column) MUST conform to ISO 3166-1 "alpha-3" standards.
-- See http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3 and verify that any code added is correct!

INSERT INTO PatentJurisdiction (ID, Code, Jurisdiction, Abbreviation, ApplicationText , PatentText, Tooltip, Active, InsertUserId)
VALUES
( 1, 'USA', 'United States', 'USPTO', 'U.S. Patent Application', 'U.S. patent', null, true, 20720),
( 2, 'CAN', 'Canada', 'CIPO', 'Canada Application', 'Canada patent', null, true, 20720),
( 3, 'AUS', 'Australia', 'APO', 'Australia Application', 'Australia patent', null, true, 20720),
( 4, 'XAA', 'Europe', 'EPO', 'EPO Application', 'European patent', 'The European Patent Office covers most countries in Europe', true, 20720),
( 5, 'JPN', 'Japan', 'JPO', 'Japan Application','Japan patent', null, true, 20720),
( 6, 'GBR', 'United Kingdom', 'UK-IPO', 'British Application', 'British patent', null, true, 20720),
( 7, 'CHN', 'China', 'SAIC', 'Chinese Patent Application', 'Chinese patent', null, true, 18099),
( 8, 'PCT', 'World/International Patent', 'PCT', 'World Patent Application', 'World patent', 'International Patent under the &quot;Patent Cooperation Treaty&quot; (PCT) &mdash; http://www.wipo.int/pct/en/treaty/about.html', true, 18099),
( 9, 'BRA', 'Brazil', 'INPI', 'Brazilian patent Application', 'Brazilian patent', null, true, 20720),
(10, 'HKG', 'Hong Kong', 'IPD HKSAR', 'Hong Kong Application', 'Hong Kong patent', 'Intellectual Property Department (IPD) of Hong Kong Special Administrative Region (SAR) of China', true, 18099),
(11, 'IND', 'India', 'CGPDTM', 'India Application', 'India patent', null, true, 18099),
(12, 'KOR', 'Korea (South)', 'KIPO', 'Korean Application', 'Korean patent', null, true, 18099),
(13, 'MEX', 'Mexico', 'IMPI', 'Mexico Application', 'Mexico patent', null, true, 18099),
(14, 'NZL', 'New Zealand', 'IPONZ', 'New Zealand Application', 'New Zealand patent', null, true, 18099),
(15, 'RUS', 'Russian Federation', 'ROSPATENT', 'Russian Federation Application', 'Russian Federation patent', null, true, 18099),
(16, 'ZAF', 'South Africa', 'CIPC', 'South Africa Application', 'South Africa patent', null, true, 18099),
(17, 'VNM', 'Vietnam', 'NOIP', 'Vietnam Application', 'Vietnam patent', null, true, 18099),
(18, 'FRA', 'France', 'INPI', 'French Patent Application', 'French patent', 'Institut National de la Propriété Industrielle', true, 18099),
(19, 'DEU', 'Germany', 'DPMA', 'German Patent Application', 'German patent', 'Deutsches Patent- und Markenamt', true, 18099)
;

SHOW WARNINGS;
commit;

SELECT count(*) FROM PatentJurisdiction INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
