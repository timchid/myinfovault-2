# Author: Lawrence Fyfe
# Date: 10/11/2006

create table ThesisCommitteeSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   StartYear varchar(4) COMMENT 'Starting academic year',
   EndYear varchar(10) COMMENT 'Ending academic year',
   StudentName varchar(50) COMMENT 'Name of the student under the committee',
   CoChair boolean COMMENT 'Was associated user the committee co-chair?', -- TODO: cochair, committeechair, and committeemember are mutually exclusive; change to enum
   CommitteeChair boolean COMMENT 'Was associated user the committee chair?',
   CommitteeMember boolean COMMENT 'Was associated user a committee member (not including chair and co-chair)?',
   MastersDegree boolean COMMENT 'Thesis is for a masters degree?', -- TODO: mastersdegree, doctoraldegree, and doctoratedegree are mutually exclusive; change to enum
   DoctoralDegree boolean COMMENT 'Thesis is for a doctoral degree?',
   DoctorateDegree boolean COMMENT 'Thesis is for a doctorate degree?',
   DegreeProgress boolean COMMENT 'Degree is in progress?', -- TODO: degreeprogress and degree awarded are mutually exclusive; change to enum
   DegreeAwarded boolean COMMENT 'Degree has been awarded?',
   Position varchar(50) COMMENT 'Current position',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Teaching: Thesis committees';
