# Author: Stephen Paulsen
# Date: 2007-02-01

# Converts a MM/DD/YYYY date like Grants use
# to a YYYY-MM-DD sql date.
# Other formats could be converted by adding more 'WHEN' clauses.

DELIMITER |

DROP FUNCTION IF EXISTS convert_grant_date|
CREATE FUNCTION convert_grant_date(odate TEXT) returns DATE
BEGIN
  DECLARE mPart TEXT;
  DECLARE dPart TEXT;
  DECLARE yPart TEXT;
  CASE
    WHEN odate like '__/__/____' THEN
    BEGIN
      SET mPart=LEFT(odate,2);
      SET dPart=SUBSTR(odate,4,2);
      SET yPart=RIGHT(odate,4);
    END;
    WHEN odate like '____' THEN
    BEGIN
      set mPart='00';
      set dPart='00';
      set yPart=odate;
    END;
  END CASE;
  RETURN CONCAT_WS('-',yPart,mPart,dPart);
END;
|

DELIMITER ;

