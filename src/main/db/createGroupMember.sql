-- Author: Rick Hendricks
-- Date: 08/05/2011
-- This table defines the members of a Group.

-- Create the User-to-Group table
-- Dependencies: UserAccount, Group 

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

drop table if exists `GroupMember`;

create table `GroupMember`
(
   `ID` int not null auto_increment,
   `PersonUUID` varchar(100) not null,
   `GroupID` int not null,
   `Active` boolean null default true,
   `InsertTimestamp` timestamp default current_timestamp,
   `InsertUserID` int,
   `UpdateTimestamp` datetime,
   `UpdateUserID` int,
   PRIMARY KEY (`ID`),
   CONSTRAINT
      FOREIGN KEY (`GroupID`)
      REFERENCES `Groups` (`ID`)
      ON DELETE CASCADE,
   CONSTRAINT FOREIGN KEY (`PersonUUID`)
      REFERENCES UserAccount(`PersonUUID`)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
