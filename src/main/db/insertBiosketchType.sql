-- Author: Stephen Paulsen
-- Date: 9/15/2008

truncate BiosketchType;

insert into BiosketchType (ID, Description, Code, InsertUserID)
values
(2, 'Curriculum Vitae', 'cv', 18099),
(3, 'NIH Biosketch', 'nih', 18099);
