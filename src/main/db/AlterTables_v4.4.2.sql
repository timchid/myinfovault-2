--  Author: Pradeep Haldiya
-- Created: 2012-06-13
-- Updated: 

-- Table and Data updates for v4.4.2 (Sprint Release)

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- Diversity Statement transaction table
source createDiversityStatement.sql

-- Mapping of Diversity Statement Document
source insertDocument.sql
source insertCollectionDocument.sql
source insertDocumentSection.sql
source insertSection.sql
source insertSystemSectionHeader.sql

-- Patents filed and granted split into seciontions
source insertBiosketchSectionLimits.sql

-- Insert a patent-filed section for each patent-granted section, unless a patent-filed section already exists for the biosketch
INSERT INTO BiosketchSection(BiosketchID, SectionID, SectionName, Availability, display, displayHeader, IsMainSection, InSelectList, MayHideHeader, DisplayInBiosketch, InsertTimestamp, InsertUserID, UpdateTimestamp, UpdateUserID)
SELECT BiosketchID, '98', 'patent-filed', Availability, display, displayHeader, IsMainSection, InSelectList, MayHideHeader, DisplayInBiosketch, now(), '20001', now(), '20001'
FROM BiosketchSection s
WHERE s.SectionID='92'
AND NOT EXISTS (
SELECT ID
FROM BiosketchSection
WHERE BiosketchID=s.BiosketchID
AND SectionID='98');

ALTER TABLE `UserRecordAnnotation` CHANGE COLUMN `Label` `Label` mediumtext NULL DEFAULT NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
