-- Author: Rick Hendricks
-- Created: 2009-07-24
-- Updated: 2010-03-02

-- The DOSSIERATTRIBUTELOCATION table is used to relate a dossier's attributes
-- to a work flow location. Attributes are associated with each location and
-- appointment type possible for a dossier. The Sequence defined for the of the
-- attributes as they will be displayed is defined for each LocationId and
-- AppointmentType. Each workflow location location and appointment type may
-- define a differing set of attributes in a different order.
--
-- COLUMN              TYPE      FUNCION
-- ______________________________________
-- ID                  int       Unique ID.
-- LocationId          int       The location id from the WORKFLOWLOCATION table.
-- DossierAttributeId  int       The attribute id from the DOSSIERATTRIBUTETYPE table.
-- AppointmentType     enum      ENUM representing the appointment type for a dossier: PRIMARY or JOINT
-- Sequence            int       The sorting sequence of the attribute representing the display order. As mentioned
--                               earlier, the sequences are defined within a  particular  AppointmentType and LocationId.
-- InsertTimestamp     timestamp The insert timestamp of the record.
-- InsertUserID        int       The user inserting the record.
-- UpdateTimestamp     datetime  The update timestamp of the record.
-- UpdateUserID        int       The user updating the record.
--
-- Define DosserAttributes for each AppointmentType valid for WorkflowLocation 1 (Packet Request)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 2 (Department)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 3 (School)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 4 (ViceProvost)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 7 (FederationSchool)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 8 (FederationViceProvost)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 9 (Senate)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 10 (Federation)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 11 (SenateFederation)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 12 (PostSenateSchool)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 13 (PostSenateViceProvost)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 14 (PostAuditReview)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 15 (SenateAppeal)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 16 (FederationAppeal)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 17 (FederationSenateAppeal)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 18 (PostAppealSchool)
-- Define DosserAttributes valid for each AppointmentType WorkflowLocation 19 (PostAppealViceProvostSchool)
--
-- Example query to retrieve attributes for a specified delegation authority, workflow location and appointment type.
-- SELECT dloc.DossierAttributeID, dtype.Description, dtype.Name
--             FROM
--             DossierAttributeType dtype,
--             DelegationAuthorityAttributeCollection dcol,
--             DossierAttributeLocation dloc,
--             DelegationAuthority dauth,
--             WorkflowLocation wfl
--             WHERE
--             dauth.DelegationAuthority='NON_REDELEGATED' AND
--             dauth.ID=dcol.DelegationAuthorityID AND
--             wfl.WorkflowNodeName='School' AND
--             dloc.LocationID = wfl.ID AND
--             dloc.AppointmentType='JOINT' AND
--             dcol.DossierAttributeLocationID=dloc.ID AND
--             dtype.ID=dloc.DossierAttributeID
--             ORDER BY dloc.Sequence;
--
-- *** Note that when adding new records,resist the temptation to
-- *** renumber the ID's. The ID's are specifically referenced in the
-- *** DELEGATIONAUTHORITYATTRIBUTECOLLECTION table.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE `myinfovault`;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`DossierAttributes`
-- -----------------------------------------------------
TRUNCATE `DossierAttributeLocation`;

SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT, AUTOCOMMIT=0;
INSERT INTO `DossierAttributeLocation` (`ID`, `LocationID`, `AppointmentType`,`DossierAttributeID`,`Sequence`,`InsertTimestamp`, `InsertUserID`)
VALUES

-- Packet Request
(1, 1, 'PRIMARY', 31, 1, current_timestamp, 18635),
(222, 1, 'PRIMARY', 67, 2, current_timestamp, 18635),
(2, 1, 'PRIMARY', 1, 3, current_timestamp, 18635),
(3, 1, 'PRIMARY', 3, 4, current_timestamp, 18635),
(4, 1, 'PRIMARY', 5, 5, current_timestamp, 18635),
(5, 1, 'PRIMARY', 14,6, current_timestamp, 18635),
(6, 1, 'PRIMARY', 7, 7, current_timestamp, 18635),
(7, 1, 'PRIMARY', 8, 8, current_timestamp, 18635),
(8, 1, 'PRIMARY', 9, 9, current_timestamp, 18635),
(9, 1, 'PRIMARY', 10, 10, current_timestamp, 18635),
(10, 1, 'PRIMARY', 11, 11, current_timestamp, 18635),
(11, 1, 'PRIMARY', 12, 12, current_timestamp, 18635),
(12,1, 'PRIMARY', 13, 13, current_timestamp, 18635),
(13, 1, 'PRIMARY', 16, 14, current_timestamp, 18635),
(15, 1, 'PRIMARY', 18, 16, current_timestamp, 18635),
(16, 1, 'PRIMARY', 19, 17, current_timestamp, 18635),
(248, 1, 'PRIMARY', 75, 18, current_timestamp, 18635),
(241, 1, 'PRIMARY', 68, 19, current_timestamp, 18635),
(242, 1, 'PRIMARY', 69, 20, current_timestamp, 18635),
(243, 1, 'PRIMARY', 70, 21, current_timestamp, 18635),
(14, 1, 'PRIMARY', 17, 15, current_timestamp, 18635),
(224, 1, 'PRIMARY', 61, 22, current_timestamp, 18635),
-- (244, 1, 'PRIMARY', 71, 23, current_timestamp, 18635),
(245, 1, 'PRIMARY', 72, 24, current_timestamp, 18635),
(225, 1, 'PRIMARY', 62, 25, current_timestamp, 18635),
(223, 1, 'PRIMARY', 60, 26, current_timestamp, 18635),
(226, 1, 'PRIMARY', 63, 27, current_timestamp, 18635),
(246, 1, 'PRIMARY', 73, 28, current_timestamp, 18635),
(227, 1, 'PRIMARY', 64, 29, current_timestamp, 18635),
(247, 1, 'PRIMARY', 74, 30, current_timestamp, 18635),
(228, 1, 'PRIMARY', 65, 31, current_timestamp, 18635),
(229, 1, 'PRIMARY', 66, 32, current_timestamp, 18635),
(320, 1, 'PRIMARY', 122,33, current_timestamp, 18635),
(17, 1, 'JOINT', 2,1, current_timestamp, 18635),
(18, 1, 'JOINT', 4,2, current_timestamp, 18635),
(19, 1, 'JOINT', 6,3, current_timestamp, 18635),
(20, 1, 'JOINT', 15,4, current_timestamp, 18635),
(21, 1, 'JOINT', 18,5, current_timestamp, 18635),
(22, 1, 'JOINT', 19,6, current_timestamp, 18635),


-- Department
(23, 2, 'PRIMARY', 31, 1, current_timestamp, 18635),
(221, 2, 'PRIMARY', 67,2, current_timestamp, 18635),
(24, 2, 'PRIMARY', 1, 3, current_timestamp, 18635),
(25, 2, 'PRIMARY', 3,4, current_timestamp, 18635),
(26, 2, 'PRIMARY', 5,5, current_timestamp, 18635),
(27, 2, 'PRIMARY', 14,6, current_timestamp, 18635),
(28, 2, 'PRIMARY', 7,7, current_timestamp, 18635),
(29, 2, 'PRIMARY', 8,8, current_timestamp, 18635),
(30, 2, 'PRIMARY', 9,9, current_timestamp, 18635),
(31, 2, 'PRIMARY', 10,10, current_timestamp, 18635),
(32, 2, 'PRIMARY', 11,11, current_timestamp, 18635),
(33, 2, 'PRIMARY', 12,12, current_timestamp, 18635),
(34, 2, 'PRIMARY', 13,13, current_timestamp, 18635),
(35, 2, 'PRIMARY', 16,14, current_timestamp, 18635),
(36, 2, 'PRIMARY', 17,15, current_timestamp, 18635),
(37, 2, 'PRIMARY', 18,16, current_timestamp, 18635),
(38, 2, 'PRIMARY', 19,17, current_timestamp, 18635),
(249, 2, 'PRIMARY', 75, 18, current_timestamp, 18635),
(234, 2, 'PRIMARY', 68, 19, current_timestamp, 18635),
(235, 2, 'PRIMARY', 69, 20, current_timestamp, 18635),
(236, 2, 'PRIMARY', 70, 21, current_timestamp, 18635),
(215, 2, 'PRIMARY', 61, 22, current_timestamp, 18635),
-- (237, 2, 'PRIMARY', 71, 23, current_timestamp, 18635),
(238, 2, 'PRIMARY', 72, 24, current_timestamp, 18635),
(216, 2, 'PRIMARY', 62, 25, current_timestamp, 18635),
(214, 2, 'PRIMARY', 60, 26, current_timestamp, 18635),
(217, 2, 'PRIMARY', 63, 27, current_timestamp, 18635),
(239, 2, 'PRIMARY', 73, 28, current_timestamp, 18635),
(218, 2, 'PRIMARY', 64, 29, current_timestamp, 18635),
(240, 2, 'PRIMARY', 74, 30, current_timestamp, 18635),
(219, 2, 'PRIMARY', 65, 31, current_timestamp, 18635),
(220, 2, 'PRIMARY', 66, 32, current_timestamp, 18635),
(321, 2, 'PRIMARY', 122,33, current_timestamp, 18635),
(39, 2, 'JOINT', 2,1, current_timestamp, 18635),
(40, 2, 'JOINT', 4,2, current_timestamp, 18635),
(41, 2, 'JOINT', 6,3, current_timestamp, 18635),
(42, 2, 'JOINT', 15,4, current_timestamp, 18635),
(43, 2, 'JOINT', 18,5, current_timestamp, 18635),
(44, 2, 'JOINT', 19,6, current_timestamp, 18635),

-- School
(230, 3, 'PRIMARY', 67, 1, current_timestamp, 18635),
(45, 3, 'PRIMARY', 31,2, current_timestamp, 18635),
(46, 3, 'PRIMARY', 18,3, current_timestamp, 18635),
(47, 3, 'PRIMARY', 20,4, current_timestamp, 18635),
(62, 3, 'PRIMARY', 34,5, current_timestamp, 18635),
(63, 3, 'PRIMARY', 36,6, current_timestamp, 18635),
(61, 3, 'PRIMARY', 32,7, current_timestamp, 18635),
(301, 3, 'PRIMARY', 109,7, current_timestamp, 20001),
(49, 3, 'PRIMARY', 23,8, current_timestamp, 18635),
(48, 3, 'PRIMARY', 22,9, current_timestamp, 18635),

(50, 3, 'JOINT', 18,1, current_timestamp, 18635),
(51, 3, 'JOINT', 21,2, current_timestamp, 18635),
(66, 3, 'JOINT', 37,3, current_timestamp, 18635),
-- (65, 3, 'JOINT', 35,4, current_timestamp, 18635),
(64, 3, 'JOINT', 107,5, current_timestamp, 20001),
(52, 3, 'JOINT', 24,6, current_timestamp, 18635),
(171, 3, 'JOINT', 40,7, current_timestamp, 18635),


-- ViceProvost
(69, 4, 'PRIMARY', 31,1, current_timestamp, 18635),   -- First two intentionally sequence 1
(231, 4, 'PRIMARY', 67, 1, current_timestamp, 18635),
(53, 4, 'PRIMARY', 18,2, current_timestamp, 18635),
(54, 4, 'PRIMARY', 25,3, current_timestamp, 18635),
(55, 4, 'PRIMARY', 26,4, current_timestamp, 18635),
(56, 4, 'PRIMARY', 27,5, current_timestamp, 18635),
(57, 4, 'PRIMARY', 28,6, current_timestamp, 18635),
(58, 4, 'PRIMARY', 29,7, current_timestamp, 18635),
(67, 4, 'PRIMARY', 38,8, current_timestamp, 18635),
(68, 4, 'PRIMARY', 39,9, current_timestamp, 18635),
(207, 4, 'PRIMARY', 58,10, current_timestamp, 20508), -- Action History document
(286, 4, 'PRIMARY', 100,11, current_timestamp, 18635),
(250, 4, 'PRIMARY', 76,12, current_timestamp, 18635),
(251, 4, 'PRIMARY', 77,13, current_timestamp, 18635),
(295, 4, 'PRIMARY', 103,14, current_timestamp, 18635),
(59, 4, 'PRIMARY', 33,15, current_timestamp, 18635),
(60, 4, 'PRIMARY', 30,16, current_timestamp, 18635),
(208, 4, 'PRIMARY', 59,17, current_timestamp, 19012),
(287, 4, 'PRIMARY', 101,18, current_timestamp, 18635),
(252, 4, 'PRIMARY', 78,19, current_timestamp, 18635),
(305, 4, 'PRIMARY', 113,20, current_timestamp, 20001),
(253, 4, 'PRIMARY', 79,21, current_timestamp, 20001),
(296, 4, 'PRIMARY', 105,22, current_timestamp, 20001),
(254, 4, 'PRIMARY', 80,23, current_timestamp, 20001),
(306, 4, 'PRIMARY', 114,24, current_timestamp, 20001),
(255, 4, 'PRIMARY', 81,25, current_timestamp, 20001),
(307, 4, 'PRIMARY', 115,26, current_timestamp, 20001),
(302, 4, 'PRIMARY', 110,27, current_timestamp, 20001),
(256, 4, 'PRIMARY', 82,28, current_timestamp, 20001),
(308, 4, 'PRIMARY', 112,29, current_timestamp, 20001),
(257, 4, 'PRIMARY', 83,30, current_timestamp, 20001),
(258, 4, 'PRIMARY', 84,31, current_timestamp, 20001),
(288, 4, 'PRIMARY', 102,32, current_timestamp, 20001),
(259, 4, 'PRIMARY', 85,33, current_timestamp, 20001),
(309, 4, 'PRIMARY', 120,34, current_timestamp, 20001),
(260, 4, 'PRIMARY', 86,35, current_timestamp, 20001),
(261, 4, 'PRIMARY', 87,36, current_timestamp, 20001),

-- FederationSchool
(92, 7, 'PRIMARY', 31,1, current_timestamp, 18635),
(93, 7, 'PRIMARY', 18,2, current_timestamp, 18635),
(94, 7, 'PRIMARY', 20,3, current_timestamp, 18635),
(95, 7, 'PRIMARY', 36,4, current_timestamp, 18635),
(96, 7, 'PRIMARY', 34,5, current_timestamp, 18635),
(97, 7, 'PRIMARY', 32,6, current_timestamp, 18635),
(98, 7, 'PRIMARY', 22,7, current_timestamp, 18635),
(99, 7, 'PRIMARY', 23,8, current_timestamp, 18635),
(100, 7, 'JOINT', 18,1, current_timestamp, 18635),
(101, 7, 'JOINT', 21,2, current_timestamp, 18635),
(102, 7, 'JOINT', 37,3, current_timestamp, 18635),
-- (103, 7, 'JOINT', 35,4, current_timestamp, 18635),
(104, 7, 'JOINT', 107,5, current_timestamp, 20001),
(105, 7, 'JOINT', 24,6, current_timestamp, 18635),

-- FederationViceProvost
(106, 8, 'PRIMARY', 18,1, current_timestamp, 18635),
(107, 8, 'PRIMARY', 25,2, current_timestamp, 18635),
(108, 8, 'PRIMARY', 26,3, current_timestamp, 18635),
(109, 8, 'PRIMARY', 27,4, current_timestamp, 18635),
(110, 8, 'PRIMARY', 28,5, current_timestamp, 18635),
(111, 8, 'PRIMARY', 29,6, current_timestamp, 18635),
(112, 8, 'PRIMARY', 38,7, current_timestamp, 18635),
(113, 8, 'PRIMARY', 39,8, current_timestamp, 18635),
(114, 8, 'PRIMARY', 33,9, current_timestamp, 18635),
(115, 8, 'PRIMARY', 30,10, current_timestamp, 18635),
(209, 8, 'PRIMARY', 59,11, current_timestamp, 19012),

-- Senate
(116, 9, 'PRIMARY', 18,1, current_timestamp, 18635),
(117, 9, 'PRIMARY', 25,2, current_timestamp, 18635),
(118, 9, 'PRIMARY', 26,3, current_timestamp, 18635),
(119, 9, 'PRIMARY', 27,7, current_timestamp, 18635),
(120, 9, 'PRIMARY', 28,6, current_timestamp, 18635),
(121, 9, 'PRIMARY', 29,5, current_timestamp, 18635),
(122, 9, 'PRIMARY', 38,8, current_timestamp, 18635),
(123, 9, 'PRIMARY', 39,4, current_timestamp, 18635),
(124, 9, 'PRIMARY', 33,9, current_timestamp, 18635),
(125, 9, 'PRIMARY', 30,10, current_timestamp, 18635),
(210, 9, 'PRIMARY', 59,11, current_timestamp, 19012),

-- Federation
(126, 10, 'PRIMARY', 18,1, current_timestamp, 18635),
(127, 10, 'PRIMARY', 25,2, current_timestamp, 18635),
(128, 10, 'PRIMARY', 26,3, current_timestamp, 18635),
(129, 10, 'PRIMARY', 27,7, current_timestamp, 18635),
(130, 10, 'PRIMARY', 28,6, current_timestamp, 18635),
(131, 10, 'PRIMARY', 29,5, current_timestamp, 18635),
(132, 10, 'PRIMARY', 38,8, current_timestamp, 18635),
(133, 10, 'PRIMARY', 39,4, current_timestamp, 18635),
(134, 10, 'PRIMARY', 33,9, current_timestamp, 18635),
(135, 10, 'PRIMARY', 30,10, current_timestamp, 18635),
(211, 10, 'PRIMARY', 59,11, current_timestamp, 19012),

-- SenateFederation
(136, 11, 'PRIMARY', 18,1, current_timestamp, 18635),
(137, 11, 'PRIMARY', 25,2, current_timestamp, 18635),
(138, 11, 'PRIMARY', 26,3, current_timestamp, 18635),
(139, 11, 'PRIMARY', 27,7, current_timestamp, 18635),
(140, 11, 'PRIMARY', 28,6, current_timestamp, 18635),
(141, 11, 'PRIMARY', 29,5, current_timestamp, 18635),
(142, 11, 'PRIMARY', 38,8, current_timestamp, 18635),
(143, 11, 'PRIMARY', 39,4, current_timestamp, 18635),
(144, 11, 'PRIMARY', 33,9, current_timestamp, 18635),
(145, 11, 'PRIMARY', 30,10, current_timestamp, 18635),
(212, 11, 'PRIMARY', 59,11, current_timestamp, 19012),

-- PostSenateSchool
(146, 12, 'PRIMARY', 31,1, current_timestamp, 18635),
(232, 12, 'PRIMARY', 67, 1, current_timestamp, 18635),
(147, 12, 'PRIMARY', 18,2, current_timestamp, 18635),
(148, 12, 'PRIMARY', 20,3, current_timestamp, 18635),
(149, 12, 'PRIMARY', 36,4, current_timestamp, 18635),
(150, 12, 'PRIMARY', 34,5, current_timestamp, 18635),
(151, 12, 'PRIMARY', 32,6, current_timestamp, 18635),
(152, 12, 'PRIMARY', 22,7, current_timestamp, 18635),
(153, 12, 'PRIMARY', 23,8, current_timestamp, 18635),

(154, 12, 'JOINT', 18,1, current_timestamp, 18635),
(155, 12, 'JOINT', 21,2, current_timestamp, 18635),
(156, 12, 'JOINT', 37,3, current_timestamp, 18635),
-- (157, 12, 'JOINT', 35,4, current_timestamp, 18635),
(158, 12, 'JOINT', 107,5, current_timestamp, 20001),
(159, 12, 'JOINT', 24,6, current_timestamp, 18635),
(199, 12, 'JOINT', 40,7, current_timestamp, 18635),

-- PostSenateViceProvost
(160, 13, 'PRIMARY', 31,1, current_timestamp, 18635),
(233, 13, 'PRIMARY', 67, 1, current_timestamp, 18635),
(161, 13, 'PRIMARY', 18,2, current_timestamp, 18635),
(289, 13, 'PRIMARY', 100,3, current_timestamp, 18635),
(162, 13, 'PRIMARY', 25,4, current_timestamp, 18635),
(163, 13, 'PRIMARY', 26,5, current_timestamp, 18635),
(164, 13, 'PRIMARY', 27,6, current_timestamp, 18635),
(165, 13, 'PRIMARY', 28,7, current_timestamp, 18635),
(166, 13, 'PRIMARY', 29,8, current_timestamp, 18635),
(167, 13, 'PRIMARY', 38,9, current_timestamp, 18635),
(168, 13, 'PRIMARY', 39,10, current_timestamp, 18635),
(262, 13, 'PRIMARY', 76,11, current_timestamp, 18635),
(263, 13, 'PRIMARY', 77,12, current_timestamp, 18635),
(297, 13, 'PRIMARY', 103,13, current_timestamp, 18635),
(169, 13, 'PRIMARY', 33,14, current_timestamp, 18635),
(170, 13, 'PRIMARY', 30,15, current_timestamp, 18635),
(213, 13, 'PRIMARY', 59,16, current_timestamp, 19012),
(290, 13, 'PRIMARY', 101,17, current_timestamp, 18635),
(264, 13, 'PRIMARY', 78,18, current_timestamp, 18635),
(310, 13, 'PRIMARY', 113,19, current_timestamp, 20001),
(265, 13, 'PRIMARY', 79,20, current_timestamp, 20001),
(298, 13, 'PRIMARY', 105,21, current_timestamp, 20001),
(266, 13, 'PRIMARY', 80,22, current_timestamp, 20001),
(311, 13, 'PRIMARY', 114,23, current_timestamp, 20001),
(267, 13, 'PRIMARY', 81,24, current_timestamp, 20001),
(312, 13, 'PRIMARY', 115,25, current_timestamp, 20001),
(303, 13, 'PRIMARY', 110,26, current_timestamp, 20001),
(268, 13, 'PRIMARY', 82,27, current_timestamp, 20001),
(313, 13, 'PRIMARY', 112,28, current_timestamp, 20001),
(269, 13, 'PRIMARY', 83,29, current_timestamp, 20001),
(270, 13, 'PRIMARY', 84,30, current_timestamp, 20001),
(291, 13, 'PRIMARY', 102,31, current_timestamp, 20001),
(271, 13, 'PRIMARY', 85,32, current_timestamp, 20001),
(314, 13, 'PRIMARY', 120,33, current_timestamp, 20001),
(272, 13, 'PRIMARY', 86,34, current_timestamp, 20001),
(273, 13, 'PRIMARY', 87,35, current_timestamp, 20001),

-- PostAuditReview
(172, 14, 'PRIMARY', 18,1, current_timestamp, 18635),
(173, 14, 'PRIMARY', 49,2, current_timestamp, 18635),

-- SenateAppeal
(174, 15, 'PRIMARY', 18,1, current_timestamp, 18635),
(175, 15, 'PRIMARY', 50,2, current_timestamp, 18635),
(176, 15, 'PRIMARY', 51,3, current_timestamp, 18635),
(200, 15, 'PRIMARY', 55,5, current_timestamp, 18635),
(201, 15, 'PRIMARY', 56,4, current_timestamp, 18635),

-- FederationAppeal
(177, 16, 'PRIMARY', 18,1, current_timestamp, 18635),
(178, 16, 'PRIMARY', 52,4, current_timestamp, 18635),
(179, 16, 'PRIMARY', 53,3, current_timestamp, 18635),
(180, 16, 'PRIMARY', 54,2, current_timestamp, 18635),
(202, 16, 'PRIMARY', 55,6, current_timestamp, 18635),
(203, 16, 'PRIMARY', 57,5, current_timestamp, 18635),

-- FederationSenateAppeal
(181, 17, 'PRIMARY', 18,1, current_timestamp, 18635),
(182, 17, 'PRIMARY', 50,2, current_timestamp, 18635),
(183, 17, 'PRIMARY', 51,3, current_timestamp, 18635),
(184, 17, 'PRIMARY', 52,7, current_timestamp, 18635),
(185, 17, 'PRIMARY', 53,6, current_timestamp, 18635),
(186, 17, 'PRIMARY', 54,5, current_timestamp, 18635),
(204, 17, 'PRIMARY', 55,9, current_timestamp, 18635),
(205, 17, 'PRIMARY', 56,4, current_timestamp, 18635),
(206, 17, 'PRIMARY', 57,8, current_timestamp, 18635),

-- PostAppealSchool
(187, 18, 'PRIMARY', 18,1, current_timestamp, 18635),
(188, 18, 'PRIMARY', 43,2, current_timestamp, 18635),
(189, 18, 'PRIMARY', 47,3, current_timestamp, 18635),
(190, 18, 'PRIMARY', 41,4, current_timestamp, 18635),
(191, 18, 'JOINT', 18,1, current_timestamp, 18635),
(192, 18, 'JOINT', 44,2, current_timestamp, 18635),
(193, 18, 'JOINT', 108,3, current_timestamp, 20001),
(194, 18, 'JOINT', 42,4, current_timestamp, 18635),

-- PostAppealViceProvost
(195, 19, 'PRIMARY', 18,1, current_timestamp, 18635),
(292, 19, 'PRIMARY', 100,2, current_timestamp, 18635),
(274, 19, 'PRIMARY', 88,3, current_timestamp, 18635),
(275, 19, 'PRIMARY', 89,4, current_timestamp, 18635),
(299, 19, 'PRIMARY', 104,5, current_timestamp, 18635),
(197, 19, 'PRIMARY', 48,6, current_timestamp, 18635),
(198, 19, 'PRIMARY', 45,7, current_timestamp, 18635),
(196, 19, 'PRIMARY', 46,8, current_timestamp, 18635),
(293, 19, 'PRIMARY', 101,9, current_timestamp, 18635),
(276, 19, 'PRIMARY', 90,10, current_timestamp, 18635),
(315, 19, 'PRIMARY', 117,11, current_timestamp, 20001),
(277, 19, 'PRIMARY', 91,12, current_timestamp, 20001),
(300, 19, 'PRIMARY', 106,13, current_timestamp, 20001),
(278, 19, 'PRIMARY', 92,14, current_timestamp, 20001),
(316, 19, 'PRIMARY', 118,15, current_timestamp, 20001),
(279, 19, 'PRIMARY', 93,16, current_timestamp, 20001),
(317, 19, 'PRIMARY', 119,17, current_timestamp, 20001),
(304, 19, 'PRIMARY', 111,18, current_timestamp, 20001),
(280, 19, 'PRIMARY', 94,19, current_timestamp, 20001),
(318, 19, 'PRIMARY', 116,20, current_timestamp, 20001),
(281, 19, 'PRIMARY', 95,21, current_timestamp, 20001),
(282, 19, 'PRIMARY', 96,22, current_timestamp, 20001),
(294, 19, 'PRIMARY', 102,23, current_timestamp, 20001),
(283, 19, 'PRIMARY', 97,24, current_timestamp, 20001),
(319, 19, 'PRIMARY', 121,25, current_timestamp, 20001),
(284, 19, 'PRIMARY', 98,26, current_timestamp, 20001),
(285, 19, 'PRIMARY', 99,27, current_timestamp, 20001)
;


COMMIT;

SET AUTOCOMMIT=@OLD_AUTOCOMMIT;
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
