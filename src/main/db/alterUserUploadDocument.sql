-- Author: Rick Hendricks
-- Created: 09/05/2008
-- Updated: 09/24/2009


-- Change from v2.3 to v3.0
ALTER TABLE `UserUploadDocument`
 ADD COLUMN `UploadLocation` VARCHAR (40) NOT NULL after Display,
 ADD COLUMN `Redacted` BOOLEAN NULL DEFAULT false after Display,
 ADD COLUMN `Confidential` BOOLEAN NULL DEFAULT false after Display;
-- ADD COLUMN `SchoolID` BOOLEAN NULL DEFAULT false after Display,
-- ADD COLUMN `DepartmentID` BOOLEAN NULL DEFAULT false after Display;

-- Temporarily, until the old CvUploadPDF and CvUploadPDFUtil are retired,
-- this column can't be required ("NOT NULL").  Remove all this when the
-- new upload is in place so the column is once again required.
ALTER TABLE `UserUploadDocument`
 MODIFY COLUMN `Year` VARCHAR(4),
 MODIFY COLUMN `UploadLocation` VARCHAR (40);
