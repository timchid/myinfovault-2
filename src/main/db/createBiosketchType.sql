-- Author: Stephen Paulsen
-- Date: 9/15/2008

create table BiosketchType
(
   ID int primary key auto_increment,
   Description varchar(40) NOT NULL,
   Code varchar(4) NOT NULL,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8
collate = utf8_general_ci;
