# Author: Stephen Paulsen
# Date: 2007-02-05

# Updates on CvStatement -- Candidate's Statement

DELIMITER |

CREATE TRIGGER UpdateCvStatement AFTER UPDATE on CvStatement
FOR EACH ROW BEGIN
  IF NEW.UploadName IS NULL
  THEN
  UPDATE CandidateStatement
    SET
       UserID=NEW.SSN,
       Year=NEW.Year,
       Content=NEW.Content,
       Sequence=NEW.Seq,
       Display=(NEW.PkPrt='Y'),
       UpdateTimestamp=now(),
       UpdateUserID=NEW.SSN
    WHERE
       OldID=OLD.STID
    ;
  ELSE
  UPDATE UserUploadDocument
    SET
       UserID=NEW.SSN,
       UploadFile=NEW.UploadName,
       Year=NEW.Year,
       Display=(NEW.PkPrt='Y'),
       UpdateTimestamp=now(),
       UpdateUserID=NEW.SSN
    WHERE
       UserID=OLD.SSN AND
       DocumentID=1 AND
       UploadFile=OLD.UploadName AND
       Year=OLD.Year
    ;
  END IF;
END;
|

DELIMITER ;

