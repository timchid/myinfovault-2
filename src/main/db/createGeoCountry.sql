
DROP TABLE IF EXISTS `GeoCountry`;
CREATE TABLE `GeoCountry` (
  `CountryCode` varchar(2) COMMENT 'ISO 3166 Country Code' NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CountryCode`)
) 
ENGINE=InnoDB 
DEFAULT CHARSET=utf8
COLLATE = utf8_general_ci;
