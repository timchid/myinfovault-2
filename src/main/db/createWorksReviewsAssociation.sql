-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-05-23

-- Create the WorksReviewsAssociation Table
-- Dependencies: The Works and Reviews tables must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS WorksReviewsAssociation;

create table WorksReviewsAssociation
(
   UserID int NOT NULL,
   WorkID int NOT NULL,
   ReviewID int NOT NULL,
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   PRIMARY KEY (`WorkID`,`ReviewID`),
   INDEX(`WorkID`,`ReviewID`),
   CONSTRAINT  `fk_wr_workid` FOREIGN KEY (`WorkID`) REFERENCES `Works`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `fk_wr_reviewid` FOREIGN KEY (`ReviewID`) REFERENCES `Reviews`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=INNODB,
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
