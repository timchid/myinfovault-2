-- Author: Pradeep K Haldiya
-- Created: 2013-09-26

-- Refactoring Annotations

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- Create Annotation tables
DROP TABLE IF EXISTS AnnotationLines;
source createAnnotations.sql;
source createAnnotationLines.sql;

-- Adding columns just for data transfer 
ALTER TABLE `AnnotationLines` 
ADD COLUMN `RecordID` INT NULL  AFTER `AnnotationID` , 
ADD COLUMN `SectionBaseTable` VARCHAR(255) NULL DEFAULT NULL  AFTER `RecordID` , 
ADD COLUMN `Valid` BOOLEAN NULL  AFTER `DisplayBefore` ;

-- Insert Valid Annotation records
INSERT INTO Annotations(ID,UserID,RecordID,SectionBaseTable,Footnote,Notation,Display,InsertTimestamp,InsertUserID,UpdateTimestamp,UpdateUserID) 
SELECT ID,UserID,RecordID,SectionBaseTable,Footnote,Notation,Display,InsertTimestamp,InsertUserID,UpdateTimestamp,UpdateUserID 
FROM UserRecordAnnotation 
GROUP BY UserID, SectionBaseTable, RecordID;

-- Insert Annotation Lines from Annotation records 
INSERT INTO AnnotationLines(AnnotationID,RecordID,SectionBaseTable,Label,RightJustify, DisplayBefore,Valid,InsertTimestamp,InsertUserID,UpdateTimestamp,UpdateUserID) 
SELECT ID,RecordID,SectionBaseTable,Label,RightJustify, DisplayBefore,true,InsertTimestamp,InsertUserID,UpdateTimestamp,UpdateUserID 
FROM UserRecordAnnotation; 

-- Select the bad records
UPDATE AnnotationLines SET Valid=false 
WHERE AnnotationID 
NOT IN (SELECT ID From Annotations);

-- Correct AnnotationIDs

-- Create temporary indexes to increase performance of the UPDATE
create index IDX_TEMP_SectionBaseTable on AnnotationLines(SectionBaseTable);
create index IDX_TEMP_RecordId on AnnotationLines(RecordId);

UPDATE AnnotationLines A, Annotations R Set A.AnnotationID = R.ID 
WHERE A.Valid=false 
AND A.SectionBaseTable = R.SectionBaseTable 
AND A.RecordID = R.RecordID;

-- Drop all rows with empty label
DELETE FROM AnnotationLines WHERE Label is NULL or LENGTH(TRIM(Label))=0;

-- Delete computational columns...temp indexes will be dropped as well
ALTER TABLE `AnnotationLines` 
DROP COLUMN `Valid` , 
DROP COLUMN `SectionBaseTable` , 
DROP COLUMN `RecordID` ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
