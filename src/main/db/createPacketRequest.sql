-- Author: Rick Hendricks
-- Created: 2015-03-16

-- PacketRequest table tracks the requests for packets

-- Dependencies: UserAccount, Packet and Dossier tables must exit

-- Set lenient modes for table mods.
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE `myinfovault`.`PacketRequest`
(
   ID INT PRIMARY KEY auto_increment,
   UserID int NOT NULL,
   PacketID  INT NOT NULL DEFAULT 0,
   DossierID INT NOT NULL UNIQUE,     
   InsertTimestamp TIMESTAMP DEFAULT current_timestamp,
   InsertUserID INT NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID INT,
   INDEX `user_idx` (`UserID` ASC),
   INDEX `dossier_idx` (`DossierID` ASC),
   CONSTRAINT `fk_user_id` FOREIGN KEY (`UserID`)
     REFERENCES `myinfovault`.`UserAccount` (`UserID`) ON DELETE CASCADE ON UPDATE NO ACTION,
--   CONSTRAINT `fk_packet_id` FOREIGN KEY (`PacketID`) 
--     REFERENCES `myinfovault`.`Packet` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
   CONSTRAINT `fk_dossier_id` FOREIGN KEY (`DossierID`) 
     REFERENCES `myinfovault`.`Dossier` (`DossierID`) ON DELETE CASCADE ON UPDATE NO ACTION
)

ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- Restore normal modes.
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;