-- Author: Rick Hendricks
-- Date: 09/07/2012
-- This table defines the paths available at each of the workflow locations

drop table if exists DossierRoutingPathDefinitions;

create table DossierRoutingPathDefinitions
(
   DelegationAuthorityID int COMMENT 'ID from DelegationAuthority',
   ActiontypeID int COMMENT 'ID from ActionType',
   CurrentWorkflowLocationID int COMMENT 'ID from WorkflowLocation',
   WorkflowNodeType ENUM ('INITIAL','ENROUTE','EXCEPTION', 'FINAL') NOT null COMMENT 'Describes the type of workflow node',
   PreviousWorkflowLocationID int COMMENT 'ID from WorkflowLocation of the workflow location prior to this location',
   NextWorkflowLocationID int COMMENT 'ID from WorkflowLocation of the workflow location following this location',
   LocationPrerequisiteAttributes varchar(1024) COMMENT 'Name from the DossierAttributeType table with value of the attributes required to move here from the previous workflow location',
   Sequence int,   
   DisplayOnlyWhenAvailable boolean DEFAULT FALSE COMMENT 'Make the routing location visible on the page only when available. The default value of FALSE will display the location, but it will not be selectable.',   
   PRIMARY KEY (`DelegationAuthorityID`,`ActiontypeID`,`CurrentWorkflowLocationID`,`NextWorkflowLocationID`,`PreviousWorkflowLocationID`),
   INDEX (`DelegationAuthorityID`,`ActiontypeID`,`CurrentWorkflowLocationID`,`NextWorkflowLocationID`,`PreviousWorkflowLocationID`),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
