-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-05-18

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM EventCategory INTO @_ORIG_COUNT;

truncate EventCategory;

INSERT into EventCategory (ID, Description, ShortDescription, InsertTimestamp, InsertUserID) values 
(1,'Regular event','Regular event',CURRENT_TIMESTAMP,20720),
(2,'Published work event','Published work event',CURRENT_TIMESTAMP,20720);

SHOW WARNINGS;
commit;

SELECT count(*) FROM EventCategory INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;