# Author: Rick Hendricks
# Date: 07/24/2009
# This script creates and itializes all tables neccesary to support
# dossier attributes. This script must be executed in the directory containing 
# each of the scripts to be invoked.

source createDossierAttributes.sql;

source createDelegationAuthority.sql;
source insertDelegationAuthority.sql;

source createDelegationAuthorityAttributeCollection.sql;
source insertDelegationAuthorityAttributeCollection.sql;

source createWorkflowLocation.sql;
source insertWorkflowLocation.sql;

source createDossierAttributeType.sql;
source insertDossierAttributeType.sql;

source createDossierAttributeLocation.sql;
source insertDossierAttributeLocation.sql;

source insertDocument.sql;
