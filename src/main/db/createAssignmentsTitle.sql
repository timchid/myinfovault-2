-- Author: Pradeep Kumar Haldiya
-- Created: 2013-09-13

-- Create the main table AssignmentTitles
-- Dependencies: The ActionAssignment table must exist.

DROP TABLE IF EXISTS AssignmentTitles;

CREATE TABLE AssignmentTitles
(
   ID INT PRIMARY KEY auto_increment,
   AssignmentID INT NOT NULL,
   RankTitle VARCHAR(225) NOT NULL,
   Step DECIMAL(3,1) NULL DEFAULT NULL,
   TitleCode VARCHAR(6) NOT NULL,
   PercentageOfTime DECIMAL(4,3) not NULL,
   WithoutSalary BOOLEAN NOT NULL DEFAULT FALSE,
   AppointmentType ENUM('NA','NINE_OVER_NINE','NINE_OVER_TWELVE','ELEVEN_OVER_TWELVE','TWELVE_OVER_TWELVE') DEFAULT NULL,
   SalaryType ENUM ('HOURLY','MONTHLY') DEFAULT NULL,
   Salary DECIMAL(9,2),
   AnnualSalary INT NULL DEFAULT NULL,
   YearsAtRank TINYINT NOT NULL DEFAULT 0,
   YearsAtStep TINYINT NOT NULL DEFAULT 0,
   InsertTimestamp TIMESTAMP DEFAULT current_timestamp,
   InsertUserID INT NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID INT,
   KEY `fk_assignmenttitles_actionassignment_id` (`AssignmentID`),
   CONSTRAINT `fk_assignmenttitles_actionassignment_id` FOREIGN KEY (`AssignmentID`) 
   REFERENCES `ActionAssignment` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
)
engine = InnoDB
default character set utf8;
