-- Author: Craig Gilmore
-- Created: 2012-12-7

-- Canidate name is used in RAF document representations; must be preserved in case name is changed in whitepages
ALTER TABLE `RAF_Forms` ADD COLUMN `CandidateName` VARCHAR(255) NOT NULL;

-- Initially set RAFs to dossier candidate's user account preferred name
UPDATE RAF_Forms r
LEFT JOIN Dossier d ON r.DossierID=d.DossierID
LEFT JOIN UserAccount u ON d.UserID=u.UserID
SET r.CandidateName=u.PreferredName
WHERE r.CandidateName='';
