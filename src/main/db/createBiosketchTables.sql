SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `myinfovault` ;
USE `myinfovault`;

-- -----------------------------------------------------
-- Table `myinfovault`.`Fonts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`Fonts` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`Fonts` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(50) NOT NULL COMMENT 'Displayed name of font' ,
  `Font` VARCHAR(100) NOT NULL COMMENT 'Actual font name used in rendering' ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `myinfovault`.`SystemCitationFormat`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`SystemCitationFormat` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`SystemCitationFormat` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(50) NOT NULL COMMENT 'Name of the format, such as MLA, APA, Chicago Manual of Style' ,
  `Implementation` VARCHAR(255) NOT NULL COMMENT 'Location of XSLT files that implement this format' ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Specify the format for citations';

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchStyle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchStyle` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchStyle` (
  `ID` INT NOT NULL AUTO_INCREMENT COMMENT 'System Style -(CollectionID) is default for that Collection' ,
  `UserID` INT NOT NULL DEFAULT 0 COMMENT 'A zero (0) UserID indicates a System Style' ,
  `StyleName` VARCHAR(50) NULL ,
  `PageWidth` INT NOT NULL DEFAULT 612 COMMENT '8.5 inches * 72 points-per-inch' ,
  `PageHeight` INT NOT NULL DEFAULT 792 COMMENT '11 inches * 72 points-per-inch' ,
  `MarginTop` INT NOT NULL DEFAULT 36 COMMENT '0.5 inches * 72 points-per-inch' ,
  `MarginRight` INT NOT NULL DEFAULT 54 COMMENT '0.75 inches * 72 points-per-inch' ,
  `MarginBottom` INT NOT NULL DEFAULT 36 COMMENT '0.5 inches * 72 points-per-inch' ,
  `MarginLeft` INT NOT NULL DEFAULT 54 COMMENT '0.75 inches * 72 points-per-inch' ,
  `TitleFontID` INT NOT NULL DEFAULT 1 COMMENT 'Use the default (#1) font' ,
  `TitleSize` INT NOT NULL DEFAULT 20 ,
  `TitleAlign` ENUM('LEFT','CENTER','RIGHT') NOT NULL DEFAULT 'CENTER' ,
  `TitleBold` BOOLEAN NOT NULL DEFAULT true ,
  `TitleItalic` BOOLEAN NOT NULL DEFAULT false ,
  `TitleUnderline` BOOLEAN NOT NULL DEFAULT false ,
  `TitleRules` BOOLEAN NOT NULL DEFAULT true COMMENT 'Use horizontal rules above/below title' ,
  `TitleEditable` BOOLEAN NOT NULL DEFAULT true ,
  `DisplaynameAlign` ENUM('LEFT','CENTER','RIGHT') NOT NULL DEFAULT 'CENTER' ,
  `HeaderFontID` INT NOT NULL DEFAULT 1 COMMENT 'Use the default (#1) font' ,
  `HeaderSize` INT NOT NULL DEFAULT 14 COMMENT 'Point size' ,
  `HeaderAlign` ENUM('LEFT','CENTER') NOT NULL DEFAULT 'LEFT' ,
  `HeaderBold` BOOLEAN NOT NULL DEFAULT true ,
  `HeaderItalic` BOOLEAN NOT NULL DEFAULT false ,
  `HeaderUnderline` BOOLEAN NOT NULL DEFAULT false ,
  `HeaderIndent` INT NOT NULL DEFAULT 0 ,
  `BodyFontID` INT NOT NULL DEFAULT 1 COMMENT 'Use the default (#1) font' ,
  `BodySize` INT NOT NULL DEFAULT 11 COMMENT 'Point size' ,
  `BodyIndent` INT NOT NULL DEFAULT 36 COMMENT '0.5 inches * 72 points-per-inch' ,
  `BodyFormatting` BOOLEAN NOT NULL DEFAULT true COMMENT 'Use the \"Format Options\" when producing output.' ,
  `FooterOn` BOOLEAN NOT NULL DEFAULT true ,
  `FooterPageNumbers` BOOLEAN NOT NULL DEFAULT true ,
  `FooterRules` BOOLEAN NOT NULL DEFAULT true COMMENT 'Use horizontal rule above footer' ,
  `CitationFormatID` INT NOT NULL DEFAULT 1 COMMENT 'Use the default (#1) citation format' ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX fk_style_headerfont (`HeaderFontID` ASC) ,
  INDEX fk_style_bodyfont (`BodyFontID` ASC) ,
  INDEX UserID (`UserID` ASC) ,
  INDEX fk_style_titlefont (`TitleFontID` ASC) ,
  INDEX fk_style_citations (`CitationFormatID` ASC) ,
  CONSTRAINT `fk_style_headerfont`
    FOREIGN KEY (`HeaderFontID` )
    REFERENCES `myinfovault`.`Fonts` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_style_bodyfont`
    FOREIGN KEY (`BodyFontID` )
    REFERENCES `myinfovault`.`Fonts` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_style_titlefont`
    FOREIGN KEY (`TitleFontID` )
    REFERENCES `myinfovault`.`Fonts` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_style_citations`
    FOREIGN KEY (`CitationFormatID` )
    REFERENCES `myinfovault`.`SystemCitationFormat` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Sizes are in points - 1/72 of an inch';

-- -----------------------------------------------------
-- Table `myinfovault`.`Biosketch`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`Biosketch` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`Biosketch` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `UserID` INT NOT NULL ,
  `Name` VARCHAR(255) NOT NULL COMMENT 'Name given to a biosketch' ,
  `Title` VARCHAR(50) NULL COMMENT 'Title line to appear _in_ the biosketch' ,
  `BiosketchType` INT NOT NULL COMMENT 'Indicates the type of this biosketch, by referencing one of the known Collection types.' ,
  `StyleID` INT NOT NULL DEFAULT 1 COMMENT 'Negative numbers indicate a System Style' ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX fk_biosketch_style (`StyleID` ASC) ,
  INDEX fk_biosketch_collection (`BiosketchType` ASC) ,
  CONSTRAINT `fk_biosketch_style`
    FOREIGN KEY (`StyleID` )
    REFERENCES `myinfovault`.`BiosketchStyle` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_biosketch_collection`
    FOREIGN KEY (`BiosketchType` )
    REFERENCES `myinfovault`.`Collection` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchExclude`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchExclude` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchExclude` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `UserID` INT NOT NULL ,
  `BiosketchID` INT NOT NULL ,
  `RecType` VARCHAR(255) NOT NULL ,
  `RecordID` INT NOT NULL ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX User (`UserID` ASC) ,
  INDEX fk_exclude_biosketch (`BiosketchID` ASC) ,
  CONSTRAINT `fk_exclude_biosketch`
    FOREIGN KEY (`BiosketchID` )
    REFERENCES `myinfovault`.`Biosketch` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchFormatLimits`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchFormatLimits` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchFormatLimits` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `BiosketchType` INT NOT NULL COMMENT 'From the Collection table' ,
  `PageWidthMin` INT NOT NULL ,
  `PageWidthMax` INT NOT NULL ,
  `PageHeightMin` INT NOT NULL ,
  `PageHeightMax` INT NOT NULL ,
  `MarginMin` INT NOT NULL ,
  `MarginMax` INT NOT NULL ,
  `HeaderSizeMin` INT NOT NULL ,
  `HeaderSizeMax` INT NOT NULL ,
  `HeaderIndentMin` INT NOT NULL ,
  `HeaderIndentMax` INT NOT NULL ,
  `BodySizeMin` INT NOT NULL ,
  `BodySizeMax` INT NOT NULL ,
  `BodyIndentMin` INT NOT NULL ,
  `BodyIndentMax` INT NOT NULL ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX fk_limits_collection (`BiosketchType` ASC) ,
  CONSTRAINT `fk_limits_collection`
    FOREIGN KEY (`BiosketchType` )
    REFERENCES `myinfovault`.`Collection` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'One table entry for each Type of biosketch';

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchSectionLimits`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchSectionLimits` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchSectionLimits` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `BiosketchType` INT NOT NULL ,
  `SectionID` INT NOT NULL ,
  `SectionName` VARCHAR(50) NOT NULL ,
  `Availability` ENUM('MANDATORY','OPTIONAL_ON','OPTIONAL_OFF','PROHIBITED') NOT NULL ,
  `IsMainSection` BOOLEAN NOT NULL ,
  `InSelectList` BOOLEAN NOT NULL ,
  `MayHideHeader` BOOLEAN NOT NULL COMMENT 'Is user allowed to turn header off?' ,
  `DisplayInBiosketch` INT NOT NULL DEFAULT 0 COMMENT 'This is PublicationType.ID if not 0' ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX fk_biosketch_limits_type (`BiosketchType` ASC) ,
  CONSTRAINT `fk_biosketch_limits_type`
    FOREIGN KEY (`BiosketchType` )
    REFERENCES `myinfovault`.`BiosketchFormatLimits` (`BiosketchType` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchRuleset`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchRuleset` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchRuleset` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `UserID` INT NOT NULL ,
  `BiosketchID` INT NOT NULL ,
  `RecType` VARCHAR(255) NULL ,
  `Operator` ENUM('AND','OR') NOT NULL DEFAULT 'AND' ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX fk_limits_biosketch (`BiosketchID` ASC) ,
  CONSTRAINT `fk_limits_biosketch`
    FOREIGN KEY (`BiosketchID` )
    REFERENCES `myinfovault`.`Biosketch` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchCriteria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchCriteria` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchCriteria` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `RulesetID` INT NOT NULL ,
  `Field` VARCHAR(45) NOT NULL ,
  `Comparison` ENUM('LT','LE','EQ','GE','GT') NOT NULL ,
  `Value` VARCHAR(45) NOT NULL ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX fk_criteria_ruleset (`RulesetID` ASC) ,
  CONSTRAINT `fk_criteria_ruleset`
    FOREIGN KEY (`RulesetID` )
    REFERENCES `myinfovault`.`BiosketchRuleset` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `myinfovault`.`PaperSize`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`PaperSize` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`PaperSize` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(20) NOT NULL ,
  `Description` VARCHAR(45) NOT NULL ,
  `Width` INT NOT NULL COMMENT 'Width in Points (1/72 of an inch)' ,
  `Height` INT NOT NULL COMMENT 'Height in Points (1/72 of an inch)' ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchSection`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchSection` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchSection` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `BiosketchID` INT NOT NULL ,
  `SectionID` INT NOT NULL ,
  `SectionName` VARCHAR(50) NOT NULL ,
  `Availability` ENUM('MANDATORY','OPTIONAL_ON','OPTIONAL_OFF','PROHIBITED') NOT NULL ,
  `display` BOOLEAN NOT NULL ,
  `displayHeader` BOOLEAN NOT NULL DEFAULT true ,
  `IsMainSection` BOOLEAN NOT NULL DEFAULT true ,
  `InSelectList` BOOLEAN NOT NULL DEFAULT true ,
  `MayHideHeader` BOOLEAN NOT NULL DEFAULT true COMMENT 'Is user allowed to turn header off?' ,
  `DisplayInBiosketch` INT NOT NULL DEFAULT 0 ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX BiosketchID (`BiosketchID` ASC) ,
  CONSTRAINT `BiosketchID`
    FOREIGN KEY (`BiosketchID` )
    REFERENCES `myinfovault`.`Biosketch` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'One entry from each user selected Section';

-- -----------------------------------------------------
-- Table `myinfovault`.`BiosketchAttributes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`BiosketchAttributes` ;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`BiosketchAttributes` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `BiosketchID` INT NOT NULL ,
  `Name` VARCHAR(30) NOT NULL ,
  `Value` TEXT NULL ,
  `Sequence` INT NOT NULL ,
  `Display` BOOLEAN NOT NULL DEFAULT 1 ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`) ,
  INDEX fk_attrib_biosketch (`BiosketchID` ASC) ,
  CONSTRAINT `fk_attrib_biosketch`
    FOREIGN KEY (`BiosketchID` )
    REFERENCES `myinfovault`.`Biosketch` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
