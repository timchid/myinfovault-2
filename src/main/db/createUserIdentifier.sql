# Author: Lawrence Fyfe
# Date: 10/24/2006

create table UserIdentifier
(
   ID int primary key auto_increment,
   UserID int,
   IdentifierID int,
   Value varchar(50),
   Label varchar(50),
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
