DELIMITER $$
DROP PROCEDURE if EXISTS fixDecisions$$
CREATE PROCEDURE fixDecisions (IN school INT, IN dept INT)
BEGIN
    DECLARE eoc, cnt, dsid INT;
    DECLARE cur1 CURSOR FOR SELECT distinct DossierID FROM DocumentSignature where SchoolID=school and DepartmentID=dept order by DossierID;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET eoc = 1;
    OPEN cur1;
    
    SET eoc = 0;
    SET cnt = 0;
    SELECT concat("Processing school: ", school);
    SELECT concat("Processing department: ", dept);

    WHILE eoc = 0 DO
        FETCH cur1 INTO dsid;
        IF eoc = 0 THEN
        SET cnt = cnt + 1;
		update DocumentSignature as ds, Dossier as d set ds.SchoolID=d.PrimarySchoolID , ds.DepartmentID=d.PrimaryDepartmentID
			 where ds.DossierID=d.dossierid and ds.DossierID=dsid;
    END IF;
    END WHILE;
    
    CLOSE cur1;
    SELECT concat("dossiers processed ", cnt);

END
$$
DELIMITER ;