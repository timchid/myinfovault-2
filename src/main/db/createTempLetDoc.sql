# Author: LEO GOLUBEV
# Date: 06/30/2009

CREATE TABLE TempOtherLetters
(
   ID int primary key auto_increment,
   DocID int,
   UploadID int unique,
   CONSTRAINT `fk_doc_upload`
    FOREIGN KEY (`UploadID` )
    REFERENCES `UserUploadDocument` (`ID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
engine = InnoDB
default character set utf8;
