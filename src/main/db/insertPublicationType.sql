# Author: Lawrence Fyfe
# Date: 10/11/2006

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM PublicationType INTO @_ORIG_COUNT;

truncate PublicationType;

insert into PublicationType (ID,Description,InsertUserID)
values
(1,'Abstract',14021),
(2,'Journal',14021),
(3,'Book Authored',14021),
(4,'Book Edited',14021),
(5,'Book Chapter',14021),
(6,'Book Reviewed',14021),
(7,'Letter to the Editor',14021),
(8,'Limited Distribution',14021),
(9,'Alternative Media',14021);

SHOW WARNINGS;
commit;

SELECT count(*) FROM PublicationType INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
