-- Author: Craig Gilmore
-- Created: 2011-07-06

-- Table updates for v3.9.1.

ALTER TABLE `PublicationSummary` 
 MODIFY COLUMN `ExternalSourceID` tinyint(1) NOT NULL DEFAULT 0;

source AlterDB_v3.9.1.sql

