-- Author: Stephen Paulsen
-- Created: 2009-10-02
-- Updated: 2009-10-04

-- Fix login columns in CvMem and UserLogin based on Section 1 of the
-- "login ID cleanup.xls" spreadsheet attached to MIV-2745
-- Dependencies: uses the fix_user_login stored procedure defined in "defineFixLogin.sql"


-- -- Section 1 of the Spreadsheet -- --

-- Login: hto  MothraID: 227822,
call fix_user_login(11843,'hue.to','hto');

-- Login: ihp  MothraID: 197846,
call fix_user_login(13109,'irva.hertz-picciotto','ihp');

-- Login: sohess  MothraID: 352280,
call fix_user_login(19036,'syhess','sohess');

-- Login: fzparson  MothraID: 2206,
call fix_user_login(1555,'gibbe.parsons','fzparson');

-- Login: szgando  MothraID: 2726,
call fix_user_login(2251,'regina.gandour-edwards','szgando');

-- Login: syasmeen  MothraID: 16959,
call fix_user_login(2302,'shagufta.yasmeen','syasmeen');

-- Login: labecket  MothraID: 172460,
call fix_user_login(2710,'laurel.beckett','labecket');

-- Login: ekret  MothraID: 166409,
call fix_user_login(3045,'elena.kret-sudjian','ekret');

-- Login: rsteinw  MothraID: 170404,
call fix_user_login(4430,'rebecca.stein-wexler','rsteinw');

-- Login: ttaoki  MothraID: 2001,
call fix_user_login(4575,'thomas.aoki','ttaoki');

-- Login: mhaight  MothraID: 3505,
call fix_user_login(5483,'michael.haight','mhaight');

-- Login: ifeinber  MothraID: 1387,
call fix_user_login(5779,'irwin.feinberg','ifeinber');

-- Login: wpthomas  MothraID: 2731,
call fix_user_login(5809,'william.thomas','wpthomas');

-- Login: dmhyde  MothraID: 2432,
call fix_user_login(5813,'dallas.hyde','dmhyde');

-- Login: latell  MothraID: 4589,
call fix_user_login(5939,'lisa.tell','latell');

-- Login: drtorres  MothraID: 4158,
call fix_user_login(6002,'jose.torres','drtorres');

-- Login: fzrabino  MothraID: 1587,
call fix_user_login(6317,'lawrence.rabinowitz','fzrabino');

-- Login: pjihrke  MothraID: 2376,
call fix_user_login(6662,'peter.ihrke','pjihrke');

-- Login: fecurry  MothraID: 2650,
call fix_user_login(7118,'fitz-roy.curry','fecurry');

-- Login: cjkoyama  MothraID: 27437,
call fix_user_login(7358,'candace.sadorra','cjkoyama');

-- 7389,lavjay.butani,lbutani: lavjay.butani,132549,
call fix_user_login(7389,'lavjay.butani','lbutani');


-- Login: meodonne  MothraID: 3299,
call fix_user_login(7809,'martha.o''donnell','meodonne');

-- Login: amsterda  MothraID: 1720,
call fix_user_login(8129,'ezra.amsterdam','amsterda');

-- 8149,tim.tautz,tautz: timothy.tautz,22711,
DELETE FROM UserLogin WHERE UserID=8149 AND Login='tim.tautz';
call fix_user_login(8149,'tim.tautz','timothy.tautz');
call fix_user_login(8149,'timothy.tautz','tautz');


-- Login: jeannett  MothraID: 204822,
call fix_user_login(8150,'jeannette.tom-lerman','jeannett');

-- Login: gandhi  MothraID: 172708,
call fix_user_login(9196,'mehul.gandhi','gandhi');

-- Login: nmboe  MothraID: 4036,
call fix_user_login(9922,'nina.boe','nmboe');

-- Login: dcassady  MothraID: 132080,
call fix_user_login(9977,'diana.cassady','dcassady');

-- Login: gmelendr  MothraID: 165759,
call fix_user_login(10071,'giselle.melendres','gmelendr');

-- Login: teague  MothraID: 13107,
call fix_user_login(10118,'judy.teague-elliott','teague');

-- Login: shlo  MothraID: 4385,
call fix_user_login(10187,'su hao.lo','shlo');

-- Login: cherie  MothraID: 16858,
call fix_user_login(10234,'cherie.felsch','cherie');

-- Login: pabarath  MothraID: 150020,
call fix_user_login(10354,'penny.barath','pabarath');

-- Login: drbenson  MothraID: 1941,
call fix_user_login(10374,'daniel.benson','drbenson');

-- Login: xli  MothraID: 203308,
call fix_user_login(10521,'xiao-dong.li','xli');

-- Login: hjwenzel  MothraID: 178883,
call fix_user_login(10874,'hans jurgen.wenzel','hjwenzel');

-- Login: letoile  MothraID: 193385,
call fix_user_login(10893,'noelle.l''etoile','letoile');

-- Login: clalberi  MothraID: 12024,
call fix_user_login(11068,'cheri.albericci','clalberi');

-- Login: abramsl  MothraID: 25469,
call fix_user_login(11178,'lisa.abramson','abramsl');

-- Login: nhagiwar  MothraID: 220355,
call fix_user_login(11202,'hagiwara.nobuko','nhagiwar');

-- Login: lwaniaga  MothraID: 241470,
call fix_user_login(11256,'lana.wania-galicia','lwaniaga');

-- Login: saroufee  MothraID: 16295,
call fix_user_login(11744,'ramez.saroufeem','saroufee');

-- Login: mchriste  MothraID: 245659,
call fix_user_login(11819,'melanie.christensen','mchriste');

-- Login: swarrena  MothraID: 8536,
call fix_user_login(11822,'susan.warren-alef','swarrena');

-- Login: rklow  MothraID: 4227,
call fix_user_login(11823,'roger.low','rklow');

-- Login: kjzbylut  MothraID: 245825,
call fix_user_login(11846,'kristina.zbylut','kjzbylut');

-- Login: szgrave  MothraID: 19038,
call fix_user_login(11874,'richard.graves','szgrave');

-- Login: cych  MothraID: 20022,
call fix_user_login(11878,'chao-yin.chen','cych');

-- Login: rychuang  MothraID: 1973,
call fix_user_login(11920,'ronald.chuang','rychuang');

-- Login: bpitts  MothraID: 218621,
call fix_user_login(12066,'brian.pitts','bpitts');

-- Login: venezuel  MothraID: 221362,
call fix_user_login(12120,'juan.garcia','venezuel');

-- 12178,tamas.vidovszky,tjv: tvidovszky,246416,
DELETE FROM UserLogin WHERE UserID=12178 AND Login='tamas.vidovszky';
call fix_user_login(12178,'tamas.vidovszky','tjv');
call fix_user_login(12178,'tjv','tvidovszky');


-- Login: aklaviol  MothraID: 9548,
call fix_user_login(12523,'anita.la violette','aklaviol');

-- 12724,rosina.bruno,rosina: rosina.robinson,11647,
DELETE FROM UserLogin WHERE UserID=12724 AND Login='rosina.bruno';
call fix_user_login(12724,'rosina.bruno','rosina.robinson');
call fix_user_login(12724,'rosina.robinson','rosina');


-- Login: fzhjel  MothraID: 2797,
call fix_user_login(12821,'leonard m.hjelmeland','fzhjel');

-- Login: kreutzer  MothraID: 3860,
call fix_user_login(12824,'ulrike m.kreutzer','kreutzer');

-- Login: fzsaxton  MothraID: 2687,
call fix_user_login(12825,'michael j.saxton','fzsaxton');

-- Login: zeljka  MothraID: 3817,
call fix_user_login(12827,'zeljka s.mcbride','zeljka');

-- Login: sztepper  MothraID: 20010,
call fix_user_login(12830,'clifford g.tepper','sztepper');

-- Login: lsvanwin  MothraID: 4469,
call fix_user_login(12887,'laura.van winkle','lsvanwin');

-- 12889,michelle.lim,mclim: mlim,169725,
DELETE FROM UserLogin WHERE UserID=12889 AND Login='michelle.lim';
call fix_user_login(12889,'michelle.lim','mlim');
call fix_user_login(12889,'mlim','mclim');


-- Login: ebstrong  MothraID: 20815,
call fix_user_login(12898,'edward bradley.strong','ebstrong');

-- Login: fzbruss  MothraID: 2421,
call fix_user_login(13035,'michael.bruss','fzbruss');

-- Login: sutingli  MothraID: 246132,
call fix_user_login(13217,'su-ting.li','sutingli');

-- Login: jswerner  MothraID: 150240,
call fix_user_login(13276,'john s..werner','jswerner');

-- Login: sbbaum  MothraID: 293110,
call fix_user_login(13294,'sandra.baum','sbbaum');

-- Login: clyang  MothraID: 223529,
call fix_user_login(13380,'claus.yang','clyang');

-- 13417,susanne.miyamoto,smiyamot: suzanne.miyamoto,71129,
DELETE FROM UserLogin WHERE UserID=13417 AND Login='susanne.miyamoto';
call fix_user_login(13417,'susanne.miyamoto','suzanne.miyamoto');
call fix_user_login(13417,'suzanne.miyamoto','smiyamot');


-- Login: ag1935  MothraID: 1689,
call fix_user_login(13993,'adam.greenspan','ag1935');

-- Login: mflee  MothraID: 24091,
call fix_user_login(14329,'michellef.lee','mflee');

-- Login: fjgarcia  MothraID: 21914,
call fix_user_login(14340,'francisco.garcia-ferrer','fjgarcia');

-- Login: johnd  MothraID: 4557,
call fix_user_login(14600,'John','johnd');

-- Login: trobrien  MothraID: 1955,
call fix_user_login(14748,'timothy.o','trobrien');

-- Login: kgmagdes  MothraID: 4671,
call fix_user_login(14800,'k.magdesian','kgmagdes');

-- Login: ernesto  MothraID: 22808,
call fix_user_login(14808,'nicola.pusterla','ernesto');

-- Login: calvert  MothraID: 3134,
call fix_user_login(14851,'christopher.calvert','calvert');

-- Login: depeters  MothraID: 3188,
call fix_user_login(14856,'edward.depeters','depeters');

-- Login: freebird  MothraID: 3654,
call fix_user_login(14861,'thomas.famula','freebird');

-- Login: yblee  MothraID: 2130,
call fix_user_login(14866,'yu-bang.lee','yblee');

-- Login: bfreeman  MothraID: 4697,
call fix_user_login(14918,'britt.burton-freeman','bfreeman');

-- Login: wchawkes  MothraID: 72438,
call fix_user_login(14930,'wayne (chris).hawkes','wchawkes');

-- Login: abjoy  MothraID: 3317,
call fix_user_login(14934,'amy block.joy','abjoy');

-- Login: uriuadam  MothraID: 3745,
call fix_user_login(14954,'janet.uriu-adams','uriuadam');

-- Login: fzzidenb  MothraID: 3873,
call fix_user_login(14957,'sheri.zidenberg-cherr','fzzidenb');

-- Login: tacahill  MothraID: 1784,
call fix_user_login(15605,'thomas.cahill','tacahill');

-- Login: jdmomsen  MothraID: 1894,
call fix_user_login(15720,'janet.momsen','jdmomsen');

-- Login: ontaigrz  MothraID: 222241,
call fix_user_login(15722,'lenna.ontai-grzebik','ontaigrz');

-- Login: rabermud  MothraID: 289350,
call fix_user_login(15847,'richard.bermudes','rabermud');

-- Login: jimeno  MothraID: 15839,
call fix_user_login(15961,'ctjones','jimeno');

-- Login: twhite  MothraID: 299834,
call fix_user_login(16564,'tfwhite','twhite');

-- Login: fzdugdal  MothraID: 2523,
call fix_user_login(16566,'ssdugdale','fzdugdal');

-- Login: fzfiguer  MothraID: 2104,
call fix_user_login(16567,'refigueroa','fzfiguer');

-- Login: peheckma  MothraID: 297976,
call fix_user_login(16570,'peheckman','peheckma');

-- Login: jons  MothraID: 2186,
call fix_user_login(16573,'jhsandoval','jons');

-- Login: fzwatson  MothraID: 2168,
call fix_user_login(16575,'kawatsongegeo','fzwatson');

-- Login: cscarter  MothraID: 245677,
call fix_user_login(16771,'ccarter','cscarter');

-- Login: muell  MothraID: 3699,
call fix_user_login(16804,'mueller','muell');

-- Login: yyeh  MothraID: 1887,
call fix_user_login(16805,'yinyeh','yyeh');

-- Login: mdill  MothraID: 327243,
call fix_user_login(16811,'mick.dill','mdill');

-- Login: choudary  MothraID: 2862,
call fix_user_login(16823,'pvchoudary','choudary');

-- Login: chiang  MothraID: 3766,
call fix_user_login(16882,'schiang','chiang');

-- Login: jjhubbar  MothraID: 350192,
call fix_user_login(17261,'jhubbard','jjhubbar');

-- Login: smtheg  MothraID: 3557,
call fix_user_login(17292,'steve','smtheg');

-- Login: debpaul  MothraID: 352535,
call fix_user_login(17321,'dpaul','debpaul');

-- Login: fzkowal  MothraID: 3063,
call fix_user_login(17339,'Stephen','fzkowal');

-- Login: ldt  MothraID: 351586,Multiple accounts
call fix_user_login(17353,'ltaylor','ldt');

-- Login: kmcall  MothraID: 148621,
call fix_user_login(17462,'a.kimberly.mcallister','kmcall');

-- Login: jclagari  MothraID: 3422,
call fix_user_login(17504,'jclagarias','jclagari');

-- Login: ez075053  MothraID: 37813,
call fix_user_login(17509,'p.pesavento','ez075053');

-- Login: rlrodrig  MothraID: 2669,
call fix_user_login(17522,'rlrodriguez','rlrodrig');

-- Login: szmathai  MothraID: 11778,
call fix_user_login(17552,'mathew','szmathai');

-- Login: rstaylor  MothraID: 296881,
call fix_user_login(17585,'robert.taylor','rstaylor');

-- Login: richman  MothraID: 2258,
call fix_user_login(17654,'david.richman','richman');

-- Login: wheelock  MothraID: 17220,
call fix_user_login(17655,'vicki.wheelock','wheelock');

-- Login: mdzhang  MothraID: 195584,
call fix_user_login(17656,'lin.zhang','mdzhang');

-- Login: pashwood  MothraID: 241340,
call fix_user_login(17657,'ashwo1','pashwood');

-- Login: rmtsolis  MothraID: 344554,
call fix_user_login(17661,'tsoli8','rmtsolis');

-- Login: aorodrig  MothraID: 247546,
call fix_user_login(17670,'anne.rodriguez','aorodrig');

-- Login: vjstewar  MothraID: 3632,
call fix_user_login(17694,'Stewart','vjstewar');

-- Login: syyuan  MothraID: 308211,
call fix_user_login(17696,'sarahyuan','syyuan');

-- Login: jrroth  MothraID: 221001,
call fix_user_login(17702,'Roth','jrroth');

-- Login: holzhao  MothraID: 298204,
call fix_user_login(17703,'Holly Zhao','holzhao');

-- Login: pmrhan  MothraID: 353176,
call fix_user_login(17704,'Jay Han','pmrhan');

-- Login: csshin  MothraID: 44342,
call fix_user_login(17705,'chris shin','csshin');

-- Login: dhbennet  MothraID: 327254,
call fix_user_login(17733,'dhbennett','dhbennet');

-- Login: jjlamber  MothraID: 246762,
call fix_user_login(17936,'jjlamber','jjlamber');

-- Login: bewolfe  MothraID: 403969,
call fix_user_login(18071,'bmrivera','bewolfe');

-- Login: eyalog  MothraID: 4656,
call fix_user_login(18581,'eyalog','eyalog');

-- Login: fzephyr  MothraID: 2185,
call fix_user_login(18696,'milton','fzephyr');

-- Login: shadams  MothraID: 400230,
call fix_user_login(19031,'sean.h.adams','shadams');

-- 19034,pjhavel,peter.havel: szphavel,3752,
DELETE FROM UserLogin WHERE UserID=19034 AND Login='pjhavel';
call fix_user_login(19034,'pjhavel','peter.havel');
call fix_user_login(19034,'peter.havel','szphavel');


-- Login: cthaiss  MothraID: 407255,
call fix_user_login(19043,'cjthaiss','cthaiss');

-- Login: fzcolomb  MothraID: 3674,Multiple accounts
call fix_user_login(19401,'ccolombi','fzcolomb');

-- Login: yongluo  MothraID: 132065,
call fix_user_login(19050,'yongluo','yongluo');

-- Login: milburn  MothraID: 351610,
call fix_user_login(19079,'cnmilburn','milburn');

-- Login: ajcornel  MothraID: 4298,
call fix_user_login(19185,'cornel','ajcornel');

-- Login: fzrhetor  MothraID: 2711,
call fix_user_login(19190,'dpabbott','fzrhetor');

-- Login: szcricri  MothraID: 23005,
call fix_user_login(19226,'chmorisseau','szcricri');

-- 19227,mpparrella,michael.parrella: mpparrel,3185,
DELETE FROM UserLogin WHERE UserID=19227 AND Login='mpparrella';
call fix_user_login(19227,'mpparrella','michael.parrella');
call fix_user_login(19227,'michael.parrella','mpparrel');


-- Login: cgsummer  MothraID: 2111,
call fix_user_login(19232,'chasum','cgsummer');

-- Login: dshatz  MothraID: 536579,
call fix_user_login(19262,'dvshatz','dshatz');

-- Login: bishopmd  MothraID: 145801,
call fix_user_login(19264,'Jwabishop','bishopmd');

-- Login: smarshal  MothraID: 242276,
call fix_user_login(19265,'smarshall','smarshal');

-- Login: fzblake  MothraID: 3089,
call fix_user_login(19267,'rjblake','fzblake');

-- Login: tbradley  MothraID: 190980,
call fix_user_login(19269,'tgbradley','tbradley');

-- Login: fzmarta  MothraID: 3018,
call fix_user_login(19276,'mealtisent','fzmarta');

-- Login: bernucci  MothraID: 242677,
call fix_user_login(19277,'lmbernucci','bernucci');

-- Login: jtenma  MothraID: 596114,
call fix_user_login(19601,'jtenma','jtenma');

-- Login: jijli  MothraID: 596412,
call fix_user_login(19610,'jijli','jijli');

-- Login: nhamilto  MothraID: 596662,
call fix_user_login(19618,'nhamilto','nhamilto');

-- Login: marusa  MothraID: 596418,
call fix_user_login(19620,'marusa','marusa');

-- Login: rejiang  MothraID: 596708,
call fix_user_login(19630,'rejiang','rejiang');

-- Login: ayako  MothraID: 596193,
call fix_user_login(19634,'ayako','ayako');

-- Login: thli  MothraID: 595345,
call fix_user_login(19639,'thli','thli');

-- Login: katehi  MothraID: 595787,
call fix_user_login(19640,'katehi','katehi');



-- -- Section 5 of the Spreadsheet -- --

-- Login: 'mlawless'  MothraID: 17463
call fix_user_login(2127,'mary beth.lawless','mlawless');

-- Login: 'dosully'  MothraID: 8817
call fix_user_login(2293,'darrell.o''sulllivan','dosully');

-- Login: 'jlmelend'  MothraID: 21111
call fix_user_login(3794,'joseph.melendres','jlmelend');

-- Login: 'bobman'  MothraID: 168948
call fix_user_login(3867,'robert.villanueva','bobman');

-- Login: 'armichel'  MothraID: 16339
call fix_user_login(4645,'angela.michelier','armichel');

-- Login: 'mcardiff'  MothraID: 6906
call fix_user_login(4886,'mary.cardiff','mcardiff');

-- Login: 'skmann'  MothraID: 8363
call fix_user_login(6795,'surinder.mann','skmann');

-- Login: 'klgeist'  MothraID: 14644
call fix_user_login(7347,'kerry.geist','klgeist');

-- Login: 'magic'  MothraID: 13768
call fix_user_login(7765,'sharon.schauer','magic');

-- Login: 'yogurt'  MothraID: 6460
call fix_user_login(7769,'judy.wolf','yogurt');

-- Login: 'ptroia'  MothraID: 168280
call fix_user_login(7850,'paolo.troia-cancio','ptroia');

-- Login: 'fztrudea'  MothraID: 1486
call fix_user_login(9273,'walter.trudeau','fztrudea');

-- Login: 'bingell'  MothraID: 166376
call fix_user_login(9404,'betty.ingell','bingell');

-- Login: 'creid'  MothraID: 203871
call fix_user_login(9582,'chandra.reid','creid');

-- Login: 'pamtise'  MothraID: 191588
call fix_user_login(9721,'tisep','pamtise');

-- Login: 'rhildret'  MothraID: 24579
call fix_user_login(9828,'rayanne.hildreth','rhildret');

-- Login: 'szrkorte'  MothraID: 11667
call fix_user_login(10119,'renee.korte','szrkorte');

-- Login: 'amueller'  MothraID: 8622
call fix_user_login(10120,'adeline.mueller','amueller');

-- Login: 'kacurley'  MothraID: 228680
call fix_user_login(10189,'karen.curley','kacurley');

-- Login: 'lmmadden'  MothraID: 25823
call fix_user_login(10202,'lisa.madden','lmmadden');

-- Login: 'jlgastin'  MothraID: 167941
call fix_user_login(10207,'jennifer.gastineau','jlgastin');

-- Login: 'szlalove'  MothraID: 12579
call fix_user_login(10208,'laurel.love','szlalove');

-- Login: 'tlow'  MothraID: 14560
call fix_user_login(10267,'teri.hernandez','tlow');

-- Login: 'mcarvidi'  MothraID: 192475
call fix_user_login(10351,'marie.carvidi','mcarvidi');

-- Login: 'tlcatron'  MothraID: 70676
call fix_user_login(10357,'terry.catron-dodkins','tlcatron');

-- Login: 'fingers'  MothraID: 20425
call fix_user_login(10362,'nikki.phipps','fingers');

-- Login: 'nosuals'  MothraID: 19450
call fix_user_login(10396,'daniel.slauson','nosuals');

-- Login: 'cpage'  MothraID: 6481
call fix_user_login(10406,'charlotte.page','cpage');

-- Login: 'cthorman'  MothraID: 202711
call fix_user_login(10408,'caillouet.thorman','cthorman');

-- Login: 'sjdurant'  MothraID: 174442
call fix_user_login(10421,'susan.durant','sjdurant');

-- Login: 'sfbahati'  MothraID: 12423
call fix_user_login(10458,'shani.bahati','sfbahati');

-- Login: 'gsalisbu'  MothraID: 20326
call fix_user_login(10459,'gayle.salisbury','gsalisbu');

-- Login: 'jeffreyd'  MothraID: 199327
call fix_user_login(10475,'tonya.lange','jeffreyd');

-- Login: 'szeylew'  MothraID: 20965
call fix_user_login(10497,'eva.lew','szeylew');

-- Login: 'hes'  MothraID: 15808
call fix_user_login(10504,'sandra.higby','hes');

-- Login: 'lhsteven'  MothraID: 21684
call fix_user_login(10827,'lisa.stevenson','lhsteven');

-- Login: 'hparks'  MothraID: 244980
call fix_user_login(11682,'heidi.parks','hparks');

-- Login: 'tmarshal'  MothraID: 18210
call fix_user_login(11821,'tina.marshall','tmarshal');

-- Login: 'cdyunger'  MothraID: 20212
call fix_user_login(11844,'catherine.yunger','cdyunger');

-- Login: 'jhershey'  MothraID: 1631
call fix_user_login(11860,'john.hershey','jhershey');

-- Login: 'gmelcher'  MothraID: 14231
call fix_user_login(12260,'gregory.melcher','gmelcher');

-- Login: 'pbbell'  MothraID: 14761
call fix_user_login(12408,'patrick.bell','pbbell');

-- Login: 'mbiegaj'  MothraID: 240629
call fix_user_login(12481,'mark.biegaj','mbiegaj');

-- Login: 'blchblnd'  MothraID: 165086
call fix_user_login(12655,'jason.huff-cook','blchblnd');

-- Login: 'argonzal'  MothraID: 222543
call fix_user_login(14320,'adriana.gonzalez','argonzal');

-- Login: 'claddagh'  MothraID: 20088
call fix_user_login(14369,'mfrassetto','claddagh');

-- Login: 'bellhorn'  MothraID: 1557
call fix_user_login(14708,'roy.bellhorn','bellhorn');

-- Login: 'mrsclark'  MothraID: 10874
call fix_user_login(15776,'msclark','mrsclark');

-- Login: 'lyeeisbe'  MothraID: 246190
call fix_user_login(15872,'lisa.yee-isbell','lyeeisbe');

-- Login: 'lisasg'  MothraID: 196388
call fix_user_login(17687,'Stenhouse-Gaskin','lisasg');

-- Login: 'lmsmith'  MothraID: 345464
call fix_user_login(17698,'lmsmith','lmsmith');

-- Login: 'llsolis'  MothraID: 237318
call fix_user_login(17904,'llsolis','llsolis');

-- Login: 'tmflores'  MothraID: 72846
call fix_user_login(17984,'tflores','tmflores');

-- Login: 'ez075860'  MothraID: 30040
call fix_user_login(19039,'jwnewman','ez075860');

-- Login: 'gschuste'  MothraID: 296973
call fix_user_login(19040,'gschuster','gschuste');

-- Login: 'clmathie'  MothraID: 475425
call fix_user_login(19056,'cmathieu','clmathie');

-- Login: 'fzcolomb'  MothraID: 3674
call fix_user_login(19271,'cmcolomb','fzcolomb');

-- Login: 'rhijmans'  MothraID: 596372
call fix_user_login(19621,'rhijmans','rhijmans');

-- Login: 'mabedi'  MothraID: 598032
call fix_user_login(19643,'mabedi','mabedi');

-- Login: 'jwhell'  MothraID: 597401
call fix_user_login(19645,'jwhell','jwhell');

-- Login: 'tfmiller'  MothraID: 597904
call fix_user_login(19650,'tfmiller','tfmiller');


