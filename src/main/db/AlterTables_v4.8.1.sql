-- Author: rickh
-- Date: 02/05/2014

-- Table updates for v4.8.1

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- Update roles
source insertMivRole.sql;
source insertDossierView.sql;

-- Update documents for EDMS processing
source createDocument.sql;
source insertDocument.sql;

source insertCollectionDocument.sql;
source insertDossierAttributeLocation.sql;
source insertDelegationAuthorityAttributeCollection.sql;

-- Correct existing Joint Dean's Decision Comments documents to be Joint Dean's Recommendation Comments documents
UPDATE UserUploadDocument SET DocumentId=48 WHERE DocumentId=46;
UPDATE DossierAttributes SET AttributeTypeId=37 where AttributeTypeId=35;

-- Adding WithoutSalary column to AssignmentTitles table
ALTER TABLE `AssignmentTitles` ADD COLUMN `WithoutSalary` BOOLEAN NOT NULL DEFAULT FALSE AFTER `PercentageOfTime`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
