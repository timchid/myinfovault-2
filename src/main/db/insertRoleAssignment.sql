-- Author: Mary Northup
-- Created: 2009-07-06
-- Updated: 2009-07-30

-- Creates roles (RoleAssignment) for all UserAccount rows found
-- based on current CvMem Access Level and Dev Team flag.
-- 1=sys_admin, 11=vpstaff, 2=schoolstaff, 4=deptstaff, 5=depthelper
-- 6=candidate  12=archiveadmin
-- 7=mivuser

DELIMITER |

DROP PROCEDURE IF EXISTS fill_role_assignment|
CREATE PROCEDURE fill_role_assignment( )
   MODIFIES SQL DATA

BEGIN
  DECLARE newrole INT;
  DECLARE u_last_row INT DEFAULT 0;
  DECLARE mivuser_cnt INT DEFAULT 0;
  DECLARE isPrimary INT DEFAULT 0;
  DECLARE commit_cnt INT DEFAULT 0;
  DECLARE u_userID INT;
  DECLARE u_cvlevel CHAR(2);
  DECLARE u_devteam TINYINT(1);
  DECLARE u CURSOR FOR SELECT UserID,CvLevel,DevTeam from UserAccount;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET u_last_row=1;

  SET u_last_row=0;
  OPEN u;
  user_cursor: LOOP
     FETCH u INTO u_userID,u_cvlevel,u_devteam;
     IF u_last_row=1 THEN
        LEAVE user_cursor;
     END IF;
       /* convert the cvlevel to the appropriate role */
     CASE u_cvlevel
        WHEN '1' THEN SET newrole=11;
        WHEN '2' THEN SET newrole=2;
        WHEN '3' THEN SET newrole=4;
        WHEN '3H' THEN SET newrole=5;
        WHEN '4' THEN SET newrole=6;
        ELSE SET newrole=7;
     END CASE;
      /* everyone has only 1 primary role */
     SET isPrimary=1;
      /* if we have a dev team member the newrole = 11 BUT the primary role is 1 */
     IF u_devteam=1 THEN
        INSERT INTO RoleAssignment SET UserID=u_userID,RoleID=1,PrimaryRole=isPrimary,InsertUserID=19012;
        SET isPrimary=0;   /* since we just saved the primary role we turn the flag off */
     END IF;
      /* now store the newrole for everyone */
     INSERT INTO RoleAssignment
        SET UserID=u_userID,RoleID=newrole,PrimaryRole=isPrimary,InsertUserID=19012;
      /* Dev Team and Vice Provost Staff get the archive role */
     IF newrole=11 THEN
        INSERT INTO RoleAssignment SET UserID=u_userID,RoleID=12,PrimaryRole=0,InsertUserID=19012;
     END IF;
      /* everyone get the miv_user role UNLESS we already gave it to you (the CASE default) */
     IF newrole=7 THEN SET mivuser_cnt = mivuser_cnt + 1;
     ELSE
        INSERT INTO RoleAssignment SET UserID=u_userID,RoleID=7,PrimaryRole=0,InsertUserID=19012;
        SET commit_cnt = commit_cnt + 1;
     END IF;
  END LOOP user_cursor;
  CLOSE u;
  SET u_last_row=0;

END;
|

DELIMITER ;

CALL fill_role_assignment;

-- Add the Vice Provost role to Barbara Horwitz
INSERT INTO RoleAssignment (UserID, RoleID, PrimaryRole, InsertUserID) VALUES (12848, 10, false, 18099);

DROP PROCEDURE IF EXISTS fill_role_assignment;
