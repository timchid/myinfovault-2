-- Author: Pradeep K Haldiya
-- Created: 2013-06-19
-- Updated: 

use myinfovault;

-- List all the dossiers with split appointments 
SELECT d.UserID,rf.ID AS `Raf_FormID`,d.DossierID,rfd.DepartmentType 
FROM RAF_Forms rf, Dossier d,RAF_FormDepartments rfd 
WHERE d.WorkflowStatus IN ('E','R') AND rf.DossierID=d.DossierID AND rf.ID=rfd.FormID AND DepartmentType='SPLIT';

-- Delete split departments from non-archived/non-canceled dossiers 
DELETE FROM RAF_FormDepartments WHERE DepartmentType='SPLIT' 
AND FormID in (SELECT rf.ID FROM RAF_Forms rf, Dossier d WHERE d.WorkflowStatus IN ('E','R') AND rf.DossierID=d.DossierID);

-- MIV-4929, removing DOCUMENTUPLOADSIGNATURE from type enum
source insertDossierAttributeType.sql;
ALTER TABLE `DossierAttributeType` CHANGE COLUMN `Type` `Type` ENUM ('DOCUMENTUPLOAD','DOCUMENT','ACTION') NOT null;

-- ****************************************************
-- START: MIV-4924, new dean/vp decision choices
-- ****************************************************
UPDATE RAF_Forms
SET Decision = 'APPROVED'
WHERE Decision = 'RECOMMENDAPPROVAL';

UPDATE RAF_Forms
SET Decision = 'DENIED'
WHERE Decision = 'RECOMMENDDENIAL';

UPDATE RAF_Forms
SET Decision = 'OTHER'
WHERE Decision = 'RECOMMENDOTHER';


UPDATE RAF_FormDepartments
SET Decision = 'APPROVED'
WHERE Decision = 'RECOMMENDAPPROVAL';

UPDATE RAF_FormDepartments
SET Decision = 'DENIED'
WHERE Decision = 'RECOMMENDDENIAL';

UPDATE RAF_FormDepartments
SET Decision = 'OTHER'
WHERE Decision = 'RECOMMENDOTHER';


UPDATE AppealDecision
SET Decision = 'APPROVED'
WHERE Decision = 'RECOMMENDAPPROVAL';

UPDATE AppealDecision
SET Decision = 'DENIED'
WHERE Decision = 'RECOMMENDDENIAL';

UPDATE AppealDecision
SET Decision = 'OTHER'
WHERE Decision = 'RECOMMENDOTHER';


ALTER TABLE `RAF_Forms` CHANGE COLUMN `Decision`
`Decision` enum('APPROVED',
                'DENIED',
                'OTHER',
                'POSITIVE_APPRAISAL',
                'POSITIVE_GUARDED_APPRAISAL',
                'GUARDED_APPRAISAL',
                'GUARDED_NEGATIVE_APPRAISAL',
                'NEGATIVE_APPRAISAL',
                'SATISFACTORY_REVIEW_ADVANCEMENT',
                'SATISFACTORY_REVIEW',
                'UNSATISFACTORY_REVIEW',
                'REAPPOINTMENT_APPROVED',
                'REAPPOINTMENT_DENIED',
                'NONE');

ALTER TABLE `RAF_FormDepartments` CHANGE COLUMN `Decision`
`Decision` enum('APPROVED',
                'DENIED',
                'OTHER',
                'POSITIVE_APPRAISAL',
                'POSITIVE_GUARDED_APPRAISAL',
                'GUARDED_APPRAISAL',
                'GUARDED_NEGATIVE_APPRAISAL',
                'NEGATIVE_APPRAISAL',
                'SATISFACTORY_REVIEW_ADVANCEMENT',
                'SATISFACTORY_REVIEW',
                'UNSATISFACTORY_REVIEW',
                'REAPPOINTMENT_APPROVED',
                'REAPPOINTMENT_DENIED',
                'NONE');

ALTER TABLE `AppealDecision` CHANGE COLUMN `Decision`
`Decision` enum('APPROVED',
                'DENIED',
                'OTHER',
                'POSITIVE_APPRAISAL',
                'POSITIVE_GUARDED_APPRAISAL',
                'GUARDED_APPRAISAL',
                'GUARDED_NEGATIVE_APPRAISAL',
                'NEGATIVE_APPRAISAL',
                'SATISFACTORY_REVIEW_ADVANCEMENT',
                'SATISFACTORY_REVIEW',
                'UNSATISFACTORY_REVIEW',
                'REAPPOINTMENT_APPROVED',
                'REAPPOINTMENT_DENIED',
                'NONE');
                
-- ****************************************************
-- END: MIV-4924, new dean/vp decision choices
-- ****************************************************
