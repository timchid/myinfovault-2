-- Author: Rick Hendricks
-- Created: 2009-06-03
-- Updated: 2014-03-10

-- Populate the MIV Roles table with our defined roles
-- Dependencies: MivRole table must exist

-- The "MIV_USER" role is a marker that should ONLY be used to
-- distinguish a Person that is an MIV user from a Person that
-- doesn't have an MIV account. Don't use base permissions on it.

-- IMPORTANT!!
--   The ID values here MUST MATCH the values in the MivRole enum!
--   If you change this you must also change MivRole.java


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

truncate MivRole;

insert into MivRole (ID, Description, ShortDescription, MivCode, KimRoleId, ViewNonRedactedDocIds, LimitEditToDossierAttributeIds, InsertTimestamp, InsertUserId)
values
( 1, 'Development Team', 'Sys. Admin.', 'SYS_ADMIN', 'MIV-ADMIN', '36,49,50,68,69', null, CURRENT_TIMESTAMP, 18635),
( 2, 'School/College Administrator', 'School/College Admin.', 'SCHOOL_STAFF', 'MIV-SCH', '36', null , CURRENT_TIMESTAMP, 18635),
( 3, 'School/College Dean', 'Dean', 'DEAN', '', '36', '22,23,24,34,35,36,37,41,42,43,44,49',CURRENT_TIMESTAMP, 18635),
( 4, 'Department Administrator', 'Dept. Admin.', 'DEPT_STAFF', 'MIV-DEPT', '36', null ,CURRENT_TIMESTAMP, 18635),
( 5, 'Department Helper', 'Dept. Helper', 'DEPT_ASSISTANT', 'MIV-DEPT-ASST', null, null, CURRENT_TIMESTAMP, 18635),
( 6, 'Candidate', 'Candidate', 'CANDIDATE', 'MIV-CAND', null, null, CURRENT_TIMESTAMP, 18635),
( 7, 'MIV User', 'User', 'MIV_USER', '', null, null, CURRENT_TIMESTAMP, 18099),
( 8, 'CRC Reviewer', 'CRC Reviewer', 'CRC_REVIEWER', 'MIV-DOSSIER-SCHOOL', '36,49,50,67,68,69', null, CURRENT_TIMESTAMP, 18635),
( 9, 'Department Reviewer', 'Dept Reviewer', 'DEPT_REVIEWER', 'MIV-DOSSIER-DEPARTMENT', '36', null,  CURRENT_TIMESTAMP, 18635),
(10, 'Vice Provost', 'Vice Provost', 'VICE_PROVOST', '', '36,49,50,67,68,69', '18,30,45,46,59,77,89,103,104', CURRENT_TIMESTAMP, 18635),
(11, 'MIV Administrator', 'MIV Admin.', 'VICE_PROVOST_STAFF', 'MIV-VP', '36,49,50,67,68,69', null, CURRENT_TIMESTAMP, 18635),
(12, 'Archive Administrator', 'Archive Admin.', 'ARCHIVE_ADMIN', 'MIV-ARCH', '36,49,50,68,69', null, CURRENT_TIMESTAMP, 18635),
(13, 'School Reviewer', 'School Reviewer', 'SCHOOL_REVIEWER', 'MIV-DOSSIER-SCHOOL', '36', null, CURRENT_TIMESTAMP, 18635),
(14, 'Joint School Reviewer', 'School Reviewer', 'J_SCHOOL_REVIEWER', 'MIV-DOSSIER-SCHOOL', '36', null, CURRENT_TIMESTAMP, 18635),
(15, 'Candidate Snapshot', 'Candidate Snapshot', 'CANDIDATE_SNAPSHOT', 'MIV-CAND', null, null, CURRENT_TIMESTAMP, 18635),
(16, 'Archive User', 'Archive User', 'ARCHIVE_USER', 'MIV-ARCH', '36', null, CURRENT_TIMESTAMP, 18635),
(17, 'Department Chair', 'Dept. Chair', 'DEPT_CHAIR', 'MIV-DEPT-CHAIR', null, null ,CURRENT_TIMESTAMP, 18635),
(18, 'Senate Administrator', 'Senate Admin.', 'SENATE_STAFF', 'NO-KIM-ROLE', '36,49,50,67,68,69','18,25,26,27,28,29,38,50,51,52,53,54,55,56,57', CURRENT_TIMESTAMP, 18099),
(19, 'Provost', 'Provost', 'PROVOST', '', '36,49,50,67,68,69','18,79,81,83,84,91,93,95,96,105,106', CURRENT_TIMESTAMP, 18099),
(20, 'Appointee', 'Appointee', 'APPOINTEE', '', null, null, CURRENT_TIMESTAMP, 20729),
(21, 'Joint Department Reviewer', 'Department Reviewer', 'J_DEPT_REVIEWER', '', '36', null, CURRENT_TIMESTAMP, 20729),
(22, 'Chancellor', 'Chancellor', 'CHANCELLOR', '', '36,49,50,67,68,69', '18,86,87,98,99', CURRENT_TIMESTAMP, 20001),
(23, 'Office of Chancellor and Provost Admin', 'OCP Admin', 'OCP_STAFF', '', null, null, CURRENT_TIMESTAMP, 18099)
;

COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
