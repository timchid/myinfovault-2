# Author: Lawrence Fyfe
# Date: 10/12/2006

create table TraineeSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   Name varchar(100) COMMENT 'Trainee name',
   Degree varchar(20) COMMENT 'Degree held by trainee',
   Type varchar(50) COMMENT 'Type associate with the trainee',
   Year varchar(20) COMMENT 'Range of years of the trainee',
   Position varchar(200) COMMENT 'Trainee current position',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Teaching: Trainees';
