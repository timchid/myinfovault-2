# Author: Lawrence Fyfe
# Date: 01/22/2007

create table ExtendingKnowledgeGathering
(
   ID int primary key auto_increment,
   UserID int,
   GatheringRoleID int,
   Title varchar(255),
   Audience varchar(255),
   Location varchar(255),
   DateSpan varchar(255),
   HeadCount varchar(10),
   Sequence int,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
