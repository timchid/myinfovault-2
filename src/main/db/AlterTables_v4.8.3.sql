-- Delete Decision records not associated with a signature record
DELETE Decision
FROM Decision
LEFT JOIN DocumentSignature
ON Decision.ID=DocumentSignature.DocumentID
AND DocumentSignature.DocumentType IN(20,57,21,84,61,85,94,86,87,88,23,28,59,77,78,93,79,80,81)
WHERE DocumentSignature.ID IS NULL;

--  Move retroactive date to academic action table
ALTER TABLE `AcademicAction` ADD COLUMN `RetroactiveDate` date NULL AFTER `EffectiveDate`;
UPDATE `AcademicAction` a
LEFT JOIN Dossier d ON a.ID=d.AcademicActionID
SET a.RetroactiveDate = d.RetroactiveDate;
ALTER TABLE `Dossier` DROP COLUMN `RetroactiveDate`;

ALTER TABLE AcademicAction ADD EndDate date NULL AFTER RetroactiveDate;

-- Update reviewer views - MIV-5522
source insertCollection.sql;
source insertCollectionDocument.sql;
source insertDossierView.sql;

-- Appointments order must be preserved
ALTER TABLE Appointment ADD Sequence INT NOT NULL DEFAULT 0 AFTER PrimaryAppointment;

-- Years and Rank, Years at Step
ALTER TABLE AssignmentTitles
ADD YearsAtRank TINYINT NOT NULL DEFAULT 0 AFTER AnnualSalary,
ADD YearsAtStep TINYINT NOT NULL DEFAULT 0 AFTER YearsAtRank;

-- AccelerationYears
ALTER TABLE AcademicAction
ADD AccelerationYears TINYINT NOT NULL DEFAULT 0 AFTER EndDate;

-- MIV-5570, dossier attribute may be optional for certain action types
ALTER TABLE DossierAttributeType
ADD Optional VARCHAR(255) NULL DEFAULT NULL COMMENT 'Comma-delimited list of DossierActionType.ID where the dossier attribute is optional (overriding Required)'
AFTER Required;

source insertDossierAttributeType.sql;

ALTER TABLE RoutingPathDefinitions
ADD ActionType ENUM ('MERIT',                                   
                     'PROMOTION',
                     'APPOINTMENT',
                     'APPOINTMENT_ENDOWED_CHAIR',           
                     'APPOINTMENT_ENDOWED_PROFESSORSHIP',   
                     'APPOINTMENT_ENDOWED_SPECIALIST',      
                     'APPOINTMENT_INITIAL_CONTINUING',      
                     'APPOINTMENT_CHANGE_DEPARTMENT',       
                     'APPOINTMENT_CHANGE_TITLE',            
                     'APPRAISAL',                           
                     'DEFERRAL_FIRST_YEAR',                 
                     'DEFERRAL_SECOND_YEAR',                
                     'DEFERRAL_THIRD_YEAR',                 
                     'DEFERRAL_FOURTH_YEAR',                
                     'EMERITUS_STATUS',                     
                     'FIVE_YEAR_REVIEW',                    
                     'FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR',   
                     'REAPPOINTMENT',                       
                     'REAPPOINTMENT_ENDOWED_CHAIR',         
                     'REAPPOINTMENT_ENDOWED_PROFESSORSHIP', 
                     'REAPPOINTMENT_ENDOWED_SPECIALIST') NULL;
                     
ALTER TABLE RoutingPathDefinitions
DROP PRIMARY KEY,
ADD UNIQUE `routingpathdefinitions_index`(`DelegationAuthority`, `ActionType`, `CurrentWorkflowLocation`,`NextWorkflowLocation`,`PreviousWorkflowLocation`);

source insertRoutingPathDefinitions.sql;

-- Document Signature Cleanup
INSERT INTO DocumentSignatureCopy SELECT s.* FROM DocumentSignature s LEFT JOIN Dossier d ON s.DossierID=d.DossierID WHERE NOT Signed AND d.WorkflowStatus in('F','X');
DELETE FROM DocumentSignature WHERE ID IN(SELECT ID FROM DocumentSignatureCopy );
