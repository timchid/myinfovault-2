# Author: Lawrence Fyfe
# Date: 01/22/2007

create table GatheringRole
(
   ID int primary key auto_increment,
   Name varchar(50),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
