-- Author: Pradeep Kumar Haldiya
-- Created: 2013-09-13

-- Create the main table to track appointment details
-- Dependencies: It contains the answers of for questions. The AcademicAction table must exist.

DROP TABLE IF EXISTS AppointmentDetails;

CREATE TABLE AppointmentDetails
(
   ID INT PRIMARY KEY auto_increment,
   AcademicActionID INT NOT NULL,
   Employee ENUM('YES','NO','NA') NOT NULL DEFAULT 'NO',
   Represented ENUM('YES','NO','NA') NOT NULL DEFAULT 'NA',
   NoticeToUnion ENUM('YES','NO','NA') NOT NULL DEFAULT 'NA',
   NoticeToLaborRelation ENUM('YES','NO','NA') NOT NULL DEFAULT 'NA',
   InsertTimestamp TIMESTAMP DEFAULT current_timestamp,
   InsertUserID INT NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID INT,
   KEY `fk_appointmentdetails_academic_action` (`AcademicActionID`),
   CONSTRAINT `fk_appointmentdetails_academic_action` FOREIGN KEY (`AcademicActionID`) 
   REFERENCES `AcademicAction` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
engine = InnoDB
default character set utf8;
