-- Author: Lawrence Fyfe
-- Date: 10/11/2006

create table PublicationSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int NOT NULL COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   TypeID int NOT NULL COMMENT 'ID corresponding to the publication type', -- TODO: ref PublicationType
   StatusID int NOT NULL COMMENT 'ID corresponding to the publication status', -- TODO: ref PublicationStatus
   -- External sources: 0 = MIV User;  1 = PubMed;  2 = EndNote
   ExternalSourceID tinyint(1) NOT NULL default 0 comment 'Identify which external source this record is from, e.g. PubMed==1',
   ExternalID varchar(50) comment 'Unique ID the external source used to identify this record.',
   Year varchar(4) NOT NULL COMMENT 'Year associated with the publication status',
   Author text NOT NULL COMMENT 'Publication author or authors', -- max is 5524
   Title text NOT NULL COMMENT 'Publication title',  -- max is 3213
   Editor varchar(255) COMMENT 'Publication editor',
   Journal varchar(255) COMMENT 'Journal to which the publication belongs',
   Volume varchar(20) COMMENT 'Journal volume',
   Issue varchar(20) COMMENT 'Volume issue',
   Pages varchar(50) COMMENT 'Issue pages cited',
   Publisher varchar(255) COMMENT 'Journal publisher',
   City varchar(50) COMMENT 'Publisher location',
   ISBN varchar(20) default NULL COMMENT 'Unique identifier for the Publication or the ISSN of the Journal in which it appears',
   EngineeringAnnotation varchar(4) default NULL COMMENT 'Corresponds to the publication status; Only used by the School of Engineering',
   Link text COMMENT 'URL to an online resource associated with the publication',
   Contribution text COMMENT 'Description of contributions to jointly authored works', -- max is 3385
   Significance text COMMENT '', -- max is 26528, next is 4516
   -- As of 2008-08-12 max length of Citation is zero. The column can be dropped.
   Citation mediumtext comment 'The old "reference" field. This will be dropped; we no longer allow "intact" citations.',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   DisplayContribution boolean NOT NULL default true COMMENT 'Should the contribution text be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int NOT NULL COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID),
   INDEX (Year)		-- adding this index on Year for the DSS view
)
engine = InnoDB
default character set utf8
collate = utf8_general_ci
COMMENT 'Book/Journal/Alternative Media/etc publication citations';
