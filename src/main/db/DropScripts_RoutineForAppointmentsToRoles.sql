-- Author : Pradeep Kumar Haldiya
-- Created: 08/23/2011
-- Description: Drop all procedures and functions specific for 
-- 'Drop scripts for appointments to roles conversion. (MIV-3956)'  

-- Drop logger specific procedures
drop PROCEDURE if exists `myinfovault`.`log_error`;
drop PROCEDURE if exists `myinfovault`.`log_info`;
drop PROCEDURE if exists `myinfovault`.`log_sql`;
drop PROCEDURE if exists `myinfovault`.`log_warn`;
drop PROCEDURE if exists `myinfovault`.`insert_log`;
drop PROCEDURE if exists `myinfovault`.`delete_log`;

-- Drop functions
drop FUNCTION if exists `myinfovault`.`getMivScopeString`;
-- drop FUNCTION if exists `myinfovault`.`isUserHaveRole`; -- No need to delete this
drop FUNCTION if exists `myinfovault`.`isConversionNeeded`;
-- drop FUNCTION if exists `myinfovault`.`getPrimaryRole`; -- No need to delete this

-- Drop procedures
drop PROCEDURE if exists `myinfovault`.`PROC_INSERT_ROLE_ASSIGNMENT`;
drop PROCEDURE if exists `myinfovault`.`PROC_DELETE_APPOINTMENT`;
drop PROCEDURE if exists `myinfovault`.`PROC_DELETE_ROLE_ASSIGNMENT`;
DROP PROCEDURE if exists `myinfovault`.`PROC_VALIDATE_ROLES`;
drop PROCEDURE if exists `myinfovault`.`PROC_CONVERT_APPOINTMENT_INTO_ROLE_ASSIGNMENT`;
drop PROCEDURE if exists `myinfovault`.`PROC_CONVERT_MAIN`;
