# Author: Lawrence Fyfe
# Date: 8/20/2007

create table CommitteeType
(
    ID int primary key auto_increment,
    Description varchar(255),
    Sequence int,
    InsertTimestamp timestamp default current_timestamp,
    InsertUserID int,
    UpdateTimestamp datetime,
    UpdateUserID int
)
engine = InnoDB
default character set utf8;
