# Author: Stephen Paulsen
# Date: 08/15/2007
#
# Note! This replaces the old CvTadvise table but does not change the
# bad-old-way the table is used.  This change is only to conform to our
# new column naming standards so exceptions for CvTadvise can be removed
# from the code.  A better way to handle student advising still needs to
# be devised.  Because of this, we are retaining the old cryptic table name
# to signify this is just a revision-2 of the existing practice.  When a
# better design is implemented the new table(s) should be given more
# descriptive names.

create table CvTadvise2
(
   ID int primary key auto_increment,
   UserID int NOT NULL,
   Year varchar(4) NOT NULL,
   UGnum int NOT NULL default 0,
   Gnum int  NOT NULL default 0,
   A3num int NOT NULL default 0,
   A4num int NOT NULL default 0,
   Advise1 varchar(100) NOT NULL default 'Number of undergraduate advisees:',
   Advise2 varchar(100) NOT NULL default 'Number of graduate advisees:',
   Advise3 varchar(100) NOT NULL default '',
   Advise4 varchar(100) NOT NULL default '',
   Sequence int NOT NULL default 9999,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
