-- Author: Jacob Saporito
-- Created: 2015-03-31

-- Create the ApiEntity table
-- Dependencies:

DROP TABLE IF EXISTS `myinfovault`.`ApiEntity`;

CREATE TABLE `myinfovault`.`ApiEntity`
(
    `ID` int primary key auto_increment,
    `EntityName` VARCHAR(64) NOT NULL UNIQUE,
    `KeyHash` VARCHAR(512) NOT NULL,
    `Salt` VARCHAR(512) NOT NULL,
    `SecurityScheme` ENUM('INTERNAL', 'BASIC') NOT NULL,
    `IPRange` VARCHAR(140) DEFAULT NULL,
    `InsertTimestamp` timestamp DEFAULT CURRENT_TIMESTAMP,
    `UpdateTimestamp` datetime, 
    KEY `k_entity_name` (`EntityName`)
)
engine = InnoDB
default character set utf8;
