-- Author: Pradeep Haldiya
-- Created: 2012-03-19
-- Updated: 

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM WorkStatus INTO @_ORIG_COUNT;

truncate WorkStatus;

INSERT INTO WorkStatus (ID, Status, Description, Active, InsertUserId)
VALUES
(1, 'Completed', 'A work nas been completed', true, 20720),
(2, 'Scheduled', 'A work has been scheduled', true, 20720),
(3, 'Submitted', 'A work has been submitted', true, 20720);

SHOW WARNINGS;
commit;

SELECT count(*) FROM WorkStatus INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
