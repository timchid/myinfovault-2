source createDecision.sql;

-- appeal decisions
INSERT INTO Decision(ID, Comment, Decision, ContraryToCommittee)
SELECT CONCAT(s.DocumentType, LPAD(s.DocumentID, 5, '0')), a.Comment, IFNULL(a.Decision, 'NONE'), IFNULL(a.ContraryToCommittee, false)
FROM DocumentSignature s
LEFT JOIN AppealDecision a
ON s.DocumentID=a.ID
WHERE s.DocumentType IN(61,57,59)
GROUP BY s.DocumentType,s.DocumentID;

-- vp final decisions
INSERT INTO Decision(ID, Comment, Decision, ContraryToCommittee)
SELECT CONCAT(s.DocumentType, LPAD(s.DocumentID, 5, '0')), r.Comment, IFNULL(r.Decision, 'NONE'), IFNULL(r.ContraryToCommittee, false)
FROM DocumentSignature s
LEFT JOIN RAF_Forms r
ON s.DocumentID=r.ID
WHERE s.DocumentType IN(21)
GROUP BY s.DocumentType,s.DocumentID;

-- dean decisions/recommendations
INSERT INTO Decision(ID, Comment, Decision, ContraryToCommittee)
SELECT CONCAT(s.DocumentType, LPAD(s.DocumentID, 5, '0')), d.Comment, IFNULL(d.Decision, 'NONE'), IFNULL(d.ContraryToCommittee, false)
FROM DocumentSignature s
LEFT JOIN RAF_FormDepartments d
ON s.DocumentID=d.ID
WHERE s.DocumentType IN(20,23,28)
GROUP BY s.DocumentType,s.DocumentID;

-- update signature record document IDs
UPDATE DocumentSignature
SET DocumentID=CONCAT(DocumentType, LPAD(DocumentID, 5, '0'))
WHERE DocumentType IN(61,57,59,21,20,23,28);

-- drop decision fields 
ALTER TABLE RAF_Forms
DROP COLUMN Comment,
DROP column Decision,
drop column ContraryToCommittee;

ALTER TABLE RAF_FormDepartments
DROP COLUMN Comment,
DROP column Decision,
drop column ContraryToCommittee;

DROP TABLE AppealDecision;