# Author: Stephen Paulsen
# Date: 09/18/2007

create table Quarantine
(
   ID int primary key auto_increment,
   UserID int NOT NULL,
   RecordID int NOT NULL,
   RecordTableName varchar(40),
   RecordFieldName varchar(40),
   DocumentID int NOT NULL,
   SectionID int NOT NULL,
   Contents mediumtext NOT NULL,
   Suggestion mediumtext,
   Fixed boolean NOT NULL default false,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
