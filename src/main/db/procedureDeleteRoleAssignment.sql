-- Author : Pradeep Kumar Haldiya
-- Created: 09/02/2011

DELIMITER $$

-- Procedure to delete Assignments by UserId and RoleId
-- @param vUserID
-- @param vRoleID
DROP PROCEDURE IF EXISTS `PROC_DELETE_ROLE_ASSIGNMENT`$$
CREATE PROCEDURE `PROC_DELETE_ROLE_ASSIGNMENT`(in vUserID INT(11),in vRoleID INT(11))
BEGIN
	/****************************************************************************/
	/* Declare Variables 												    	*/
	/****************************************************************************/
	DECLARE sqlScript VARCHAR(4000) DEFAULT null;

	-- just for log
	SET sqlScript = CONCAT('DELETE FROM `RoleAssignment` WHERE `UserID`=',vUserId,' AND `RoleID`=', vRoleID);
	CALL log_sql(sqlScript);

	DELETE FROM `RoleAssignment` WHERE `UserID`=vUserId AND `RoleID`=vRoleID;
	
END$$

DELIMITER ;
