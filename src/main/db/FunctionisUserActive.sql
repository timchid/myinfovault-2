-- Author : Pradeep Kumar Haldiya
-- Created: 10/31/2011

DELIMITER $$

-- Function to get User's Active flag by the UserID
DROP FUNCTION IF EXISTS `isUserActive`$$
CREATE FUNCTION `isUserActive`(vUserId INT(11)) RETURNS tinyint(1)
BEGIN
	DECLARE isActive BOOLEAN DEFAULT false;

	SELECT ifnull((SELECT `Active` FROM `UserAccount` WHERE `UserID`=vUserId LIMIT 1),false) INTO isActive; 

	RETURN isActive;
END$$

DELIMITER ;


