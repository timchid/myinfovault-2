# Author: Lawrence Fyfe
# Date: 2007-02-20

# Inserts on CvStatement -- Candidate's Statement

DELIMITER |

CREATE TRIGGER InsertCvStatement AFTER INSERT on CvStatement
FOR EACH ROW BEGIN
  IF NEW.UploadName IS NULL
  THEN
  INSERT INTO CandidateStatement
    SET
       UserID=NEW.SSN,
       OldID=NEW.STID,
       Year=NEW.Year,
       Content=NEW.Content,
       Sequence=NEW.Seq,
       Display=true,
       InsertTimestamp=now(),
       InsertUserID=NEW.SSN
    ;
  ELSE
  INSERT INTO UserUploadDocument
    SET
       UserID=NEW.SSN,
       DocumentID=1,
       UploadFile=NEW.UploadName,
       Year=NEW.Year,
       Display=true,
       InsertTimestamp=now(),
       InsertUserID=NEW.SSN
    ;
  END IF;
END;
|

DELIMITER ;

