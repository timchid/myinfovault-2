-- Author: Rick Hendricks
-- Created: 2014-08-29
-- Updated: never
--
-- Fix multiple annotation records for a specified UserId and base section table which are associated with the same base table record.
--
-- Required Input:
--     inputUserId           - the UserId for which the annotation records will be processed.   
--     inputBaseSectionTable - the section table for to which the annotation records refer. 
--                             Values may be as follows:
--                             +-------------------------------+
--                             | SectionBaseTable              |
--                             +-------------------------------+
--                             | AdministrativeActivitySummary |
--                             | CommitteeSummary              |
--                             | CourseEvaluationSummary       |
--                             | EditorialAdvisoryBoardSummary |
--                             | Events                        |
--                             | ExtendingKnowledgeGathering   |
--                             | ExtendingKnowledgeMedia       |
--                             | ExtendingKnowledgeOther       |
--                             | PatentSummary                 |
--                             | PresentationSummary           |
--                             | PublicationEvents             |
--                             | PublicationSummary            |
--                             | Reviews                       |
--                             | Works                         |
--                             +-------------------------------+
-- 
-- Currently there is a problem when doing a bulk change to add a Notation which causes a new Annotation record to be created
-- with the Notation rather than updating the original Annotation record. If there happens to be an AnnotationLine record associated
-- with the original Annotation record, the annotation will "disappear" from the annotation page because the newly added Annotation
-- record, which has no associated AnnotationLine, will be used instead of the original Annotation record when displaying the page
-- and creating the PDF.   
--
-- The methodology to correct this will be to select all Annotation records for a UserId and sort them by the base table RecordId
-- and InsertTimestamp. With the records in this ascending order, the original Annotation record will be followed by any records which
-- were added and pointing to the same base table RecordId. The Notation, Footnote and Display data for each record pointing to the 
-- same base table record will be rolled up into the original record, and the additional Annotation record(s) will be deleted.
--
-- Accounts with duplicate annotation records pointing to the same SectionBaseTable record may be identified with the following query:
--   mysql> SELECT count(*), A.ID, A.UserID, A.SectionBaseTable, A.RecordID FROM Annotations A group by A.UserId, A.SectionBaseTable, A.RecordId having count(*) >1; 

DROP PROCEDURE IF EXISTS Fix_Annotations;

DELIMITER $$

CREATE PROCEDURE Fix_Annotations (IN inputUserId int(11), inputBaseTable varchar(40))
BEGIN

        DECLARE bDone INT DEFAULT FALSE;
        DECLARE footnote mediumtext;
        DECLARE notation varchar(4);
        DECLARE display tinyint(1);

        DECLARE lastFootnote mediumtext;
        DECLARE lastNotation varchar(4);
        DECLARE lastDisplay tinyint(1);

        DECLARE firstAnnotationId int(11);
        DECLARE annotationId int(11);
        DECLARE recordId int(11);
        DECLARE lastRecordId int(11);

        DECLARE dupCount INT;

        DECLARE curs CURSOR FOR SELECT A.ID, A.RecordID, ifnull(A.Footnote, '') AS Footnote, A.Notation, A.Display FROM Annotations A WHERE A.UserId=inputUserId and A.SectionBaseTable IN (inputBaseTable) order by A.RecordId, A.InsertTimestamp;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = TRUE;

        -- Create back-up table for the annotation records we will be updating/deleting
        CREATE TABLE IF NOT EXISTS AnnotationsBackup LIKE Annotations;

        -- Add the records we will be processing to the backup
        INSERT INTO AnnotationsBackup
        SELECT *  FROM Annotations A WHERE A.UserId=InputUserId and A.SectionBaseTable IN (inputBaseTable);

OPEN curs;

set lastRecordId = 0;
set firstAnnotationId = 0;
set lastFootNote = NULL;
set lastNotation = NULL;
set lastDisplay = 1;
set dupCount = 0;

read_loop: LOOP
        FETCH curs INTO annotationId, recordId, footnote, notation, display;

        -- Check if we are done
        IF bDone THEN
                -- No more records to process, but we still need to update the last record
                IF lastRecordId != 0 and firstAnnotationId != 0 and dupCount > 0 THEN
                        UPDATE Annotations set Footnote=lastFootNote, Notation=lastNotation, Display=lastDisplay, UpdateTimestamp=NOW(), UpdateUserID=18635 where ID = firstAnnotationId;
                        SELECT CONCAT('Updated Annotation record ',FirstAnnotationId,' for ',inputBaseTable, ' record ',LastRecordId);
                END IF;
                LEAVE read_loop;
        END IF;

        SELECT CONCAT('Processing annotation record ',annotationId, ' for ', inputBaseTable, ' record ', recordId);

        -- See the recordId has changed
        IF recordId != lastRecordId THEN
                -- We are at a new record, check to update the original annotation record
                IF lastRecordId != 0 and firstAnnotationId != 0 and dupCount > 0 THEN
                        UPDATE Annotations set Footnote=lastFootNote, Notation=lastNotation, Display=lastDisplay where ID = firstAnnotationId;
                        SELECT CONCAT('Updated Annotation record ',FirstAnnotationId,' for ',inputBaseTable, ' record ',LastRecordId);
                END IF;
                -- Reset for the new record 
                set firstAnnotationId = annotationId;
                set lastRecordId = recordId;
                set dupCount = 0;
        ELSE
                set dupCount = dupCount+1;
        END IF;
        
        -- At the same record number, save the data for this record
        set lastFootNote = footnote;
        set lastNotation = notation;
        set lastDisplay = display;

        -- Check to delete the duplicate annotation record
        IF firstAnnotationId != annotationId THEN
                DELETE FROM Annotations where ID = annotationId;
                SELECT CONCAT('Deleted annotation record ',annotationId, ' for ',inputBaseTable,' record ',recordId);
        END IF;

END LOOP;

COMMIT;

CLOSE curs;

END $$

DELIMITER ;
