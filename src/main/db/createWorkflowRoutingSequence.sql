-- Author: Rick Hendricks
-- Date: 2011-12-02
--
-- This table defines the workflow routing sequence for a dossier by delegation authority and action type.
-- There is an entry for each legal combination of delegation authority, action type and workflow location
-- with a sequence number defining the position of the workflow location in the routing sequence.
--
-- As an example, the routing sequence for a Relelegated Merit action would be:
-- Candidate -> Department -> School/college -> ReadyToArchive -> Archive
-- Such a routing sequence would be defined in the WorkflowRoutingSequence with 5 records, one for each of the
-- 5 workflow locations along with the sequence where the location falls within the sequence, as well as the
-- delegation authority and action type.
--
-- The DelegationAuthorityID is defined in the DelegationAuthority table
-- The ActionTypeID is defined in the ActionType table
-- The WorkflowLocationID is defined in the WorkflowLocation table

DROP TABLE IF EXISTS WorkflowRoutingSequence;

CREATE TABLE `WorkflowRoutingSequence`
(
   `ID` INT PRIMARY KEY AUTO_INCREMENT,
   `DelegationAuthorityID` INT NOT NULL,
   `ActionTypeID` INT NOT NULL,
   `WorkflowLocationID` INT NOT NULL,
   `RoutingSequence` INT NOT NULL,
   `InsertUserID` INT,
   `UpdateTimestamp` DATETIME,
   `UpdateUserID` INT,
   INDEX(`DelegationAuthorityID`),
   KEY `fk_delegationauthorityid` (`DelegationAuthorityID`),
   KEY `fk_actiontypid` (`ActionTypeID`),
   KEY `fk_workflowlocationid` (`WorkflowLocationID`)
)
engine = InnoDB
default character set utf8;
