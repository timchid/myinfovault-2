# Author: Rick Hendricks
# Date: 07/23/2009
# This table relates the Dossier attribute types (DossierAttributeType) to a location. The sequence defines
# the order in which the attributes should be displayed on the View Dossier Status/Manage Open Actions jsp pages.

drop table if exists DossierAttributeLocation;

create table DossierAttributeLocation
(
   ID int primary key auto_increment,
   LocationId int,
   DossierAttributeId int,
   AppointmentType ENUM ('PRIMARY','JOINT') NOT NULL,
   Sequence int,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
