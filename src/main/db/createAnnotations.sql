-- Author: Pradeep K Haldiya
-- Created: 2013-09-23
-- Updated: 2015-03-23 (SDP)

-- Create the Annotation Table

DROP TABLE IF EXISTS Annotations;

CREATE TABLE Annotations
(
   ID INT PRIMARY KEY AUTO_INCREMENT,
   UserID INT NOT NULL,
   RecordID INT NOT NULL,
   SectionBaseTable VARCHAR(255) NULL,
   PacketID INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'zero (0) for master annotations'
   Footnote mediumtext,
   Notation VARCHAR(4),
   Display BOOLEAN NOT NULL DEFAULT true,
   InsertTimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   InsertUserID INT,
   UpdateTimestamp DATETIME,
   UpdateUserID INT,
   INDEX (UserID),
   CONSTRAINT `fk_packetid`
     FOREIGN KEY (`PacketID`)
     REFERENCES Packet(ID)
)
engine = InnoDB
default character set utf8;
