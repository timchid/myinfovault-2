-- title step must allow decimal increment
ALTER TABLE `AssignmentTitles`
CHANGE COLUMN `Step`
`Step` DECIMAL(3,1) NULL DEFAULT NULL;
