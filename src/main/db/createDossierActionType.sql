-- Author: Rick Hendricks
-- Created: 2011-12-02
--
-- This table defines the action types for an action (Merit, Promotion, etc).

DROP TABLE IF EXISTS DossierActionType;

CREATE TABLE DossierActionType
(
   ID INT PRIMARY KEY AUTO_INCREMENT,
   ActionType ENUM ('MERIT',                                   
                    'PROMOTION',
                    'APPOINTMENT',
                    'APPOINTMENT_ENDOWED_CHAIR',           
                    'APPOINTMENT_ENDOWED_PROFESSORSHIP',   
                    'APPOINTMENT_ENDOWED_SPECIALIST',      
                    'APPOINTMENT_INITIAL_CONTINUING',      
                    'APPOINTMENT_CHANGE_DEPARTMENT',       
                    'APPOINTMENT_CHANGE_TITLE',            
                    'APPRAISAL',                           
                    'DEFERRAL_FIRST_YEAR',                 
                    'DEFERRAL_SECOND_YEAR',                
                    'DEFERRAL_THIRD_YEAR',                 
                    'DEFERRAL_FOURTH_YEAR',                
                    'EMERITUS_STATUS',                     
                    'FIVE_YEAR_REVIEW',                    
                    'FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR',   
                    'REAPPOINTMENT',                       
                    'REAPPOINTMENT_ENDOWED_CHAIR',         
                    'REAPPOINTMENT_ENDOWED_PROFESSORSHIP', 
                    'REAPPOINTMENT_ENDOWED_SPECIALIST')
)
engine = InnoDB
default character set utf8;
