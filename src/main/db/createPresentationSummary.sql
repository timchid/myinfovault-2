# Author: Lawrence Fyfe
# Date: 03/14/2006

create table PresentationSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record',
   Title mediumtext,
   Year varchar(4),
   MonthID int,
   Day int,
   LocationDescription text,
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8;
