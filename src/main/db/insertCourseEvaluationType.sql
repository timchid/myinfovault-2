# Author: Lawrence Fyfe
# Date: 01/12/2007

truncate CourseEvaluationType;

insert into CourseEvaluationType (ID,Sequence,Description,InsertUserID)
values
(1,2,'Complete',14021),
(2,1,'Summary',14021),
(3,3,'Not Available',18099);
