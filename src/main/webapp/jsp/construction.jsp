<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>MIV &ndash; Construction</title>
  <script type="text/javascript">
    var mivConfig = new Object();
    mivConfig.isIE = false;
  </script>

  <%@ include file="/jsp/metatags.html" %>

  <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
  
  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
  <script type="text/javascript">mivConfig.isIE=true;</script>
  <![endif]-->
<c:set var="user" scope="request" value="${MIVSESSION.user}"/>  
</head>
<body>

<jsp:include page="/jsp/mivheader.jsp" />

<div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt;&nbsp;Construction
</div><!-- breadcrumbs -->

<div id="main"><!-- MIV Main Div -->

    <h1>Under Construction</h1>

    <p class="notice"><strong>The process you have selected is not available at this time.</strong></p>

    <p class="notice">
        This feature is expected to be available soon.
    </p>

    Questions? Please contact the MIV Project Team at
    <a href='mailto:miv-help@ucdavis.edu' title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>.

</div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html>
