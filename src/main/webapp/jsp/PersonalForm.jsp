<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE HTML>
<html>

<head>
  <title>MIV &ndash; Personal Information</title>
  <script type="text/javascript">
    djConfig = {
        isDebug: false,
        //debugAtAllCosts: true,
        //debugContainerId : "myDojoDebug",
        parseOnLoad: true // parseOnLoad *must* be true to parse the wigits
    };
    var mivConfig = new Object();
    mivConfig.isIE = false;
  </script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/FloatingPane.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/special_character2.css'/>">

  <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <jwr:script src="/bundles/mivvalidation.js" />
    <script type="text/javascript" src="<c:url value='/js/personalform.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/charpalette.js'/>"></script>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
  <jsp:include page="/jsp/mivheader.jsp" />

  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; Personal Information
  </div><!-- breadcrumbs -->

  <div id="main">
    <h1>Personal: Personal Information</h1>

    <c:set var="hasError" value=""/>
    <c:set var="Message" value=""/>
    <c:if test="${(not empty success && success == false && not empty CompletedMessage)}">
        <c:set var='hasError' scope='page' value=' class="hasmessages"'/>
        <c:set var="Message" value="<div id='errorbox'>${CompletedMessage}</div>"/>
    </c:if>

    <div id="errormessage" ${hasError}>
         ${Message}
    </div>

    <c:set var="hasMessages" value=""/>
    <c:set var="Message" value=""/>
    <c:if test="${(not empty success && success == true && not empty CompletedMessage)}">
        <c:set var='hasmessages' scope='page' value=' class="hasmessages"'/>
        <c:set var="Message" value="<div id='successbox'>${CompletedMessage}</div>"/>
    </c:if>

    <div id="CompletedMessage" ${hasmessages}>
        ${Message}
    </div>

    <c:if test="${(not empty CompletedMessage)}">
        <script type="text/javascript">
            var $CompletedMessage = $('#CompletedMessage');
            $CompletedMessage.removeClass('hasmessages');
            $CompletedMessage.prop('style','');
        </script>
    </c:if>

    <div id="enterdataguide">
      <div id="requiredfieldlegend">* = Required Field</div>
      <div id="specialformattinghelp"><a href="<c:url value='/help/special_formatting.html'/>" title="Do you want to format text?" target="mivhelp">Do you want to format text?</a></div>
    </div>

    <div id="maindialog" class="pagedialog">
      <div class="dojoDialog mivDialog">
          <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
        <form  class="mivdata" id="mytestform" action="<c:url value='/PersonalForm'/>" method="post">
        <c:set var="phoneset" value="${personaldata.phonesList}"/><%--
        --%><c:forEach var="phone" items="${phoneset}"><%--
              --%><c:set var="phType" value="${phone.phoneTypeID}"/><%--
              --%><c:if test="${phType == 1}"><%--
                --%><c:set var="pTel" value="${phone.number}"/><%--
                --%><c:set var="pTelRecID" value="${phone.phoneRecID}"/><%--
              --%></c:if><%--

              --%><c:if test="${phType == 2}"><%--
                --%><c:set var="pFax" value="${phone.number}"/><%--
                --%><c:set var="pFaxRecID" value="${phone.phoneRecID}"/><%--
              --%></c:if><%--
              --%><c:if test="${phType == 3}"><%--
                --%><c:set var="cTel" value="${phone.number}"/><%--
                --%><c:set var="cTelRecID" value="${phone.phoneRecID}"/><%--
              --%></c:if><%--
              --%><c:if test="${phType == 4}"><%--
                --%><c:set var="cFax" value="${phone.number}"/><%--
                --%><c:set var="cFaxRecID" value="${phone.phoneRecID}"/><%--
              --%></c:if><%--
              --%><c:if test="${phType == 5}"><%--
                --%><c:set var="wTel" value="${phone.number}"/><%--
                --%><c:set var="wTelRecID" value="${phone.phoneRecID}"/><%--
              --%></c:if><%--
              --%><c:if test="${phType == 6}"><%--
                --%><c:set var="wFax" value="${phone.number}"/><%--
                --%><c:set var="wFaxRecID" value="${phone.phoneRecID}"/><%--
              --%></c:if><%--
            --%><c:if test="${phType == 7}"><%--
              --%><c:set var="cellPhone" value="${phone.number}"/><%--
              --%><c:set var="cellRecID" value="${phone.phoneRecID}"/><%--
            --%></c:if><%--
        --%></c:forEach><%-- each phone --%>

          <input type="hidden" name="pTelRecID"  value="${not empty rec.pTelRecID ? rec.pTelRecID : pTelRecID}">
          <input type="hidden" name="pFaxRecID"  value="${not empty rec.pFaxRecID ? rec.pFaxRecID : pFaxRecID}">
          <input type="hidden" name="cTelRecID"  value="${not empty rec.cTelRecID ? rec.cTelRecID : cTelRecID}">
          <input type="hidden" name="cFaxRecID"  value="${not empty rec.cFaxRecID ? rec.cFaxRecID : cFaxRecID}">
          <input type="hidden" name="wTelRecID"  value="${not empty rec.wTelRecID ? rec.wTelRecID : wTelRecID}">
          <input type="hidden" name="wFaxRecID"  value="${not empty rec.wFaxRecID ? rec.wFaxRecID : wFaxRecID}">
          <input type="hidden" name="cellRecID"  value="${not empty rec.cellRecID ? rec.cellRecID : cellRecID}">
        <c:set var="addresses" value="${personaldata.addresses}"/><%--
    --%><c:forEach var="address" items="${addresses}"><%--
      --%><c:if test="${address.addressTypeID == 1}"><%--
        --%><c:set var="precid" value="${address.addressRecID}"/><%--
        --%><c:set var="varpaddressstreet1" value="${address.street1}"/><%--
        --%><c:set var="varpaddressstreet2" value="${address.street2}"/><%--
        --%><c:set var="varpcity" value="${address.city}"/><%--
        --%><c:set var="varpstate" value="${address.state}"/><%--
        --%><c:set var="varpzip" value="${address.zipCode}"/><%--
      --%></c:if><%-- address type 1 --%><%--

    --%><c:if test="${address.addressTypeID == 2}"><%--
      --%><c:set var="crecid" value="${address.addressRecID}"/><%--
      --%><c:set var="varcaddressstreet1" value="${address.street1}"/><%--
      --%><c:set var="varcaddressstreet2" value="${address.street2}"/><%--
      --%><c:set var="varccity" value="${address.city}"/><%--
      --%><c:set var="varcstate" value="${address.state}"/><%--
      --%><c:set var="varczip" value="${address.zipCode}"/><%--
    --%></c:if><%-- address type 2 --%><%--

    --%><c:if test="${address.addressTypeID == 3}"><%--
      --%><c:set var="wrecid" value="${address.addressRecID}"/><%--
      --%><c:set var="varwaddressstreet1" value="${address.street1}"/><%--
      --%><c:set var="varwaddressstreet2" value="${address.street2}"/><%--
      --%><c:set var="varwcity" value="${address.city}"/><%--
      --%><c:set var="varwstate" value="${address.state}"/><%--
      --%><c:set var="varwzip" value="${address.zipCode}"/><%--
      --%><c:set var="phones" value="${address.phones}"/><%--
    --%></c:if><%-- address type 3 --%><%--

    --%></c:forEach>
          <input type="hidden" name="pAddressRecID"  value="${not empty rec.pAddressRecID ? rec.pAddressRecID : precid}">
          <input type="hidden" name="cAddressRecID"  value="${not empty rec.cAddressRecID ? rec.cAddressRecID : crecid}">
          <input type="hidden" name="wAddressRecID"  value="${not empty rec.wAddressRecID ? rec.wAddressRecID : wrecid}">

          <div class="formline">
            <span class="textfield">
              <label for="displayName" class="f_required">Display Name</label> <span>(for all documents):</span>
              <input type="text"
                     id="displayName" name="displayName"
                     size="33" maxlength="255"
                     value="${not empty rec.displayName ? rec.displayName : personaldata.displayName}">
            </span>
          </div><!--formline-->

          <div class="formline">
            <input type="hidden" name="employeeID"  value="${not empty rec.employeeID ? rec.employeeID : personaldata.employeeID}">
            Employee ID#: <input type="text" readonly="readonly" name="empID"  value="${not empty rec.employeeID ? rec.employeeID : personaldata.employeeID}">
          </div><!--formline-->

          <!-- Permanent Address  -->
          <div class="addressblock">
            <div class="formline">
              <span class="textfield">
                <label for="paddressstreet1">Permanent Address:</label>
                <input type="text" class="newline"
                       id="paddressstreet1" name="paddressstreet1"
                       size="33" maxlength="100"
                       value="${not empty rec.paddressstreet1 ? rec.paddressstreet1 : varpaddressstreet1}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="paddressstreet2" style="display:none;">Permanent Address - Line 2:</label>
                <input type="text"
                       id="paddressstreet2" name="paddressstreet2"
                       size="33" maxlength="100"
                       value="${not empty rec.paddressstreet2 ? rec.paddressstreet2 : varpaddressstreet2}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="pcity">City:</label>
                <input type="text"
                       id="pcity" name="pcity"
                       size="23" maxlength="50"
                       value="${not empty rec.pcity ? rec.pcity : varpcity}">
              </span>

              <span class="textfield">
                <label for="pstate">State:</label>
                <input type="text"
                       id="pstate" name="pstate"
                       size="4" maxlength="2"
                       value="${not empty rec.pstate ? rec.pstate : varpstate}">
              </span>

              <span class="textfield">
                <label for="pzip">Zip:</label>
                <input type="text"
                       id="pzip" name="pzip"
                       size="12" maxlength="10"
                       value="${not empty rec.pzip ? rec.pzip : varpzip}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="pTelephone">Telephone:</label>
                <input type="tel"
                       id="pTelephone" name="pTelephone"
                       size="21" maxlength="50"
                       value="${not empty rec.pTelephone ? rec.pTelephone : pTel}">
              </span>
              <span class="textfield">
                <label for="pFax">Fax:</label>
                <input type="tel"
                       id="pFax" name="pFax"
                       size="21" maxlength="50"
                       value="${not empty rec.pFax ? rec.pFax : pFax}">
              </span>
            </div><!--formline-->
          </div><!-- addressblock -->

      <hr>

          <!-- Current Address  -->
          <div class="addressblock">
            <div class="formline">
              <span class="textfield">
                <label for="caddressstreet1">Current Address:</label>
                <input type="text" class="newline"
                       id="caddressstreet1" name="caddressstreet1"
                       size="33" maxlength="100"
                       value="${not empty rec.caddressstreet1 ? rec.caddressstreet1 : varcaddressstreet1}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="caddressstreet2" style="display:none;">Current Address - Line 2:</label>
                <input type="text"
                       id="caddressstreet2" name="caddressstreet2"
                       size="33" maxlength="100"
                       value="${not empty rec.caddressstreet2 ? rec.caddressstreet2 : varcaddressstreet2}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="ccity">City:</label>
                <input type="text"
                       id="ccity" name="ccity"
                       size="23" maxlength="50"
                       value="${not empty rec.ccity ? rec.ccity : varccity}">
              </span>
              <span class="textfield">
                <label for="cstate">State:</label>
                <input type="text"
                       id="cstate" name="cstate"
                       size="4" maxlength="2"
                       value="${not empty rec.cstate ? rec.cstate : varcstate}">
              </span>
              <span class="textfield">
                <label for="czip">Zip:</label>
                <input type="text"
                       id="czip" name="czip"
                       size="12" maxlength="10"
                       value="${not empty rec.czip ? rec.czip : varczip}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="cTelephone">Telephone:</label>
                <input type="tel"
                       id="cTelephone" name="cTelephone"
                       size="21" maxlength="50"
                       value="${not empty rec.cTelephone ? rec.cTelephone : cTel}">
              </span>
              <span class="textfield">
                <label for="cFax">Fax:</label>
                <input type="tel"
                       id="cFax" name="cFax"
                       size="21" maxlength="50"
                       value="${not empty rec.cFax ? rec.cFax : cFax}">
            </span>
            </div><!--formline-->
          </div><!-- addressblock -->

      <hr>

          <!-- Work Address  -->
          <div class="addressblock">
            <div class="formline">
              <span class="textfield">
                <label for="waddressstreet1">Work Address:</label>
                <input type="text" class="newline"
                       id="waddressstreet1" name="waddressstreet1"
                       size="33" maxlength="100"
                       value="${not empty rec.waddressstreet1 ? rec.waddressstreet1 : varwaddressstreet1}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="waddressstreet2" style="display:none;">Work Address - Line 2:</label>
                <input type="text"
                       id="waddressstreet2" name="waddressstreet2"
                       size="33" maxlength="100"
                       value="${not empty rec.waddressstreet2 ? rec.waddressstreet2 : varwaddressstreet2}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="wcity">City:</label>
                <input type="text"
                       id="wcity" name="wcity"
                       size="23" maxlength="50"
                       value="${not empty rec.wcity ? rec.wcity : varwcity}">
              </span>
              <span class="textfield">
                <label for="wstate">State:</label>
                <input type="text"
                       id="wstate" name="wstate"
                       size="4" maxlength="2"
                       value="${not empty rec.wstate ? rec.wstate : varwstate}">
              </span>
              <span class="textfield">
                <label for="wzip">Zip:</label>
                <input type="text"
                       id="wzip" name="wzip"
                       size="12" maxlength="10"
                       value="${not empty rec.wzip ? rec.wzip : varwzip}">
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield">
                <label for="wTelephone">Telephone:</label>
                <input type="tel"
                       id="wTelephone" name="wTelephone"
                       size="21" maxlength="50"
                       value="${not empty rec.wTelephone ? rec.wTelephone : wTel}">
              </span>
              <span class="textfield">
                <label for="wFax">Fax:</label>
                <input type="tel"
                       id="wFax" name="wFax"
                       size="21" maxlength="50"
                       value="${not empty rec.wFax ? rec.wFax : wFax}">
              </span>
            </div><!--formline-->
          </div><!-- addressblock -->

      <hr>

          <div class="formline">
            <span class="textfield">
              <label for="cellPhone">Cell Phone:</label>
              <input type="tel"
                     id="cellPhone" name="cellPhone"
                     size="21" maxlength="50"
                     value="${not empty rec.cellPhone ? rec.cellPhone : cellPhone}">
            </span>
          </div><!--formline-->

          <div class="formline">
            <span class="textfield">
              <label for="email">Email:</label>
              <input type="email"
                     id="email" name="email"
                     size="52" maxlength="255"
                     value="${not empty rec.email ? rec.email : personaldata.email}">
            </span>
          </div><!--formline-->

          <input type="hidden" name="emailRecID" value="${not empty rec.emailRecID ? rec.emailRecID : personaldata.emailRecID}">
          <div class="formline">
            <span class="textfield">
              <label for="url">Web Site:</label>
              <input type="url"
                     id="url" name="url"
                     size="52" maxlength="255"
                     value="${not empty rec.url ? rec.url : personaldata.website}">
            </span>
          </div><!--formline-->

          <input type="hidden" name="urlRecID" value="${not empty rec.urlRecID ? rec.urlRecID : personaldata.websiteRecID}">
          <div class="formline">
            <span class="textfield">
              <label for="dob">Date of Birth:</label>
              <input type="text" class="datepicker"
                     id="dob" name="dob"
                     size="11" maxlength="20"
                     value="${not empty rec.dob ? rec.dob : personaldata.birthDate}">
            </span>
          </div><!--formline-->

          <c:set var="isCitizen" value="${not empty personaldata.USCitizen ? personaldata.USCitizen==true : false}"/>

          <c:if test="${not empty rec.usCitizenInd}">
            <c:set var="isCitizen" value="${rec.usCitizenInd == 1}"/>
          </c:if>

          <div class="formline">
            <fieldset class="mivfieldset">
                <legend></legend>
                <span class="radioset">
                    US Citizen:
                    <span class="radiobutton">
                    <label for="usCitizenIndYes">Yes</label>
                    <input type="radio" name="usCitizenInd" id="usCitizenIndYes"
                           value="1" ${isCitizen?"checked":""}>
                    </span>
                    <span class="radiobutton">
                    <label for="usCitizenIndNo">No</label>
                    <input type="radio" name="usCitizenInd" id="usCitizenIndNo"
                           value="0" ${!isCitizen?"checked":""}>
                    </span>
                </span><!-- radioset -->
                <span class="textfield">
                    <label for="dateEntered">If No, Date Arrived:</label>
                    <input type="text" class="datepicker"
                           id="dateEntered" name="dateEntered"
                           size="11" maxlength="20"
                           value="${not empty rec.dateEntered ? rec.dateEntered : personaldata.dateEntered}">
                </span>
            </fieldset>
          </div><!--formline-->

          <div class="formline">
            <span class="textfield">
            <label for="visaType">Type Of Visa:</label>
            <input type="text"
                   id="visaType" name="visaType"
                   size="12" maxlength="20"
                   value="${not empty rec.visaType ? rec.visaType : personaldata.visaType}">
            </span>
          </div><!--formline-->

          <div class="buttonset">
            <input type="hidden" name="perRecID"  value="${not empty rec.perRecID ? rec.perRecID : personaldata.recordID}">
            <input type="submit" id="save" name="save" value="Save" title="Save">
            <input type="reset" id="resetPersonalForm" name="resetPersonalForm" value="Reset" title="Reset">
          </div><!-- buttonset -->

        </form>
      </div><!-- mivDialog -->
    </div><!-- mainDialog -->
  </div><!-- main -->
  <jsp:include page="/jsp/mivfooter.jsp" />
</body>
</html><%--
 vim:ts=8 sw=4 sr et:
--%>
