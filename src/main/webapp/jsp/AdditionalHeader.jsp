<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/>
<div>
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
</div>

<!-- This is the data entry form for "${recordTypeName}" records -->
  <script type="text/javascript"><!--
    mivFormProfile = {
          required: [ "value" ],
          trim: ["value" ]
        };
  // -->
  </script>
<form class="mivdata" id="mytestform" action="<c:url value='${constants.config.saveservlet}'/>" method='post'>
  <input type="hidden" name="headerid" id="headerid" value="${rec.id}"/>
  <input type="hidden" name="documentid" id="documentid" value="${documentid}"/>
  <input type="hidden" id="sectionname" name="sectionname" value="${sectionname}">
  <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
  <input type="hidden" id="subtype" name="subtype" value="${subtype}">

  <div class="formline">
  <span class="textfield" title="${constants.tooltips.header}">
    <label for="value" class="f_required">${constants.labels.value}</label>
    <input type="text"
           name="value" id="value" value="${rec.value}" size="50" maxlength="100" />
  </span>
  </div><!-- formline -->

  <div class="buttonset">
    <input type="submit" name="save" id="save" value="Save" title="Save" />
    <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
  </div>
</form>
<%--
 vi: se ts=8 sw=2 et:
--%>
