<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="pktypeName" value="${packetid == 0 ? 'Master' : 'Packet'}"/><%--
--%><c:set var="pkclassName" value="${packetid == 0 ? 'master' : 'packet'}"/><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; Manage ${pktypeName} ${annotationname} Annotations</title>
    <script type="text/javascript">
    /* <![CDATA[ */
        djConfig = {
            isDebug: false,
            parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
        };
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/FloatingPane.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/ResizeHandle.css'/>">

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivuser.css'/>" />
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/annotation.css'/>" />

    <script type="text/javascript" src="<c:url value='/js/jquery-ui.plugins.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/annotation_new.js'/>"></script>

    <!-- param id - ${param.recid} -->
    <c:if test="${not empty recid}">
    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dojox/layout/FloatingPane.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/charpalette.js'/>"></script>
    </c:if>

<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body class="tundra ${pkclassName}"><%-- The class "pkclassName" (master or packet) is used both for style and javascript selectors. --%>

<c:if test="${empty rec}">
<c:set var="annotationFormClass" scope="page" value="hidden"/>
</c:if>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
<div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    <c:if test="${packetid != 0}">
    &gt; <a href="<c:url value='/packet'/>" title="Packet Management">Packet Management</a>
    </c:if>
    &gt; Manage ${pktypeName} ${annotationname} Annotations
</div><!-- breadcrumbs -->


<div id="main"><!-- MIV Main Div --><%-- eclipse reports: "No end tag (</div>)" ... That is correct, there is Intentionally no end tag for div-main --%>

<h1>Manage ${pktypeName} ${annotationname} Annotations</h1>

    <div id="loading" class="loading">Loading...</div>
    <script type="text/javascript">
    	showLoadingDiv();
    </script>

<div id="AnnotationLineForm" class="${annotationFormClass}">
<c:set var="annotationtext" scope="page" value="Add a line to this ${annotationname} entry."/>
<c:if test="${annotationtype=='publications' || annotationtype=='creativeactivities'}">
	<c:set var="annotationtext" scope="page" value="Add a line, a notation, a footnote or a combination to this ${annotationname} entry."/>
</c:if>

<c:if test="${not empty param.action && param.action == 'save'}">
<div id="CompletedMessage" class="hasmessages">
      <div id="successbox">${annotationname} annotation has been saved successfully.</div>
</div>
<script type="text/javascript">
        $("#CompletedMessage").removeClass("hasmessages");
</script>
</c:if>

<h2>${annotationtext}</h2>

<div class="mivDialog">
<input type="hidden" id="actionResult" name="actionResult"  value="${param.actionResult}">
<input type="hidden" id="selectedRecords" name="selectedRecords"  value="${param.selectedRecords}">

<form name="AnnotationLineForm" action="Annotations" method="post">
<%-- old params --%>
<input type="hidden" name="mode" value="${param.mode}">
<%-- renamed (2007-04-20) params --%>
<input type="hidden" id="annotationAction" name="annotationAction"  value="${param.actionType}"><%-- needed for annotations.js to work --%>
<input type="hidden" id="annotationRecType" name="type"  value="${type}">
<input type="hidden" id="annotationRecId" name="recid"  value="${recid}"><%-- record these annotations are attached to --%>
<input type="hidden" id="annotationRecSection" name="section"  value="${section}"><%-- section of record --%><%--       WTF?!!?!  changed duplicated ID "annotationRecId" to "annotationRecSection" --%><%-- and I think this "section" is not needed. Yes, it's referenced in Annotations.saveAnnotation, but that could use "type" (rectype) instead.  Annotations should not care about Sections, only record type and number. --%>
<input type="hidden" id="annotationPacketId" name="packetid"  value="${packetid}"><%-- packet id of record --%>
<input type="hidden" id="annotationtype" name="annotationtype"  value="${annotationtype}"><%-- annotation type --%>
<input type="hidden" name="idBefore" value="${rec.idBefore}"><%-- 'line before' record # from annotation table --%>
<input type="hidden" name="idAfter" value="${rec.idAfter}"><%-- 'line after' record # from annotation table --%>
<input type="hidden" id="annotationname" value="${annotationname}">
<input type="hidden" id="display" name="display"  value="${display}">
<input type="hidden" id="requestType" name="requestType"  value="save-annotations">
<c:if test="${annotationtype=='publications' || annotationtype=='creativeactivities'}">
        <input type="hidden" id="isNotation" name="isNotation"  value="true">
</c:if>
<input type="hidden" id="annotationId" name="annotationId"  value="${rec.annotationId}">


<br>

<!-- Palette look for field by the name of "errormessage" to insert maxlength exceeded message -->
<div dojotype="dijit.layout.ContentPane" id="errormessage"></div>


<fieldset class="fieldblock">
	<legend>${annotationname} Design Preview</legend>
	${rec.preview}
</fieldset>
<br>
<fieldset class="fieldblock">
	<legend>Design</legend>

<table class="annotationform"><%-- Using a table to lay out a form. I'm going to hurl now. --%>

  <tr>
  <!-- Don't show the "Include in packet" radio buttons for master annotations (packetid = 0) -->
  <c:choose>
    <c:when test="${packetid > 0}">      
      <td class="annotation-field-label" width="30%"><label for="display-record" >Include in this packet?</label></td>
      <td width="70%">
        <span class="tooltip" title="Yes"><%-- OMG! The tooltip is "Yes"!  ...  That clarifies EVERYTHING! --%>
                <input type="radio" name="display-record" id="display-record-true"
                    value="1"${rec.display==1?" checked":""}>
                    &nbsp;<label for="display-record-true">Yes</label>&nbsp;
        </span>
        <span class="tooltip" title="No">
                <input type="radio" name="display-record" id="display-record-false"
                    value="0"${empty rec.display || rec.display==0?" checked":""}>
                    &nbsp;<label for="display-record-false">No</label>&nbsp;
        </span>
        <input type="hidden" name="displayBefore" value="${rec.displayBefore}">
      </td>
    </c:when>
    <c:otherwise>
      <td width="30%"></td>
      <td width="70%"></td>
    </c:otherwise>       
  </c:choose>
  </tr>

  <!-- Label Above -->
  <tr>
    <th colspan="2"  align="center">
      <h2>Draw a line <em>above</em> with label ${annotationmessage}</h2>
    </th>
  </tr>

  <tr>
    <td class="annotation-field-label">&#8224;&nbsp;<label for="labelBefore" >Label Above:</label></td>
    <td>
      <span class="tooltip" title="Label Above">
        <textarea id="labelBefore" name="labelBefore" ${packetid == 0 ? '' : 'disabled'} wrap="soft">${rec.labelBefore}</textarea>
      </span>
      <input type="hidden" name="displayBefore" value="${rec.displayBefore}">
    </td>
  </tr>

  <tr>
    <td class="annotation-field-label">Label Placement:</td>
    <td><label for="justifyBeforeLeft">Left Justify</label>&nbsp;
      <span class="tooltip" title="Left Justify">
      <input type="radio" name="justifyBefore" id="justifyBeforeLeft" ${packetid == 0 ? '' : 'disabled'}
             value="left"${rec.justifyBefore == "false" ? " checked" : ""}><%--"--%>
          &nbsp;<label for="justifyBeforeRight">Right Justify</label>&nbsp;
      </span>
      <span class="tooltip" title="Right Justify">
        <input type="radio" name="justifyBefore" id="justifyBeforeRight" ${packetid == 0 ? '' : 'disabled'}
               value="right"${rec.justifyBefore == "true" || empty rec.justifyBefore ? " checked" : ""}><%--"--%>
      </span>
    </td>
  </tr>

  <!-- Label Below -->
  <tr>
    <th colspan="2" align="center">
      <h2>Draw a line <em>below</em> with label ${annotationmessage}</h2>
    </th>
  </tr>

  <tr>
    <td class="annotation-field-label">&#8224;&nbsp;<label for="labelAfter">Label Below:</label></td>
    <td>
      <span class="tooltip" title="Label Below">
        <textarea id="labelAfter" name="labelAfter" ${packetid == 0 ? '' : 'disabled'} wrap="soft">${rec.labelAfter}</textarea>
      </span>
      <input type="hidden" name="displayAfter" value="${rec.displayAfter}">
    </td>
  </tr>
  <tr>
    <td class="annotation-field-label">Label Placement:</td>
    <td><label for="justifyAfterLeft">Left Justify</label>&nbsp;
    <span class="tooltip" title="Left Justify">
    <input type="radio" name="justifyAfter" id="justifyAfterLeft" ${packetid == 0 ? '' : 'disabled'}
           value="left"${rec.justifyAfter == "false" ?" checked":""}><%--"--%>
    &nbsp;<label for="justifyAfterRight">Right Justify</label>&nbsp;
    </span>
    <span class="tooltip" title="Right Justify">
    <input type="radio" name="justifyAfter" id="justifyAfterRight" ${packetid == 0 ? '' : 'disabled'}
           value="right"${rec.justifyAfter == "true" || empty rec.justifyAfter?" checked":""}><%--"--%>
    </span>
    </td>
  </tr>

<c:if test="${annotationtype=='publications' || annotationtype=='creativeactivities'}">
  <!-- Line Annotations -->
  <tr>
    <th colspan="2"  align="center">
    	<h2>Add a notation to this line</h2>
    </th>
  </tr>

  <tr>
    <td rowspan="4" class="annotation-field-label">Select ${annotationname} notations:</td>
  </tr>

  <c:if test="${packetid != 0}">
  <tr>
    <td>
    <span class="tooltip" title="${annotationname} included in the review period">
        <input type="checkbox" id="x_included" name="included"${rec.fIncluded?" checked":""}>&nbsp;
        <label for="included">* = ${annotationname} included in the review period</label>
    </span><!-- tooltip -->
    </td>
  </tr>

  <tr>
    <td>
    <span class="tooltip" title="Most significant works">
        <input type="checkbox" id="x_significant" name="significant"${rec.fSignificant?" checked":""}>&nbsp;
        <label for="significant">x = Most significant works</label>
    </span><!-- tooltip -->
    </td>
  </tr>
  </c:if>

  <c:if test="${packetid == 0}">
  <tr>
    <td>
    <span class="tooltip" title="Major mentoring role">
        <input type="checkbox" id="x_mentoring" name="mentoring"${rec.fMentoring?" checked":""}>&nbsp;
        <label for="mentoring">+ = Major mentoring role</label>
    </span><!-- tooltip -->
    </td>
  </tr>

  <tr>
    <td>
    <span class="tooltip" title="Refereed">
        <input type="checkbox" id="x_refereed" name="refereed"${rec.fRefereed?" checked":""}>&nbsp;
        <label for="refereed">@ = Refereed</label>
    </span><!-- tooltip -->
    </td>
  </tr>
  </c:if>

  <!-- Footnotes -->
  <tr>
    <th colspan="2" align="center">
    	<h2> Add a footnote to this line </h2>
    </th>
  </tr>

  <tr>
    <td class="annotation-field-label">&#8224;&nbsp;<label for="footnote">Footnote:</label>
    <br/><span class="small">Footnotes are indentified with a "#" in front of the record number.</span>
    </td>
    <td>
    <span class="tooltip" title="Footnote">
    <textarea name="footnote" id="footnote" wrap="soft">${rec.footnote}</textarea>
    </span>
    </td>
  </tr>
</c:if>

</table>

</fieldset>
<!-- End of Form Table -->

<div class="buttonset">
<input type="submit" value="Save" title="Save" name="Save">
<input type="submit" value="Cancel" title="Cancel" name="Cancel">
</div>
</form>

</div> <!-- mivDialog End -->

<div id="lineBreakNote">
      <span class="legend">&#8224;</span> = <span class="description">Field supports line breaks (by pressing the "Enter" key on the keyboard),
      and the data will appear with line breaks in MIV documents.</span>
</div>
<hr>
</div><!--#AnnotationLineForm-->


<div id="selectheader">
<h2> Select a ${annotationname} from the preview area below to edit.</h2>
</div>
<!-- FIXME: These buttons, from annotation.js, should be in this .jsp file instead.
$('div.section-buttonset').append('<input type="button" id="addnotations" value="Add Notations" title="Add notations on all selected records" disabled>');
$('div.section-buttonset').append('<input type="button" id="clearnotations" value="Clear Selected Records" title="Clear selected records" class="hidden">');

<div class="buttonset section-buttonset right">
    <a class="linktobutton" title="Show all records" href="${recordUrl}&display=false">Show All Records</a>
    <input type="button" id="addnotations" value="Add Notations" title="Add notations on all selected records" disabled>
    <input type="button" id="clearnotations" value="Clear Selected Records" title="Clear selected records" class="hidden">
</div>
-->
<%-- The rest of this page is generated by the Annotations servlet --%>
