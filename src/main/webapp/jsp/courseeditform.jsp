<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>

<script type="text/javascript"><!--
    mivFormProfile = {
      required: [ ${constants.config.required} ],
      trim: ["year", "title", "coursenumber", "units",
             "graduatecount", "effort"],
      constraints: {
        year: function(contents) {
            var curYear = new Date().getFullYear();
            
            if(isNaN(contents)){
            	return "INVALID_NUMBER";
            }
            
            var numyear = contents * 1;
            
            if( numyear < 1900 || numyear > (curYear +1) ){
            	return "INVALID_YEAR";
            }
            
            return true;
        },
        termtypeid: miv.validation.isDropdown
      },
      errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}" }
    };
    
// -->
</script>


<div>
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
    <div id="links">
        <%-- <div id="specchar"><a href="#">Add Special Characters</a></div>
             div id="format"><a href="#">Bold / Italic / Underline</a></div --%>
    </div>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">

<input type="hidden" id="typeid" name="typeid" value="1"><%-- 1==Course --%>
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<div class="formline">
<!-- Year -->
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  
  <input type="text" id="year" name="year" size="5" maxlength="4"
			value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
</span>

<!-- Quarter -->
<span class="dropdown" title="${constants.tooltips.termtypeid}">
<!-- stun(12/13/07) -->
  <label for="termtypeid" class="f_required">${constants.labels.termtypeid}</label>
  <select id="termtypeid" name="termtypeid">
  <c:forEach var="opt" items="${constants.options.termtype}"><%--
--%>    <option value="${opt.id}"${rec.termtypeid==opt.id?' selected="selected"':''}>${opt.name}</option>
  </c:forEach><%--
--%></select>
</span>
</div><!-- formline -->

<div class="formline">
<!-- Course title -->
<span class="textfield" title="${constants.tooltips.title}">
  <label for="title" class="f_required">${constants.labels.title}</label><br />
  <textarea id="title" name="title"
            rows="3" cols="54" wrap="soft"
            >${rec.title}</textarea>
</span>
</div><!-- formline -->

<div class="formline">
<!-- Course Number -->
<span class="textfield" title="${constants.tooltips.coursenumber}">
  <label for="coursenumber">${constants.labels.coursenumber}</label>
  <input type="text"
         id="coursenumber" name="coursenumber"
         size="10" maxlength="20"
         value="${rec.coursenumber}"
         >
</span>

<!-- Units -->
<span class="textfield" title="${constants.tooltips.units}">
  <label for="units">${constants.labels.units}</label>
  <input type="text" class="f_decimal"
         id="units" name="units"
         size="4" maxlength="4"
         value="${rec.units}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Undergraduate Count -->
<span class="textfield" title="${constants.tooltips.undergraduatecount}">
  <label for="undergraduatecount">${constants.labels.undergraduatecount}</label>
  <input type="text" class="f_numeric"
         id="undergraduatecount" name="undergraduatecount"
         size="6" maxlength="6"
         value="${rec.undergraduatecount}"
         >
</span>

<!-- Graduate Count -->
<span class="textfield" title="${constants.tooltips.graduatecount}">
  <label for="graduatecount">${constants.labels.graduatecount}</label>
  <input type="text" class="f_numeric"
         id="graduatecount" name="graduatecount"
         size="6" maxlength="6"
         value="${rec.graduatecount}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Percent Effort -->
<span class="textfield" title="${constants.tooltips.effort}">
  <label for="effort">${constants.labels.effort}</label>
  <input type="text"
         id="effort" name="effort"
         size="6" maxlength="4"
         value="${rec.effort}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
