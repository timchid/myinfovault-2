<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%>

<!DOCTYPE html>
<html>
<head>
  <title>UC Davis: MyInfoVault &ndash; Main Navigation</title>
</head>
<body>
<%-- assigndoctype --%>
<h1>
Assign a Type to Document
</h1>
<div id="docTypes" style="formline">
<select id="documentTypes" size="7">
   <c:forEach var="item" items="${let.oletype}">
    <option value="${item.id}">${item.description}</option>
   </c:forEach>
</select>
</div>
</body>
</html>