<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>MIV &ndash; View Kuali Document</title>

  <script type="text/javascript">
  /* <![CDATA[ */
      var mivConfig = new Object();
      mivConfig.isIE = false;
  /* ]]> */
  </script>
  <%@ include file="/jsp/metatags.html" %>
  
  <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
  <!-- MIV Header Div -->
  <jsp:include page="/jsp/mivheader.jsp" />

  <!-- Breadcrumbs Div -->
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; View Kuali Document
  </div><!-- breadcrumbs -->

  <!-- MIV Main Div -->
  <div id="main">
        <h1>View Kuali Document</h1>
        <p>We're sorry, but this MIV feature is not currently available. Please select <a href="<c:url value='/MIVMain'/>" title="Home">Home</a> for the MIV Main Menu</p>
  </div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html><%--
vi: se ts=8 sw=2 et:
--%>
