<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/>
<div>
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
</div>

  <script type="text/javascript"><!--
    mivFormProfile = {
          required: [ "header" ],
          trim: ["header" ]
        };
  // -->
  </script>
<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='/SectionHeader'/>">

<input type="hidden" id="sectionid" name="sectionid" value="${sectionid}">
<input type="hidden" id="addheaderid" name="addheaderid" value="${addsectionid}">

<!-- Header Description -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.header}">
  <label for="header"  class="f_required">${constants.labels.header}</label>
  <input type="text"
         id="header" name="header"
         size="50" maxlength="100"
         value="${sectionheader}"
         >
  <%-- <c:if test="${fn:length(error)>0}"> 
    <div style="color:#F00000;">${constants.strings.headerblank}</div>
  </c:if> --%>
</span>
</div><!-- formline -->

<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save"/>
 <input type="submit" id="reset" name="reset" value="Cancel" title="Cancel"><script type="text/javascript">disableCancel();</script>
</div>

</form>
