<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE html><%--
--%><c:set var="actionName" value="Edit"/><%--
--%><c:set var="recordNum" value="#${rec.id}"/><%--
--%><c:if test="${empty rec.id}"><%--
--%><c:set var="actionName" value="Add"/><%--
--%><c:set var="recordNum" value=""/><%--
--%></c:if>
<html class="enterdata">
    <c:set var="recordTypeAdd" value="additional"/><%--
--%><c:set var="recordTypeAddHead" value="additionalheader"/>
<head>
    <title>
    MIV &ndash;
    ${actionName} <%--
--%><c:if test="${recordTypeName == recordTypeAdd}">Record</c:if><%--
--%><c:if test="${recordTypeName == recordTypeAddHead}">Section</c:if><%--
--%>${constants.strings.pagetitle}<c:if test="${fn:length(constants.strings.subtitle)>0 and fn:length(constants.strings.pagetitle)>0}">: </c:if><%--
--%><c:if test="${fn:length(constants.strings.subtitle)>0}">${constants.strings.subtitle}</c:if>
    </title>

    <%-- ${actionName} record ${recordNum} in --%>
    <script type="text/javascript">
    /* <![CDATA[ */
        djConfig = {
            isDebug: false,
            parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
        };
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/FloatingPane.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/ResizeHandle.css'/>">

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dojox/layout/FloatingPane.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/charpalette.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <jwr:script src="/bundles/mivvalidation.js" />
    <script type="text/javascript" src="<c:url value='/js/editform.js'/>"></script>

<c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:set var="refererbase" value="${fn:replace(fn:replace(fn:replace(header.referer,'CompletedMessage','zremoved'),'<','&lt;'),'\"','&amp;quot;')}"/><%-- ' quote to fix syntax highlighting --%>
</head>

<body>
<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);  // 'thisYear' is used in several pages that get included below as the 'editorform'
   ]]></jsp:scriptlet>

  <!-- Adding report help file link -->
  <c:set var="helpPage" scope="request" value="${constants.config.helpfile}"/>
  <jsp:include page="/jsp/mivheader.jsp" />
  <!-- Breadcrumbs Div -->
  <c:set var="prev_pagedescription" value="${constants.strings.description} List" />
  <c:set var="current_pagedescription" value="${constants.strings.description} Form" />

  <c:set var="mainRecType" value="${recordTypeName}" />
  <c:if test="${not empty mainRecType && mainRecType == 'additionalheader' }">
      <c:set var="mainRecType" value="additional" />
  </c:if>

  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; <a href="<c:url value='${constants.config.itemlistservlet}?type=${mainRecType}&recordClass=${recordClass}&sectionname=${sectionname}&documentid=${documentid}&subtype=${subtype}'/>" title="${prev_pagedescription}">${prev_pagedescription}</a>
    &gt; ${current_pagedescription}
  </div><!-- breadcrumbs -->


  <!-- MIV Main Div -->
  <div id="main">
    <h1>${actionName} <%--
  --%><c:if test="${recordTypeName == recordTypeAdd}">Record</c:if><%--
  --%><c:if test="${recordTypeName == recordTypeAddHead}">Section</c:if><%--
 --%> &ldquo;${constants.strings.description}&rdquo;</h1>
    <!-- record [${recordNum}] (blank if adding) -->
    <c:if test="${fn:length(rec.updatetimestamp) > 0}">
      <div id="timestamp">This record was last edited ${rec.updatetimestamp}.</div>
    </c:if>

    <div id="errormessage" style="${(not empty success && success == false && not empty CompletedMessage) ? "display:block;":"" }">
      ${CompletedMessage}
    </div>

    <%-- Only non-upload record types have required fields or formatting --%>
    <c:if test="${not fn:containsIgnoreCase(recordTypeName,'upload')}">
    <div id="enterdataguide">
      <div id="requiredfieldlegend">* = Required Field</div>
      <div id="specialformattinghelp"><a href="<c:url value='/help/special_formatting.html'/>" title="Do you want to format text?" target="mivhelp">Do you want to format text?</a></div>
    </div>
    </c:if>

    <div id="maindialog" class="pagedialog"><%-- This is a single-page, Not a pop-up, dialog --%>
      <div class="dojoDialog mivDialog"><!--might want to be more flexible with dialog widths-->
      <jsp:include page="${editorform}" />
      </div><!-- mivDialog -->
      <script type="text/javascript"> /* <![CDATA][ */
        var saveButton=dojo.byId("save");
        if (saveButton) {
           saveButton.disabled=true;
        }
      /* ]]> */</script>
      <c:if test="${fn:length(constants.config.translatenewline)>0 || constants.config.allowmarkup==true}">
      <div id="lineBreakNote">
      <span class="legend">&#8224;</span> = <span class="description">Field supports line breaks (by pressing the "Enter" key on the keyboard),
      and the data will appear with line breaks in MIV documents.</span>
      </div>
      </c:if>
      <a id="referer" style="display:none;" href="${refererbase}">${refererbase}</a>
    </div><!-- pagedialog -->

  </div><!-- main -->

<jsp:include page="/jsp/mivfooter.jsp" />

</body>
</html>
<%--
 vim:ts=8 sw=2 et:
--%>
