<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="spring" uri="/spring"%><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; ${constants.strings.designmybiosketch}</title>
    <%@ include file="/jsp/metatags.html"%>
    <script type="text/javascript">
    /* <![CDATA[ */
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivDesign.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/special_character2.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/biosketch.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivuser.css'/>">
    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/biosketch.js'/>"></script>
    <c:set var="user" scope="request" value="${MIVSESSION.user}" />
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; <a href="<c:url value='/biosketch/BiosketchPreview'/>" title="${constants.strings.description} List">${constants.strings.description} List</a>
        &gt; <a href="<c:url value='/biosketch/BiosketchMenu'/>" title="${constants.strings.description} Menu">${constants.strings.description} Menu</a>
        &gt; ${constants.strings.designmybiosketch}
    </div><!-- breadcrumbs -->

    <div id="main" class="designer">
        <h1 class="biosketchheader">${constants.strings.designmybiosketch}</h1>
        <c:if test="${showwizard =='true'}">
        <div id="wizardsteps">
            <p>Following are the steps to create your ${constants.strings.description}. You are currently at Step 3.</p>
            <div class="inactivestep"><span class="step">Step 1:</span> ${constants.strings.displayselectoptionstitle}</div>
            <div class="inactivestep"><span class="step">Step 2:</span> ${constants.strings.selectbiosketchdata}</div>
            <div class="activestep"><span class="step">Step 3:</span> ${constants.strings.designmybiosketch}</div>
            <div class="inactivestep"><span class="step">Step 4:</span> ${constants.strings.createmybiosketch}</div>
        </div><!-- wizardsteps -->
        <div id="cancelurl"><c:url value='/biosketch/BiosketchPreview'/></div><%--
    --%></c:if>

        <!-- <div id="maindialog" class="dojoDialog mivDialog pagedialog"> -->
            <form class="mivdata designmybiosketch" id="biosketchform" name="designForm" method="post" action="<c:url value='/biosketch/DesignMyBiosketch'/>">
                <c:set var="saveLabel" value="${showwizard ? 'Proceed' : 'Save'}"/>
                
                <div class="designbuttongroup">
                    <div class="buttonset left">
                        <input type="submit" name="btnSubmit" value="${saveLabel}" title="Save">
                        <input type="reset" name="popupformcancel" id="popupformcancel" value="Cancel" title="Cancel">
                    </div><!-- buttonset -->
                    
                    <div class="buttonset right checkuncheck-buttongroup">
			        	<input type="button" title="Check All" value="Check All" onclick="javascript:checkUncheckAll($('#biosketchform'),true)" />
			          	<input type="button" title="Clear All" value="Clear All" onclick="javascript:checkUncheckAll($('#biosketchform'),false)" />
			        </div>
                </div>

                <div id="workarea">
                    <div class="printpacket">
                        <div class="selectoption">Print</div>
                        <div class="contentlegend">${biosketchDesignCommand.biosketchName} Content</div>
                    </div>
                    <c:set var="sections" value="${biosketchDesignCommand.recordList}" /><%--
                --%><c:forEach items="${sections}" varStatus="displayRow"><%--
                --%><%-- evaluate the design record type --%><%--
                --%><c:set var="designType" value="${biosketchDesignCommand.recordList[displayRow.index].type}" /><%--
                --%><c:set var="state" value="${biosketchDesignCommand.recordList[displayRow.index].disabled}" /><%--

                --%><c:choose><%--

                --%><c:when test="${designType=='HEADING'}">
                    <!-- heading row -->
                    <div class="documenthead">
                        <c:choose>
                            <c:when test="${biosketchDesignCommand.recordList[displayRow.index].displayable}">
                                <spring:bind path="biosketchDesignCommand.recordList[${displayRow.index}].display"><%--
                                --%><span class="printoption">
                                        <%--
                                    --%><input type="hidden" name="_${status.expression}">
                                        <input type="checkbox" name="${status.expression}" id="${status.expression}"
                                               value="true" title="Check to include this item"
                                               ${state ? ' disabled' : ''} ${status.value && !state ? ' checked' : ''}>
                                            &nbsp;
                                    </span>

                                    <h2><label for="${status.expression}">${biosketchDesignCommand.recordList[displayRow.index].preview}</label></h2>
                                </spring:bind>
                            </c:when>

                            <c:otherwise><%--
                                --%><h2>${biosketchDesignCommand.recordList[displayRow.index].preview}</h2>
                            </c:otherwise><%--
                    --%></c:choose>
                    </div><!--documenthead-->
                    </c:when><%--

                --%><c:when test="${designType=='DUMMYHEADING'}">
                    <!-- dummyheading row -->
                    <div class="documenthead">
                        <c:choose>
                            <c:when test="${biosketchDesignCommand.recordList[displayRow.index].displayable}">
                                <spring:bind path="biosketchDesignCommand.recordList[${displayRow.index}].display">
                                    <span class="printoption">
                                        <%--
                                    --%><input type="hidden" name="_${status.expression}">
                                        <input type="checkbox" name="${status.expression}" id="${status.expression}"
                                               value="true" title="Check to include this item"
                                               ${state ? ' disabled' : ''} ${status.value && !state ? ' checked' : ''}>
                                            &nbsp;
                                    </span><!--printoption-->

                                    <h2><label for="${status.expression}">${biosketchDesignCommand.recordList[displayRow.index].preview}</label></h2>
                                </spring:bind>
                            </c:when>

                            <c:otherwise>
                                <h2>${biosketchDesignCommand.recordList[displayRow.index].preview}</h2>
                            </c:otherwise>
                        </c:choose>
                    </div><!--documenthead-->
                    </c:when><%--

                --%><c:when test="${designType=='SUBHEADING'}">
                        <!-- subheading row -->
                        <c:if test="${!state}"> <!-- MIV-4311 : Remove all sections with no data on Design My CV Page- Step3 -->
                            <div class="section">
                                <div class="sectionhead">
                                    <c:choose>
                                        <c:when test="${biosketchDesignCommand.recordList[displayRow.index].displayable}">
                                            <spring:bind path="biosketchDesignCommand.recordList[${displayRow.index}].display">
                                                <span>
                                                    <input type="hidden" name="_${status.expression}">
                                                    <input type="checkbox" name="${status.expression}" id="${status.expression}"
                                                           value="true" title="Check to include this item"
                                                           ${state ? ' disabled' : ''}${status.value && !state ? ' checked' : ''}>
                                                    <%--
                                            --%></span>

                                                <h3><label for="${status.expression}">${biosketchDesignCommand.recordList[displayRow.index].preview}</label></h3>
                                            </spring:bind>
                                        </c:when>

                                        <c:otherwise>
                                            <h3>${biosketchDesignCommand.recordList[displayRow.index].preview}</h3>
                                        </c:otherwise>
                                    </c:choose>
                                </div><!-- sectionhead -->
                            </div><!-- section -->
                        </c:if>
                    </c:when><%--

                --%><c:when test="${designType=='ADDITIONALHEADER'}">
                    <!-- subheading row -->
                    <div class="sectionsubhead">
                        <c:choose>
                            <c:when test="${biosketchDesignCommand.recordList[displayRow.index].displayable}">
                                <spring:bind path="biosketchDesignCommand.recordList[${displayRow.index}].display">
                                    <span>&nbsp;
                                        <%--
                                    --%><input type="hidden" name="_${status.expression}">
                                        <input type="checkbox" name="${status.expression}" id="${status.expression}"
                                               value="true" title="Check to include this item"
                                               ${status.value ? ' checked' : ''}>
                                        &nbsp;<%--
                                --%></span>

                                    <h4 style="display: inline;"><label for="${status.expression}">${biosketchDesignCommand.recordList[displayRow.index].preview}</label></h4>
                                </spring:bind>
                            </c:when>

                            <c:otherwise>
                                <h4 style="display: inline;">${biosketchDesignCommand.recordList[displayRow.index].preview}</h4>
                            </c:otherwise>
                        </c:choose>
                    </div><!--sectionsubhead-->
                    </c:when><%--

                --%><c:when test="${designType=='ATTRIBUTE'}">
                    <!-- attribute row -->
                    <div class="listblock">
                        <spring:bind path="biosketchDesignCommand.recordList[${displayRow.index}].display">
                        <div class="printable">
                            <div class="selectoption">
                                <span class="printoption"> &nbsp;<%--
                                --%><c:if test="${biosketchDesignCommand.recordList[displayRow.index].displayable}"><%--
                                --%>
                                    <input type="hidden" name="_${status.expression}">
                                    <input type="checkbox" name="${status.expression}" id="${status.expression}"
                                           value="true" title="Check to include this item"
                                           ${status.value ? ' checked' : ''}${biosketchDesignCommand.recordList[displayRow.index].disabled ? ' disabled' : ''}><%--
                                --%><%--
                                --%></c:if>
                                </span>
                            </div><!--selectoption-->
                        </div><!--printable-->
                        <div>
                            <%-- Hide year field for publicationevents record type because the preview contains year details --%>
                            <c:if test="${not empty biosketchDesignCommand.recordList[displayRow.index].year
                                            && !( biosketchDesignCommand.recordList[displayRow.index].recType == 'publicationevents-published'
                                                  || biosketchDesignCommand.recordList[displayRow.index].recType == 'publicationevents-submitted'
                                                  || biosketchDesignCommand.recordList[displayRow.index].recType == 'publicationevents-in-press' ) }"><%--
                        --%><div class="timeframe">${biosketchDesignCommand.recordList[displayRow.index].year}</div>
                            </c:if><%--
                        --%>
                        <c:choose>
                            <c:when test="${biosketchDesignCommand.recordList[displayRow.index].displayable}">
                                <label for="${status.expression}"><span class="attributepreview">${biosketchDesignCommand.recordList[displayRow.index].attributeName}</span> ${biosketchDesignCommand.recordList[displayRow.index].preview}</label>
                            </c:when>

                            <c:otherwise>
                                <span class="attributepreview">${biosketchDesignCommand.recordList[displayRow.index].attributeName}</span> ${biosketchDesignCommand.recordList[displayRow.index].preview}
                            </c:otherwise>
                        </c:choose>
                        </div>
                        </spring:bind>
                    </div><!--listblock-->
                    </c:when><%--

                --%><c:when test="${designType=='NODATA'}">
                    <%-- nodata indicator --%><%--
                --%><div class="sectionbody">
                        <em style="margin-left: 1em;">${biosketchDesignCommand.recordList[displayRow.index].preview}</em>
                    </div><%--
                --%></c:when><%--

                --%><c:otherwise>
                    <!-- record row ${biosketchDesignCommand.recordList[displayRow.index].recType} -->
                    <div class="listblock">
                        <spring:bind path="biosketchDesignCommand.recordList[${displayRow.index}].display">
                        <div class="selectoption">
                            <span class="printoption"> &nbsp;<%--
                            --%><c:if test="${biosketchDesignCommand.recordList[displayRow.index].displayable}"><%--
                            --%>
                                <input type="hidden" name="_${status.expression}">
                                <input type="checkbox" name="${status.expression}" id="${status.expression}" value="true"
                                       title="Check to include this item"${status.value ? ' checked' : ''}><%--
                            --%><%--
                            --%></c:if>
                            </span>
                        </div><!-- selectoption -->
                        <div class="record">
                            <!-- Hide year field for publicationevents record type because the preview contains year details -->
                            <c:if test="${not empty biosketchDesignCommand.recordList[displayRow.index].year
                                            && !( biosketchDesignCommand.recordList[displayRow.index].recType == 'publicationevents-published'
                                                  || biosketchDesignCommand.recordList[displayRow.index].recType == 'publicationevents-submitted'
                                                  || biosketchDesignCommand.recordList[displayRow.index].recType == 'publicationevents-in-press' )}"><%--
                        --%><div class="timeframe">${biosketchDesignCommand.recordList[displayRow.index].year}</div>
                            </c:if><%--
                        --%><div class="preview">
                            <c:choose>
                                <c:when test="${biosketchDesignCommand.recordList[displayRow.index].displayable}">
                                    <label for="${status.expression}">${biosketchDesignCommand.recordList[displayRow.index].preview}</label>
                                </c:when>

                                <c:otherwise>${biosketchDesignCommand.recordList[displayRow.index].preview}</c:otherwise>
                            </c:choose>
                        </div>
                        </div><!-- record -->
                        <div class="clearboth"></div>
                        </spring:bind>
                    </div><!--listblock-->
                    </c:otherwise><%--

                --%></c:choose><%--

                --%></c:forEach>
                    <div style="float:none;clear:both;"></div><%-- This forces height after all section divs but before the workarea closes.
                                                                   It's needed to get the workarea border to extend all the way down. --%>
                </div><!-- workarea -->
                <div class="designbuttongroup">
                    <div class="buttonset left">
                        <input type="submit" name="btnSubmit" value="${saveLabel}" title="Save">
                        <input type="reset" name="popupformcancel" id="popupformcancelBottom" value="Cancel" title="Cancel">
                    </div><!-- buttonset -->
                    
                    <div class="buttonset right checkuncheck-buttongroup">
			        	<input type="button" title="Check All" value="Check All" onclick="javascript:checkUncheckAll($('#biosketchform'),true)" />
			          	<input type="button" title="Clear All" value="Clear All" onclick="javascript:checkUncheckAll($('#biosketchform'),false)" />
			        </div>
                </div>                
            </form>
        <!-- </div> --><!-- mainDialog -->
    </div><!-- main -->
    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vim:ts=8 sw=4 et sr:
--%>
