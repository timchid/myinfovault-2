<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [],
        trim: ["percenttime", "content"],
        constraints: {
        	percenttime : miv.validation.isInteger
      	},
        errormap: {"INVALID_INTEGER":"Percentage Time must be an integer."}
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>
<p class="formhelp">
Only Federation Titles must enter Position Description data.
</p>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->

<div id="uploadlink"><%--
--%><c:url var="nextLink" value="/PdfUpload"><%--
--%>    <c:param name="_flowId" value="pdfupload-external-flow"/><%--
--%>    <c:param name="documentAttributeName" value="positiondescription"/><%--
--%>    <c:param name="isYearField" value="true"/><%--
--%></c:url>
<a href="<c:out value='${nextLink}' escapeXml='true'/>" title="Upload a PDF">Upload a PDF</a> or enter data in the fields below.
</div>

<jsp:include page="/jsp/wysiwyginit.inc"/>
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Percentage Time -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.percenttime}">
  <label for="percenttime" class="f_required">${constants.labels.percenttime}</label>
  <input type="text"
         class="f_required mceEditor"
         id="percenttime" name="percenttime"
         size="4" maxlength="3"
         value="${rec.percenttime}"
         >
</span>
</div><!-- formline -->

<!-- Content -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.content}">
  &#8224;&nbsp;
  <label for="content" class="f_required">${constants.labels.content}</label><br />
  <textarea
            id="content" name="content" class="mceEditor"
            rows="10" cols="113" wrap="soft"
            >${rec.content}</textarea>
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save" />
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
