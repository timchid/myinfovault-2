<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<%@ include file="/jsp/metatags.html" %>

<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/jquery.qtip.min.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
    
    <script type="text/javascript" src="<c:url value='/js/jquery.qtip.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/qtip_config.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/jquery-ui.plugins.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/diversitystatement.js'/>"></script>

    <style>
    html.js .dojoDialog .formhelp {
        display: none;
    }
    html.js div.mivDialog fieldset legend {
        padding-right: 2em;
    }
    </style>


    <jsp:scriptlet><![CDATA[
        Integer d = Integer.valueOf( java.util.Calendar.getInstance().get(java.util.Calendar.YEAR) );
        pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
    ]]></jsp:scriptlet>

    <script type="text/javascript"><!--
        mivFormProfile = {
            required: ["year"],
            trim: ["year","teachingcontent","servicecontent","researchcontent"],
            constraints: {
                year: function(contents) {
                    var curYear = new Date().getFullYear();

                    if (isNaN(contents)) {
                        return "INVALID_NUMBER";
                    }

                    var numyear = contents * 1;

                    if (numyear < 1900 || numyear > (curYear + 1)) {
                        return "INVALID_YEAR";
                    }

                    return true;
                }
            },
            errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}" }
        };
    // -->
    </script>

    <div id="loading" class="loading" style="display: none;">Loading...</div>
    <div id="userheading">
        <div class="standout">${user.targetUserInfo.displayName}</div>
        <div class="forminfo" alt="Information about ${constants.strings.description} Form"></div>
    </div>

    <div class="formhelp">
    <p>
        Consistent with the requirements of both the UC Davis
        <a href="http://www.ucop.edu/academic-personnel/_files/apm/apm-210.pdf"
           target="mivexternal"> Instructions to Appointment and Promotion Committees: APM-210</a>
        and the UCOP Vice Provost for Academic Personnel
        <a href="http://www.ucop.edu/academic-personnel/_files/documents/eval-contributions-diversity.pdf"
           target="mivexternal">Evaluation Contributions to Diversity for Faculty Appointment and Promotion Under APM-210</a>,
        faculty contributions to diversity and equal opportunity in the four areas of evaluation
        (teaching, university and public service, and scholarly and creative activities) must
        receive recognition and reward in the academic review process for appointment and promotion.
    </p>
    <p>
        In these instructions, the term "diversity activities" includes not
        only these activities contributing to diversity and equal opportunity,
        but also include faculty contributions to affirmative action in these
        four areas of evaluation that are consistent with the
        <a href="http://www.ucop.edu/academic-personnel/_files/documents/affirmative.pdf"
           target="mivexternal">University of California Affirmative Action Guidelines for Faculty Recruitment and Retention</a>.
    </p>
    </div><!-- formhelp -->

    <!-- This is the data entry form for "${recordTypeName}" records -->
    <jsp:include page="/jsp/wysiwyginit.inc"/>
    <form class="mivdata" id="mytestform" name="mytestform"
          method="post" action="<c:url value='${constants.config.saveservlet}'/>">

        <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
        <input type="hidden" id="recid" name="recid" value="${rec.id}">
        <input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">


        <div class="formline">
            <span class="textfield" title="${constants.tooltips.year}">
                <label for="year" class="f_required">${constants.labels.year}</label>
                <input type="text" id="year" name="year" size="5" maxlength="4"
                       value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
            </span>
        </div><!-- formline -->

        <div id="designForm">
            <!-- Teaching Content -->
            <fieldset class="mivcollapsible">
                <legend>${constants.tooltips.teachingcontent}</legend>
                <label for="teachingcontent" class="hidden">${constants.labels.teachingcontent}</label>
                <%-- <div class="hint" > expand to see ${constants.tooltips.teachingcontent} fields</div> --%>
                <textarea class="mceEditor" id="teachingcontent" name="teachingcontent"
                          rows="10" cols="113" wrap="soft">${rec.teachingcontent}</textarea>
            </fieldset>

            <!-- Service Content -->
            <fieldset class="mivcollapsible">
                <legend>${constants.tooltips.servicecontent}</legend>
                <label for="servicecontent" class="hidden">${constants.labels.servicecontent}</label>
                <%-- <div class="hint" > expand to see ${constants.tooltips.servicecontent} fields</div> --%>
                <textarea class="mceEditor" id="servicecontent" name="servicecontent"
                          rows="10" cols="113" wrap="soft">${rec.servicecontent}</textarea>
            </fieldset>

            <!-- Research Content -->
            <fieldset class="mivcollapsible">
                <legend>${constants.tooltips.researchcontent}</legend>
                <label for="researchcontent" class="hidden">${constants.labels.researchcontent}</label>
                <%-- <div class="hint" > expand to see ${constants.tooltips.researchcontent} fields</div> --%>
                <textarea class="mceEditor" id="researchcontent" name="researchcontent"
                          rows="10" cols="113" wrap="soft">${rec.researchcontent}</textarea>
            </fieldset>
        </div><!-- #designForm -->

        <div class="buttonset">
            <input type="submit" id="save" name="save" value="Save" title="Save" />
            <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel" />
        </div>

    </form>
