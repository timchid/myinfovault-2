<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>
   
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: [ "year","advise1", "advise2", "advise3", "advise4" ],
        constraints: {
            year: function(contents) {
                var curYear = new Date().getFullYear();
                
                if(isNaN(contents)){
                	return "INVALID_NUMBER";
                }
                
                var numyear = contents * 1;
                
                if( numyear < 1900 || numyear > (curYear +1) ){
                	return "INVALID_YEAR";
                }
                
                return true;
            },
            ugnum: miv.validation.isInteger,
            gnum: miv.validation.isInteger,
            a3num: miv.validation.isInteger,
            a4num: miv.validation.isInteger            
          },
          errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}" }
    };
// -->
</script>

<div>
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
        <%-- <div id="specchar"><a href="#">Add Special Characters</a></div>
             div id="format"><a href="#">Bold / Italic / Underline</a></div --%>
    <div id="links">
    </div>
</div>
<p class="formhelp">
Enter the average number of advisees for the review period. Enter only one record per year.
The first two description labels must not be blanked out, but the wording may be changed to
conform to department standards. The last two description labels are optional and can be
changed, but should be the same for all entries, to be consistent.
</p>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">

<!-- Academic Year -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  
  <input type="text" id="year" name="year" size="5" maxlength="4"
			value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
</span>
</div><!-- formline -->

<!-- Row One -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.advise1}">
  <label for="advise1" errorlabel="Row 1 - Description"  class="f_required">${constants.labels.advise1}</label>
  <input type="text"
         id="advise1" name="advise1"
         size="36" maxlength="100"
         value="${not empty rec.advise1 ? rec.advise1 : 'Number of undergraduate advisees:'}"
         >
</span>

<span class="textfield" title="${constants.tooltips.ugnum}">
  <label for="ugnum" errorlabel="Row 1 - No." class="f_required">${constants.labels.ugnum}</label>
  <input type="text" class="f_numeric"
         id="ugnum" name="ugnum"
         size="4" maxlength="6"
         value="${not empty rec.ugnum?rec.ugnum:0}"
         >
</span>
</div><!-- formline -->

<!-- Row Two -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.advise2}">
  <label for="advise2" errorlabel="Row 2 - Description" class="f_required">${constants.labels.advise2}</label>
  <input type="text"
         id="advise2" name="advise2"
         size="36" maxlength="100"
         value="${not empty rec.advise2 ? rec.advise2 : 'Number of graduate advisees:'}"
         >
</span>

<span class="textfield" title="${constants.tooltips.gnum}">
  <label for="gnum" errorlabel="Row 2 - No." class="f_required">${constants.labels.gnum}</label>
  <input type="text" class="f_numeric"
         id="gnum" name="gnum"
         size="4" maxlength="6"
         value="${not empty rec.gnum?rec.gnum:0}"
         >
</span>
</div><!-- formline -->

<!-- Row Three -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.advise3}">
  <label for="advise3">${constants.labels.advise3}</label>
  <input type="text"
         id="advise3" name="advise3"
         size="36" maxlength="100"
         value="${rec.advise3}"
         >
</span>

<span class="textfield" title="${constants.tooltips.a3num}">
  <label for="a3num" errorlabel="Row 3 - No.">${constants.labels.a3num}</label>
  <input type="text" class="f_numeric"
         id="a3num" name="a3num"
         size="4" maxlength="6"
         value="${not empty rec.a3num?rec.a3num:0}"
         >
</span>
</div><!-- formline -->

<!-- Row Four -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.advise4}">
  <label for="advise4">${constants.labels.advise4}</label>
  <input type="text"
         id="advise4" name="advise4"
         size="36" maxlength="100"
         value="${rec.advise4}"
         >
</span>

<span class="textfield" title="${constants.tooltips.a4num}">
  <label for="a4num" errorlabel="Row 4 - No.">${constants.labels.a4num}</label>
  <input type="text" class="f_numeric"
         id="a4num" name="a4num"
         size="4" maxlength="6"
         value="${not empty rec.a4num?rec.a4num:0}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
