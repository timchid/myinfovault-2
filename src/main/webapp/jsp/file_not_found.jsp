<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; Page Not Found</title>

    <%@ include file="/jsp/metatags.html" %>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>

    <style type="text/css">
	div#alert, div#message {
	    float: left;
	}
	div#alert { 
	    margin-left: 2em;
	    margin-top: 1em;
	}
	div#alert img {
	    transform: scale(1.2);
	}
	div#message {
	    margin-left: 2em;
	}
	div#decoration {
	    clear: both;
	}
	#main h1 {
	    margin-top: 1em;
	}
    </style>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
  </div><!-- breadcrumbs -->



  <div id="main">

    <h1>Page Not Found</h1>

    <div id="alert">
      <img src="<c:url value='/images/alert_black-64.png'/>" alt="Important!">
    </div>

    <div id="message">
        <p>
        <strong>We're sorry, the requested page or file could not be found!</strong><br>
        It may have been removed or had its name changed.
        </p>
        <p>
        Contact the MIV Project Team at
        <a href="mailto:miv-help@ucdavis.edu" title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
        if you need further assistance.
        </p>
    </div>

    <div id="decoration">
        <img src="<c:url value='/images/egghead.jpg'/>" border="0"
             alt="Robert Arneson's Egghead ceramic sculpture located at UC Davis">
    </div>
  </div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 et:
--%>
