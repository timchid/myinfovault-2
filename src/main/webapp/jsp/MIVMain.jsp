<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>UC Davis: MyInfoVault &ndash; Main Navigation</title>

    <%@ include file="/jsp/metatags.html" %>

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
    <![endif]-->
    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    <miv:permit action="UserList">
    <style type="text/css">
    div#activity table {
        border-collapse: collapse;
    }
    div#activity table thead tr {
        background-color: #EEE;
    }
    div#activity table th,
    div#activity table td {
        border: 1px solid #B2B2B2;
        text-align: left;
        padding: 3px 10px;
    }
    </style></miv:permit>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
  <div id="breadcrumbs" title="Home">
    Home
  </div><!-- breadcrumbs -->


<!-- MIV Main Div -->
<div id="main">

  <div id="left">
    <div id="getstarted">
        <p><%--
    --%><miv:permit roles="CANDIDATE">
        Learn how to enter data, create and design your dossier, and send your dossier to your department:<%--
    --%></miv:permit><%--
    --%><miv:permit roles="DEPT_ASSISTANT|OCP_STAFF">
        Learn how to enter data for a Candidate, and create and design their packet:<%--
    --%></miv:permit>
        <a href="/miv/help/getting_started.html" title="How do I get started?" target="mivhelp">How do I get started?</a>
        </p>
    </div>

    <miv:permit roles="CANDIDATE & !~SENATE_STAFF">
    <div class="selectlist" id="mydossier">
        <div class="heading">
            <div class="icon" id="dossier" title="My Dossier"><h1>My Dossier</h1></div>
        </div>
        <ul>
            <li><a href="<c:url value='/packet'/>" title="My Packet Requests">My&nbsp;Packet&nbsp;Requests</a>
            <c:set var="countclass" value="${counts.PacketRequests == 0 ? 'zero' : 'number'}"/>
                <span class="${countclass}">(${counts.PacketRequests})</span></li>
        </ul>
    </div>
    </miv:permit>

    <miv:permit roles="(CANDIDATE|DEAN|VICE_PROVOST_STAFF|OCP_STAFF) & !(~DEPT_ASSISTANT|~SENATE_STAFF)">
    <div class="selectlist" id="signdocuments">
        <div class="heading">
            <div class="icon" id="sign" title="Sign Documents"><h1>Sign&nbsp;Documents</h1></div>
        </div>
        <ul><%--
    --%><miv:permit roles="CANDIDATE|SYS_ADMIN">
            <li><a href="<c:url value='/ElectronicSignature?_flowId=electronicSignature-disclosure-flow'/>"
                   title="View My Complete Dossier and Sign My Disclosure Certificate">View My Complete Dossier/Sign My Disclosure Certificate</a><%--
             --%><c:set var="countclass" value="${counts.SignDisclosure == 0 ? 'zero' : 'number'}"/>
                <span class="${countclass}">(${counts.SignDisclosure})</span></li><%--
    --%></miv:permit><%--
    --%><miv:permit roles="DEAN">
            <c:url var="link" value="/ElectronicSignature">
                <c:param name="_flowId" value="electronicSignature-decision-flow"/>
                <c:param name="capacity" value="DEAN"/>
            </c:url>
            <li><a href="<c:out escapeXml="true" value="${link}"/>"
                   id="dean_sig" title="Review/Sign Dean's Final Decision/Recommendation">Review/Sign Dean's Final Decision/Recommendation</a><%--
            --%><c:set var="countclass" value="${counts.SignDean == 0 ? 'zero' : 'number'}"/><%--
            --%>&nbsp;<span class="${countclass}">(${counts.SignDean})</span></li><%--
    --%></miv:permit><%--
    --%><miv:permit roles="VICE_PROVOST_STAFF|VICE_PROVOST">
            <c:url var="link" value="/ElectronicSignature">
                <c:param name="_flowId" value="electronicSignature-decision-flow"/>
                <c:param name="capacity" value="VICE_PROVOST"/>
            </c:url>
            <li><a href="<c:out escapeXml="true" value="${link}"/>"
                   id="vp_sig" title="Review/Sign Vice Provost's Final Decision or Recommendation">Review/Sign Vice&nbsp;Provost's Final&nbsp;Decision/Recommendation</a><%--
            --%><c:set var="countclass" value="${counts.SignViceProvost == 0 ? 'zero' : 'number'}"/><%--
            --%>&nbsp;<span class="${countclass}">(${counts.SignViceProvost})</span></li><%--
    --%></miv:permit><%--
    --%><miv:permit roles="PROVOST">
            <c:url var="link" value="/ElectronicSignature">
                <c:param name="_flowId" value="electronicSignature-decision-flow"/>
                <c:param name="capacity" value="PROVOST"/>
            </c:url>
            <li><a href="<c:out escapeXml="true" value="${link}"/>"
                   id="provost_sig" title="Review/Sign Provost's Final Decision/Recommendation">Review/Sign Provost's Final Decision/Recommendation</a><%--
            --%><c:set var="countclass" value="${counts.SignProvost == 0 ? 'zero' : 'number'}"/><%--
            --%>&nbsp;<span class="${countclass}">(${counts.SignProvost})</span></li><%--
    --%></miv:permit><%--
    --%><miv:permit roles="CHANCELLOR">
            <c:url var="link" value="/ElectronicSignature">
                <c:param name="_flowId" value="electronicSignature-decision-flow"/>
                <c:param name="capacity" value="CHANCELLOR"/>
            </c:url>
            <li><a href="<c:out escapeXml="true" value="${link}"/>"
                   id="chancellor_sig" title="Review/Sign Chancellor's Final Decision">Review/Sign Chancellor's Final Decision</a><%--
            --%><c:set var="countclass" value="${counts.SignChancellor == 0 ? 'zero' : 'number'}"/><%--
            --%>&nbsp;<span class="${countclass}">(${counts.SignChancellor})</span></li><%--
    --%></miv:permit>
    <miv:permit roles="OCP_STAFF">
        <c:url var="link" value="/ElectronicSignature">
            <c:param name="_flowId" value="electronicSignature-decision-flow"/>
            <c:param name="capacity" value="PROVOST"/>
        </c:url>
        <li><a href="<c:out escapeXml="true" value="${link}"/>"
               title="View Provost Final Decision/Recommendation">View Provost's Final Decision/Recommendation</a>
            <span class="${counts.ViewProvost == 0 ? 'zero' : 'number'}">(${counts.ViewProvost})</span></li>

        <c:url var="link" value="/ElectronicSignature">
            <c:param name="_flowId" value="electronicSignature-decision-flow"/>
            <c:param name="capacity" value="CHANCELLOR"/>
        </c:url>
        <li><a href="<c:out escapeXml="true" value="${link}"/>"
               title="Review/Sign Chancellor's Final Decision">View Chancellor's Final Decision</a>
            <span class="${counts.ViewChancellor == 0 ? 'zero' : 'number'}">(${counts.ViewChancellor})</span></li>
    </miv:permit>
        </ul>
    </div><%--
--%></miv:permit>

    <miv:permit roles="CANDIDATE|DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|SENATE_STAFF">
    <div class="selectlist" id="reviewdossiers">
        <div class="heading">
            <div class="icon" id="review" title="Review&nbsp;Dossiers"><h1>Review&nbsp;Dossiers</h1></div>
        </div>
        <ul><%--
    --%><miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|SENATE_STAFF">
            <li><a href="<c:url value='/AssignReviewers?_flowId=assignReviewers-flow'/>"
                   title="Assign Dossier Reviewers">Assign&nbsp;Dossier&nbsp;Reviewers</a></li><%--
    --%></miv:permit><%--
    --%><miv:permit action="Review Dossier" usertype="REAL"><%--miv:permit roles="CANDIDATE|VICE_PROVOST_STAFF|SENATE_STAFF|SYS_ADMIN" usertype="REAL"--%>
          <miv:permit roles="CANDIDATE" usertype="PROXY">
            <li><a href="<c:url value='/ReviewDossiers?_flowId=reviewDossiers-flow'/>"
                   title="Review Other Candidate's Dossiers">Review&nbsp;Other Candidate's&nbsp;Dossiers</a><%--
            --%><c:set var="countclass" value="${counts.ReviewDossiers == 0 ? 'zero' : 'number'}"/><%--
            --%>&nbsp;<span class="${countclass}">(${counts.ReviewDossiers})</span></li>
          </miv:permit><%--
    --%></miv:permit>
        </ul>
        <%-- Hide the whole "Review Dossiers" block if it has no children --%>
        <script type="text/javascript">if (jQuery("div#reviewdossiers ul").children().length < 1) jQuery("div#reviewdossiers").hide();</script>
    </div><%--
--%></miv:permit>

    <%-- Use your browser's "View Source" to see the comment below --%>
    <!-- real role: ${user.userInfo.person.primaryRoleType}   proxied? ${user.userInfo!=user.targetUserInfo}   proxy role: ${user.targetUserInfo.person.primaryRoleType} -->
    <%--Senate Staff gets Manage Open Actions, but not when they are switched to someone else --%>
    <c:if test="${! (user.userInfo.person.primaryRoleType=='SENATE_STAFF' && user.targetUserInfo.person.primaryRoleType!='SENATE_STAFF') }">
    <miv:permit roles="(CANDIDATE|DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|VICE_PROVOST|SENATE_STAFF|DEPT_CHAIR|DEAN)&!(~DEPT_ASSISTANT)">
    <div class="selectlist" id="manageopenactions">
        <div class="heading">
            <div class="icon" id="open" title="Open&nbsp;Actions"><h1>Open&nbsp;Actions</h1></div>
        </div>
        <ul>
            <miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|VICE_PROVOST|SENATE_STAFF|DEPT_CHAIR|DEAN">
            <li><a href="<c:url value='/OpenActions?_flowId=openactions-flow'/>"
                   title="Manage Open Actions">Manage&nbsp;Open&nbsp;Actions</a><%--
                 <c:set var="countclass" value="${counts.OpenActions == 0 ? 'zero' : 'number'}"/>
             --%><%-- <span class="${countclass}">(${counts.OpenActions})</span> --%></li></miv:permit>
            <miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF">
                <li><a href="<c:url value='/createAction'/>"
                       title="Start an Action">Start&nbsp;an&nbsp;Action</a>
            </li>

                <li><a href="<c:url value='/OpenActions'>
                                 <c:param name='_flowId' value='initiateAcademicAction-flow'/>
                             </c:url>"
                       title="Start a New Appointment — not for Appointments Via Change in Department or Title">Start a New Appointment</a></li>
            </miv:permit>
            <miv:permit roles="(CANDIDATE|DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|DEPT_CHAIR|DEAN)&!(~DEPT_ASSISTANT)">
            <li><a href="<c:url value='/ViewDossierSnapshots?_flowId=viewdossiersnapshots-flow'/>" title="View Dossier Snapshots">View&nbsp;Dossier&nbsp;Snapshots</a></li>
            </miv:permit>
            <miv:permit roles="VICE_PROVOST_STAFF">
            <li><a href="<c:url value='/CancelDossier?_flowId=canceldossier-flow'/>" title="Cancel Actions">Cancel&nbsp;Actions</a></li>
            </miv:permit>
        </ul>
    </div><%--
--%></miv:permit>
    </c:if>

  </div><!-- left -->


  <div id="right">
    <miv:permit roles="DEPT_ASSISTANT|OCP_STAFF">
    <div id="mrakImage"><img src="<c:url value='/images/mrak.jpg'/>" alt="Mrak Hall" width="400" height="300"></div><%--
--%></miv:permit>

    <miv:permit roles="CANDIDATE">
    <div class="selectlist">
        <div class="heading">
            <div class="icon" id="biosketches" title="CV&nbsp;&amp;&nbsp;Biosketches"><h1>CV&nbsp;&amp;&nbsp;Biosketches</h1></div>
        </div>
        <ul>
            <li><a href="<c:url value='/biosketch/BiosketchPreview?biosketchtypeid=2'/>" title="Manage My Curriculum Vitae (CV)">Manage&nbsp;My Curriculum&nbsp;Vitae&nbsp;(CV)</a></li>
            <li><a href="<c:url value='/biosketch/BiosketchPreview?biosketchtypeid=3'/>" title="Manage My NIH Biosketch">Manage&nbsp;My NIH&nbsp;Biosketch</a></li>
        </ul>
    </div>

    <miv:permit roles="!(~SENATE_STAFF|~DEPT_ASSISTANT|VICE_PROVOST)">
    <div class="selectlist">
        <div class="heading">
            <div class="icon" id="completed" title="Completed Actions" ><h1>Completed&nbsp;Actions</h1></div>
        </div>
        <ul>
        <miv:permit roles="DEAN"><%-- Why is this here? This is already within a Candidate-only permit block --%>
            <li><a href="<c:url value='/ViewPostAuditDossiers?_flowId=viewpostauditdossiers-flow'/>" title="Dossiers at Post Audit or Appeal">Dossiers&nbsp;at&nbsp;Post&nbsp;Audit&nbsp;or&nbsp;Appeal</a></li>
        </miv:permit>
        <miv:permit roles="CANDIDATE"><%-- Why is this here? This is already within a Candidate-only permit block --%>
            <li><a href="<c:url value='/ViewDossierArchive?_flowId=viewdossierarchive-flow'/>" title="View Dossier Archive">View&nbsp;Dossier&nbsp;Archive</a></li>
        </miv:permit>
        </ul>
    </div>
    </miv:permit><%-- no Archives for Candidate as VP, a Dept Assistant, or Senate Staff here --%>
    </miv:permit><%-- Candidate block --%>

    <miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|SENATE_STAFF">
    <div class="selectlist" id="manageusers">
        <div class="heading">
            <div class="icon" id="manage" title="Manage&nbsp;Users"><h1>Manage&nbsp;Users</h1></div>
        </div>
        <ul>
            <li><a href="<c:url value='/user/add'/>" title="Add a New User">Add&nbsp;a&nbsp;New&nbsp;User</a></li>
            <miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF">
            <li><a href="<c:url value='/ManageUsers?_flowId=manageUsers-flow&amp;flowType=edit' />" title="Edit a User's Account">Edit&nbsp;a&nbsp;User's&nbsp;Account</a></li>
            </miv:permit>
            <li><a href="<c:url value='/ManageUsers?_flowId=manageUsers-flow&amp;flowType=activate'/>" title="Deactivate/Reactivate a User">Deactivate/Reactivate a User</a></li>
            <li><a href="<c:url value='/ManageUsers?_flowId=manageGroups-flow'/>" title="Manage Groups">Manage Groups</a></li>
            <miv:permit action="Manage Executives">
            <li><a href="<c:url value='/ManageUsers'>
                         <c:param name="_flowId" value="manageUsers-flow"/>
                         <c:param name="flowType" value="executive"/>
                         </c:url>" title="Manage Chancellor, Provost, and Vice Provost">Manage Executive Roles</a></li>
            </miv:permit>
            <%-- <miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF">
            <li><a href="<c:url value='/ConvertAppointee'/>"
                   title="Convert an Appointee to a Candidate">Convert Appointee to Candidate</a></li>
            </miv:permit> --%>
        </ul>
    </div>
    </miv:permit>

    <miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST|VICE_PROVOST_STAFF|SENATE_STAFF">
    <div class="selectlist">
        <div class="heading">
            <div class="icon" id="completed" title="Completed&nbsp;Actions"><h1>Completed&nbsp;Actions</h1></div>
        </div>
        <ul><%--
        --%><miv:permit roles="SCHOOL_STAFF|SENATE_STAFF">
            <li><a href="<c:url value='/ViewPostAuditDossiers?_flowId=viewpostauditdossiers-flow'/>"
                  title="Dossiers at Post Audit or Appeal">Dossiers&nbsp;at&nbsp;Post&nbsp;Audit&nbsp;or&nbsp;Appeal</a></li><%--
        --%></miv:permit>
            <miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|DEPT_CHAIR">
                <li><a href="<c:url value='/ViewDossierArchive?_flowId=viewdossierarchive-flow'/>"
                      title="View Dossier Archive">View&nbsp;Dossier&nbsp;Archive</a></li><%--
        --%></miv:permit><%--
        --%><miv:permit roles="VICE_PROVOST">
            <li><a href="<c:url value='/ViewPostAuditDossiers?_flowId=viewpostauditdossiers-flow'/>"
                  title="Dossiers at Post Audit or Appeal">Dossiers&nbsp;at&nbsp;Post&nbsp;Audit&nbsp;or&nbsp;Appeal</a></li>
            <li><a href="<c:url value='/ViewDossierArchive?_flowId=viewdossierarchive-flow'/>"
                  title="View Dossier Archive">View&nbsp;Dossier&nbsp;Archive</a></li><%--
        --%></miv:permit><%--
        --%><miv:permit roles="VICE_PROVOST_STAFF">
            <li><a href="<c:url value='/PostAuditDossiers?_flowId=postauditdossiers-flow'/>"
                  title="Send Dossiers to Post Audit">Send&nbsp;Dossiers&nbsp;to&nbsp;Post&nbsp;Audit</a></li>
            <li><a href="<c:url value='/ViewPostAuditDossiers?_flowId=viewpostauditdossiers-flow'/>"
                  title="Dossiers at Post Audit or Appeal">Dossiers&nbsp;at&nbsp;Post&nbsp;Audit&nbsp;or&nbsp;Appeal</a></li>
            <li><a href="<c:url value='/ArchiveDossiers?_flowId=archiveDossiers-flow'/>"
                  title="Archive Completed Dossiers">Archive&nbsp;Completed&nbsp;Dossiers</a></li>
            <li><a href="<c:url value='/ViewDossierArchive?_flowId=viewdossierarchive-flow'/>"
                  title="View Dossier Archive">View&nbsp;Dossier&nbsp;Archive</a></li><%--
        --%></miv:permit><%--
    --%></ul>
    </div>
    </miv:permit>

    <div class="selectlist">
        <div class="heading">
            <div class="icon" id="reports" title="Reports"><h1>Reports</h1></div>
        </div>
        <ul>
            <li><a href="<c:url value='/SearchCriteria?_flowId=report-search-flow'/>" title="View MIV Users">View&nbsp;MIV&nbsp;Users</a></li>
            <li><a href="<c:url value='/SearchCriteria?_flowId=report-search-flow&amp;roleType=DEAN'/>" title="View MIV Deans">View&nbsp;MIV&nbsp;Deans</a></li>
            <li><a href="<c:url value='/SearchCriteria?_flowId=report-search-flow&amp;roleType=DEPT_CHAIR'/>" title="View MIV Department Chairs">View&nbsp;MIV Department&nbsp;Chairs</a></li>
            <miv:permit roles="!OCP_STAFF" usertype="REAL">
            <li><a href="<c:url value='/SearchCriteria?_flowId=openactions-report-search-flow'/>" title="View MIV Open Actions">View&nbsp;MIV&nbsp;Open&nbsp;Actions</a></li>
            </miv:permit>
            <miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF">
            <li><a href="<c:url value='/jsp/finduser.jsp'/>" title="Look up user information from an MIV ID number, a first or last name, or email.">MIV User Lookup Utility</a></li>
            </miv:permit>
        </ul>
    </div>

    <miv:permit roles="SYS_ADMIN">
    <div class="selectlist" id="utility">
        <div class="heading">
            <div class="icon" id="devteam" title="MIV System Administration"><h1>MIV System Administration</h1></div>
        </div>
        <ul>
            <li><a href="<c:url value='/AboutMIV'/>"
                   title="Get MyInfoVault build information for this deployment">MIV Build Information</a></li>
            <li><a href="<c:url value='/jsp/mivoptions.jsp'/>"
                   title="Set the MyInfoVault application options">MIV Options</a></li>
            <li><a href="<c:url value='/util/SearchLDAP'/>"
                   title="Look for a user in LDAP">Search LDAP</a></li>
            <li><a href="<c:url value='/Imports'>
                         <c:param name="_flowId" value="vetmed-flow" />
                         </c:url>"
                   title="Import VetMed CSVs">VetMed Import</a></li>
            <miv:permit action="UserList">
            <li><a href="#" id="showactivity"
                   title="Show or Hide the logged-in user list">Show/Hide User Activity</a></li>
            </miv:permit>
        </ul>
    </div>
    </miv:permit>

  </div><!-- right -->

    <miv:permit action="UserList">
    <div id="activity" style="display:none;">
        <table>
            <thead>
                <tr><th>Date/Time</th> <th>User</th> <th>Activity</th></tr>
            </thead>
            <tbody><%--
        --%><c:forEach var="item" items="${activity}" varStatus="apptLoopCount">
                <tr><td>${item.when}</td> <td>${item.who}</td> <td><c:out value="${item.what}" escapeXml="true" /></td></tr><%--
        --%></c:forEach>
            </tbody>
        </table>
    </div>
    </miv:permit>

</div><!-- main -->

<miv:permit action="UserList">
<script type="text/javascript">
$(document).ready(function() {
    $('#showactivity').click(function(e) {
        e.preventDefault();
        $('#activity').toggle();
    });
});
</script>
</miv:permit>

<jsp:include page="/jsp/miv_small_footer.jsp" />
<c:if test="${not empty param.alertmsg}">
<script>
  alert("${param.alertmsg}");
</script>
</c:if>
</body>
</html><%--
 vi: se ts=8 sw=4 et:
--%>
