<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="spring" uri="/spring"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; ${constants.strings.selectbiosketchdata}</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>">

    <%@ include file="/jsp/metatags.html"%>
    <script type="text/javascript">
    /* <![CDATA[ */
        djConfig = {
            isDebug: false,
            parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
        };
    /* ]]> */
    </script>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
]]></jsp:scriptlet>

    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <jwr:script src="/bundles/mivvalidation.js" />
    <script type="text/javascript" src="<c:url value='/js/biosketch.js'/>"></script>
    <script type="text/javascript">
    /* <![CDATA[ */
    var mivConfig = new Object();
    mivConfig.isIE = false;
    /* ]]> */
    </script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/biosketch.css'/>">

    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <![endif]-->

    <c:set var="user" scope="request" value="${MIVSESSION.user}" />
</head>
<body class="tundra">

    <jsp:include page="/jsp/mivheader.jsp" />
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; <a href="<c:url value='/biosketch/BiosketchPreview'/>" title="${constants.strings.description} List">${constants.strings.description} List</a>
        &gt; <a href="<c:url value='/biosketch/BiosketchMenu'/>" title="${constants.strings.description} Menu">${constants.strings.description} Menu</a>
        &gt; ${constants.strings.selectbiosketchdata}
    </div><!-- breadcrumbs -->

    <div id="main">
        <h1 class="biosketchheader">${constants.strings.selectbiosketchdata}</h1>
        <c:if test="${showwizard=='true'}">
        <div id="wizardsteps">
            <p>Following are the steps to create your ${constants.strings.description}. You are currently at Step 2.</p>
            <div class="inactivestep"><span class="step">Step 1:</span> ${constants.strings.displayselectoptionstitle}</div>
            <div class="activestep"><span class="step">Step 2:</span> ${constants.strings.selectbiosketchdata}</div>
            <div class="inactivestep"><span class="step">Step 3:</span> ${constants.strings.designmybiosketch}</div>
            <div class="inactivestep"><span class="step">Step 4:</span> ${constants.strings.createmybiosketch}</div>
        </div><!-- wizardsteps -->
        <div id="cancelurl"><c:url value='/biosketch/BiosketchPreview'/></div>
        </c:if>

        <spring:bind path="selectDataCommand">
            <c:if test="${not empty status.errorMessages}">
                <c:set var='errorClass' scope='page' value=' class="haserrors"'/>
              </c:if>
        </spring:bind>

        <div dojotype="dijit.layout.ContentPane" id="errormessage"${errorClass}>
            <spring:bind path="selectDataCommand">
            <c:if test="${not empty status.errorMessages}">
                <div id="errorbox">
                    Error(s):
                    <ul>
                    <c:forEach var="errorMessage" items="${status.errorMessages}">
                        <li>${errorMessage}</li>
                    </c:forEach>
                    </ul>
                </div>
            </c:if>
            </spring:bind>
        </div>
        <div id="enterdataguide">
            <div id="requiredfieldlegend">* = Required Field</div>
        </div>

        <div id="maindialog" class="pagedialog">
            <div class="dojoDialog mivDialog wideDialog">
                <div id="userheading" class="standout">${user.targetUserInfo.displayName}</div>
                <div class="standout">${selectDataCommand.name}</div>

                <form class="mivdata selectbiosketchdata" id="biosketchform" method="post" action="<c:url value='/biosketch/SelectBiosketchData'/>" name="selectdata">
                    <div class="formline">
                        <span class="textfield"  title="${constants.tooltips.startYear}">
                            <label> Select data</label>
                            <label for="startYear">${constants.labels.startYear}:</label>
                            <spring:bind path="selectDataCommand.startYear">
                            <input  type="text"
                                    id="startYear" name="startYear"
                                    size="5" maxlength="4"
                                    class="f_required"
                                    value="${selectDataCommand.startYear}"
                                    title="Year must be from 1900 to ${thisYear}">
                            </spring:bind>
                        </span>
                        &nbsp;&nbsp;
                        <span class="textfield"  title="${constants.tooltips.endYear}">
                            <label for="endYear">${constants.labels.endYear}:</label>
                            <spring:bind path="selectDataCommand.endYear">
                            <input  type="text"
                                    id="endYear" name="endYear"
                                    size="5" maxlength="4"
                                    class="f_required"
                                    value="${selectDataCommand.endYear}"
                                    title="Year must be from 1900 to ${thisYear}">
                            </spring:bind>
                        </span>
                    </div><!--formline-->

                    <div class="formline">
                        <span class="f_required">Include the following data:</span>
                    </div><!--formline-->

                    <div class="formline checkuncheck-buttongroup">
                        <input type="button" title="Check All" value="Check All" onclick="javascript:checkUncheckAll($('#checklist'),true)">
                        <input type="button" title="Clear All" value="Clear All" onclick="javascript:checkUncheckAll($('#checklist'),false)">
                    </div>

                    <div id="checklist" class="formline">
                        <div class="${biosketchType} biosketchdata">
                            <div class="biosketchblock">
                                <ul class="biosketchlist">
                                    <c:forEach var="p" items="${selectDataCommand.biosketchSection}" varStatus="counter">
                                        <c:if test="${p.inSelectList}">
                                            <li>
                                                <form:checkbox path="selectDataCommand.biosketchSection[${counter.index}].display"
                                                               label="${p.sectionName}"
                                                               title="${p.sectionName}"
                                                               disabled="${p.availability=='MANDATORY'}"/>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                    </div><!-- #checklist -->

                    <div class="buttonset">
                        <input type="hidden" name="newdatarecord" value="${newdatarecord}">
                        <c:if test="${showwizard =='true'}"><%--
                    --%><input type="submit" name="btnSubmit" value="Proceed" title="Proceed" id="save"><%--
                    --%></c:if><%--
                    --%><c:if test="${showwizard !='true'}"><%--
                    --%><input type="submit" name="btnSubmit" value="Save" title="Save" id="save"><%--
                    --%></c:if>
                        <input type="reset" name="popupformcancel" id="popupformcancel" value="Cancel" title="Cancel">
                    </div>

                </form>
            </div><!-- mivDialog -->
        </div><!-- mainDialog -->
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vim:ts=8 sw=4 et sr:
--%>
