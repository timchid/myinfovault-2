<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!-- ******************** Footer ******************** -->
<jsp:useBean id="date" scope="page" class="java.util.Date"></jsp:useBean>
<div id="footer">
    Copyright &copy; ${date.year + 1900} The Regents of the University of California, Davis campus. All Rights Reserved.<br>
    <a href="<c:url value='/help/about_this_site.html'/>"
       title="About This Site"
       target="mivhelp">About This Site</a>&nbsp;|&nbsp;MIV Version:&nbsp;${MIVSESSION.information.mivVersion}/${MIVSESSION.information.buildRevision}
    <address><a href="mailto:miv-help@ucdavis.edu" title="Contact the MIV Project Team">Contact the MIV Project Team</a></address>
</div><!-- footer -->

<!-- ******* Special character mapping ******* -->
<div dojoType="dojox.layout.FloatingPane"
     id="char_palette"
     widgetid="char_palette"
     title="&nbsp; Special Character Palette"
     constrainToContainer="false"
     closable="true"
     dockable="false"
     displayMinimizeAction="false"
     resizable="false"
     hasShadow="false"
     style="width:23.5em; height:36.2em; left:400px; top:160px; display:none;"
     >

<div id='character_map'>
<table border='1' cellspacing='0' cellpadding='0'>
<tr>
  <td class="buttonbar" colspan="12">
  <div id="tagbuttons">
    <span title="Insert subscript tags" class="subtag">
        <img src="<c:url value='/images/sub.gif'/>" alt="Insert subscript tags" onClick="insertTag('sub')">
    </span>
    <span title="Insert superscript tags" class="suptag">
        <img src="<c:url value='/images/sup.gif'/>" alt="Insert superscript tags" onClick="insertTag('sup')">
    </span>
  </div>
  <div id="taghelptext">Select text and click the subscript or superscript button to format.</div>
  </td>
</tr>
  <%--
<!--
   Instead of a link with target, we could use a javascript window.open() call
   to open, position, and set the size of the window.

   From http://www.tizag.com/javascriptT/javascriptpopups.php

   Javascript Window.Open Arguments

   There are three arguments that the window.open function takes. First the
   relative or absolute URL of the web page to be opened. Secondly, a text name
   for the window, and lastly a long string that contains all the different
   properties of the window.

   Naming a window is very useful if you want to manipulate it later with
   Javascript. However, this is beyond the scope of this lesson and we will
   instead be focusing on the different properties you can set with your
   brand spanking new Javascript window. Below are some of the more important
   properties.

   * dependent - Subwindow closes if parent(the window that opened it) window closes
   * fullscreen - Display browser in full screen mode
   * height - The height of the new window, in pixels
   * width - The width of the new window, in pixels
   * left - Pixel offset from the left side of the screen
   * top - Pixel offset from the top of the screen
   * resizable - Allow the user to resize the window or prevent resizing
   * status - Display the status bar or not

   Dependent, fullscreen, resizable, and status are all examples of ON/OFF
   properties. You can either set them equal to zero to turn them off, or
   set them to one to turn them ON. There is no inbetween setting for these
   types of properties.

--> --%>

<tr class="charlist">
<!-- row 1, inverted ! to bullet -->
<td class="charmap" title="Inverted Exclamation mark" onClick="insertCharacter('&#161;')">&#161;</td>
<td class="charmap" title="Inverted Question mark" onClick="insertCharacter('&#191;')">&#191;</td>
<td class="charmap" title="Left Double-Quote" onClick="insertCharacter('&#8220;')">&#8220;</td>
<td class="charmap" title="Right Double-Quote" onClick="insertCharacter('&#8221;')">&#8221;</td>
<td class="charmap" title="Circumflex Accent" onClick="insertCharacter('&#0094;')">&#0094;</td>
<td class="charmap" title="Left Guillemet" onClick="insertCharacter('&#171;')">&#171;</td>
<td class="charmap" title="Right Guillemet" onClick="insertCharacter('&#187;')">&#187;</td>
<td class="charmap" title="Broken bar" onClick="insertCharacter('&#166;')">&#166;</td>
<td class="charmap" title="Section sign" onClick="insertCharacter('&#167;')">&#167;</td>
<td class="charmap" title="Paragraph sign/Pilcrow" onClick="insertCharacter('&#182;')">&#182;</td>
<td class="charmap" title="Middle dot" onClick="insertCharacter('&#183;')">&#183;</td>
<td class="charmap" title="Bullet" onClick="insertCharacter('&#8226;')">&#8226;</td>
</tr>

<tr class="charlist">
<!-- row 2, 'dagger' sign to Logical NOT sign -->
<td class="charmap" title="Dagger" onClick="insertCharacter('&#8224;')">&#8224;</td>
<td class="charmap" title="Copyright sign" onClick="insertCharacter('&#169;')">&#169;</td>
<td class="charmap" title="Reserved/Registered sign" onClick="insertCharacter('&#174;')">&#174;</td>
<td class="charmap" title="Trademark sign" onClick="insertCharacter('&#8482;')">&#8482;</td>
<td class="charmap" title="Cent sign" onClick="insertCharacter('&#162;')">&#162;</td>
<td class="charmap" title="Pound sign" onClick="insertCharacter('&#163;')">&#163;</td>
<td class="charmap" title="Euro sign" onClick="insertCharacter('&#8364;')">&#8364;</td>
<td class="charmap" title="Yen/Yuan sign" onClick="insertCharacter('&#165;')">&#165;</td>
<td class="charmap" title="Plus-Minus sign" onClick="insertCharacter('&#177;')">&#177;</td>
<td class="charmap" title="Multiplication sign" onClick="insertCharacter('&#215;')">&#215;</td>
<td class="charmap" title="Division sign" onClick="insertCharacter('&#247;')">&#247;</td>
<td class="charmap" title="Logical NOT" onClick="insertCharacter('&#172;')">&#172;</td>
</tr>

<tr class="charlist">
<!-- row 3, micro sign to 'A' A-circumflex  -->
<td class="charmap" title="Micro sign" onClick="insertCharacter('&#181;')">&#181;</td>
<td class="charmap" title="Degree sign" onClick="insertCharacter('&#176;')">&#176;</td>
<td class="charmap" title="Superscript 1" onClick="insertCharacter('&#185;')">&#185;</td>
<td class="charmap" title="Superscript 2" onClick="insertCharacter('&#178;')">&#178;</td>
<td class="charmap" title="Superscript 3" onClick="insertCharacter('&#179;')">&#179;</td>
<td class="charmap" title="One-quarter fraction" onClick="insertCharacter('&#188;')">&#188;</td>
<td class="charmap" title="One-half fraction" onClick="insertCharacter('&#189;')">&#189;</td>
<td class="charmap" title="Three-quarters fraction" onClick="insertCharacter('&#190;')">&#190;</td>
<td class="charmap" title="Latin small f with hook" onClick="insertCharacter('&#402;')">&#402;</td>
<td class="charmap" title="Capital A with grave accent" onClick="insertCharacter('&#192;')">&#192;</td>
<td class="charmap" title="Capital A with acute accent" onClick="insertCharacter('&#193;')">&#193;</td>
<td class="charmap" title="Capital A with circumflex" onClick="insertCharacter('&#194;')">&#194;</td>
</tr>

<tr class="charlist">
<!-- row 4, A-tilde to I~circumflex -->
<td class="charmap" title="Capital A with tilde" onClick="insertCharacter('&#195;')">&#195;</td>
<td class="charmap" title="Capital A with diaeresis" onClick="insertCharacter('&#196;')">&#196;</td>
<td class="charmap" title="Capital A with ring above" onClick="insertCharacter('&#197;')">&#197;</td>
<td class="charmap" title="Capital AE ligature" onClick="insertCharacter('&#198;')">&#198;</td>
<td class="charmap" title="Capital C with cedilla" onClick="insertCharacter('&#199;')">&#199;</td>
<td class="charmap" title="Capital E with grave accent" onClick="insertCharacter('&#200;')">&#200;</td>
<td class="charmap" title="Capital E with acute accent" onClick="insertCharacter('&#201;')">&#201;</td>
<td class="charmap" title="Capital E with circumflex" onClick="insertCharacter('&#202;')">&#202;</td>
<td class="charmap" title="Capital E with diaeresis" onClick="insertCharacter('&#203;')">&#203;</td>
<td class="charmap" title="Capital I with grave accent" onClick="insertCharacter('&#204;')">&#204;</td>
<td class="charmap" title="Capital I with acute accent" onClick="insertCharacter('&#205;')">&#205;</td>
<td class="charmap" title="Capital I with circumflex" onClick="insertCharacter('&#206;')">&#206;</td>
</tr>

<tr class="charlist">
<!-- row 5, I~diaeresis to U~circumflex -->
<td class="charmap" title="Capital I with diaeresis" onClick="insertCharacter('&#207;')">&#207;</td>
<td class="charmap" title="Capital letter &quot;Eth&quot;" onClick="insertCharacter('&#208;')">&#208;</td>
<td class="charmap" title="Capital N with tilde" onClick="insertCharacter('&#209;')">&#209;</td>
<td class="charmap" title="Capital O with grave accent" onClick="insertCharacter('&#210;')">&#210;</td>
<td class="charmap" title="Capital O with acute accent" onClick="insertCharacter('&#211;')">&#211;</td>
<td class="charmap" title="Capital O with circumflex" onClick="insertCharacter('&#212;')">&#212;</td>
<td class="charmap" title="Capital O with tilde" onClick="insertCharacter('&#213;')">&#213;</td>
<td class="charmap" title="Capital O with diaeresis" onClick="insertCharacter('&#214;')">&#214;</td>
<td class="charmap" title="Capital O with stroke" onClick="insertCharacter('&#216;')">&#216;</td>
<td class="charmap" title="Capital U with grave accent" onClick="insertCharacter('&#217;')">&#217;</td>
<td class="charmap" title="Capital U with acute accent" onClick="insertCharacter('&#218;')">&#218;</td>
<td class="charmap" title="Capital U with circumflex" onClick="insertCharacter('&#219;')">&#219;</td>
</tr>

<tr class="charlist">
<!-- row 6, U~diaeresis to c~cedilla -->
<td class="charmap" title="Capital U with diaeresis" onClick="insertCharacter('&#220;')">&#220;</td>
<td class="charmap" title="Capital Y with acute accent" onClick="insertCharacter('&#221;')">&#221;</td>
<td class="charmap" title="Capital Y with diaeresis" onClick="insertCharacter('&#376;')">&#376;</td>
<td class="charmap" title="Capital letter &quot;Thorn&quot;" onClick="insertCharacter('&#222;')">&#222;</td>
<td class="charmap" title="a with grave accent" onClick="insertCharacter('&#224;')">&#224;</td>
<td class="charmap" title="a with acute accent" onClick="insertCharacter('&#225;')">&#225;</td>
<td class="charmap" title="a with circumflex" onClick="insertCharacter('&#226;')">&#226;</td>
<td class="charmap" title="a with tilde" onClick="insertCharacter('&#227;')">&#227;</td>
<td class="charmap" title="a with diaeresis" onClick="insertCharacter('&#228;')">&#228;</td>
<td class="charmap" title="a with ring above" onClick="insertCharacter('&#229;')">&#229;</td>
<td class="charmap" title="ae ligature" onClick="insertCharacter('&#230;')">&#230;</td>
<td class="charmap" title="c with cedilla" onClick="insertCharacter('&#231;')">&#231;</td>
</tr>

<tr class="charlist">
<!-- row 7, e-grave accent to o-acute accent -->
<td class="charmap" title="e with grave accent" onClick="insertCharacter('&#232;')">&#232;</td>
<td class="charmap" title="e with acute accent" onClick="insertCharacter('&#233;')">&#233;</td>
<td class="charmap" title="e with circumflex" onClick="insertCharacter('&#234;')">&#234;</td>
<td class="charmap" title="e with diaeresis" onClick="insertCharacter('&#235;')">&#235;</td>
<td class="charmap" title="i with grave accent" onClick="insertCharacter('&#236;')">&#236;</td>
<td class="charmap" title="i with acute accent" onClick="insertCharacter('&#237;')">&#237;</td>
<td class="charmap" title="i with circumflex" onClick="insertCharacter('&#238;')">&#238;</td>
<td class="charmap" title="i with diaeresis" onClick="insertCharacter('&#239;')">&#239;</td>
<td class="charmap" title="small letter &quot;Eth&quot;" onClick="insertCharacter('&#240;')">&#240;</td>
<td class="charmap" title="n with tilde" onClick="insertCharacter('&#241;')">&#241;</td>
<td class="charmap" title="o with grave accent" onClick="insertCharacter('&#242;')">&#242;</td>
<td class="charmap" title="o with acute accent" onClick="insertCharacter('&#243;')">&#243;</td>
</tr>

<tr class="charlist">
<!-- row 8, o~circumflex to German 'es-zett' -->
<td class="charmap" title="o with circumflex" onClick="insertCharacter('&#244;')">&#244;</td>
<td class="charmap" title="o with tilde" onClick="insertCharacter('&#245;')">&#245;</td>
<td class="charmap" title="o with diaeresis" onClick="insertCharacter('&#246;')">&#246;</td>
<td class="charmap" title="o with stroke" onClick="insertCharacter('&#248;')">&#248;</td>
<td class="charmap" title="u with grave accent" onClick="insertCharacter('&#249;')">&#249;</td>
<td class="charmap" title="u with acute accent" onClick="insertCharacter('&#250;')">&#250;</td>
<td class="charmap" title="u with circumflex" onClick="insertCharacter('&#251;')">&#251;</td>
<td class="charmap" title="u with diaeresis" onClick="insertCharacter('&#252;')">&#252;</td>
<td class="charmap" title="y with acute accent" onClick="insertCharacter('&#253;')">&#253;</td>
<td class="charmap" title="y with diaeresis" onClick="insertCharacter('&#255;')">&#255;</td>
<td class="charmap" title="small letter &quot;thorn&quot;" onClick="insertCharacter('&#254;')">&#254;</td>
<td class="charmap" title="German &quot;es-zett&quot; or &quot;scharfes s&quot;" onClick="insertCharacter('&#223;')">&#223;</td>
</tr>

<tr class="charlist">
<!-- row 9, Greek Alpha to Greek Mu -->
<td class="charmap" title="Greek capital Alpha" onClick="insertCharacter('&#913;')">&#913;</td>
<td class="charmap" title="Greek capital Beta" onClick="insertCharacter('&#914;')">&#914;</td>
<td class="charmap" title="Greek capital Gamma" onClick="insertCharacter('&#915;')">&#915;</td>
<td class="charmap" title="Greek capital Delta" onClick="insertCharacter('&#916;')">&#916;</td>
<td class="charmap" title="Greek capital Epsilon" onClick="insertCharacter('&#917;')">&#917;</td>
<td class="charmap" title="Greek capital Zeta" onClick="insertCharacter('&#918;')">&#918;</td>
<td class="charmap" title="Greek capital Eta" onClick="insertCharacter('&#919;')">&#919;</td>
<td class="charmap" title="Greek capital Theta" onClick="insertCharacter('&#920;')">&#920;</td>
<td class="charmap" title="Greek capital Iota" onClick="insertCharacter('&#921;')">&#921;</td>
<td class="charmap" title="Greek capital Kappa" onClick="insertCharacter('&#922;')">&#922;</td>
<td class="charmap" title="Greek capital Lambda" onClick="insertCharacter('&#923;')">&#923;</td>
<td class="charmap" title="Greek capital Mu" onClick="insertCharacter('&#924;')">&#924;</td>
</tr>

<tr class="charlist">
<!-- row 10, Greek Nu to Greek Omega -->
<td class="charmap" title="Greek capital Nu" onClick="insertCharacter('&#925;')">&#925;</td>
<td class="charmap" title="Greek capital Xi" onClick="insertCharacter('&#926;')">&#926;</td>
<td class="charmap" title="Greek capital Omicron" onClick="insertCharacter('&#927;')">&#927;</td>
<td class="charmap" title="Greek capital Pi" onClick="insertCharacter('&#928;')">&#928;</td>
<td class="charmap" title="Greek capital Rho" onClick="insertCharacter('&#929;')">&#929;</td>
<td class="charmap" title="Greek capital Sigma" onClick="insertCharacter('&Sigma;')">&Sigma;</td>
<td class="charmap" title="Greek capital Tau" onClick="insertCharacter('&#932;')">&#932;</td>
<td class="charmap" title="Greek capital Upsilon" onClick="insertCharacter('&#933;')">&#933;</td>
<td class="charmap" title="Greek capital Phi" onClick="insertCharacter('&#934;')">&#934;</td>
<td class="charmap" title="Greek capital Chi" onClick="insertCharacter('&#935;')">&#935;</td>
<td class="charmap" title="Greek capital Psi" onClick="insertCharacter('&#936;')">&#936;</td>
<td class="charmap" title="Greek capital Omega" onClick="insertCharacter('&#937;')">&#937;</td>
</tr>

<tr class="charlist">
<!-- row 11, Greek alpha to Greek mu -->
<td class="charmap" title="Greek small alpha" onClick="insertCharacter('&#945;')">&#945;</td>
<td class="charmap" title="Greek small beta" onClick="insertCharacter('&#946;')">&#946;</td>
<td class="charmap" title="Greek small gamma" onClick="insertCharacter('&#947;')">&#947;</td>
<td class="charmap" title="Greek small delta" onClick="insertCharacter('&#948;')">&#948;</td>
<td class="charmap" title="Greek small epsilon" onClick="insertCharacter('&#949;')">&#949;</td>
<td class="charmap" title="Greek small zeta" onClick="insertCharacter('&#950;')">&#950;</td>
<td class="charmap" title="Greek small eta" onClick="insertCharacter('&#951;')">&#951;</td>
<td class="charmap" title="Greek small theta" onClick="insertCharacter('&#952;')">&#952;</td>
<td class="charmap" title="Greek small iota" onClick="insertCharacter('&#953;')">&#953;</td>
<td class="charmap" title="Greek small kappa" onClick="insertCharacter('&#954;')">&#954;</td>
<td class="charmap" title="Greek small lambda" onClick="insertCharacter('&#955;')">&#955;</td>
<td class="charmap" title="Greek small mu" onClick="insertCharacter('&#956;')">&#956;</td>
</tr>

<tr class="charlist">
<!-- row 12, Greek nu to Greek omega -->
<td class="charmap" title="Greek small nu" onClick="insertCharacter('&#957;')">&#957;</td>
<td class="charmap" title="Greek small xi" onClick="insertCharacter('&#958;')">&#958;</td>
<td class="charmap" title="Greek small omicron" onClick="insertCharacter('&#959;')">&#959;</td>
<td class="charmap" title="Greek small pi" onClick="insertCharacter('&#960;')">&#960;</td>
<td class="charmap" title="Greek small rho" onClick="insertCharacter('&#961;')">&#961;</td>
<td class="charmap" title="Greek small sigma" onClick="insertCharacter('&#963;')">&#963;</td>
<td class="charmap" title="Greek small tao" onClick="insertCharacter('&#964;')">&#964;</td>
<td class="charmap" title="Greek small upsilon" onClick="insertCharacter('&#965;')">&#965;</td>
<td class="charmap" title="Greek small phi" onClick="insertCharacter('&#966;')">&#966;</td>
<td class="charmap" title="Greek small chi" onClick="insertCharacter('&#967;')">&#967;</td>
<td class="charmap" title="Greek small psi" onClick="insertCharacter('&#968;')">&#968;</td>
<td class="charmap" title="Greek small omega" onClick="insertCharacter('&#969;')">&#969;</td>
</tr>

</table>
</div><!-- character_map -->
</div><!-- char_palette --><%--
<!--
     iconSrc="/miv/js/dojo/src/widget/templates/images/floatingPaneClose.gif"
     postCreate="this.closeWindow=this.minimizeWindow"
     postCreate="this.minimizeWindow();this.closeWindow=this.minimizeWindow"
-->  --%>
<!-- ******* END Special character mapping ******* -->
<script type="text/javascript">
/* jQuery('body').addClass("${user.userInfo.person.primaryRoleType}").addClass("target${user.targetUserInfo.person.primaryRoleType}").addClass("${MIVSESSION.config.productionServer ? 'prod' : 'non-prod'}"); */
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("${MIVSESSION.config.gaTrackerCode}");
pageTracker._initData();
pageTracker._trackPageview();
</script>
