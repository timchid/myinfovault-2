<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; Error Alert</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEmail.css'/>">

    <script type="text/javascript" src="<c:url value='/js/errorpage.js'/>"></script>
<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
<c:set var="message" value="${empty message ? param.message : message}"/>
<c:set var="config" value="${MIVSESSION.config}"/>
<c:set var="isProduction" value="${config.productionServer}"/>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; Error Alert
  </div><!-- breadcrumbs -->


<!-- MIV Main Div -->
<div id="main">
<%
java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("EEEEEEEEE MMM dd yyyy kk:mm:ss");
String date = df.format(new java.util.Date());
pageContext.setAttribute("errtime", date, PageContext.PAGE_SCOPE);
%>

  <h1>Error Alert</h1>

  <div class="notice"><!--style="clear:both; margin:0px; margin-bottom:1em; padding:0px; width:40em;"-->
  <p class="error" style="width: 40em;margin-top:0">
  The MIV system has encountered an error and cannot proceed.</p>
  <p class="warning">
  In order to resolve this issue, please report this error and send
  the message below to the MIV ${isProduction ? 'Help Desk.' : 'Project Team.'}
  <c:if test="${isProduction}">
  <br><strong>Sending this e-mail will open a <em>Service-Now</em> ticket.</strong>
  </c:if>
  </p>
  </div><!--.error-->

  <div class="fakemail">
    <div class="headerline">
      <span class="prompt">From:</span>
      <span class="field">${user.userInfo.displayName} &lt;${isProduction ? user.userInfo.email : "noreply+f@ucdavis.edu"}&gt;</span>
    </div>
    <div class="headerline">
      <span class="prompt">To:</span> <span class="field">
        ${isProduction ? "miv-help@ucdavis.edu" : "miv-team@ucdavis.edu"}</span>
    </div>
    <div class="headerline">
      <span class="prompt">Date:</span> <span class="field">${errtime}</span>
    </div>
    <div class="headerline">
      <span class="prompt">Subject:</span> <span class="field">System Error Report from MIV</span>
    </div>
    <div class="message">
      <form id="msgform" action="<c:url value='/ReportError'/>" method="post" accept-charset="utf-8">
      <div class="system">
          <textarea id="systemmessage" name="systemmessage"
                    rows="4" cols="80" wrap="soft" readonly
>I am ${user.userInfo.displayName} and was logged in to MIV as ${MIVSESSION.user.loginName}<c:if test="${user.userInfo != user.targetUserInfo}">.  I was working with ${user.targetUserInfo.displayName}'s data </c:if>
on ${errtime} when I got the following error:
    ${message}</textarea>
      </div>
      <div class="user">
          <textarea id="message" name="message"
                    rows="4" cols="80" wrap="soft"
                    tabindex="1"
          ></textarea>
      </div>

      <div class="buttonset">
          <input type="submit" tabindex="2" name="send" id="send" value="Send" title="Send"/>
          <input type="hidden" id="errtime" name="errtime" value="${errtime}"/>
          <input type="hidden" id="user" name="user" value="${user.userID}"/>
          <input type="hidden" id="targetuser" name="targetuser" value="${user.targetUserInfo.person.userId}"/>
      </div>
      </form>
    </div><!--.message-->
  </div><!--.fakemail-->

</div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 sw=2 et:
--%>
