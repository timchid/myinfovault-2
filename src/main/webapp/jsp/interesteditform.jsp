<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [],
        trim: ["header", "content"]
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>
<jsp:include page="/jsp/wysiwyginit.inc"/>
<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Header Description -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.header}">
  <label for="header" class="f_required">${constants.labels.header}</label>
  <input type="text"
         id="header" name="header"
         size="50" maxlength="100"
         value="${rec.header}"
         >
</span>
</div><!-- formline -->
<em>Note: Special characters cannot be added to the "Title" field.</em><br/><br/>


<!-- Free Text Area -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.content}">
  &#8224;&nbsp;
  <label for="content" class="f_required">${constants.labels.content}</label><br />
  <textarea
            id="content" name="content"
            rows="10" cols="113" wrap="soft"
            class="f_required mceEditor"
            >${rec.content}</textarea>
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save" />
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
