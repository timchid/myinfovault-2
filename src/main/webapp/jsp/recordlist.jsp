<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash;
    ${type != 'honor' ? constants.strings.pagetitle : ''}<c:if test="${fn:length(constants.strings.subtitle)>0}">${type != 'honor' ? ':' : ''}
    ${constants.strings.subtitle}</c:if></title>
    <script type="text/javascript">
    /* <![CDATA[ */
        djConfig = {
            isDebug: false,
            //debugAtAllCosts: true,
            parseOnLoad: false // parseWidgets *must* be false, or we connect to the dialog too early.
        };
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>

    <%@ include file="/jsp/metatags.html" %>
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/resequence.css'/>">

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" >
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivuser.css'/>">
    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/detect.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/jquery-ui.plugins.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/jquery-contained-sticky-scroll.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/recordlist.js'/>"></script>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
<%-- Uncomment this script block to do serious javascript debugging on the dojo toolkit
<script>
//These need to be here if we've set debugAtAllCosts:true above.
dojo.require("dijit.Dialog");
//dojo.hostenv.writeIncludes();
</script><%-- --%>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; ${constants.strings.description}
    </div><!-- breadcrumbs -->

    <%-- debug: param.recid: [${param.recid}]  --%>
    <div id="main"><!-- MIV Main Div -->
<%-- debug:
    localID from HTTP session = ${localID}
    loginName from session = ${loginName}
    schoolID=${user.targetUserInfo.schoolID} / deptID=${user.targetUserInfo.deptID}
--%>
<%-- debug:
    constants: ${constants}
    constants.options: ${constants.options}
    constants.strings: ${constants.strings}
    constants.classes: ${constants.classes}
--%>
<%-- For pop-up dialogs: Instead of having the dojoType and widgetId in the HTML, when we
     create the dialog in code we need a simple DOM node that will be replaced by the dialog. --%>
        <div id="DialogNode"></div>

        <div id="listarea">
            <div id="header" class="header">
            <%-- This next div is here only to float the H1 and Add-Button
                 When they are floated the div is no longer visible
                 so the backgound color doesnt matter --%>
                <div style="background-color: #f0fff0;">
                    <h1 id="recordtypedescription">${constants.strings.pagehead}</h1>

                    <div id="typelist">
                        <form id="typelistform" class="recordlist" method="get" action="<c:url value='/ItemList'/>">
                            <input type="hidden" id="classTL" name="recordClass" value="${recordClass}">
                            <c:set var="pubTypeConstant" value="pubtype-${type}"/><%--
                        --%><c:set var="pubtype" value="${constants.config[pubTypeConstant]}"/><%--
                        --%><%-- debug: pubTypeConstant: [${pubTypeConstant}]  pubtype: [${pubtype}] --%><%--
                        --%><c:if test="${not empty param['recordClass']}"><%--
                        --%><div id="pubTypeList">
                                <label class="hidden" for="pubtypeselection">Select ${param['recordClass']=='publication'?'Publication':'CreativeActivity'} x</label>
                                <select id="pubtypeselection" name="type">
                                <c:if test="${param['recordClass']=='publication'}">
                                    <option value="abstract"${pubtype==1?" selected":""}>Abstracts</option>
                                    <option value="media"${pubtype==9?" selected":""}>Alternative Media</option>
                                    <option value="book-author"${pubtype==3?" selected":""}>Books Authored</option>
                                    <option value="book-chapter"${pubtype==5?" selected":""}>Book Chapters</option>
                                    <option value="book-editor"${pubtype==4?" selected":""}>Books Edited</option>
                                    <option value="review"${pubtype==6?" selected":""}>Books Reviewed</option>
                                    <option value="journal"${pubtype==2?" selected":""}>Journals</option>
                                    <option value="letter-to-editor"${pubtype==7?" selected":""}>Letters to the Editor</option>
                                    <option value="limited"${pubtype==8?" selected":""}>Limited Distribution</option>
                                    <option value="presentation"${pubtype==10?" selected":""}>Presentations</option>
                                </c:if>
                                <c:if test="${param['recordClass']=='creativeactivities'}">
                                    <option value="works"${pubtype==94?" selected":""}>Creative Work</option>
                                    <option value="events"${pubtype==95?" selected":""}>Public Dissemination</option>
                                    <option value="publicationevents"${pubtype==108?" selected":""}>Publication Events</option>
                                    <option value="reviews"${pubtype==96?" selected":""}>Reviews by Others</option>
                                </c:if>
                                <c:if test="${param['recordClass']=='candidatestatements'}">
                                    <option value="statement"${pubtype==11?" selected":""}>Candidate's Statement</option>
                                    <option value="diversitystatement"${pubtype==12?" selected":""}>Diversity Statement</option>
                                </c:if>
                                </select><%-- this quote mark -->" helps with correct syntax highlighting. --%>
                                <div class="buttonset">
                                    <input type="submit" id="gobutton" name="gobutton" value="Select">
                                </div>
                            </div><!-- #pubTypeList -->
                            </c:if>
                        </form>
                    </div><!-- typelist -->
                </div><!-- for H1 and Button -->

                <div class="doctype">
                    (${constants.strings.documenttypelabel}&nbsp;${constants.strings.documenttype}.)
                </div>

                <%-- sectionheading: [${sectionheading}]  constants.strings.description: [${constants.strings.description}] --%>
                <div style="clear: both; padding-bottom: 1em;">
                    <strong><c:choose><%--
                        --%><c:when test="${fn:length(sectionheading)>0}">${sectionheading}</c:when><%--
                        --%><c:when test="${fn:length(constants.strings.description)>0}">${constants.strings.description}</c:when><%--
                        --%><c:otherwise>${type} items</c:otherwise><%--
                    --%></c:choose>
                        for ${user.targetUserInfo.displayName}
                    </strong>
                    <%-- (logged in as ${user.userInfo.person.userId} /  ${MIVSESSION.loginName}) --%>
                    <c:if test="${pubtype==2}"><br>Download Journal entries from the MEDLINE database through the
                    <a href="<c:url value='/Imports?_flowId=publications-download-flow'/>" title="PubMed Download Utility">PubMed Download</a> utility.</c:if>
                </div>

                <c:if test="${not empty success && success == true && not empty CompletedMessage}"><%--
                --%><c:set var='hasMessages' scope='page' value=' class="hasmessages"'/><%--
            --%></c:if>

                <div id="CompletedMessage" ${hasMessages}>
                    ${CompletedMessage}
                </div>
                <script type="text/javascript">
                    dojo.removeAttr(dojo.byId("CompletedMessage"), "style");
                    dojo.removeClass(dojo.byId("CompletedMessage"), "hasmessages");
                </script>

                <!-- Add Record and Resequence Buttons -->
                <div class="addrecord">
                    <div class="buttonset"><%--
                    --%><c:choose><%--
                    --%><c:when test="${not empty constants.config.editurl}">
                        <a href="<c:url value='${constants.config.editurl}'/>"
                           class="linktobutton"
                           title="${constants.tooltips.addrecord}"
                        >${constants.strings.addrecord}</a><%--
                    --%></c:when><%--
                    --%><c:otherwise>
                        <form id="addform" class="recordlist" method="get" action="<c:url value='${constants.config.editorservlet}'/>">
                            <input type="hidden" id="rectypeA" name="rectype" value="${type}">
                            <input type="hidden" id="pubtypeA" name="pubtype" value="${pubtype}">
                            <input type="hidden" id="documentidA" name="documentid" value="${documentid}">
                            <input type="hidden" id="sectionnameA" name="sectionname" value="${sectionname}">
                            <input type="hidden" id="subtypeA" name="subtype" value="${subtype}">
                            <input type="hidden" id="classA" name="recordClass" value="${recordClass}">
                            <input type="submit" id="E0000" name="E0000" value="${constants.strings.addrecord}" title="${constants.tooltips.addrecord}">
                        </form><%--
                    --%></c:otherwise><%--
                    --%></c:choose>

                        <c:choose><c:when test="${constants.config.ordering=='fixed'}"><%-- put the reseq button unless order is fixed --%>
                        <a href="<c:url value='/help/resequencing.html'/>" target="mivhelp"
                           title="Resequencing Rules: Help on how to resequence and the ordering rules">Resequencing Rules</a>
                        (${constants.strings.description} may not be resequenced)
                        </c:when><c:otherwise><%--
                    --%><c:set var="resequenceable" value=""/><%--
                    --%><c:if test="${empty sections}"><%--
                        --%><c:set var="resequenceable" value=" disabled"/><%--
                    --%></c:if>
                        <input type="button" id="SaveResequence1" class="SaveResequence hidden" value="Save Resequencing" title="Click to save resequence"><%--

                    --%><c:set var="mainRecType" value="${type}" /><%--
                    --%><c:if test="${not empty mainRecType && mainRecType == 'additionalheader' }"><%--
                        --%><c:set var="mainRecType" value="additional" /><%--
                    --%></c:if>
                        <a href="<c:url value='${constants.config.itemlistservlet}?type=${mainRecType}&recordClass=${recordClass}&sectionname=${sectionname}&documentid=${documentid}&subtype=${subtype}'/>"
                           id="CancelResequence1" class="CancelResequence linktobutton hidden" title="Click to cancel resequence"
                        >Cancel Resequencing</a>
                        <!-- <input type="button" id="CancelResequence1" class="CancelResequence hidden" value="Cancel Resequencing" title="Click to cancel resequence"> -->

                        <input type="button" id="ActiveResequence1" class="ActiveResequence" value="Resequence" title="Click to resequence records or sections">
                    </c:otherwise></c:choose>
                    </div><!-- .buttonset -->

                    <!--br--><%-- FIXME: There should NOT be a br tag here. It is not breaking a line. Use CSS margins & padding if space is needed. --%>
                    <p class="helplinks resequence hidden">
                    Drag and drop a record to resequence it using the
                    <a title="Resequencing Rules: Help on how to resequence and the ordering rules"
                       target="mivhelp" href="/miv/help/resequencing.html" class="popuplink">resequencing
                       rules</a> for this data category.
                    </p>
                </div><!-- .addrecord -->
            </div><!-- #header -->

        <c:choose><%--
        --%><c:when test="${empty sections}">No Items Found</c:when><%--
        --%><c:otherwise><c:set var="noprintFound" value="${false}"/>
            <form id="recordlistform" class="recordlist" method="get" action="<c:url value='${constants.config.editorservlet}'/>">
                <div style="display:inline;"><%-- This was put here way back on Rev 2830 Aug. 2008 to fix an IE bug.  Can it go away now? --%>
                    <input type="hidden" id="documentid" name="documentid" value="${documentid}">
                    <input type="hidden" id="sectionname" name="sectionname" value="${sectionname}">
                    <input type="hidden" id="subtype" name="subtype" value="${subtype}">
                    <input type="hidden" id="rectype" name="rectype" value="${type}">
                    <input type="hidden" id="classA" name="recordClass" value="${recordClass}">

                    <ul id="${type}-sections" class="sections">
                    <!-- The following is the list of "${recordTypeName}" records -->
                    <%-- START OF THE SECTION LOOP : each "section" gets a section heading and a list of records.
                --%><c:forEach var="section" items="${sections}"><c:set var="recset" value="${section.records}"/><%--
                    --%><c:set var="sectionid" value="${section.key}"/><%--
                    --%><c:if test="${type=='additional'}"><%--
                        --%><c:set var="sectionid" value="${section.key}-${section.recordNumber}"/><%--
                    --%></c:if>
                        <li id="SH-${sectionid}" class="section">
                            <div class="section" id="${sectionid}"><%--
                            --%><c:set var="sechead" value="${section.key}-sectionhead"/><%--
                            --%><c:set var="subhead" value="${section.key}-subhead"/>
                                <div class="sectionhead"><%-- sechead:[${sechead}]  subhead:[${subhead}] --%>
                                    <c:if test="${empty section.heading}"><%--
                                --%><h2>${constants.strings[sechead]}</h2><!-- constants.string[sechead] --><%--
                                --%></c:if><%--
                                --%><c:if test="${not empty section.heading}">
                                    <h2 class="whitespacepreserve"<c:if test="${section.headerEditable}"> id="P${section.recordNumber}"</c:if>>${section.heading}</h2><!-- section.heading -->
                                    <c:if test="${section.headerEditable}">
                                    &nbsp;&nbsp;
                                    <div class="buttonset">
                                        <input type="submit" id="EH${section.recordNumber}" name="EH${section.recordNumber}" value="Edit" title="Edit">
                                        <input type="submit" id="DH${section.recordNumber}" name="DH${section.recordNumber}" class="recorddel sectionhead" value="Delete" title="Delete">
                                    </div><%--
                                --%></c:if><%-- if header is editable
                                --%></c:if><%-- if there is a heading --%>
                                    &nbsp;<%-- This nbsp is important: it's what gives the div.sectionhead some height --%><%--
                                /* Leave this next line, in case we re-enable the 'display in dossier' checkbox */
                                /* This is the 'column heading' above the checkboxes */
                                    <div class="displaycb">Display</div>        --%>
                                <%--  <h3>&nbsp;${constants.strings[subhead]}</h3>  -- The nbsp here in the h3 is important! --%>
                                </div><!-- sectionhead -->

                                <c:if test="${type=='additional'}">
                                <div class="additionalbutton buttonset"><%--
                                --%><c:set var="additionalbuttonTitle" value="Add a New Record"/><%--
                                --%><c:if test="${not empty section.heading}"><%--
                                    --%><c:set var="additionalbuttonTitle" value="Add a New Record for ${section.heading} section"/><%--
                                --%></c:if>
                                    <input type="submit" id="AR${section.recordNumber}" name="AR${section.recordNumber}" value="Add a New Record" title="${additionalbuttonTitle}">
                                </div><!-- .additionalbutton -->
                                </c:if>

                                <c:if test="${not empty recset}">
                                <ul class="mivrecords" id="${sectionid}-records">
                                <%-- Start of the RECORD Loop : a list item is created for each of the records in the record set.
                            --%><c:forEach var="entry" items="${recset}"><c:set var="rec" value="${entry.value}"/><%--
                                --%><c:set var="noprint" value=""/><%--
                                --%><c:if test="${rec.display=='0'||rec.display=='false'}"><%--
                                    --%><c:set var="noprint" value=" noprint"/><%--
                                    --%><c:set var="noprintFound" value="${true}"/><%--
                                --%></c:if><%--
                                --%><c:set var="previous" value=""/><c:if test="${param.recid==entry.key}"><c:set var="previous" value="previous "/></c:if>
                                    <li rel="sequencekey:${rec.sequencekey}" class="${previous}records${noprint}" id="P${entry.key}">
                                        <div class="entry">
                                        <%--   <!-- rec: ${rec} --> --%>
                                        <%--/*  if rec.year is empty, the firefox was not obeying the width attribute specified in CSS (which is actually correct as per W3C)
                                                but IE still obey the width even if the content is empty. Even min-width attribute and whitespaces did not apply the width for empty element.
                                                so in order to look the preview consitent adding a new clause to check if rec.year is empty add a &nbsp; */ --%><%--
                                        --%><c:set var="yearclass" value="${constants.classes.year}"/><%--
                                        --%><c:if test="${not empty rec.yearclass}"><%--
                                            --%><c:set var="yearclass" value="${yearclass} ${rec.yearclass}"/><%--
                                        --%></c:if>
                                            <div class="${yearclass}">${fn:length(fn:trim(rec.year)) > 0 ? rec.year : '&nbsp;'}</div>
                                            <div class="recordentry">
                                            <c:choose><%--
                                            --%><c:when test="${fn:length(fn:trim(rec.preview)) > 0}"><%--
                                            --%><div class="preview ${section.key}-records">
                                                    ${rec.preview}
                                                    <c:if test="${not empty rec.link}">
                                                    <div class="record-links">
                                                        ${rec.link}
                                                    </div>
                                                    </c:if>
                                                </div><!-- .preview -->

                                                <%-- See if there are reference table records --%><%--
                                            --%><c:if test="${rec.associatedRecordCount > 0}"><%--
                                            --%><%-- There are reference table records. Iterate through them --%>
                                                <ul>
                                                <c:forEach begin="0" end="${rec.associatedRecordCount-1}" varStatus="count"><%--
                                                --%><c:set var="linkkey" value="link${count.index}"/><%--
                                                --%><c:set var="key" value="preview${count.index}"/><%--
                                                --%><c:set var="recordtypekey" value="previewrecordtype${count.index}"/>
                                                    <li class="associatedrecord ${rec[recordtypekey]}">
                                                        <div class="preview ${rec[recordtypekey]}-records">${rec[key]}
                                                        <c:if test="${not empty rec[linkkey]}">
                                                            <div class="record-links">
                                                                ${rec[linkkey]}
                                                            </div>
                                                        </c:if>
                                                        </div>
                                                        <%--<span class="preview ${section.key}-records">( Debug: Record Type = ${rec[recordtypekey]} )</span>--%>
                                                    </li>
                                                </c:forEach>
                                                </ul>
                                                </c:if>
                                                </c:when>
                                              <%--  These two cases haven't been used for years.
                                                <c:when test="${fn:length(fn:trim(rec.title)) <= 0}">
                                                [ <span class="reference">${rec.citation}</span> ]
                                                </c:when>
                                                <c:otherwise>{
                                                ${rec.author}
                                                <span class="title">${rec.title}</span>
                                                <c:if test="${not empty rec.journal}"><!-- Feb26 added if for BookCh -->
                                                ${rec.journal},</c:if>
                                                <!--VolumeIssue-->${rec.VolumeIssue}<!--/VolumeIssue-->
                                                <!--pages-->${rec.pages}<!--/pages-->
                                                V:${rec.volume} I:${rec.issue} P:${rec.pages}
                                                }</c:otherwise>
                                              --%>
                                            </c:choose>
                                            </div><!-- recordentry -->

                                            <div class="packetdisplay"><%--
                                            /* Again, this is for the "display in dossier" checkbox. Leave it here.
                                                <div class="display">
                                                    <input type="checkbox"${rec.display=='1'||rec.display=='true'?" checked":""}>
                                                </div><!-- display --> --%>
                                                <c:if test="${rec.display=='0'||rec.display=='false'}"><!--[<span class="dagger">&dagger;</span>]--></c:if>
                                            </div><!-- dossierdisplay -->

                                            <div class="buttongroup">
                                                <div class="buttonset">
                                                    <c:if test="${empty rec.readonly}">
                                                    <input type="submit" class="recordedit"
                                                            id="E${entry.key}" name="E${entry.key}"
                                                            title="Edit Record #${entry.key}" value="Edit">
                                                    </c:if>
                                                    <input type="submit" class="recorddel"
                                                            id="D${entry.key}" name="D${entry.key}"
                                                            title="Delete Record #${entry.key}" value="Delete">
                                                </div><!-- buttonset -->
                                            </div><!-- .buttongroup -->
                                        </div><!-- entry -->
                                    </li>
                                </c:forEach><%-- End of the RECORD Loop --%>
                                </ul>
                                </c:if>
                            </div><!-- .section -->
                        </li>
                    </c:forEach><%-- END OF THE SECTION LOOP --%>
                    </ul>
                </div><!-- style=display:inline -->
            </form>

            <!-- Bottom set: Add Record and Resequence Buttons -->
            <div class="addrecord">
                <div class="buttonset">
                <c:choose>
                    <c:when test="${not empty constants.config.editurl}">
                    <a class="linktobutton" title="${constants.tooltips.addrecord}" href="<c:url value='${constants.config.editurl}'/>">${constants.strings.addrecord}</a>
                    </c:when>
                    <c:otherwise>
                    <form id="addform2" class="recordlist" method="get" action="<c:url value='${constants.config.editorservlet}'/>">
                        <input type="hidden" id="rectype2A" name="rectype" value="${type}">
                        <input type="hidden" id="pubtype2A" name="pubtype" value="${pubtype}">
                        <input type="hidden" id="documentid2A" name="documentid" value="${documentid}">
                        <input type="hidden" id="sectionname2A" name="sectionname" value="${sectionname}">
                        <input type="hidden" id="subtype2A" name="subtype" value="${subtype}">
                        <input type="submit" id="E0000-2" name="E0000" value="${constants.strings.addrecord}" title="${constants.tooltips.addrecord}">
                    </form>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${constants.config.ordering=='fixed'}"><%-- put the reseq button unless order is fixed --%>
                    <!-- order is "fixed" == no resequence button -->
                    </c:when>
                    <c:otherwise>
                        <c:set var="resequenceable" value="" />
                        <c:if test="${empty sections}">
                            <c:set var="resequenceable" value=" disabled" />
                        </c:if>
                    <input type="button" id="SaveResequence2" class="SaveResequence hidden" value="Save Resequencing" title="Click to save resequence">
                    <a href="<c:url value='${constants.config.itemlistservlet}?type=${mainRecType}&recordClass=${recordClass}&sectionname=${sectionname}&documentid=${documentid}&subtype=${subtype}'/>"
                       id="CancelResequence2" class="CancelResequence linktobutton hidden" title="Click to cancel resequence">Cancel Resequencing</a>
                    <!-- <input type="button" id="CancelResequence2" class="CancelResequence hidden" value="Cancel Resequencing" title="Click to cancel resequence"> -->
                    <input type="button" id="ActiveResequence2" class="ActiveResequence" value="Resequence" title="Click to resequence records or sections">
                    </c:otherwise>
                </c:choose>
                </div><!-- buttonset -->
            </div><!-- addrecord -->
            </c:otherwise>
        </c:choose><%-- the list was not empty --%>
        </div><!-- listarea -->

        <%-- c:if test="${noprintFound}">
        <!--
        <div id="notes" class="noprint">
            <span class="dagger">&dagger;</span> = Item not included in Dossier
        </div>
        -->
        <!-- notes -->
        </c:if --%>

    </div><!-- main -->

    <jsp:include page="/jsp/mivfooter.jsp" />
    <%-- This next div is here purely for the javascript to use.
         Leave it alone unless you are very familiar with javascript, Java,
         the MIV "Item Listing" servlet, and the refactored MIV system in general.
    --%>
    <div style="display:none;">
        <span id="EditorServlet" class="${constants.config.editorservlet}"></span>
        <span id="DeleteServlet" class="${constants.config.deleteservlet}"></span>
    </div>
                                                                <%--
    <!-- Constants display, and Config in particular -->
    <!-- div>
        <h2>Constants</h2>
        ${constants}
    </div>
    <div>
        <h2>Params</h2><c:forEach var="p" items="${param}">
        ${p.key}         ${p.value}
        </c:forEach>
        <h2>Config</h2>
        ${constants.config}
    </div>
    <div style="margin-bottom:2ex">&nbsp;</div -->
    <!-- end of Constants -->
                                                                --%>
<script type="text/javascript">
/* <![CDATA[ */
// FIXME: Javascript should not be in the page; it should be in a separate .js file.
$(document).ready(function() {

    $(".recorddel").each(function(index) {
          if ($(this).attr('type')!='button') {
              // type property can’t be changed using jquery
              //document.getElementById($(this).attr('id')).setAttribute('type','button'); // not working in IE
              changeInputType(document.getElementById($(this).attr('id')),"button");
          }
    });

    var isSectionReSeq = "${type}"=="additional";

    if ("${constants.config.ordering}"!="fixed") {
        setupResequence(isSectionReSeq);
    }

//  if ("${not empty param.recid}" == "true")
    if (${not empty param.recid})
    {
        var curtop = 0;

        // function exists() included into jquery-ui.plugins.js
        if ($("#P${param.recid}").exists()) {
            curtop = $("#P${param.recid}").position().top;
        }

        // MIV-4515: After changing a publication Type, Edit Confirmation displays at the very top of the browser window
        if (isNaN(curtop) || curtop == 0) {
            curtop = 175;
        }

        if (${fn:length(CompletedMessage)>0})
        {
            showMessage("CompletedMessage", null, curtop, true);

            // modifying history using pushState
            if (typeof history.pushState == 'function')
            {
                var url='ItemList?type=${type}';

                if ("${recordClass}".length != 0) {
                        url += "&recordClass=${recordClass}";
                }
                if ("${sectionname}".length != 0) {
                        url += "&sectionname=${sectionname}";
                }
                if ("${documentid}".length != 0) {
                        url += "&documentid=${documentid}";
                }
                if ("${subtype}".length != 0) {
                        url += "&subtype=${subtype}";
                }

                var stateObj = {};
                history.pushState(stateObj, "dummy", url);
            }
        }
    }
    else
    {
        setContainedStickyScroll('#header');
    }
});
/* ]]> */
</script>

</body>
</html>
<%--
 vim:ts=8 sw=4 et sr:
--%>
