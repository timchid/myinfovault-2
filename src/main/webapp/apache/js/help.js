addLoadEvent( function() { setTimeout(initHelp, 1); } );

function initHelp()
{
    var links = document.getElementsByTagName("a");

    for (var i=0; i<links.length; i++)
    {
        if (links[i].target == "mivhelp") {
            links[i].onclick = openhelpwindow;
        }
    }
}

function openhelpwindow(evt)
{
    var link = {};

    // IE handling
    var e = evt || window.event;

    if (e.target) {
        // Mozilla handling
        link = e.target;
    }
    else if (e.srcElement) {
        // IE handling
        link = e.srcElement;
    }

    var page = link['href'];

    if (page) window.open(page, "mivhelp", "status=1,menubar=1,toolbar=1,location=1,height=425,width=655,scrollbars=1,resizable=1").focus();
}

/*
 * Set a window.onload event, ensuring that we're adding to,
 * not replacing, any already established onload handlers.
 *
 * From Simon Willison's Weblog,
 *   http://simon.incutio.com/archive/2004/05/26/addLoadEvent
 * and as used in Behaviour,
 *   http://bennolan.com/behaviour/
 */
function addLoadEvent(func)
{
    var oldonload = window.onload;

    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            oldonload();
            func();
        };
    }
}
/*<span class="vim">
 vim:ts=8 sw=4 et sm:
</span>*/
