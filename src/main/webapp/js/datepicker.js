'use strict';
// For jQuery-UI datepicker

// Modernizr checks for HTML5 native datepicker support.
// Only implement the jQuery datepicker if there is no native datepicker support.

/*if (!Modernizr.inputtypes.date) {
	$(function() {
		//$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd', constrainInput: false });
		$('.datepicker').datepicker({
			//dateFormat : 'mm/dd/yy',
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			constrainInput : false,
			yearRange: '1900:c+10'
		});
	});
}*/

$(function() {
	if ( typeof jQuery.ui !== 'undefined' ) {
		$('.datepicker').datepicker({
			dateFormat : 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			constrainInput : false,
			yearRange: '1900:c+10',
			currentText: 'Find Today',
			closeText: 'Close',
			buttonImage: '/miv/images/buttons/datepicker.png',
			buttonImageOnly: true,
			showOn: 'both'
		});

		$('img[class="ui-datepicker-trigger"]').each(function() {
			$(this).attr('title','Click to open a datepicker. format is (yy-mm-dd)');
		});
	}
});
