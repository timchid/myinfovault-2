'use strict';
/* exported isFloat, isBlank */
/* global dijit, getElementByName, getElementById */
// This first line will destroy the miv object if it already exists.
// Consider:
//   var miv = miv || {};   -or-
//   window.miv = window.miv || {};

// Pradeep K Haldiya (2013-10-22) - removing dojo calls and replacing with jquery calls

var miv = miv || {};

miv.validation = {
    validate: function (form, profile) {
        // Essentially private properties of results object
        var $required = $();
        var missing = [];
        var invalid = [];
        var errorFields = [];
        var $formFields = $();
        var orderedErrorList=[];

        if (form !== undefined) {
            $formFields = $('fieldset, *:input:not(:button):not(:submit):not(:checked), select, textarea, table.dataTable',form);
        }

        profile = profile || {};

        // results object summarizes the validation
        var results = {
            isSuccessful : function() {
                return (!this.hasInvalid() && !this.hasMissing());
            },

            isRequired : function(element) {
                return $required.index(element) >= 0;
            },

            hasMissing : function() {
                return (missing.length > 0);
            },

            getMissing : function() {
                return missing;
            },

            isMissing : function(elemObj) {
                for (var i = 0; i < missing.length; i++) {

                    if ($(elemObj).is(missing[i].field_element)) {
                        return true;
                    }

                }
                return false;
            },

            getMissingElement : function(elemObj) {
                for (var i = 0; i < missing.length; i++) {

                    if ($(elemObj).is(missing[i].field_element)) {
                        return missing[i];
                    }

                }
                return undefined;
            },

            hasInvalid : function() {
                return (invalid.length > 0);
            },

            getInvalid : function() {
                return invalid;
            },

            isInvalid : function(elemObj) {
                for (var i = 0; i < invalid.length; i++) {

                    if ($(elemObj).is(invalid[i].field_element)) {
                        return true;
                    }

                }
                return false;
            },

            getInvalidElement : function(elemObj) {
                for (var i = 0; i < invalid.length; i++) {

                    if ($(elemObj).is(invalid[i].field_element)) {
                        return invalid[i];
                    }

                }
                return undefined;
            },

            getErrorCount : function() {
                return invalid.length + missing.length;
            },

            getErrorFields : function() {
                return errorFields;
            },

            getFirstErrorField : function() {

                if (orderedErrorList.length > 0) {
                    return orderedErrorList[0];
                }

                return undefined;
            },

            getFormFields : function() {
                return $formFields;
            },

            getOrderedErrorList: function() {
                return orderedErrorList;
            }
        };

        var console = window.console || { 'log' : function(){} };

        // Trim removes white space at the front and end of the fields.
        // check of text and textarea input types and trim the value
        var doTrim = profile.trim;

        var fnTrim = function(i, e) {
            var $e = $(e);

            $e.val($.trim($e.val()));
        };

        if (doTrim)
        {
            for (var i = 0; i < doTrim.length; i++)
            {
                var field = doTrim[i];
                if (jQuery.type(field) !== 'string') { continue; }

                var $element = getElementById(field);

                /*
                 * If field is not a valid ID, treat as a selector in the form context.
                 */
                if (!$element.exists())
                {
                    $element = $(field, form);
                }

                if (!$element.exists())
                {
                    console.log('This trim element does not exist in the form: ' + field + ' --validation.js');
                    continue;
                }

                // trim text and text area elements
                $('input[type=text], textarea', $element).each(fnTrim);
            }
        }

        // See if required input fields have values missing.
        var doRequired = profile.required;
        if (doRequired)
        {
            if (typeof doRequired === 'string')
            {
                doRequired = new Array(doRequired);
            }

            for (var j = 0; j < doRequired.length; j++)
            {
                var reqField = doRequired[j];
                if (jQuery.type(reqField) !== 'string') { continue; }

                var $reqElement = getElementById(reqField);

                /*
                 * If field is not a valid ID, treat as a selector in the form context.
                 */
                if (!$reqElement.exists()) {
                    $reqElement = $(reqField, form);
                }

                if (!$reqElement.exists())
                {
                    console.log('This required element is not in the form: ' + reqField + ' --validation.js');
                    continue;
                }

                // add element(s) to required
                $required = $required.add($reqElement);
            }
        }

        /*
         * Build array of missing elements (elements that are required and blank).
         */
        $required.each(function(i, e) {
            var $element = $(e);

            if (isBlank($element))
            {
                missing.push({'field_element' : $element, 'error_code' : 'REQUIRED'});
                errorFields.push($element);
            }
        }); // end for loop

        // Find invalid input fields.
        var doConstraints = profile.constraints;
        if (doConstraints)
        {
            // $.map( map, function( value, key ) {
            $.each(doConstraints, function(field, routine)
            {
                if (jQuery.type(field) !== 'string' || jQuery.type(routine) !== 'function') { return true; }//continue

                // Get the element to validate by ID
                var $element = getElementById(field);

                /*
                 * If field is not a valid ID, try as a selector in the form context.
                 */
                var bSelector = false;
                if (!$element.exists())
                {
                    $element = $(field, form);
                    bSelector = $element.exists();

                    if (!bSelector)
                    {
                        console.log('This constraint field does not exist in the form: ' + field + ' --validation.js');
                    }
                }

                $element.each(function(i, e)
                {
                    var $e = $(e);

                    // Apply constraint if element is not blank
                    if (!isBlank($e))
                    {
                        /*
                         * Call the function associated with the element.
                         *
                         * If the element was derived from a selector, pass the element.
                         * Else, the element was derived from an ID, pass the value and ID.
                         *
                         * For jquery selectors, the function is applied to each element (wrapped
                         * in a jquery object).
                         *
                         * TODO: Later, make the function always take a jQuery object.
                         */
                        var isValid = bSelector ? routine($e) : routine($e.val(), $e.prop('id'));

                        /*
                         * isValid is either:
                         *  - a boolean 'true'
                         *  - a String with an error key
                         *  - object with field and error key
                         */
                        if (isValid !== true)
                        {
                            /* This didn't really work, and you'll see that the 'then' branch and 'else' branch
                             * do essentially the same thing -- but I'm leaving it in because I think moving to
                             * an object-based vs. string-based validation indicator is where we should move.
                             * This was for MIV-5400
                             */
                            if (isValid.hasOwnProperty('fld'))
                            {
                                //invalid.push({ 'field_element' : isValid.fld, 'error_code' : isValid.message });
                                //errorFields.push($element);
                                // swap the two $element uses, see if that works
                                invalid.push({ 'field_element' : $e, 'error_code' : isValid.message });
                                errorFields.push($e/*isValid.fld*/);

                                isValid = isValid.message;
                            }
                            else
                            {
                                invalid.push({'field_element' : $e, 'error_code' : isValid});
                                errorFields.push($e);
                            }
                        }
                    }
                });
            });
        }
        // end of for (name in checkit)

        // Generate the ordered error List
        $formFields.each(function(i, e)
        {
            var f;
            var $fieldElement = $(e);
            if ( results.isMissing($fieldElement) ) {
                f = results.getMissingElement($fieldElement);
            }
            else if ( results.isInvalid($fieldElement) ) {
                f = results.getInvalidElement($fieldElement);
            }

            if (f !== undefined && jQuery.inArray(f, orderedErrorList) === -1) {
                orderedErrorList.push(f);
            }
        });
        return results;
    },

    isNumber: function (field) {
        if ( isNaN(parseFloat(field)) || !isFinite(field) ) {
            return 'INVALID_NUMBER';
        }
        return true;
    },

    isInteger: function (field) {
        var numOne = 1;
        var testField = field * numOne;
        var isValid;
        if (testField === 'NaN') {
            return 'INVALID_INTEGER';
        }
        else {
            var rounded = Math.round(testField);
            isValid = (rounded == testField); // jshint ignore:line
            if (!isValid) {
                return 'INVALID_INTEGER';
            }
        }

        return isValid;
    },

    isDropdown: function (field) {
        if (field === '' || field === 0) {
            return 'REQUIRED';
        }
        return true;
    },

    isLink: function (field) {
        var re = /^(https?|ftp):\/\//;

        if (!(field.length < 1 || field.match(re))) {
            return 'INVALID_LINK';
        }

        return true;
    },

    onKeyPress: function (/*Boolean*\/ isFocused*/) { //isFocused not used.
        return this.validate();

    },

    onBlur: function () {
        var isValid = this.validate();

        dijit.hideTooltip(this.domNode);
        if (!isValid) {
            var message = this.getErrorMessage(true);
            dijit.showTooltip(message, this.domNode, this.tooltipPosition);
        }
    }

}; // end of the 'validation' object

// Is this used somewhere in a way that it can't be part of the miv.validation
// object?
// If so, explain it; or better, fix where it's used so this can follow "isInteger:" above.
function isFloat(value, max, min)
{
    var isValid = true;

    if (value.length > 0)
    {
        isValid = miv.validation.isNumber(value);

        if (isValid)
        {
            var floatValue = parseFloat(value);
            if (max !== undefined && floatValue > parseFloat(max)) {
                return 'INVALID_MAX_VALUE';
            }
            else if (min !== undefined && floatValue < parseFloat(min)) {
                return 'INVALID_MIN_VALUE';
            }
        }
    }
    return isValid;
}

/*
 * Key entry input element types.
 *
 * TODO: add "password", "file", "url"?
 */
var keyEntryTypes = ['text', 'search', 'textarea', 'number', 'email'];

/**
 * Determine if the given jQuery element is a "blank" form field.
 */
function isBlank($element)
{
    var sType = $element.prop('type');

    // key entry type?
    if ($.inArray(sType, keyEntryTypes) > -1)
    {
        // remove all tags and trim the value then check for length of text
        if ($.trim($element.val()).length === 0)
        {
            return true;
        }
    }
    // Does drop-down box have option selected.
    else if (((sType === 'select-one' &&
                    ($element.val() === '0' || $element.val() === '-1' || $.trim($element.val()) === '')) ||
                    (sType === 'select-multiple' && $.trim($element.val()) === '')))
    {
        return true;
    }
    else if (sType === 'radio')
    {
        $element = getElementByName($element.prop('name'));
        var $checkedElement = $element.filter(':checked');

        if (typeof $checkedElement !== 'undefined' && $.trim($checkedElement.val()).length === 0)
        {
            return true;
        }
    }

    return false;
}


/*<span class="vim">
 vim:ts=8 sw=4 et sr:
</span>*/
