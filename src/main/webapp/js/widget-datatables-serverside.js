'use strict';

$(function() {
    /*
     * Empty Data Tables JSON response.
     */
    var emptyResponse = {
        draw            : 0,
        data            : {},
        recordsFiltered : 0,
        recordsTotal    : 0
    };

    /**
     * @typedef {Object} serverside~ui
     * @description The ui object passed into callbacks from this widget. Contains the affected row and its associated data.
     * @property {jQuery#Object} row - Selected row
     * @property {Object} data - Data backing selected row
     */

    /**
     * @fileOverview DataTables server-side table widget.
     * @author Craig Gilmore
     * @version 1.0
     * @namespace serverside
     * @memberof jQuery.custom
     * @example
     * // apply server-side widget to 'div'
     * var element = $('div#element').serverside(results.serverside({
     *     source  : '/miv/DataTables/dossier',
     *     columns : [ { title : 'Candidate',            data : 'action.candidate' },
     *                 { title : 'Action Type',          data : 'action.actionType.description' },
     *                 { title : 'Delegation Authority', data : 'action.delegationAuthority.description' } ],
     *     // attach handler to 'select' event in options
     *     select  : function(event, ui) {
     *                   console.log(ui);
     *               },
     *     draw    : function(api) {
     *                   console.log(api.page.info().recordsTotal);
     *               }
     * });
     *
     * // select odd rows
     * element.serverside('select', 'tr.odd');
     *
     * // select even rows
     * var api = element.data('custom-serverside');
     * api.select('tr.even');
     *
     * // attach handler to the server-side widget 'details' event
     * element.on('serversidedetails', function(event, ui) {
     *     console.log(ui);
     * });
     * @see jQuery.dataTables
     */
    $.widget('custom.serverside', {
        /**
         * @member serverside#options
         * @description Widget options
         * @property {boolean}                [selectMultiple=true] If multiple records may be selected
         * @property {boolean}                [bDetails=true]       If hidden column data is to be shown in an expandable details pane per row
         * @property {string}                 source                URL to JSON data source
         * @property {Object}                 [params]              name-to-value URL parameters
         * @property {columns}                columns               Table columns
         * @property {serverside#draw}        [draw=$.noop]         Called after each table draw
         * @property {serverside#init}        [init=$.noop]         Called after each the table is first initialized
         * @property {serverside#fail}        [fail=$.noop]         Called if data tables AJAX request fails
         */
        options  : {
            selectMultiple : true,
            bDetails : true,
            ordering : true,
            params : {},
            draw : $.noop,
            init : $.noop,
            fail : $.noop
        },

        /**
         * @private
         * @description Initialize this widget.
         */
        _create : function() {
            var self = this;

            // A Data Tables request failed
            var fail = function(statusCode, statusText) {
                console.log('Request Failed\n\tStatus code: ' + statusCode +
                            '\n\tStatus Text: ' + statusText);

                /**
                 * @callback serverside#fail
                 * @description AJAX fail event callback.
                 * @param {number} statusCode HTTP status code
                 * @param {string} statusText HTTP status text
                 */
                self.options.fail(statusCode, statusText);
            };

            // table DOM JQuery selector
            self.table = $('<table>').prop('id', self.options.type + 'dt').addClass('display').appendTo(self.element);

            $.each(self.options.columns, function(index, column) {
                /*
                 * Add details control as first column if:
                 *   - details initially enabled
                 *   - there's at least one hidden column with a title set
                 */
                if (self.options.bDetails && column.visible === false && column.title) {
                    self.options.bDetails = true;
                }

                // set default content to empty string for each column (if not already set)
                column.defaultContent = column.defaultContent || '';
            });

            if (self.options.bDetails) {
                self.options.columns.unshift({
                    className      : 'details-control',
                    orderable      : false,
                    data           : null,
                    defaultContent : ''
                });
            }

            /*
             * Maintain set of selected and expanded rows.
             */
            self.selectedRows = {};
            self.expandedRows = {};

            // apply Data Tables
            self.dt = self.table.DataTable({
                processing : true,
                serverSide : true,
                bAutoWidth : false,
                oLanguage : {
                    sSearch : 'Find in all: '
                },
                ajax : function(data, callback) {
                    for (var i in self.options.columns) {
                        if (self.options.columns.hasOwnProperty(i)) {
                            var render = self.options.columns[i].render;

                            // sending rendering with request to ensure that each reference is included in the response
                            if (render) {
                                if (typeof render === 'string') {
                                    render = { '_' : render };
                                }

                                data.columns[i].render = render;
                            }
                        }
                    }

                    $.getJSON(self.options.source,
                              $.extend({},
                                       self.options.params,
                                       { 'command' : JSON.stringify(data) })
                    ).done(function(json) {
                         /*
                          * Fail if error value set in JSON.
                          *
                          * TODO: When we upgrade to Spring MVC 3+, 'error' will not
                          * be set as a result of exceptions on the server, instead,
                          * the server will respond with the proper status code.
                          */
                         if (json.error) {
                             fail(500, json.error);
                         }

                         callback(json);
                    }).fail(function(jqxhr) {
                         fail(jqxhr.status, jqxhr.statusText);

                         callback(emptyResponse);
                    });
                },
                columns : self.options.columns,
                ordering : self.options.ordering,
                order : [[self.options.bDetails ? 1 : 0, 'asc']],
                headerCallback : function(thead/*, data, start, end, display*/) {
                    $('th', thead).attr('scope', 'col');
                },
                fnRowCallback : function(row, data) {
                    var $row = $(row);

                    /*
                     * Adding to the row selector and row DOM attributes the
                     * name-value pairs in the row data's "DT_RowData" object.
                     *
                     * TODO: This fixes what seems to be a bug in the current
                     * version of Data Tables (v1.10.4). Perhaps in later
                     * versions, this can be removed.
                     */
                    for (var name in data.DT_RowData) {
                        if (data.DT_RowData.hasOwnProperty(name)) {
                            var value = data.DT_RowData[name];

                            // add data name-value pair to the row selector data
                            $row.data(name, value);

                            // add data name-value as row attributes with the "data-" prefix.
                            $row.attr('data-' + name, value);
                        }
                    }

                    // row was selected before this draw
                    if (self.selectedRows[data.DT_RowId]) {
                        $row.addClass('selected');
                    }

                    // row was expanded before this draw
                    if (self.expandedRows[data.DT_RowId]) {
                        self.showDetails(row);
                    }

                    /**
                     * @function serverside~anonymous
                     * @listens jQuery#click
                     * @description De-selects or selects enclosed table row if row is currently selected or not selected, respectively
                     * @param {jQuery#event:click} event Click event
                     */
                    $row.not('[data-disabled]').find('td:not(.details-control)').on('click', function(event) {
                        $row.is('.selected') ? self.deselect(row, event) : self.select(row, event); //jshint ignore:line
                    });

                    /**
                     * @function serverside~anonymous
                     * @listens jQuery#click
                     * @description Hides or shows enclosed table row details if row details are currently shown or hidden, respectively
                     * @param {jQuery#event:click} event Click event
                     */
                    $row.find('td.details-control').on('click', function(event) {
                        $row.is('.shown') ? self.hideDetails(row, event) : self.showDetails(row, event); //jshint ignore:line
                    });
                },
                drawCallback : function(/*settings*/) {
                    /**
                     * @callback serverside#draw
                     * @description Called after each table draw
                     * @param {jQuery.dataTables#Object} api Data tables API
                     */
                    self.options.draw(this.api());
                },
                initComplete : function(/*settings, json*/) {
                    /**
                     * @callback serverside#init
                     * @description Called after the table is first initialized
                     * @param {jQuery.dataTables#Object} api Data tables API
                     */
                    self.options.init(this.api());
                }
            });
        },

        /**
         * @method serverside#select
         * @description Select table rows.
         * @param {jQuery.dataTables#row-selector} selector Rows to select
         * @param {jQuery#Event}            [event]  Event triggering the selection
         * @fires serverside#serversideselect
         */
        select : function(selector, event) {
            var self = this;

            // use DT 'rows' for multiple selection, 'row' for singular (gets first row matching selector)
            var rows = this.options.selectMultiple ? this.dt.rows(selector) : this.dt.row(selector);

            // loop thru each row index
            rows.indexes().each(function(index) {
                // table row api
                var r = self.dt.row(index);

                var data = r.data();

                // continue if row is marked disabled
                if (data.DT_RowData && data.DT_RowData.disabled) { return true; }

                // reset current selections if multiple select is disabled
                if (!self.options.selectMultiple) {
                    $('.selected', self.table).removeClass('selected');

                    self.selectedRows = {};
                }

                // attempt to select if row is not selected
                if (!self.selectedRows[data.DT_RowId]) {
                    var row = $(r.node());

                    row.addClass('selected');

                    // mark this row as selected
                    self.selectedRows[data.DT_RowId] = row;

                    // fire row selected event, if there's a parent event
                    if (event) {
                        /**
                         * @event serverside#serversideselect
                         * @description Called after a row is selected
                         * @param {jQuery#Event}  event   Server-side select event
                         * @param {serverside~ui} ui
                         */
                        self._trigger('select', event, {
                            row  : row,
                            data : data
                        });
                    }
                }
            });
        },

        /**
         * @method serverside#deselect
         * @description De-select table rows.
         * @param {jQuery.dataTables#row-selector} selector Rows to de-select
         * @param {jQuery#Event}            [event]  Event triggering the selection
         * @fires serverside#serversidedeselect
         */
        deselect : function(selector, event) {
            var self = this;

            // loop thru each row matching the selector
            this.dt.rows(selector).indexes().each(function(index) {
                // table row api
                var r = self.dt.row(index);

                var data = r.data();

                // attempt to de-select if row is selected
                if (self.selectedRows[data.DT_RowId]) {
                    var row = $(r.node());

                    row.removeClass('selected');

                    // unmark this row as selected
                    delete self.selectedRows[data.DT_RowId];

                    // fire row de-selected event, if there's a parent event
                    if (event) {
                        /**
                         * @event serverside#serversidedeselect
                         * @description Called after a row is de-selected
                         * @param {jQuery#Event}  event   Server-side de-select event
                         * @param {serverside~ui}        ui
                         */
                        self._trigger('deselect', event, {
                            row  : row,
                            data : data
                        });
                    }
                }
            });
        },

        /**
         * @method serverside#showDetails
         * @description Show row details.
         * @param {jQuery.dataTables#row-selector} selector Rows for which to show details
         * @param {jQuery#Event}            [event]  Event triggering the details to be shown
         * @fires serverside#serversidedetails
         */
        showDetails : function(selector, event) {
            var self = this;

            // loop thru each row matching the selector
            this.dt.rows(selector).indexes().each(function(index) {
                // table row api
                var r = self.dt.row(index);

                // child is hidden
                if (!r.child.isShown()) {
                    var row = $(r.node());
                    var data = r.data();

                    // if child row doesn't yet exist
                    if (!r.child()) {
                        // create a table to display this rows hidden data
                        var hiddenData = $('<table><tbody></tbody></table>');

                        // loop thru each column in this table
                        self.dt.columns().indexes().each(function(index) {
                            // table column api
                            var column = self.dt.column(index);

                            // if this column is not visible
                            if (!column.visible()) {
                                // column header text (from the column.title initialization option)
                                var label = $(column.header()).text();

                                // only if there is a label for this column
                                if (label) {
                                    // 'display' type cell value rendering
                                    var value = self.dt.cell(row, index).render('display');

                                    if (value) {
                                        // append label and rendered cell for this column and row
                                        $('tbody', hiddenData).append('<tr><th scope="row">' + label + '</th><td>' + value + '</td></tr>');
                                    }
                                }
                            }
                        });

                        // if not empty, create child row with the created table
                        if (hiddenData.find('tr').length) {
                            r.child(hiddenData);

                            var ui = {
                                row       : row,
                                data      : data,
                                container : hiddenData.parent()
                            };

                            /**
                             * @event serverside#serversidedetails
                             * @description Called after a row's details are first generated
                             * @param {jQuery#Event}  event        Server-side details event
                             * @param {serverside~ui}        ui
                             * @param {jQuery#Object} ui.container Row details container
                             */
                            self._trigger('details', event, ui);
                        }
                    }

                    // show details
                    r.child.show();
                    row.addClass('shown');

                    // mark this row expanded
                    self.expandedRows[data.DT_RowId] = row;
                }
            });
        },

        /**
         * @method serverside#hideDetails
         * @description Hide row details.
         * @param {jQuery.dataTables#row-selector} selector Rows for which to hide details
         * @param {jQuery#Event}            [event]  Event triggering the details to be hidden
         */
        hideDetails : function(selector, event) {
            var self = this;

            // loop thru each row matching the selector
            this.dt.rows(selector).indexes().each(function(index) {
                // table row api
                var r = self.dt.row(index);

                // child is currently shown
                if (r.child.isShown()) {
                    var row = $(r.node());
                    var data = r.data();

                    row.removeClass('shown');

                    // hide child row
                    r.child.hide();

                    // unmark this row as expanded
                    delete self.expandedRows[data.DT_RowId];
                }
            });
        },

        /**
         * @method serverside#filter
         * @description Filter table.
         * @param {jQuery.dataTables#column-selector} selector       Columns to filter by term
         * @param {string}                     term           Search term
         * @param {boolean}                    [bRegex=false] If search term is a regular expression
         */
        filter : function(selector, term, bRegex) {
            this.dt.column(selector).search(term, !!bRegex).draw();
        },

        /**
         * @private
         * @description Destroy this widget.
         */
        _destroy : function() {
            this.dt.destroy();
            this.table.remove();
        }
    });
});
