'use strict';
/* global showLoadingDiv, showMessage,
   highlight,
   getPositionTop, setDefaultFocus, hideLoadingMessage
*/
$(document).ready(function() {
	// don't show loading image for pop-up form.
	if ($('#dialog-form').size() === 0) {
	    showLoadingDiv();
	}

	// highlight anything that needs highlighting if we're returning from a POST or something.
	setTimeout(highlightInit, 200);

	// attach handler to the "display-this-record" checkboxes
	$('.recorddisplay', $('.previewitemlist')).click(function() {
		setRecordVisibilityInDossier($(this));
	});

	// not all annotation pages accept notations...
	// only provide the pop-up notation form when the annotation form has a notation block.
	if ($('#isNotation').size() !== 0 && $('#isNotation').val()) {
		initNotationHandlers();
	}

});



/**
 * Add notation handling controls to this annotations page, and
 * setup the popup multi-record notation dialog.
 * This is called only when the current category of records allows
 * notations, and only ONCE when the page loads.
 */
function initNotationHandlers()
{
	// Insert the "Add Notations" and "Clear Selections" buttons into the page.
	$('div.section-buttonset').append('<input type="button" id="addnotations" value="Add Notations"' +
					  ' title="Add notations on all selected records" disabled>')
				  .append('<input type="button" id="clearselections"' +
					  ' value="Clear Selected Records" title="Clear selected records" class="hidden">');

	// (grab the buttons for use below)
	var $addnotations = $('#addnotations');
	var $clearselections = $('#clearselections');


	// Attach the "Add Notations" button handler.
	$addnotations.click(function() {
	    $('#actionResult').val(''); // clear any previous results
	    openNotationPopup();
	});


	// Attach the "Clear Selected Records" button handler.
	$clearselections.click(function() {
	    // un-select all records by removing the class.
	    $('.publine.ui-select', $('.previewitemlist')).removeClass('ui-select');
	    $addnotations.prop('disabled', true);	// disable the "Add Notations" button.
	    $clearselections.addClass('hidden');	// hide the "Clear" button.
	    // stop preventing text selection.
	    $('#main').off('selectstart').removeClass('unselectable');
	});


	// Select records with click, shift-click, or ctrl/cmd-click.
	var $publines = $('#main div.previewitemlist div.publine');
	var lastSelectedIndex = 0;
	$publines.click(function(e) {

	    if (!$(e.target).is('a.linktobutton') && !$(e.target).is('.recorddisplay')) {
		e.preventDefault();

		var $this = $(this);
		// Clicked with the ctrl key (windows/linux) or cmd-⌘/"meta" key (mac)
		// to add/remove individual records from the selection.
		if (e.ctrlKey || e.metaKey) {
		    if ($this.hasClass('ui-select')) {
			$this.removeClass('ui-select');
			if ( $('div.recordsection .ui-select').size() < 1 ) {
			    $('#main').off('selectstart').removeClass('unselectable');
			}
		    }
		    else {
			// Every sane browser uses CSS 'user-select' to prevent selection. IE 8 & 9 need to prevent the selectstart event.
			$('#main').on('selectstart', function(e) { e.preventDefault(); }).addClass('unselectable');
			$this.addClass('ui-select');
			lastSelectedIndex = $publines.index($this);
		    }
		}

		// Clicked with the shift key to extend the selection.
		else if (e.shiftKey) {
		    $('#main').on('selectstart', function(e) { e.preventDefault(); }).addClass('unselectable');
		    // Get the shift+click element
		    var selectedElementIndex = $publines.index($this);
		    var indexOfRows;

		    // Mark all records as selected between the last selected record and this one.
		    if (lastSelectedIndex < selectedElementIndex) {
			// deselect all selected records above the last selected record
			for (indexOfRows = 0; indexOfRows < lastSelectedIndex; indexOfRows++) {
			    $publines.eq(indexOfRows).removeClass('ui-select');
			}

			for (indexOfRows = lastSelectedIndex; indexOfRows <= selectedElementIndex; indexOfRows++) {
			    $publines.eq(indexOfRows).addClass('ui-select');
			}
		    }
		    else {
			for (indexOfRows = selectedElementIndex; indexOfRows <= lastSelectedIndex; indexOfRows++) {
			    $publines.eq(indexOfRows).addClass('ui-select');
			}
		    }
		}

		// Clicked w/o modifier keys - select just the one record clicked.
		else {
		    lastSelectedIndex = $publines.index($this);
		    $publines.removeClass('ui-select');
		    $this.addClass('ui-select');
		    $('#main').on('selectstart', function(e) { return false; }).addClass('unselectable');
		}

		// If the result is more than zero selected records, make sure that the
		// "Add Notations" button is enabled and the "Clear" button is visible.
		if ($('.publine.ui-select', $('.previewitemlist')).size() > 0) {
		    $addnotations.prop('disabled', false);
		    $clearselections.removeClass('hidden');
		}
		// Otherwise no selections remain, disable "Add Notations" and hide "Clear"
		else {
		    $addnotations.prop('disabled', true);
		    $clearselections.addClass('hidden');
		}
	    }

	});
}



/**
 * Open the dialog for the currently selected set of records.
 *
 * Load the dialog form file and set it up when the load completes.
 * Create the jQuery dialog object from its <div>
 */
function openNotationPopup()
{
	// Add the <div> that will hold the dialog to the page.
	$('#main').append($('<div id="dialog-form" title="Add/Edit ' + $('#annotationname').val() + ' Notations"></div>'));


	// Load the dialog/form itself from its source file.
	// Call the "complete" function/parameter when loading is done.
	var $dialogForm = $( '#dialog-form' ).load('/miv/jsp/notationform.jspf', function() {
	    setupNotationForm();

	    // Set the states of the example checkboxes.
	    $('#tri_check_asis', $('#dialog-form')).prop('checked', false).prop('indeterminate', true);
	    $('#tri_check_add', $('#dialog-form')).prop('checked', true).prop('indeterminate', false);
	    $('#tri_check_remove', $('#dialog-form')).prop('checked', false).prop('indeterminate', false);

	    // No clicking on the example checkboxes.
	    var boxSamples = $('#popup-form ul.notes li input');
	    boxSamples.click(function(e) {
		e.preventDefault();
	    });
            boxSamples.attr('tabIndex', '-1');


	});

	$dialogForm.dialog({
	    autoOpen : false,
	    height   : 270,
	    width    : 490,
	    modal    : true,

	    buttons : {
		'Save'   : function(evt) {
		    var $form = $dialogForm.contents().find('form');
		    var action = $form.attr( 'action' );
		    var formdata = null;

		    var check_states = '';


		    // Get checkboxes from the packet_nota section when in a body.packet page,
		    // or from the master_nota section when in a body.master page.
		    // The body is _either_ .master or .packet, so one of these will
		    // always select 2 checkboxes while the other selects zero.
		    var inputs = $('body.packet #packet_nota input[type=checkbox], body.master #master_nota input[type=checkbox]');
		    var somethingChanged = false;

		    // Find which checkboxes have been changed — we only send the ones that changed.
		    inputs.each(function(idx, el) {
			var $this = $(this);
			// Is the current checkbox-state property the same as the load_state?
			// ... if not, this checkbox has changed state and we want to send it.
			if (  ! $this.prop( $this.data('load_state') )  ) {
			    somethingChanged = true;
			    check_states = check_states + ( ',' + this.id + '=' + checkBoxState(this) );
			}
		    });

		    if (somethingChanged) {
			$('#cb_states', $form).val( check_states );
			formdata = $form.serialize();

			$.post( action, formdata,
				function( data ) {
				    var resultObj = JSON.parse( data );

				    if (resultObj.success) {
					var recids = [];
					$('.publine.ui-select').each(function() {
					    recids.push($(this).attr('id'));
					});
					window.location.href = window.location.href +
					    '&actionResult=notations_saved&selectedRecords=' +
					    recids.join(',');
				    }
				    else {
					showMessage(null,
					            '<div id="errorbox">There was an error saving your notations.<br>Please contact miv-help.</div>'
						   );
					$( this ).dialog( 'close' );
				    }
				}
			);
		    }
		    else { // Nothing was changed when user pressed Save.
			$( this ).dialog( 'close' );
			// message(' something re: nothing to save ');
			var msg = '<div id="deletebox">No changes were made to be saved.</div>';
			showMessage('CompletedMessage', msg);
		    }
		}, // end of Save button.

		'Cancel' : function(evt) {
		    $( this ).dialog( 'close' );
		}
	    },

	    close : function() {
		$dialogForm.remove();
	    }
	});

	$dialogForm.dialog( 'open' );
}



/**
 * Called from the .load() callback when the dialog finishes loading and is ready to be set up.
 * Set up the dialog for the current set of selections, taking into account whether this
 * is Master or Packet specific annotations.
 */
function setupNotationForm()
{
	var $form = $('#dialog-form').contents().find('form');

	var packetId = 1 * $('#annotationPacketId').val();
	$form.append( $('<input type="hidden" name="packetid" value="' + packetId + '">') );
	$form.append( $('<input type="hidden" name="selections">') );

	// Get the type of annotation (Publication, Creative Activities) and its String name.
	var annotationtype = $('#annotationtype').val();
	var annotationname = $('#annotationname').val();

	var $selected = $('.publine.ui-select');
	var selectedCount = $selected.length;

	var selectedRecs = [];         // Array of objects, 1 for each selected record, holding the selected 'rectype' and 'recid'
	var notationCounts = {};       // Count of each type of notation found in the selected records.

	$selected.each(function(/*index, value*/) {

	    var notesPresent = $(this).data('notations');
	    if (notesPresent !== undefined) {
		$.each(notesPresent.split(','),
			function(i, notation) {
			    //console.log('i=' + i + '  and notation=' + notation + '  and this=' + this);
			    if (notationCounts[notation] === undefined) {
				notationCounts[notation] = 1;
			    }
			    else {
				notationCounts[notation]++;
			    }
			});
	    }

	    selectedRecs.push(
		{
		section : $(this).data('section'), // I hope to get rid of section; record type should be enough information.
		recid   : $(this).data('recid'),
		rectype : $(this).data('rectype')
		}
	    );

	});

	//console.log('stringified notation counts: ' + JSON.stringify(notationCounts));
	$('input[name=selections]').val( JSON.stringify(selectedRecs) );

	// Now set the initial state of the checkboxes...
	setupCheckboxes($form, selectedCount, notationCounts);


	$('.annotation-label', $form).html(annotationname);
	$('#annotationtype', $form).val(annotationtype);

}


/**
 * Set the initial state of the checkboxes.
 *     If a notation-count is zero that notation's checkbox will be UNCHECKED
 *     If a notation-count matches the total selected record count that notation's checkbox will be CHECKED
 *     If 0 < notation-count < record-count the notation's checkbox will be UNCHECKED + INDETERMINATE
 */
function setupCheckboxes($form, total, notationCounts)
{
	var inputs = $('input[type=checkbox]', $form);

	inputs.each(function(i, el) {
	    var $this = $(this);
	    var which = $this.attr('id').toUpperCase();
	    var nChecked = notationCounts[which] || 0;

	    $this.prop('checked', nChecked === total);
	    $this.prop('unchecked', nChecked === 0);
            var indeterminate = nChecked > 0 && nChecked < total;
	    $this.prop('indeterminate', indeterminate);

	    if (! indeterminate) {
		$this.removeClass('ui-rotate-indeterminate-checkbox');
	    }

	    // Save the initial state as data on the checkbox element.
	    $this.data( 'load_state', checkBoxState($this) );

	    $this.click(function(e) {
		$this = $(this);
		$this.prop( 'unchecked', ! $this.prop('checked') && ! $this.prop('indeterminate') );
	    });
	});
	initTriStateCheckBox();
	
	// Now disable the master or packet checkboxes we won't be using.
        $( 'body.packet #master_nota input[type=checkbox], body.master #packet_nota input[type=checkbox]' ).prop('disabled', true);
}


function setRecordVisibilityInDossier(record)
{
    // TODO: THIS Needs to change to get the PacketId and send it along with the record-section and the record-id
	var publineDiv = $(record).closest('div.publine');
	var packetId = 1 * $('#annotationPacketId').val();
	var recType = publineDiv.data('rectype');
	var recId = publineDiv.data('recid');
	var lists = $(record).val().split('~');
	var msg = '';
	if ($(record).is(':checked')) {
		msg = '<div id="successbox">Record has been added to dossier, to apply changes create my dossier again.</div>';
	}
	else {
		msg = '<div id="deletebox">Record has been removed from dossier, to apply changes create my dossier again.</div>';
	}

	/* Send the data using post and put the results in a div */
	$.post('/miv/Annotations?requestType=save-record-visibility' +
	        '&packetid=' + packetId +
	        '&section=' + lists[0] +
		'&rectype=' + recType +
		'&recid=' + recId +
		'&display-record=' + $(record).is(':checked'),
		function(data) {
		var resultObj = JSON.parse( data );

		try {
			if (resultObj.success) {
				highlight( publineDiv.attr('id') );
				showMessage('CompletedMessage', msg, getPositionTop($(publineDiv).attr('id')));

				if ($(record).is(':checked')) {
					$(publineDiv).removeClass('inactive');
				}
				else {
					$(publineDiv).addClass('inactive');
				}

				if ($('#display').val() === 'true' || $('#display').val() === 1) {
					var recordSectionDiv = $(publineDiv).closest('div.recordsection');
					$(publineDiv).remove();

					// remove section block when it has no record in it.
					if ($('div.publine', $(recordSectionDiv)).size() === 0) {
						var recordBlockDiv = $(recordSectionDiv).closest('div.recordblock');
						$(recordSectionDiv).remove();

						// remove recordsection block when it has no record in it.
						if ($('div.recordsection', $(recordBlockDiv)).size() === 0) {
							var previewItemListDiv = $(recordBlockDiv).closest('div.previewitemlist');
							$(recordBlockDiv).remove();

							// if no section and records available for annotation set error message.
							if ($('div.recordblock', $(previewItemListDiv)).size() === 0) {
								$('#selectheader').css('display', 'none');
								$('#errormessage').css('display', 'block').append('<div id="errorbox"></div>');
								$('#errorbox').html('Annotations can be added to extending knowledge records.<br>' +
										    'You have not selected any extending knowledge records to display at ' +
										    '<a href="/miv/packet" title="Packet Management" style="color: inherit;">' +
										    '<em>Packet Management</em></a> page or click on the <em>Show All Records</em> button.</div>');
								$(previewItemListDiv).append($('#errormessage'));
							}
						}
					}
				}
			}
		} catch (e) {
			console.warn('exception \'e\' is: ' + e);
		}
	});
}


/**
 * Highlight saved record with confirmation messages.
 */
function highlightInit()
{
	var action = $('#annotationAction').val();
	var actionResult = $('#actionResult').val();
	$('#actionResult').val('');
	var isMessage = false;

	if (actionResult === 'notations_saved') {
		isMessage = true;
		var selectedRecords = $('#selectedRecords').val();
		$.each(selectedRecords.split(','), function(index, record) {
			highlight( record );
			});
		showMessage(null, '<div id="successbox">Notations has been updated successfully.</div>');
	}
	else if (actionResult === 'nothing_to_save') {
		isMessage = true;
		showMessage(null, '<div id="successbox">There is nothing to update</div>');
	}
	else {
		var message = '<div id="successbox">' + $('#annotationname').val() +
			      ' annotation has been saved successfully.</div>';
		var recordID = $('#annotationRecId').val();
		var recType = $('#annotationRecType').val();
		if (action !== null &&
		    action !== undefined &&
		    action === 'save') {
			isMessage = true;
			// highlight the saved record
			highlight('L' + recordID + recType);

			// show confirmation messages
			showMessage('CompletedMessage', message, getPositionTop('L' + recordID + recType));
		}
		else {
			setDefaultFocus();
		}
	}

	// modifying history using pushState - trim all the crazy URL parameters off.
	if (isMessage && typeof history.pushState === 'function') {
		var url = (window.location.href)
			.replace(/&recid=[^&]+/, '')
			.replace(/&section=[^&]+/, '')
			.replace(/&type=[^&]+/, '')
			.replace(/&actionType=[^&]+/, '')
			.replace(/&actionResult=[^&]+/, '')
			.replace(/&selectedRecords=[^&]+/, '');
		var stateObj = {};
		history.pushState(stateObj, 'dummy', url);
	}

	hideLoadingMessage();
}

/* vim:ts=8 sw=4 noet sr:
   EOF -- Don't put anything after this line. */
