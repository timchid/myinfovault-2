'use strict';
/* global getClickedButton, validateForm, qTipObject, aTipSize */
/* SDP 2012-05-01:
        My change, putting the "var fnInitSomething" functions inside the document.ready
        anonymous function has a few effects.
        o It takes the functions out of the global scope, reducing namespace "pollution".
        o It implicitly indicates that these are not globally available functions to be
          called by others (don't have to search the code base to see if they're being
          used somewhere).
        o It allows the Closure compiler to know they are local, and therefore optimize them better.

        This is the first file I've looked at in considering making the Google Closure Compiler
        part of the build process. One benefit is that all these comments would be stripped.
*/
// call initializations on document ready
var mivFormProfile = {
        required: [ 'group\\.name'  ],
        constraints: {
            'group\\.name' : function(value) {
                return value.length > 25 ? 'NAME_LENGTH' : true;
            },
            'group\\.description' : function(value) {
                return value.length > 100 ? 'NAME_LENGTH' : true;
            },
            '#permission' : function(element) {
                var hiddenElements = $('input[name=group\\.hidden]', element);

                // ignore if "is hidden" elements DNE
                if (!hiddenElements.length) { return true; }

                var hidden = hiddenElements.filter(':checked');

                // if no "is hidden" checkbox is checked
                if (!hidden.length) {
                    return 'HIDDEN_CHECKED';
                }

                // if checked "is hidden" checkbox is false
                if (hidden.val() === 'false')
                {
                    var roles = $('input[name^=assignedRoles]:checked', element);

                    // if no "admin roles" checkbox is checked
                    if (!roles.length) {
                        return 'ROLES_CHECKED';
                    }
                }

                return true;// valid
            }
        },
        errormap: {
            'NAME_LENGTH'        : 'Must not be greater than 25 characters',
            'DESCRIPTION_LENGTH' : 'Must not be greater than 100 characters',
            'HIDDEN_CHECKED'     : '<em>Shared</em> or <em>Private</em> must be selected',
            'ROLES_CHECKED'      : 'At least one administrator group must be selected'
            }
};
$(document).ready(function() {

    /*
     * Initialization for groupedit.jsp
     */
    var fnInitGroupEdit = function() {
        /** @const */ var groupForm = $('#assignForm');

        // get form input values
        var sharedInput = $('input[name=group\\.hidden]', groupForm);
        var rolesInput = $('input[name^="assignedRoles"]', groupForm);
        var readOnlyInput = $('input[name=group\\.readOnly]', groupForm);
        var optionLabels = $('label[for^="assignedRoles"],' +
                            'label[for^="group\\.readOnly"]', groupForm);

        // if shared/private radios exist
        if (sharedInput.length) {
            // enable/disable assigned roles based on shared/private radio
            var fnSetRolesStatus = function() {
                // disabled if not shared
                var bDisabled = sharedInput.length &&
                                sharedInput.filter(':checked').val() !== 'false';

                // set disabled status for assigned roles and readOnly inputs
                rolesInput.prop('disabled', bDisabled);
                readOnlyInput.prop('disabled', bDisabled);

                // set assigned roles, readOnly, and confidential labels
                if (bDisabled) {
                    optionLabels.addClass('disabled');
                }
                else {
                    optionLabels.removeClass('disabled');
                }
            };

            // set roles status initially and on click
            fnSetRolesStatus();
            sharedInput.click(fnSetRolesStatus);
        }

        // form validation
        groupForm.submit(function(event) {
            /*
             * "formnovalidate" attribute on selected submit button, do not validate.
             */
            if (getClickedButton(groupForm).attr('formnovalidate') !== undefined) { return; }

            var formValidation = jQuery.extend(true, {}, mivFormProfile); // to get fresh copy of mivFormProfile

            if (!validateForm(groupForm, formValidation, $('#messagebox'))) {
                event.preventDefault();
            }
        });
    };

    /*
    * Initialization for groupedit.jsp tool tips
    */
    var fnInitTooltipHelp = function() {

        $('div.helpicon').each(
            function() {
                var fieldset = $(this).closest('fieldset');
                // add specific content for the fieldset
                // qTipObject is available at qtip_config.js
                qTipObject.style.classes = qTipObject.style.classes + aTipSize.medium;
                qTipObject.content = {
                    title: { text: fieldset.children('legend').text()},
                    text: fieldset.find('div.help')
                };

                $(this).qtip(qTipObject);
        });
    };

    // Now run the initialization functions
    fnInitGroupEdit();
    fnInitTooltipHelp();

}); /* end of document.ready */
