'use strict';
/* global ENTER, fnInitDataTable, sSearch */
/* exported fnInitAssignment */
//text and titles for advanced button
var AdvancedButtonText = 'Advanced Search';
var SimpleButtonText = 'Hide Advanced Search';
var AdvancedButtonTitle = 'Select for advanced search fields';
var SimpleButtonTitle = 'Select to hide the advanced search fields';

//normalize tab indexes and clear default filter
var drawCallback = function() {
    var availableSearch = $('#available_filter input[type=text]');
    if (availableSearch.val() === 'group') {
        availableSearch.val('');
    }

    $(':focusable').each(function(index, element) {
        $(element).attr('tabindex', index);
    });
};

/*
 * Initialization for assignment.jsp
 *
 * INPUTS:
 *  - parentForm      : The JQuery selector of the form by which data is submitted for this assignment
 *  - assignedTable   : Holds the membership for the group/reviewer set being edited
 *  - availableTable  : Contains the result of the search
 *  - bReview         : Are the assignment tables called from the assign reviewers page?
 *  - bEditMembership : Is editing the assigned membership allowed?
 *  - bEditMembers    : Is editing the individual assigned members allowed? (i.e. activate/deactivate)
 *  - bEditGroups     : Is editing the assigned groups allowed? (i.e. activate/deactivate its members)
 */
var fnInitAssignment = function(assignedTable,
                                availableTable,
                                bReview,
                                bEditMembership,
                                bEditMembers,
                                bEditGroups) {
    //delete an array of rows
    function assignRows(anRow, bAssignedTable) {
        var self = (bAssignedTable ? assignedTable : availableTable).dataTable().api();

        //delete each row in the array
        for (var i in anRow) {
            if (anRow.hasOwnProperty(i)) {
                var rowApi = self.row(anRow[i]);

                //get data from this row
                var aData = rowApi.data();

                //remove data from source table
                rowApi.remove().draw();

                //toggle assigned state
                aData.bAssign = !aData.bAssign;

                var addRemove = bAssignedTable ? 'Add' : 'Remove';
                var editView = bAssignedTable ? 'View' : 'Edit';

                //add record to appropriate table
                var newRow = (bAssignedTable ?
                              availableTable :
                              assignedTable).dataTable().api()
                                            .row.add(aData)
                                            .draw()
                                            .node();

                $('.addPerson, .removePerson', newRow).text(addRemove)
                                                      .attr('title', addRemove + ' this person');
                $('.addGroup, .removeGroup', newRow).text(addRemove)
                                                    .attr('title', addRemove + ' this group');
                $('.view', newRow).text(editView + ' members')
                                  .attr('title', editView + ' group members');

                $('td', newRow).addClass('selectable');
            }
        }

        //notify user for member transfer if not a review
        if (!bReview) {
            fnNotifyUser();
        }
    }

    //view groups
    $('table').on('click', '.view', {}, function() {
        var button = $(this);
        var list = button.closest('td').find('ul');

        var assigned = button.closest('table').is(assignedTable);

        list.slideToggle(400, function() {
            if (assigned) {
                button.text(list.is(':visible') ? 'Done' : 'Edit members');
                button.attr('title', list.is(':visible') ?
                                     'Done editing group members' :
                                     'Edit group members');
            }
            else {
                button.text(list.is(':visible') ? 'Hide members' : 'View members');
                button.attr('title', list.is(':visible') ?
                                     'Hide group members' :
                                     'View group members');
            }
        });
    });

    //assign/unassign group or person.
    $('table').on('click', '.addGroup, .addPerson, .removePerson, .removeGroup', {}, function() {
        var buttonRow = $(this).closest('tr');
        var rowTable = $(this).closest('.dataTable');

        assignRows([buttonRow], rowTable.is(assignedTable));
    });

    //activate/deactivate button click event
    $('table').on('click', '[name=activatePeople], [name=deactivatePeople]', {}, function () {
        fnNotifyUser();
        var name = $(this).closest('li');
        var action = name.hasClass('inactive') ? 'Deactivate' : 'Activate';

        //sets the activate button's text and title
        $(this).attr('title', action + ' this group member').text(action);

        //toggle class
        name.toggleClass('inactive');
    });

    $('#searchtoggle').click(function() {
        var button = $(this);

        $('#searchFields').slideToggle(400, function() {
            var advancedText = $(this).is(':visible') ? SimpleButtonText : AdvancedButtonText;
            var advancedTitle = $(this).is(':visible') ? SimpleButtonTitle : AdvancedButtonTitle;

            button.text(advancedText)
                  .attr('title', advancedTitle);
        });
    });

    var columnById = {
        'search1' : 1, //name
        'inputName' : 1,
        'search2' : 2, //surname
        'lname' : 2,
        'search3' : 5, //department
        'department' : 5,
        'search4' : 5, //school/college
        'school' : 5
    };
    function processAdvancedSearch(id, filter) {
        var api = availableTable.dataTable().api();

        if (filter === '0:0' || filter === '0' || filter === 'All') {
            filter = '';
        }
        else if (id === 'search2' || id === 'lname') {//search by first letter of last name
            filter = '^' + filter;//use regex to only compare against first letter
        }
        else if (id === 'search3' || id === 'department') {
            filter = '^' + filter + '$';
        }
        else if (id === 'search4' || id === 'school') {
            filter = '^' + filter + ':';//TODO fix for no department?
        }

        //clear other filters before applying new one
        api.search('');
        for (var i = 0; i <= api.columns()[0].length; i++) {
            api.column(i).search('');
        }

        api.column(columnById[id])
           .search(filter, (id !== 'search1' && id !== 'inputName')) //filter using regex if we aren't searching by name
           .draw();
    }

    $('#searchFields input[type=submit]').click(function(event) {
        event.preventDefault();

        var id = $(this).attr('id');
        var filter = $(this).closest('.formline').find(':input:first').val();

        processAdvancedSearch(id, filter);
    });

    $('#searchFields input[type=text], #searchFields select').on('keydown', function(event) {
        if (event.keyCode === ENTER) {
            event.preventDefault();

            var id = $(this).attr('id');
            var filter = $(this).val();

            processAdvancedSearch(id, filter);
        }
    });

    //get parent form
    var parentForm = $('#assignForm');

    //stop form submission/event firing on enter
    $('input, select', parentForm).on('keydown', function(event) {
        if (event.keyCode === ENTER) {
            event.preventDefault();
        }
    });

    //This a new group edit? (True if not a review and the group name is initially empty
    var bNewGroup = !bReview && !$('input[name=group\\.name]', parentForm).val();

    //table data
    function fnGetAssignedData() {
        return assignedTable.dataTable().api().rows().nodes();
    }

    //get post data of table changes since last post
    function getPostEntries() {
        //initialize post data
        var aoPostEntries = [];

        function push(name, val) {
            aoPostEntries.push({
                'name' : name,
                'value' : val
            });
        }

        //extract post entries from the assigned table records
        var aoAssignedData = fnGetAssignedData();

        function groupMemberActivation(_index, element) {
            //add entry in the form "<GROUPID>:<USERID>"
            push($(element).is('.inactive') ? 'deactivateGroups' : 'activateGroups',
                 $('input[name=id]', row).val() + ':' + $('input[name=memberid]', element).val());
        }

        var i, row, bPerson;
        for (i = 0; i < aoAssignedData.length; i++) {
            //data map for this record
            row = aoAssignedData[i]; //the markup

            //is this a person record?
            bPerson = $('input[name=type]', row).is('[value=person]');

            /*
             * Proceed if editing membership is allowed
             */
            if (bEditMembership) {
                //add/remove the user/group
                push('add' + (bPerson ? 'People' : 'Groups'),
                     $('input[name=id]', row).val());
            }

            /*
             * Proceed if the following are true:
             *  - editing the individual members is allowed
             *  - this record is a group
             */
            if (bEditGroups && !bPerson) {
                //examine each member of this group
                var members = $('li', row);

                members.each(groupMemberActivation);
            }
        }

        var availableData = $('#available').dataTable().api()
                                           .rows(':has(.removePerson), :has(.removeGroup)')
                                           .nodes();
        for (i = 0; i < availableData.length; i++) {
            row = availableData[i];

            bPerson = $('input[name=type]', row).is('[value=person]');

            if (bEditMembership) {
                //add/remove the user/group
                push('remove' + (bPerson ? 'People' : 'Groups'),
                     $('input[name=id]', row).val());
            }
        }

        return aoPostEntries;
    }

    //generate and append to the parent form inputs from post data on submit
    parentForm.submit(function() {
        var aoPostData = getPostEntries();

        for (var i = 0; i < aoPostData.length; i++) {
            parentForm.append($('<input/>').attr('type', 'hidden').attr('name', aoPostData[i].name).val(aoPostData[i].value));
        }
    });



    //initialize tables
    availableTable = initAssignmentTable(availableTable);
    assignedTable = initAssignmentTable(assignedTable);

    /*
     * MIV-4176; function to notify user that altering this
     * assignment will not affect its counter part in 'Manage
     * groups' or 'Assign dossier reviewers,' respectively.
     */
    function fnNotifyUser(event) {
        /*
         * Continue if the user has yet to be notified and this is
         * not a new group (that is, this is a group edit or a
         * dossier review assignment).
         */
        if (!availableTable.data('bNotified') && !bNewGroup) {
            //the notification message is based on the type of assignment
            var sMessage =  bReview ?
                            'Altering the review assignment for this dossier will not affect the original group.' :
                            'Altering the membership of this group will not affect any ' +
                            'dossier reviews to which this group has already been assigned.';

            $('<div/>').attr('title', 'Please note').text(sMessage).dialog({
                modal         : true,
                closeOnEscape : false,
                buttons       : [{
                                    text  : 'OK',
                                    click : function() {
                                                //user has been notified
                                                availableTable.data('bNotified', true);

                                                //close the dialog
                                                $(this).dialog('close');
                                            }
                                }]
            });
        }
    }

    /*
     * Return assignment data table for the given table.
     */
    function initAssignmentTable(oTable) {
        /* detaching before dom manipulation improves performance */
        var parent = oTable.parent();
        var parentParent = parent.parent();
        parent.detach();

        //is the initiating table the assigned table? or the available?
        var bAssignedTable = oTable.is(assignedTable);
        var bAvailableTable = oTable.is(availableTable);

        //initialize table with custom settings
        return fnInitDataTable(oTable, {
            'fnInfoCallback' : function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                                   return bAvailableTable ?
                                          Math.min(iStart, iTotal) + ' to ' + iEnd + ' of ' + iTotal + ' entries' :
                                          sPre;
                               },
            'bProcessing'    : true,
            'oLanguage'      : {
                                   'sZeroRecords'    :   bAssignedTable ?
                                                         'There are no users or groups assigned' :
                                                         'No users or groups were found',
                                   'sLoadingRecords' : 'Loading...',

                                   'sSearch'         :   bAvailableTable ?
                                                         '<span>Search</span>&nbsp;' +
                                                         '<span class="inputtext" title="Enter text to search">_INPUT_</span>' +
                                                         '<input class="reset-button" title="Reset the Search" type="image" src="/miv/images/buttons/ui-clear-button.png">' :
                                                         sSearch
                                 },
            'iDisplayLength' : 10,
            'bAutoWidth'     : false,
            'aoColumns'      : [
                                /* Name Header */   null,
                                /* Sort Name */     { 'bSearchable': true,
                                                      'bVisible':    false },
                                /* Surname */       { 'bSearchable': true,
                                                      'bVisible': false},
                                /* School */        { 'bSearchable': true,
                                                      'bVisible':    false },
                                /* Department */    { 'bSearchable': true,
                                                      'bVisible':    false },
                                /* Scope String */  { 'bSearchable': true,
                                                      'bVisible':    false},
                                /* person/group */  { 'bSearchable' : true,
                                                      'bVisible' : false}
                               ],
            'fnDrawCallback' : drawCallback,
            'fnRowCallback'  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var self = this; //jshint ignore:line

                //add tab index to make focusable
                $('td.selectable', nRow).attr('tabindex', 100 + iDisplayIndex);

                return nRow;
            },
            'fnInitComplete' : function() {
                parentParent.append(parent);

                /* replace the type of the automatically generated input field
                 * so that styles are correctly applied */
                $('.inputtext input[type=search]').attr('type', 'text');

                function clearAllFilters() {
                    var api = oTable.dataTable().api();

                    //clear other filters in case there is anything in the history state
                    for (var i = 0; i <= api.columns()[0].length; i++) {
                        api.column(i).search('');
                    }

                    //initialize filter to group, to emulate old behavior.
                    api.search(bAvailableTable ? 'group' : '').draw();

                    if (bAvailableTable) {
                        $('#available_wrapper input[type=text]').val('');
                    }

                    return api;
                }

                if (bAvailableTable) {
                    clearAllFilters();
                }

                $('#available_wrapper, #assigned_wrapper').on('click', '.reset-button', {}, function(event) {
                    event.preventDefault();
                    clearAllFilters();
                });
            }
        });
    }
};
