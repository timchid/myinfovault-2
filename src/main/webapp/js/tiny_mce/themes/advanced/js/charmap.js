tinyMCEPopup.requireLangPack();

var charmap = [
   	['&iexcl;',   '&#161;',  true, 'Inverted Exclamation mark'],
   	['&iquest;',  '&#191;',  true, 'Inverted Question mark'],
   	['&ldquo;',   '&#8220;', true, 'Left Double-Quote'],
   	['&rdquo;',   '&#8221;', true, 'Right Double-Quote'],
   	['&circ;',    '&#710;',  true, 'Circumflex Accent'],
   	['&laquo;',   '&#171;',  true, 'Left Guillemet'],
   	['&raquo;',   '&#187;',  true, 'Right Guillemet'],
   	['&brvbar;',  '&#166;',  true, 'Broken bar'],
   	['&sect;',    '&#167;',  true, 'Section sign'],
   	['&para;',    '&#182;',  true, 'Paragraph sign/Pilcrow'],
   	['&middot;',  '&#183;',  true, 'Middle dot'],
   	['&bull;',    '&#8226;', true, 'Bullet'],
   	['&dagger;',  '&#8224;', true, 'Dagger'],
   	['&copy;',    '&#169;',  true, 'Copyright sign'],
   	['&reg;',     '&#174;',  true, 'Reserved/Registered sign'],
   	['&trade;',   '&#8482;', true, 'Trademark sign'],
   	['&cent;',    '&#162;',  true, 'Cent sign'],
   	['&pound;',   '&#163;',  true, 'Pound sign'],
   	['&euro;',    '&#8364;', true, 'Euro sign'],
   	['&yen;',     '&#165;',  true, 'Yen/Yuan sign'],
   	['&plusmn;',  '&#177;',  true, 'Plus-Minus sign'],
   	['&times;',   '&#215;',  true, 'Multiplication sign'],
   	['&divide;',  '&#247;',  true, 'Division sign'],
   	['&not;',     '&#172;',  true, 'Logical NOT'],
   	['&micro;',   '&#181;',  true, 'Micro sign'],
   	['&deg;',     '&#176;',  true, 'Degree sign'],
   	['&sup1;',    '&#185;',  true, 'Superscript 1'],
   	['&sup2;',    '&#178;',  true, 'Superscript 2'],
   	['&sup3;',    '&#179;',  true, 'Superscript 3'],
   	['&frac14;',  '&#188;',  true, 'One-quarter fraction'],
   	['&frac12;',  '&#189;',  true, 'One-half fraction'],
   	['&frac34;',  '&#190;',  true, 'Three-quarters fraction'],
   	['&fnof;',    '&#402;',  true, 'Latin small f with hook'],
   	['&Agrave;',  '&#192;',  true, 'Capital A with grave accent'],
   	['&Aacute;',  '&#193;',  true, 'Capital A with acute accent'],
   	['&Acirc;',   '&#194;',  true, 'Capital A with circumflex'],
   	['&Atilde;',  '&#195;',  true, 'Capital A with tilde'],
   	['&Auml;',    '&#196;',  true, 'Capital A with diaeresis'],
   	['&Aring;',   '&#197;',  true, 'Capital A with ring above'],
   	['&AElig;',   '&#198;',  true, 'Capital AE ligature'],
   	['&Ccedil;',  '&#199;',  true, 'Capital C with cedilla'],
   	['&Egrave;',  '&#200;',  true, 'Capital E with grave accent'],
   	['&Eacute;',  '&#201;',  true, 'Capital E with acute accent'],
   	['&Ecirc;',   '&#202;',  true, 'Capital E with circumflex'],
   	['&Euml;',    '&#203;',  true, 'Capital E with diaeresis'],
   	['&Igrave;',  '&#204;',  true, 'Capital I with grave accent'],
   	['&Iacute;',  '&#205;',  true, 'Capital I with acute accent'],
   	['&Icirc;',   '&#206;',  true, 'Capital I with circumflex'],
   	['&Iuml;',    '&#207;',  true, 'Capital I with diaeresis'],
   	['&ETH;',     '&#208;',  true, 'Capital letter &quot;Eth&quot;'],
   	['&Ntilde;',  '&#209;',  true, 'Capital N with tilde'],
   	['&Ograve;',  '&#210;',  true, 'Capital O with grave accent'],
   	['&Oacute;',  '&#211;',  true, 'Capital O with acute accent'],
   	['&Ocirc;',   '&#212;',  true, 'Capital O with circumflex'],
   	['&Otilde;',  '&#213;',  true, 'Capital O with tilde'],
   	['&Ouml;',    '&#214;',  true, 'Capital O with diaeresis'],
   	['&Oslash;',  '&#216;',  true, 'Capital O with stroke'],
   	['&Ugrave;',  '&#217;',  true, 'Capital U with grave accent'],
   	['&Uacute;',  '&#218;',  true, 'Capital U with acute accent'],
   	['&Ucirc;',   '&#219;',  true, 'Capital U with circumflex'],
   	['&Uuml;',    '&#220;',  true, 'Capital U with diaeresis'],
   	['&Yacute;',  '&#221;',  true, 'Capital Y with acute accent'],
   	['&Yuml;',    '&#376;',  true, 'Capital Y with diaeresis'],
   	['&THORN;',   '&#222;',  true, 'Capital letter &quot;Thorn&quot;'],
   	['&agrave;',  '&#224;',  true, 'a with grave accent'],
   	['&aacute;',  '&#225;',  true, 'a with acute accent'],
   	['&acirc;',   '&#226;',  true, 'a with circumflex'],
   	['&atilde;',  '&#227;',  true, 'a with tilde'],
   	['&auml;',    '&#228;',  true, 'a with diaeresis'],
   	['&aring;',   '&#229;',  true, 'a with ring above'],
   	['&aelig;',   '&#230;',  true, 'ae ligature'],
   	['&ccedil;',  '&#231;',  true, 'c with cedilla'],
   	['&egrave;',  '&#232;',  true, 'e with grave accent'],
   	['&eacute;',  '&#233;',  true, 'e with acute accent'],
   	['&ecirc;',   '&#234;',  true, 'e with circumflex'],
   	['&euml;',    '&#235;',  true, 'e with diaeresis'],
   	['&igrave;',  '&#236;',  true, 'i with grave accent'],
   	['&iacute;',  '&#237;',  true, 'i with acute accent'],
   	['&icirc;',   '&#238;',  true, 'i with circumflex'],
   	['&iuml;',    '&#239;',  true, 'i with diaeresis'],
   	['&eth;',     '&#240;',  true, 'small letter &quot;Eth&quot;'],
   	['&ntilde;',  '&#241;',  true, 'n with tilde'],
   	['&ograve;',  '&#242;',  true, 'o with grave accent'],
   	['&oacute;',  '&#243;',  true, 'o with acute accent'],
   	['&ocirc;',   '&#244;',  true, 'o with circumflex'],
   	['&otilde;',  '&#245;',  true, 'o with tilde'],
   	['&ouml;',    '&#246;',  true, 'o with diaeresis'],
   	['&oslash;',  '&#248;',  true, 'o with stroke'],
   	['&ugrave;',  '&#249;',  true, 'u with grave accent'],
   	['&uacute;',  '&#250;',  true, 'u with acute accent'],
   	['&ucirc;',   '&#251;',  true, 'u with circumflex'],
   	['&uuml;',    '&#252;',  true, 'u with diaeresis'],
   	['&yacute;',  '&#253;',  true, 'y with acute accent'],
   	['&yuml;',    '&#255;',  true, 'y with diaeresis'],
   	['&thorn;',   '&#254;',  true, 'small letter &quot;thorn&quot;'],
   	['&szlig;',   '&#223;',  true, 'German &quot;es-zett&quot; or &quot;scharfes s&quot;'],
   	['&Alpha;',   '&#913;',  true, 'Greek capital Alpha'],
   	['&Beta;',    '&#914;',  true, 'Greek capital Beta'],
   	['&Gamma;',   '&#915;',  true, 'Greek capital Gamma'],
   	['&Delta;',   '&#916;',  true, 'Greek capital Delta'],
   	['&Epsilon;', '&#917;',  true, 'Epsilon'],
   	['&Zeta;',    '&#918;',  true, 'Greek capital Zeta'],
   	['&Eta;',     '&#919;',  true, 'Greek capital Eta'],
   	['&Theta;',   '&#920;',  true, 'Greek capital Theta'],
   	['&Iota;',    '&#921;',  true, 'Greek capital Iota'],
   	['&Kappa;',   '&#922;',  true, 'Greek capital Kappa'],
   	['&Lambda;',  '&#923;',  true, 'Greek capital Lambda'],
   	['&Mu;',      '&#924;',  true, 'Greek capital Mu'],
   	['&Nu;',      '&#925;',  true, 'Greek capital Nu'],
   	['&Xi;',      '&#926;',  true, 'Greek capital Xi'],
   	['&Omicron;', '&#927;',  true, 'Greek capital Omicron'],
   	['&Pi;',      '&#928;',  true, 'Greek capital Pi'],
   	['&Rho;',     '&#929;',  true, 'Greek capital Rho'],
   	['&Sigma;',   '&#931;',  true, 'Greek capital Sigma'],
   	['&Tau;',     '&#932;',  true, 'Greek capital Tau'],
   	['&Upsilon;', '&#933;',  true, 'Greek capital Upsilon'],
   	['&Phi;',     '&#934;',  true, 'Greek capital Phi'],
   	['&Chi;',     '&#935;',  true, 'Greek capital Chi'],
   	['&Psi;',     '&#936;',  true, 'Greek capital Psi'],
   	['&Omega;',   '&#937;',  true, 'Greek capital Omega'],
   	['&alpha;',   '&#945;',  true, 'Greek small alpha'],
   	['&beta;',    '&#946;',  true, 'Greek small beta'],
   	['&gamma;',   '&#947;',  true, 'Greek small gamma'],
   	['&delta;',   '&#948;',  true, 'Greek small delta'],
   	['&epsilon;', '&#949;',  true, 'Greek small epsilon'],
   	['&zeta;',    '&#950;',  true, 'Greek small zeta'],
   	['&eta;',     '&#951;',  true, 'Greek small eta'],
   	['&theta;',   '&#952;',  true, 'Greek small theta'],
   	['&iota;',    '&#953;',  true, 'Greek small iota'],
   	['&kappa;',   '&#954;',  true, 'Greek small kappa'],
   	['&lambda;',  '&#955;',  true, 'Greek small lambda'],
   	['&mu;',      '&#956;',  true, 'Greek small mu'],
   	['&nu;',      '&#957;',  true, 'Greek small nu'],
   	['&xi;',      '&#958;',  true, 'Greek small xi'],
   	['&omicron;', '&#959;',  true, 'Greek small omicron'],
   	['&pi;',      '&#960;',  true, 'Greek small pi'],
   	['&rho;',     '&#961;',  true, 'Greek small rho'],
   	['&sigma;',   '&#963;',  true, 'Greek small sigma'],
   	['&tau;',     '&#964;',  true, 'Greek small tau'],
   	['&upsilon;', '&#965;',  true, 'Greek small upsilon'],
   	['&phi;',     '&#966;',  true, 'Greek small phi'],
   	['&chi;',     '&#967;',  true, 'Greek small chi'],
   	['&psi;',     '&#968;',  true, 'Greek small psi'],
   	['&omega;',   '&#969;',  true, 'Greek small omega']
   ];

tinyMCEPopup.onInit.add(function() {
	tinyMCEPopup.dom.setHTML('charmapView', renderCharMapHTML());
	addKeyboardNavigation();
});

function addKeyboardNavigation(){
	var tableElm, cells, settings;

	cells = tinyMCEPopup.dom.select("a.charmaplink", "charmapgroup");

	settings ={
		root: "charmapgroup",
		items: cells
	};
	/*cells[0].tabindex=0;
	tinyMCEPopup.dom.addClass(cells[0], "mceFocus");
	if (tinymce.isGecko) {
		cells[0].focus();		
	} else {
		setTimeout(function(){
			cells[0].focus();
		}, 100);
	}*/
	tinyMCEPopup.editor.windowManager.createInstance('tinymce.ui.KeyboardNavigation', settings, tinyMCEPopup.dom);
}

function renderCharMapHTML() {
	var charsPerRow = 12, tdWidth=20, tdHeight=20, i;
	var html = '<table border="0" cellspacing="0" cellpadding="5" width="' + (tdWidth*charsPerRow) + '"><tr height="' + tdHeight + '">';
	var cols=-1;

	for (i=0; i<charmap.length; i++) {
		var insertCharFn;

		if (charmap[i][2]==true) {
			cols++;
			insertCharFn = 'insertChar(\'' + charmap[i][1].substring(2,charmap[i][1].length-1) + '\');';
			html += ''
				+ '<td class="charmap" onclick="'+insertCharFn+'">'
				+ '<a class="charmaplink" role="button" href="javascript:void(0)" onclick="return false;" onmousedown="return false;" title="' + charmap[i][3] +'">'
				+ charmap[i][1]
				+ '</a></td>';
			if ((cols+1) % charsPerRow == 0)
				html += '</tr><tr height="' + tdHeight + '">';
		}
	 }

	if (cols % charsPerRow > 0) {
		var padd = charsPerRow - (cols % charsPerRow);
		for (var i=0; i<padd-1; i++)
			html += '<td width="' + tdWidth + '" height="' + tdHeight + '" class="charmap">&nbsp;</td>';
	}

	html += '</tr></table></div>';
	html = html.replace(/<tr height="20"><\/tr>/g, '');

	return html;
}

function insertChar(chr) {
	tinyMCEPopup.execCommand('mceInsertContent', false, '&#' + chr + ';');
	tinyMCEPopup.editor.focus();
	
	// Other then IE no need to close window
	if (!tinyMCEPopup.isWindow && !tinymce.isIE)
	{
		return true;
	}

	//window.focus();
	tinyMCEPopup.close();
}

function tagContent(tag) {
	var subst = '<' + tag + '>{$selection}</' + tag + '>';
	tinyMCE.execCommand('mceReplaceContent',false,subst);

	// Refocus in window
	if (tinyMCEPopup.isWindow || tinymce.isIE)
	{
		window.focus();
	}
}

/*function previewChar(codeA, codeB, codeN) {
	var elmA = document.getElementById('codeA');
	var elmB = document.getElementById('codeB');
	var elmV = document.getElementById('codeV');
	var elmN = document.getElementById('codeN');

	if (codeA=='#160;') {
		elmV.innerHTML = '__';
	} else {
		elmV.innerHTML = '&' + codeA;
	}

	elmB.innerHTML = '&amp;' + codeA;
	elmA.innerHTML = '&amp;' + codeB;
	elmN.innerHTML = codeN;
}*/
