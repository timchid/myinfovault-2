'use strict';
/* global $CompletedMessage, showMessage, removeErrorClasses, validateForm, mivFormProfile,
   trim, dojo, getElementById, initCharacterPalette, focusToFirstErrorField, removeLayoutPanels */
/* exported removeFLD, checkUncheckAllBold, checkUncheckAllItalic, checkUncheckAllUnderline,
   checkIfAllItalic, checkIfAllUnderline, checkIfAllBold */
var regexBoldCheck = new RegExp(/df[0-9]+[:]bold/);
var regexItalicCheck = new RegExp(/df[0-9]+[:]italic/);
var regexUnderlineCheck = new RegExp(/df[0-9]+[:]underline/);

var formatPatterns = [];
var formatPatternsRowId = [];

$(document).ready(function() {

    if ($('#successbox, #infobox, #deletebox', $CompletedMessage).text().length > 0)
    {
        showMessage('CompletedMessage',null);
    }

    var $form = $('form#formatData');

    $form.submit(function(event) {
        var $errormessage = $('#errormessage');
        var formProfile = setupMivFormProfile();

        if (typeof formProfile !== 'undefined')
        {
            initFormatPatternsList();
            removeErrorClasses('errorfield');
            if (!validateForm($form, formProfile, $errormessage, 'dummy-errorclass')) {
                event.preventDefault();
            }
        }
    });

});

function validateFormatPattern(content, elemid)
{
    var id = elemid.replace('pf', '').replace(':pattern', '');
    var fieldsList = document.getElementById('pf'+id+':').value;
    var pattern = document.getElementById('pf'+id+':pattern').value;
    var patternBold = document.getElementById('pf'+id+':bold').checked;
    var patternItalic = document.getElementById('pf'+id+':italic').checked;
    var patternUnderline = document.getElementById('pf'+id+':underline').checked;
    //alert(fieldsList+"~"+pattern+"~"+patternBold+"~"+patternItalic+"~"+patternUnderline);

    if (fieldsList !== '-- Select A Field --' || trim(pattern) !== '' || (patternBold || patternItalic || patternUnderline))
    {
        var hasError = false;

        if (fieldsList === '-- Select A Field --') {
            $(getElementById('pf'+id+':')).addClass('errorfield');
            hasError = true;
        }

        if (trim(pattern) === '')
        {
            $(getElementById('pf'+id+':pattern')).addClass('errorfield');
            hasError = true;
        }

        if (!(patternBold || patternItalic || patternUnderline))
        {
            $(getElementById('pf'+id+':style')).addClass('errorfield');
            hasError = true;
        }

        if (hasError) { return 'INVALID_PATTERN_FORMAT'; }

        var values = fieldsList+'<val/>'+trim(pattern)+'<val/>'+patternBold+'<val/>'+patternItalic+'<val/>'+patternUnderline;

        var index = dojo.indexOf(formatPatterns, values);
        if ( formatPatterns.length === 0 || index === -1)
        {
            formatPatterns.push(values);
            formatPatternsRowId.push(id);
        }
        else
        {
            var rowDiv = $(getElementById('pf'+formatPatternsRowId[index]+':rowid'));

            $(getElementById('pf'+id)).addClass('errorfield');
            return 'CUSTOM_ERROR_MSG:Duplicate format pattern as row- ' + rowDiv.val();
        }
    }
    return true;
}

function setupMivFormProfile()
{
//    var formValidation = {};  // this empty object is guaranteed to be immediately replaced by mivFormProfile or empty constraints. Don't initialize it.
//
//    if (typeof mivFormProfile == 'undefined') {
//        formValidation = {constraints:{}};
//    }
//    else {
//        formValidation = mivFormProfile;
//    }
    // set the form validation object to the mivFormProfile if there is one, or to an empty constraints object if not.
    var formValidation = mivFormProfile || { constraints: {} };

    var patternObjs = dojo.query('input[id$=:pattern]');

    for (var i=0; i < patternObjs.length; i++) {
        if (dojo.indexOf(formValidation.constraints, patternObjs[i].id) === -1) {
            dojo.query('label[for=' + patternObjs[i].id + ']')[0].innerHTML = 'Format patterns row- ' + (i+1);
                formValidation.constraints[patternObjs[i].id] = validateFormatPattern;
        }
    }
    initFormatPatternsList();
    return formValidation;
}

function reSequenceRows()
{
    var divObjs = dojo.query('div[class=srno]');
    var index;
    for (var i=0; i < divObjs.length; i++) {
        index = divObjs[i].id.replace('pf', '').replace(':srno', '');
        document.getElementById(divObjs[i].id).innerHTML = '<input name="pf'+index+':rowid" id="pf'+index+':rowid" value="'+(i+1)+'" type="hidden"> '+(i+1)+'.';
    }
}

function initFormatPatternsList()
{
    formatPatterns = [];
    formatPatternsRowId = [];
}

//Add a field to the format dialog
function addField(fieldTypeCounter, div, pattern, patternCounter)
{
    var maxPatterns = (document.formatData.maxpatterns.value * 1);
    var patternCount = (document.getElementById(patternCounter).value * 1) + 1;
    var patternCounterObj = document.getElementById(patternCounter);

    // Only add if we have less than maxPatterns patterns
    if (patternCount > maxPatterns)
    {
        showMessage('You have entered the maximum number of pattern format options: '+(patternCount-1));
        return;
    }

    // Add the new pattern
    patternCounterObj.value = patternCount;

    var divId = document.getElementById(div);
    var fieldCounter = document.getElementById(fieldTypeCounter);
    var fieldnum = (document.getElementById(fieldTypeCounter).value * 1) + 1;

    var fields = document.formatData.formatFieldSelection.value;
    var fieldArr = fields.split(',');
    var fieldname = div+fieldnum;

    var boldToolTip = document.formatData.boldToolTip.value;
    var boldLabel = document.formatData.boldLabel.value;
    var italicToolTip = document.formatData.italicToolTip.value;
    var italicLabel = document.formatData.italicLabel.value;
    var underlineToolTip = document.formatData.underlineToolTip.value;
    var underlineLabel = document.formatData.underlineLabel.value;
    var patternToolTip = document.formatData.patternToolTip.value;
    var fieldSelectToolTip = document.formatData.fieldSelectToolTip.value;
    var fieldDeleteToolTip = document.formatData.fieldDeleteToolTip.value;
    var fieldDeleteLabel = document.formatData.fieldDeleteLabel.value;


    // Update counter for the next field
    fieldCounter.value = fieldnum;

    // Elements to be created for the new field
    var newdiv;
    var formatRowNumber;
    var formatLine;
    var formatField;
    var formatPattern;
    var formatStyle;
    var formatStyleDiv;
    var formatBold;
    var formatItalic;
    var inputElement;
    var formatUnderline;
    var formatRemoveLink;
    var hrefElement;
    var adddiv;
    //var paragraph;


    newdiv = document.createElement('div');
    newdiv.setAttribute('id', fieldname);

    formatLine = document.createElement('span');
    formatLine.setAttribute('class', 'formatline');

    formatRowNumber = document.createElement('div');
    formatRowNumber.setAttribute('id', fieldname+':srno');
    formatRowNumber.setAttribute('name', fieldname+':srno');
    formatRowNumber.setAttribute('class', 'srno');
    formatRowNumber.innerHTML = fieldnum;

    formatField = document.createElement('div');
    formatField.setAttribute('name', 'formatfield');
    formatField.setAttribute('class', 'patternfield');
    formatField.setAttribute('title', fieldSelectToolTip);

    // Get all of the selection options
    var options='';
    for (var i=0; i < fieldArr.length; i++) {
        options = options+'<option value="'+fieldArr[i]+'">'+fieldArr[i]+'</option>';
    }

    formatField.innerHTML = '<select name="'+fieldname+':" id="'+fieldname+':">'+options+'</select>&nbsp;';

    // Add input for the pattern field if included
    if (pattern !== null && pattern !== undefined)
    {
        formatPattern = document.createElement('div');
        formatPattern.setAttribute('class', 'patternvalue');
        formatPattern.setAttribute('title', patternToolTip);
        // Create an input element to allow handlers to be added for special charater palette
        inputElement = document.createElement('input');
        formatPattern.innerHTML = '<input name="'+fieldname+':pattern" id="'+fieldname+':pattern" maxlength="255" type="text">  ';
    }

    formatPattern.innerHTML += '<label class="hidden" for="'+fieldname+':pattern"></label>';

    // Bold, Italic and Underline Styles
    formatStyle = document.createElement('div');
    formatStyle.setAttribute('class', 'patternstyle');

    formatStyleDiv = document.createElement('div');
    formatStyleDiv.setAttribute('id', fieldname+':style');
    formatStyleDiv.setAttribute('name', fieldname+':style');
    formatStyleDiv.setAttribute('class', 'styleblock');

    // Bold
    formatBold = document.createElement('span');
    formatBold.setAttribute('class', 'formatbold');
    formatBold.setAttribute('title', boldToolTip);

    formatBold.innerHTML = '<input name="'+fieldname+':bold" id="'+fieldname+':bold" type="checkbox">'+' <label for="'+fieldname+':bold">' + boldLabel + '</label> ';

    // Italic
    formatItalic = document.createElement('span');
    formatItalic.setAttribute('class', 'formatitalic');
    formatItalic.setAttribute('title', italicToolTip);

    formatItalic.innerHTML = '<input name="'+fieldname+':italic" id="'+fieldname+':italic" type="checkbox">'+' <label for="'+fieldname+':italic">' + italicLabel + '</label> ';

    // Underline
    formatUnderline = document.createElement('span');
    formatUnderline.setAttribute('class', 'formatunderline');
    formatUnderline.setAttribute('title', underlineToolTip);

    formatUnderline.innerHTML = '<input name="'+fieldname+':underline" id="'+fieldname+':underline" type="checkbox">'+' <label for="'+fieldname+':underline">' + underlineLabel + '</label> ';

    // Remove field link
    formatRemoveLink = document.createElement('div');
    formatRemoveLink.setAttribute('class', 'buttonset');

    //hrefElement = document.createElement('a');
    hrefElement = document.createElement('input');
    hrefElement.setAttribute('onclick', 'removeFLD("'+fieldname+'","'+div+'")');
    hrefElement.setAttribute('size', '20');
    hrefElement.setAttribute('id', 'D'+fieldname.replace('pf', ''));
    hrefElement.setAttribute('name', 'D'+fieldname.replace('pf', ''));
    hrefElement.setAttribute('type', 'button');
    hrefElement.setAttribute('class', 'mivbutton');
    hrefElement.setAttribute('title', fieldDeleteToolTip);
    hrefElement.setAttribute('value', fieldDeleteLabel);

    //hrefElement.appendChild(document.createTextNode(' '+fieldDeleteLabel));

    formatRemoveLink.appendChild(hrefElement);

    // Add all of format elements to the style element
    formatStyleDiv.appendChild(formatBold);
    formatStyleDiv.appendChild(document.createTextNode(' '));
    formatStyleDiv.appendChild(formatItalic);
    formatStyleDiv.appendChild(document.createTextNode(' '));
    formatStyleDiv.appendChild(formatUnderline);

    formatStyle.appendChild(formatStyleDiv);
    formatStyle.appendChild(document.createTextNode(' '));
    formatStyle.appendChild(formatRemoveLink);

    // Add the Field, Pattern and style to the line
    formatLine.appendChild(formatRowNumber);
    formatLine.appendChild(formatField);
    formatLine.appendChild(formatPattern);
    formatLine.appendChild(formatStyle);

    // Add the line to the new div
    newdiv.appendChild(formatLine);

    // onclick event does not fire on IE when removing the newly added div unless we create another
    // new div and then assign to it the innerHTML of our new div.
    adddiv = document.createElement('div');
    adddiv.setAttribute('id', fieldname);
    adddiv.setAttribute('class', 'formatpatterns');
    adddiv.innerHTML = newdiv.innerHTML;

    // the paragraph is for proper spacing from the previous div
    //paragraph = document.createElement('p');
    // divId.insertBefore(paragraph, divId.firstChild);
    //divId.insertBefore(adddiv, divId.firstChild);
    divId.appendChild(adddiv); // add the node at the bottom of div

    //Initialize the special character palette
    initCharacterPalette();
    initFormatPatternsList();
    reSequenceRows();
}

//remove a field to the publications format dialog
function removeFLD(fieldname, div)
{
    // Decrement the patternCounter
    var patternCount = (document.getElementById('patternCount').value * 1) - 1;

    if (patternCount < 0)
    {
        patternCount = 0;
    }

    document.getElementById('patternCount').value = patternCount;

    var d = document.getElementById(div);
    var child = document.getElementById(fieldname);
    d.removeChild(child);

    removeLayoutPanels('#errormessage');
    focusToFirstErrorField();
    reSequenceRows();
    initFormatPatternsList();

    // Must have at least one
    if (patternCount === 0) {
        addField('pfCount', 'pf', 'pattern', 'patternCount');
    }
}

function checkUncheckAll(regex, checkAll)
{
    // get the form elements
    var frm = document.formatData;
    var el = frm.elements;
    // loop through the elements...
    for (var i=0; i < el.length; i++)
    {
        // and check if it is a checkbox matching the regex
        if (el[i].type === 'checkbox' && el[i].name.match(regex))
        {
            // Check or uncheck all
            el[i].checked = checkAll;
        }
    }
}

//Select/Unselect all bold checkboxes
function checkUncheckAllBold()
{
    checkUncheckAll(regexBoldCheck, document.formatData.checkuncheckallbold.checked);
}

//Select/Unselect all italic checkboxes
function checkUncheckAllItalic () {
    checkUncheckAll(regexItalicCheck, document.formatData.checkuncheckallitalic.checked);
}

//Select/Unselect all underline checkboxes
function checkUncheckAllUnderline () {
    checkUncheckAll(regexUnderlineCheck, document.formatData.checkuncheckallunderline.checked);
}

//Check if all italic boxes are checked
// If they are Check the 'select all' box
function checkIfAllItalic() {
    document.formatData.checkuncheckallitalic.checked = checkIfAllChecked(regexItalicCheck);
}

//Check if all underline boxes are checked
// If they are Check the 'select all' box
function checkIfAllUnderline() {
    document.formatData.checkuncheckallunderline.checked = checkIfAllChecked(regexUnderlineCheck);
}

//Check if all bold boxes are checked
// If they are Check the 'select all' box
function checkIfAllBold() {
    document.formatData.checkuncheckallbold.checked = checkIfAllChecked(regexBoldCheck);
}

//Check if all boxes are checked
// return true if yes, false otherwise
function checkIfAllChecked(regex)
{
    var frm = document.formatData;
    // default to checked
    var checkBox = true;
    // loop through the elements...
    var el = frm.elements;
    for (var i=0; i < el.length; i++)
    {
        // and check if it is a checkbox matching the regex
        if (el[i].type === 'checkbox' && el[i].name.match(regex))
        {
            // If any one is not checked, unselect
            if (!el[i].checked) {
                checkBox = false;
            }
        }
    }
    return(checkBox);
}

/*<span class="vim">
 vim:ts=8 sw=4 et sm:
</span>*/
