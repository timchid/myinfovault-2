'use strict';
/* global getClickedButton, validateForm */
/*
 * Script for executive.jsp.
 */

var mivFormProfile = {
    required : [ 'viceProvostID', 'provostID', 'chancellorID' ]
};

$(document).ready(function() {
    /*
     * Chancellor, Provost, and Vice Provost user suggestion
     */
    $('.suggestion').suggestion({
        suggestionSource : 'person/candidate'
    });

    var form = $('form');

    // form validation
    form.submit(function(event) {
        /*
         * "formnovalidate" attribute on selected submit button, do not validate.
         */
        if (getClickedButton(form).attr('formnovalidate') !== undefined) { return; }

        if (!validateForm(form, mivFormProfile, $('#messagebox'))) {
            event.preventDefault();
        }
    });
});
