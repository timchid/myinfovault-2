'use strict';

$(document).ready(function()
{
	var miv = miv || {};
	miv.defaultMessage = 'Add your own information here...';

	miv.msgArea = $('#message');
	miv.msgArea.val(miv.defaultMessage);
	miv.msgArea.focus();
	miv.msgArea.select();

	// Clear the default message when you enter the field.
	miv.msgArea.bind('mouseover click focus', function(e) {
		if (miv.msgArea.val() === miv.defaultMessage) {
			miv.msgArea.val('');
		}
	});

	// Restore the prompt text if the field is blank when exiting it.
	miv.msgArea.blur(function(e) {
		if (miv.msgArea.val().length === 0) {
			miv.msgArea.val(miv.defaultMessage);
		}
	});

	// Restore the prompt when the mouse leaves, unless the field has the focus
	miv.msgArea.mouseout(function(e) {
		if (document.activeElement !== this && (miv.msgArea.val().length === 0)) {
			miv.msgArea.val(miv.defaultMessage);
		}
	});

	// Clear the default message when the form is submitted.
	$('#msgform').submit(function(e) {
		if (miv.msgArea.val() === miv.defaultMessage) {
			miv.msgArea.val('');
		}
	});
});
