
$(document).ready(function() {
	$("#associate_fieldset legend").click(function() {
		if ($("#associate_fieldset").hasClass('collapsed'))
		{
			$("#associate_fieldset").removeClass('collapsed');
			$("#association").removeClass('hidden');
		}
		else
		{
			$("#associate_fieldset").addClass('collapsed');
			$("#association").addClass('hidden');
		}
	});

	$('#mytestform').submit(function() {
		var $selectedids = $('#associationframe').contents().find('#selectedids');
		var ids = $selectedids.val();

		if ($('#selectedids') != undefined && $('#selectedids').val() != undefined) {
		    $('#selectedids').val(ids);
		}
		else {
			var $selectedidsObj = $('<input>').attr('type', 'hidden').attr('name', 'selectedids').attr('id', 'selectedids').val(ids);
			$('#mytestform').append($selectedidsObj);
		}
	});

	$("#associate_fieldset legend").click();
});

function removeOption(selectDom, removeVal)
{
	$(selectDom).children("option").each(function () {
		if ( removeVal == $(this).val())
		{
			$(this).remove();
		}
	});
}


// to check the id belongs to the selectedids
function findSelectedId(id, selectedids)
{
	var isfind = false;
	//var $selectedids = $('#selectedids');
	$.each(selectedids.split(", "),function(index) {
		if ( id == this)
		{
			isfind = true;
		}
	});
	return isfind;
}

//to remove the list of options
function clearoptions(opt)
{
	if (opt != undefined)
	{
		opt.empty();
	}
}

// to match the targatStr into Association value
function matchAssociation(sourceStr, targatStr )
{
	//return sourceStr.replace(/(&nbsp;)+/ig, " ").toLowerCase().indexOf(targatStr.toLowerCase()) != -1 ? true : false;
	return htmlDecode(sourceStr).toLowerCase().indexOf(targatStr.toLowerCase()) != -1 ? true : false;
}

/**
 * update selectedids from selected option list
 *
 * @param selectedlist : Dom object of select element
 * @param selectedids : Dom object
 */
function updateSelectedIds(selectedlist,selectedids)
{
	// clear all selected values
	selectedids.val("");

	selectedlist.children().each(function () {
		if ( selectedids.val() == '' )
		{
			selectedids.val($(this).val());
		}
		else
		{
			selectedids.val( selectedids.val() + ", "+$(this).val() );
		}
	});
}

function validateAssociation(yearVal)
{
	var $year = $('#associationframe').contents().find("#year");
	var rectype = $('#rectype').val();
	var eventYear = get4DigitYear(yearVal);

	if ( eventYear != null && eventYear != undefined )
	{
		if ($year)
		{
			$year.val(eventYear);
		}
	}
	else
	{
		$year.val("");
		return;
	}

	/* FIXME: use of "eval()" is dangerous, creating opportunities for XSS attacks. Use "JSON.parse()" instead. */
	var associationListJson = eval($('#associationframe').contents().find("#associationListJson").val());
	var $selectedids = $('#associationframe').contents().find("#selectedids");

	// clear search boxes for Available and Selected
	$('#associationframe').contents().find("#txtsearchavailable").val("");
	$('#associationframe').contents().find("#txtsearchselected").val("");

	var $available = $('#associationframe').contents().find("#available");
	var $selected = $('#associationframe').contents().find("#selected");

	// clear available and selected option lists
	clearoptions($available);
	clearoptions($selected);

	// set data into available and select options
	var selectedIndex = 0;
	var availableIndex = 0;
	for (var index=0; index < associationListJson.length; index++)
	{
		var association = associationListJson[index];
		if ( association != undefined )
		{
			var option = document.createElement('option');
			$(option).attr('value', association['id']);
			$(option).html( association['value'] );

			//alert(association['year'] + " == "+ $year.val() );
			// work year must be less than event data year
			if ( ( rectype == 'works'  && $year.val() <= association['year'] )
					|| ( (rectype == 'events' || rectype == 'reviews' || rectype == 'publicationevents')  && $year.val() >= association['year'] ) )
			{
				if ( findSelectedId(association['id'], $selectedids.val()))
				{
					selectedIndex++;
					$(option).attr('class', ((selectedIndex)%2==0?'even':'odd' ) + " " +association['id']);
					$selected.append(option);
				}
				else
				{
					availableIndex++;
					$(option).attr('class', ((availableIndex)%2==0?'even':'odd' ) + " " +association['id']);
					$available.append(option);
				}
			}
		}
	}

	// reset the selectedids field
	updateSelectedIds($selected,$selectedids);
}

function get4DigitYear(datavalue)
{
	var yearRegex = /\d{4}/gm;
	return yearRegex.exec(datavalue);
}
