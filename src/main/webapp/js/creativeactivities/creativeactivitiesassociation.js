$(document).ready(function() {

	removeInvalidAssociationOptions();

	setSelectedValuetoParentForm($('#selectedids').val());

	// filter available list of elements
	$("#txtsearchavailable").change(function() {
		findIntoAvailableList($(this).val());
	});

	$("#txtsearchavailable").bind('keypress',function(e) {
		if (e.keyCode == '13')
		{
			findIntoAvailableList($(this).val());
			return false;
		}
	});

	// filter selected list of elements
	$("#txtsearchselected").change(function() {
		findIntoSelectedList($(this).val());
	});

	$("#txtsearchselected").bind('keypress',function(e) {
		if (e.keyCode == '13')
		{
			findIntoSelectedList($(this).val());
			return false;
		}
	});

	// Add items into selected box from available box
	$("#add").click(function() {
		var $availablelist = $('#available option:selected');
		var $selectedlist = $('#selected');

		$availablelist.each(function () {
			$selectedlist.append(this);
		});

		updateSelectedIds($('#selected'),$('#selectedids'));

		setEvenOddOptionCss($('#selected'));
		setEvenOddOptionCss($('#available'));

		$("#txtsearchselected").val("");

		setSelectedValuetoParentForm($('#selectedids').val());

		// return false to stop submit of form
		return false;
	});

	// Add items into available box from selected box
	$("#remove").click(function() {
		var $availablelist = $('#available');
		var $selectedlist = $('#selected option:selected');
		$selectedlist.each(function () {
			$availablelist.append(this);
		});

		updateSelectedIds($('#selected'),$('#selectedids'));

		setEvenOddOptionCss($('#selected'));
		setEvenOddOptionCss($('#available'));

		$("#txtsearchavailable").val("");

		setSelectedValuetoParentForm($('#selectedids').val());

		// return false to stop submit of form
		return false;
	});


});

function setSelectedValuetoParentForm(selected)
{
	var $parentSelectedIds = $(parent.document).contents().find('#selectedids');
	$parentSelectedIds.val(selected);
}

function removeInvalidAssociationOptions()
{
	/* FIXME: use of "eval()" is dangerous, creating opportunities for XSS attacks. Use "JSON.parse()" instead. */
	var associationListJson = eval($('#associationListJson').val());
	var $availablelist = $('#available');
	var $selectedlist = $('#selected');
	var selectedids = $('#selectedids').val();
	var year = $('#year').val();

	for (var index=0; index < associationListJson.length; index++)
	{
		if ( year != '' && !isNaN(year) && year >= 1900 )
		{
			var assObj = associationListJson[index];

			if ( findSelectedId(associationListJson[index]['id'], selectedids) )
			{
				// selected
				if ( !associationListJson[index]['isvalid'] || associationListJson[index]['isvalid'] == 'false' ) {
					//remove item
					removeOption($selectedlist,associationListJson[index]['id']);
				}
			}
                        else
			{
				// available
				if ( !associationListJson[index]['isvalid'] || associationListJson[index]['isvalid'] == 'false' ) {
					//remove item
					removeOption($availablelist, associationListJson[index]['id']);
				}
			}
		}
	}
}

function findIntoSelectedList(txtsearchselected)
{
	var associationListJson = eval($('#associationListJson').val());
	var $selectedlist = $('#selected');
	clearoptions($selectedlist);
	/*var txtsearchselected = $(this).val();*/
	var matchIndex=0;
	var $year = $('#year');
	var rectype = $('#rectype').val();

	for (var index=0; index < associationListJson.length; index++)
	{
		if ( associationListJson[index] != undefined &&
				matchAssociation(associationListJson[index]['value'],txtsearchselected) &&
				findSelectedId(associationListJson[index]['id'], $('#selectedids').val()))
		{
			if ( $year != undefined && $year.val() != '' && $year.val() != '0')
			{
				if ( ( rectype == 'works'  && $year.val() > associationListJson[index]['year'] )
						|| ( (rectype == 'events' || rectype == 'reviews')  && $year.val() < associationListJson[index]['year'] ) )
				{
					continue;
				}
			}

			matchIndex++;
			var option = document.createElement('option');
			$(option).attr('value',associationListJson[index]['id']);
			$(option).attr('title',associationListJson[index]['value']);
			$(option).attr('class', ((matchIndex)%2==0?'even':'odd' ) + " " +associationListJson[index]['id']);
			$(option).html( associationListJson[index]['value'] );

			$selectedlist.append(option);
		}
	}
	setSelectedValuetoParentForm($('#selectedids').val());
}

function findIntoAvailableList(txtsearchavailable)
{
	var associationListJson = eval($('#associationListJson').val());
	var $availablelist = $('#available');
	clearoptions($availablelist);
	/*var txtsearchavailable = $(this).val();*/
	var matchIndex=0;
	var $year = $('#year');
	var rectype = $('#rectype').val();

	for (var index=0; index < associationListJson.length; index++)
	{
		if ( associationListJson[index] != undefined &&
				matchAssociation(associationListJson[index]['value'],txtsearchavailable) &&
				!findSelectedId(associationListJson[index]['id'], $('#selectedids').val()))
		{
			if ( $year != undefined && $year.val() != '' && $year.val() != '0')
			{
				if ( ( rectype == 'works'  && associationListJson[index]['year'] < $year.val() )
						|| ( (rectype == 'events' || rectype == 'reviews')  && associationListJson[index]['year'] > $year.val() ) )
				{
					continue;
				}
			}

			matchIndex++;
			var option = document.createElement('option');
			$(option).attr('value',associationListJson[index]['id']);
			$(option).attr('title',associationListJson[index]['value']);
			$(option).attr('class', ((matchIndex)%2==0?'even':'odd' ) + " " +associationListJson[index]['id']);
			$(option).html( associationListJson[index]['value'] );

			$availablelist.append(option);
		}
	}
	setSelectedValuetoParentForm($('#selectedids').val());
}

function setEvenOddOptionCss(optionlist)
{
	$(optionlist).children('option').each(function (index) {
		if ( $(this).hasClass('odd') )
		{
			$(this).removeClass('odd').addClass((index+1)%2==0?'even':'odd');
		}
                else if ( $(this).hasClass('even') )
		{
			$(this).removeClass('even').addClass((index+1)%2==0?'even':'odd');
		}
                else if ( $(this).hasClass('odderror') )
		{
			$(this).removeClass('odderror').addClass((index+1)%2==0?'evenerror':'odderror');
		}
                else if ( $(this).hasClass('evenerror') )
		{
			$(this).removeClass('evenerror').addClass((index+1)%2==0?'evenerror':'odderror');
		}
	});
}
