
$(document).ready(function() {

    $('#association').addClass('hidden');

    // adding fields for suggestion
    /*if ($('#eventtypeid').val() != undefined && typeof jQuery.ui !== 'undefined') {
        var txteventtypeid = '<input type="text" id="txteventtypeid" name="txteventtypeid" value="" maxlength="50">';
        $('#eventtypeid').after(txteventtypeid);

        //var txteventtypeerrorlabel = '<label for="txteventtypeid" class="hidden">Event Type</label>';
        //$('#txteventtypeid').after(txteventtypeerrorlabel);
        // Move the label that referenced 'eventtypeid' to now point to 'txteventtypeid'
        $('label[for="eventtypeid"]').attr('for', 'txteventtypeid');

        if ($('#eventtypeid option:selected').val() != 0) {
            $('#txteventtypeid').val($('#eventtypeid option:selected').text());
        }

        $('#txteventtypeid').autocomplete({
            minLength : 0,
            source : '/miv/suggest/event/type',
            focus : function(event, ui) {
                $('#txteventtypeid').val(ui.item.value);
                return false;
            },
            select : function(event, ui) {
                $('#txteventtypeid').val(ui.item.value);
                $('#eventtypeid').val(ui.item.id);
                return false;
            },
            change : function(event, ui) {
                if (ui.item == null) {
                    $('#txteventtypeid').val('');
                    $('#eventtypeid').val('0');
                    $('#txteventtypeid').focus();
                }
            }
        });

    }*/

    $('.datepicker').datepicker({
        dateFormat : 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        constrainInput : false,
        yearRange: '1900:'+(parseInt($('#thisYear').val())+2),
        currentText: 'Find Today',
        closeText: 'Close',
        buttonImage: '/miv/images/buttons/datepicker.png',
        buttonImageOnly: true,
        showOn: 'both'
    });

    $('img[class="ui-datepicker-trigger"]').each(function() {
        $(this).attr('title', 'Click to open a datepicker. format is (mm/dd/yy)');
    });

    if ( $('#usepopups').val() == 'false' /*&& typeof jQuery.ui !== 'undefined'*/) {

            /*var selectedids = '<input type="hidden" id="selectedids" name="selectedids" value="">';
            $('#display').after(selectedids);*/

            // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
            $( '#dialog:ui-dialog' ).dialog( 'destroy' );

            tips = $( '.validateTips' );

            $( '#newevent' ).button().click(function() {

                var innerHTMLData = '<div id="tipsbox">' +
                                        '<p class="validateTips">* = Required Field</p>' +
                                    '</div>' +
                                    '<fieldset style="width: 94%; height: 95%;">' +
                                        '<iframe id="addelement" name="addelement" scrolling="auto" height="505px" width="680px" src="/miv/EditRecord?rectype=works&pubtype=94&documentid=&sectionname=&subtype=&E0000=Add+a+New+Record&usepopups=true" frameborder="0" scrolling="yes">' +
                                        '</iframe>'+
                                    '</fieldset>';
                //alert(innerHTMLData);

                $( '#dialog-form' ).html(innerHTMLData);

                $( '#dialog-form' ).dialog( 'open' );
            });
            
            $( '#dialog-form' ).dialog({
                autoOpen: false,
                height: 635,
                width: 710,
                modal: true,
                buttons: {
                    'Save': function() {
                        removeErrorDiv();
                        var dialogContents = $('#addelement').contents();
                        var workyear = dialogContents.find( '#workyear' );
                        var worktypeid = dialogContents.find("#worktypeid");
                        var statusid = dialogContents.find( '#statusid' );
                        var creators = dialogContents.find( '#creators' );
                        var title = dialogContents.find( '#title' );
                        var description = dialogContents.find( '#description' );
                        var url = dialogContents.find( '#url' );
                        var allFields = $( [] ).add( workyear ).add( worktypeid ).add( creators ).add( title ).add( description ).add( url ).add( statusid );

                        allFields.removeClass( 'errorfield' );
                        var bValid = true;

                        if (!checkRequired(workyear)) bValid = false;
                        if ( bValid && !checkRegexp( workyear, /\d{4}/i, 'Year must be 4 digit number.' )) bValid = false;

                        if ( bValid && !isNaN(workyear.val()) )
                        {
                            var numyear = workyear.val() * 1;
                            var curYear = new Date().getFullYear();

                            if ( numyear < 1900 || numyear > (curYear +2) ) {
                                workyear.addClass( 'errorfield' );
                                updateTips( workyear, 'Year must be from 1900 to '+(parseInt($('#thisYear').val())+2)+'.' );
                                bValid = false;
                            }
                        }

                        if (worktypeid.val() == 0) {
                            worktypeid.addClass( 'errorfield' );
                            updateTips( worktypeid, 'This field is required.' )
                            bValid = false;
                        }

                        if ( statusid.val() == 0 ) {
                            statusid.addClass( 'errorfield' );
                            updateTips( statusid, 'This field is required.' );
                            bValid = false;
                        }

                        if (!checkRequired(creators)) bValid = false;
                        if (!checkRequired(title)) bValid = false;
                        if (!checkRequired(description)) bValid = false;

                        if ( url.val().replace(/(^\s*|\s*$)/i, '').length > 0 && !checkRegexp( url, /^(https?|ftp):\/\//, 'Invalid url string.' )) {
                            bValid = false;
                        }

                        if ( !bValid ) {
                            focusFirstError(allFields);
                        }
                        else {
                            var $from = $('#addelement').contents().find('form');
                            var action = $from.attr( 'action' );
                            var formdata = $from.serialize();

                            /* Send the data using post and put the results in a div */
                            $.post( action, formdata,
                                function( data ) {
                                    var resultObj = eval('(' + data + ')');
                                    var recid = resultObj.recnum;
                                    var result = resultObj.success;

                                    if (resultObj != undefined) {
                                        if (result != undefined && result) {
                                            $('#associationframe').attr('src', $('#associationframe').attr('src'));
                                            $( '#dialog-form' ).dialog( 'close' );
                                            showMessage(null,"<div id=\"successbox\">"+resultObj.message+"</div>",$("#associationframe").position().top);
                                        }
                                        else {
                                            showResponseError(resultObj.errorfields,resultObj.message,allFields);
                                        }
                                    }
                                });
                        }
                    },
                    Cancel: function() {
                        $( this ).dialog( 'close' );
                    }
                },
                close: function() {
                    $( '#dialog-form' ).html('');
                }
            });
    }
    else {
        $('.associationbuttons').remove();
    }

    $('#eventstartdate').change(function() {
        if (typeof validateAssociation == 'function') {
                validateAssociation($(this).val());
        }
    });

    $('#year').change(function() {
        if (typeof validateAssociation == 'function') {
            validateAssociation($(this).val());
        }
    });

    /*$('#associationframe').load(function() {
        removeInvalidAssociationOptions();
    });*/

});
