'use strict';

$(document).ready(function() {

	$('.percentageofreturn').addClass('hidden');
	getPercentageOfReturn();

	$('#responsetotal, #enrollment').keyup(function() {
		getPercentageOfReturn();
	});

});

function getPercentageOfReturn()
{
	var responsetotal = $('#responsetotal');
	var enrollment = $('#enrollment');

	try{
		$('#percentageofreturn').val('');
		$('.percentageofreturn').addClass('hidden');

		if( !isNaN(responsetotal.val()) && !isNaN(enrollment.val()) && enrollment.val() > 0 && responsetotal.val() > 0)
		{
			$('#percentageofreturn').val( ((responsetotal.val() / enrollment.val()) * 100).toFixed(2) + '%' );
			$('.percentageofreturn').removeClass('hidden');
		}
	}catch(err)
	{
		//$("#percentageofreturn").val("");
	}
}
