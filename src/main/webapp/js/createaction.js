/* global actionTypes, miv */
'use strict';
$(document).ready(function() {
    var VALIDATION_ERROR = 'You must specify the user for which the action is being started.';
    var form = $('form');
    var delAuthOptions = $('#delegationAuthority option');
    $('option[value=REDELEGATED]').prop('selected', true);

    //without this, there would be a gap between validation on the search and
    //validation on actual user selection
    var dtInitialized = false;

    miv.validate.registerValidator('rowSelected', function(selector) {
        return $('tr.selected').length === 1 ? true : VALIDATION_ERROR;
    });

    miv.validate.registerValidator('dataTableInitialized', function() {
        return dtInitialized ? true : VALIDATION_ERROR;
    })

    $('.datepicker').datepicker({
        dateFormat : 'MM d, yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        constrainInput : false,
        yearRange: 'c-10:'+(parseInt($('#currentYear').val())+1),
        currentText: 'Find Today',
        closeText: 'Close',
        buttonImage: '/miv/images/buttons/datepicker.png',
        buttonImageOnly: true,
        showOn: 'both'
   });

    $('img[class="ui-datepicker-trigger"]').each(function() {
        $(this).attr('title', 'Open the datepicker');
    });

    form.on('init.dt', '.dataTable', {}, function() {
        $('.dataTable').addClass('validated')
                       .data('constraints', 'rowSelected');

        dtInitialized = true;
    });

    form.on('destroy.dt', '.dataTable', {}, function() {
        dtInitialized = false;
    });

    // correct effectiveDate format
    $('#effectiveDate').change(function(event) {
        var $self = $(this);

        if ($self.val().trim() !== '') {
            if (validateEffectiveDate($self.val()) === true)
            {
                $self.val(Date.parse($self.val()).toString('MMMM d, yyyy'));
            }
            else
            {
                $self.val('').focus();
            }
        }
    });

    $('#actionType').change(function() {
        var validDelegations = actionTypes[$(this).val()];
        var validDelAuthOptions = $();
        var delAuthSelect = $('#delegationAuthority');

        for (var i = 0; i < validDelegations.length; i++) {
            validDelAuthOptions = validDelAuthOptions.add(
                delAuthOptions.filter('[value=' + validDelegations[i] + ']')
            );
        }

        delAuthSelect.empty()
                     .append(validDelAuthOptions);

        //default to redelegated, if option exists
        $('option[value=REDELEGATED]', delAuthSelect).prop('selected', true);

        delAuthSelect.trigger('change');
    });

    $('input.startAction').click(function(event) {
        event.preventDefault();

        //Not actually doing a proper submit, so we'll need to do the validation 100% ourselves.
        miv.validate.config('favorNative', false);

        if (miv.validate.validate()) {
            var targetUserID = $('.dataTable').DataTable().row('.selected').data().userId;

            miv.api.post(
                '/action',
                {
                    'success': function(action) {
                        window.location.href = '/miv/createAction/confirm?dossier=' + action.dossierId;
                    }
                },
                {
                    userId : targetUserID,
                    actionType : $('#actionType option:selected').val(),
                    submitterId : $('#submitterId').val(),
                    effectiveDate : Date.parse($('#effectiveDate').val()),
                    delegationAuthority : $('#delegationAuthority').val(),
                    accelerationYears : 0
                }
            );
        }
    });

    $('.buttonlink').click(function() {
        window.location.href = $(this).data('href');
    });
});
// $(document).ready


/**
 * validate effective date field
 */
function validateEffectiveDate(value) {
    var date = Date.parse(value);
    return date !== null && date instanceof Date && date.compareTo(Date.parse('01/01/1900')) > -1;
}
// validateDateField
