'use strict';
/* global QUnit, miv */

QUnit.module('The miv.dom library');
QUnit.test('Api Completeness', function(assert) {
    assert.ok(miv.dom.tag instanceof Function, 'contains the tag generation function');
    assert.ok(miv.dom.getTextNode instanceof Function, 'contains the text-only extraction function');
});

QUnit.test('The MIV dom tag generation library', function(assert) {
    var domElement = miv.dom.tag('div', {class : 'testClass', id : 'fakeDiv'});

    assert.ok(domElement instanceof jQuery, 'returns a jQuery object');
    assert.ok(domElement.is('div'), 'containing the proper tag');
    assert.ok(domElement.is('#fakeDiv.testClass'), 'with the specified attributes');
});

QUnit.test('The MIV text extraction function', function(assert) {
    var domElement = $('<div>Return this<span>but not this</span> and this</div>');

    assert.equal(miv.dom.getTextNode($('span', domElement)),
                'but not this',
                'returns the text inside the tag');
    assert.equal(miv.dom.getTextNode(domElement),
                'Return this and this',
                'without text in its children');
});
