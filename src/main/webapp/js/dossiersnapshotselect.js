'use strict';

$(document).ready(function( ) {
    $('table.sortable').tablesorter({sortList: [[0,0],[5,0]]});
    $(':radio').click(function( ) {
        var checkedValue = $('input[name=listby]:checked').val();
        var rowCount = 0;
        $('tbody tr').each(function() {
            if ($(this).hasClass(checkedValue) || checkedValue === 'All')
            {
                $(this).show();
                rowCount++;
            }
            else
            {
                $(this).hide();
            }
        });  //end of .each(function)
        $('div.searchresults').html('Search Results = '+rowCount);
    });  //end of .click(function)
});
