'use strict';
/* global dojo, trim, getClickedButton, validateForm */
//dojo.require("dojo.io.*");      // for FormBind
//dojo.require("dojo.*");   // for event.connect
//dojo.require("dijit.*");
//dojo.require("dojo.validate");
//dojo.require("dojo.validate.check");
dojo.require('dojo.parser');
dojo.require('dijit.layout.ContentPane');
dojo.require('dijit.form.ValidationTextBox');
dojo.require('dijit.form.NumberTextBox');

//dojo.addOnLoad( function() { setTimeout(runBiosketchInit, 200); } );
var mivFormProfile;

$(document).ready(function() {
    if ($('form.biosketchdisplayoptions').size() > 0)
    {
        mivFormProfile = {
                trim: ['name', 'title'],
                required: ['name', 'title']
        };
    }

    if ($('form.selectbiosketchdata').size() > 0)
    {
        mivFormProfile = {
                constraints: {
                    startYear: function(contents) {
                        var curYear = new Date().getFullYear();

                        if (trim(contents)==='') {
                            return true;
                        }

                        if (isNaN(contents)) {
                            return 'INVALID_NUMBER';
                        }

                        var numyear = contents * 1;

                        if ( numyear < 1900 || numyear > curYear) {
                            return 'INVALID_YEAR';
                        }

                        return true;
                    },
                    endYear:  function(contents) {
                        var curYear = new Date().getFullYear();

                        if (trim(contents)==='') {
                            return true;
                        }

                        if (isNaN(contents)) {
                            return 'INVALID_NUMBER';
                        }

                        var numyear = contents * 1;

                        if ( numyear < 1900 || numyear > curYear ) {
                            return 'INVALID_YEAR';
                        }

                        var startyearObj = dojo.byId('startYear');

                        if ( startyearObj && trim(startyearObj.value).length > 0 &&
                             !isNaN(startyearObj.value) &&
                             numyear < startyearObj.value) {
                            return 'INVALID_END_YEAR_RANGE';
                        }

                        return true;
                    }
                },
                errormap: { 'INVALID_YEAR':'Year must be from 1900 to ${thisYear}',
                            'INVALID_NUMBER':'Year must be numeric.',
                            'INVALID_END_YEAR_RANGE':'End Year must be greater than or equal to Start Year.'}
            };
    }

    runBiosketchInit();

    if ($('form.designmybiosketch').size() > 0)
    {
        setUpDesignMyBiosketch();
        setUpCheckUncheckAll();
    }
});

var curtop = 0;

function runBiosketchInit()
{
    var $mybiosketchform = $('#biosketchform');
    $mybiosketchform.submit(function( event ) {

        var $clickedButton = getClickedButton($(this));

        if( $clickedButton.prop('id') !== 'popupformcancel')
        {
            if (typeof mivFormProfile !== 'undefined') {
                var $errormessage = $('#errormessage');


                if (!validateForm($mybiosketchform, mivFormProfile, $errormessage)) {
                    event.preventDefault();
                }
            }
        }

    });

    // Attach handler to respond to 'Delete' button clicks
    //var delList = dojo.getElementsByClass("recorddel", listAreaElement);
    dojo.query('#listarea .recorddel').onclick(doDelete);
    //dojo.connect( delList, 'onclick', doDelete );

    // Clear the highlighting from the "duplicated" record if one was set.
    setTimeout(
        function() {
            //var prev = dojo.getElementsByClass("previous", listAreaElement);
            var prev = dojo.query('#listarea .previous');
            dojo.forEach(
                prev,
                function(node, idx, ary) {
                    dojo.removeClass(node, 'previous');
                }
            );
        },
        7000 // clear the highlighting after 7 seconds
    );
    var hCAlign = dojo.byId('headerCAlign');
    if (hCAlign !== null && hCAlign !== undefined)
    {
        dojo.connect( hCAlign, 'onclick', disableIndent );
    }
    var hLAlign = dojo.byId('headerLAlign');
    if (hLAlign !== null && hLAlign !== undefined)
    {
        dojo.connect( hLAlign, 'onclick', enableIndent );
    }
    var cancelBtn = dojo.byId('popupformcancel');
    if (cancelBtn === null || cancelBtn === undefined) { cancelBtn = dojo.byId('formcancel'); }
    if (cancelBtn === null || cancelBtn === undefined) { cancelBtn = dojo.byId('cancel'); }
    if (cancelBtn !== null && cancelBtn !== undefined && cancelBtn.nodeName === 'INPUT')
    {
        switch (cancelBtn.type)
        {
            case 'submit':
            case 'reset':
            case 'button':
                //alert("ID: "+cancelBtn.id+" | nodeName: "+cancelBtn.nodeName+" | type: "+cancelBtn.type);
                dojo.connect( cancelBtn, 'onclick', cancelform );
                break;
            default:
                // do nothing; the element is not a button.
                break;
        }
    }
    cancelBtn = dojo.byId('popupformcancelBottom');
    if (cancelBtn !== null &&
        cancelBtn !== undefined &&
        cancelBtn.nodeName === 'INPUT' &&
        cancelBtn.type === 'reset')
    {
        dojo.connect( cancelBtn, 'onclick', cancelform );
    }
}


function cancelform(e)
{
    e.preventDefault();

    var backurl;
    var cancelurl = dojo.byId('cancelurl');

    if (cancelurl) {
        if(cancelurl.firstChild !== null && cancelurl.firstChild !== undefined)
        {
            backurl = cancelurl.firstChild.nodeValue;
        }else
        {
            backurl = cancelurl.value;
        }
    }

    if (!backurl) {
        backurl = document.referrer;
    }

    if (backurl.length > 0) {
        backurl = cleanUrl(backurl);
        backurl = backurl.replace(/[&?]biosketchid=[^&]+/, '');
        window.location = backurl;
    }
    else {
        history.back();
    }
}


function cleanUrl(url)
{
    var cleaned = url;
    // if there's a leftover "recid=NNN" strip it off
    cleaned = cleaned.replace(/&recid=\d+/, '');
    // strip any previous completed-message
    //  ? but what to look for to end the pattern? anything not an '&'
    cleaned = cleaned.replace(/&CompletedMessage=[^&]+/, '');
    cleaned = cleaned.replace(/&timestamp=[^&]+/, '');
    return cleaned;
}

/*
function onKeyUp(e)
{
    managePresent();
}


function managePresent()
{
    var textBoxId = dojo.byId("endYear");
    var checkBoxID = dojo.byId("presentYear");

    if (textBoxId)
    {
        var textLength = textBoxId.value.length;

        if (textLength != null && textLength > 0)
        {
            checkBoxID.checked = false;
            checkBoxID.disabled = true;
        }
        else
        {
            checkBoxID.checked = true;
            checkBoxID.disabled = false;
        }
    }
}
*/


var globalTimer;// = undefined;
function doDelete(e)
{
    e.preventDefault();
//mmn - trying some replacement code!!
    //var btnPressed = dojo.getEventTarget(e).id;
    var btnPressed = e.target.id;
    //alert("doDelete called with event ["+e+"], button ["+btnPressed+"]");
    var btnEl = dojo.byId(btnPressed);
    //var recItem = dojo.getParentByType(btnEl, "LI");
    var recItem = dojo.query('li#P'+btnPressed.substring(2))[0];
//    //var recItemForPdfs = dojo.getParentByType(btnEl, "UL");
//    var recItemPdftemp = dojo.query("UL");
//    for (var i=0; i<recItemPdftemp.length; i++)
//    {
//        if (dojo.isDescendant(recItem, recItemPdftemp[i]))
//        {
//        // ?? What's this? It creates a var in a scope that immediately closes.
//            var recItemForPdfs = recItemPdftemp[i];
//        }
//    }

    var preItem = dojo.byId('P'+btnPressed.substring(2));
    if (preItem === null || preItem === undefined)
    {
        preItem = btnEl;
    }

    curtop = 0;
    if (preItem !== null && preItem !== undefined)
    {
        if (preItem.offsetParent) {
            do {
                curtop += preItem.offsetTop;
            } while (preItem = preItem.offsetParent); //jshint ignore:line
        }
    }

    var deleteMessage = 'This document and all of its settings will be permanently deleted.';
    var editEl = document.createElement('editing');
    dojo.place(editEl, recItem, 'first');

    var removeHighLight = getDeleteClassFunction(recItem, editEl);
    var confirmed = confirm(deleteMessage);

    if (!confirmed)
    {
        removeHighLight();
        return;
    }

    // Remove the DOM node, then send a message to the server to delete the actual record.
    recItem.parentNode.removeChild(recItem);

    var mivRecDeleteURL = '/miv/biosketch/BiosketchPreview?'+btnPressed+'=Delete';

//    var msgDiv = dojo.byId("CompletedMessage");
//    //dojo.dom.removeChildren(msgDiv);
    dojo.xhrPost({
        url: mivRecDeleteURL,
        load: function(response, ioArgs)
        {
            deleteFinished(response, ioArgs);
        },
        handleAs: 'text'
    });
} // doDelete()


function deleteFinished(data, ioArgs)
{
    var delMsg;
    var msgDiv = dojo.byId('CompletedMessage');
    var resultObj = eval({message:data}); //jshint ignore:line
    delMsg = resultObj.message;
    msgDiv.removeAttribute('style');

//alert("delete completed, data is:\n["+data+"]\nmsgDiv is ["+msgDiv+"]");
//mmn- this will be hard to replace
//var nodes = dojo.createNodesFromText(data);
//mmn this is a very temporary solution

    if(delMsg.indexOf('document has been deleted.') !== -1)
    {
        msgDiv.innerHTML = '<div id="deletebox">' + delMsg + '</div>';
    }
    else
    {
        msgDiv.innerHTML = '<div id="infobox">' + delMsg + '</div>';
    }
//alert("node list length:\n["+nodes.length+"]");
    popupMessage('CompletedMessage');
}

function popupMessage(divMessage)
{
    var aTimer;
    var msgDiv = dojo.byId(divMessage);

    msgDiv.removeAttribute('style');
    dojo.style(msgDiv, 'display', 'block');
    dojo.style(msgDiv, 'opacity', '1');
    dojo.style(msgDiv, 'position', 'absolute');
    dojo.style(msgDiv, 'top', curtop + 'px');

    var fadeTime=700;  // how long the fade takes: 7/10th's of a second
    var viewTime=4000; // how long the notice stays up: 4.0 seconds
  //viewTime=20000;//20 seconds to test.
  //viewTime=240000;//4 minutes to test.
    if (typeof globalTimer === 'number')
    {
        clearTimeout(globalTimer);
    }
    aTimer = setTimeout(function() {
        globalTimer = undefined;
        //dojo.lfx.toggle.fade.hide(msgDiv,fadeTime);
        var showfade = dojo.fadeOut({node: msgDiv, duration: fadeTime});
        showfade.play();
    }, viewTime);
    globalTimer = aTimer;
}


function getDeleteClassFunction(item, cssClass)
{
    function _deleteCssClass() {
        dojo.removeClass(item, cssClass);
    }
    return _deleteCssClass;
}


function disableIndent(e)
{
    var hAlign = dojo.byId('headerCAlign');
    var indent = dojo.byId('headerIndent');
    if (indent !== null && indent !== undefined && hAlign.checked)
    {
        //dojo.disableSelection(indent);
        indent.disabled = true;
        dojo.addClass(indent,'disabled');
    }
}//disableIndent


function enableIndent(e)
{
    var hAlign = dojo.byId('headerLAlign');
    var indent = dojo.byId('headerIndent');
    if (indent !== null && indent !== undefined && hAlign.checked)
    {
        //dojo.enableSelection(indent);
        indent.disabled = false;
        dojo.removeClass(indent,'disabled');
    }
}//enableIndent


/*
 * To Group the Document header and section records
 */
function setUpDesignMyBiosketch()
{
    $('div.documenthead').each( function(index) {
        var nextAllElements = $(this).nextAll('div.section, div.sectionsubhead, div.listblock, div.documenthead');

        var document = $('<div/>').prop('id','document'+index);
        var documenthead = $(this);
        document.append($(documenthead));

        var isdocumenthead = false;
        $(nextAllElements).each( function() {
            if(!isdocumenthead)
            {
                if($(this).hasClass('documenthead'))
                {
                    isdocumenthead = true;
                }else{
                    document.append($(this));
                }
            }
        });

        $(document).appendTo($('div#workarea'));
    });
}

/*
 * To create dynamic CheckAll UncheckAll buttons for each document
 */
function setUpCheckUncheckAll()
{
    $('div[id^="document"]').each(function( index )
    {
        // Add CheckAll UncheckAll buttons only if you have some check boxes to check or uncheck
        if($('input[type=checkbox]:not(:disabled)',$(this)).not('.contribution').size() > 0)
        {
            var checkuncheck_buttongroup = $('<div/>').addClass('formline buttonset right checkuncheck-buttongroup');
            checkuncheck_buttongroup.append('<input type="button" onclick="javascript:checkUncheckAll($(\'#document'+index+'\'),true)" value="Check Section" title="Check All Section Records">');
            checkuncheck_buttongroup.append('<input type="button" onclick="javascript:checkUncheckAll($(\'#document'+index+'\'),false)" value="Clear Section" title="Clear All Section Records">');

            $(this).children('div.documenthead').append($(checkuncheck_buttongroup));
        }
    });
}
/*<span class="vim">
 vim:ts=8 sw=4 et sr:
</span>*/
