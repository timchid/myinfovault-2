'use strict';
/*
 * The menu is used on most pages. JQuery's use of the dollar sign must not conflict
 * with the use of the Prototype framework on 'jsp/resquence.jsp'.
 */

//jQuery.noConflict();
var $J=jQuery;
// Selectors
var bodyTag;
var listItems;
var listHeaders;

// Stack of timeout pointers
var timeoutStack;
var timeoutMenu;

// Hover CSS class defined in miv.css
var hoverClass;
$J(document).ready(function() {
    // Set delay of seven-tenths of a second
    timeoutMenu = new TimeoutMenu(700);

    /*
     * Defines the TimeoutMenu object for a given argument of delay in milliseconds.
     *
     * Instantiating the object sets event listeners and handlers to delay
     * the close of the 'jsp/mivmenu.html' when the mouse momentarily leaves
     * the menu. Set timeout property to the whole number delay in milliseconds.
     */
    function TimeoutMenu(delay)
    {
        // Selectors
        bodyTag = $J('body');
        listItems = $J('#menu li');
        listHeaders = $J('#menu li.expand, #menu >ul >li');

        // Stack of timeout pointers
        timeoutStack = [];

        // Hover CSS class defined in miv.css
        hoverClass = 'hover';

        // Event listeners
        bodyTag.bind('click',escape);
        listItems.bind('mouseenter',enter);
        listHeaders.bind('mouseleave',leave);

        // Mouse clicks away from the menu
        function escape()
        {
            removeHover(listItems);
        }

        // Mouse enters a list item
        function enter(event)
        {
            // Cancel the most recent timeout
            clearTimeout(timeoutStack.pop());

            var current = $J(event.currentTarget);

            // Remove the hover class from all but the current targets ancestors and descendants
            removeHover(listItems.not(current.parents()).not(current.find('li')));

            // Add hover class current mouse target
            current.addClass(hoverClass);
        }

        // Mouse leaves a list header
        function leave(event)
        {
            // Push the remove hover class on target to the timeout stack
            timeoutStack.push(setTimeout(function() { removeHover($J(event.currentTarget)); }, delay));
        }

        function removeHover(targets)
        {
            targets.removeClass(hoverClass);
        }
    }
});
