'use strict';
/**
 * Sets up disclosure mail form validation and text area note text hint.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.1
 */
$(document).ready(function() {
	/**
	 * @constant
	 * @type {jQuery}
	 */
	var toRequiredError = $('<li/>').append('<em>To</em> address is required');

    // form validation on save button click
    $('#save').click(function(event) {
        // if 'To' field is empty
        if (!$.trim($('#emailTo').val())) {
            // stop form submission
            event.preventDefault();

            //clear previous errors and add current error to error list
            $('#errormessage ul').empty().append(toRequiredError);

            //show error container
            $('#errormessage').addClass('haserrors');
        }
    });
});
