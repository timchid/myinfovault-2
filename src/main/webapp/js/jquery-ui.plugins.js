/*

    NOTE!

    Even though this file is called "jquery-ui.plugins" it is NOT part of jquery or jquery-ui
     ... don't waste your time looking for jQuery documentation like I did.

    These are Plug-Ins built here, for MIV, that leverage jquery-ui functionality.

 */


/*
 * jQuery UI Collapsible Fieldset
 *
 * Depends:
 *     jquery.ui.widget.js
 *     /css/expandable.css
 *
 * Options:
 *     Name                   Type              Default             Parameters  Description
 *     -----------------------------------------------------------------------------------------------------------------------
 *     sCollapsibleClass      string            "collapsible"       -
 *     sCollapsibleBodyClass  string            "collapsible-body"  -
 *     sCollapsibleHintClass  string            "hint"              -
 *     sCollapsedClass        string            "collapsed"         -
 *     sHint                  string,function                       element     Text hint when field set in closed state
 *     sOpenTooltip           string,function                       element     Field set legend tool-top when in closed state
 *     sCloseTooltip          string,function                       element     Field set legend tool-top when in open state
 *     open                   function                              event       Callback fired after the field set opens
 *     close                  function                              event       Callback fired after the field set closes
 *     bOpen                  boolean,function  true                element     In open state initially?
 *
 * Methods:
 *     open
 *     close
 *     destroy
 *
 * Example:
 *     // Give collapsible functionality to selector elements
 *     $("fieldset").collapsible({
 *         open  : function(event) {
 *                     //do something on open
 *                 },
 *         close : function(event) {
 *                     //do something on close
 *                 },
 *         bOpen : function(element) {
 *                     //return if open initially
 *                 }
 *         sHint : function(element) {
 *                     return "The legend says this: " + $("legend", element).text();
 *                 }
 *     });
 *
 *     // Trigger open on selector elements
 *     $("fieldset").collapsible('open');
 */

// Event key codes
var key = {
    ENTER       : 13,
    SPACE       : 32,
    UP_ARROW    : 38,
    DOWN_ARROW  : 40
};

(function($) {
    $.widget("ui.collapsible", {
        options : {
            sCollapsibleClass     : "collapsible",
            sCollapsibleBodyClass : "collapsible-body",
            sCollapsibleHintClass : "hint",
            sCollapsedClass       : "collapsed",
            sHint                 : function(element) {
                                        return "expand to see " + $("legend", element).text().toLowerCase();
                                    },
            sOpenTooltip          : function(element) {
                                        return "Select to expand " + $("legend", element).text().toLowerCase();
                                    },
            sCloseTooltip         : function(element) {
                                        return "Select to collapse " + $("legend", element).text().toLowerCase();
                                    },
            open                  : $.noop,
            close                 : $.noop,
            bOpen                 : true
        },

        // Create the open/close functionality
        _create : function() {
            var self = this;

            // Give main element the collapsible class
            self.element.addClass(self.options.sCollapsibleClass);

            // Add open/close functionality on legend
            self.legend = $("legend", self.element).click(function(event) {
                                                              self._toggle(event);
                                                          })
                                                   // Accessibility: handle keyboard events
                                                   .keyup(function(event) {
                                                              switch (event.keyCode) {
                                                                  case key.SPACE:
                                                                      self._toggle(event);
                                                                      break;
                                                                  case key.UP_ARROW:
                                                                      self.close(event);
                                                                      break;
                                                                  case key.DOWN_ARROW:
                                                                      self.open(event);
                                                                      break;
                                                              }
                                                          });

            // Move all field set (except legend) child elements into new collapsible body div
            self.body = $("<div/>").addClass(self.options.sCollapsibleBodyClass)
                                   .append(self.element.children().not(self.legend))
                                   .appendTo(self.element);

            // Add collapsible hint text to the fieldset
            self.hint = $("<div/>").addClass(self.options.sCollapsibleHintClass)
                                   .appendTo(self.element);
        },

        // Set initial states
        _init : function() {
            // bOpen is either a boolean or function that returns one
            var bOpen = $.isFunction(this.options.bOpen)
              ? this.options.bOpen(this.element)
              : this.options.bOpen;

            bOpen ? this.open() : this.close();

            // Append to provide keyboard accessibility to field set.
            this.legend.html($("<a/>").attr("href","#")
                                      .text(this.legend.text()));

            // Set hint text
            this.hint.text($.isFunction(this.options.sHint)
                         ? this.options.sHint(this.element)
                         : this.options.sHint);
        },
        _toggle : function(event) {
            this.body.is(":hidden") ? this.open(event) : this.close(event);
        },

        // Open field set
        open : function(event) {
            //show element body
            this.body.show();

            // Remove  the collapsed class from the main element
            this.element.removeClass(this.options.sCollapsedClass);

            // Set legend title for open state
            this.legend.attr("title",
                             $.isFunction(this.options.sCloseTooltip)
                           ? this.options.sCloseTooltip(this.element)
                           : this.options.sCloseTooltip);

            // Trigger open callback
            this._trigger('open', event);
        },

        // Close field set
        close : function(event) {
            // Hide element body
            this.body.hide();

            // Give main element the collapsed class
            this.element.addClass(this.options.sCollapsedClass);

            // Set legend title for closed state
            this.legend.attr("title",
                             $.isFunction(this.options.sOpenTooltip)
                           ? this.options.sOpenTooltip(this.element)
                           : this.options.sOpenTooltip);

            // Trigger close callback
            this._trigger('close', event);
        },

        // Remove widgets footprint
        destroy : function() {
            // Remove collapsible styles
            this.element.removeClass(this.options.sCollapsibleClass).removeClass(this.options.sClosedClass);

            // Remove collapse/expand functionality and legend title
            this.legend.unbind('click').attr("title", "");

            // Restore children to fieldset and remove body
            this.body.children().appendTo(this.element);
            this.body.remove();

            // Remove expand hint
            this.hint.remove();
        }
    });
})(jQuery);


/*
 * jQuery UI More/Less Collapsible Text
 *
 * Depends:
 *     jquery.ui.widget.js
 *     /css/expandable.css
 *
 * Options:
 *     Name           Type              Default                     Parameters  Description
 *     -----------------------------------------------------------------------------------------------------------------------
 *     sOpenTooltip   string,function   "Select to show more text"  element     Field set legend tool-top when in closed state
 *     sCloseTooltip  string,function   "Select to show less text"  element     Field set legend tool-top when in open state
 *     open           function          -                           event       Callback fired after the field set opens
 *     close          function          -                           event       Callback fired after the field set closes
 *     bOpen          boolean,function  true                        element     In open state initially?
 *
 * Methods:
 *     open
 *     close
 *     destroy
 *
 * Example:
 *     //given more/less functionality to selector elements
 *     $("span.somethings").collapsible({
 *         open  : function(event) {
 *                     //do something on open
 *                 },
 *         close : function(event) {
 *                     //do something on close
 *                 },
 *         bOpen : function(element) {
 *                     //return if open initially
 *                 }
 *     });
 *
 *     //trigger open on selector elements
 *     $("span.something").collapsible('open');
 */
(function($) {
    $.widget("ui.moreless", {
        options : {
            sOpenTooltip          : "Show more text",
            sCloseTooltip         : "Show less text",
            open                  : $.noop,
            close                 : $.noop,
            bOpen                 : true
        },
        // Create the more/less functionality
        _create : function() {
            var self = this;

            self.body = $("<div/>").addClass("moreless")
                                   .appendTo(self.element.parent())
                                   .append(self.element);

            self.button = $("<button/>").click(function(event) {
                                                   event.preventDefault();

                                                   self._toggle(event);
                                               })
                                        .keyup(function(event) {
                                                   if (event.keyCode == key.SPACE) {
                                                       self._toggle(event);
                                                   }
                                               })
                                        .prependTo(self.body);

            // Reset button hidden/shown if window is resized
            $(window).resize(function() {
                self._setButton();
            });
        },

        // Set initial states
        _init : function() {
            // bOpen is either a boolean or function that returns one
            var bOpen = $.isFunction(this.options.bOpen)
              ? this.options.bOpen(this.element)
              : this.options.bOpen;

            bOpen ? this.open() : this.close();

            // Set button hidden/shown initially
            this._setButton();
        },

        // Set button hidden if body is not overflowing
        _setButton : function() {
            //hide the button initially
            this.button.hide();

            //if body is overflowed, show the button
            if (this.body.prop("scrollHeight") > this.body.prop("clientHeight")
             || this.body.prop("scrollWidth") > this.body.prop("clientWidth")) {
                this.button.show();
            }
        },

        // Toggle states between open and closed
        _toggle : function(event) {
            this.body.is(".collapsed") ? this.open(event) : this.close(event);
        },

        // Show all text
        open : function(event) {
            // Show hidden text
            this.body.removeClass("collapsed");

            // Set text and title for open state
            this.button.text("less")
                       .attr("title",
                             $.isFunction(this.options.sCloseTooltip)
                           ? this.options.sCloseTooltip(this.element)
                           : this.options.sCloseTooltip);

            // Trigger open callback
            this._trigger('open', event);
        },

        // Hide extra lines of text
        close : function(event) {
            // Hide text
            this.body.addClass("collapsed");

            // Set text and title for closed state
            this.button.text("more")
                       .attr("title",
                             $.isFunction(this.options.sOpenTooltip)
                           ? this.options.sOpenTooltip(this.element)
                           : this.options.sOpenTooltip);

            // Trigger close callback
            this._trigger('close', event);
        },

        // Remove widgets footprint
        destroy : function() {
            // Restore original element and remove body
            this.element.appendTo(this.body.parent());
            this.body.remove();
        }
    });
})(jQuery);


/**
 * checkbox with class="ui-rotate-indeterminate-checkbox" has the functionality of
 * (Rotate) Indeterminate Checkboxes
 * refrence :: http://css-tricks.com/indeterminate-checkboxes/
 */
$(function() {
    initTriStateCheckBox();
});

/**
 * Initialize tri-state checkboxes found on the page.
 * Click handler taken from CSS-TRICKS "Indeterminate Checkboxes"
 *   at http://css-tricks.com/indeterminate-checkboxes/
 * credit to Chris Coyier and Jon Stuebe
 */
function initTriStateCheckBox()
{
    var $check = $('input[type=checkbox].ui-rotate-indeterminate-checkbox');

    /* tri-state checkbox rotates through three states, held in the data attribute 'checked'
     *     state 0: unchecked
     *     state 1: indeterminate
     *     state 2: checked
     */

    // Find any properties already attached to a .ui-rotate-indeterminate-checkbox
    // and set the initial state accordingly. No props defaults to unchecked.
    $check.each(function(i, el) {
        var $this = $(this);

        // In these if-tests we don't want "truthy" values, we only want actual true/false boolean values.
        if ( $this.prop('checked') === true ) {
            $this.data('checked', 2);
            $this.addClass('checked');
            $this.removeClass('unchecked indeterminate');
        }
        else if ( $this.prop('indeterminate') === true ) {
            $this.data('checked', 1);
            $this.addClass('indeterminate');
            $this.removeClass('unchecked checked');
        }
        else /* $this.prop('unchecked') === true, or no props */ {
            $this.data('checked', 0);
            $this.addClass('unchecked');
            $this.removeClass('indeterminate checked');
        }
    });

    // Add click handler to all of the indeterminate checkboxes to rotate through the states.
    $check.click(function(e) {
        var el = $(this);
        switch (el.data('checked')) {
            // Unchecked, going to Indeterminate
            case 0:
                el.data('checked', 1);
                el.prop('indeterminate', true);
                el.prop('checked', false);
                el.addClass('indeterminate');
                el.removeClass('unchecked checked');
                break;
            // Indeterminate, going to Checked
            case 1:
                el.data('checked', 2);
                el.prop('indeterminate', false);
                el.prop('checked', true);
                el.addClass('checked');
                el.removeClass('unchecked indeterminate');
                break;
            // Checked, going to Unchecked
            default:
                el.data('checked', 0);
                el.prop('indeterminate', false);
                el.prop('checked', false);
                el.addClass('unchecked');
                el.removeClass('indeterminate checked');
                break;
        }
    });
};

function checkBoxState(checkBox)
{
    return $(checkBox).prop('indeterminate') ? 'indeterminate' : $(checkBox).prop('checked') ? 'checked' : 'unchecked';
}
