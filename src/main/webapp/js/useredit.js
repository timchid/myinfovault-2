'use strict';
var restrictedRoles = [ 'SCHOOL_STAFF', 'VICE_PROVOST_STAFF', 'SENATE_STAFF' , 'SYS_ADMIN'];

/*
 * Manage dynamic updates to the Add and Edit User page.
 */
$(document).ready(function() {
    var primaryHeader = $('h2:first');
    var jointHeader = $('h2:nth-of-type(2)').toggle($('div.jointline').length > 0);
    var addButton = $('#additem');
    var deleteButtons = $('.deleteAppointment');
    var roleSelect = $('#selectedRole');
    var homeDepartmentAcademicSenateOption = $('select[name=homeDepartment\\.scope] option[value="42:500"]');

    // stow all and scoped sets of department options
    var all_department_options = $('#alldepts').remove().find('option');
    var scoped_department_options = $('#scopeddepts').remove().find('option');
    var user_scoped_department_options = $('#userscopeddepts').remove().find('option');

    // stow currently selected role
    var currentRole = roleSelect.val();

    // stow the original role the user had (when the page loaded, NOT before they were chosen for edit)
    var originalRole = roleSelect.val();

    // stow any additional department formlines in case we remove them for role change
    var oldVal = [];
    $('select.additionaldept :selected').each(function(i, selected){
        oldVal[i] = $(selected).val();
    });

    /*
     * Update headers, buttons, and department selection lists on role change.
     */
    roleSelect.change(function(event) {
        var selectedRole = $(event.currentTarget).val();

        var bCandidate = selectedRole === 'CANDIDATE';

        // Set the text to use based on the role chosen.
        var appointment = (bCandidate ? 'Appointment' : 'Assignment');

        // can assign if selected role not restricted
        var canAssign = $.inArray(selectedRole, restrictedRoles) < 0;

        // update primary header
        primaryHeader.text('Primary ' + appointment);

        // update joint header text, show if there are joint lines
        jointHeader.text((bCandidate ? 'Joint ' : 'Additional ') + appointment + 's')
                   .toggle($('div.jointline').length > 0);

        // show or hide addButton, based on the role and number of additional depts.
        addButton.val('Add ' + appointment)
                 .attr('title', 'Add Another ' + appointment)
                 .toggle(canAssign);

        // update delete button text
        deleteButtons.attr('title', 'Delete This ' + appointment);

        var department_options;
        if (!bCandidate && $('form').is('.CANDIDATE')) {
            department_options = user_scoped_department_options;
        }
        else {
            department_options = scoped_department_options;
        }
        var selectOptions = bCandidate ? all_department_options : department_options;

        // here we setup any additional departments and either reset the selected value to NONE (0:0)
        // OR if this is a second time through and the user is going back to the original role
        // reset the additional departments to the original selections
        // Additional (Joint) departments
        $('select.additionaldept').each(function(index, el) {
            el = $(el);
            var newVal = (selectedRole === originalRole ? oldVal[index] : '0:0');
            el.empty()
              .append(selectOptions.clone())
              .val(newVal)
              .prop('disabled', !canAssign);
        });

        //hide jointline if  currentRole was Academic and selectedRole isn't OR
        //  if !canAssign (because these roles can't get joint assignments)
        if (selectedRole !== currentRole) {
        	if (!canAssign || (!bCandidate && currentRole === 'CANDIDATE')) {
        		$('div.jointline').hide();
        	}
    		else {
        		$('div.jointline').show();
        	}
        }

        // now hide the header if the lines are not visible
        var jointlineVisible = $('div.jointline').is(':visible');
        jointHeader = $('h2:nth-of-type(2)').toggle($('div.jointline').length > 0 && jointlineVisible);

        // update assignment/appointment options class
        $('span.options').removeClass(currentRole)
                         .addClass(selectedRole);

        // set default primary department for senate staff
        homeDepartmentAcademicSenateOption.prop('selected', selectedRole === 'SENATE_STAFF');

        // update current role to selected for future role changes
        currentRole = selectedRole;
    })
    // set default role selection
    .change();
});
