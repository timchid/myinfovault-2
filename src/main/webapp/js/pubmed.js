'use strict';
/*
 * PubMed form and select page handlers
 *
 * Depends:
 *     /js/juqery-ui.plugins.js
 */
$(document).ready(function() {
    //setup collapsible field sets
    $('fieldset').collapsible();

    //from and to year disabled when custom year range not selected
    $('input[name=yearRange]').change(function(event) {
        $('select[name$=Year]').prop('disabled', $(this).val()*1 !== 0);
    });
    $('input[name=yearRange][value=3]').trigger('change');

    // form validation on search button click
    $('button[name=_eventId][value=search]').click(function(event) {
        // holds form error messages
        var errors = [];

        // get form input values
        var surname = $.trim($('input[name=surname]').val());
        var givenInitial = $.trim($('input[name=givenInitial]').val());
        var middleInitial = $.trim($('input[name=middleInitial]').val());

        //check surname exists
        if (!surname) {
            errors.push('Last name is required');
        }

        //check given initial exists
        if (!givenInitial) {
            errors.push('First initial is required');
        }
        //check given initial is one alphabet character
        else if (!/^[a-zA-Z]?$/.test(givenInitial)) {
            errors.push('First initial must be a letter');
        }

        //check middle initial is one alphabet character if exists
        if (middleInitial && !/^[a-zA-Z]?$/.test(middleInitial)) {
            errors.push('Middle initial must be a letter');
        }

        // if there's at least one error
        if (errors.length) {
            // stop form submission
            event.preventDefault();

            //select error list
            var errorList = $('.errormessage ul');

            // if an error list already exists in the DOM
            if (errorList.size() > 0) {
                // remove previous errors
                errorList.empty();
            }
            //else, create error message container and list
            else {
                errorList = $('<ul/>');

                $('<div/>').addClass('errormessage')
                           .append($('<strong/>').text('ERROR'))
                           .append(errorList)
                           .insertAfter('h1');
            }

            // build the list of error messages
            for (var i=0; i<errors.length; i++) {
                errorList.append($('<li/>').append(errors[i]));
            }
        }
    });

    //clear all
    $('button#clearall').click(function(event) {
        event.preventDefault();

        $('input:checkbox').prop('checked', false);
    });

    //check all
    $('button#selectall').click(function(event) {
        event.preventDefault();

        $('input:checkbox').prop('checked', true);
    });

    //collapse large citations
    $('cite').moreless({
        bOpen : false
    });
});
