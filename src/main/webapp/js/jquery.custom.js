/**
 * jQuery Custom functions
 */

/**
 * if ($(selector).exists()) { }
 */
jQuery.fn.exists = function(){return this.length > 0;};


/**
 * if ($.exists(selector)) { }
 */
jQuery.exists = function(selector) {return $(selector).length > 0;};