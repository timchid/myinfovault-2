'use strict';
/* global fnInitDataTable */
/*
 * VetMed import script for /WEB-INF/jsp/vetmedimport.jsp
 */
 var oInvalid;
$(document).ready(function() {
    var oProcessed = fnInitDataTable('processed', {'iDisplayLength' : 10});
    oInvalid = fnInitDataTable('invalid', {'iDisplayLength' : 10});
    fnInitDataTable('persisted', {'iDisplayLength' : 10});

    // add select all check box to record check box column header
    oProcessed.find('thead > tr:first-child > th:first-child').html(
        $('<input/>').prop('type', 'checkbox')
                     .prop('title', 'Select all records')
                     .change(function() {
                         $('input[type=checkbox]', oProcessed.fnGetNodes()).prop('checked', $(this).is(':checked'));
                     })
    );

    // on submit of the processed records form, add checked checkboxes to the form post
    oProcessed.closest('form').submit(function() {
        $('input:checked', oProcessed.fnGetHiddenTrNodes()).prop('type', 'hidden').appendTo(this);
    });

    // add CSV data to invalid link
    $('#invalidLink').prop('href', 'data:text/csv;charset=UTF-8,' + encodeURIComponent(getCSV(oInvalid)));
});

/*
 * Get CSV from datatable.
 */
function getCSV(oTable) {
    var csv = '';

    var fnGetCell = function(index, value) {
        csv += $(value).text() + ',';
    };

    $('thead tr th', oInvalid).each(fnGetCell);

    $(oInvalid.fnGetNodes()).each(function(index, value) {
        csv += '\n';

        $('td', value).each(fnGetCell);
    });

    return csv;
}
