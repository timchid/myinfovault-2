<%@ tag body-content="scriptless" %><%--
--%><%@ attribute name="required" required="false" %><%--
--%><%@ attribute name="roles" required="false" %><%--
--%><%@ attribute name="allow" required="false" %><%--
--%><%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ tag import="edu.ucdavis.myinfovault.*" %><%--
--%><%

 /* Get the current user's (old-style) access level */
 //AccessLvl l = ((MIVSession)session.getAttribute("MIVSESSION")).getUser().getAccess();
 AccessLvl l = ((MIVSession)session.getAttribute("MIVSESSION")).getUser().getTargetUserInfo().getAccess();
 /* and any parameters provided: required or roles, use 'roles' if both found */
 String required = (String)jspContext.findAttribute("required");
 String roles    = (String)jspContext.findAttribute("roles");

 /* Test here for the params set. */
 /* For the moment we'll just pretend it's always "required" */

 /* 'roles' should allow comma, plus, and parentheses; such as:
      "MIVADMIN,((DEPTADMIN,SCHOOLADMIN)+HELPER)"
    which would mean "allow if the user has the MIVAdmin role,
    and allow if user has either Dept. or School admin along
    with the 'helper' role.  Not a very good example in terms
    of the current meanings of access levels, but you get
    the picture.
 */

 AccessLvl testlvl = AccessLvl.parse(required);
 int result = l.compareTo(testlvl);
 if (result >= 0) {
 /**/    out.write("<!-- You're Allowed");
 /**/    if (result > 0) {
 /**/        out.write(", with "+result+" level(s) to spare");
 /**/    }
 /**/    out.println("! -->");
%><div style="background-color: #e7ffe7;"><jsp:doBody/></div><%
 }
 else {
    /* In Real-Life we won't print anything here... there'd be no "else" */
    out.println("<div style=\"color:red; font-weight:bold;\">Request Denied!<br/>");
    out.println("You need "+testlvl+" to see this, but you only have "+l+"<br/>");
    out.println("You are "+(-result)+" Levels too low.</div>");
    out.write("<!-- Not Allowed: Need level "+testlvl+
                ", Have level "+l+" : "+(-result)+" Levels too low. -->");
 }
%>
