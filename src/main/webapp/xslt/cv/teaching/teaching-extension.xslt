<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="teaching-extension">
        <fo:block id="extension" space-before="12pt" margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="extension-record">
            <fo:block>
                <fo:table table-layout="fixed" width="100%" space-after="12pt">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>
                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="extension-record"/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>

    <xsl:template match="extension-record">
       <fo:table-row keep-together.within-page="1">
       		<fo:table-cell>
               <fo:block>
                   <xsl:apply-templates select="year" mode="teaching-extension"/>
               </fo:block>
           </fo:table-cell>
           <fo:table-cell>
               <fo:block>
                   <xsl:apply-templates select="coursenumber" mode="teaching-extension"/>               
                   <xsl:apply-templates select="title" mode="teaching-extension"/>              
                   <xsl:apply-templates select="units" mode="teaching-extension"/>               
                   <xsl:apply-templates select="duration" mode="teaching-extension"/>               
                   <xsl:apply-templates select="locationdescription" mode="teaching-extension"/>
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
   
   <xsl:template match="year" mode="teaching-extension">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="coursenumber" mode="teaching-extension">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="title" mode="teaching-extension">
        <fo:inline>, <xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="units" mode="teaching-extension">
        <fo:inline>, <xsl:apply-templates/> Units</fo:inline>
    </xsl:template>
    <xsl:template match="duration" mode="teaching-extension">
        <fo:inline>, <xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="locationdescription" mode="teaching-extension">
        <fo:inline>, <xsl:apply-templates/></fo:inline>
    </xsl:template>

</xsl:stylesheet>
