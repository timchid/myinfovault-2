<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY dash "-">
 <!ENTITY space " ">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="teaching-thesis">
        <fo:block id="thesis" space-before="12pt" margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="thesis-record">
            <fo:block>
                <fo:table table-layout="fixed" width="100%" space-after="12pt">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>

                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="thesis-record"/>

                    </fo:table-body>
                </fo:table>
            </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>

    <xsl:template match="thesis-record">
       <fo:table-row keep-together.within-page="1">
	       	<fo:table-cell>
	                <fo:block>
	                    <xsl:apply-templates select="startyear" mode="teaching"/>&dash;<xsl:apply-templates select="endyear" mode="teaching"/>
	                </fo:block>
	            </fo:table-cell>
	      	 <fo:table-cell>
           <fo:block>
              <xsl:apply-templates select="studentname" mode="teaching"/>
              <xsl:apply-templates select="committeechair" mode="teaching"/>
              <xsl:apply-templates select="cochair" mode="teaching"/>
              <xsl:apply-templates select="committeemember" mode="teaching"/>
              <xsl:apply-templates select="doctoraldegree" mode="teaching"/>
              <xsl:apply-templates select="doctoratedegree" mode="teaching"/>
              <xsl:apply-templates select="mastersdegree" mode="teaching"/>
              <xsl:apply-templates select="degreeprogress" mode="teaching"/>
              <xsl:apply-templates select="degreeawarded" mode="teaching"/>
              <xsl:apply-templates select="position" mode="teaching"/>
           </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
   
   <xsl:template match="studentname" mode="teaching">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>

   <xsl:template match="committeechair" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, Chair</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="cochair" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, Co-Chair</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="committeemember" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, Member</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="doctoraldegree" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, Ph.D.</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="doctoratedegree" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, Doctorate</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="mastersdegree" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, Masters</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="degreeprogress" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, In Progress</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="degreeawarded" mode="teaching">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">, Awarded</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="position" mode="teaching">
      <fo:inline>, <xsl:apply-templates /></fo:inline>
   </xsl:template>

</xsl:stylesheet>
