<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="teaching-contact-hours">
        <fo:block id="contact-hours" space-before="12pt" margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="contact-hours-record">
            <fo:block>
                <fo:table table-layout="fixed" width="100%" space-after="12pt">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>
                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="contact-hours-record"/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>

    <xsl:template match="contact-hours-record">
        <fo:table-row>
        		<fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="term" mode="teaching"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="lecture" mode="teaching"/><xsl:if test="(discussion or lab or clinic) and lecture">, </xsl:if>
                    <xsl:apply-templates select="discussion" mode="teaching"/><xsl:if test="(lab or clinic) and discussion">, </xsl:if>
                    <xsl:apply-templates select="lab" mode="teaching"/><xsl:if test="clinic and lab">, </xsl:if>
                    <xsl:apply-templates select="clinic" mode="teaching"/>
                </fo:block>
            </fo:table-cell>            
        </fo:table-row>
    </xsl:template>
    <xsl:template match="term" mode="teaching">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="lecture" mode="teaching">
        <fo:inline>Lecture=<xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="discussion" mode="teaching">
        <fo:inline>Discussion=<xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="lab" mode="teaching">
        <fo:inline>Lab=<xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="clinic" mode="teaching">
        <fo:inline>Clinic=<xsl:apply-templates/></fo:inline>
    </xsl:template>
    
</xsl:stylesheet>
