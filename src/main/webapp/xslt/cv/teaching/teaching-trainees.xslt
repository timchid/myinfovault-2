<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY colon ":">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="teaching-trainees">
        <fo:block id="teaching-trainees" space-before="12pt" margin-left="{$content.indent}">
          <xsl:call-template name="subsection-header">
            <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
          </xsl:call-template>
          <xsl:if test="trainee-record">
                   <fo:block>
                <fo:table table-layout="fixed" width="100%" space-after="12pt">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>

                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="trainee-record"/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
          </xsl:if>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="trainee-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="year" mode="teaching"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>                   
                   <xsl:apply-templates select="name" mode="teaching-trainee"/>
                   <xsl:apply-templates select="degree" mode="teaching-trainee"/>
                   <xsl:apply-templates select="type" mode="teaching-trainee"/>
                   <xsl:apply-templates select="position" mode="teaching-trainee"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template match="name" mode="teaching-trainee">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="degree" mode="teaching-trainee">
      <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="type" mode="teaching-trainee">
      <fo:inline>&comma;&space;<xsl:apply-templates />
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="position" mode="teaching-trainee">
      <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
   </xsl:template>
</xsl:stylesheet>
