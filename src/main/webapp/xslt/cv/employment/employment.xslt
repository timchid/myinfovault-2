<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 ]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="employment">
        <fo:block  margin-left="{$content.indent}">
        		<xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="employment-record">
            <fo:table table-layout="fixed" width="100%" space-after="12pt">
                <fo:table-column column-width="40%"/>
                <fo:table-column column-width="60%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="employment-record"/>
                </fo:table-body>
            </fo:table>            
            </xsl:if>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="employment-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="startdate" mode="employment" />-<xsl:apply-templates select="enddate" mode="employment" />
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="institution" mode="employment" />
                    <xsl:apply-templates select="location" mode="employment" />
                    <xsl:apply-templates select="title" mode="employment" />
<!--MIV-1739 removing the salary info from the preview                    <xsl:apply-templates select="salary" mode="employment" />-->
										<xsl:apply-templates select="remark" mode="employment" />
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template match="startdate" mode="employment">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="enddate" mode="employment">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="institution" mode="employment">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="location" mode="employment">
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="title" mode="employment">
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="remark" mode="employment">
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
<!--    <xsl:template match="salary" mode="employment">-->
<!--        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>-->
<!--    </xsl:template>-->
</xsl:stylesheet>
