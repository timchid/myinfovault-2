<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="works-completed|works-scheduled|works-submitted">
    <fo:block space-before="12pt" margin-left="{$content.indent}">
      <fo:block xsl:use-attribute-sets="subheader.format" space-after="12pt" keep-with-next="1">
        <xsl:value-of select="section-header"/>
      </fo:block>
      <xsl:apply-templates select="works-record" />
    </fo:block>
  </xsl:template>

  <!-- Works -->
  <xsl:template  match="works-record" name="works-record">    
    <fo:block padding-bottom="2pt">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="10%" />
        <fo:table-column column-width="90%" />
        <fo:table-body start-indent="0pt">
          <fo:table-row>
            <fo:table-cell>
              <fo:block text-align="left">
                <xsl:apply-templates select="workyear" />
              </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block text-align="left">
                  <xsl:apply-templates select="worktypedescription" />
                  <xsl:apply-templates select="creators" />
                  <xsl:apply-templates select="title" />
                </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block></fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                <xsl:apply-templates select="description" />
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block></fo:block>
            </fo:table-cell>
            <xsl:choose>
                <xsl:when test="link">
              <fo:table-cell text-align="left">
                <xsl:apply-templates select="link"> 
                  <xsl:with-param name="urldescription" select="'View Work Information'" />
                  <xsl:with-param name="urlvalue"><xsl:value-of select="link"/></xsl:with-param>
                </xsl:apply-templates>
              </fo:table-cell>
                </xsl:when>
                <xsl:otherwise>
                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>
                </xsl:otherwise>
            </xsl:choose>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>
