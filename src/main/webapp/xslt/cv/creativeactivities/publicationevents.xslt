<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="publicationevents-published|publicationevents-in-press|publicationevents-submitted">
    <fo:block space-before="12pt" margin-left="{$content.indent}">
      <fo:block xsl:use-attribute-sets="subheader.format" space-after="12pt" keep-with-next="1">
        <xsl:value-of select="section-header"/>
      </fo:block>
      <xsl:apply-templates select="publicationevents-record" />
    </fo:block>
  </xsl:template>

  <!-- Events -->
  <xsl:template match="publicationevents-record" name="publicationevents-record">
    <fo:block padding-bottom="2pt">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="10%" />
        <fo:table-column column-width="90%" />
        <fo:table-body start-indent="0pt">
          <fo:table-row>
            <!-- Don't use <number> elements since they are sequential for the record type and -->    
            <!-- we want the numbering to restart for each new type.-->    
              <fo:table-cell>
              <fo:block text-align="left">
                <xsl:apply-templates select="year" />
              </fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block text-align="left">
                <xsl:apply-templates select="title" />
                <xsl:apply-templates select="publication" />
                <xsl:apply-templates select="editors" />
                <xsl:apply-templates select="volume" />
                <xsl:apply-templates select="issue" />
                <xsl:apply-templates select="pages" />
                <xsl:apply-templates select="publisher" />
                <xsl:apply-templates select="city" />
                <xsl:apply-templates select="province" />
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block></fo:block>
            </fo:table-cell>
            <xsl:choose>
              <xsl:when test="link">
                <fo:table-cell text-align="left">
                  <xsl:apply-templates select="link">
                    <xsl:with-param name="urldescription" select="'View Publication Information'" />
                    <xsl:with-param name="urlvalue"><xsl:value-of select="link" /></xsl:with-param>
                  </xsl:apply-templates>
                </fo:table-cell>
              </xsl:when>
              <xsl:otherwise>
                <fo:table-cell>
                  <fo:block></fo:block>
                </fo:table-cell>
              </xsl:otherwise>
            </xsl:choose>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>
