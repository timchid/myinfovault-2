<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="course-evaluations">
        <xsl:if test="evaluation-record">
        <fo:block margin-left="{$content.indent}">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="evaluation-record"/>
                </fo:table-body>
            </fo:table>            
        </fo:block>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="evaluation-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="year" mode="evaluations" />
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="course" mode="evaluations"/>
                    
                    <xsl:apply-templates select="description" mode="evaluations"/>
                    <fo:block>                    
                    	<xsl:apply-templates select="enrollment" mode="evaluations"/>
                        <xsl:apply-templates select="responsetotal" mode="evaluations"/>
                        <xsl:apply-templates select="percentageofreturn" mode="evaluations"/>
                        <xsl:apply-templates select="type" mode="evaluations"/>
                        <xsl:apply-templates select="instructorscore" mode="evaluations"/>
                        <xsl:apply-templates select="coursescore" mode="evaluations"/>
                    </fo:block>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template match="year" mode="evaluations">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="course" mode="evaluations">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="description" mode="evaluations">
        <fo:inline>: <xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="enrollment" mode="evaluations">
        <fo:inline>Total Enrollment: <xsl:apply-templates />
        	<xsl:if test="../responsetotal or ../percentageofreturn">&comma;</xsl:if>
        </fo:inline>
    </xsl:template>
    <xsl:template match="responsetotal" mode="evaluations">
        <fo:inline>&space;Total Responses: <xsl:apply-templates />
        	<xsl:if  test="../percentageofreturn">&comma;</xsl:if>
	</fo:inline>
    </xsl:template>
    <xsl:template match="percentageofreturn" mode="evaluations">
        <fo:inline>&space; % of Return: <xsl:apply-templates />%</fo:inline>
    </xsl:template>
    <xsl:template match="type" mode="evaluations">
        <fo:inline>&space;(<xsl:apply-templates />)</fo:inline>
    </xsl:template>
    <xsl:template match="instructorscore" mode="evaluations">
        <fo:inline>, Instructor Score: <xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="coursescore" mode="evaluations">
        <fo:inline>, Course Score: <xsl:apply-templates /></fo:inline>
    </xsl:template>
</xsl:stylesheet>
