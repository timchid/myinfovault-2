<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY comma ",">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="honors">
        <fo:block margin-left="{$content.indent}">
<!--           <xsl:call-template name="subsection-header">-->
<!--                 <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>-->
<!--            </xsl:call-template>-->
            <xsl:if test="honor-record">
            <fo:table table-layout="fixed" width="100%" space-after="12pt">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="honor-record"/>
                </fo:table-body>
            </fo:table>
			</xsl:if>		
        </fo:block>
    </xsl:template>

    <!-- HONORS AND AWARDS -->
    <xsl:template match="honor-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="year" mode="honors"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="description" mode="honors"/>
                    <xsl:apply-templates select="remark" mode="honors"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <!-- HONOR DATA TAGS -->
    <xsl:template match="year" mode="honors">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
    
    <xsl:template match="description" mode="honors">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
    <xsl:template match="remark" mode="honors">
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    

</xsl:stylesheet>
