<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <xsl:template match="position-description">
        <xsl:if test="position-record">
        <fo:block margin-left="{$content.indent}">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>		           	
                <fo:table-body start-indent="0pt">
                		<fo:table-row text-align="left" font-weight="bold" text-decoration="underline">
			              <fo:table-cell padding-bottom="12pt">
			                 <fo:block>Time</fo:block>
			              </fo:table-cell>
			              <fo:table-cell padding-bottom="12pt">
			                 <fo:block>Description</fo:block>
			              </fo:table-cell>
			            </fo:table-row>
                    <xsl:apply-templates select="position-record"/>
                </fo:table-body>
            </fo:table>            
        </fo:block>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="position-record">
        <fo:table-row>
            <fo:table-cell padding-bottom="12pt">
                <fo:block>
                    <xsl:apply-templates select="percenttime" mode="posdescription"/> %
                </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-bottom="12pt">
                <fo:block>
                    <xsl:apply-templates select="pdcontent" mode="posdescription"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

	<xsl:template match="percenttime" mode="posdescription">
		<xsl:apply-templates/>
    </xsl:template>

	<xsl:template match="pdcontent" mode="posdescription">
		<xsl:apply-templates/>
    </xsl:template>


</xsl:stylesheet>