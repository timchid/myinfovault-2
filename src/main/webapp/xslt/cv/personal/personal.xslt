<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
<!ENTITY space '&#160;'>
<!ENTITY comma ",">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- PERSONAL -->

    <xsl:template match="personal">
        <fo:block margin-left="{$content.indent}">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="30%"/>
                <fo:table-column column-width="70%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="personal-record"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="personal-email">
        <xsl:if test="$email='true'">
            <fo:block margin-left="{$content.indent}">
                <fo:table table-layout="fixed" width="100%">
                    <fo:table-column column-width="30%"/>
                    <fo:table-column column-width="70%"/>
                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="email-record"/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </xsl:if>
    </xsl:template>   
 
    <xsl:template match="address-record" mode="perm">
    	<xsl:if test="$permanent_address='true'">
        <xsl:call-template name="address">
        	<xsl:with-param name="addresstype">Permanent Address</xsl:with-param>
        </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <xsl:template match="address-record" mode="curr">
    	<xsl:if test="$current_address='true'">
        <xsl:call-template name="address">
        	<xsl:with-param name="addresstype">Current Address</xsl:with-param>
        </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <xsl:template match="address-record" mode="work">
    	<xsl:if test="$office_address='true'">
        <xsl:call-template name="address">
        	<xsl:with-param name="addresstype">Work Address</xsl:with-param>
        </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="address">
		 	<xsl:param name="addresstype" />
		 	<fo:block margin-left="{$content.indent}">
		     <fo:table table-layout="fixed" width="100%">
		         <fo:table-column column-width="30%"/>
		         <fo:table-column column-width="70%"/>
		         <fo:table-body start-indent="0pt">
		             <fo:table-row>
		                 <fo:table-cell>
		                     <fo:block font-weight="bold" ><xsl:value-of select="$addresstype" />:</fo:block>
		                 </fo:table-cell>		                 
                     <xsl:apply-templates select="street1" mode="personal"/>
		             </fo:table-row>
								 <xsl:apply-templates select="street2" mode="personal"/>
								 <xsl:if test="city or state or zipcode">							 
		             <fo:table-row>
		                 <fo:table-cell>
		                     <fo:block>&space;</fo:block>
		                 </fo:table-cell>
		                 <fo:table-cell>
		                     <fo:block>
		                         <fo:inline>
		                           <xsl:apply-templates select="city" mode="personal"/>
		                           <xsl:apply-templates select="state" mode="personal"/>
		                           <xsl:apply-templates select="zipcode" mode="personal"/>
		                         </fo:inline>
		                     </fo:block>
		                 </fo:table-cell>
		             </fo:table-row>
		             </xsl:if>	
		         </fo:table-body>
		     </fo:table>
		 	</fo:block>
 </xsl:template>

    <xsl:template name="permphone">
    	<xsl:param name="value" />
    	<xsl:if test="$permanent_phone='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Phone</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>        	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
    <xsl:template name="currphone">
    	<xsl:param name="value" />
    	<xsl:if test="$current_phone='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Phone</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>      	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
    <xsl:template name="workphone">
    	<xsl:param name="value" />
    	<xsl:if test="$office_phone='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Phone</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>         	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
    <xsl:template name="cellphone">
    	<xsl:param name="value" />
    	<xsl:if test="$cell_phone='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Cell Phone</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>    	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
    <xsl:template name="permfax">
    	<xsl:param name="value" />
    	<xsl:if test="$permanent_fax='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Fax</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>         	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
    <xsl:template name="currfax">
    	<xsl:param name="value" />
    	<xsl:if test="$current_fax='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Fax</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>      	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
    <xsl:template name="workfax">
    	<xsl:param name="value" />
    	<xsl:if test="$office_fax='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Fax</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>      	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
    <xsl:template name="website">
    	<xsl:param name="value" />
    	<xsl:if test="$link='true'">
        <xsl:call-template name="phone-record">
        <xsl:with-param name="phonelabel">Web Site</xsl:with-param>
        <xsl:with-param name="value"><xsl:value-of select="$value" /></xsl:with-param>      	
        </xsl:call-template>
      </xsl:if>
    </xsl:template>
   
    

    <xsl:template match="personal" mode="birthdate">
      <xsl:apply-templates select="personal-record" mode="birthdate"/>
    </xsl:template>

    <xsl:template match="personal" mode="citizen">
      <xsl:apply-templates select="personal-record" mode="citizen"/>
    </xsl:template>

    <xsl:template match="personal" mode="dateentered">
      <xsl:apply-templates select="personal-record" mode="dateentered"/>
    </xsl:template>

    <xsl:template match="personal" mode="visatype">
      <xsl:apply-templates select="personal-record" mode="visatype"/>
    </xsl:template>

    <xsl:template name="phone-record">
    		<xsl:param name="phonelabel" />
    		<xsl:param name="value" />        
         <fo:block margin-left="{$content.indent}">
             <fo:table table-layout="fixed" width="100%">
                 <fo:table-column column-width="30%"/>
                 <fo:table-column column-width="70%"/>
                 <fo:table-body start-indent="0pt">
                     <fo:table-row>
                         <fo:table-cell>
                             <fo:block font-weight="bold"><xsl:value-of select="$phonelabel" />:</fo:block>
                         </fo:table-cell>
                         <fo:table-cell>
                             <fo:block>
                                 <xsl:value-of select="$value" />
                             </fo:block>
                         </fo:table-cell>
                     </fo:table-row>
                 </fo:table-body>
             </fo:table>
         </fo:block>
    </xsl:template>

    <xsl:template match="email-record">
      <xsl:if test="$email = 'true'" > 
        <fo:table-row>
            <fo:table-cell>
                <fo:block font-weight="bold">Email:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="value"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
      </xsl:if> 
    </xsl:template>  
    
    <xsl:template match="personal-record" mode="birthdate">
        <xsl:if test="$birthDate = 'true'" > 
        <fo:block margin-left="{$content.indent}">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="30%"/>
                <fo:table-column column-width="70%"/>
                <fo:table-body start-indent="0pt">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-weight="bold">Date of Birth:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="birthdate" mode="personal"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
            </fo:block>
            </xsl:if>
    </xsl:template>
    
    <xsl:template match="personal-record" mode="citizen">
        <xsl:if test="$citizen = 'true'" > 
            <fo:block margin-left="{$content.indent}">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="30%"/>
                <fo:table-column column-width="70%"/>
                <fo:table-body start-indent="0pt">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-weight="bold">U.S Citizen:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="citizen" mode="personal"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
        </xsl:if>
    </xsl:template>

    <xsl:template match="personal-record" mode="dateentered">
        <xsl:if test="$date_entered = 'true'" > 
            <fo:block margin-left="{$content.indent}">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="30%"/>
                <fo:table-column column-width="70%"/>
                <fo:table-body start-indent="0pt">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-weight="bold">Date Arrived:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="dateentered" mode="dateentered"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
        </xsl:if>
    </xsl:template>

    <xsl:template match="personal-record" mode="visatype">
        <xsl:if test="$visa_type = 'true'" > 
            <fo:block margin-left="{$content.indent}">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="30%"/>
                <fo:table-column column-width="70%"/>
                <fo:table-body start-indent="0pt">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-weight="bold">Type of Visa:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="visatype" mode="visatype"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="street1" mode="personal">
    	<fo:table-cell>
         <fo:block>
             <xsl:apply-templates/>
         </fo:block>
     </fo:table-cell>
        
    </xsl:template>
    <xsl:template match="street2" mode="personal">
    	<fo:table-row>
          <fo:table-cell>
              <fo:block>&space;</fo:block>
          </fo:table-cell>
          <fo:table-cell>
              <fo:block>
                  <xsl:apply-templates/>
              </fo:block>
          </fo:table-cell>
      </fo:table-row>        
    </xsl:template>
    <xsl:template match="city" mode="personal">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="state" mode="personal">
    	<fo:inline>&comma;&space;<xsl:apply-templates/></fo:inline>        
    </xsl:template>
    <xsl:template match="zipcode" mode="personal">
        <fo:inline>&space;<xsl:apply-templates/></fo:inline> 
    </xsl:template>
    <xsl:template match="number" mode="personal">
        <fo:block>
            <xsl:value-of select="following-sibling::number[position()=1]"/>
        </fo:block>
    </xsl:template>
    <xsl:template match="birthdate" mode="personal">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="citizen" mode="personal">
        <xsl:choose>
            <xsl:when test="text()= '1'">
                Yes
            </xsl:when>
            <xsl:otherwise>
                No
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
