<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 <!ENTITY inpress "** IN PRESS **">
 <!ENTITY submitted "** SUBMITTED **">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="patent-granted|patent-filed|disclosure">
        <fo:block id="{local-name()}" space-before="12pt" margin-left="{$content.indent}">
          <xsl:call-template name="subsection-header">
             <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
          </xsl:call-template>
          <xsl:apply-templates select="patent-record" />
        </fo:block>
    </xsl:template>
    
    <!-- PATENTS -->
    <xsl:template match="patent-record" >
           <fo:block padding-bottom="12pt">
            <!-- table-layout="auto" is currently not supported by FOP  -->
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body start-indent="0pt">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="year" />
                            </fo:block>    
                        </fo:table-cell>
                        <fo:table-cell>                           
                           <fo:block>
                                   <fo:block>
                                       <xsl:apply-templates select="author" />
                                       <xsl:text>&space;</xsl:text>
                                       <xsl:apply-templates select="title" />
                                   </fo:block>
                                   <fo:block>    
                                       <xsl:apply-templates select="patentdetail" />
                                       <xsl:apply-templates select="patentinfo" />
                                </fo:block>
                               
                               <xsl:if test="licensing">
                                   <fo:block space-before="1em">
                                           <xsl:apply-templates select="licensing" />
                                   </fo:block>
                               </xsl:if>
                           </fo:block>                              
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
   </xsl:template>
   
   <xsl:template match="year">
        <fo:inline>
            <xsl:apply-templates />
        </fo:inline>
   </xsl:template>
   
   <xsl:template match="title">
        <xsl:variable name="value" select="." />
        <xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
        <xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
        <xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
        <xsl:variable name="hasPunctuation">
            <xsl:choose>
                <!-- Original -->
                <!-- xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz?!&quote;','ABCDEFGHIJKLMNOPQRSTUVWXYZ- - -')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..' )">1</xsl:when -->
                <!-- First Look -->
                <!-- xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'','')!='_.') or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz?!&quote;','ABCDEFGHIJKLMNOPQRSTUVWXYZ- - -')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..' )">1</xsl:when -->
                <!-- Match the TitleFormatter logic -->
                <xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <fo:inline>
            <xsl:choose>
                <xsl:when test="parent::patent-record">
                    <xsl:apply-templates /><xsl:if test="$hasPunctuation != '1' and $lastCharacter != '&qmark;' and $lastCharacter != '&expoint;' and $lastCharacter != '&quote;'">&period;</xsl:if></xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates />
                </xsl:otherwise>
            </xsl:choose>
        </fo:inline>
    </xsl:template>
    
    <xsl:template match="author">
        <xsl:variable name="value" select="." />
        <xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
        <xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
        <xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
        <xsl:variable name="hasPunctuation">
            <xsl:choose>
            <xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <fo:inline>
            <xsl:choose>
                <xsl:when test="parent::patent-record">
                    <xsl:apply-templates /><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if></xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates />
                </xsl:otherwise>    
            </xsl:choose>
        </fo:inline>
    </xsl:template>
    
    <xsl:template match="patentinfo" >
        <fo:inline>
               <xsl:text>&space;</xsl:text>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
    
    <xsl:template match="patentdetail" >
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
   
    <xsl:template match="licensing" >
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
</xsl:stylesheet>
