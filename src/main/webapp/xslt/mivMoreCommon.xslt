<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 ]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<!-- Strip space nodes within all elements -->
	<xsl:strip-space elements="*" />

	<xsl:template name="nameblock">
		<fo:block font-size="16pt" font-weight="bold" text-align="center"
			space-after="12pt">
			<xsl:apply-templates select="//fname" />
			<xsl:apply-templates select="//lname" />
		</fo:block>
	</xsl:template>

	<xsl:template match="fname">
		<fo:inline space-end="0.5em">
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="lname">
		<fo:inline>
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<!-- Add ending punctuation to input value. -->
	<xsl:template name="addPunctuation">
		<xsl:param name="value" />
		<xsl:param name="punctuationValue" />
		<xsl:param name="followWith" />
		<!-- Check if there is already ending punctuation and only add if any of 
			the -->
		<!-- following are not present -->
		<xsl:variable name="lastCharacter"
			select="substring($value, string-length($value))" />
		<xsl:choose>
			<xsl:when test="$lastCharacter = '&period;'">
			</xsl:when>
			<xsl:when test="$lastCharacter = '&comma;'">
			</xsl:when>
			<xsl:when test="$lastCharacter = '&qmark;'">
			</xsl:when>
			<xsl:when test="$lastCharacter = '&expoint;'">
			</xsl:when>
			<!-- No punctuation ending the field, add the input punctuation. -->
			<xsl:otherwise>
				<xsl:value-of select="$punctuationValue" />
			</xsl:otherwise>
		</xsl:choose>
		<!-- Always add any value specified to follow the punctuation. -->
		<xsl:value-of select="$followWith" />
	</xsl:template>

	<!-- Check for greek characters in the input string and set font size, family, 
		weight and style accordingly for output. Regex would be the preferable way 
		to accomplish this, but requires an upgrade to the XSL 2.0 specification. -->
	<xsl:template name="checkForGreekSymbols">
		<xsl:param name="length" />
		<xsl:param name="index" />
		<xsl:if test="$index &lt;= $length">
			<xsl:variable name="inputString" select="." />
			<xsl:variable name="currentChar" select="substring($inputString,$index,1)" />
			<xsl:choose>
				<xsl:when
					test="contains($currentChar,'&#913;') or
                               contains($currentChar,'&#914;') or
                               contains($currentChar,'&#915;') or
                               contains($currentChar,'&#916;') or
                               contains($currentChar,'&#917;') or
                               contains($currentChar,'&#918;') or
                               contains($currentChar,'&#919;') or
                               contains($currentChar,'&#920;') or
                               contains($currentChar,'&#921;') or
                               contains($currentChar,'&#922;') or
                               contains($currentChar,'&#923;') or
                               contains($currentChar,'&#924;') or
                               contains($currentChar,'&#925;') or
                               contains($currentChar,'&#926;') or
                               contains($currentChar,'&#927;') or
                               contains($currentChar,'&#928;') or
                               contains($currentChar,'&#929;') or
                               contains($currentChar,'&#930;') or
                               contains($currentChar,'&#931;') or
                               contains($currentChar,'&#932;') or
                               contains($currentChar,'&#933;') or
                               contains($currentChar,'&#934;') or
                               contains($currentChar,'&#935;') or
                               contains($currentChar,'&#936;') or
                               contains($currentChar,'&#937;') or
                               contains($currentChar,'&#938;') or
                               contains($currentChar,'&#939;') or
                               contains($currentChar,'&#940;') or
                               contains($currentChar,'&#941;') or
                               contains($currentChar,'&#942;') or
                               contains($currentChar,'&#943;') or
                               contains($currentChar,'&#944;') or
                               contains($currentChar,'&#945;') or
                               contains($currentChar,'&#946;') or
                               contains($currentChar,'&#947;') or
                               contains($currentChar,'&#948;') or
                               contains($currentChar,'&#949;') or
                               contains($currentChar,'&#950;') or
                               contains($currentChar,'&#951;') or
                               contains($currentChar,'&#952;') or
                               contains($currentChar,'&#953;') or
                               contains($currentChar,'&#954;') or
                               contains($currentChar,'&#955;') or
                               contains($currentChar,'&#956;') or
                               contains($currentChar,'&#957;') or
                               contains($currentChar,'&#958;') or
                               contains($currentChar,'&#959;') or
                               contains($currentChar,'&#960;') or
                               contains($currentChar,'&#961;') or
                               contains($currentChar,'&#962;') or
                               contains($currentChar,'&#963;') or
                               contains($currentChar,'&#964;') or
                               contains($currentChar,'&#965;') or
                               contains($currentChar,'&#966;') or
                               contains($currentChar,'&#967;') or
                               contains($currentChar,'&#968;') or
                               contains($currentChar,'&#969;')">
					<fo:inline font-family="Symbol">
						<xsl:value-of select="$currentChar" />
					</fo:inline>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$currentChar" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="checkForGreekSymbols">
				<xsl:with-param name="index" select="$index + 1" />
				<xsl:with-param name="length" select="string-length($inputString)" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- Handle paragraph alignment. -->
	<xsl:template match="p|div">
		<xsl:call-template name='para'>
			<xsl:with-param name="palign">
				<xsl:choose>
					<xsl:when test="@align">
						<xsl:value-of select="@align" />
					</xsl:when>
					<xsl:when test="not(@align) and @style">
						<xsl:choose>
							<xsl:when
								test="string-length(substring-before(substring-after(@style,'text-align'),';')) = 0">
								<xsl:value-of
									select="normalize-space(substring-after(substring-after(@style,'text-align'),':'))" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of
									select="normalize-space(substring-after(substring-before(substring-after(@style,'text-align'),';'),':'))" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						left
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="lindent">
				<xsl:choose>
					<xsl:when test="contains(@style,'padding-left')">
						<xsl:choose>
							<xsl:when
								test="string-length(substring-before(substring-after(@style,'padding-left'),';')) = 0">
								<xsl:value-of
									select="normalize-space(substring-after(substring-after(@style,'padding-left'),':'))" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of
									select="normalize-space(substring-after(substring-before(substring-after(@style,'padding-left'),';'),':'))" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="contains(@style,'margin-left')">
						<xsl:choose>
							<xsl:when
								test="string-length(substring-before(substring-after(@style,'margin-left'),';')) = 0">
								<xsl:value-of
									select="normalize-space(substring-after(substring-after(@style,'margin-left'),':'))" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of
									select="normalize-space(substring-after(substring-before(substring-after(@style,'margin-left'),';'),':'))" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="para">
		<xsl:param name="palign" />
		<xsl:param name="lindent" />
		<xsl:variable name="align">
			<xsl:choose>
				<xsl:when test="$palign='left'">
					left
				</xsl:when>
				<xsl:when test="$palign='right'">
					right
				</xsl:when>
				<xsl:when test="$palign='center'">
					center
				</xsl:when>
				<xsl:when test="$palign='justify'">
					justify
				</xsl:when>
				<xsl:otherwise>
					left
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<!-- and extra spacing needs to be rendered in case of an empty <p></p> 
				tag and <p><br/> -->
			<xsl:when
				test="not(node()[normalize-space()]) or descendant::node()[1][self::br] or descendant::node()[1][self::hr]">
				<fo:block text-align="{$align}" white-space-treatment="preserve">
					<xsl:text></xsl:text>
					<xsl:apply-templates />
				</fo:block>
			</xsl:when>
			<!-- In case of any other regular p tag dont preserve white space so as 
				to avoid the extra spaces at the beggining of the line -->
			<xsl:otherwise>
				<fo:block text-align="{$align}">
					<xsl:if test="string-length($lindent) > 0">
						<xsl:attribute name="start-indent"><xsl:value-of
							select="$lindent" /></xsl:attribute>
					</xsl:if>
					<xsl:apply-templates />
				</fo:block>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- List item setup. -->
	<xsl:param name="list-startdist-default" select="string('1em')" />
	<xsl:param name="list-gap-default" select="string('0.2em')" />
	<xsl:attribute-set name="list.item">
		<xsl:attribute name="space-before">0.4em</xsl:attribute>
		<xsl:attribute name="space-after">0.4em</xsl:attribute>
		<xsl:attribute name="relative-align">baseline</xsl:attribute>
	</xsl:attribute-set>

	<!-- Handle unordered lists. -->
	<xsl:template match="ul">
		<!-- determine the distance between the start of the list-item-label and 
			the start of the list-item-body, the distance between the end of the list-item-label 
			and the start of the list-item-body. -->
		<xsl:variable name="start-dist-local">
			<xsl:choose>
				<xsl:when test="./@startdist">
					<xsl:value-of select="./@startdist" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$list-startdist-default" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="gap-local">
			<xsl:choose>
				<xsl:when test="./@gap">
					<xsl:value-of select="./@gap" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$list-gap-default" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- Generate fo:list-block. -->
		<fo:list-block provisional-distance-between-starts="{$start-dist-local}"
			provisional-label-separation="{$gap-local}">
			<!-- Process the descendants of li -->
			<xsl:apply-templates />
		</fo:list-block>
	</xsl:template>

	<xsl:template match="ul/li">
		<fo:list-item xsl:use-attribute-sets="list.item">
			<!-- Generate list label. -->
			<!-- The end position of the label is calculated by label-end()function. -->
			<!-- The characters for label of line are specified in the type attribute. 
				Initial value is “&#x2022;” -->
			<fo:list-item-label end-indent="label-end()">
				<fo:block text-align="end">
					<xsl:choose>
						<xsl:when test="../@type='disc'">
							<xsl:text>●</xsl:text>
						</xsl:when>
						<xsl:when test="../@type='circle'">
							<xsl:text>○</xsl:text>
						</xsl:when>
						<xsl:when test="../@type='square'">
							<xsl:text>□</xsl:text>
						</xsl:when>
						<xsl:when test="../@type='bsquare'">
							<xsl:text>■</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>&#x2022;</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:list-item-label>
			<!-- Generate the list body. -->
			<!-- The starting position of the label is calculated by the body-start() 
				function -->
			<fo:list-item-body start-indent="body-start()"
				text-align="justify">
				<fo:block>
					<xsl:apply-templates />
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>
	</xsl:template>

	<!-- Handle ordered lists. -->
	<xsl:template match="ol">
		<!-- determine the distance between the start of the list-item-label and 
			the start of the list-item-body, the distance between the end of the list-item-label 
			and the start of the list-item-body. -->
		<xsl:variable name="start-dist-local">
			<xsl:choose>
				<xsl:when test="./@startdist">
					<xsl:value-of select="./@startdist" />
				</xsl:when>
				<xsl:otherwise>
					<!-- Sets the list-item offset to the length of the string representation 
						of the number of li elements in em units -->
					<xsl:value-of select="string(concat(string-length(count(./li)), 'em'))" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="gap-local">
			<xsl:choose>
				<xsl:when test="./@gap">
					<xsl:value-of select="./@gap" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$list-gap-default" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- generate fo:list-block -->
		<fo:list-block provisional-distance-between-starts="{$start-dist-local}"
			provisional-label-separation="{$gap-local}">
			<!-- Process the descendants of li -->
			<xsl:apply-templates />
		</fo:list-block>
	</xsl:template>

	<xsl:template match="ol/li">
		<fo:list-item xsl:use-attribute-sets="list.item">
			<!-- generate list-item-label -->
			<!-- the end position of the list-item-label is calculated by label-end() 
				function -->
			<!-- label format is specified in the type attribute. The initial value 
				is '1'. -->
			<fo:list-item-label end-indent="label-end()"
				text-align="right">
				<fo:block>
					<xsl:choose>
						<xsl:when test="../@type">
							<xsl:number format="{../@type}" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:number format="1." />
						</xsl:otherwise>
					</xsl:choose>
				</fo:block>
			</fo:list-item-label>
			<!-- generate the list-item-body -->
			<!-- The start position of the list-item-label is calculated by body-start() 
				function -->
			<fo:list-item-body start-indent="body-start()"
				text-align="justify">
				<fo:block>
					<!-- the descendants of li are specified by the descendants of templates. -->
					<xsl:apply-templates />
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>
	</xsl:template>

	<!-- this one's easy; <tr> corresponds to <fo:table-row> -->
	<xsl:template match="tr">
		<xsl:choose>
			<!-- Have to make sure there is a td or th child element -->
			<xsl:when test="name(child::node())='td' or name(child::node())='th'">
				<fo:table-row>
					<xsl:apply-templates />
				</fo:table-row>
			</xsl:when>
			<!-- No td or th. have to add a cell and block to keep FOP happy -->
			<xsl:otherwise>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<xsl:apply-templates />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Handle table header cells. They should be bold and centered by default. 
		Look back at the containing <table> tag to see if a border width was specified. -->
	<xsl:template match="th">
		<fo:table-cell font-weight="bold" text-align="center"
			padding-left="2pt" padding-right="2pt">
			<xsl:if test="ancestor::table[1]/@border > 0">
				<xsl:attribute name="border-style">solid</xsl:attribute>
				<xsl:attribute name="border-width">1pt</xsl:attribute>
			</xsl:if>
			<fo:block>
				<xsl:apply-templates />
			</fo:block>
		</fo:table-cell>
	</xsl:template>

	<!-- Handle table data cells. Look back at the containing <table> tag to 
		see if a border width was specified. -->
	<xsl:template match="td">
		<fo:table-cell padding-left="2pt" padding-right="2pt">
			<xsl:if test="ancestor::table/@border > 0">
				<xsl:attribute name="border-style">solid</xsl:attribute>
				<xsl:attribute name="border-width">1pt</xsl:attribute>
			</xsl:if>
			<fo:block>
				<xsl:choose>
					<xsl:when test="@align='left'">
						<xsl:attribute name="text-align">start</xsl:attribute>
					</xsl:when>
					<xsl:when test="@align='center'">
						<xsl:attribute name="text-align">center</xsl:attribute>
					</xsl:when>
					<xsl:when test="@align='right'">
						<xsl:attribute name="text-align">end</xsl:attribute>
					</xsl:when>
					<xsl:when test="@align='justify'">
						<xsl:attribute name="text-align">justify</xsl:attribute>
					</xsl:when>
				</xsl:choose>
				<xsl:apply-templates />
			</fo:block>
		</fo:table-cell>
	</xsl:template>

	<xsl:template match="column1">
		<fo:inline>
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="column2">
		<fo:inline>
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="br">
		<xsl:choose>
			<xsl:when test="name(preceding-sibling::node()[1])='br'">
				<fo:block white-space-treatment="preserve">
					<xsl:text>&space;</xsl:text>
				</fo:block>
			</xsl:when>
			<xsl:when test="name(preceding-sibling::node()[1])='p'">
				<fo:block white-space-treatment="preserve">
					<xsl:text>&space;</xsl:text>
				</fo:block>
			</xsl:when>
			<xsl:otherwise>
				<fo:block>
					<xsl:text></xsl:text>
				</fo:block>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>