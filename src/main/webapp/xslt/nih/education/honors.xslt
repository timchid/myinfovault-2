<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="honors">
      <xsl:if test="honor-record">
        <fo:block id="honors">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body>
                    <xsl:apply-templates select="honor-record"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
      </xsl:if>
    </xsl:template>

    <!-- HONORS AND AWARDS -->
    <xsl:template match="honor-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="year" mode="honors" />
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="description" mode="honors"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <!-- HONOR DATA TAGS -->
    <xsl:template match="year" mode="honors">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
    <xsl:template match="description" mode="honors">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
    

</xsl:stylesheet>
