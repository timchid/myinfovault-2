<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="education">
        <fo:block font-size="8pt" space-before="2pt"> EDUCATION/TRAINING <fo:inline
            font-style="italic" font-size="8pt">(Begin with baccalaureate or other initial professional education, such as nursing,
            include postdoctoral training and residency training if applicable.)</fo:inline></fo:block>
        
        <fo:table table-layout="auto" width="100%" border-top="black" border-bottom="black"
            space-before="2pt">
            <fo:table-column column-width="45%" />
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="25%" />
            <fo:table-header border-top="thin solid black" border-bottom="thin solid black"  text-align="center" font-size="10pt">
                <fo:table-row >
                    <fo:table-cell border-right="thin solid black" display-align="center" >
                        <fo:block padding="2pt" font-size="8pt">INSTITUTION AND LOCATION</fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="thin solid black">
                        <fo:block padding="2pt" font-size="8pt">DEGREE</fo:block>
                        <fo:block font-style="italic" font-size="8pt">(if applicable)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-right="thin solid black" display-align="center"  >
                        <fo:block padding="2pt" font-size="8pt">MM/YY</fo:block>
                    </fo:table-cell>
                    <fo:table-cell display-align="center" >
                        <fo:block padding="2pt" font-size="8pt">FIELD OF STUDY</fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-header>
            <fo:table-body>
              <xsl:apply-templates select="education-record" />
             	<!-- Only way to get bottom line in RTF format..."fo:leader" and "fo:block border=" don't work! -->
                <fo:table-row>
                  <fo:table-cell border-bottom="thin solid black">
            	    <fo:block/>
                  </fo:table-cell>
                  <fo:table-cell border-bottom="thin solid black">
             	    <fo:block/>
                  </fo:table-cell>
                  <fo:table-cell border-bottom="thin solid black">
            	    <fo:block/>
                  </fo:table-cell>
                  <fo:table-cell border-bottom="thin solid black">
            	   <fo:block/>
                 </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
    
    <xsl:template match="education-record">
        <fo:table-row>
            <fo:table-cell text-align="left" border-right="thin solid black">
                <fo:block font-size="11pt" padding="1pt"><xsl:apply-templates
                    select="institution"/><xsl:if test="location">, <xsl:apply-templates select="location" mode="education"/></xsl:if></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="left" border-right="thin solid black" margin-left="4pt">
                <fo:block font-size="11pt" padding="1pt">
                    <xsl:apply-templates select="degree" mode="education"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="left" border-right="thin solid black">
                <fo:block font-size="11pt" padding="1pt" text-align="center">
                    <xsl:if test="enddate"><xsl:apply-templates select="endmonthid" mode="education"/>/<xsl:apply-templates select="enddate" mode="education"/></xsl:if></fo:block>
            </fo:table-cell>
            <fo:table-cell text-align="left" margin-left="4pt">
                <fo:block font-size="11pt" padding="1pt">
                    <xsl:apply-templates select="field" mode="education"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    <xsl:template match="location" mode="education">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="field" mode="education">
        <fo:inline padding-left="4pt"><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="startdate" mode="education">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="endmonthid" mode="education">
        <xsl:choose>
            <xsl:when test="(string-length(current()) &lt; 2)">
                <fo:inline>0<xsl:value-of select="current()"/></fo:inline>
            </xsl:when>
            <xsl:otherwise>
                <fo:inline><xsl:value-of select="current()"/></fo:inline>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="enddate" mode="education">
        <fo:inline><xsl:value-of select="substring(current(),3)"/></fo:inline>
    </xsl:template>
    <xsl:template match="degree" mode="education">
        <fo:inline padding-left="4pt"><xsl:apply-templates /></fo:inline>
    </xsl:template>

</xsl:stylesheet>
