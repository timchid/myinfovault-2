<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY colon ":">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- GRANTS -->

    <xsl:template match="grants-ongoing-research">
        <xsl:call-template name="grants"/>
    </xsl:template>
   
    <xsl:template match="grants-completed-research">
        <xsl:call-template name="grants"/>
    </xsl:template>
   
    <xsl:template match="grants-other-research">
        <xsl:call-template name="grants"/>
    </xsl:template>

 <xsl:template name="grants">
        <xsl:apply-templates select="grantnih-record"/>
    </xsl:template>
    <xsl:template match="grantnih-record">
        <fo:block space-after="12pt">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="25%"/>
                <fo:table-column column-width="40%"/>
                <fo:table-column column-width="35%"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="grantnumber"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>
                               <xsl:apply-templates select="principalinvestigator"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <xsl:choose>
                              <xsl:when test="child::startdate and child::enddate">
                                <fo:block>
                                    <xsl:apply-templates select="startdate" mode="grants"/> -
                                    <xsl:apply-templates select="enddate" mode="grants"/>
                                </fo:block>
                                </xsl:when>
                                <xsl:when test="child::submitdate">
                                    <fo:block>
                                        <xsl:apply-templates select="submitdate" mode="grants"/>
                                    </fo:block>
                                </xsl:when>
                                <xsl:otherwise>
                                    <fo:block>
                                        <xsl:apply-templates select="startdate" mode="grants"/>
                                        <xsl:apply-templates select="enddate" mode="grants"/>
                                    </fo:block>
                                </xsl:otherwise>
                            </xsl:choose>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
            <!-- Outside of table -->
            <xsl:apply-templates select="grantsource"/>
            <xsl:apply-templates select="granttitle"/>
            <xsl:apply-templates select="grantdescription"/>
            <xsl:apply-templates select="rolename" mode="grants"/>
        </fo:block>
    </xsl:template>
 
    <!-- GRANT DATA TAGS -->
    <xsl:template match="granttype">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="granttitle">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="grantnumber">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="grantdescription">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="grantsource">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="grantamount">
        <fo:inline>$<xsl:apply-templates/></fo:inline>
    </xsl:template>

    <xsl:template match="submitdate" mode="grants">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="startdate" mode="grants">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="enddate" mode="grants">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="principalinvestigator">
        <fo:inline><xsl:apply-templates/>&space;(PI)</fo:inline>
    </xsl:template>

    <xsl:template match="statusname" mode="grants">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="rolename" mode="grants">
        <fo:block>
            Role&colon;&space;<xsl:apply-templates/>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>
