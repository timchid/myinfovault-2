<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="service-committee">
     <xsl:if test="committee-all-record">
      <fo:block id="service-committee" padding-bottom="12pt" keep-with-previous="1">
        <fo:table table-layout="fixed" width="100%">
          <fo:table-column column-width="20%" />
          <fo:table-column column-width="80%" />
            <fo:table-body>                
              <xsl:apply-templates select="committee-all-record"/>
            </fo:table-body>
        </fo:table>
      </fo:block>
     </xsl:if>
    </xsl:template>
    
    <xsl:template match="committee-all-record">
      <fo:table-row>
        <fo:table-cell>
          <fo:block>
            <xsl:apply-templates select="year"/>
          </fo:block>
        </fo:table-cell>
        <fo:table-cell>
          <fo:block>
            <xsl:apply-templates select="role" mode="service"/>
            <xsl:apply-templates select="description" mode="service"/>                               
          </fo:block>
        </fo:table-cell>
      </fo:table-row>
    </xsl:template>

    <xsl:template match="role" mode="service">
        <fo:inline><xsl:apply-templates/>&#160;- </fo:inline>
    </xsl:template>
    <xsl:template match="year" mode="service">
        <fo:inline><xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="description" mode="service">
        <fo:inline><xsl:apply-templates/></fo:inline>
    </xsl:template>

    
</xsl:stylesheet>
