<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY qmark "?">
 <!ENTITY quote '"'>
 <!ENTITY colon ":">
 <!ENTITY expoint "!">
 ]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- JOURNALS -->

    <xsl:template match="nih-publications">
        <fo:block id="nih-publications">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <!-- JOURNALS -->
    <xsl:template match="journal-record">
        <xsl:call-template name="record"/>
    </xsl:template>
    <!-- BOOK CHAPTERS -->
    <xsl:template match="book-chapter-record">
        <xsl:call-template name="record"/>
    </xsl:template>

    <!-- LETTERS TO THE EDITOR -->

    <xsl:template match="letter-to-editor-record">
        <xsl:call-template name="record"/>
    </xsl:template>

    <!-- BOOKS EDITED -->

    <xsl:template match="book-editor-record">
        <xsl:call-template name="record"/>
    </xsl:template>

    <!-- BOOK REVIEWS -->

    <xsl:template match="review-record">
        <xsl:call-template name="record"/>
    </xsl:template>

    <!-- BOOKS AUTHORED -->

    <xsl:template match="book-author-record">
        <xsl:call-template name="record"/>
    </xsl:template>

    <!-- LIMITED DISTRIBUTION -->

    <xsl:template match="limited-record">
        <xsl:call-template name="record"/>
    </xsl:template>

    <!-- ALTERNATIVE MEDIA -->

    <xsl:template match="media-record">
        <xsl:call-template name="record"/>
    </xsl:template>

    <!-- ABSTRACTS -->

    <xsl:template match="abstract-record">
        <xsl:call-template name="record"/>
    </xsl:template>
    <xsl:template match="presentation-record">
        <xsl:call-template name="record"/>
    </xsl:template>


    <!-- PUBLICATION DATA TAGS -->

    <xsl:template match="number" mode="publications">
        <fo:inline>
            <xsl:apply-templates/>&period;&space;
        </fo:inline>
    </xsl:template>

    <xsl:template match="year" mode="publications">
        <fo:inline>&comma;&space;<xsl:apply-templates/>&period;</fo:inline>
    </xsl:template>

    <xsl:template match="author" mode="publications">
        <fo:inline>
            <xsl:choose>
                <xsl:when
                    test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record"
                    ><xsl:apply-templates/>&period;&space;</xsl:when>
                <xsl:when
                    test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record"
                    ><xsl:apply-templates/>&colon;&space;</xsl:when>
                <xsl:otherwise><xsl:apply-templates/>&period;&space;</xsl:otherwise>
            </xsl:choose>
        </fo:inline>
    </xsl:template>

    <xsl:template match="contributor" mode="publications">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="editor" mode="publications">
        <fo:inline>, <xsl:apply-templates/>, (ed)</fo:inline>
    </xsl:template>

    <xsl:template match="title" mode="publications">
        <xsl:variable name="value" select="." />
        <xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
        <xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
        <xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
        <xsl:variable name="hasPunctuation">
            <xsl:choose>
                <!-- Original -->
                <!-- xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz?!&quote;','ABCDEFGHIJKLMNOPQRSTUVWXYZ- - -')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..' )">1</xsl:when -->
                <!-- Match the TitleFormatter logic -->
                <xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <fo:inline>
            <xsl:choose>
                <xsl:when
                    test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record">
                    <xsl:apply-templates /><xsl:if test="$hasPunctuation != '1' and $lastCharacter != '&qmark;' and $lastCharacter != '&expoint;' and $lastCharacter != '&quote;'">&period;</xsl:if></xsl:when>
                <xsl:when
                    test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record | parent::presentation-record">
                    <xsl:apply-templates /><xsl:choose><xsl:when test="../editor or ../journal or ../publisher or ../volume or ../issue or ../city or ../pages or ../locationdescription">&comma;</xsl:when><xsl:otherwise><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if></xsl:otherwise></xsl:choose>
                </xsl:when>
                <xsl:otherwise><xsl:apply-templates /><xsl:if test="not(preceding-sibling::locationdescription or following-sibling::locationdescription)">&period;</xsl:if></xsl:otherwise>
            </xsl:choose>
        </fo:inline>
    </xsl:template>

    <xsl:template match="journal" mode="publications">
        <fo:inline>
            <xsl:choose>
                <xsl:when
                    test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record"
                    >&period;&space;<xsl:apply-templates/></xsl:when>
                <xsl:when
                    test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record"
                    >&comma;&space;<xsl:apply-templates/></xsl:when>
                <xsl:otherwise><xsl:apply-templates/>. </xsl:otherwise>
            </xsl:choose>
        </fo:inline>
    </xsl:template>

    <xsl:template match="publisher" mode="publications">
        <fo:inline>&comma;&space;<xsl:apply-templates/></fo:inline>
    </xsl:template>

    <xsl:template match="city" mode="publications">
        <fo:inline>&comma;&space;<xsl:apply-templates/></fo:inline>
    </xsl:template>

    <xsl:template match="volume" mode="publications">
        <fo:inline>
            <xsl:choose>
                <xsl:when
                    test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record"
                    >&comma;&space;<xsl:apply-templates/></xsl:when>
                <xsl:when
                    test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record"
                    >&comma;&space;Vol. <xsl:apply-templates/></xsl:when>
                <xsl:otherwise><xsl:apply-templates/>, </xsl:otherwise>
            </xsl:choose>
        </fo:inline>
    </xsl:template>

    <xsl:template match="issue" mode="publications">
        <fo:inline>(<xsl:apply-templates/>)</fo:inline>
    </xsl:template>

    <xsl:template match="pages" mode="publications">
        <fo:inline>
            <xsl:choose>
                <xsl:when
                    test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record"
                    >&colon;&space;<xsl:apply-templates/></xsl:when>
                <xsl:when
                    test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record"
                    >&period;&space;pp. <xsl:apply-templates/></xsl:when>
                <xsl:otherwise>: <xsl:apply-templates/></xsl:otherwise>
            </xsl:choose>
        </fo:inline>
    </xsl:template>

    <xsl:template match="monthname" mode="publications">
        <xsl:if test="text() != 'None'">
            <fo:inline><xsl:apply-templates/>, </fo:inline>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="locationdescription" mode="publications">
        <fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates/></fo:inline>
    </xsl:template>

    <xsl:template match="addheader" mode="publications">
        <fo:block font-weight="bold" text-decoration="underline" padding-top="12pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="addcontent" mode="publications">
        <fo:block padding-top="12pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="column1" mode="publications">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="column2" mode="publications">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>

    <xsl:template name="record" mode="publications">
        <fo:block font-size="${text.fontsize}" keep-together.within-page="always">
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="5%"/>
            <fo:table-column column-width="95%"/>
          <fo:table-body>
            <fo:table-row>
            <fo:table-cell>
               <fo:block>
                    <xsl:apply-templates select="number" mode="publications"/>
               </fo:block>
            </fo:table-cell>
            <fo:table-cell>
            <xsl:choose>
                <xsl:when test="descendant::citation">
                  <fo:block>
                     <xsl:apply-templates select="citation" mode="publications"/>
                  </fo:block>
                </xsl:when>
                <xsl:otherwise>
                    <fo:block>
                        <xsl:apply-templates select="author" mode="publications"/>
                        <xsl:apply-templates select="title" mode="publications"/>
                        <xsl:apply-templates select="editor" mode="publications"/>
                        <xsl:apply-templates select="journal" mode="publications"/>
                        <xsl:apply-templates select="volume" mode="publications"/>
                        <xsl:apply-templates select="issue" mode="publications"/>
                        <xsl:apply-templates select="publisher" mode="publications"/>
                        <xsl:apply-templates select="city" mode="publications"/>
                        <xsl:apply-templates select="pages" mode="publications"/>
                        <xsl:apply-templates select="locationdescription" mode="publications"/>
                        <xsl:apply-templates select="year" mode="publications"/>                        
                    </fo:block>
                </xsl:otherwise>
            </xsl:choose>
            </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
        </fo:block>
    </xsl:template>

 </xsl:stylesheet>
