<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
<!ENTITY cr "&#x0A;">
<!ENTITY space " ">
<!ENTITY period ".">
<!ENTITY apostrophy "'">
<!ENTITY comma ",">
<!ENTITY qmark "?">
<!ENTITY expoint "!">
]>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="html-tags.xslt"/>
    <xsl:import href="mivDocSetup.xslt"/>
    
    <xsl:template match="/">
        <xsl:call-template name="documentSetup">
            <xsl:with-param name="documentName" select="/decision/description" />
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="/decision">
        <fo:block>
            <fo:block font-size="16pt" font-weight="bold" text-align="center">
                <xsl:value-of select="description"/>
            </fo:block>
            
            <fo:block font-size="11pt" text-align="center">
                <xsl:value-of select="candidate"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="action"/>
            </fo:block>
        </fo:block>
       
        <xsl:apply-templates select="choices"/>
            
        <fo:table space-before="20pt">
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell width="20pt" border-bottom="solid" text-align="center">
                        <fo:block font-weight="bold"><xsl:if test="iscontrary"><xsl:text>x</xsl:text></xsl:if></fo:block>
                    </fo:table-cell>
                    
                    <fo:table-cell>
                        <fo:block margin-left="5pt"><xsl:text>Decision contrary to the committee recommendation</xsl:text></fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
       
        <xsl:apply-templates select="comments"/>
        
        <xsl:apply-templates select="signature"/>
    </xsl:template>
    
    <xsl:template match="comments">
        <fo:block space-before="20pt">
             <fo:block font-size="11pt" font-weight="bold">
                 <xsl:text>Comments:</xsl:text>
             </fo:block>
             
             <fo:block font-size="11pt" text-align="left" linefeed-treatment="preserve">
                 <xsl:apply-templates/>
             </fo:block>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="signature">
        <fo:block space-before="20pt">
            <fo:table width="100%" >
                <fo:table-column column-width="8%"/>
                <fo:table-column column-width="50%"/>
                <fo:table-column column-width="10%"/>
                <fo:table-column column-width="5%"/>
                <fo:table-column column-width="27%"/>
                <fo:table-body>
                    <fo:table-row font-size="11pt" height="16pt" text-align="center">
                        <fo:table-cell>
                            <fo:block font-weight="bold"><xsl:text>Signed:</xsl:text></fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block font-style="italic">
                                <xsl:if test="invalidmessage">
                                    <fo:block-container width="10cm"
                                        height="4cm"
                                        position="absolute"
                                        top="-.5cm"
                                        left="0cm">
                                        <fo:block>
                                            <fo:external-graphic width="9cm"
                                                height="1.8cm" 
                                                content-height="scale-to-fit"
                                                content-width="scale-to-fit"
                                                src="url('${webapp}/images/invalid.png')"/>
                                        </fo:block>
                                        <fo:block font-size="10pt" font-weight="bold" color="#CC5555" linefeed-treatment="preserve">
                                            <xsl:value-of select="invalidmessage"/>
                                        </fo:block>
                                    </fo:block-container>
                                </xsl:if>
                                
                                <xsl:value-of select="name"/>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block/></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block font-weight="bold"><xsl:text>Date:</xsl:text></fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block font-style="italic">
                                <xsl:value-of select="date"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="choices">
        <fo:table space-before="40pt">
            <fo:table-body>
                <fo:table-row>
                    <xsl:apply-templates select="choice"/>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
    
    <xsl:template match="choice">
        <fo:table-cell width="20pt" border-bottom="solid" text-align="center" display-align="after">
            <fo:block font-weight="bold"><xsl:if test="ischosen"><xsl:text>x</xsl:text></xsl:if></fo:block>
        </fo:table-cell>
        
        <fo:table-cell display-align="after">
            <fo:block margin-left="5pt"><xsl:value-of select="description"/></fo:block>
        </fo:table-cell>
    </xsl:template>
</xsl:stylesheet>