<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:import href="html-tags.xslt"/>
    <xsl:import href="mivCommon.xslt"/>
  
    <!-- Define the document name. -->
    <xsl:variable name="documentName" select="'Agricultural Experiment Station'"/>
  
    <!-- document setup is done in mivCommon.xslt -->
    <xsl:template match="/">
        <xsl:call-template name="documentSetup">
            <xsl:with-param name="documentName" select="$documentName" />
            <xsl:with-param name="hasSections" select="//agstation-record" />
        </xsl:call-template>
    </xsl:template>
    
   
    <xsl:template match="packet">
        <xsl:if test="count(descendant::agstation-reports/agstation-record)>0">
            <xsl:call-template name='maintitle'>
                <xsl:with-param name="documentName" select="$documentName" />
            </xsl:call-template>
            <fo:block font-weight="bold" text-align="center" space-after="12pt">Annual Reports</fo:block>
            <xsl:call-template name='nameblock'/>
            <fo:block id="main" font-size="12pt" font-family="Helvetica">
                <xsl:apply-templates select="agstation-reports/agstation-record"/>
            </fo:block>
        </xsl:if>
    </xsl:template>
   
    <xsl:template match="agstation-record">
        <fo:block padding-bottom="12pt">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%" />
                <fo:table-column column-width="80%" />
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell padding="2pt">
                            <fo:block>Title:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="2pt">
                            <fo:block><xsl:apply-templates select="reporttitle"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="2pt">
                            <fo:block>Number:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="2pt">
                            <fo:block><xsl:apply-templates select="reportnumber"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="2pt">
                            <fo:block>Investigators:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="2pt">
                            <fo:block><xsl:apply-templates select="investigatorname"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding="2pt">
                            <fo:block>Date(s):</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="2pt">
                            <fo:block><xsl:apply-templates select="datespan"/></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
   
    <xsl:template match="reporttitle|reportnumber|investigatorname|datespan">
        <fo:block><xsl:apply-templates /></fo:block>
    </xsl:template>
   
    <xsl:template match="symbol">
        <fo:inline font-size="9pt" font-family="Symbol">
            <xsl:value-of select="."/>
        </fo:inline>
    </xsl:template>
</xsl:stylesheet>