<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
<!ENTITY cr "&#x0A;">
<!ENTITY space " ">
<!ENTITY period ".">
<!ENTITY comma ",">
<!ENTITY qmark "?">
<!ENTITY expoint "!">
<!ENTITY nbsp "&#160;"> 
]>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="html-tags.xslt" />
    <xsl:import href="mivCommon.xslt" />
    
    <xsl:variable name="documentName" select="/aaf/documentname"/>
    <xsl:variable name="actionDescription" select="/aaf/actiondescription"/>
    
    <!-- document setup is done in mivCommon.xslt -->
    <xsl:template match="/">
        <xsl:call-template name="documentSetup">
            <xsl:with-param name="documentName" select="$documentName" />
            <xsl:with-param name="documentHeader">
                <xsl:value-of select="concat($actionDescription,' - ',/aaf/candidatename)" />
            </xsl:with-param>
            <xsl:with-param name="documentFooter" select="$documentName" />
            <xsl:with-param name="hasSections" select="'true'" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="/aaf">
        <fo:block space-after="10pt">
            <xsl:call-template name="documentname">
                <xsl:with-param name="label" select="$documentName" />
            </xsl:call-template>
    
            <fo:block font-size="13pt" font-weight="bold" text-align="center" space-after="2pt">
                <xsl:apply-templates select="actiondescription" />
            </fo:block>
        </fo:block>
    
        <xsl:call-template name="aaf-header" />
        <xsl:apply-templates select="accelerationyears" />
        <xsl:apply-templates select="questions" />
        <xsl:apply-templates select="statuses" />
    </xsl:template>

    <xsl:template name="aaf-header">
        <fo:block space-after="10pt" font-weight="bold">
            <fo:table table-layout="auto" width="100%"
                border-top="black" border-bottom="black" space-before="2pt" border-collapse="separate">
                <fo:table-column column-width="14%" />
                <fo:table-column column-width="20%" />
                <fo:table-column column-width="29%" />
                <fo:table-column column-width="17%" />
                <fo:table-column column-width="20%" />
                <fo:table-body>
                    <fo:table-row>
                        <xsl:apply-templates select="candidatename" />
                    </fo:table-row>
                    <fo:table-row>
                        <xsl:apply-templates select="effectivedate" />
                        <fo:table-cell><fo:block /></fo:table-cell>
                        <xsl:apply-templates select="retroactivedate" />
                    </fo:table-row>
                    <fo:table-row>
                        <xsl:apply-templates select="enddate" />
                        <fo:table-cell><fo:block /></fo:table-cell>
                        <fo:table-cell><fo:block /></fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="candidatename">
        <fo:table-cell padding-top="10pt" font-size="10pt">
            <fo:block><xsl:value-of select="@label" />:</fo:block>
        </fo:table-cell>
        
        <fo:table-cell padding-top="10pt" font-size="9pt" number-columns-spanned="2"
                       border-bottom-style="dotted" border-bottom-width="1px" border-bottom-color="black">
            <fo:block><xsl:apply-templates /></fo:block>
        </fo:table-cell>
    </xsl:template>
    
    <xsl:template match="effectivedate|retroactivedate|enddate">
        <fo:table-cell padding-top="10pt" font-size="10pt">
            <fo:block><xsl:value-of select="@label" />:</fo:block>
        </fo:table-cell>
        
        <fo:table-cell padding-top="10pt" font-size="9pt"
                       border-bottom-style="dotted" border-bottom-width="1px" border-bottom-color="black">
            <fo:block><xsl:apply-templates /></fo:block>
        </fo:table-cell>
    </xsl:template>

    <xsl:template name="documentname">
        <xsl:param name="label" />
        <fo:block id="{$label}" font-size="19pt" font-weight="bold" text-align="center">
            <xsl:value-of select="$label" />
        </fo:block>
    </xsl:template>

    <!-- For now we are mimicking the look of the legacy RAF PDF here for the Acceleration Years -->
    <xsl:template match="accelerationyears">
        <fo:block space-after="10pt" font-weight="bold">
            <fo:table table-layout="auto" width="100%"
                border-top="black" border-bottom="black" space-before="2pt" border-collapse="separate">
                <fo:table-column column-width="8%" />
                <fo:table-column column-width="5%" />
                <fo:table-column column-width="20%" />
                <fo:table-column column-width="5%" />
                <fo:table-body>
                    <fo:table-row>
                        <!-- Always print the "Normal" label-->
                        <fo:table-cell padding-top="5pt" font-size="10pt">
                            <fo:block>Normal:</fo:block>
                        </fo:table-cell>
                        
                        <xsl:choose>
                            <!-- If the accelerationyears value is 0, place an X for the Normal label, otherwise leave blank -->
                            <xsl:when test="self::node()[text()='0']">
                                <fo:table-cell padding-top="5pt" font-size="9pt"
                                    border-bottom-style="dotted" border-bottom-width="1px" border-bottom-color="black">
                                    <fo:block text-align="center">X</fo:block>
                                </fo:table-cell>
                                
                                <!-- Print the "Acceleration Years" label with no value when the value is 0 -->
                                <fo:table-cell padding-top="5pt" font-size="10pt">
                                    <fo:block text-align="center"><xsl:value-of select="@label" />:</fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-top="5pt" font-size="9pt"
                                    border-bottom-style="dotted" border-bottom-width="1px" border-bottom-color="black">
                                    <fo:block/>
                                </fo:table-cell>
                            </xsl:when>
                            
                            <!-- If the accelerationyears value is not 0, leave the Normal label with a blank value and print the value for the defined label -->
                            <xsl:otherwise>
                                <fo:table-cell padding-top="5pt" font-size="9pt"
                                    border-bottom-style="dotted" border-bottom-width="1px" border-bottom-color="black">
                                    <fo:block/>
                                </fo:table-cell>
                                
                                <!-- Print the "Acceleration Years" label with the value -->
                                <fo:table-cell padding-top="5pt" font-size="10pt">
                                    <fo:block text-align="center"><xsl:value-of select="@label" />:</fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-top="5pt" font-size="9pt"
                                    border-bottom-style="dotted" border-bottom-width="1px" border-bottom-color="black">
                                    <fo:block text-align="center"><xsl:apply-templates /></fo:block>
                                </fo:table-cell>
                            </xsl:otherwise>
                        </xsl:choose>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="questions">
        <fo:block space-after="10pt">
            <fo:table table-layout="auto" width="100%" 
                border-top="black" border-bottom="black" space-before="2pt" border-collapse="separate" border-spacing="1pt 0pt">
                <fo:table-column column-width="2%" />
                <fo:table-column column-width="88%" />
                <fo:table-column column-width="10%" />
                <fo:table-body>
                    <xsl:apply-templates select="question" />
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="question">
        <fo:table-row>
            <fo:table-cell padding-top="12pt">
                <fo:block font-size="10pt" font-weight="bold">
                    <xsl:number value="position()" format="1" />&period;
                </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="10pt">
                <fo:block>
                    <fo:inline font-size="10pt" font-weight="bold">
                        <xsl:apply-templates select="description" />
                    </fo:inline>
                    <fo:inline font-size="9pt" font-style="italic" margin-left="5px">
                        <xsl:apply-templates select="direction" />
                    </fo:inline>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="10pt">
                <fo:block font-size="10pt" text-align="center"
                    border-bottom-style="dotted" border-bottom-width="1px"
                    border-bottom-color="black">
                    <xsl:apply-templates select="response" />
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="statuses">
        <fo:block space-after="10pt">
            <fo:table table-layout="auto" width="100%"
                border-top="black" border-bottom="black" space-before="2pt">
                <xsl:choose>
                    <xsl:when test="present">
                        <fo:table-column column-width="48.5%" />
                        <fo:table-column column-width="3%" />
                        <fo:table-column column-width="48.5%" />
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:table-column column-width="0%" />
                        <fo:table-column column-width="100%" />
                    </xsl:otherwise>
                </xsl:choose>

                <fo:table-body>
                    <fo:table-row>
                        <xsl:apply-templates select="present" />
                        <fo:table-cell><fo:block /></fo:table-cell>
                        <xsl:apply-templates select="proposed" />
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="present|proposed">
        <fo:table-cell padding-top="5pt">
            <fo:block font-size="11pt" font-weight="bold">
                <xsl:apply-templates select="@label"/>
            </fo:block>
            <fo:block>
                <xsl:apply-templates select="assignment" />
            </fo:block>
        </fo:table-cell>
    </xsl:template>

    <xsl:template match="assignment">
        <fo:block space-before="5pt" border-style="solid"
            border-width="1px" padding="5pt">
            <xsl:apply-templates select="scope" />
            <xsl:apply-templates select="percentoftime" />
            <xsl:apply-templates select="titles" />
        </fo:block>
    </xsl:template>
    
    <xsl:template match="scope">
        <fo:block font-size="9pt" space-after="5pt">
            <xsl:choose>
                    <!-- label, school, and department in separate blocks if there's a present status -->
                    <xsl:when test="ancestor::statuses[present]">
                        <fo:block font-weight="bold"><xsl:value-of select="@label" />:</fo:block>
                        <fo:block><xsl:apply-templates select="school"/></fo:block>
                        <fo:block><xsl:apply-templates select="department"/></fo:block>
                    </xsl:when>
                    
                    <!-- otherwise, show all inline delimited by a dash -->
                    <xsl:otherwise>
                        <fo:inline font-weight="bold"><xsl:value-of select="@label" />:&space;</fo:inline>
                        
                        <xsl:apply-templates select="school"/>
                        
                        <xsl:if test="department"><xsl:text> - </xsl:text></xsl:if>
                        
                        <xsl:apply-templates select="department"/>
                    </xsl:otherwise>
                </xsl:choose>
        </fo:block>
    </xsl:template>

    <xsl:template match="titles">
        <fo:block padding-top="5pt" border-top-style="dashed" margin-left="0pt"
            border-top-width="1px" border-top-color="black" >
            <xsl:apply-templates select="title" />
        </fo:block>
    </xsl:template>
    
    <xsl:template match="title">
        <fo:block space-after="3pt" keep-together.within-page="always">
            <!-- Hide the title border if this is the last node -->
            <xsl:if test="position() != last()">
                <xsl:attribute name="border-bottom-style">dotted</xsl:attribute>
                <xsl:attribute name="border-bottom-width">1px</xsl:attribute>
                <xsl:attribute name="border-bottom-color">black</xsl:attribute>
            </xsl:if>

            <fo:table table-layout="auto" width="100%">
                <fo:table-column column-width="57%" />
                <fo:table-column column-width="43%" />
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell padding-top="2pt"><xsl:apply-templates select="title-code" /></fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding-top="2pt"><xsl:apply-templates select="rank-title" /></fo:table-cell>
                        <fo:table-cell padding-top="2pt"><xsl:apply-templates select="step" /></fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell padding-top="2pt"><xsl:apply-templates select="percentoftime" /></fo:table-cell>
                        
                        <!-- show duration if this is a proposed title or not an appointment -->
                        <xsl:if test="ancestor::proposed or ancestor::aaf[not(@actiontype='APPOINTMENT')]">
                            <fo:table-cell padding-top="2pt"><xsl:apply-templates select="duration" /></fo:table-cell>
                        </xsl:if>
                    </fo:table-row>
                    <fo:table-row>
                        <xsl:choose>
                            <xsl:when test="monthly-salary">
                                <fo:table-cell padding-top="2pt"><xsl:apply-templates select="monthly-salary" /></fo:table-cell>
                                <fo:table-cell padding-top="2pt"><xsl:apply-templates select="annual-salary" /></fo:table-cell>
                            </xsl:when>
        
                            <xsl:otherwise>
                                <fo:table-cell padding-top="2pt"><xsl:apply-templates select="hourly-rate" /></fo:table-cell>
                            </xsl:otherwise>
                        </xsl:choose>
                    </fo:table-row>
                    <xsl:if test="ancestor::present">
                        <fo:table-row>
                            <xsl:choose>
                                <xsl:when test="ancestor::aaf[@actiontype='REAPPOINTMENT' or @actiontype='APPOINTMENT_INITIAL_CONTINUING']">
                                   <fo:table-cell padding-top="2pt"><xsl:apply-templates select="quartercount" /></fo:table-cell>
                                </xsl:when>

                                <xsl:when test="ancestor::aaf[@actiontype='APPOINTMENT']">
                                    <fo:table-cell padding-top="2pt"><fo:block></fo:block></fo:table-cell>
                                    <fo:table-cell padding-top="2pt"><fo:block></fo:block></fo:table-cell>
                                </xsl:when>

                                <xsl:otherwise>
                                    <fo:table-cell padding-top="2pt"><xsl:apply-templates select="years-at-rank" /></fo:table-cell>
                                    <fo:table-cell padding-top="2pt"><xsl:apply-templates select="years-at-step" /></fo:table-cell>
                                </xsl:otherwise>
                            </xsl:choose>
                        </fo:table-row>
                    </xsl:if>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="title-code|duration|monthly-salary|annual-salary|hourly-rate|rank-title|step|years-at-rank|years-at-step|quartercount">
        <fo:block>
            <fo:inline font-size="9pt" font-weight="bold" text-align="left">
                <xsl:value-of select="@label" />:&space;
            </fo:inline>
            <fo:inline font-size="9pt" font-weight="normal" text-align="left">
                <xsl:apply-templates />
            </fo:inline>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="percentoftime">
        <fo:block>
            <fo:inline font-size="9pt" font-weight="bold" text-align="left">
                <xsl:value-of select="@label" />:&space;
            </fo:inline>
            <fo:inline font-size="9pt" font-weight="normal" text-align="left">
                <xsl:apply-templates />
            </fo:inline>
            <xsl:if test="@withoutsalary">
                <fo:inline font-size="9pt" font-weight="normal" text-align="left">
                    <xsl:text>&space;WOS</xsl:text>
                </fo:inline>
            </xsl:if>
        </fo:block>
    </xsl:template>    
</xsl:stylesheet>
