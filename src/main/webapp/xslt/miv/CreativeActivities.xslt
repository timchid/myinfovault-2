<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:import href="html-tags.xslt" />
  <xsl:import href="mivCommon.xslt" />
  <xsl:import href="works.xslt" />
  <xsl:import href="events.xslt" />
  <xsl:import href="publicationevents.xslt" />
  <xsl:import href="reviews.xslt" />
  <xsl:import href="annotations.xslt"/>
  <xsl:import href="creativeactivitiescommon.xslt" />
      
  <!-- Define the document name. -->
  <xsl:variable name="documentName" select="'Creative Activities'" />
  <xsl:variable name="titleAdditional"> 
      <xsl:apply-templates select="//creative-activities-additional/section-header"/>
  </xsl:variable>

  <!-- Global variable to identify if a header page is needed(i.e) if there is data other than additional information. -->
  <xsl:variable name="hasHeaderpage" select="//*/works-record | //*/events-record | //*/publicationevents-record | //*/reviews-record"/>
  <xsl:variable name="hasAdditional" select="/*/creative-activities-additional"/>
      
  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
         <xsl:choose>
                  <xsl:when test="$hasHeaderpage">
                          <xsl:call-template name="documentSetup">
                               <xsl:with-param name="documentName" select="$documentName" />
                               <xsl:with-param name="title1" select="''" />
                               <xsl:with-param name="title2" select="''" />
                               <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                               <xsl:with-param name="hasSections" select="$hasHeaderpage" />
                       </xsl:call-template>
                  </xsl:when>        
                  <xsl:when test="$hasAdditional">
                       <xsl:call-template name="documentSetup">
                               <xsl:with-param name="documentName" select="''" />
                               <xsl:with-param name="title1" select="''" />
                               <xsl:with-param name="title2" select="''" />
                               <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                               <xsl:with-param name="hasSections" select="$hasAdditional" />
                       </xsl:call-template>
                  </xsl:when>
          </xsl:choose>                               
  </xsl:template>
  

  <xsl:template match="packet">
    <xsl:call-template name="main" />
  </xsl:template>
  
  <xsl:template name="main">
    <fo:block id="{$documentName}" font-size="12pt" font-family="Helvetica">
      <!-- Handle all works sections -->
      <xsl:if  test="//*/works-record">
        <xsl:call-template name='maintitle'>
          <xsl:with-param name="documentName">Creative Work</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="nameblock" />
        <xsl:apply-templates select="works-completed/works-record"/>
        <xsl:apply-templates select="works-scheduled/works-record"/>
        <xsl:apply-templates select="works-submitted/works-record"/>

      </xsl:if>    
      <!-- Handle all events sections -->
      <xsl:if  test="//*/events-record">
        <xsl:call-template name='maintitle'>
          <xsl:with-param name="documentName">Creative Activities: Public Dissemination</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="nameblock" />
        <xsl:apply-templates select="book-completed/events-record|book-scheduled/events-record|book-submitted/events-record"/>
        <xsl:apply-templates select="broadcast-completed/events-record|broadcast-scheduled/events-record|broadcast-submitted/events-record"/>
        <xsl:apply-templates select="catalogs-completed/events-record|catalogs-scheduled/events-record|catalogs-submitted/events-record"/>
        <xsl:apply-templates select="cd-dvd-video-completed/events-record|cd-dvd-video-scheduled/events-record|cd-dvd-video-submitted/events-record"/>
        <xsl:apply-templates select="citation-completed/events-record|citation-scheduled/events-record|citation-submitted/events-record"/>
        <xsl:apply-templates select="commission-completed/events-record|commission-scheduled/events-record|commission-submitted/events-record"/>
        <xsl:apply-templates select="concert-completed/events-record|concert-scheduled/events-record|concert-submitted/events-record"/>
        <xsl:apply-templates select="exhibitions-group-completed/events-record|exhibitions-group-scheduled/events-record|exhibitions-group-submitted/events-record"/>
        <xsl:apply-templates select="exhibitions-solo-completed/events-record|exhibitions-solo-scheduled/events-record|exhibitions-solo-submitted/events-record"/>
        <xsl:apply-templates select="structure-landscape-completed/events-record|structure-landscape-scheduled/events-record|structure-landscape-submitted/events-record"/>
        <xsl:apply-templates select="fashion-show-completed/events-record|fashion-show-scheduled/events-record|fashion-show-submitted/events-record"/>
        <xsl:apply-templates select="interview-commentary-completed/events-record|interview-commentary-scheduled/events-record|interview-commentary-submitted/events-record"/>
        <xsl:apply-templates select="performance-event-completed/events-record|performance-event-scheduled/events-record|performance-event-submitted/events-record"/>
        <xsl:apply-templates select="private-collection-completed/events-record|private-collection-scheduled/events-record|private-collection-submitted/events-record"/>
        <xsl:apply-templates select="product-completed/events-record|product-scheduled/events-record|product-submitted/events-record"/>
        <xsl:apply-templates select="program-notes-completed/events-record|program-notes-scheduled/events-record|program-notes-submitted/events-record"/>
        <xsl:apply-templates select="public-collection-completed/events-record|public-collection-scheduled/events-record|public-collection-submitted/events-record"/>
        <xsl:apply-templates select="reading-completed/events-record|reading-scheduled/events-record|reading-submitted/events-record"/>
        <xsl:apply-templates select="recordings-completed/events-record|recordings-scheduled/events-record|recordings-submitted/events-record"/>
        <xsl:apply-templates select="reproductions-completed/events-record|reproductions-scheduled/events-record|reproductions-submitted/events-record"/>
        <xsl:apply-templates select="screening-completed/events-record|screening-scheduled/events-record|screening-submitted/events-record"/>
        <xsl:apply-templates select="theatre-production-completed/events-record|theatre-production-scheduled/events-record|theatre-production-submitted/events-record"/>
        <xsl:apply-templates select="curated-exhibition-completed/events-record|curated-exhibition-scheduled/events-record|curated-exhibition-submitted/events-record"/>
      </xsl:if>

      <!-- Handle all publication events sections -->
      <xsl:if  test="//*/publicationevents-record">
        <xsl:call-template name='maintitle'>
          <xsl:with-param name="documentName">Creative Activities: Publication Events</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="nameblock" />
        <xsl:apply-templates select="publicationevents-published/publicationevents-record"/>
        <xsl:apply-templates select="publicationevents-in-press/publicationevents-record"/>
        <xsl:apply-templates select="publicationevents-submitted/publicationevents-record"/>
      </xsl:if>
      
      <!-- Handle reviews - no sections -->
      <!-- Handle all publication events sections -->
      <xsl:if  test="//*/reviews-record">
        <xsl:call-template name='maintitle'>
          <xsl:with-param name="documentName">Creative Activities: Reviews by Others</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="nameblock" />
        <xsl:apply-templates select="reviews/reviews-record"/>
      </xsl:if>
      
      <!-- Additional information -->
      <xsl:apply-templates select="creative-activities-additional"/>

    </fo:block>
  </xsl:template>

  <xsl:template match="works-submitted/works-record">
    <xsl:call-template name="works">
       <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::works-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="works-scheduled/works-record">
    <xsl:call-template name="works">
       <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::works-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="works-completed/works-record">
    <xsl:call-template name="works">
       <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::works-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="book-completed/events-record | 
  broadcast-completed/events-record | 
  catalogs-completed/events-record | 
  cd-dvd-video-completed/events-record | 
  citation-completed/events-record | 
  commission-completed/events-record | 
  concert-completed/events-record | 
  exhibitions-group-completed/events-record |
  exhibitions-solo-completed/events-record |
  structure-landscape-completed/events-record |
  fashion-show-completed/events-record |
  interview-commentary-completed/events-record |
  performance-event-completed/events-record |
  private-collection-completed/events-record |
  product-completed/events-record |
  program-notes-completed/events-record |
  public-collection-completed/events-record |
  reading-completed/events-record |
  recordings-completed/events-record |
  reproductions-completed/events-record |
  screening-completed/events-record |
  theatre-production-completed/events-record |
  curated-exhibition-completed/events-record">
    <xsl:call-template name="events">
      <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::events-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="book-scheduled/events-record | 
  broadcast-scheduled/events-record | 
  catalogs-scheduled/events-record | 
  cd-dvd-video-scheduled/events-record | 
  citation-scheduled/events-record | 
  commission-scheduled/events-record | 
  concert-scheduled/events-record | 
  exhibitions-group-scheduled/events-record |
  exhibitions-solo-scheduled/events-record |
  structure-landscape-scheduled/events-record |
  fashion-show-scheduled/events-record |
  interview-commentary-scheduled/events-record |
  performance-event-scheduled/events-record |
  private-collection-scheduled/events-record |
  product-scheduled/events-record |
  program-notes-scheduled/events-record |
  public-collection-scheduled/events-record |
  reading-scheduled/events-record |
  recordings-scheduled/events-record |
  reproductions-scheduled/events-record |
  screening-scheduled/events-record |
  theatre-production-scheduled/events-record |
  curated-exhibition-scheduled/events-record">
  <xsl:call-template name="events">
      <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::events-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="book-submitted/events-record | 
  broadcast-submitted/events-record | 
  catalogs-submitted/events-record | 
  cd-dvd-video-submitted/events-record | 
  citation-submitted/events-record | 
  commission-submitted/events-record | 
  concert-submitted/events-record | 
  exhibitions-group-submitted/events-record |
  exhibitions-solo-submitted/events-record |
  structure-landscape-submitted/events-record |
  fashion-show-submitted/events-record |
  interview-commentary-submitted/events-record |
  performance-event-submitted/events-record |
  private-collection-submitted/events-record |
  product-submitted/events-record |
  program-notes-submitted/events-record |
  public-collection-submitted/events-record |
  reading-submitted/events-record |
  recordings-submitted/events-record |
  reproductions-submitted/events-record |
  screening-submitted/events-record |
  theatre-production-submitted/events-record |
  curated-exhibition-submitted/events-record">
    <xsl:call-template name="events">
      <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::events-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="publicationevents-published/publicationevents-record">
    <xsl:call-template name="publicationevents">
      <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::publicationevents-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="publicationevents-in-press/publicationevents-record">
    <xsl:call-template name="publicationevents">
      <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::publicationevents-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="publicationevents-submitted/publicationevents-record">
    <xsl:call-template name="publicationevents">
      <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::publicationevents-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="works">
    <xsl:param name="sectionname"/>
    <fo:block>
      <xsl:if test="not(preceding-sibling::works-record)">
      <fo:block white-space-collapse="false" font-weight="bold" font-size="14pt" text-decoration="underline"
            space-after="12pt">
             <xsl:apply-templates select="../section-header" />
      </fo:block>
      </xsl:if>
      <xsl:call-template name="works-record"/>
      <xsl:if test="not(following-sibling::works-record)">
        <xsl:if test="preceding-sibling::*/footnote or self::*/footnote">
                <xsl:call-template name="footnotes"/>
        </xsl:if>
        <xsl:apply-templates select="../annotations-legends"/>        
      </xsl:if>  
    </fo:block>
  </xsl:template>

  <xsl:template name="events" >
    <xsl:param name="sectionname"/>
    <fo:block>
      <xsl:if test="not(preceding-sibling::events-record)">
      <fo:block white-space-collapse="false" font-weight="bold" font-size="14pt" text-decoration="underline"
            space-after="12pt">
             <xsl:apply-templates select="../section-header" />
      </fo:block>
      </xsl:if>
      <xsl:call-template name="events-record"/>
      <xsl:if test="not(following-sibling::events-record)">
        <xsl:if test="preceding-sibling::*/footnote or self::*/footnote">
                <xsl:call-template name="footnotes"/>
        </xsl:if>
        <xsl:apply-templates select="../annotations-legends"/>
      </xsl:if>  
    </fo:block>
  </xsl:template>

  <xsl:template name="publicationevents" >
    <xsl:param name="sectionname"/>
    <fo:block>
      <xsl:if test="not(preceding-sibling::publicationevents-record)">
      <fo:block white-space-collapse="false" font-weight="bold" font-size="14pt" text-decoration="underline" space-after="12pt">
        <xsl:apply-templates select="../section-header" />
      </fo:block>
      </xsl:if>
      <xsl:call-template name="publicationevents-record"/>
      <xsl:if test="not(following-sibling::publicationevents-record)">
        <xsl:if test="preceding-sibling::*/footnote or self::*/footnote">
          <xsl:call-template name="footnotes"/>
        </xsl:if>
        <xsl:apply-templates select="../annotations-legends"/>
      </xsl:if>  
    </fo:block>
  </xsl:template>

  <xsl:template match="reviews/reviews-record">
    <xsl:call-template name="reviews">
      <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="not(following-sibling::reviews-record) and following::*">
      <fo:block break-after="page"/>
    </xsl:if>
  </xsl:template>

  <!-- reviews -->
  <xsl:template name="reviews">
    <xsl:param name="sectionname"/>
    <fo:block>
      <xsl:if test="not(preceding-sibling::reviews-record)">
      <fo:block white-space-collapse="false" font-weight="bold" font-size="14pt" text-decoration="underline" space-after="12pt">
        <xsl:apply-templates select="../section-header" />
      </fo:block>
      </xsl:if>
      <xsl:call-template name="reviews-record"/>
      <xsl:if test="not(following-sibling::reviews-record)">
        <xsl:if test="preceding-sibling::*/footnote or self::*/footnote">
          <xsl:call-template name="footnotes"/>
        </xsl:if>
        <xsl:apply-templates select="../annotations-legends"/>
      </xsl:if>  
    </fo:block>
  </xsl:template>
  
  <!-- Additional Information -->
  <xsl:template match="creative-activities-additional">
        <!-- ADDITIONAL INFORMATION -->
        <fo:block id="{local-name()}">
                <fo:block id="title-additional" white-space-collapse="false" font-size="20pt" font-weight="bold"
                        space-after="16pt" text-align="center">
                        <xsl:apply-templates select="section-header" />
                </fo:block>
                <xsl:call-template name="nameblock" />
                <xsl:apply-templates select="additional-record" />
        </fo:block>
  </xsl:template>

  <xsl:template name="sectionheader">
    <xsl:param name="sectionname"/>
    <fo:block white-space-collapse="false" font-weight="bold" font-size="14pt" text-decoration="underline"
          space-after="12pt">
      <xsl:value-of select="$sectionname" />
    </fo:block>
  </xsl:template>
</xsl:stylesheet>
