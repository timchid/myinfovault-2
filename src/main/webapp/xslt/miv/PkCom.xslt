<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY dash "-">
 <!ENTITY period ".">
 ]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:import href="html-tags.xslt" />
	<xsl:import href="mivCommon.xslt" />
	<xsl:import href="annotations.xslt"/>
	
	<!-- Define the document name. -->
  <xsl:variable name="documentName" select="'List of Service'"/>
  <xsl:variable name="titleAdditional"> 
      <xsl:apply-templates select="//service-additional/section-header"/>
  </xsl:variable>
  
  <!-- Global variable to identify if a header page is needed(i.e) if there is data other than additional information. -->
  <xsl:variable name="hasHeaderpage" select="/*/service-campus | /*/service-department | /*/service-school | /*/service-systemwide | /*/service-other-university | /*/service-other-nonuniversity
      | /*/service-board | /*/service-administrative"/>
  <xsl:variable name="hasAdditional" select="/*/service-additional"/>
  
  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
        <xsl:choose>
          <xsl:when test="$hasHeaderpage">
                  <xsl:call-template name="documentSetup">
                       <xsl:with-param name="documentName" select="$documentName" />
                       <xsl:with-param name="title1" select="''" />
                       <xsl:with-param name="title2" select="''" />
                       <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                       <xsl:with-param name="hasSections" select="$hasHeaderpage" />
               </xsl:call-template>
          </xsl:when>        
          <xsl:when test="$hasAdditional">
               <xsl:call-template name="documentSetup">
                       <xsl:with-param name="documentName" select="''" />
                       <xsl:with-param name="title1" select="''" />
                       <xsl:with-param name="title2" select="''" />
                       <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                       <xsl:with-param name="hasSections" select="$hasAdditional" />
               </xsl:call-template>
	        </xsl:when>
        </xsl:choose>
  </xsl:template>

  <xsl:template match="packet">
    <xsl:if test="$hasHeaderpage">
      <xsl:call-template name='maintitle'>
        <xsl:with-param name="documentName" select="$documentName" />
      </xsl:call-template>
      <xsl:call-template name="nameblock" />
    </xsl:if>
    <xsl:call-template name="main"/>
  </xsl:template>
  	
      <xsl:template name="main">
        <fo:block id="main" font-size="12pt" font-family="Helvetica">
          <xsl:apply-templates select="service-administrative" />
	  <xsl:if test="/*/service-campus | /*/service-department | /*/service-school | /*/service-systemwide | /*/service-other-university | /*/service-other-nonuniversity">
             <fo:block space-after="12pt">
               <fo:block id="committees" font-size="12pt" font-weight="bold"
                 text-decoration="underline">
                 Committees
               </fo:block>
                 <!-- TODO: The order of committees here shouldn't be determined by the 
                   XSLT. They should appear in the order that the DocumentSection table specifies. -->
                 <xsl:apply-templates select="service-department" />
                 <xsl:apply-templates select="service-school" />
                 <xsl:apply-templates select="service-campus" />
                 <xsl:apply-templates select="service-systemwide" />
                 <xsl:apply-templates select="service-other-university" />
                 <xsl:apply-templates select="service-other-nonuniversity" />
             </fo:block>
	  </xsl:if>
          <xsl:apply-templates select="service-board" />
          <xsl:apply-templates select="service-additional" />
         </fo:block>
	</xsl:template>

	<!-- Start of  Modification for new record names -->
	<xsl:template match="service-department">
		<fo:block padding-top="12pt">
			<fo:block id="service-department" font-size="10pt"
				font-weight="bold" text-decoration="underline" space-after="12pt">
				Department/Section
			</fo:block>
			<xsl:apply-templates select="committee-record" />
		</fo:block>
	</xsl:template>

	<xsl:template match="committee-record">
		<fo:block space-after="12pt">
		<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="100%" />
				<fo:table-body>
					<xsl:if test="./labelbefore">
					<fo:table-row keep-with-next="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
									<xsl:with-param name="displaybefore" select="1" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
					<fo:table-row>
						<fo:table-cell>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="25%" />
								<fo:table-column column-width="75%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="year" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="role" />
												<xsl:apply-templates
													select="description" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
							</fo:table-cell>
					</fo:table-row>
					<xsl:if test="./labelafter">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
									<xsl:with-param name="displaybefore" select="0" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>

	<!-- End of  Modification for new record names -->
	<xsl:template match="service-school">
		<fo:block padding-top="12pt">
			<fo:block id="service-school" font-size="10pt"
				font-weight="bold" text-decoration="underline" space-after="12pt">
				School/College/Division
			</fo:block>
			<xsl:apply-templates select="committee-record" />
		</fo:block>
	</xsl:template>

	<xsl:template match="service-campus">
		<fo:block padding-top="12pt">
			<fo:block id="service-campus" font-size="10pt"
				font-weight="bold" text-decoration="underline" space-after="12pt">
				Campus
			</fo:block>
			<xsl:apply-templates select="committee-record" />
		</fo:block>
	</xsl:template>

	<xsl:template match="service-systemwide">
		<fo:block padding-top="12pt">
			<fo:block id="service-systemwide" font-size="10pt"
				font-weight="bold" text-decoration="underline" space-after="12pt">
				Systemwide
			</fo:block>
			<xsl:apply-templates select="committee-record" />
		</fo:block>
	</xsl:template>

	<xsl:template match="service-other-university">
		<fo:block padding-top="12pt">
			<fo:block id="service-other-university" font-size="10pt"
				font-weight="bold" text-decoration="underline" space-after="12pt">
				Other University
			</fo:block>
			<xsl:apply-templates select="committee-record" />
		</fo:block>
	</xsl:template>

	<xsl:template match="service-other-nonuniversity">
		<fo:block padding-top="12pt">
			<fo:block id="service-other-nonuniversity" font-size="10pt"
				font-weight="bold" text-decoration="underline" space-after="12pt">
				Other Non-University
			</fo:block>
			<xsl:apply-templates select="committee-record" />
		</fo:block>
	</xsl:template>
	
	<xsl:template match="service-board">
		<xsl:if test="name(preceding-sibling::*[1]) != 'department'"><fo:block space-before="36pt"></fo:block></xsl:if>		
			<fo:block id="service-board" font-weight="bold"
				text-decoration="underline" space-after="12pt">
				Editorial and Advisory Boards
			</fo:block>
			<xsl:apply-templates select="advisoryboard-record" />
	</xsl:template>
	
	<!-- Start of  Modification for new record names -->
	<xsl:template match="service-administrative">
		<fo:block space-after="36pt">
			<fo:block id="service-administrative" font-weight="bold"
				text-decoration="underline" space-after="12pt">
				Administrative Activities
			</fo:block>
			<xsl:apply-templates select="administrativeactivity-record" />
			</fo:block>
	</xsl:template>

	<xsl:template match="administrativeactivity-record">
		<fo:block space-after="12pt">
		<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="100%" />
				<fo:table-body>
					<xsl:if test="./labelbefore">
					<fo:table-row keep-with-next="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
									<xsl:with-param name="displaybefore" select="1" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
					<fo:table-row>
						<fo:table-cell>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="25%" />
								<fo:table-column column-width="75%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="startyear" />
												<xsl:apply-templates select="endyear" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="title" />
												<xsl:apply-templates
													select="percenteffort" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
					<xsl:if test="./labelafter">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
									<xsl:with-param name="displaybefore" select="0" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>

	<xsl:template match="advisoryboard-record">
		<fo:block space-after="12pt">
		<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="100%" />
				<fo:table-body>
					<xsl:if test="./labelbefore">
					<fo:table-row keep-with-next="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
									<xsl:with-param name="displaybefore" select="1" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
					<fo:table-row>
						<fo:table-cell>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="25%" />
								<fo:table-column column-width="75%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="years" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates
													select="description" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
					<xsl:if test="./labelafter">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
									<xsl:with-param name="displaybefore" select="0" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	<!-- End of  Modification for new record names -->

	<!-- SERVICE -->
	<xsl:template match="service-record">
		<fo:block space-after="12pt">
		<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="100%" />
				<fo:table-body>
					<xsl:if test="./labelbefore">
					<fo:table-row keep-with-next="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
									<xsl:with-param name="displaybefore" select="1" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
					<fo:table-row>
						<fo:table-cell>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="25%" />
								<fo:table-column column-width="75%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="year" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="role" />
												<xsl:apply-templates
													select="description" />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
					<xsl:if test="./labelafter">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
									<xsl:with-param name="displaybefore" select="0" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>

	<!-- ADDITIONAL INFORMATION -->

	<xsl:template match="service-additional">		
		<fo:block id="service-additional" break-before="page">
			<fo:block id="title-additional" white-space-collapse="false" font-size="20pt" font-weight="bold"
						space-after="16pt" text-align="center">
				<xsl:apply-templates select="section-header" />
			</fo:block>
                        <fo:block font-size="16pt" font-weight="bold"
					text-align="center" space-after="12pt">
				<xsl:apply-templates select="//fname" />
				<xsl:apply-templates select="//lname" />
			</fo:block>
			<xsl:apply-templates select="additional-record" />
		</fo:block>
	</xsl:template>

	<xsl:template match="year|startyear|title|description">
		<fo:inline>
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<!-- Start of modifications for new fileds -->

	<xsl:template match="endyear">
		<fo:inline>&space;&dash;&space;<xsl:apply-templates /></fo:inline>
	</xsl:template>

	<xsl:template match="percenteffort">
		<xsl:if test="string-length(.)!=0">
			<fo:inline>&comma;&space;<xsl:apply-templates />% effort</fo:inline>
		</xsl:if>
	</xsl:template>

	<!-- End of modifications for new fileds -->

	<xsl:template match="role">
		<fo:inline>
			<xsl:apply-templates />
			<xsl:if
				test="preceding::description|following::description">&comma;&space;
			</xsl:if>
		</fo:inline>
	</xsl:template>
</xsl:stylesheet>
