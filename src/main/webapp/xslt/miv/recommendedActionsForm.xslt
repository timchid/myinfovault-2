<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
<!ENTITY cr "&#x0A;">
<!ENTITY space " ">
<!ENTITY period ".">
<!ENTITY comma ",">
<!ENTITY qmark "?">
<!ENTITY expoint "!">
]>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="html-tags.xslt" />
    <xsl:import href="mivDocSetup.xslt" />

    <!-- document setup is done in mivDocSetup.xslt -->
    <xsl:template match="/">
        <xsl:call-template name="documentSetup">
            <xsl:with-param name="documentName" select="/raf/documentname" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="/raf">
        <fo:block space-after="10pt">
            <xsl:apply-templates select="documentname"/>
            
            <xsl:apply-templates select="actiondescription"/>
        </fo:block>
        
        <fo:block border-bottom-style="solid" border-bottom-width="2px">
            <xsl:apply-templates select="retroactivedate"/>
            <xsl:apply-templates select="effectivedate"/>
            <xsl:apply-templates select="enddate"/>
            <xsl:apply-templates select="candidatename" />
            <xsl:apply-templates select="departments"/>
    
            <xsl:call-template name="appointmentAndAcceleration" />
            <xsl:call-template name="yearsAtRankAndStep" />
        </fo:block>
        
        <xsl:apply-templates select="statuses"/>
    </xsl:template>
    
    <xsl:template match="documentname">
        <fo:block font-size="19pt" font-weight="bold" text-align="center">
            <xsl:apply-templates />
        </fo:block>
    </xsl:template>
    
    <xsl:template match="actiondescription">
        <fo:block font-size="13pt" font-weight="bold" text-align="center" space-after="2pt">
            <xsl:apply-templates />
        </fo:block>
    </xsl:template>
    
    <xsl:template match="candidatename">
        <fo:block space-after="5pt">
            <fo:table table-layout="auto" width="100%" border-top="black" border-bottom="black" space-before="2pt">
                <fo:table-column column-width="10%" />
                <fo:table-column column-width="49%" />
                <fo:table-column column-width="41%" />
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block font-size="12pt" font-weight="bold">
                                Name:
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block font-size="12pt" font-weight="bold"
                                text-align="left">
                                <xsl:apply-templates />
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block font-size="12pt" font-weight="normal">
                                
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    
                    <fo:table-row>
                        <fo:table-cell><fo:block /></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block border-top-style="solid" margin-left="0pt" margin-right="0px" border-top-width="1px" border-top-color="black" />
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block /></fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="retroactivedate|effectivedate|enddate">
        <fo:block space-after="5pt">
            <fo:table width="100%">
                <fo:table-column column-width="46%" />
                <fo:table-column column-width="30%" />
                <fo:table-column column-width="24%" />
            
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell><fo:block /></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block font-size="11pt" font-weight="bold" text-align="right">
                                <xsl:value-of select="@label" />:
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block font-size="10pt" font-weight="normal" text-align="center">
                                <xsl:apply-templates/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <!-- Use the top of the next row's last cell as the underline of the date -->
                    <fo:table-row>
                        <fo:table-cell><fo:block /></fo:table-cell>
                        <fo:table-cell><fo:block /></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block margin-left="3pt" margin-right="1pt" border-top-style="solid" border-top-width="1px" />
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template name="appointmentAndAcceleration">
        <xsl:variable name="accelerationyears" select="/raf/accelerationyears" />
        <xsl:variable name="decelerationyears" select="/raf/decelerationyears" />
        
        <fo:block space-after="10pt">
            <fo:table width="100%" space-before="2pt">
                <fo:table-column column-width="5%" /><!-- value -->
                <fo:table-column column-width="8%" /><!-- 9 Mo. -->
                <fo:table-column column-width="5%" /><!-- space -->
                <fo:table-column column-width="5%" /><!-- value -->
                <fo:table-column column-width="8%" /><!-- 11Mo. -->
                <fo:table-column column-width="5%" /><!-- space -->
                <fo:table-column column-width="9%" /><!-- Normal: -->
                <fo:table-column column-width="5%" /><!-- value -->
                <fo:table-column column-width="5%" /><!-- space -->
                <fo:table-column column-width="15%" /><!-- Accel.(Years) -->
                <fo:table-column column-width="5%" /><!-- value -->
                <fo:table-column column-width="5%" /><!-- space -->
                <fo:table-column column-width="15%" /><!-- Decel.(Years) -->
                <fo:table-column column-width="5%" /><!-- value -->
                <fo:table-body>
                    <!-- x marked row -->
                    <fo:table-row font-size="12pt" font-weight="normal" text-align="center">
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block>
                                <xsl:if test="/raf/appointmenttype9mo"><xsl:text>x</xsl:text></xsl:if>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block>
                                <xsl:text>9 Mo.</xsl:text>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block /></fo:table-cell>
    
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block>
                                <xsl:if test="/raf/appointmenttype11mo">x</xsl:if>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block>
                                <xsl:text>11 Mo.</xsl:text>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block /></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block>
                                <xsl:text>Normal:</xsl:text>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block>
                                <xsl:if test="/raf/normal"><xsl:text>x</xsl:text></xsl:if>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block /></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block>
                                <xsl:text>Accel. (Years):</xsl:text>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block>
                                <xsl:if test="/raf/accelerationyears">
                                    <xsl:value-of select="number(/raf/accelerationyears)" />
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block /></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block>
                                <xsl:text>Decel. (Years):</xsl:text>
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block>
                                <xsl:if test="/raf/decelerationyears">
                                    <xsl:value-of select="number(/raf/decelerationyears)" />
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template name="yearsAtRankAndStep">
        <xsl:variable name="rank" select="/raf/yearsatrank" />
        <xsl:variable name="step" select="/raf/yearsatstep" />
        
        <fo:block space-after="9pt">
            <fo:table table-layout="auto" width="100%" space-before="2pt">
                <fo:table-column column-width="1%" />
                <fo:table-column column-width="15%" />
                <fo:table-column column-width="5%" />
                <fo:table-column column-width="5%" />
                <fo:table-column column-width="15%" />
                <fo:table-column column-width="5%" />
                <fo:table-column column-width="54%" />
                <fo:table-body>
                    <fo:table-row font-size="11pt" font-weight="normal" text-align="center">
                        <fo:table-cell><fo:block /></fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block><xsl:text>Years at Rank:</xsl:text></fo:block>
                        </fo:table-cell>
    
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block>
                                <xsl:value-of select="number(/raf/yearsatrank)" />
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block /></fo:table-cell>
    
                        <fo:table-cell>
                            <fo:block><xsl:text>Years at Step:</xsl:text></fo:block>
                        </fo:table-cell>
    
                        <fo:table-cell border-bottom-style="solid">
                            <fo:block>
                                <xsl:value-of select="number(/raf/yearsatstep)" />
                            </fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell><fo:block/></fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="departments">
        <fo:block space-after="9pt">
            <fo:table table-layout="auto" width="100%" border-top="black" border-bottom="black" space-before="2pt">
                <fo:table-column column-width="12%" />
                <fo:table-column column-width="65%" />
                <fo:table-column column-width="3%" />
                <fo:table-column column-width="10%" />
                <fo:table-column column-width="10%" />
                
                <fo:table-body>
                    <xsl:apply-templates/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="department">
        <fo:table-row font-size="10pt">
            <fo:table-cell padding-top="10pt">
                <fo:block font-weight="bold">
                    <xsl:text>Department:</xsl:text>
                </fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding-top="10pt" border-bottom-style="solid" margin-left="5px">
                <fo:block text-align="left"><xsl:value-of select="name" /></fo:block>
            </fo:table-cell>
        
            <fo:table-cell><fo:block/></fo:table-cell>
        
            <fo:table-cell padding-top="10pt">
                <fo:block font-weight="bold">
                    <xsl:text>% of Time:</xsl:text>
                </fo:block>
            </fo:table-cell>
        
            <fo:table-cell padding-top="10pt" border-bottom-style="solid" margin-left="5px">
                    <fo:block text-align="center"><xsl:value-of select="percentoftime" /></fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="statuses|ranks">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="status">
        <fo:block space-before="10pt">
            <xsl:apply-templates select="type"/>
            
            <fo:table table-layout="auto" width="100%">
                <fo:table-column column-width="45%" />
                <fo:table-column column-width="12%" />
                <fo:table-column column-width="13%" />
                <fo:table-column column-width="16%" />
                <fo:table-column column-width="14%" />
                
                <fo:table-header>
                    <fo:table-row line-height="20pt" font-size="11pt" font-weight="bold" text-align="center">
                        <fo:table-cell>
                            <fo:block><xsl:text>Rank, Title &amp; Step</xsl:text></fo:block>
                        </fo:table-cell>
                        
                        <fo:table-cell>
                            <fo:block><xsl:text>% of Time</xsl:text></fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block><xsl:text>Title Code</xsl:text></fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block><xsl:text>Monthly Salary</xsl:text></fo:block>
                        </fo:table-cell>

                        <fo:table-cell>
                            <fo:block><xsl:text>Annual Salary</xsl:text> </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-header>

                <fo:table-body>
                    <xsl:apply-templates select="ranks"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="type">
        <fo:block text-align="left" font-size="14pt" font-weight="bold" space-after="10pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="rank">
        <fo:table-row line-height="15pt" font-size="9pt" text-align="center" margin-left="3px" border-bottom-style="solid">
            <fo:table-cell>
                <fo:block text-align="left"><xsl:value-of select="rankandstep" /></fo:block>
            </fo:table-cell>
            
            <fo:table-cell>
                <fo:block><xsl:apply-templates select="percentoftime"/></fo:block>
            </fo:table-cell>
    
            <fo:table-cell>
                <fo:block><xsl:apply-templates select="titlecode" /></fo:block>
            </fo:table-cell>
    
            <fo:table-cell>
                <fo:block><xsl:apply-templates select="monthlysalary" /></fo:block>
            </fo:table-cell>
    
            <fo:table-cell>
                <fo:block><xsl:apply-templates select="annualsalary" /></fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
</xsl:stylesheet>