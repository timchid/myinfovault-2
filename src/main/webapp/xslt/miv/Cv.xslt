<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:import href="html-tags.xslt"/>
   
   <xsl:template match="/">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="myinfovault"
               page-height="11in" page-width="8.5in" margin-top="1.0in"
               margin-bottom="1.0in" margin-left="1.0in"
               margin-right="1.0in">
               <fo:region-body/>
               <fo:region-after/>
            </fo:simple-page-master>
         </fo:layout-master-set>
         <fo:page-sequence master-reference="myinfovault">
            <fo:static-content flow-name="xsl-region-after">
                <fo:block text-align="center">
                    <fo:page-number />
                </fo:block>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:apply-templates />
            </fo:flow>
         </fo:page-sequence>
      </fo:root>
   </xsl:template>

   <xsl:template match="packet">
       <fo:block font-size="20pt" font-weight="bold" space-after="16pt" text-decoration="underline" text-align="left">
           CURRICULUM VITAE
       </fo:block>
       <fo:block text-align="left"
                font-size="16pt"
                font-weight="bold"
		text-decoration="underline" 
                space-after="16pt">PERSONAL</fo:block>
       
       <fo:table table-layout="fixed" width="100%">
           <fo:table-column column-width="20%" />
           <fo:table-column column-width="80%" />
           <fo:table-body>
               <xsl:apply-templates select="personal" />
               <xsl:apply-templates select="personal-namerecord" />
               <xsl:apply-templates select="personal-address" />
               <xsl:apply-templates select="personal-phone" />
               <xsl:apply-templates select="personal-email" />
               <xsl:apply-templates select="personal-link" />
           </fo:table-body>
       </fo:table>

       
       <xsl:apply-templates select="personal-additional"/>

       <fo:block text-align="left"
                font-size="16pt"
                font-weight="bold"
		text-decoration="underline" 
                space-after="16pt">HONORS AND AWARDS</fo:block>
       <fo:block><xsl:apply-templates select="honors"/></fo:block>

       <fo:block text-align="left"
                font-size="16pt"
                font-weight="bold"
		 text-decoration="underline" 
                space-after="16pt">PUBLICATIONS</fo:block>
       <fo:block id="publications" font-size="12pt" font-family="Helvetica">
           <xsl:apply-templates select="journals-published" />
           <xsl:apply-templates select="journals-in-press" />
           <xsl:apply-templates select="journals-submitted" />
           <xsl:apply-templates select="book-chapters-published" />
           <xsl:apply-templates select="book-chapters-in-press" />
           <xsl:apply-templates select="book-chapters-submitted" />
           <xsl:apply-templates select="editor-letters-published" />
           <xsl:apply-templates select="editor-letters-in-press" />
           <xsl:apply-templates select="editor-letters-submitted" />
           <xsl:apply-templates select="books-edited-published" />
           <xsl:apply-templates select="books-edited-in-press" />
           <xsl:apply-templates select="books-edited-submitted" />
           <xsl:apply-templates select="reviews-published" />
           <xsl:apply-templates select="reviews-in-press" />
           <xsl:apply-templates select="reviews-submitted" />
           <xsl:apply-templates select="books-authored-published" />
           <xsl:apply-templates select="books-authored-in-press" />
           <xsl:apply-templates select="books-authored-submitted" />
           <xsl:apply-templates select="limited-published" />
           <xsl:apply-templates select="limited-in-press" />
           <xsl:apply-templates select="limited-submitted" />
           <xsl:apply-templates select="media-published" />
           <xsl:apply-templates select="media-in-press" />
           <xsl:apply-templates select="media-submitted" />
           <xsl:apply-templates select="abstracts-published" />
           <xsl:apply-templates select="abstracts-in-press" />
           <xsl:apply-templates select="abstracts-submitted" />
           <xsl:apply-templates select="presentations" />
           <xsl:apply-templates select="publication-additional" />
       </fo:block>

       <xsl:apply-templates select="grants"/>
       <fo:block><xsl:apply-templates select="grants/grant-record"/></fo:block>

       <fo:block id="teaching"
                text-align="left"
                font-size="16pt"
                font-weight="bold"
		text-decoration="underline" 
                space-after="16pt">TEACHING</fo:block>
       <fo:block><xsl:apply-templates select="teaching-courses"/></fo:block>
       <xsl:apply-templates select="teaching-trainees"/>
       <xsl:apply-templates select="teaching-supplement-term"/>
       <xsl:apply-templates select="teaching-supplement-month"/>
       <xsl:apply-templates select="teaching-additional"/>

       <fo:block id="service"
                text-align="left"
                font-size="16pt"
                font-weight="bold"
		text-decoration="underline"
                space-after="16pt">SERVICE</fo:block>
      <fo:block>
         <xsl:apply-templates select="service-department"/>
         <xsl:apply-templates select="service-school"/>
         <xsl:apply-templates select="service-campus"/>
         <xsl:apply-templates select="service-systemwide"/>
         <xsl:apply-templates select="service-other-university"/>
         <xsl:apply-templates select="service-other-nonuniversity"/>
         <xsl:apply-templates select="service-board"/>
         <xsl:apply-templates select="service-administrative"/>
         <xsl:apply-templates select="service-additional"/>
      </fo:block>

   </xsl:template>
   
   <!-- PERSONAL -->
   
   <xsl:template match="personal">      
      <xsl:apply-templates select="personal-record" />
   </xsl:template>
   <xsl:template match="personal-address">
      <xsl:apply-templates select="address-record" />
   </xsl:template>
   <xsl:template match="personal-phone">
      <xsl:apply-templates select="phone-record" />
   </xsl:template>
   <xsl:template match="personal-email">
      <xsl:apply-templates select="email-record" />
   </xsl:template>
   <xsl:template match="personal-link">
      <xsl:apply-templates select="link-record" />
   </xsl:template>
   <xsl:template match="personal-additional">
      <xsl:apply-templates select="additional-record" />
   </xsl:template>


   <xsl:template match="personal-namerecord">
       <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block>Name:</fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="fname" />
		   <xsl:apply-templates select="lname" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
    
   <xsl:template match="personal-record">
       <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block>Birth Date:</fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="birthdate" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <xsl:template match="address-record">
       <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block><xsl:apply-templates select="addresstype" />:</fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="street1" />
                   <xsl:apply-templates select="street2" />
                   <xsl:apply-templates select="city" />
                   <xsl:apply-templates select="state" />
                   <xsl:apply-templates select="zipcode" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <xsl:template match="phone-record">
       <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block><xsl:apply-templates select="phonetype" />:</fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="phonenumber" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <xsl:template match="email-record">
      <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block>E-mail:</fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="value" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
   
   <xsl:template match="link-record">
      <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block>Web Link:</fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="value" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
   
   <!-- HONORS -->
   
   <xsl:template match="honors">
      <fo:block id="honors">
         <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="80%" />
            <fo:table-body>
               <xsl:apply-templates select="honor-record" />
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>

   <!-- HONORS AND AWARDS -->
   <xsl:template match="honor-record">
       <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="year" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="description" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <!-- JOURNALS -->
   
   <xsl:template match="journals-published">
      <fo:block id="journals-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="journal-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="journals-in-press">
      <fo:block  id="journals-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="journal-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="journals-submitted">
      <fo:block id="journals-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="journal-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="journal-record">
      <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- BOOK CHAPTERS -->
   
   <xsl:template match="book-chapters-published">
      <fo:block id="book-chapters-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-chapter-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="book-chapters-in-press">
      <fo:block id="book-chapters-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-chapter-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="book-chapters-submitted">
      <fo:block id="book-chapters-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-chapter-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="book-chapter-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- LETTERS TO THE EDITOR -->
   
   <xsl:template match="editor-letters-published">
      <fo:block id="editor-letters-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="letter-to-editor-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="editor-letters-in-press">
      <fo:block id="editor-letters-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="letter-to-editor-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="editor-letters-submitted">
      <fo:block id="editor-letters-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="letter-to-editor-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="letter-to-editor-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- BOOKS EDITED -->
   
   <xsl:template match="books-edited-published">
      <fo:block id="books-edited-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-editor-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="books-edited-in-press">
      <fo:block id="books-edited-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-editor-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="books-edited-submitted">
      <fo:block id="books-edited-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-editor-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="book-editor-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- BOOK REVIEWS -->
   
   <xsl:template match="reviews-published">
      <fo:block id="reviews-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="review-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="reviews-in-press">
      <fo:block id="reviews-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="review-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="reviews-submitted">
      <fo:block id="reviews-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="review-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="review-record">
       <xsl:call-template name="record" />
   </xsl:template>

   <!-- BOOKS AUTHORED -->
   
   <xsl:template match="books-authored-published">
      <fo:block id="books-authored-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-author-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="books-authored-in-press">
      <fo:block id="books-authored-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-author-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="books-authored-submitted">
      <fo:block id="books-authored-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="book-author-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="book-author-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- LIMITED DISTRIBUTION -->
   
   <xsl:template match="limited-published">
      <fo:block id="limited-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="limited-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="limited-in-press">
      <fo:block id="limited-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="limited-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="limited-submitted">
      <fo:block id="limited-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="limited-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="limited-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- ALTERNATIVE MEDIA -->
   
   <xsl:template match="media-published">
      <fo:block id="media-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="media-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="media-in-press">
      <fo:block id="media-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="media-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="media-submitted">
      <fo:block id="media-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="media-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="media-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- ABSTRACTS -->
   
   <xsl:template match="abstracts-published">
      <fo:block id="abstracts-published">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="abstract-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="abstracts-in-press">
      <fo:block id="abstracts-in-press">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="abstract-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="abstracts-submitted">
      <fo:block id="abstracts-submitted">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="abstract-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="abstract-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- PRESENTATIONS -->
   
   <xsl:template match="presentations">
      <fo:block id="presentations">
         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
         <xsl:apply-templates select="presentation-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="presentation-record">
       <xsl:call-template name="record" />
   </xsl:template>
   
   <!-- ADDITIONAL INFORMATION -->
   
   <xsl:template match="publication-additional">
      
      <!-- ADDITIONAL INFORMATION -->
      <fo:block id="publication-additional">
         <xsl:apply-templates select="additional-record" />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="additional-record">
       <fo:block padding-bottom="12pt">
           <xsl:apply-templates select="addheader" />
           <xsl:apply-templates select="addcontent" />
           <fo:table table-layout="fixed" width="100%">
               <xsl:choose>
                   <xsl:when test="colpercent = 5">
                       <fo:table-column column-width="5%" />
                       <fo:table-column column-width="95%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 10">
                       <fo:table-column column-width="10%" />
                       <fo:table-column column-width="90%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 15">
                       <fo:table-column column-width="15%" />
                       <fo:table-column column-width="85%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 20">
                       <fo:table-column column-width="20%" />
                       <fo:table-column column-width="80%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 30">
                       <fo:table-column column-width="30%" />
                       <fo:table-column column-width="70%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 35">
                       <fo:table-column column-width="35%" />
                       <fo:table-column column-width="65%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 40">
                       <fo:table-column column-width="40%" />
                       <fo:table-column column-width="60%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 45">
                       <fo:table-column column-width="45%" />
                       <fo:table-column column-width="55%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 50">
                       <fo:table-column column-width="50%" />
                       <fo:table-column column-width="50%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 55">
                       <fo:table-column column-width="55%" />
                       <fo:table-column column-width="45%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 60">
                       <fo:table-column column-width="60%" />
                       <fo:table-column column-width="40%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 65">
                       <fo:table-column column-width="65%" />
                       <fo:table-column column-width="35%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 70">
                       <fo:table-column column-width="70%" />
                       <fo:table-column column-width="30%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 75">
                       <fo:table-column column-width="75%" />
                       <fo:table-column column-width="25%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 80">
                       <fo:table-column column-width="80%" />
                       <fo:table-column column-width="20%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 85">
                       <fo:table-column column-width="85%" />
                       <fo:table-column column-width="15%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 90">
                       <fo:table-column column-width="90%" />
                       <fo:table-column column-width="10%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 95">
                       <fo:table-column column-width="95%" />
                       <fo:table-column column-width="5%" />
                   </xsl:when>
                   <xsl:otherwise>
                       <fo:table-column column-width="25%" />
                       <fo:table-column column-width="75%" />
                   </xsl:otherwise>
               </xsl:choose>
               <fo:table-body>
                   <fo:table-row>
                       <fo:table-cell>
                           <fo:block>
                               <xsl:apply-templates select="column1" />
                           </fo:block>
                       </fo:table-cell>
                       <fo:table-cell>
                           <fo:block>
                               <xsl:apply-templates select="column2" />
                           </fo:block>
                       </fo:table-cell>
                   </fo:table-row>
               </fo:table-body>
           </fo:table>
       </fo:block>
   </xsl:template>
   
   <!-- GRANTS -->
   
   <xsl:template match="grants">
      <fo:block id="grants"
                font-size="16pt"
                font-weight="bold"
                space-after="16pt"
		text-decoration="underline"
                text-align="left">GRANTS AND CONTRACTS</fo:block>
   </xsl:template>
   
   <xsl:template match="grant-record">
      <fo:block padding-bottom="24pt" keep-together.within-page="always">
         <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="80%" />
            <fo:table-body>
               <fo:table-row>
                  <fo:table-cell padding="2pt">
                     <xsl:choose>
                  <xsl:when test="child::submitdate">
                     <fo:block><xsl:apply-templates select="submitdate"/></fo:block>
                  </xsl:when>
                  <xsl:when test="child::startdate and child::enddate">
                     <fo:block>
                        <xsl:apply-templates select="startdate"/> - <xsl:apply-templates select="enddate"/>
                     </fo:block>
                  </xsl:when>
                  <xsl:otherwise>
                     <fo:block>
                        <xsl:apply-templates select="startdate"/>
                        <xsl:apply-templates select="enddate"/>
                     </fo:block>
                  </xsl:otherwise>
                  </xsl:choose>
                  </fo:table-cell>
                  <fo:table-cell padding="2pt">
                     <fo:block><xsl:apply-templates select="grantamount"/>,
                     <xsl:apply-templates select="rolename"/>,
                     <xsl:apply-templates select="granttitle"/>,
		     <xsl:apply-templates select="grantsource"/>
                     <xsl:apply-templates select="grantdescription"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>
   
   <!-- TEACHING -->
   
   <xsl:template match="teaching-courses">
 	<fo:block id="courses"
                font-size="16pt"
                font-weight="bold"
                space-after="16pt"
		text-decoration="underline"
                text-align="left">COURSES</fo:block>
     <xsl:apply-templates select="course-record"/>
   </xsl:template>
   
   <xsl:template match="course-record">
       <fo:block>
         <fo:table table-layout="fixed"
                   width="100%"
                   space-after="10pt">
            <fo:table-column column-width="32%" />
            <fo:table-column column-width="68%" />
           
            <fo:table-body>
               <fo:table-row >
		<fo:table-cell padding="2pt">
                     <fo:block>
                        <xsl:apply-templates select="year"/> : <xsl:apply-templates select="description"/>
                     </fo:block>
                  </fo:table-cell>
 		<fo:table-cell padding="2pt">
                     <fo:block>
		    	<xsl:apply-templates select="title"/>                     
		     </fo:block>
                </fo:table-cell>
               </fo:table-row>              
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="teaching-trainees">
      <fo:block id="teaching-trainees">
         <fo:table table-layout="fixed" width="100%" border="thin solid black" space-after="10pt">
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="20%" />
            <fo:table-body>
               <fo:table-row font-weight="bold">
                  <fo:table-cell border="thin solid black" padding="5px" text-align="left">
                     <fo:block>Name</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Degree</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Trainee Type</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Years</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="left">
                     <fo:block>Current Position</fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <xsl:apply-templates select="trainee-record" />
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="trainee-record">
       <fo:table-row>
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="name" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="degree" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="type" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="year" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="position" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
   
   <xsl:template match="teaching-supplement-term">
      <fo:block id="teaching-supplement-term">
         <fo:block text-align="center"
                font-size="16pt"
                font-weight="bold"
                space-after="12pt">
            Teaching Supplement by Term
         </fo:block>
         <fo:block>
         <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="25%" />
            <fo:table-column column-width="60%" />
            <fo:table-column column-width="5%" />
            <fo:table-body>
               <fo:table-row font-weight="bold" text-decoration="underline">
                  <fo:table-cell padding="4px">
                     <fo:block>Year</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Term</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Title</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block text-align="center">Hours</fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <xsl:apply-templates select="supplement-record" />
            </fo:table-body>
         </fo:table>
         </fo:block>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="teaching-supplement-month">
      <fo:block id="teaching-supplement-month">
         <fo:block text-align="center"
                font-size="16pt"
                font-weight="bold"
                space-after="12pt">
            Teaching Supplement by Month
         </fo:block>
         <fo:block>
         <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="25%" />
            <fo:table-column column-width="60%" />
            <fo:table-column column-width="5%" />
            <fo:table-body>
               <fo:table-row font-weight="bold" text-decoration="underline">
                  <fo:table-cell padding="4px">
                     <fo:block>Year</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Month</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Title</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block text-align="center">Hours</fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <xsl:apply-templates select="supplement-record" />
            </fo:table-body>
         </fo:table>
         </fo:block>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="supplement-record">
       <fo:table-row>
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="year" />
               </fo:block>
           </fo:table-cell>
           <xsl:choose>
           <xsl:when test="monthname">
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="monthname" />
               </fo:block>
           </fo:table-cell>
           </xsl:when>
           <xsl:otherwise>
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="description" />
               </fo:block>
           </fo:table-cell>
           </xsl:otherwise>
           </xsl:choose>
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="teachingtype" />: <xsl:apply-templates select="title" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="4px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="hours" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
   
   <!-- ADDITIONAL INFORMATION -->
   
   <xsl:template match="teaching-additional">
      <fo:block id="teaching-additional">
         <xsl:apply-templates select="additional-record" />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="additional-record">
       <fo:block padding-bottom="12pt">
           <xsl:apply-templates select="addheader" />
           <xsl:apply-templates select="addcontent" />
           <fo:table table-layout="fixed" width="100%">
               <xsl:choose>
                   <xsl:when test="colpercent = 5">
                       <fo:table-column column-width="5%" />
                       <fo:table-column column-width="95%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 10">
                       <fo:table-column column-width="10%" />
                       <fo:table-column column-width="90%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 15">
                       <fo:table-column column-width="15%" />
                       <fo:table-column column-width="85%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 20">
                       <fo:table-column column-width="20%" />
                       <fo:table-column column-width="80%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 30">
                       <fo:table-column column-width="30%" />
                       <fo:table-column column-width="70%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 35">
                       <fo:table-column column-width="35%" />
                       <fo:table-column column-width="65%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 40">
                       <fo:table-column column-width="40%" />
                       <fo:table-column column-width="60%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 45">
                       <fo:table-column column-width="45%" />
                       <fo:table-column column-width="55%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 50">
                       <fo:table-column column-width="50%" />
                       <fo:table-column column-width="50%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 55">
                       <fo:table-column column-width="55%" />
                       <fo:table-column column-width="45%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 60">
                       <fo:table-column column-width="60%" />
                       <fo:table-column column-width="40%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 65">
                       <fo:table-column column-width="65%" />
                       <fo:table-column column-width="35%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 70">
                       <fo:table-column column-width="70%" />
                       <fo:table-column column-width="30%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 75">
                       <fo:table-column column-width="75%" />
                       <fo:table-column column-width="25%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 80">
                       <fo:table-column column-width="80%" />
                       <fo:table-column column-width="20%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 85">
                       <fo:table-column column-width="85%" />
                       <fo:table-column column-width="15%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 90">
                       <fo:table-column column-width="90%" />
                       <fo:table-column column-width="10%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 95">
                       <fo:table-column column-width="95%" />
                       <fo:table-column column-width="5%" />
                   </xsl:when>
                   <xsl:otherwise>
                       <fo:table-column column-width="25%" />
                       <fo:table-column column-width="75%" />
                   </xsl:otherwise>
               </xsl:choose>
               <fo:table-body>
                   <fo:table-row>
                       <fo:table-cell>
                           <fo:block>
                               <xsl:apply-templates select="column1" />
                           </fo:block>
                       </fo:table-cell>
                       <fo:table-cell>
                           <fo:block>
                               <xsl:apply-templates select="column2" />
                           </fo:block>
                       </fo:table-cell>
                   </fo:table-row>
               </fo:table-body>
           </fo:table>
       </fo:block>
   </xsl:template>
   
   <!-- SERVICE -->


   <xsl:template match="role">
      <fo:inline><xsl:apply-templates />&#160;- </fo:inline>
   </xsl:template>
	


   
   <xsl:template match="service-department">
      <fo:block space-after="24pt">
         <fo:block id="service-department"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">Department/Section</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-school">
      <fo:block space-after="24pt">
         <fo:block id="service-school"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">School/College/Division</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-campus">
      <fo:block space-after="24pt">
         <fo:block id="service-campus"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">Campus</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-systemwide">
      <fo:block space-after="24pt">
         <fo:block id="service-systemwide"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">Systemwide</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-other-university">
      <fo:block space-after="24pt">
         <fo:block id="service-other-university"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">Other University</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-other-nonuniversity">
      <fo:block space-after="24pt">
         <fo:block id="service-other-nonuniversity"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">Other Non-university</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-board">
      <fo:block space-after="24pt">
         <fo:block id="service-board"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">Editorial and Advisory Boards</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-administrative">
      <fo:block space-after="24pt">
         <fo:block id="service-administrative"
                   font-weight="bold"
                   text-decoration="underline"
                   space-after="12pt">Administrative Activities</fo:block>
         <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="service-record">
      <fo:block padding-bottom="12pt" keep-together.within-page="always">
         <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="80%" />
            <fo:table-body>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>
                        <xsl:apply-templates select="year"/>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block>
                        <xsl:apply-templates select="role" />
                        <xsl:apply-templates select="description" />
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>
   
   <!-- ADDITIONAL INFORMATION -->
   
   <xsl:template match="service-additional">
      <fo:block id="service-additional">
         <xsl:apply-templates select="additional-record" />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="additional-record">
       <fo:block padding-bottom="12pt">
           <xsl:apply-templates select="addheader" />
           <xsl:apply-templates select="addcontent" />
           <fo:table table-layout="fixed" width="100%">
               <xsl:choose>
                   <xsl:when test="colpercent = 5">
                       <fo:table-column column-width="5%" />
                       <fo:table-column column-width="95%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 10">
                       <fo:table-column column-width="10%" />
                       <fo:table-column column-width="90%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 15">
                       <fo:table-column column-width="15%" />
                       <fo:table-column column-width="85%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 20">
                       <fo:table-column column-width="20%" />
                       <fo:table-column column-width="80%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 30">
                       <fo:table-column column-width="30%" />
                       <fo:table-column column-width="70%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 35">
                       <fo:table-column column-width="35%" />
                       <fo:table-column column-width="65%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 40">
                       <fo:table-column column-width="40%" />
                       <fo:table-column column-width="60%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 45">
                       <fo:table-column column-width="45%" />
                       <fo:table-column column-width="55%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 50">
                       <fo:table-column column-width="50%" />
                       <fo:table-column column-width="50%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 55">
                       <fo:table-column column-width="55%" />
                       <fo:table-column column-width="45%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 60">
                       <fo:table-column column-width="60%" />
                       <fo:table-column column-width="40%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 65">
                       <fo:table-column column-width="65%" />
                       <fo:table-column column-width="35%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 70">
                       <fo:table-column column-width="70%" />
                       <fo:table-column column-width="30%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 75">
                       <fo:table-column column-width="75%" />
                       <fo:table-column column-width="25%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 80">
                       <fo:table-column column-width="80%" />
                       <fo:table-column column-width="20%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 85">
                       <fo:table-column column-width="85%" />
                       <fo:table-column column-width="15%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 90">
                       <fo:table-column column-width="90%" />
                       <fo:table-column column-width="10%" />
                   </xsl:when>
                   <xsl:when test="colpercent = 95">
                       <fo:table-column column-width="95%" />
                       <fo:table-column column-width="5%" />
                   </xsl:when>
                   <xsl:otherwise>
                       <fo:table-column column-width="25%" />
                       <fo:table-column column-width="75%" />
                   </xsl:otherwise>
               </xsl:choose>
               <fo:table-body>
                   <fo:table-row>
                       <fo:table-cell>
                           <fo:block>
                               <xsl:apply-templates select="column1" />
                           </fo:block>
                       </fo:table-cell>
                       <fo:table-cell>
                           <fo:block>
                               <xsl:apply-templates select="column2" />
                           </fo:block>
                       </fo:table-cell>
                   </fo:table-row>
               </fo:table-body>
           </fo:table>
       </fo:block>
   </xsl:template>

   <!-- NAMED TEMPLATES -->
   
   <xsl:template name="record">
       <fo:block padding-bottom="12pt" keep-together.within-page="always">
           <!-- table-layout="auto" is currently not supported by FOP  -->
           <fo:table table-layout="fixed" width="100%">
               <fo:table-column column-width="20%" />               
               <fo:table-column column-width="80%" />
               <fo:table-body>
                   <fo:table-row>
                       <fo:table-cell>
                           <fo:block>
                               <xsl:apply-templates select="number" />.                         
                               <xsl:apply-templates select="year" />
                           </fo:block>
                       </fo:table-cell>
                       <fo:table-cell>
                           <xsl:choose>
                               <xsl:when test="descendant::citation">
                                   <fo:block>
                                       <xsl:apply-templates select="citation" />
                                   </fo:block>
                               </xsl:when>
                               <xsl:otherwise>
                                   <fo:block>
                                       <xsl:apply-templates select="monthname" />
                                       <xsl:apply-templates select="author" />
                                       <xsl:apply-personaltemplates select="title" />
                                       <xsl:apply-templates select="editor" />
                                       <xsl:apply-templates select="journal" />
                                       <xsl:apply-templates select="volume" />
                                       <xsl:apply-templates select="issue" />
                                       <xsl:apply-templates select="publisher" />
                                       <xsl:apply-templates select="city" />
                                       <xsl:apply-templates select="pages" />
                                       <xsl:apply-templates select="locationdescription" />.
                                   </fo:block>
                               </xsl:otherwise>
                           </xsl:choose>
                       </fo:table-cell>
                   </fo:table-row>
                   <xsl:if test="link">
                   <fo:table-row>
                       <fo:table-cell>
                           <fo:block></fo:block>
                       </fo:table-cell>
                       <fo:table-cell>
                           <fo:block></fo:block>
                       </fo:table-cell>
                       <fo:table-cell>
                           <fo:block text-decoration="underline">
                               <fo:basic-link>
                               <xsl:attribute name="external-destination">
                                  <xsl:value-of select="link"/>
                               </xsl:attribute>
                               Click Here for Article
                               </fo:basic-link>
                           </fo:block>
                       </fo:table-cell>
                   </fo:table-row>
                   </xsl:if>
               </fo:table-body>
           </fo:table>
       </fo:block>
   </xsl:template>




   
   <!-- PERSONAL DATA TAGS -->

   <xsl:template match="addresstype">
      <fo:inline><xsl:apply-templates />&#160;</fo:inline>
   </xsl:template>
   
   <xsl:template match="street1">
      <fo:inline><xsl:apply-templates />&#160;</fo:inline>
   </xsl:template>
   
   <xsl:template match="street2">
      <fo:inline><xsl:apply-templates />,&#160;</fo:inline>
   </xsl:template>
   
   <xsl:template match="city">
      <fo:inline><xsl:apply-templates />,&#160;</fo:inline>
   </xsl:template>
   
   <xsl:template match="state">
      <fo:inline><xsl:apply-templates />&#160;</fo:inline>
   </xsl:template>
   
   <xsl:template match="zipcode">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>


   <xsl:template match="phonetype">
      <fo:inline><xsl:apply-templates />&#160;</fo:inline>
   </xsl:template>


   
   <!-- HONOR DATA TAGS -->
   
   <xsl:template match="description">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>

   <!-- PUBLICATION DATA TAGS -->
   
   <xsl:template match="number">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="year">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>

   <xsl:template match="author">
      <fo:inline>
      <xsl:choose>
         <xsl:when test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record"><xsl:apply-templates />. </xsl:when>
         <xsl:when test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record | parent::limited-record"><xsl:apply-templates />: </xsl:when>
      </xsl:choose>
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="contributor">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="editor">
      <fo:inline>, <xsl:apply-templates />, (ed)</fo:inline>
   </xsl:template>

   <xsl:template match="title">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="journal">
      <fo:inline>
      <xsl:choose>
         <xsl:when test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record">. <xsl:apply-templates /></xsl:when>
         <xsl:when test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record | parent::limited-record">, <xsl:apply-templates /></xsl:when>
      </xsl:choose>
      </fo:inline>
   </xsl:template>

   <xsl:template match="publisher">
      <fo:inline>, <xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="city">
      <fo:inline>, <xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="volume">
      <fo:inline>
      <xsl:choose>
         <xsl:when test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record">, <xsl:apply-templates /></xsl:when>
         <xsl:when test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record | parent::limited-record">, Vol. <xsl:apply-templates /></xsl:when>
      </xsl:choose>
      </fo:inline>
   </xsl:template>

   <xsl:template match="issue">
      <fo:inline>(<xsl:apply-templates />)</fo:inline>
   </xsl:template>

   <xsl:template match="pages">
      <fo:inline>
      <xsl:choose>
         <xsl:when test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record">: <xsl:apply-templates /></xsl:when>
         <xsl:when test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record">. pp. <xsl:apply-templates /></xsl:when>
         <xsl:otherwise>, pp. <xsl:apply-templates /></xsl:otherwise>
      </xsl:choose>
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="monthname">
      <xsl:if test="text() != 'None'">
      <fo:inline><xsl:apply-templates />, </fo:inline>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="locationdescription">
      <fo:inline>, <xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="addheader">
      <fo:block font-weight="bold" text-decoration="underline" padding-top="12pt">
          <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="addcontent">
      <fo:block padding-top="12pt">
          <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="column1">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="column2">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <!-- GRANT DATA TAGS -->
   
   <xsl:template match="granttype">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="granttitle">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="grantdescription">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="grantsource">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="grantamount">
      <fo:inline>$<xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="submitdate">
      <xsl:apply-templates />
   </xsl:template>
   
   <xsl:template match="startdate">
      <xsl:apply-templates />
   </xsl:template>
   
   <xsl:template match="enddate">
      <xsl:apply-templates />
   </xsl:template>
   
   <xsl:template match="principalinvestigator">
      <fo:inline>, <xsl:apply-templates />&#160;(Principal Investigator)</fo:inline>
   </xsl:template>
   
   <xsl:template match="statusname">
      <fo:block><xsl:apply-templates /></fo:block>
   </xsl:template>
   
   <xsl:template match="rolename">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <!-- TEACHING DATA TAGS -->
   
   <xsl:template match="coursenumber">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="units">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="teachingtype">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>

   <xsl:template match="symbol">
      <fo:inline font-size="9pt" font-family="Symbol">
         <xsl:value-of select="."/>
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="highlight">
       <xsl:choose>
           <xsl:when test="@bold='1' and @italic='0' and @underline='0'">
               <fo:inline font-weight="bold">
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
           <xsl:when test="@bold='0' and @italic='0' and @underline='1'">
               <fo:inline text-decoration="underline">
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
           <xsl:when test="@bold='0' and @italic='1' and @underline='0'">
               <fo:inline font-style="italic">
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
           <xsl:when test="@bold='1' and @italic='0' and @underline='1'">
               <fo:inline font-weight="bold" text-decoration="underline">
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
           <xsl:when test="@bold='1' and @italic='1' and @underline='0'">
               <fo:inline font-weight="bold" font-style="italic">
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
           <xsl:when test="@bold='0' and @italic='1' and @underline='1'">
               <fo:inline text-decoration="underline" font-style="italic">
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
           <xsl:when test="@bold='0' and @italic='0' and @underline='0'">
               <fo:inline>
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
           <xsl:when test="@bold='1' and @italic='1' and @underline='1'">
               <fo:inline font-weight="bold" text-decoration="underline" font-style="italic">
                   <xsl:value-of select="." />
               </fo:inline>
           </xsl:when>
       </xsl:choose>
   </xsl:template>

</xsl:stylesheet>
