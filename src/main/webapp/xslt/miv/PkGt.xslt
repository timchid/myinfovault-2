<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY space " ">
 <!ENTITY dash "-">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:import href="html-tags.xslt"/>
    <xsl:import href="mivCommon.xslt"/>
   
    <!-- Define the document name. -->
    <xsl:variable name="documentName" select="'Grants and Contracts'"/>
  
    <!-- document setup is done in mivCommon.xslt -->
    <xsl:template match="/">
        <xsl:call-template name="documentSetup">
            <xsl:with-param name="documentName" select="$documentName" />
            <xsl:with-param name="hasSections" select="//grants-active | //grants-pending | //grants-completed | //grants-notawarded | //grants-none" />
        </xsl:call-template>
    </xsl:template>
   
   
    <xsl:template match="packet">
        <xsl:call-template name='maintitle'>
            <xsl:with-param name="documentName" select="$documentName" />
        </xsl:call-template>
        
        <xsl:call-template name="nameblock"/>
        
        <xsl:apply-templates select="grants-active"/>  
        <xsl:apply-templates select="grants-pending"/>
        <xsl:apply-templates select="grants-completed"/>
        <xsl:apply-templates select="grants-notawarded"/>
        <xsl:apply-templates select="grants-none"/>    
    </xsl:template>
   
    <xsl:template match="grants-active|grants-pending|grants-completed|grants-notawarded|grants-none">
        <fo:block id="{local-name()}" font-size="12pt" white-space-collapse="false" font-weight="bold" text-decoration="underline">
            <xsl:apply-templates select="section-header" />
        </fo:block>
        
        <fo:block padding-top="12pt">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%" />
                <fo:table-column column-width="80%" />
                
                <fo:table-body>
                    <xsl:apply-templates select="grant-record"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
   
    <xsl:template match="grant-record">
        <fo:table-row>
            <fo:table-cell padding="2pt">
               <fo:block font-weight="bold">Title:</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
               <fo:block><xsl:apply-templates select="granttitle"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row>
            <fo:table-cell padding="2pt">
               <fo:block font-weight="bold">Agency:</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
               <fo:block><xsl:apply-templates select="grantsource"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row>
            <fo:table-cell padding="2pt">
               <fo:block font-weight="bold">Grant No.:</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
               <fo:block><xsl:apply-templates select="grantnumber"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
     
        <fo:table-row>
            <fo:table-cell padding="2pt">
               <fo:block font-weight="bold">Amount:</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
               <fo:block><xsl:apply-templates select="grantamount"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
     
        <fo:table-row>
            <fo:table-cell padding="2pt">
               <fo:block font-weight="bold">Date(s):</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
                <xsl:call-template name="grantdate" />
            </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row>
            <fo:table-cell padding="2pt">
               <fo:block font-weight="bold">PI/Co-PI:</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
                <fo:block>
                   <xsl:apply-templates select="rolename"/>
                   <xsl:apply-templates select="principalinvestigator"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
        
        <!-- On local machine mysql returns 1 or 0 for displaydescription, on Panthro, true or false -->   
         <xsl:if test="displaydescription = '1' or displaydescription = 'true'">  
            <xsl:if test="grantdescription">
                <fo:table-row>
                    <fo:table-cell padding="2pt">
                        <fo:block font-weight="bold">Goal:</fo:block>
                    </fo:table-cell>
                    
                    <fo:table-cell padding="2pt">
                        <fo:block>
                            <xsl:apply-templates select="grantdescription"/>
                            
                            <xsl:call-template name="addPunctuation">
                                <xsl:with-param name="value" select="grantdescription" />
                                <xsl:with-param name="punctuationValue" select="'&period;'"/>
                            </xsl:call-template>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </xsl:if>
         </xsl:if>
         
        <fo:table-row>
            <fo:table-cell padding="2pt">
                <fo:block font-weight="bold">Type:</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
                <fo:block><xsl:apply-templates select="granttype"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
        
        <fo:table-row>
            <fo:table-cell padding="2pt">
                <fo:block font-weight="bold">Status:</fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding="2pt">
                <fo:block padding-bottom="24pt"><xsl:apply-templates select="statusname"/></fo:block>
            </fo:table-cell>
        </fo:table-row> 
   </xsl:template>
    
   <!-- Title,Description,Source,Amount,SubmitDate,StartDate,EndDate,PrincipalInvestigator,StatusID -->
   
   <xsl:template match="granttype|granttitle|grantdescription|grantsource|statusname|rolename">
      <xsl:apply-templates />
   </xsl:template>
   
   <xsl:template match="grantamount">
      <fo:block>$<xsl:apply-templates /></fo:block>
   </xsl:template>
   
   <xsl:template name="grantdate">
       <fo:block>
           <xsl:choose>
               <xsl:when test="child::startdate">
                   <xsl:apply-templates select="startdate" mode="grants"/>
    
                   <xsl:if test="child::enddate">
                       <xsl:text> - </xsl:text>
                       <xsl:apply-templates select="enddate" mode="grants"/>
                   </xsl:if>
               </xsl:when>
               
               <xsl:otherwise>
                   <xsl:apply-templates select="submitdate" mode="grants"/>
               </xsl:otherwise>
           </xsl:choose>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="principalinvestigator">
      <fo:inline>&comma;&space;<xsl:apply-templates />&space;(Principal Investigator)</fo:inline>
   </xsl:template>
</xsl:stylesheet>
