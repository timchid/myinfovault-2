<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:import href="html-tags.xslt" />
  <xsl:import href="mivCommon.xslt" />
  <xsl:import href="creativeactivitiescommon.xslt" />
  <xsl:import href="annotations.xslt"/>
  
  <!-- Works -->
  <xsl:template match="works-record" name="works-record">
    <fo:block padding-bottom="2pt">

<!-- This is turned off for now to not print each work type on a different page -->
<!--       <xsl:variable name="worktypedescription"> -->
<!--           <xsl:value-of select="preceding-sibling::works-record[1]/worktypedescription"/> -->
<!--       </xsl:variable> -->
    
<!--       If previous worktype is the same as the current node, print the worktype     -->
<!--       <xsl:variable name="printworktype"> -->
<!--         <xsl:choose> -->
<!--           <xsl:when test="worktypedescription = $worktypedescription"> -->
<!--             <xsl:value-of select="'false'"/> -->
<!--           </xsl:when> -->
<!--           <xsl:otherwise> -->
<!--             <xsl:value-of select="'true'"/> -->
<!--           </xsl:otherwise> -->
<!--         </xsl:choose> -->
<!--       </xsl:variable> -->

<!--       <xsl:if test="$printworktype = 'true'"> -->
<!--         If the preceding sibling is a works-record and we are changing types, we need a page break -->
<!--         <xsl:if test="preceding-sibling::works-record"> -->
<!--           <fo:block break-before="page"></fo:block> -->
<!--         </xsl:if> -->
<!--         <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"> -->
<!--          <xsl:value-of select="worktypedescription" /> -->
<!--         </fo:block> -->
<!--       </xsl:if> -->

<!--       Save the current type to use for numbering.  -->
<!--       <xsl:variable name="currenttype"> -->
<!--         <xsl:value-of select="worktypedescription" /> -->
<!--       </xsl:variable> -->
      
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="100%" />
        <fo:table-body>
          <xsl:if test="./labelbefore">
            <fo:table-row keep-with-next="always">
              <fo:table-cell>
	            <fo:block>
	              <xsl:call-template name="label">
	                <xsl:with-param name="rightalign"
	                  select="./labelbefore/@rightjustify" />
	                <xsl:with-param name="displaybefore"
	                  select="1" />
	              </xsl:call-template>
	            </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </xsl:if>
        <fo:table-row>
          <fo:table-cell>
            <fo:table table-layout="fixed" width="100%">
              <fo:table-column column-width="10%" />
              <fo:table-column column-width="10%" />
              <fo:table-column column-width="80%" />
              <fo:table-body start-indent="0pt">
                <fo:table-row>
                    <!-- Don't use <number> elements since they are sequential for the record type and -->    
                    <!-- we want the numbering to restart for each new type.-->    
                    <fo:table-cell>
                      <fo:block text-align="left">                        
                          	<xsl:apply-templates select="notation" />&space;
							<xsl:if test="descendant::footnote">&hash;</xsl:if>
							<xsl:apply-templates select="number" />&period;&space;
                             <!-- Uncomment to handle numbering when sorted by worktypedescription -->   
<!--                         <xsl:value-of select="count(preceding-sibling::works-record/worktypedescription[text() = $currenttype])+1"/>&period; -->
                      </fo:block>
                    </fo:table-cell>
                    
                    <fo:table-cell>
                      <fo:block text-align="left">
                        <xsl:apply-templates select="workyear" />
                      </fo:block>
                    </fo:table-cell>

                    <fo:table-cell>
                        <fo:block>
                          <xsl:apply-templates select="worktypedescription" />
                          <xsl:apply-templates select="creators" />
                          <xsl:apply-templates select="title" />
                        </fo:block>
                    </fo:table-cell>
                  </fo:table-row>

                  <fo:table-row>
                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>

                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>

                    <fo:table-cell>
                      <fo:block>
                        <xsl:apply-templates select="description" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>

                  <fo:table-row>
                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>

                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>

                    <xsl:choose>
                        <xsl:when test="link">
                      <fo:table-cell text-align="left">
                        <xsl:apply-templates select="link"> 
                          <xsl:with-param name="urldescription" select="'View Work Information'" />
                          <xsl:with-param name="urlvalue"><xsl:value-of select="link"/></xsl:with-param>
                        </xsl:apply-templates>
                      </fo:table-cell>
                        </xsl:when>
                        <xsl:otherwise>
                            <fo:table-cell>
                              <fo:block></fo:block>
                            </fo:table-cell>
                        </xsl:otherwise>
                    </xsl:choose>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:block>
    <fo:block space-before="10pt" space-after="10pt">
      <xsl:if test="worksevents-record">
        <xsl:apply-templates select="worksevents-record" />
      </xsl:if>
      <xsl:if test="workspublicationevents-record">
        <xsl:apply-templates select="workspublicationevents-record" />
      </xsl:if>
    </fo:block>
    <xsl:if test="./labelafter">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="100%" />
        <fo:table-body>
          <fo:table-row keep-with-next="always">
            <fo:table-cell>
	          <fo:block>
	            <xsl:call-template name="label">
                  <xsl:with-param name="rightalign"
                    select="./labelafter/@rightjustify" />
                  <xsl:with-param name="displaybefore"
                    select="0" />
	            </xsl:call-template>
	          </fo:block>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>  
    </xsl:if>
  
<!-- The below code handles footnotes when work types are printed on a different page-->
    <!-- Variables to check if we are at the end of a type section --> 
<!--       <xsl:variable name="currentworktypedescription"> -->
<!--           <xsl:value-of select="self::works-record/worktypedescription"/> -->
<!--       </xsl:variable> -->

<!--       <xsl:variable name="nextworktypedescription"> -->
<!--           <xsl:value-of select="following-sibling::works-record[1]/worktypedescription"/> -->
<!--       </xsl:variable> -->


   <!-- If we are at the end of a type section, check to print footnotes and  -->
<!--     notations for the section we are finishing up --> 
<!--   <xsl:if test="$currentworktypedescription != $nextworktypedescription"> -->
<!--     See if there are any footnotes in this section -->
<!--     <xsl:variable name="footnotepresent"> -->
<!--       <xsl:for-each -->
<!--         select="preceding-sibling::works-record/worktypedescription[text() = $currentworktypedescription]"> -->
<!--         <xsl:if test="../footnote"> -->
<!--           <xsl:value-of select="'true'" /> -->
<!--         </xsl:if> -->
<!--       </xsl:for-each> -->
<!--       <xsl:if test="self::works-record/footnote"> -->
<!--         <xsl:value-of select="'true'" /> -->
<!--       </xsl:if> -->
<!--     </xsl:variable> -->
<!--     <xsl:if test="$footnotepresent != ''"> -->
<!--       <fo:block font-size="8pt" font-style="italic" -->
<!--         space-after="1pt" font-weight="bold">Footnotes:</fo:block> -->

<!--       Get the footnotes for the preceeding entries matching the current work type -->
<!--       <xsl:for-each select="preceding-sibling::works-record/worktypedescription[text() = $currentworktypedescription]"> -->
<!--         <xsl:if test="../footnote"> -->
<!--           <fo:block font-size="8pt" font-style="italic" space-after="1pt"># -->
<!--             <xsl:value-of select="position()" />&period; -->
<!--               <fo:inline space-start="0.5em"><xsl:value-of select="../footnote" /></fo:inline>&period; -->
<!--           </fo:block> -->
<!--         </xsl:if> -->
<!--       </xsl:for-each> -->
<!--       Get the footnote for the current entry -->
<!--         <xsl:if test="self::works-record/footnote"> -->
<!--           <fo:block font-size="8pt" font-style="italic" space-after="1pt"># -->
<!--             <xsl:value-of select="count(preceding-sibling::works-record/worktypedescription[text() = $currentworktypedescription])+1" />&period; -->
<!--               <fo:inline space-start="0.5em"><xsl:value-of select="self::works-record/footnote" /></fo:inline>&period; -->
<!--           </fo:block> -->
<!--         </xsl:if> -->
<!--     </xsl:if> -->

<!--     See if there are any notations in this section -->
<!--     <xsl:variable name="notations"> -->
<!--       <xsl:for-each -->
<!--         select="preceding-sibling::works-record/worktypedescription[text() = $currentworktypedescription]"> -->
<!--         <xsl:value-of select="../notation" /> -->
<!--       </xsl:for-each> -->
<!--       <xsl:value-of select="self::works-record/notation" /> -->
<!--     </xsl:variable> -->
<!--     <xsl:if test="$notations != ''"> -->
<!--       <xsl:call-template name="notations" /> -->
<!--     </xsl:if> -->
<!--   </xsl:if> -->
  </xsl:template>
  
  <xsl:template match="worksevents-record" name="worksevents-record">
    <fo:table table-layout="fixed" width="100%">
      <fo:table-column column-width="100%" />
      <fo:table-body start-indent="35pt">
        <fo:table-row>
          <fo:table-cell>
            <fo:table table-layout="fixed" width="100%">
              <fo:table-column column-width="20%" />
              <fo:table-column column-width="80%" />
              <fo:table-body start-indent="0pt">
                <fo:table-row>
                  <fo:table-cell padding-right="5pt">
                    <fo:block text-align="right" >
                      <xsl:apply-templates select="eventstartdate" />
                      <xsl:if test="descendant::eventenddate">
                    	<fo:block>
                    	<fo:inline>
                    		<xsl:text>to&space;</xsl:text>
                    	</fo:inline>
                    	<xsl:apply-templates select="eventenddate" />
                    	</fo:block>
                      </xsl:if>    
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding-right="5pt">
                    <fo:block>
                     <xsl:apply-templates select="eventtype" />
                     <xsl:apply-templates select="eventdescription" />
                     <xsl:apply-templates select="venue" />
                     <xsl:apply-templates select="venuedescription" />
                     <xsl:apply-templates select="city" />
                     <xsl:apply-templates select="province" />
                    </fo:block>
                   </fo:table-cell>
                </fo:table-row>
              </fo:table-body>
            </fo:table>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>
  
  <xsl:template match="workspublicationevents-record" name="workspublicationevents-record">
    <fo:table table-layout="fixed" width="100%">
      <fo:table-column column-width="100%" />
      <fo:table-body start-indent="35pt">
        <fo:table-row>
          <fo:table-cell>
            <fo:table table-layout="fixed" width="100%">
              <fo:table-column column-width="20%" />
              <fo:table-column column-width="80%" />
              <fo:table-body start-indent="0pt">
                <fo:table-row>
                  <fo:table-cell padding-right="5pt">
                    <fo:block text-align="right">
                      <xsl:apply-templates select="year" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding-right="5pt">
                    <fo:block>
                     <xsl:apply-templates select="eventtype" />
                     <xsl:apply-templates select="title" />
                     <xsl:apply-templates select="publication" />
                     <xsl:apply-templates select="editors" />
                     <xsl:apply-templates select="volume" />
                     <xsl:apply-templates select="issue" />
                     <xsl:apply-templates select="pages" />
                     <xsl:apply-templates select="publisher" />
                     <xsl:apply-templates select="city" />
                     <xsl:apply-templates select="province" />
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </fo:table-body>
            </fo:table>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template> 
 
<!--   <xsl:template name="worksfootnotes"> -->
<!--     <xsl:param name="worktype"/> -->
<!--       <fo:block font-size="8pt" font-style="italic" -->
<!--         space-after="8pt"> -->
<!--         <fo:table table-layout="fixed" width="100%"> -->
<!--           <fo:table-column column-width="100%" /> -->
<!--           <fo:table-body> -->
<!--             <fo:table-row> -->
<!--               <fo:table-cell> -->
<!--                 <fo:block font-weight="bold">Footnotes:</fo:block> -->
<!--                   <xsl:for-each select="preceding-sibling::works-record/worktypedescription[text() = $worktype]"> -->
<!--                      <xsl:variable name="footnote"> -->
<!--                          <xsl:value-of select="../footnote" /> -->
<!--                      </xsl:variable> -->
<!--                      <xsl:if test="$footnote != ''"> -->
<!--                      <fo:block> -->
<!--                        # -->
<!--                        <xsl:value-of select="position()" />&period; -->
<!--                        <fo:inline space-start="0.5em"> -->
<!--                          <xsl:value-of select="$footnote" /> -->
<!--                        </fo:inline>&period; -->
<!--                      </fo:block> -->
<!--                      </xsl:if> -->
<!--                    </xsl:for-each> -->
<!--               </fo:table-cell> -->
<!--             </fo:table-row> -->
<!--           </fo:table-body> -->
<!--         </fo:table> -->
<!--       </fo:block> -->
<!--   </xsl:template> -->
 
</xsl:stylesheet>
