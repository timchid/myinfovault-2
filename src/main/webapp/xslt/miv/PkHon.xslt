<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:import href="html-tags.xslt"/>
   <xsl:import href="mivCommon.xslt"/>
   
   <!-- Define the document name. -->
  <xsl:variable name="documentName" select="'Honors and Awards'"/>
  
  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
    <xsl:call-template name="documentSetup">
        <xsl:with-param name="documentName" select="$documentName" />
        <xsl:with-param name="hasSections" select="//honor-record" />
    </xsl:call-template>
</xsl:template>

  <xsl:template match="packet">
     <xsl:call-template name='maintitle'>
        <xsl:with-param name="documentName" select="$documentName" />
      </xsl:call-template>
      <xsl:call-template name="nameblock"/>
      <fo:block><xsl:apply-templates select="honors"/></fo:block>
   </xsl:template>
   
   <xsl:template match="honors">
      <fo:block id="honors">
         <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="25%" />
            <fo:table-column column-width="75%" />
            <fo:table-body>
               <fo:table-row font-weight="bold" text-decoration="underline">
                  <fo:table-cell padding="5px">
                     <fo:block>Year</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="5px">
                     <fo:block>Description</fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <xsl:apply-templates select="honor-record" />
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>

   <!-- HONORS AND AWARDS -->
   <xsl:template match="honor-record">
       <fo:table-row>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="year" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px">
               <fo:block>
                   <xsl:apply-templates select="description" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>
   
   <xsl:template match="year|description">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="symbol">
      <fo:inline font-size="9pt" font-family="Symbol">
         <xsl:value-of select="."/>
      </fo:inline>
   </xsl:template>
</xsl:stylesheet>