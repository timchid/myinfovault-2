<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
<!ENTITY cr "&#x0A;">
<!ENTITY space " ">
<!ENTITY period ".">
<!ENTITY comma ",">
<!ENTITY qmark "?">
<!ENTITY expoint "!">
]>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="html-tags.xslt"/>
    <xsl:import href="mivDocSetup.xslt"/>
    <xsl:import href="signature.xslt"/>

    <!-- document setup is done in mivDocSetup.xslt -->
    <xsl:template match="/">
        <xsl:call-template name="documentSetup">
            <xsl:with-param name="documentName" select="/dc/documentname" />
        </xsl:call-template>
    </xsl:template>

    <!-- starting main document body template -->
    <xsl:template match="dc">
        <xsl:apply-templates select="documentname"/>
        <xsl:apply-templates select="signature/emaileddate"/>
        <xsl:apply-templates select="candidatename"/>
        <xsl:apply-templates select="appointmentname"/>
        <xsl:apply-templates select="actiondescription"/>
        <xsl:apply-templates select="ranks" />
        <xsl:apply-templates select="effectivedate"/>
        <xsl:apply-templates select="additionalinfo"/>
        <xsl:apply-templates select="signature"/>
    </xsl:template>

    <xsl:template match="documentname">
        <fo:block font-size="19pt" font-weight="bold" text-align="center">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="emaileddate">
        <fo:block width="100%" font-size="11pt" text-align="right" space-before="24pt">
            <fo:inline font-weight="bold"><xsl:text>Date: </xsl:text></fo:inline>
            <fo:inline><xsl:apply-templates/></fo:inline>
        </fo:block>
    </xsl:template>

    <xsl:template match="candidatename">
        <fo:block font-size="11pt" font-weight="bold" text-align="left" space-before="12pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="appointmentname|actiondescription">
        <fo:block font-size="10pt" text-align="left">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="effectivedate">
        <fo:block font-size="10pt" font-style="italic" margin-right="5px" text-align="left" height="9pt" space-before="13pt">
            <xsl:text>This is to certify that I have reviewed all of the materials being submitted for my personnel action effective </xsl:text>
            <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
            <xsl:value-of select="'.'"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="additionalinfo">
        <fo:block font-size="10pt" font-weight="bold" space-before="10pt" text-align="left">
            Additional information:
        </fo:block>
        <fo:block font-size="10pt" font-style="italic" margin-right="5px" text-align="left" space-before="5pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <!-- template for ranks (present and proposed info) -->
    <xsl:template match="ranks">
        <fo:table table-layout="auto" width="100%" border-collapse="collapse" space-before="26pt">
            <fo:table-column column-width="3%"/>
            <fo:table-column column-width="35%"/>
            <fo:table-column column-width="13%"/>
            <fo:table-column column-width="1%"/>
            <fo:table-column column-width="35%"/>
            <fo:table-column column-width="13%"/>

            <fo:table-body>
                <fo:table-row height="11pt" font-size="10pt" font-weight="bold" text-align="left">
                    <fo:table-cell><fo:block/></fo:table-cell>

                    <fo:table-cell border-bottom-style="solid">
                        <fo:block>Present Rank, Title &amp; Step</fo:block>
                    </fo:table-cell>

                    <fo:table-cell border-bottom-style="solid">
                        <fo:block>% of Time</fo:block>
                    </fo:table-cell>

                    <fo:table-cell><fo:block/></fo:table-cell>

                    <fo:table-cell border-bottom-style="solid">
                        <fo:block>Proposed Rank, Title &amp; Step</fo:block>
                    </fo:table-cell>

                    <fo:table-cell border-bottom-style="solid">
                        <fo:block>% of Time</fo:block>
                    </fo:table-cell>
                </fo:table-row>

                <xsl:apply-templates select="rank"/>
            </fo:table-body>
        </fo:table>
    </xsl:template>

    <xsl:template match="rank">
        <fo:table-row height="16pt" font-size="10pt" text-align="left">
            <fo:table-cell padding-top="2pt" padding-left="2pt">
                <fo:block font-weight="bold">
                    <xsl:value-of select="position()"/>&period;
                </fo:block>
            </fo:table-cell>

            <fo:table-cell padding-top="2pt" padding-left="2pt">
                <fo:block><xsl:apply-templates select="presentrankandstep"/></fo:block>
            </fo:table-cell>

            <fo:table-cell padding-top="2pt" padding-left="2pt">
                <fo:block><xsl:apply-templates select="presentpercentoftime"/></fo:block>
            </fo:table-cell>

            <fo:table-cell padding-top="2pt" padding-left="2pt">
                <fo:block/>
            </fo:table-cell>

            <fo:table-cell padding-top="2pt" padding-left="2pt">
                <fo:block><xsl:apply-templates select="proposedrankandstep"/></fo:block>
            </fo:table-cell>

            <fo:table-cell padding-top="2pt" padding-left="2pt">
                <fo:block><xsl:apply-templates select="proposedpercentoftime"/></fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="presentpercentoftime|proposedpercentoftime">
        <xsl:apply-templates/>%
    </xsl:template>

    <xsl:template match="signature">
        <xsl:apply-templates select="revision"/>

        <fo:block font-size="10pt" font-style="italic" margin-right="5px" text-align="left" space-before="12pt">
            <xsl:text>I certify that I have reviewed my dossier and I have reviewed the
            department's recommendation (and redacted evaluations, if applicable).</xsl:text>
        </fo:block>

        <!-- signature template from signature.xslt -->
        <xsl:apply-imports/>

        <fo:block font-size="9pt" font-style="italic" margin-right="5px" text-align="right" space-before="14pt">
            (Must be dated same date as Department Letter or after)
        </fo:block>
    </xsl:template>

    <xsl:template match="revision">
        <fo:table table-layout="auto" width="100%" space-before="12pt">
            <fo:table-column column-width="10%"/>
            <fo:table-column column-width="90%"/>

            <fo:table-body text-align="left" font-size="10pt">
                <fo:table-row height="11pt">
                    <fo:table-cell padding-right="5px">
                        <fo:block font-weight="bold">Revised:</fo:block>
                    </fo:table-cell>

                    <fo:table-cell>
                        <fo:block>
                            <fo:inline>
                                This Disclosure Certificate supersedes the signed certificate dated
                            </fo:inline>

                            <fo:inline font-weight="bold" font-style="italic">
                                <xsl:apply-templates select="date"/>.
                            </fo:inline>
                        </fo:block>

                        <fo:block font-size="9pt" font-style="italic">
                            If this document is revised, the candidate will need to sign again.
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>

                <fo:table-row height="11pt">
                    <fo:table-cell><fo:block/></fo:table-cell>

                    <fo:table-cell padding-top="8pt">
                        <fo:block>The following changes/additions have been made:</fo:block>

                        <fo:block font-style="italic" space-before="5pt" margin-right="10pt" text-align="justify">
                           <xsl:apply-templates select="changes"/>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>
</xsl:stylesheet>