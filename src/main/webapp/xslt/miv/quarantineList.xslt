<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:template match="/">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="myinfovault"
               page-height="11in" page-width="8.5in" margin-top="1.0in"
               margin-bottom="1.0in" margin-left="1.0in"
               margin-right="1.0in">
               <fo:region-body/>
               <fo:region-after/>
            </fo:simple-page-master>
         </fo:layout-master-set>
         <fo:bookmark-tree>
            <fo:bookmark internal-destination="maintitle">
               <fo:bookmark-title>Quarantine List</fo:bookmark-title>
            </fo:bookmark>
         </fo:bookmark-tree>
         <fo:page-sequence master-reference="myinfovault">
            <fo:static-content flow-name="xsl-region-after">
               <fo:table table-layout="fixed" width="100%">
                   <fo:table-column column-width="90%" />
                   <fo:table-column column-width="10%" />
                   <fo:table-body>
                       <fo:table-row>
                           <fo:table-cell>
                               <fo:block text-align="left">Quarantine List</fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                               <fo:block text-align="right">
                                   <fo:page-number />
                               </fo:block>
                           </fo:table-cell>
                       </fo:table-row>
                   </fo:table-body>
               </fo:table>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:apply-templates />
            </fo:flow>
         </fo:page-sequence>
      </fo:root>
   </xsl:template>
   
   <xsl:template match="quarantine">
      <xsl:call-template name="maintitle"/>
      <fo:block font-size="16pt" font-weight="bold" text-align="center" space-after="12pt">
      </fo:block>
      <fo:block><xsl:apply-templates select="reportdate"/></fo:block>
      <fo:block><xsl:apply-templates select="quarantine-list"/></fo:block>
   </xsl:template>
   
   <xsl:template name="maintitle">
      <fo:block id="maintitle"
                font-size="20pt"
                font-weight="bold"
                space-after="16pt"
                text-align="center">Quarantine List</fo:block>
   </xsl:template>
   

 <xsl:template match="quarantine-list">
      <fo:block id="quarantine-list" keep-together.within-page="always">
           <xsl:apply-templates select="quarantine-record" />
      </fo:block>
   </xsl:template>

 <xsl:template match="quarantine-record">
 	<fo:table table-layout="fixed" width="100%" space-after="10pt"
 		border="thin solid black">
 		<fo:table-column column-width="100%" />
 		<fo:table-body>
 			<fo:table-row font-weight="bold">
 				<fo:table-cell border="thin solid black" padding="2px"
 					text-align="left">
 					<fo:block><xsl:value-of select="name"></xsl:value-of> (<xsl:value-of select="sectionname"></xsl:value-of>)</fo:block>
 				</fo:table-cell>
 			</fo:table-row>

 			<fo:table-row>
 				<fo:table-cell border="thin solid black" padding="2px"
 					text-align="left">
 					<fo:block>
 						<xsl:apply-templates select="contents" />
 					</fo:block>
 				</fo:table-cell>
 			</fo:table-row>
 		</fo:table-body>
 	</fo:table>
 </xsl:template>

   <xsl:template match="contents">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>

   <xsl:template match="reportdate">
      <fo:block font-size="12pt" font-weight="bold" text-align="center" space-after="12pt">
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
      </fo:block>
   </xsl:template>
   
 <!--
    find the width= attribute of all the <th> and <td>
    elements in the first <tr> of this table. They are
    in pixels, so divide by 72 to get inches
  -->
  <xsl:template match="tbody">
    <fo:table table-layout="fixed">
      <xsl:variable name="width"
        select="floor(ancestor::table/@width div 72)" />
      <xsl:variable name="align" select="ancestor::table/@align" />
      <xsl:attribute name="width">
          <xsl:value-of select="$width" />in          
     </xsl:attribute>
      <xsl:for-each select="tr[1]/th|tr[1]/td">
         <xsl:attribute name="column-width">    
              <xsl:value-of select="@width"/>
         </xsl:attribute> 
      </xsl:for-each>
      <fo:table-body>
        <xsl:apply-templates />
      </fo:table-body>

    </fo:table>
  </xsl:template>

  <!-- this one's easy; <tr> corresponds to <fo:table-row> -->
  <xsl:template match="tr">
    <fo:table-row>
      <xsl:apply-templates />
    </fo:table-row>
  </xsl:template>

  <!--
    Handle table header cells. They should be bold
    and centered by default. Look back at the containing
    <table> tag to see if a border width was specified.
  -->
  <xsl:template match="th">
    <fo:table-cell font-weight="bold" text-align="center" padding-left="2pt" padding-right="2pt">
      <xsl:if test="ancestor::table[1]/@border > 0">
        <xsl:variable name="borderwidth">
          <xsl:value-of select="ancestor::table[1]/@border" />
        </xsl:variable>
        <xsl:attribute name="border-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">"1pt"</xsl:attribute>
      </xsl:if>
      <fo:block>
        <xsl:apply-templates />
      </fo:block>
    </fo:table-cell>
  </xsl:template>

  <!--
    Handle table data cells.  Look back at the containing
    <table> tag to see if a border width was specified.
  -->
  <xsl:template match="td">
    <fo:table-cell padding-left="2pt" padding-right="2pt">
      <xsl:if test="ancestor::table/@border > 0">
        <xsl:variable name="borderwidth">
          <xsl:value-of select="ancestor::table[1]/@border" />
        </xsl:variable>
        <xsl:attribute name="border-style">solid</xsl:attribute>
        <xsl:attribute name="border-width">"1pt"</xsl:attribute>
      </xsl:if>
      <fo:block>
        <xsl:choose>
          <xsl:when test="@align='left'">
            <xsl:attribute name="text-align">start</xsl:attribute>
          </xsl:when>
          <xsl:when test="@align='center'">
            <xsl:attribute name="text-align">center</xsl:attribute>
          </xsl:when>
          <xsl:when test="@align='right'">
            <xsl:attribute name="text-align">end</xsl:attribute>
          </xsl:when>
        </xsl:choose>
        <xsl:apply-templates />
      </fo:block>
    </fo:table-cell>
  </xsl:template>

</xsl:stylesheet>