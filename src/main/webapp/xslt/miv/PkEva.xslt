<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
 
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:import href="html-tags.xslt"/>
   <xsl:import href="mivCommon.xslt"/>
   <xsl:import href="annotations.xslt"/>

    <!-- Define the document name. -->
    <xsl:variable name="documentName" select="'List of Evaluations'"/>
  
    <!-- document setup is done in mivCommon.xslt -->
    <xsl:template match="/">
      <xsl:call-template name="documentSetup">
        <xsl:with-param name="documentName" select="$documentName" />
        <xsl:with-param name="hasSections" select="//evaluation-record" />
      </xsl:call-template>
    </xsl:template>

   <xsl:template match="packet">
     <xsl:call-template name='maintitle'>
        <xsl:with-param name="documentName" select="$documentName" />
      </xsl:call-template>
      <xsl:call-template name="nameblock"/>
      <fo:block id="main"><xsl:apply-templates select="course-evaluations"/></fo:block>
      <fo:block><xsl:apply-templates select="course-evaluations/evaluation-record"/></fo:block>
   </xsl:template>
   
   <xsl:template match="course-evaluations">
       <fo:table table-layout="fixed" width="100%" space-after="12pt">
           <fo:table-column column-width="25%" />
           <fo:table-column column-width="45%" />
           <fo:table-column column-width="15%" />
           <fo:table-column column-width="15%" />
           <fo:table-body>
               <fo:table-row>
                   <fo:table-cell number-columns-spanned="2">
                       <fo:block></fo:block>
                   </fo:table-cell>
                   <fo:table-cell number-columns-spanned="2">
                       <fo:block text-align="center">------Scores------</fo:block>
                   </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                   <fo:table-cell>
                       <fo:block font-weight="bold" text-decoration="underline">Year</fo:block>
                   </fo:table-cell>
                   <fo:table-cell>
                       <fo:block font-weight="bold" text-decoration="underline">Program</fo:block>
                   </fo:table-cell>
                   <fo:table-cell>
                       <fo:block text-align="center" font-weight="bold" text-decoration="underline">Instructor</fo:block>
                   </fo:table-cell>
                   <fo:table-cell>
                       <fo:block text-align="center" font-weight="bold" text-decoration="underline">Course</fo:block>
                   </fo:table-cell>
               </fo:table-row>
           </fo:table-body>
       </fo:table>
   </xsl:template>

   <!-- COURSE EVALUATION -->
   <xsl:template match="evaluation-record">
   <fo:table table-layout="fixed" width="100%" margin-bottom="10pt">
	<fo:table-column column-width="100%" />
	<fo:table-body>
		<xsl:if test="./labelbefore">
		<fo:table-row keep-with-next="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
						<xsl:with-param name="displaybefore" select="1" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<fo:table-row>
			<fo:table-cell>
			       <fo:table table-layout="fixed" width="100%" space-after="12pt" >
			           <fo:table-column column-width="25%" />
			           <fo:table-column column-width="45%" />
			           <fo:table-column column-width="15%" />
			           <fo:table-column column-width="15%" />
			           <fo:table-body >
			               <fo:table-row keep-together.within-page="always">
			               	   <fo:table-cell padding-right="10pt">
			                       <fo:block>
			                           <xsl:apply-templates select="year" />
			                       </fo:block>
			                   </fo:table-cell>
			                   <fo:table-cell>
			                       <fo:block>
			                           <xsl:apply-templates select="course" />
			                           <xsl:if test="child::description">&colon;&space;</xsl:if>
			                           <xsl:apply-templates select="description" />
			                       </fo:block>
			                       <xsl:if test="child::enrollment">                     
				                       <fo:block>
				                           Total Enrollment:
				                           <xsl:apply-templates select="enrollment" />
				                       </fo:block>
			                       </xsl:if>
			                       <xsl:if test="child::responsetotal">                     
				                       <fo:block>
				                           Total Responses:
				                           <xsl:apply-templates select="responsetotal" />
				                       </fo:block>
			                       </xsl:if>
			                       <xsl:if test="child::percentageofreturn">                     
				                       <fo:block>
				                           % of Return:
				                           <xsl:apply-templates select="percentageofreturn" />
				                           %				                           
				                       </fo:block>
			                       </xsl:if>
			                       <fo:block>&leftparen;<xsl:apply-templates select="type" />&rightparen;
			                       </fo:block>
			                   </fo:table-cell>
			                   <fo:table-cell>
			                       <fo:block text-align="center">
			                           <xsl:apply-templates select="instructorscore" />
			                       </fo:block>
			                   </fo:table-cell>
			                   <fo:table-cell>
			                       <fo:block text-align="center">
			                           <xsl:apply-templates select="coursescore" />
			                       </fo:block>
			                   </fo:table-cell>
			               </fo:table-row>
			               <xsl:if test="link">
			                 <fo:table-row>
			                <fo:table-cell>
			                  <fo:block></fo:block>
			                </fo:table-cell>
			                <fo:table-cell>                
			                  <fo:block text-decoration="underline" color="#0033ff">
			                    <fo:basic-link>
			                      <xsl:attribute name="external-destination">
			                                    <xsl:value-of select="link" />
			                      </xsl:attribute>View this On-Line Evaluation
			                    </fo:basic-link>
			                 </fo:block>
			               </fo:table-cell>
			             </fo:table-row>
			           </xsl:if>
			         </fo:table-body>
			       </fo:table>
    			</fo:table-cell>
		</fo:table-row>
		<xsl:if test="./labelafter">
		<fo:table-row keep-with-previous="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
						<xsl:with-param name="displaybefore" select="0" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
	</fo:table-body>
   </fo:table>
   </xsl:template>

   <xsl:template match="year|course|description|responsetotal|enrollment|instructorscore|coursescore|type">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
</xsl:stylesheet>