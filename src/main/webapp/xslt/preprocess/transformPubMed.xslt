<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-system="${dtd.publications}"/>

    <xsl:template match="/">
        <document>
            <records>
                <xsl:apply-templates select="PubmedArticleSet/PubmedArticle/MedlineCitation"/>
            </records>
        </document>
    </xsl:template>

    <xsl:template match="MedlineCitation">
        <record>
            <xsl:apply-templates select="PMID"/>
            <xsl:apply-templates select="Article"/>
        </record>
    </xsl:template>
    
    <xsl:template match="PMID">
        <ref-type>2</ref-type>
        <ref-status>1</ref-status>
        <external-source sourceid="1">PubMed</external-source>
        <external-id><xsl:apply-templates /></external-id>
        <url>
            <xsl:text>http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;dopt=Citation&amp;list_uids=</xsl:text>
            <xsl:apply-templates />
        </url>
    </xsl:template>
    
    <xsl:template match="Article">
        <xsl:choose>
            <xsl:when test="@PubModel='Print' or @PubModel='Print-Electronic'">
                <xsl:apply-templates select="Journal/JournalIssue/PubDate"/>
			</xsl:when>
			<xsl:otherwise>
			    <xsl:apply-templates select="ArticleDate"/>
			</xsl:otherwise>
		</xsl:choose>
        <xsl:apply-templates select="AuthorList"/>
        <xsl:apply-templates select="ArticleTitle"/>
        <xsl:apply-templates select="Journal/Title"/>
        <xsl:apply-templates select="../MedlineJournalInfo/MedlineTA"/>
        <xsl:apply-templates select="Journal/JournalIssue/Volume"/>
        <xsl:apply-templates select="Journal/JournalIssue/Issue"/>
        <xsl:apply-templates select="Pagination/MedlinePgn"/>
        <xsl:apply-templates select="Journal/ISSN"/>
    </xsl:template>
    
    <xsl:template match="MedlineTA">
        <publisher><xsl:apply-templates /></publisher>
    </xsl:template>

    <xsl:template match="PubDate|ArticleDate">
        <date>
            <xsl:apply-templates />
        </date>
    </xsl:template>
    
    <xsl:template match="MedlineDate">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="Year">
        <year><xsl:apply-templates /></year>
    </xsl:template>
    
    <xsl:template match="Month">
        <month><xsl:apply-templates /></month>
    </xsl:template>
    
    <xsl:template match="Day">
        <day><xsl:apply-templates /></day>
    </xsl:template>
    
    <xsl:template match="AuthorList">
        <authors><xsl:apply-templates select="Author" /></authors>
    </xsl:template>
    
    <xsl:template match="Author">
        <author>
            <xsl:value-of select="LastName"/>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="Initials"/>
        </author>
    </xsl:template>

    <xsl:template match="ArticleTitle">
        <title><xsl:apply-templates /></title>
    </xsl:template>
    
    <xsl:template match="Title">
        <periodical><xsl:apply-templates /></periodical>
    </xsl:template>

    <xsl:template match="Volume">
        <volume><xsl:apply-templates /></volume>
    </xsl:template>

    <xsl:template match="Issue">
        <issue><xsl:apply-templates /></issue>
    </xsl:template>

    <xsl:template match="MedlinePgn">
        <pages><xsl:apply-templates /></pages>
    </xsl:template>

    <xsl:template match="ISSN">
        <isbn><xsl:apply-templates /></isbn>
    </xsl:template>
</xsl:stylesheet>