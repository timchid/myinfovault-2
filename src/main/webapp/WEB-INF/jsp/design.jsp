<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" buffer="none"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>${strings.pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>
    <%@ include file="apiclientutil.jspf" %>
    <script type="text/javascript">
        var mivConfig = new Object();
        mivConfig.isIE = false;
        var sectionServlet = "${constants.config.sectionheaderservlet}";

        var packetId = "${packet.packetId}";
        var packetName = "${packet.packetName}" || "New Packet";
        var userId = ${userId};
        var packetIsNew = ${packet.newPacket != false};
    </script>

    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/Packet.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/designpacket.js'/>"></script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivDesign.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/special_character2.css'/>">
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivuser.css'/>">
    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body class=tundra>
  <jsp:include page="/jsp/mivheader.jsp" />
  
   <jsp:include page="/WEB-INF/jsp/breadcrumbs.jsp">
        <jsp:param value="${strings.pageName}" name="pageTitle"/>
   </jsp:include>

  <!-- MIV Main Div -->
  <div id="main" class="designer">
    <h1>${strings.pageName}: ${packet.packetName}</h1>
    <div class="links">
      <div>
        <h2>${strings.manageAnnotationsHeader}</h2>
        <ul>
            <li><a class="annotationslink" href="<c:url value='/Annotations?annotationtype=creativeactivities&packetid=${packetid}'/>" title="${tooltips.creativeActivitiesAnnotations}">${strings.creativeActivitiesAnnotationLabel}</a></li>
            <li><a class="annotationslink" href="<c:url value='/Annotations?annotationtype=extendingknowledge&packetid=${packetid}'/>" title="${tooltips.extendingKnowledgeAnnotations}">${strings.extendingKnowledgeLabel}</a></li>
            <li><a class="annotationslink" href="<c:url value='/Annotations?annotationtype=evaluation&packetid=${packetid}'/>" title="${tooltips.evaluationsAnnotations}">${strings.evaluationsLabel}</a></li>
            <li><a class="annotationslink" href="<c:url value='/Annotations?annotationtype=publications&packetid=${packetid}'/>" title="${tooltips.publicationsAnnotations}">${strings.publicationsAnnotationLabel}</a></li>
            <li><a class="annotationslink" href="<c:url value='/Annotations?annotationtype=service&packetid=${packetid}'/>" title="${tooltips.serviceAnnotations}">${strings.servicesLabel}</a></li>
        </ul>
      </div>

      <div>
        <h2>${strings.manageFormatHeader}</h2>
        <ul>
            <li><a class="formatlink" href="<c:url value='/ManageFormatOptions?dataType=6'/>" title="${tooltips.publicationFormat}">${strings.publicationsFormatLabel}</a></li>
            <li><a class="formatlink" href="<c:url value='/ManageFormatOptions?dataType=52'/>" title="${tooltips.creativeActivitiesFormat}">${strings.creativeActivitiesFormatLabel}</a></li>
        </ul>
      </div>
    </div>

    <c:if test="${not empty success && success == true && not empty CompletedMessage}">
      <c:set var='hasMessages' scope='page' value=' class="hasmessages"'/>
    </c:if>

    <div id="CompletedMessage" ${hasMessages}>
      ${CompletedMessage}
    </div>

    <div id="loading" class="loading">Loading...</div>
    <script type="text/javascript">
      showLoadingDiv();
    </script>

    <form action="<c:url value='/packet/design'/>" id="designForm" name="designForm" method="post">

    <div class="designbuttongroup">
        <div class="buttonset left">
                <input type="submit" id="savetop" name="save" value="${strings.save}" title="${tooltips.save}" class="savepacketoptions">
                <input type="submit" id="resettop" name="reset" value="${strings.reset}" onclick="doReset()" title="${tooltips.reset}" class="reset">
        </div>
        <div class="buttonset right checkuncheck-buttongroup">
                <input type="button" title="${tooltips.checkAll}" value="${strings.checkAll}" onclick="javascript:checkUncheckAll($('#designForm'),true)" />
                <input type="button" title="${tooltips.clearChecked}" value="${strings.clearChecked}" onclick="javascript:checkUncheckAll($('#designForm'),false)" />
        </div>
    </div>

    <div id="workarea">
      <div class="printpacket">
        <div class="selectoption">${strings.packetInclusionHeader}</div>
        <div class="contentlegend">${strings.formHeader}</div>
      </div><%--
      --%><c:set var="documents" value="${documentsList}"/><%--
      --%><c:set var="i" value="0"/><%--
      --%><c:set var="cbvalue" value=""/><%--
      --%><c:forEach var="designdocument" items="${documents}"><%--
      --%><c:set var="displayOptionsList" value="${designdocument.displayOptionsList}"/>
      <div id="document${designdocument.documentIdentifier.value}">
        <div class="documenthead">
          <h2>${designdocument.name}</h2><%--
          --%><c:if test="${!empty designdocument.annotationLegend}"> <span class="annotationLegend">${designdocument.annotationLegend}</span></c:if>
                  <div class="formline buttonset right checkuncheck-buttongroup">
                          <input type="button" title="${tooltips.checkSection}" value="${strings.checkSection}" onclick="javascript:checkUncheckAll($('#document${designdocument.documentIdentifier.value} > div.documentbody'),true)" />
                          <input type="button" title="${tooltips.clearSection}" value="${strings.clearSection}" onclick="javascript:checkUncheckAll($('#document${designdocument.documentIdentifier.value} > div.documentbody'),false)" />
            </div>
        </div><!-- documenthead -->
        <div class="documentbody"><%--
        --%><c:choose><%--
         --%><c:when test="${empty displayOptionsList}"><%-- No design document sections
              TODO: NO SECTIONS TO DISPLAY! Inform the user?
         --%></c:when><%--
         --%><c:otherwise><%-- At least one design document section
          --%><c:forEach var="options" items="${displayOptionsList}">
                <c:set var="sectionId" value="${fn:endsWith(options.sectionName, 'additional')? 0 : options.sectionID}" />
                <c:set var="showHeader" value="${options.displaySectionHeader=='1' || options.displaySectionHeader=='true'}"/>
          <div class="section"><%--
            --%><c:if test="${options.showSectionHeader}"><%--
--%><c:if test="${options.recType!='committee'}">
<%-- When we have candidate statement and agstation manually entered records we don't show any section heading for them --%><%--
--%><c:if test="${options.sectionID != '1' && options.sectionID != '55' }">
<%--
  --%><c:if test="${options.showSection}">
            <div class="sectionhead"><%--
              --%><c:if test="${options.includeInDossier}">
                <span>
                <%--
                --%><c:set var="i" value="${i+1}"/><%--
                --%><c:if test="${fn:length(cbvalue)>0}"><%--
                  --%><c:set var="cbvalue" value="${cbvalue}:dcb${i},${options.sectionID},${options.recType},d,${options.printSectionHeader},USH,${options.sectionHeading}"/><%--
                --%></c:if><%--
                --%><c:if test="${fn:length(cbvalue)==0}"><%--
                  --%><c:set var="cbvalue" value="dcb${i},${options.sectionID},${options.recType},d,${options.printSectionHeader},USH,${options.sectionHeading}"/><%--
                --%></c:if><%--
--%>
                <label class="hidden" for="sectionheader${options.sectionID}">${strings.inclusionLabel}</label>
                <input type="checkbox" id="sectionheader${options.sectionID}" <%-- this is used by designpacket.js --%>
                       name="dcb${i}"
                       title="${tooltip.inclusion}"
                       value="1" ${options.printSectionHeader?" checked":""} ${options.disableSectionHeader?" disabled":""}
                       data-sectionid="${options.sectionID}">
                </span><%--
              --%></c:if>
              <h3>
                <span id="H${options.sectionID}">
                  <span id="HL${options.sectionID}" class="headertext">${options.sectionHeading}</span>
                </span><!-- end span HsectionID -->
              </h3>
              <span id="T${options.sectionID}" style="display: none;">
              </span><!-- end span TsectionID -->
              <span id ="L${options.sectionID}"><%--
                --%><c:if test="${options.showHeaderEditLink}">
                  &nbsp; <a class="headeredit" id="he${options.sectionID}" title="${strings.editHeader}"
                      href="<c:url value='/SectionHeader'>
                              <c:param name='sectionid' value='${options.sectionID}'/>
                              <c:param name='header' value='${options.sectionHeading}'/>
                            </c:url>">${strings.editHeader}</a><%--
                --%></c:if>
              </span>
            </div><!-- sectionhead --><%--
        --%></c:if><%--Section Heading Not Empty
--%></c:if><%--SectionID 1 or 55
--%></c:if><%--recordType not committee
--%><c:if test="${options.recType=='committee' && sectionHasData}"><%--
--%><c:if test="${options.showCommitteesHeading=='true'}">
<div class="sectionhead">
<h3>Committees</h3>
</div><!-- sectionhead --><%--
--%></c:if><%--
--%><c:if test="${!empty options.displaySections}">
<div class="sectionsubhead">
              <h4 class="headertext">${options.sectionHeading}</h4>
</div><!-- sectionsubhead --><%--
--%></c:if><%--
--%></c:if><%-- recordType committee
            --%></c:if><%-- showSectionHeader

    --%><c:set var="recordSets" value="${options.displaySections}"/><%--
--%><%--
            --%><c:if test="${(options.showSectionHeader == false) && (empty options.displaySections)}">
                <div class="sectionbody">
                  <em style="margin-left:1em;">${strings.emptySection}</em>
                </div><%--
            --%></c:if><%--

          --%><c:forEach var="recordSet" items="${recordSets}"><%--
            --%><c:if test="${fn:endsWith(options.sectionName, 'additional')}"><%--
                --%><c:set var="errmissing" value="ERR_MISSING"/><%--
                --%><c:choose><%--
                  --%><c:when test="${fn:containsIgnoreCase(recordSet.heading, errmissing)}"><%--
                    --%><c:set var="headervalue" value=""/><%--
                  --%></c:when><%--
                  --%><c:otherwise><%--
                    --%><c:set var="headervalue" value="${recordSet.heading}"/><%--
                  --%></c:otherwise><%--
                --%></c:choose>
              <div class="sectionsubhead"><%--
              --%><c:if test="${options.includeInDossier}">
                <span><%--
                --%><c:set var="i" value="${i+1}"/><%--
                --%><c:if test="${fn:length(cbvalue)>0}"><%--
                  --%><c:set var="cbvalue" value="${cbvalue}:dcb${i},${recordSet.recordNumber},${options.recType},d,${recordSet.showHeader},AH"/><%--
                --%></c:if><%--
                --%><c:if test="${fn:length(cbvalue)==0}"><%--
                  --%><c:set var="cbvalue" value="dcb${i},${recordSet.recordNumber},${options.recType},d,${recordSet.showHeader},AH"/><%--
                --%></c:if>
                  <label class="hidden" for="dcb${i}">${strings.inclusionLabel}</label>
                  <input type="checkbox" id="dcb${i}"
                         name="dcb${i}"
                         title="${tooltips.inclusion}"
                         value="1" ${recordSet.showHeader?" checked":""}
                         data-sectionid="${sectionId}"
                         data-recordid="${recordSet.recordNumber}">
                  <input type="hidden" name="sectionName" value="${options.sectionName}">
                </span><%--
              --%></c:if>
              <h4>
                <span id="H${recordSet.recordNumber}">
                  <span id="HL${recordSet.recordNumber}" class="headertext">${recordSet.heading}</span>
                </span><!-- end span HsectionID -->
              </h4>
              <span id="T${recordSet.recordNumber}" style="display: none;">
              </span><!-- end span TsectionID -->
              <span id ="L${recordSet.recordNumber}"><%--
                --%><c:if test="${options.showHeaderEditLink}">
                  &nbsp; <a class="addnheaderedit" id="he${recordSet.recordNumber}" title="${strings.editHeader}"
                  href="<c:url value='/SectionHeader?addheaderid=${recordSet.recordNumber}&amp;header=${recordSet.heading}'/>">${strings.editHeader}</a><%--
                --%></c:if>
              </span>
              </div><!-- sectionsubhead --><%--
            --%></c:if><%-- fn:endsWith --%><%--

            --%><div class="sectionbody"><%--
            --%><c:if test="${!empty recordSet.records}">
            <ul class="mivrecords"><%--
              --%><c:forEach var="record" items="${recordSet.records}">
              <c:set var="recordPacketKey" value="${packet.packetId}:${record.value.id}:${sectionId}" />
              <c:set var="printRec" value="${packet.newPacket}"/>
              <c:set var="printContribution" value="${packet.newPacket}"/>
              <c:set var="printGoal" value="${packet.newPacket}"/>
              <c:set var="packetUpdateTime"
                     value="${packet.modifiedTime == null ? packet.createTime : packet.modifiedTime}" />
              <c:set var="newOrChanged" value="" />
              <fmt:parseDate value="${record.value.inserttimestamp}"
                             var="createDate"
                             pattern="yyyy-MM-dd HH:mm:ss"
                             type="BOTH" />
              <c:choose>
                      <c:when test="${createDate > packetUpdateTime}">
                              <c:set var="newOrChanged" value="new" />
                      </c:when>
                      <c:when test="${createDate <= packetUpdateTime && record.value.updatetimestamp != null}">
                              <fmt:parseDate value="${record.value.updatetimestamp}"
                                             var="modifyDate"
                                             pattern="yyyy-MM-dd HH:mm:ss"
                                             type="BOTH" />
                              <c:if test="${modifyDate > packetUpdateTime}">
                                      <c:set var="newOrChanged" value="changed" />
                              </c:if>
                      </c:when>
              </c:choose>
              <c:forEach var="displayField" items="${packet.packetItemsMap[recordPacketKey].partsArray}">
                      <c:choose>
                              <c:when test="${displayField == 'display'}"><c:set var="printRec" value="${true}" /></c:when>
                              <c:when test="${displayField == 'displaycontribution'}"><c:set var="printContribution" value="${true}" /></c:when>
                              <c:when test="${displayField == 'displaydescription'}"><c:set var="printGoal" value="${true}" /></c:when>
                      </c:choose>
              </c:forEach>
              <li class="sectionlist">
                <div class="listblock">
                  <div class="printable">
                    <div class="selectoption">
                    <span class="printoption"><%--
                      --%><c:if test="${options.showDisplay}"><%--
                               when we try to update PDF upload records we need to update UserUploadDocument table, so
                               we are passing that table name as part of name and values in future this condition should
                               be handled by subtype and some variable in custom DesignPacketDisplayOptions object
                --%><c:if test="${options.sectionID == '71' || options.sectionID == '72' || options.sectionID == '99' }"><%--
                  --%><c:set var="i" value="${i+1}"/><%--
                  --%><c:if test="${fn:length(cbvalue)>0}"><%--
                    --%><c:set var="cbvalue" value="${cbvalue}:dcb${i},${record.key},${options.recType},d,${printRec},UD"/><%--
                  --%></c:if><%--
                  --%><c:if test="${fn:length(cbvalue)==0}"><%--
                    --%><c:set var="cbvalue" value="dcb${i},${record.key},${options.recType},d,${printRec},UD"/><%--
                  --%></c:if>
                  <label class="hidden" for="dcb${i}">${strings.inclusionLabel}</label>
                  <input type="checkbox"
                         id="dcb${i}"
                         name="dcb${i}"
                         class="displaycheck"
                         title="${tooltips.inclusion}"
                         value="1"
                         ${printRec? "checked" : ""}
                         data-displaycode="display"
                         data-sectionid="${sectionId}"
                         data-recordid="${record.value.id}"><%--
                --%></c:if><%--
                --%><c:if test="${options.sectionID != '71' && options.sectionID != '72' && options.sectionID != '99' }"><%--
                  --%><c:set var="i" value="${i+1}"/><%--
                  --%><c:if test="${fn:length(cbvalue)>0}"><%--
                     --%><c:set var="cbvalue" value="${cbvalue}:dcb${i},${record.key},${options.recType},d,${printRec}"/><%--
                  --%></c:if><%--
                  --%><c:if test="${fn:length(cbvalue)==0}"><%--
                    --%><c:set var="cbvalue" value="dcb${i},${record.key},${options.recType},d,${printRec}"/><%--
                  --%></c:if>
                  <label class="hidden" for="dcb${i}">${strings.inclusionLabel}</label>
                  <input type="checkbox"
                         id="dcb${i}"
                         name="dcb${i}"
                         title="${tooltips.inclusion}"
                         value="1"
                         class="displaycheck"
                         ${printRec ? "checked" : ""}
                         data-displaycode="display"
                         data-sectionid="${sectionId}"
                         data-recordid="${record.value.id}"><%--
                --%></c:if><%--
              --%></c:if><%--
              --%><c:set var="qDataSectionKey" value="${options.sectionName}::${record.key}"/><%--
              --%><c:set var="packetRecord" value="${qDataSectionRecordMap[qDataSectionKey]}"/><%--
              --%><c:if test="${packetRecord != null}">
                 <img src="<c:url value='/images/invalidHTML.gif'/>" border="0"><%--
              --%></c:if>
               </span>
               <span class="annotationMarker" title="This entry has Annotations">${record.value.annotationText}</span>
               </div><!-- selectoption -->
               <div class="record">
                  <c:if test="${not empty record.value.year && options.recType !='publicationevents'}"><div class="timeframe">${record.value.year}</div></c:if>
                  <span class="tag ${newOrChanged}">
                      ${newOrChanged}
                  </span>
                  <div class="preview">
                      ${record.value.preview}
                      <div class="record-links">
                          ${record.value.link}
                          <c:if test="${options.showAnnotationLink}"><%--
                          --%><c:if test="${fn:endsWith(options.sectionName, 'additional') != true}"><%--
                          --%>${record.value.annotationLink}
                              </c:if><%-- ends with additional
                      --%></c:if><%--showAnnotationLink --%>
                      </div>
                  </div><!-- preview -->
               </div><!-- record -->
                  </div><!-- printable -->
                  <c:if test="${not empty record.value.contribution || not empty record.value.significance || not empty record.value.contributors}"><%--
                  --%>
                  <div class="printable contribution">
                    <div class="selectoption">
                     <span class="printoption"><%--
                         --%><c:set var="valueforid" value="${i}"/><%--
                         --%><c:set var="i" value="${i+1}"/><%--

                         --%><c:if test="${fn:length(cbvalue)>0}"><%--
                            --%><c:set var="cbvalue" value="${cbvalue}:dcb${i},${record.key},${options.recType},dc,${printContribution}"/><%--
                          --%></c:if><%--
                          --%><c:if test="${fn:length(cbvalue)==0}"><%--
                            --%><c:set var="cbvalue" value="dcb${i},${record.key},${options.recType},dc,${printContribution}"/><%--
                          --%></c:if>
                      <c:choose>
                        <c:when test="${options.recType=='works'}">
                            <c:set var="inclusionLabel" value="${strings.includeJointlyCreated}"/>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${not empty record.value.contribution && not empty record.value.significance}">
                                        <c:set var="inclusionLabel" value="${strings.includeJointlyAuthoredAndResearch}"/>
                                </c:when>
                                <c:when test="${not empty record.value.contribution}">
                                        <c:set var="inclusionLabel" value="${strings.includeJointlyAuthored}"/>
                                </c:when>
                                <c:when test="${not empty record.value.significance}">
                                        <c:set var="inclusionLabel" value="${strings.includeResearch}"/>
                                </c:when>
                            </c:choose>
                        </c:otherwise>
                      </c:choose>
                      <input type="checkbox"
                             id="dcb${valueforid}contribution"
                             name="dcb${i}"
                             title="${tooltips.inclusion}"
                             value="1"
                             class="displaycheck"
                             ${printContribution ? "checked" : ""}
                             ${printRec ? "" : "disabled"}
                             class="contribution"
                             data-displaycode="displaycontribution"
                             data-sectionid="${sectionId}"
                             data-recordid="${record.value.id}">
                      <label for="dcb${valueforid}contribution">${inclusionLabel}</label>
                      </span><!-- printoption -->
                    </div><!-- selectoption -->
                    <div class="record">
                      <div id="dcb${valueforid}contributioninfo" class="${!printRec && printContribution?'showcontribution':'hidecontribution'}">${strings.mainRecordNotSelected}</div>
                    </div><!-- record -->
                  </div><!-- printable -->
                  </c:if><%-- DisplayContribution --%><%--

                  --%><c:if test="${not empty record.value.grantdescription}"><%--
                  --%><c:if test="${options.showGoals}" ><%--
                  --%>
                  <div class="printable">
                    <div class="selectoption">
                      <span class="printoption"><%--
                      --%><c:set var="valueforgoalid" value="${i}"/><%--
                      --%><c:set var="i" value="${i+1}"/><%--
                      --%><c:if test="${fn:length(cbvalue)>0}"><%--
                        --%><c:set var="cbvalue" value="${cbvalue}:dcb${i},${record.key},${options.recType},dd,${printGoal}"/><%--
                      --%></c:if><%--
                      --%><c:if test="${fn:length(cbvalue)==0}"><%--
                        --%><c:set var="cbvalue" value="dcb${i},${record.key},${options.recType},dd,${printGoal}"/><%--
                      --%></c:if>
                      <label class="hidden" for="dcb${valueforgoalid}goal">${strings.inclusionLabel}</label>
                      <input type="checkbox"
                             id="dcb${valueforgoalid}goal"
                             name="dcb${i}"
                             title="${tooltips.inclusion}"
                             value="1"
                             class="displaycheck"
                             ${printGoal ? "checked" : ""}
                             ${printRec ? "" : "disabled"}
                             data-displaycode="displaydescription"
                             data-sectionid="${sectionId}"
                             data-recordid="${record.value.id}">
                      </span><!-- printoption -->
                    </div><!-- selectoption -->
                     <div class="record">
                      <div class="contributiontitle"><span class="goallabel">&nbsp;Goal:</span> ${record.value.grantdescription}</div>
                      <div id="dcb${valueforgoalid}goalinfo" class=${!printRec && printGoal?"showcontribution":"hidecontribution"}>${strings.goalWontAppear}</div>
                     </div><!-- record -->
                  </div><!-- printable -->
                  </c:if><%-- showGoals
                  --%></c:if><%-- grant description not empty --%>
                  <div class="clearboth"></div><%-- make sure the listblock has dimensions --%>
                </div><!-- listblock -->
              </li>
              </c:forEach><%-- for each record --%>
            </ul>
            </c:if><%-- records not empty --%>
          </div><!-- sectionbody -->
          </c:forEach><%-- for each RecordSet
 --%></div><!-- section --><%--

  --%></c:forEach><%-- for each options
  --%></c:otherwise><%--
  --%></c:choose><%--
 --%></div><!-- documentbody --><%--
 --%></div><!-- document -->

 </c:forEach><%-- for each design document
--%><c:if test="${fn:length(cbvalues)>0}">
      <input type="hidden" id="cbvalues" name="cbvalues" value="${cbvalues}"><%--
--%></c:if><%--
--%><c:if test="${fn:length(cbvalues)==0}">
      <input type="hidden" id="cbvalues" name="cbvalues" value="${cbvalue}"><%--
--%></c:if><%--
--%>
      <div style="clear:both"></div><%-- make a clean finish inside the workarea --%>
    </div><!-- workarea -->

    <div class="designbuttongroup">
        <div class="buttonset left">
                <input type="submit" id="savebottom" name="save" value="${strings.save}" title="${tooltips.save}" class="savepacketoptions">
                <input type="submit" id="resetbottom" name="reset" value="${strings.reset}" onclick="doReset()" title="${tooltips.reset}" class="reset">
        </div>
        <div class="buttonset right checkuncheck-buttongroup">
                <input type="button" title="${tooltips.checkAll}" value="${strings.checkAll}" onclick="javascript:checkUncheckAll($('#designForm'),true)" />
                <input type="button" title="${tooltips.clearChecked}" value="${strings.clearChecked}" onclick="javascript:checkUncheckAll($('#designForm'),false)" />
        </div>
    </div>


    </form>
  </div><!-- main -->

  <div id="leaveConfirmDialog" class="popup-dialog">
      <p>${strings.dialogText} <span class="newMessage">${packet.newPacket ? strings.newPacketMessage : ''}</span></p>
  </div>

  <jsp:include page="/jsp/miv_small_footer.jsp" />

<script type="text/javascript">
/* <![CDATA][ */
$(document).ready(function(){
    $(".reset").each(function(index) {
        if ($(this).attr('type')!='button') {
            // type property can’t be changed using jquery
            //document.getElementById($(this).attr('id')).setAttribute('type','button'); // not working in IE
            changeInputType(document.getElementById($(this).attr('id')),"button");
        }
    });
});

/* ]]> */
</script>

</body>
</html><%--
 vim:ts=8 sw=2 et:
--%>
