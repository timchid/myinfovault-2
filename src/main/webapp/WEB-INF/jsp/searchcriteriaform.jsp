<%--
    DESCRIPTION:
        Forms fields for posting/binding to the SearchCriteria form.

    INPUT:
        * "param.searchGroups" : Searching for groups is allowed?

--%><%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="searchRole" value="${user_role!=null ? user_role.shortDescription : 'user'}"/>

<fieldset id="searchFields">
    <p class="placeholder">
        <spring:message text="${strings.instruction}" arguments="${searchRole}"/>
    </p>

    <div class="formline">
        <c:set var="arguments" value="${searchRole}${not empty param.searchGroups ? ' or group' : ''},${strings.searchName}"/>
        <span class="txtbox"
              title="<spring:message text="${strings.searchFieldTitle}"
                                     arguments="${arguments}"/>">
            <form:label path="inputName">
                <spring:message text="${strings.searchFieldLabel}"
                                arguments="${arguments}"/>:
            </form:label>

            <%-- <form:input path="inputName"/> --%>
            <input type="text" name="inputName" id="inputName" value="${not empty searchCriteria ? searchCriteria.inputName : ''}" autofocus>

            <!--[if lt IE 8]>
            <input type="text" name="" value="" class="fake">
            <![endif]-->

<%--             <input type="hidden" name="roleType" value="${roleType}"> --%>

            <span class="buttonset">
               <input type="submit"
                      name="_eventId_searchname"
                      id="search1"
                      value="${strings.searchButtonLabel}"
                      title="<spring:message text="${strings.searchButtonTitle}"
                                             arguments="${arguments}"/>">
            </span>
        </span>
    </div><!-- formline -->

    <div class="formline">
        <c:set var="arguments" value="${searchRole},${strings.searchLastName}"/>
        <span class="dropdown"
              title="<spring:message text="${strings.searchFieldTitle}"
                                     arguments="${arguments}"/>">
            <form:label path="lname">
                <spring:message text="${strings.searchFieldLabel}"
                                arguments="${arguments}"/>:
            </form:label>

            <form:select path="lname"
                         items="${fn:split(strings.lastNameOptions, ',')}"/>

            <span class="buttonset">
               <input type="submit"
                      name="_eventId_searchlname"
                      id="search2"
                      value="${strings.searchButtonLabel}"
                      title="<spring:message text="${strings.searchButtonTitle}"
                                             arguments="${arguments}"/>">
            </span>
        </span>
    </div><!-- formline -->

    <%-- MIV Dean Report : It makes no sense to search by Department --%>
    <c:if test="${user_role != 'DEAN' and not empty departments}">
        <div class="formline">
            <c:set var="arguments" value="${searchRole},${strings.searchDepartment}"/>
            <span class="dropdown"
                  title="<spring:message text="${strings.searchFieldTitle}"
                                         arguments="${arguments}"/>">
                <form:label path="department">
                    <spring:message text="${strings.searchFieldLabel}"
                                         arguments="${arguments}"/>:
                </form:label>

                <form:select path="department">
                    <c:forEach var="opt" items="${departments}">
                       <form:option value="${opt.schoolid}:${opt.departmentid}">
                           ${opt.school}${!empty opt.description ? " - " : ""}${opt.description}
                       </form:option>
                    </c:forEach>
                </form:select>
            </span>

            <span class="buttonset">
                <input type="submit"
                       name="_eventId_searchdept"
                       id="search3"
                       value="${strings.searchButtonLabel}"
                       title="<spring:message text="${strings.searchButtonTitle}"
                                              arguments="${arguments}"/>">
            </span>
        </div><!-- formline -->
    </c:if>

    <c:if test="${not empty schools}">
        <c:set var="arguments" value="${searchRole},${strings.searchSchool}"/>
        <div class="formline">
            <span class="dropdown"
                  title="<spring:message text="${strings.searchFieldTitle}"
                                         arguments="${arguments}"/>">
                <form:label path="school">
                    <spring:message text="${strings.searchFieldLabel}"
                                    arguments="${arguments}"/>:
                </form:label>

                <form:select path="school">
                    <c:forEach var="opt" items="${schools}">
                        <form:option value="${opt.schoolid}">${opt.description}</form:option>
                    </c:forEach>
                </form:select>

                <span class="buttonset">
                     <input type="submit"
                            name="_eventId_searchschool"
                            id="search4"
                            value="${strings.searchButtonLabel}"
                            title="<spring:message text='${strings.searchButtonTitle}' arguments='${arguments}'/>">
                </span>
            </span>
        </div><!-- formline -->
    </c:if>
</fieldset>
