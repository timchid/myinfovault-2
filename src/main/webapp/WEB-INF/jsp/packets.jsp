<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><%--
--%><%--@ taglib prefix="spring" uri="http://www.springframework.org/tags"--%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%><%--@ taglib prefix="miv" uri="mivTags" --%><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash;
    Packet List
    </title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" href="<c:url value='/css/packets.css'/>">
    <style>
    .debug_info { background-color: lightyellow; font-size: 90%; display: none; }
    .debug_info .var { font-family: Andale Mono, Consolas, Courier, monospace; }
    </style>
    <script src="<c:url value='/js/Packet.js'/>"></script>
    <script src="<c:url value='/js/packets.js'/>"></script>
    <%@ include file="apiclientutil.jspf" %>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    <c:set var="date_brief" scope="page" value="MMM d, yyyy"/>
    <c:set var="date_full"  scope="page" value="MMM d 'at' h:mm a"/>
<%--    <c:set var="c_fmt" value="${date_full}"/>
    <c:set var="m_fmt" value="${date_full}"/> --%>
    <c:set var="this_year" scope="page" value="<%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)%>" />
</head>

<body>
    <!-- MIV Header Div -->
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; Manage My Packets
    </div><!-- breadcrumbs -->

    <!-- MIV Main Div -->
    <main id="main" role="main">

        <h1>Packet List for ${user.targetUserInfo.displayName}</h1>

        <p class="debug_info">
            <span class="intro">Debug Info:</span><br>
            &nbsp; <span class="var">message</span> is: ${message}<br>
            &nbsp; <span class="var">user</span> is: ${user.userInfo}
        </p>
        <c:if test="${not empty message}">
        <div id="messagebox" class="hasmessages"><p>${message}</p></div>
        <script>$('#messagebox').delay(9000).slideUp(500);</script>
        </c:if>

        <div class="addrecord">
            <button type="button" id="addPacket" title="Create a New Packet from your Master Data">Add Packet</button>
        </div>

        <div id="listarea">

            <%-- List of the Packet Requests --%>
            <section id="requestlist" class="packetlist section">
                <c:if test="${requests != null}">
                <div class="sectionhead">
                    <h2>Packet Requests</h2>
                </div>
                <ol class="workitems">
                <c:if test="${empty requests}">
                    <span>You have no outstanding requests to submit a packet.</span>
                </c:if>
                <c:forEach var="pktreq" items="${requests}" varStatus="row">
                    <c:set var="action" value="${pktreq.dossier.action}" />
                    <c:set var="r_fmt" value="${(1900 + pktreq.requestTime.year == this_year) ? date_full : date_brief}"/>
                    <c:set var="s_fmt" value="${(1900 + pktreq.submitTime.year  == this_year) ? date_full : date_brief}"/>
                    <li class="records selectable">
                        <div class="record">
                            <span class="packetrequest documentname">${action.fullDescription}</span>
                            <c:choose>
                                <c:when test="${not pktreq.submitted}">
                                    <span class="submission">
                                        <c:choose>
                                            <c:when test="${not empty packets}">
                                                <form:form method="post" action="/miv/packet/submit">
                                                    <select name="packetId">
                                                        <c:forEach items="${packets}" var="pkt">
                                                            <option value="${pkt.packetId}">${pkt.packetName}</option>
                                                        </c:forEach>
                                                    </select>
                                                    <input type="hidden" name="dossierId" value="${pktreq.dossier.dossierId}">
                                                    <button type="submit"
                                                            class="submitpacket"
                                                            title="Submit selected packet to packet request">Submit</button>
                                                </form:form>
                                            </c:when>
                                            <c:otherwise>
                                                You must create a packet for submission.
                                            </c:otherwise>
                                        </c:choose>
                                    </span>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach items="${packets}" var="pkt">
                                        <c:if test="${pkt.packetId == pktreq.packetId}">
                                            <span class="buttons ui-pointer ui-left">
                                                <button name="viewSubmitted"
                                                        class="viewSubmitted"
                                                        value="${pkt.packetId}"
                                                        title="View &quot;${pkt.packetName}&quot;"
                                                        data-href="${pktreq.submittedPacketUrl}">
                                                    <span class="btnLabel">View Submitted Packet</span>
                                                </button>
                                            </span>
                                            <span class="submission">Submitted Packet: <em>${pkt.packetName}</em></span>
                                        </c:if>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                            <span class="timestamps">
                                <span class="requesttime"><fmt:formatDate pattern="${r_fmt}" value="${pktreq.requestTime}" /></span>
                                <c:if test="${pktreq.submitted}">
                                    <span class="submittime"><fmt:formatDate pattern="${s_fmt}" value="${pktreq.submitTime}" /></span>
                                </c:if>
                            </span>
                        </div>
                    </li>
                </c:forEach>
                </ol>
                </c:if>
            </section>


            <%-- List of the User's Packets --%>
            <section id="packetlist" class="packetlist section">
                <c:if test="${packets != null}">
                <div class="sectionhead">
                    <h2>Packets</h2>
                </div>
                <form:form method="post" commandName="dopacket">
                <ol class="workitems">
                <c:if test="${empty packets}">
                    <span>You do not have any packets.</span>
                </c:if>
                <c:forEach var="pkt" items="${packets}" varStatus="row">
                    <c:set var="c_fmt" value="${(1900 + pkt.createTime.year   == this_year) ? date_full : date_brief}"/>
                    <c:set var="m_fmt" value="${(1900 + pkt.modifiedTime.year == this_year) ? date_full : date_brief}"/>
                    <li class="file selectable" data-pkid="${pkt.packetId}">
                        <div class="file entry${dtime > pkt.modifiedTimestamp ? ' stale' : ''}">
                            <span class="packet documentname">${pkt.packetName}</span>
                            <span class="buttons ui-pointer ui-left">
                                <button name="edit" class="edit" value="${pkt.packetId}"
                                        title="Edit &quot;${pkt.packetName}&quot;"><span class="btnLabel">Edit</span></button>
                                <button name="delete" class="delete" value="${pkt.packetId}"
                                        title="Delete &quot;${pkt.packetName}&quot;"><span class="btnLabel">Delete</span></button>
                                <button name="duplicate" class="duplicate" value="${pkt.packetId}"
                                        title="Make a copy of &quot;${pkt.packetName}&quot;"><span class="btnLabel">Duplicate</span></button>
                                <button name="rename" class="rename" value="${pkt.packetId}"
                                        title="Change the name of &quot;${pkt.packetName}&quot;"><span class="btnLabel">Rename</span></button>
                            </span>
                            <span class="preview">
                                <a href="<c:url value='/packet/preview/${pkt.packetId}'/>"
                                   title="See a Preview of your packet &quot;${pkt.packetName}&quot;">Preview <em>&ldquo;${pkt.packetName}&rdquo;</em></a>
                            </span>
                            <span class="timestamps">
                                <span class="ctime" data-ctime="${pkt.createTimestamp}"><fmt:formatDate pattern="${c_fmt}" value="${pkt.createTime}" /></span>
                                <span class="mtime" data-mtime="${pkt.modifiedTimestamp}"><fmt:formatDate pattern="${m_fmt}" value="${pkt.modifiedTime}"/></span>
                            </span>
                        </div>
                    </li>
                </c:forEach>
                </ol>
                </form:form>
                </c:if>
            </section>

        </div><!-- listarea -->


        <div id="confirmDialog">
            <form:form method="post" commandName="dopacket">
                <p>Are you sure you want to <span id="actionName">action</span>
                   the packet named &ldquo;<span id="packetName">packetName</span>&rdquo;?</p>
                <div class="confirmButtons" style="display:none;">
                    <button type="submit" id="confirmed" class="action-button" name="action" value="pkid">confirm</button>
                </div>
            </form:form>
        </div>

        <div id="namePacket">
            <form:form method="post" action="packet/add" commandName="add">
                <span title="Provide a Name for your new Packet">
                <label for="packetname">Packet Name:</label> <input type="text" id="newPacketName" name="packetname" required pattern=".*\S.*" /><!-- pattern="^\S.+\S$" /> -->
                </span>
                <div class="confirmButtons" style="display:none;">
                    <button type="submit" id="reallyAddPacket" name="add" value="addPacket" class="action-button">Create Packet</button>
                </div>
            </form:form>
        </div>

    </main><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
    <c:if test="${not empty scrollTop}">
    <script>
    $(document).scrollTop(${scrollTop});
    $(document).scrollLeft(${scrollLeft});
    </script>
    </c:if>
</body>
</html><%--
vi: se ts=8 sw=4 sr et:
--%>
