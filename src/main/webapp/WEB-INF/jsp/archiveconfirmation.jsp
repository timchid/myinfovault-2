<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <c:set var="pagetitle" value="Archived Dossiers: Verification"/>
        <title>MIV &ndash; ${pagetitle}</title>
        <script type="text/javascript">
           var mivConfig = new Object();
           mivConfig.isIE = false;
        </script>
        <%@ include file="/jsp/metatags.html" %>
        
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
	    <link rel="stylesheet" type="text/css" href="<c:url value='/css/postauditreview.css'/>">
	    
        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
            <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
      <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
          &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='root'/>
            </c:url>" title="Archive Completed Dossiers">Archive Completed Dossiers</a>
         &gt; ${pagetitle}
    </div><!-- breadcrumbs -->

        <!-- MIV Main Div -->
        <div id="main">
            <h1>${pagetitle}</h1>
            <form:form name="archive" cssClass="mivdata" method="post" commandName="ArchiveDossiersForm" >

            <c:if test="${empty dossiersToArchiveMap}">
                <div>No dossiers have been selected to archive.</div>
            </c:if>

            <c:if test="${!empty dossiersToArchiveMap}">
            	<c:set var="dossierCount" scope="page" value="${fn:length(dossiersToArchiveMap)}"/>
            	
                <p>The following dossiers have been selected for EDMS and MIV archiving.</p>
                <p>Select the "Archive Dossiers Now!" button to archive the following dossiers,
                     or the "Cancel" button to stop the archiving of these dossiers at this time.</p>
                <p class="attention"><strong>Dossiers can no longer be returned for edits once they have been archived.</strong></p>

                <div class="buttonset">
                    <input type="submit" title="Archive Dossiers Now!" name="_eventId_archive" value="Archive Dossiers Now!">
                    <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
                    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
                </div>

                <div id="listarea" class="verifydossiers">
                	<div class="dossiercount">Total&nbsp;Dossier${dossierCount>1?'s':''}&nbsp;=&nbsp;${dossierCount}</div>
                    <ul class="itemlist"><%--
                    --%><c:forEach var="dossier" items="${dossiersToArchiveMap}" varStatus="rowIndex">
                        <li class="records ${(rowIndex.count)%2==0?'even':'odd'}">
							<strong>${dossier.key}:</strong> ${dossier.value}</li><%--
                    --%></c:forEach>
                    </ul>
                </div>

                <div class="buttonset">
                    <input type="submit" title="Archive Dossiers Now!" name="_eventId_archive" value="Archive Dossiers Now!">
                    <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
                    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
                </div>

            </c:if>
          </form:form>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
