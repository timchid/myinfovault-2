<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE html>

<html>
    <head>
        <title>Widget Sandbox</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/widget-draganddrop.css'/>">

        <script type="text/javascript" src="<c:url value='/js/widget-draganddrop.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/widget-sandbox.js'/>"></script>
        <jwr:script src="/bundles/mivlibs.js" />
    </head>
    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <main>
            <h1>Sandbox for Testing Stuff</h1>

            <div class="container sourceonly">
                <div>1</div>
                <div>2</div>
                <div>3</div>
            </div>

            <div class="container destinationonly">
            </div>

            <form id="testform">
                <input type="text" data-constraints="between(1,3);step(1,0.5)" value="2.5"><br>
                <input type="number" min="1" max="5"><br>
                <input type="email" snarfblat="42"><br>
                <label>This is
                    a test               <input type="email" snarfblat="42"><br></label>
                <label for="testlabel">  Second test.</label><input id="testlabel" type="email" snarfblat="42"><br>
                <input type="email" snarfblat="42" data-errormessage="{name} isn't right. {name} is wrong." data-name="Last email "><br>
                <input type="button" onclick="miv.validate.validate()" value="Validate"><input type="submit">

                <input type="radio" value="test1" name="testradio" data-constraints="required">
                <input type="radio" value="test2" name="testradio">
                <input type="radio" value="test3" name="testradio">

                <input type="radio" value="test1" name="testradio2" data-constraints="required">
                <input type="radio" value="test2" name="testradio2">
                <input type="radio" value="test3" name="testradio2">
            </form>
        </main>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
