<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${strings.pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        
        &gt; <a href="<c:url value='${context.request.contextPath}'>
                      <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                      <c:param name='_eventId' value='root'/>
                      <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                      </c:url>" title="${strings.rootTitle}">${strings.rootTitle}</a>

        &gt; ${strings.pageTitle}
    </div>

    <div id="main">
        <h1>${strings.pageTitle}</h1>

        <c:if test="${form.signature.signed}">
            <p>${strings.signeddisclosuremessage}
               <fmt:formatDate type="both"
                               dateStyle="short"
                               timeStyle="short"
                               value="${form.signature.signingDate}"/></p>
        </c:if>
            
        <div><strong>${strings.candidateLabel}</strong>
             ${form.signature.document.dossier.candidate.displayName}</div>

        <div><strong>${strings.viewPdfLabel}</strong>
             <a href="${form.dossierUrl}"
                title="${form.signature.document.dossier.action.description}">${form.signature.document.dossier.action.description}</a></div>

        <div><strong>${strings.requestForLabel}</strong>
             <a href="${form.disclosureUrl}">${strings.disclosureLink}</a></div>
             
        <div><strong>${strings.requestByLabel}</strong>
             ${form.signature.document.schoolName}${!empty form.signature.document.departmentName ? ' &ndash; ' : ''}${form.signature.document.departmentName}</div>

        <c:if test="${!form.signature.signed}">
        	<div class="certificate">
        	<p>
        		<br>
        		${strings.signCertificate}&nbsp;<strong><fmt:formatDate type="date" dateStyle="long" value="${form.signature.document.dossier.action.effectiveDate}"/></strong>.
        		<br>${strings.signPledge}
            </p>
            </div>

            <form:form id="signatureForm" cssClass="mivdata" method="post" commandName="form">
                <div class="buttonset">
                    <input type="submit"
                           name="_eventId_sign"
                           title="${strings.signButton}"
                           value="${strings.signButton}"/>
                           
                    <input type="submit"
                           name="_eventId_root"
                           title="${strings.cancelButton}"
                           value="${strings.cancelButton}"/>
                </div>
            </form:form>
        </c:if>
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
vi: se ts=8 sw=2 et:
--%>
