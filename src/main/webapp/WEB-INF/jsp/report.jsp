<%--

  Report/Log table. Choose table columns to display, their
  order, labels, and display formats in config properties.

  | Request Scope Var   | Description                                                          |
  +---------------------+----------------------------------------------------------------------+
  | entries             | iterable collection of entry objects (table rows)                    |
  | dossier             | dossier object to render dossier header page                         |
  | pageTitle           | page title                                                           |
  | strings.fields      | ordered, comma-delimited list of entry object fields (table columns) |
  | labels.<field>      | entry field label                                                    |
  | formats.<field>     | entry field format pattern                                           |
  | data.<field>        | bean reference for the field's data attribute                        |
  | strings.footer      | message format pattern for table footer message                      |
  | config.script       | custom JavaScript file in /js/                                       |
  | config.style        | custom CSS file in /css/                                             |


  E.G. The 'log' view state for the event webflow in mivconfig.properties:

     # entry fields in order
     event-log-string-fields=dossierId,insertTimestamp,realUser.displayName,cost,detail

     # entry field labels
     event-log-label-dossierId=Dossier ID
     event-log-label-insertTimestamp=Date Inserted
     event-log-label-realUser.displayName=Real User
     event-log-label-cost=Dollars Spent
     event-log-label-detail=Additional Details

     # entry field formats
     event-log-format-dossierId={0,number,integer}
     event-log-format-insertTimestamp={0,date,full}
     event-log-format-cost={0,number,currency}

     # entry field fragments
     event-log-fragment-detail=report-detail.jsp

     # html5 custom data attribute per field
     event-log-data-insertTimestamp=insertTimestamp.time

--%><%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${pageTitle}</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
        <c:if test="${not empty config.style}">
            <link rel="stylesheet" type="text/css" href="<c:url value='/css/${config.style}'/>">
        </c:if>

	<script type="text/javascript" src="<c:url value='/js/detect.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/jquery-contained-sticky-scroll.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>
        <c:if test="${not empty config.script}">
            <script type="text/javascript" src="<c:url value='/js/${config.script}'/>"></script>
        </c:if>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${pageTitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${pageTitle}</h1>

            <jsp:include page="/jsp/dossierheader.jsp"/>

            <table id="report" class="display">
                <caption>${strings.caption}</caption>

                <thead>
                    <tr><%--
                    --%><c:forEach var="field" items="${strings.fields}">
                       <th class="datatable-column-filter" title="${labels[field]}">${labels[field]}</th><%--
                    --%></c:forEach>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th colspan="${fn:length(fn:split(strings.fields, ','))}">
                            <spring:message text="${strings.footer}" arguments="${fn:length(entries)}"/></th>
                    </tr>
                </tfoot>

                <tbody>
                    <%-- loop thru entries --%>
                    <c:forEach var="entry" items="${entries}" varStatus="row">
                        <tr class="${(row.count)%2==0?'even':'odd'}">
                            <%-- loop thru entry fields--%><%--
                        --%><c:forEach var="field" items="${strings.fields}"><%--
                            --%><%--
                                        loop thru entry field keys.

                                        A field like:
                                            'dossier.candidate.displayName'

                                        will be dereferenced in entry like:
                                            entry['dossier']['candidate']['displayName']
                                --%><%--
                            --%><c:set var="fieldValue" scope="request" value="${entry}"/><%--
                            --%><c:forEach var="key" items="${fn:split(field, '.')}"><%--
                                --%><c:set var="fieldValue" scope="request" value="${not empty fieldValue ? fieldValue[key] : ''}"/><%--
                            --%></c:forEach><%--

                            --%><%--
                                        Build a custom html5 data attribute for the <td> for this field.

                                        e.g.
                                                Let's say that the "field" variable currently has the value "meetingDate"

                                                data['meetingDate'] dereferenced as the entry bean attribute "posted.time"

                                                which is dereferenced as "entry['posted']['time']"

                                                which evaluates to 12345456757

                                                So the <td> tag is rendered as <td data-raw="12345456757">
                                                FIXME:  This should do: <td data-sort="12345456757">
                                                    See https://datatables.net/examples/advanced_init/html5-data-attributes.html
                                --%><%--
                            --%><c:set var="dataAttribute" value=""/><%--
                            --%><c:if test="${not empty data[field]}"><%--
                                --%><c:set var="fieldData" scope="request" value="${entry}"/><%--
                                --%><c:forEach var="key" items="${fn:split(data[field], '.')}"><%--
                                    --%><c:set var="fieldData" scope="request" value="${not empty fieldData ? fieldData[key] : ''}"/><%--
                                --%></c:forEach><%--

                                --%><c:set var="dataAttribute" value=' data-sort="${fieldData}"'/><%--
                            --%></c:if>
                                <td${dataAttribute}><%--
                                --%><c:choose><%--
                                    --%><%-- a JSP fragment has been configured for this field --%><%--
                                    --%><c:when test="${not empty fragments[field]}"><%--
                                        --%><jsp:include page="${fragments[field]}" /><%--
                                    --%></c:when><%--

                                        <%-- else, a format pattern for this field exists --%><%--
                                    --%><c:when test="${not empty formats[field]}"><%--
                                        --%><spring:message text="${formats[field]}" arguments="${fieldValue}"/>
                                        </c:when><%--

                                    --%><%-- otherwise, just print the field value for this entry --%><%--
                                    --%><c:otherwise>
                                        ${fieldValue}<%--
                                    --%></c:otherwise><%--
                                --%></c:choose>
                                </td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
