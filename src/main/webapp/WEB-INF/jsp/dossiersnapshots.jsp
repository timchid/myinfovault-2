<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><!DOCTYPE html>
<html>

<c:set var="pageTitle" value="${strings.pagetitle}"/>

<head>
    <title>MIV &ndash; ${pageTitle}</title>
    <script type="text/javascript">
    /* <![CDATA[ */
      var mivConfig = new Object();
      mivConfig.isIE = false;
    /* ]]> */
    </script>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/dossierAction.css'/>">

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <style type="text/css" media="screen">
        /*
         * Override styles needed due to the mix of different CSS sources!
         */
        .dataTables_info { padding-top: 0; }
        .dataTables_paginate { padding-top: 0; }
        .css_right { float: right; }
        #theme_links span { float: left; padding: 2px 10px; }
    </style>

    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>
    <script type="text/javascript">
        $(document).ready( function() {
            oTable = fnInitDataTable('snapshotlist', {"bHelpBar" : true, "aaSorting" : [], "aoColumns" : [{"iDataSort": 5 }, null, null, null, {"iDataSort": 5 } ,{"bVisible": false }] });
        } );
    </script>

<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>
<body>

<jsp:include page="/jsp/mivheader.jsp" />

<div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; <a title="${strings.root}" href="${strings.href}">${strings.root}</a>
    &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='back'/>
            </c:url>" title="${strings.prev}">${strings.prev}</a>
    &gt; ${pageTitle}
</div><!-- breadcrumbs -->


<div id="main"><!-- MIV Main Div -->

<h1>${pageTitle}</h1>
    <miv:permit action="! View Snapshots" usertype="REAL">
    <p><strong>You are not authorized to view this page.</strong></p>
    </miv:permit>
    <miv:permit action="View Snapshots" usertype="REAL">
    <div id="openaction-summary"><%-- Adding div to handle sticky header --%>
      <div id="sticky">

        <%-- dossier attribute is in request scope, so no need to create page scoped variable --%>
        <jsp:include page="/jsp/dossierheader.jsp"></jsp:include>

        <ul class="dossierlinks">
            <miv:permit action="View Signature Log">
                <li><a href="<c:url value='${context.request.contextPath}'>
                             <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                             <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                             <c:param name='_eventId' value='event_log'/>
                             <c:param name='category' value='SIGNATURE'/>
                             <c:param name='dossierId' value='${dossier.dossierId}'/>
                             </c:url>" title="View the Signature Log" >View the Signature Log</a></li>
            </miv:permit>
        </ul> <!--  dossierlinks  -->
      </div>
    </div><!-- openaction-summary -->
      <c:choose>
        <c:when test="${!empty snapshots and fn:length(snapshots.qualifiedSnapshotsList) > 0}">
        <div class="full_width">
          <table id="snapshotlist" class="display">
            <thead>
              <tr>
                <th title="Snapshot Routing Order">Snapshot</th>
                <th title="Appointment">Appointment</th>
                <th title="School/College">School/College</th>
                <th title="Department">Department</th>
                <th title="Date Submitted to">Date Submitted to:</th>
                <th title="Timestamp">Timestamp</th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="snapshot" items="${snapshots.qualifiedSnapshotsList}" varStatus="row">
                <tr class="${(row.count)%2==0?'even':'odd'}">
                  <td><a href='${snapshot.snapshotFileUrl}' title='View ${snapshot.snapshotLocation} Snapshot'>${snapshot.snapshotLocation}</a></td>
                  <td>${snapshot.snapshotAppointment}</td>
                  <td>${snapshot.snapshotSchool}</td>
                  <td>${snapshot.snapshotDepartment}</td>
                  <td>${snapshot.routedToNode}</td>
                  <td>${snapshot.snapshotTimestamp}</td>
                </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
        </c:when>
        <c:when test="${!empty snapshots and !snapshots.authorized}">
          <p><strong>You are not authorized to view this document.</strong></p>
        </c:when>
        <c:when test="${routedBefore20100119}">
          <p><strong>This dossier was routed prior to January 19, 2010 (the v3 upgrade) and will not have a snapshot available for viewing.</strong></p>
        </c:when>
        <c:otherwise>
          <p><strong>No snapshots available for this dossier.</strong></p>
        </c:otherwise>
      </c:choose>
    </miv:permit>

</div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
