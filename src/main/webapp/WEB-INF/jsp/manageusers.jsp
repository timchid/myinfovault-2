<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>MIV &ndash; Manage Users</title>

  <%@ include file="/jsp/metatags.html" %>

  <link rel="stylesheet" type="text/css" title="MIV Style" href="<c:url value='/css/mivEnterData.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

    <!-- breadcrumbs -->
    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="Manage Users" name="pageTitle"/>
    </jsp:include>

<!-- MIV Main Div -->
<div id="main">

    <h1>Manage Users</h1>

    <p>
    Select one of the options below to manage an MIV user's role, status or access.
    </p>

    <div>
        <ul class="menulist">

            <miv:permit roles="VICE_PROVOST_STAFF|SENATE_STAFF|SCHOOL_STAFF|DEPT_STAFF" usertype="PROXY">
            <li>
                <a href="<c:url value='/user/add'/>" title="Add a New User">Add a New User</a>
            </li>

            <miv:permit roles="VICE_PROVOST_STAFF|SCHOOL_STAFF|DEPT_STAFF">
            <li>
                <a href="<c:url value='${context.request.contextPath}'>
                            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                            <c:param name='_eventId' value='edit'/>
                        </c:url>" title="Edit A User&#39;s Account">Edit A User&#39;s Account</a>
            </li>
            </miv:permit>

            <li>
                <a href="<c:url value='${context.request.contextPath}'>
                            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                            <c:param name='_eventId' value='activate'/>
                        </c:url>" title="Deactivate/Reactivate A User">Deactivate/Reactivate A User</a>
            </li>

            <li>
                <a href="<c:url value='${context.request.contextPath}'>
                            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                            <c:param name='_eventId' value='group'/>
                        </c:url>" title="Manage Groups">Manage Groups</a>
            </li>

            <miv:permit action="Manage Executives">
            <li>
                <a href="<c:url value='${context.request.contextPath}'>
                         <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                         <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                         <c:param name='_eventId' value='executive'/>
                         </c:url>" title="Manage Chancellor, Provost, and Vice Provost">Manage Executive Roles</a>
            </li>
            </miv:permit>
        </miv:permit>
        </ul>
    </div> <!-- links -->

</div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
<c:if test="${not empty param.alertmsg}">
<script>
  alert("${param.alertmsg}");
</script>
</c:if>
</body>
</html><%--
 vi: se ts=8 sw=4 et:
--%>
