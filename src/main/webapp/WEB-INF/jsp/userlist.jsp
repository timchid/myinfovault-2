<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${strings.pagetitle}${fn:length(strings.subtitle)>0 ? ":" : ""}${strings.subtitle}</title>

        <script type="text/javascript">
        /* <![CDATA][ */
        var mivConfig = new Object();
        mivConfig.isIE = false;
        /* ]]> */
        </script>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
        <style type="text/css" media="screen">
            /*
             * Override styles needed due to the mix of different CSS sources!
             */
            .dataTables_info { padding-top: 0; }
            .dataTables_paginate { padding-top: 0; }
            .css_right { float: right; }
            #theme_links span { float: left; padding: 2px 10px; }
        </style>

        <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                fnInitDataTable('userlist', {"bHelpBar" : true, "aFilterColumns": [0,1,2,3]});
            });
        </script>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${strings.pagetitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${strings.description} for <em>${searchCriteriaText}</em></h1>

            <%-- list errors --%>
            <c:if test="${!empty selectErrorMessage}">
                <div id="errormessage" class="haserrors">
                    <div id="errorbox">
                        <ul>Error -
                            <li>${selectErrorMessage}</li>
                        </ul>
                    </div>
                </div>
            </c:if>

            <%-- description --%>
            <c:choose>
                <c:when test="${empty results}">
                        <p>We're sorry, no matching results were found.</p>
                </c:when>

                <c:otherwise>
                    <p>${strings.message}</p>

                    <p>Select a column header to sort by that column.</p>
                </c:otherwise>
            </c:choose>

            <%-- results table --%>
            <c:if test="${!empty results}">
                <div class="searchresults">Search Results = ${fn:length(results)}</div>

                <div class="full_width">
                    <table id="userlist" class="display">
                        <thead>
                            <tr>
                                <th class="datatable-column-filter" title="MIV User">MIV User</th>
                                <th class="datatable-column-filter" title="School/College">School/College</th>
                                <th class="datatable-column-filter" title="Department">Department</th>
                                <th class="datatable-column-filter" title="Role">Role</th>
                             </tr>
                        </thead>

                        <tbody>
                            <c:forEach var="usr" items="${results}" varStatus="row">
                                <tr class="${(row.count)%2==0?'even':'odd'}">
                                    <td><c:choose>
                                        <c:when test="${flowRequestContext.activeFlow.id == 'select-flow'
                                                     || flowRequestContext.activeFlow.id == 'manageUsers-edit-flow'}">
                                            <a href="<c:url value='${context.request.contextPath}'>
                                                     <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                                                     <c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
                                                     <c:param name='_eventId' value='select'/>
                                                     <c:param name='id' value='${usr.userId}'/>
                                                     </c:url>"
                                               title="${strings.selectTitle} ${usr.sortName}">${usr.sortName}</a>
                                        </c:when>

                                        <c:otherwise>${usr.sortName}</c:otherwise>
                                    </c:choose></td>
                                    <td>${usr.primaryAppointment.schoolName}</td>
                                    <td>${!empty usr.primaryAppointment.departmentName ? usr.primaryAppointment.departmentName : '&nbsp;'}</td>
                                    <td>${usr.primaryRoleType.shortDescription}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html><%--
 vi: se ts=8 sw=4 et:
--%>
