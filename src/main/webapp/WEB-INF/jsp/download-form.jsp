<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%><jsp:useBean id="now" class="java.util.Date" /><%--
--%><fmt:formatDate var="currentYear" value="${now}" pattern="yyyy" /><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${strings.title}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
    
    <script type="text/javascript" src="<c:url value='/js/jquery-ui.plugins.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/pubmed.js'/>"></script>
</head>
<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; <a href="<c:url value='/Imports'>
                      <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                      <c:param name='_eventId' value='root'/>
                      <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                      </c:url>" title="${strings.roottitle}">${strings.roottitle}</a>
                        
        &gt; ${strings.title}
    </div>

    <div id="main">
        <h1>${strings.title}</h1>
      
        <spring:hasBindErrors name="form">
            <c:if test="${errors.errorCount > 0}">
                <div id="errormessage">
                    <strong>ERROR</strong>
                  
                    <ul>
                        <c:forEach var="errMsgObj" items="${errors.allErrors}">
                            <li><spring:message htmlEscape="false"
                                                text="${errMsgObj.defaultMessage}"/></li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
        </spring:hasBindErrors>
        
        <div class="mivDialog wideDialog">
	        <p>${strings.instruction}</p>
	        
	        <form:form  id="downloadForm" cssClass="mivdata" method="post" commandName="form">
	            <fieldset class="columns">
	                <legend>${strings.authorNameLegend}</legend>
	                
	                <p><em>${strings.note}</em></p>
	                
	                <div class="formline">
		                <div>
		                    <form:label path="surname"
		                                cssClass="f_required">${strings.surnameLabel}</form:label>
		                </div>
		                
		                <div>
			                <form:input path="surname"
			                            title="${strings.surnameTitle}"
			                            maxlength="25"
			                            size="16"/>
		                </div>
	                </div>
	                
	                <div class="formline">
		                <div>
			                <form:label path="givenInitial"
			                            cssClass="f_required">${strings.givenInitialLabel}</form:label>
		                </div>
		                
		                <div>
			                <form:input path="givenInitial"
			                            title="${strings.givenInitialTitle}"
			                            maxlength="1"
			                            size="1"/>
		                </div>
	                </div>
	                
	                <div class="formline">
	                    <div>
		                  <form:label path="middleInitial">${strings.middleInitialLabel}</form:label>
		                </div>
		                
		                <div>
			                <form:input path="middleInitial"
			                            title="${strings.middleInitialTitle}"
			                            maxlength="1"
			                            size="1"/>
		                </div>
	                </div>
	            </fieldset>
	            
	            <fieldset class="columns">
	                <legend>${strings.yearRangeLegend}</legend>
	                
	                <div class="formline">
		                <form:radiobutton path="yearRange"
		                                  label="${strings.yearRangeLastOneLabel}"
		                                  title="${strings.yearRangeLastOneTitle}"
		                                  value="1"/>
		                                  
		                <form:radiobutton path="yearRange"
		                                  label="${strings.yearRangeLastThreeLabel}"
		                                  title="${strings.yearRangeLastThreeTitle}"
		                                  value="3"/>
		                                  
		                <form:radiobutton path="yearRange"
		                                  label="${strings.yearRangeLastFiveLabel}"
		                                  title="${strings.yearRangeLastFiveTitle}"
		                                  value="5"/>
		                                  
		                <form:radiobutton path="yearRange"
		                                  label="${strings.yearRangeCustomLabel}"
		                                  title="${strings.yearRangeCustomTitle}"
		                                  value="0"/>
	                </div>
	                
	                <div class="formline">
		                <form:label path="fromYear">${strings.fromYearLabel}</form:label>
		                
		                <form:select path="fromYear"
		                             title="${strings.fromYearTitle}">
		                    <c:forEach var="yearsAgo" begin="0" end="100">
		                        <form:option value="${currentYear-yearsAgo}">${currentYear-yearsAgo}</form:option>
		                    </c:forEach>
		                </form:select>
	                </div>
	                
	                <div class="formline">
		                <form:label path="toYear">${strings.toYearLabel}</form:label>
		                
		                <form:select path="toYear"
		                             title="${strings.toYearTitle}">
		                    <c:forEach var="yearsAgo" begin="0" end="100">
		                        <form:option value="${currentYear-yearsAgo}">${currentYear-yearsAgo}</form:option>
		                    </c:forEach>
		                </form:select>
	                </div>
	            </fieldset>
	            
	            <div class="buttonset">
	                <button type="submit"
	                        name="_eventId"
	                        value="search"
	                        title="${strings.searchTitle}">${strings.searchLabel}</button>
	                            
	                <button type="submit"
	                        name="_eventId"
	                        value="root"
	                        title="${strings.cancelTitle}">${strings.cancelLabel}</button>
	            </div>
	        </form:form>
        </div><!-- mivDialog -->
    </div><!-- main -->
    
    <jsp:include page="/jsp/mivfooter.jsp" />
</body>
</html>
