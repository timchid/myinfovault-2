<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" buffer="none"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><c:set var="isViceProvost" value="${form.viceProvost}"/><%--
--%><c:set var="isNotDepartment" value="${!form.department && !form.packetRequest}"/><%--
--%><c:set var="isAppointment" value="${form.actionType == 'APPOINTMENT'}"/><%--

    Assignment title step pattern (whole number increments for new appointments, .5 otherwise)

--%><c:set var="stepIncrement" value="${isAppointment ? 0 : 5}"/><%--
--%><c:set var="stepPattern" value="(\d{0,2}(\.[0|${stepIncrement}]?)?)|A(/?S)?|N(/?A)?"/><%--

    Assignment title step field explanation

--%><spring:message var="stepCustomValidity" text="${tooltips.stepCustomValidity}" arguments="${stepIncrement}"/><%--
--%><!DOCTYPE html>
<html>
<head>
    <c:set var="pageTitle" scope="page" value="${strings.pageTitle} - ${form.actionType.description}"/>
    <title>${pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/academicaction.css'/>"/>

    <script type="text/javascript" src="<c:url value='/js/detect.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/jquery-contained-sticky-scroll.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/widget-suggestion.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/date.js'/>"></script>
    <jwr:script src="/bundles/mivvalidation.js" />
    <script type="text/javascript" src="<c:url value='/js/academicaction.js'/>"></script>

    <jsp:scriptlet><![CDATA[
        Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
        pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
    ]]></jsp:scriptlet>

    <script type="text/javascript">
        var currentYear = ${thisYear};
        var isNotDepartment = ${isNotDepartment};
    </script>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${pageTitle}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${pageTitle}</h1>

        <c:choose>
            <c:when test="${!isAppointment}">
                <jsp:include page="/jsp/dossierheader.jsp" />
            </c:when>

            <%-- existing appointment action edit warning --%>
            <c:when test="${not empty form.candidate && form.department}">
                <div id="warning">
                    <div class="sticky">${strings.warning}</div>
                </div>
            </c:when>
        </c:choose>

        <jsp:include page="binderrors.jsp" />

        <c:if test="${isAppointment}">
        <p class="attention">
        Use this form <em>ONLY</em> for New Appointments, not for Appointments via Change in Department or Title.
        </p>
        </c:if>

        <form:form cssClass="mivdata" method="post" commandName="form">
            <div class="sections ${form.actionType}">
                <h2><span>${form.delegationAuthority.description}</span>
                    ${form.actionType.description} effective
                    <span><fmt:formatDate type='date' dateStyle='long' value='${form.effectiveDateAsDate}'/></span></h2>

                <div>
                    <fieldset class="cell" title="${tooltips.delegationAuthority}">
                        <legend class="f_required">${labels.delegationAuthority}</legend>

                        <form:radiobuttons path="delegationAuthority"
                                           items="${form.dossierDelegationAuthorities}"
                                           itemLabel="description"
                                           disabled="${isViceProvost}"/>
                    </fieldset>

                    <div class="cell" title="${tooltips.effectiveDate}">
                        <spring:bind path="effectiveDate">
                            <label for="${status.expression}" class="f_required">${labels.effectiveDate}</label>

                            <input type=text class="datepicker"
                                   id="${status.expression}"
                                   name="${status.expression}"
                                   value="<fmt:formatDate type='date' dateStyle='long' value='${form.effectiveDateAsDate}'/>"
                                   size="20"
                                   required
                                   ${isNotDepartment ? 'disabled' : ''}>
                        </spring:bind>
                    </div>

                    <div class="cell" title="${tooltips.endDate}">
                        <spring:bind path="endDate">
                            <label for="${status.expression}">${labels.endDate}</label>

                            <input type=text class="datepicker"
                                   id="${status.expression}"
                                   name="${status.expression}"
                                   value="<fmt:formatDate type='date' dateStyle='long' value='${form.endDateAsDate}'/>"
                                   size="20"
                                   ${isNotDepartment ? 'disabled' : ''}>
                        </spring:bind>
                    </div>

                    <c:if test="${isViceProvost}">
                        <div class="cell" title="${tooltips.retroactiveDate}">
                            <spring:bind path="retroactiveDate">
                                <label for="${status.expression}">${labels.retroactiveDate}</label>

                                <input type=text class="datepicker"
                                       id="${status.expression}"
                                       name="${status.expression}"
                                       value="<fmt:formatDate type='date' dateStyle='long' value='${form.retroactiveDateAsDate}'/>"
                                       size="20">
                            </spring:bind>
                        </div>
                    </c:if>

                    <c:if test="${!isAppointment}">
                        <div class="formline" title="${tooltips.accelerationYears}">
                            <spring:bind path="accelerationYears">
                                <label for="${status.expression}">${labels.accelerationYears}</label>

                                <input type="number"
                                       min="0"
                                       max="20"
                                       step="1"
                                       id="${status.expression}"
                                       name="${status.expression}"
                                       value="${status.value > 0 ? status.value : 0}"
                                       ${isNotDepartment ? 'disabled' : ''}>
                            </spring:bind>
                            ${strings.normalAcceleration}
                        </div>
                    </c:if>
                </div>

                <c:if test="${isAppointment}">
                    <c:set var="appointeeInformation" scope="page"
                        value="${labels.candidateInformation}" />
                    <c:if test="${not empty form.candidate}">
                        <c:set var="appointeeInformation" scope="page"
                            value="Appointee:&nbsp;${form.candidate.displayName}" />
                    </c:if>

                    <h2>
                        <span>${appointeeInformation}</span>
                    </h2>

                    <div>
                        <div id="questions">
                            <fieldset class="formline">
                                <legend class="f_required">${labels.currentEmployee}</legend>

                                <form:radiobuttons
                                    path="currentEmployee"
                                    items="${form.polarResponses}"
                                    itemLabel="description"
                                    disabled="${isNotDepartment}" />
                                <p class="instruction">${strings.currentEmployee}</p>
                            </fieldset>

                            <fieldset class="formline">
                                <legend>${labels.representedEmployee}</legend>

                                <form:radiobuttons
                                    path="representedEmployee"
                                    items="${form.polarResponses}"
                                    itemLabel="description"
                                    disabled="${isNotDepartment}" />
                                <p class="instruction">${strings.representedEmployee}</p>
                            </fieldset>

                            <fieldset class="formline">
                                <legend>${labels.unionNoticeRequired}</legend>

                                <form:radiobuttons
                                    path="unionNoticeRequired"
                                    items="${form.polarResponses}"
                                    itemLabel="description"
                                    disabled="${isNotDepartment}" />
                                <p class="instruction">${strings.unionNoticeRequired}</p>
                            </fieldset>

                            <fieldset class="formline">
                                <legend>${labels.laborRelationsNotified}</legend>

                                <form:radiobuttons
                                    path="laborRelationsNotified"
                                    items="${form.polarResponses}"
                                    itemLabel="description"
                                    disabled="${isNotDepartment}" />
                                <p class="instruction">${strings.laborRelationsNotified}</p>
                            </fieldset>
                        </div>

                        <fieldset class="formline"
                            title="${strings.appointeeName}">
                            <legend>${labels.appointeeName}</legend>

                            <div class="cell usersearch hidden"
                                title="${tooltips.usersearch}">
                                <label for="usersearch">${labels.usersearch}</label>
                                <input id="usersearch"
                                    class="userSuggestion"
                                    data-initial="${not empty form.candidate ? form.candidate.displayName : ''}"
                                    ${isNotDepartment ? 'disabled' : ''}>
                            </div>

                            <div class="cell"
                                title="${tooltips.givenName}">
                                <form:label path="givenName"
                                    cssClass="f_required">${labels.givenName}</form:label>
                                <form:input path="givenName"
                                    disabled="${isNotDepartment}" />
                            </div>

                            <div class="cell"
                                title="${tooltips.middleName}">
                                <form:label path="middleName">${labels.middleName}</form:label>
                                <form:input path="middleName"
                                    disabled="${isNotDepartment}" />
                            </div>

                            <div class="cell"
                                title="${tooltips.surname}">
                                <form:label path="surname"
                                    cssClass="f_required">${labels.surname}</form:label>
                                <form:input path="surname"
                                    disabled="${isNotDepartment}" />
                            </div>
                        </fieldset>
                    </div>
                </c:if>

                <h2>${labels.appointmentDetails}</h2>

                <div id="status-block">
                    <%--
                        ASSIGNMENT STATUSES
                    --%>
                    <c:forEach var="assignmentStatus" items="${form.assignmentStatuses}"><div id="${assignmentStatus.key}">
                        <fieldset class="status">
                            <legend>${assignmentStatus.key.description}</legend>

                            <%--
                                ASSIGNMENTS
                            --%>
                            <c:forEach var="assignment" items="${assignmentStatus.value}" varStatus="assignmentRow">
                                <c:set var="assignmentPath" value="assignmentStatuses[${assignmentStatus.key}][${assignmentRow.index}]"/>
                                <c:set var="scopeDescription" value="${assignment.scopeDescription}"/>

                                <div class="assignment ${assignment.primary ? 'primary':'joint'}" data-path="${assignmentPath}">
                                    <h3><span><c:choose>
                                        <c:when test="${not empty scopeDescription}">${scopeDescription}</c:when>
                                        <c:when test="${assignment.primary}">${labels.primaryAppointment}</c:when>
                                        <c:otherwise>${labels.jointAppointment}</c:otherwise>
                                    </c:choose> &ndash; <fmt:formatNumber type='percent' value='${assignment.percentOfTime}'/></span></h3>

                                    <fieldset>
                                        <fieldset class="formline cells">
                                            <div class="cell" title="${tooltips.department}">
                                                <spring:bind path="${assignmentPath}.scope">
                                                    <label for="${status.expression}"
                                                           class="f_required">${labels.department}</label>

                                                    <input type="text"
                                                           id="${status.expression}"
                                                           name="${status.expression}"
                                                           value="${status.value}"
                                                           data-initial="${scopeDescription}"
                                                           size="40"
                                                           ${isNotDepartment || !isAppointment ? 'disabled' : ''}>
                                                </spring:bind>
                                            </div>

                                            <div class="cell" title="${tooltips.appointmentPercentOfTime}">
                                                <spring:bind path="${assignmentPath}.percentOfTime">
                                                    <label for="${status.expression}"
                                                           class="f_required">${labels.percentOfTime}</label>

                                                    <input type="number"
                                                           id="${status.expression}"
                                                           name="${status.expression}"
                                                           value="<fmt:formatNumber type='number' pattern="###" value='${status.value * 100}'/>"
                                                           min="0"
                                                           max="100"
                                                           step="1"
                                                           required
                                                           ${isNotDepartment ? 'disabled' : ''}>
                                                </spring:bind>
                                            </div>
                                        </fieldset>

                                        <%--
                                            TITLES
                                        --%>
                                        <c:forEach var="title" items="${assignment.titles}" varStatus="titleRow">
                                            <c:set var="titlePath" value="${assignmentPath}.titles[${titleRow.index}]"/>
                                            <c:set var="description" value="${title.description}"/>

                                            <div class="title" data-path="${titlePath}">
                                                <h4><span><c:choose><%-- use scope description as --%>
                                                    <c:when test="${not empty description}">${description}</c:when>
                                                    <c:otherwise>${labels.title}</c:otherwise>
                                                </c:choose> &ndash; <fmt:formatNumber type='percent'
                                                                                      value='${title.percentOfTime}'/></span></h4>

                                                <fieldset>
                                                    <fieldset class=formline>
                                                        <div class="cell" title="${tooltips.code}">
                                                            <form:label path="${titlePath}.code"
                                                                        cssClass="f_required">${labels.code}</form:label>
                                                            <form:input path="${titlePath}.code"
                                                                        size="6"
                                                                        maxlength="4"
                                                                        disabled="${isNotDepartment}"/>
                                                        </div>

                                                        <div class="cells">
                                                            <div class="cell" title="${tooltips.rankAndTitle}">
                                                                <form:label path="${titlePath}.description"
                                                                                cssClass="f_required">${labels.rankAndTitle}</form:label>
                                                                <form:input path="${titlePath}.description"
                                                                            size="35"
                                                                            maxlength="225"
                                                                            disabled="${isNotDepartment}"/>
                                                            </div>

                                                            <div class="cell" title="${tooltips.step}">
                                                                <spring:bind path="${titlePath}.step">
                                                                    <label for="${status.expression}">${labels.step}</label>

                                                                    <input type="text"
                                                                           id="${status.expression}"
                                                                           name="${status.expression}"
                                                                           value="${status.value}"
                                                                           size="4"
                                                                           pattern="${stepPattern}"
                                                                           data-customvalidity="${stepCustomValidity}"
                                                                           ${isNotDepartment ? 'disabled' : ''}>
                                                                </spring:bind>
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <fieldset class="formline">
                                                        <div class="cells">
                                                            <div class="cell" title="${tooltips.titlePercentOfTime}">
                                                                <spring:bind path="${titlePath}.percentOfTime">
                                                                    <fmt:formatNumber type='number' pattern="###" value='${status.value * 100}' var="percentOfTime"/>
                                                                    <label for="${status.expression}" class="f_required">${labels.percentOfTime}</label>

                                                                    <input type="number"
                                                                           id="${status.expression}"
                                                                           name="${status.expression}"
                                                                           value="${percentOfTime}"
                                                                           min="0"
                                                                           max="100"
                                                                           step="1"
                                                                           required
                                                                           ${isNotDepartment ? 'disabled' : ''}>
                                                                </spring:bind>
                                                            </div>

                                                            <div class="cell${isNotDepartment || percentOfTime > 0 ? ' disabled' : ''}" title="${tooltips.withoutSalary}">
                                                                <spring:bind path="${titlePath}.withoutSalary">
                                                                    <label for="${status.expression}">${labels.withoutSalary}</label>

                                                                    <input type="checkbox"
                                                                           id="${status.expression}"
                                                                           name="${status.expression}"
                                                                           value="true"
                                                                           ${status.value ? 'checked' : ''}>

                                                                    <c:if test="${status.value}">
                                                                        <input type="hidden"
                                                                               name="_${status.expression}"
                                                                               value="on">
                                                                    </c:if>
                                                                </spring:bind>
                                                            </div>
                                                        </div>

                                                        <div class="cell" title="${tooltips.appointmentDuration}">
                                                            <label for="${titlePath}.appointmentDuration" class="f_required">${labels.appointmentDuration}</label>

                                                            <form:select path="${titlePath}.appointmentDuration"
                                                                         id="${titlePath}.appointmentDuration"
                                                                         disabled="${isNotDepartment}">
                                                                <form:option value="NA">-- Select --</form:option>
                                                                <c:forEach var="opt" items="${form.appointmentDurations}">
                                                                    <spring:message text="${tooltips.appointmentDurationOption}" arguments="${opt.basis},${opt.paidOver}" var="appointmentDurationsOption" />
                                                                    <form:option value="${opt}" title="${appointmentDurationsOption}">${opt.label}</form:option>

                                                                    <%-- use commented code if apply tooltip from AppointmentDuration enum --%>
                                                                    <%-- <form:option value="${opt}" title="${opt.description}">${opt.label}</form:option> --%>
                                                                </c:forEach>
                                                            </form:select>
                                                        </div>
                                                    </fieldset>

                                                    <fieldset class="formline">
                                                        <legend class="formhelp">${strings.salaryPeriod}</legend>

                                                        <fieldset class="cell" title="${tooltips.salaryPeriod}">
                                                            <legend class="f_required">${labels.salaryPeriod}</legend>

                                                            <form:radiobuttons path="${titlePath}.salaryPeriod"
                                                                               items="${form.salaryPeriods}"
                                                                               itemLabel="label"
                                                                               disabled="${isNotDepartment}"/>
                                                        </fieldset>

                                                        <div class="cells salaries">
                                                            <div class="cell" title="${tooltips.periodSalary}">
                                                                <spring:bind path="${titlePath}.periodSalary">
                                                                    <label for="${status.expression}" class="f_required">${title.salaryPeriod.description}</label>

                                                                    <input type="text"
                                                                           id="${status.expression}"
                                                                           name="${status.expression}"
                                                                           value="${status.value < 0.0 ? '' : status.value}"
                                                                           size="9"
                                                                           maxlength="9"
                                                                           ${isNotDepartment ? 'disabled' : ''}>
                                                                </spring:bind>
                                                            </div>

                                                            <div class="cell" title="${tooltips.annualSalary}">
                                                                <spring:bind path="${titlePath}.annualSalary">
                                                                    <label for="${status.expression}" class="f_required">${labels.annualSalary}</label>

                                                                    <input type="text"
                                                                           id="${status.expression}"
                                                                           name="${status.expression}"
                                                                           value="${status.value < 0.0 ? '' : status.value}"
                                                                           size="11"
                                                                           maxlength="11"
                                                                           ${isNotDepartment ? 'disabled' : ''}>
                                                                </spring:bind>
                                                            </div>
                                                        </div>
                                                    </fieldset>

                                                    <c:if test="${!isAppointment && assignmentStatus.key=='PRESENT'}">
                                                        <c:set var="showQuarterCount"
                                                               value="${form.actionType == 'REAPPOINTMENT' or
                                                                        form.actionType == 'APPOINTMENT_INITIAL_CONTINUING'}"/>

                                                        <fieldset class="formline">
                                                            <div class="cell" title="${showQuarterCount ? tooltips.quarterCount : tooltips.yearsAtRank}">
                                                                <spring:bind path="${titlePath}.yearsAtRank">
                                                                    <label for="${status.expression}"
                                                                           class="f_required">${showQuarterCount ? labels.quarterCount : labels.yearsAtRank}</label>

                                                                    <input type="number"
                                                                           id="${status.expression}"
                                                                           name="${status.expression}"
                                                                           min="0"
                                                                           max="${showQuarterCount ? 24 : 50}"
                                                                           step="1"
                                                                           value="${status.value < 0 ? '' : status.value}"
                                                                           required
                                                                           ${isNotDepartment ? 'disabled' : ''}>
                                                                </spring:bind>
                                                            </div>

                                                            <c:if test="${not showQuarterCount}">
                                                                <div class="cell" title="${tooltips.yearsAtStep}">
                                                                    <spring:bind path="${titlePath}.yearsAtStep">
                                                                        <label for="${status.expression}"
                                                                               class="f_required">${labels.yearsAtStep}</label>

                                                                        <input type="number"
                                                                               id="${status.expression}"
                                                                               name="${status.expression}"
                                                                               min="0"
                                                                               max="50"
                                                                               step="1"
                                                                               value="${status.value < 0 ? '' : status.value}"
                                                                               required
                                                                               ${isNotDepartment ? 'disabled' : ''}>
                                                                    </spring:bind>
                                                                </div>
                                                            </c:if>
                                                        </fieldset>
                                                    </c:if>
                                                </fieldset>

                                                <button type="button"
                                                        class="remove"
                                                        title="${tooltips.removeTitle}"
                                                        ${isNotDepartment ? 'disabled' : ''}>${labels.removeTitle}</button>
                                            </div>
                                        </c:forEach>

                                        <button type="button"
                                                class="add"
                                                title="${tooltips.addTitle}"
                                                ${isNotDepartment ? 'disabled' : ''}>${labels.addTitle}</button>
                                    </fieldset>

                                    <button type="button"
                                            class="remove"
                                            title="${tooltips.removeAppointment}"
                                            ${isNotDepartment ? 'disabled' : ''}>${labels.removeAppointment}</button>
                                </div>
                            </c:forEach>

                            <button type="button"
                                    class="add"
                                    title="${tooltips.addAppointment}"
                                    ${isNotDepartment ? 'disabled' : ''}>${labels.addAppointment}</button>
                        </fieldset>
                    </div></c:forEach>
                </div>
            </div>

            <div class="buttonset">
                <input type="submit"
                       name="_eventId_save"
                       value="${labels.save}"
                       id="save"
                       title="${tooltips.save}">
                <input type="submit"
                       name="_eventId_cancel"
                       value="${labels.cancel}"
                       id="cancel"
                       title="${tooltips.cancel}"
                       formnovalidate>
            </div>
        </form:form>
    </div>

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
