<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; Confirm Edit Action Form</title>

        <%@ include file="/jsp/metatags.html" %>
        
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/confirmlaunchaction.css'/>">
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="Confirm Edit Action Form" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>Confirm Edit Action Form</h1>
            <div id="maindialog" class="pagedialog">

                <div class="mivDialog">
                    <div class="reason">
                        <c:forEach var="message" items="${messages}">
                        <p>${message}</p>
                        </c:forEach>
                    </div>

                    <div class="select">Select Continue to edit the Action Form</div>

                    <form:form method="get" commandName="form" cssClass="mivdata">
                        <div class="buttonset">
                            <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
                            <input type="hidden" name="_flowId" value="${flowRequestContext.activeFlow.id}">
                            <input type="submit"
                                   name="_eventId_launch"
                                   value="Continue"
                                   id="continue"
                                   title="Continue to transfer to edit the action form"
                                   formnovalidate>
                            <input type="submit"
                                   name="_eventId_cancel"
                                   value="Cancel"
                                   id="cancel"
                                   title="I don't want to edit the action form, send me back to Open Action"
                                   formnovalidate>
                        </div><%-- .buttonset --%>
                    </form:form>

                </div><%-- .mivDialog --%>

             </div><%-- #maindialog --%>
        </div><%-- #main --%>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
