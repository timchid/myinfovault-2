<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE html>
<html>

    <c:set var="packetRequestAction" value="Packet Request"/>
    <c:if test="${requestAction == 'CANCELED'}">
          <c:set var="packetRequestAction" value="Packet Request Cancellation"/>
    </c:if>
      
    <c:set var="pageTitle" value="${form.dossier.action.candidate.displayName}'s&nbsp;${packetRequestAction}&nbsp;${strings.pageTitle}"/>
    <c:set var="successMessage" value="${form.dossier.action.candidate.displayName}'s&nbsp;${packetRequestAction}&nbsp;${strings.successMessage}"/>
    <c:if test="${skipNotification}">
        <c:set var="pageTitle" value="${form.dossier.action.candidate.displayName}'s&nbsp;${packetRequestAction}&nbsp;${strings.pageTitleNoEmail}"/>
        <c:set var="successMessage" value="${form.dossier.action.candidate.displayName}'s&nbsp;${packetRequestAction}&nbsp;${strings.successMessageNoEmail}"/>
    </c:if>

    
    <head>
        <title>MIV &ndash;&nbsp;${pageTitle}</title>

        <%@ include file="/jsp/metatags.html" %>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${pageTitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${pageTitle}</h1>

            <c:choose>
                <c:when test="${skipNotification}">
                    <p>${successMessage}&nbsp;<fmt:formatDate pattern="MM/dd/yy, h:mm a" value="<%=new java.util.Date()%>"/></p>
                </c:when>
                <c:otherwise>
                    <p>${successMessage}&nbsp;<fmt:formatDate pattern="MM/dd/yy, h:mm a" value="${form.emailSent}"/></p>
           
                    <p><strong>${strings.toLabel}:</strong> ${form.emailTo}</p>
             
                    <c:if test="${!empty form.emailCc}">
                        <p><strong>${strings.ccLabel}:</strong> ${form.emailCc}</p>
                    </c:if>
                </c:otherwise>
            </c:choose>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>