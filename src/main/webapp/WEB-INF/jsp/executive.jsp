<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${strings.pageTitle}</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/executive.css'/>"/>

        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/widget-suggestion.js'/>"></script>
        <jwr:script src="/bundles/mivvalidation.js" />
        <script type="text/javascript" src="<c:url value='/js/executive.js'/>"></script>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${strings.pageTitle}</h1>

            <jsp:include page="binderrors.jsp" />

            <p>${strings.instruction}</p>

            <div class="mivDialog">
            <form:form method="post" commandName="form">
                <div class="formline" title="${tooltips.viceProvost}">
                   <spring:bind path="viceProvostID">
                        <label for="${status.expression}" class="f_required">${labels.viceProvost}</label>

                        <input type="text"
                               class="suggestion${status.error ? ' errorfield': ''}"
                               data-initial="${not empty form.viceProvost ? form.viceProvost.displayName : ''}"
                               id="${status.expression}"
                               name="${status.expression}"
                               value="${status.value}"
                               required>
                  </spring:bind>
                </div>

                <div class="formline" title="${tooltips.provost}">
                  <spring:bind path="provostID">
                        <label for="${status.expression}" class="f_required">${labels.provost}</label>

                        <input type="text"
                               class="suggestion${status.error ? ' errorfield': ''}"
                               data-initial="${not empty form.provost ? form.provost.displayName : ''}"
                               id="${status.expression}"
                               name="${status.expression}"
                               value="${status.value}"
                               required>
                   </spring:bind>
                </div>

                <div class="formline" title="${tooltips.chancellor}">
                  <spring:bind path="chancellorID">
                        <label for="${status.expression}" class="f_required">${labels.chancellor}</label>

                        <input type="text"
                               class="suggestion${status.error ? ' errorfield': ''}"
                               data-initial="${not empty form.chancellor ? form.chancellor.displayName : ''}"
                               id="${status.expression}"
                               name="${status.expression}"
                               value="${status.value}"
                               required>
                    </spring:bind>
                </div>

                <div class="buttonset">
                    <input type="submit"
                           name="_eventId_save"
                           value="${labels.save}"
                           id="save"
                           title="${tooltips.save}">
                    <input type="submit"
                           name="_eventId_cancel"
                           value="${labels.cancel}"
                           id="cancel"
                           title="${tooltips.cancel}"
                           formnovalidate>
                </div>
            </form:form>
            </div><%-- div.mivDialog --%>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
