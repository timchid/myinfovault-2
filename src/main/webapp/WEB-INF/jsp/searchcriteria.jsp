<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; <%--View MIV Users</title>
    --%>${strings.pagetitle}<%--
    --%><c:if test="${fn:length(strings.subtitle)>0}">:${strings.subtitle}</c:if>
    </title>
   <script type="text/javascript">
      var mivConfig = new Object();
      mivConfig.isIE = false;
    </script>

  <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" >
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
</head>
<body>

    <!-- Adding report help file link -->
    <c:if test="${flowRequestContext.activeFlow.id == 'report-search-flow'}" >
        <c:set var="helpPage" scope="request" value="reports.html"/>
    </c:if>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.description}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${strings.description}</h1>

        <div id="maindialog" class="pagedialog">
            <div class="mivDialog wideDialog">
                <div id="userheading">
                    <span class="standout">${user.targetUserInfo.displayName}</span>
                </div>

                <form:form commandName="searchCriteria" method="post" cssClass="mivdata">
                    <c:choose>
                        <c:when test="${flowRequestContext.activeFlow.id == 'openactions-report-search-flow'}">
                            <jsp:include page="searchcriteriacheckboxes.jsp" />
                        </c:when>
                        
                        <c:otherwise>
                            <jsp:include page="searchcriteriaform.jsp" />
                        </c:otherwise>
                    </c:choose>
                </form:form>

            </div><!-- widedialog -->
        </div><!-- maindialog -->
    </div><!--main-->
    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>

</html><%--
 vim:ts=8 sw=2 et:
--%>
