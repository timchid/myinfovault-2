<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; <%--Manage Open Actions: Search Results
    --%>${strings.pagetitle}<c:if test="${fn:length(strings.subtitle)>0}">:
    ${strings.subtitle}</c:if>
    </title>
    <script type="text/javascript">
        var mivConfig = new Object();
        mivConfig.isIE = false;
    </script>

    <%@ include file="/jsp/metatags.html" %>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <style type="text/css" media="screen">
        /*
         * Override styles needed due to the mix of different CSS sources!
         */
        .dataTables_info { padding-top: 0; }
        .dataTables_paginate { padding-top: 0; }
        .css_right { float: right; }
        #theme_links span { float: left; padding: 2px 10px; }
    </style>

    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>

    <script type="text/javascript">
        $(document).ready( function() {
            fnInitDataTable("userlist", {"bHelpBar":true, "bDeferRender":true, "aFilterColumns": []});
        } );
    </script>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>
<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
      <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>

      <c:if test="${flowRequestContext.activeFlow.id != 'assignReviewers-flow' &&
                    flowRequestContext.activeFlow.id != 'reviewDossiers-flow' &&
                    flowRequestContext.activeFlow.id != 'viewdossierstatus-flow'}">
      &gt; <a href="<c:url value='${context.request.contextPath}'>
                    <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                    <c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
                    <c:param name='_eventId' value='search'/>
                    </c:url>" title="${strings.prev}">${strings.prev}</a>
      </c:if>

      &gt; ${strings.description}
    </div><!-- breadcrumbs -->

    <div id="main">
      <h1>${strings.description}${not empty searchCriteriaText ? " for " : ""}${searchCriteriaText}</h1>
<%--
      <miv:permit action="! View Snapshots" usertype="REAL">
      <p><strong>You are not authorized to view this page.</strong></p>
      </miv:permit> --%>
      <miv:permit roles="CANDIDATE|VICE_PROVOST_STAFF|SCHOOL_STAFF|DEPT_STAFF|SENATE_STAFF" usertype="REAL">
     <c:if test="${empty results}">
      <c:choose>
      <c:when test="${flowRequestContext.activeFlow.id == 'assignReviewers-flow' ||
                      flowRequestContext.activeFlow.id == 'reviewDossiers-flow'}">
          <p>There are no available dossiers for review at this time.</p>
          <p>
          Note: The dossier review period must be selected as "Open" on the
          "Manage Open Actions &gt; Open Action" page in order for assigned reviewers
          to be able to view dossiers.
          </p>
      </c:when>
      <c:otherwise>
          <p>We're sorry, no matching results were found.</p>
      </c:otherwise>
      </c:choose>

      </c:if>

      <c:if test="${!empty results}">
        <p>${strings.message}</p>
        <c:if test="${flowRequestContext.activeFlow.id == 'viewdossiersnapshots-flow'}">
           <p><em>Note: Dossiers routed prior to January 19, 2010 (v3 upgrade) will not have snapshots appearing in MIV.</em></p>
        </c:if>

        <div class="searchresults">Search Results = ${fn:length(results)}</div>

        <div class="full_width">
            <table id="userlist" class="display">
                <thead>
                    <tr>
                    <c:choose>
                    <c:when test="${flowRequestContext.activeFlow.id == 'assignReviewers-flow' ||
                                    flowRequestContext.activeFlow.id == 'reviewDossiers-flow'}">
                      <th class="datatable-column-filter" title="MIV User">MIV User</th>
                      <th class="datatable-column-filter" title="Appointment">Appointment</th>
                      <th class="datatable-column-filter" title="School/College">School/College</th>
                      <th class="datatable-column-filter" title="Department">Department</th>
                      <th class="datatable-column-filter" title="Delegation Authority">Delegation Authority</th>
                      <th class="datatable-column-filter" title="Action">Action</th>
                      <th class="datatable-column-filter" title="Location">Location</th>
                    </c:when>
                    <c:when test="${flowRequestContext.activeFlow.id == 'viewdossierarchive-flow'}">
                      <th class="datatable-column-filter" title="MIV User">MIV User</th>
                      <th class="datatable-column-filter" title="School/College">School/College</th>
                      <th class="datatable-column-filter" title="Department">Department</th>
                      <th class="datatable-column-filter" title="Delegation Authority">Delegation Authority</th>
                      <th class="datatable-column-filter" title="Action">Action</th>
                      <th class="datatable-column-filter" title="Date Submitted to Dept.">Date Submitted to Dept.</th>
                      <th class="datatable-column-filter" title="Archive Date">Archive Date</th>
                    </c:when>
                    <c:when test="${flowRequestContext.activeFlow.id == 'viewdossiersnapshots-flow'}">
                      <th class="datatable-column-filter" title="MIV User">MIV User</th>
                      <th class="datatable-column-filter" title="School/College">School/College</th>
                      <th class="datatable-column-filter" title="Department">Department</th>
                      <th class="datatable-column-filter" title="Delegation Authority">Delegation Authority</th>
                      <th class="datatable-column-filter" title="Action">Action</th>
                      <th class="datatable-column-filter" title="Date Submitted to Dept.">Date Submitted to Dept.</th>
                    </c:when>
                    <c:otherwise>
                      <th class="datatable-column-filter" title="MIV User">MIV User</th>
                      <th class="datatable-column-filter" title="Appointment">Appointment</th>
                      <th class="datatable-column-filter" title="School/College">School/College</th>
                      <th class="datatable-column-filter" title="Department">Department</th>
                      <th class="datatable-column-filter" title="Delegation Authority">Delegation Authority</th>
                      <th class="datatable-column-filter" title="Action">Action</th>
                      <th class="datatable-column-filter" title="Date Submitted to Dept.">Date Submitted to Dept.</th>
                      <th class="datatable-column-filter" title="Date Last Routed">Date Last Routed</th>
                      <th class="datatable-column-filter" title="Location">Location</th>

                      <c:if test="${flowRequestContext.activeFlow.id == 'viewpostauditdossiers-flow'}">
                          <th class="datatable-column-filter" title="Review period">Review Period</th>
                      </c:if>
                    </c:otherwise>
                    </c:choose>
                    </tr>
                </thead>

                <tbody>
                <!-- Set flag for SYS_ADMIN role to view all dossiers regarless of location  -->
                <c:set var="canViewAll" value="false" scope="page" />
                <miv:permit roles="SYS_ADMIN">
                <c:set var="canViewAll" value="true" scope="page" />
                </miv:permit>
                <c:forEach var="usr" items="${results}" varStatus="row">
                    <tr class="${(row.count)%2==0?'even':'odd'}"><%--
                    --%><c:choose><%--
                    --%><c:when test="${flowRequestContext.activeFlow.id == 'openactions-flow' ||
                                        flowRequestContext.activeFlow.id == 'viewpostauditdossiers-flow' ||
                                        flowRequestContext.activeFlow.id == 'viewdossierstatus-flow'}">
                        <!-- If dossier is at Candidate location, do not show link to OpenActions unless canViewAll flag is set -->
                        <td>
                            <c:choose>
                                <c:when test="${usr.actionLocation == 'Candidate' && !canViewAll}">
                                <a title="Action is not in process and must be submitted to the Department for ${usr.sortName}">${usr.sortName}</a>
                                </c:when>
                                <c:otherwise>
                                <c:url var='nextLink' value='OpenActions'><%--
                                    --%><c:param name='userId' value='${usr.userId}'/><%--
                                    --%><c:param name='dossierId' value='${usr.dossierId}'/><%--
                                    --%><c:param name='_eventId' value='select'/><%--
                                    --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
                                    </c:url>
                                <a title="Manage Action for ${usr.sortName}"
                                   href="<c:out value='${nextLink}' escapeXml='true'/>">${usr.sortName}</a>
                                </c:otherwise>
                            </c:choose>
                        </td><%--
                    --%></c:when><%--
                    --%><c:when test="${flowRequestContext.activeFlow.id == 'assignReviewers-flow'}">
                        <td><%--
                      --%><c:url var='nextLink' value='AssignReviewers'><%--
                        --%><c:param name='userId' value='${usr.userId}'/><%--
                        --%><c:param name='dossierId' value='${usr.dossierId}'/><%--
                        --%><c:param name='schoolId' value='${usr.schoolId}'/><%--
                        --%><c:param name='departmentId' value='${usr.departmentId}'/><%--
                        --%><c:param name='_eventId' value='select'/><%--
                        --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
                     <%-- MIV-3044 Parse the first and last name from the sortName. This gets the middle name as well
                        --%><c:set var='nameLength' value='${fn:length(usr.sortName)}'/><%--
                        --%><c:set var='commaPos' value='${fn:indexOf(usr.sortName,",")}'/><%--
                        --%><c:set var='lastName' value='${fn:substring(usr.sortName,0,commaPos)}'/><%--
                        --%><c:set var='firstName' value='${fn:trim(fn:substring(usr.sortName,commaPos+1,nameLength))}'/><%--
                        --%><c:param name='userName' value='${firstName} ${lastName}'/><%--
                      --%></c:url>
                            <a title="Assign reviewers for ${usr.sortName} dossier."
                               href="<c:out value='${nextLink}' escapeXml='true'/>">${usr.sortName}</a>
                        </td><%--
                    --%></c:when><%--
                    --%><c:when test="${flowRequestContext.activeFlow.id == 'reviewDossiers-flow'}">
                        <td><%--
                      --%><c:url var='nextLink' value='ReviewDossiers'><%--
                        --%><c:param name='dossierId' value='${usr.dossierId}'/><%--
                        --%><c:param name='schoolId' value='${usr.schoolId}'/><%--
                        --%><c:param name='departmentId' value='${usr.departmentId}'/><%--
                        --%><c:param name='_eventId' value='select'/><%--
                        --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
                        --%><c:param name='userName' value='${usr.givenName} ${usr.surname}'/><%--
                      --%></c:url>
                            <a title="Review dossier for ${usr.sortName}."
                               href="<c:out value='${nextLink}' escapeXml='true'/>">${usr.sortName}</a>
                        </td><%--
                    --%></c:when><%--
                    --%><c:when test="${flowRequestContext.activeFlow.id == 'viewdossierarchive-flow'}">
                        <td><%--
                      --%><c:url var='nextLink' value='ViewDossierArchive'><%--
                        --%><c:param name='dossierId' value='${usr.dossierId}'/><%--
                        --%><c:param name='schoolId' value='${usr.schoolId}'/><%--
                        --%><c:param name='departmentId' value='${usr.departmentId}'/><%--
                        --%><c:param name='_eventId' value='select'/><%--
                        --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
                     <%-- MIV-3044 Parse the first and last name from the sortName. This gets the middle name as well
                        --%><c:set var='nameLength' value='${fn:length(usr.sortName)}'/><%--
                        --%><c:set var='commaPos' value='${fn:indexOf(usr.sortName,",")}'/><%--
                        --%><c:set var='lastName' value='${fn:substring(usr.sortName,0,commaPos)}'/><%--
                        --%><c:set var='firstName' value='${fn:substring(usr.sortName,commaPos+1,nameLength)}'/><%--
                        --%><c:param name='userName' value='${firstName} ${lastName}'/><%--
                      --%></c:url>
                            <a title="View archived dossiers for ${usr.sortName}."
                               href="<c:out value='${nextLink}' escapeXml='true'/>">${usr.sortName}</a>
                        </td><%--
                    --%></c:when><%--
                    --%><c:when test="${flowRequestContext.activeFlow.id == 'viewdossiersnapshots-flow'}">
                        <td><%--
                      --%><c:url var='nextLink' value='ViewDossierSnapshots'><%--
                        --%><c:param name='dossierId' value='${usr.dossierId}'/><%--
                        --%><c:param name='schoolId' value='${usr.schoolId}'/><%--
                        --%><c:param name='departmentId' value='${usr.departmentId}'/><%--
                        --%><c:param name='_eventId' value='select'/><%--
                        --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
                     <%-- MIV-3044 Parse the first and last name from the sortName. This gets the middle name as well
                        --%><c:set var='nameLength' value='${fn:length(usr.sortName)}'/><%--
                        --%><c:set var='commaPos' value='${fn:indexOf(usr.sortName,",")}'/><%--
                        --%><c:set var='lastName' value='${fn:substring(usr.sortName,0,commaPos)}'/><%--
                        --%><c:set var='firstName' value='${fn:substring(usr.sortName,commaPos+1,nameLength)}'/><%--
                        --%><c:param name='userName' value='${firstName} ${lastName}'/><%--
                      --%></c:url>
                            <a title="View dossier snapshots for ${usr.sortName}."
                               href="<c:out value='${nextLink}' escapeXml='true'/>">${usr.sortName}</a>
                        </td><%--
                    --%></c:when><%--
                    --%><c:otherwise><%--
             --%><td>${usr.sortName}</td><%--
                    --%></c:otherwise><%--
                --%></c:choose>
            <c:choose>
            <c:when test="${flowRequestContext.activeFlow.id == 'assignReviewers-flow' ||
                            flowRequestContext.activeFlow.id == 'reviewDossiers-flow'}">
                  <td>${usr.appointmentType}</td>
                  <td>${usr.schoolName}</td>
                  <td>${!empty usr.departmentName ? usr.departmentName : '&nbsp;'}</td>
                  <td>${usr.delegationAuthority}</td>
                  <td>${usr.actionType}</td>
                  <td>${usr.locationDescription}</td>
            </c:when>
            <c:when test="${flowRequestContext.activeFlow.id == 'viewdossierarchive-flow'}">
                  <td>${usr.schoolName}</td>
                  <td>${!empty usr.departmentName ? usr.departmentName : '&nbsp;'}</td>
                  <td>${usr.delegationAuthority}</td>
                  <td>${usr.actionType}</td>
                  <td>${usr.submitDate}</td>
                  <td>${usr.archiveDate}</td>
            </c:when>
            <c:when test="${flowRequestContext.activeFlow.id == 'viewdossiersnapshots-flow'}">
                  <td>${usr.schoolName}</td>
                  <td>${!empty usr.departmentName ? usr.departmentName : '&nbsp;'}</td>
                  <td>${usr.delegationAuthority}</td>
                  <td>${usr.actionType}</td>
                  <td>${usr.submitDate}</td>
            </c:when>
            <c:otherwise>
                  <td>${usr.appointmentType}</td>
                  <td>${usr.schoolName}</td>
                  <td>${!empty usr.departmentName ? usr.departmentName : '&nbsp;'}</td>
                  <td>${usr.delegationAuthority}</td>
                  <td>${usr.actionType}</td>
                  <td>${usr.submitDate}</td>
                  <td>${usr.lastRoutedDate}</td>
                  <td>${usr.locationDescription}</td>

                  <c:if test="${flowRequestContext.activeFlow.id == 'viewpostauditdossiers-flow'}">
                      <td>${usr.reviewStatus.label}</td>
                  </c:if>
            </c:otherwise>
              </c:choose>
              </tr>
            </c:forEach>
                </tbody>
            </table>
        </div><!-- full_width -->
    </c:if><%-- end if test="${!empty results}" --%>
  </miv:permit><%-- whole page blocked for people w/o View Snapshots permission --%>
  </div><!--main-->
    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>

</html>
