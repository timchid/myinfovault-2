<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${strings.pageTitle}</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css' />"/>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/raf.css' />"/>
        
        <script type="text/javascript" src="<c:url value='/js/widget-selection.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/rafselect.js'/>"></script>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${strings.pageTitle}</h1>

            <div id="maindialog" class="dojoDialog pagedialog mivDialog">
                <span class="standout">${form.raf.dossier.candidate.displayName}</span>
                
                <form:form cssClass="mivdata" method="post" commandName="form">
                    <fieldset class="mivfieldset selection">
                        <legend>${strings.instruction}</legend>
                        
                        <label>
                            ${strings.actionTypeLabel}
                            <form:select multiple="true"
                                         path="actionType"
                                         items="${form.dossierActionTypes}"
                                         itemLabel="description"/>
                        </label>
                        
                        <label>
                            ${strings.delegationAuthorityLabel}
                            <form:select multiple="true"
                                         path="delegationAuthority"
                                         items="${form.dossierDelegationAuthorities}"
                                         itemLabel="description"/>
                        </label>
                    </fieldset>
                    
                    <div class="description">${form.delegationAuthority.description}
                                             ${form.actionType.description}</div>
                    
                    <div class="buttonset">
                        <input type="submit"
                               title="${strings.selectTitle}"
                               name="_eventId_select"
                               value="${strings.selectLabel}"/>
                               
                        <input type="submit"
                               title="${strings.cancelTitle}"
                               name="_eventId_cancel"
                               value="${strings.cancelLabel}"/>
                    </div>
                </form:form>
            </div>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
